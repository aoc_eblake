#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>

static int debug_level = -1;
static void __attribute__((unused))
debug_raw(int level, const char *fmt, ...) {
  va_list ap;
  if (debug_level < 0)
    debug_level = atoi(getenv("DEBUG") ?: "0");
  if (debug_level >= level) {
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
  }
}
#define debug(...) debug_raw(1, __VA_ARGS__)

#define LIMIT 21
struct point {
  bool asteroid;
  int blocked;
};
static struct point grid[LIMIT][LIMIT];
static size_t bound;
struct info {
  int level;
  float slope;
  int coord;
};
static struct info list[LIMIT * LIMIT];
static int next;

static bool
check_one (int to, int from_x, int from_y) {
  int to_x = to % bound, to_y = to / bound;
  int dx = to_x - from_x, dy = to_y - from_y;
  int from = from_y * bound + from_x;
  bool result = false;

  debug_raw(2, " checking %d.%d, slope %d/%d", to_x, to_y, dy, dx);
  if (grid[to_y][to_x].blocked == from) {
    debug_raw(2, " already visited\n");
    return false;
  }
  do {
    if (grid[to_y][to_x].asteroid)
      result = true;
    grid[to_y][to_x].blocked = from;
    debug_raw(2, " [visiting %d.%d]", to_x, to_y);
    to_x += dx;
    to_y += dy;
  } while (to_x >= 0 && to_x < bound && to_y >= 0 && to_y < bound);
  debug_raw(2, " %s\n", result ? "hit" : "clear");
  return result;
}

static int
check(int point) {
  int x = point % bound, y = point / bound;
  int i, total = 0;

  debug_raw(2, "checking %d=%d.%d %c\n", point, x, y,
            grid[y][x].asteroid ? '#' : '.');
  if (!grid[y][x].asteroid)
    return 0;
  for (i = point - 1; i >= 0; i--)
    if (check_one(i, x, y))
      total++;
  for (i = point + 1; i < bound * bound; i++)
    if (check_one(i, x, y))
      total++;
  debug_raw(2, "hit %d\n", total);
  return total;
}

static void
prep_one (int to, int from_x, int from_y) {
  int to_x = to % bound, to_y = to / bound;
  int dx = to_x - from_x, dy = to_y - from_y;
  int from = from_y * bound + from_x;
  float slope = (float)-dy / dx;
  int level = 1;

  debug(" prepping %d.%d, slope %d/%d=%g", to_x, to_y, dy, dx, slope);
  if (grid[to_y][to_x].blocked == -from) {
    debug(" already visited\n");
    return;
  }
  do {
    if (grid[to_y][to_x].asteroid) {
      list[next].level = level + (dx < 0);
      list[next].slope = slope;
      list[next++].coord = to_x * 100 + to_y;
      level += 2;
    }
    grid[to_y][to_x].blocked = -from;
    debug(" [visiting %d.%d]", to_x, to_y);
    to_x += dx;
    to_y += dy;
  } while (to_x >= 0 && to_x < bound && to_y >= 0 && to_y < bound);
  debug(" %d prepped\n", next);
}

static void
prep(int point) {
  int x = point % bound, y = point / bound;
  int i;

  list[next].level = 0;
  list[next].slope = 0;
  list[next++].coord = x * 10 + y;
  for (i = point - 1; i >= 0; i--)
    prep_one(i, x, y);
  for (i = point + 1; i < bound * bound; i++)
    prep_one(i, x, y);
}

static int
comp(const void *pa, const void *pb) {
  const struct info *a = pa, *b = pb;
  if (a->level < b->level)
    return -1;
  if (a->level > b->level)
    return 1;
  if (a->slope > b->slope)
    return -1;
  if (a->slope < b->slope)
    return 1;
  printf("unexpected equality for coord %d and %d\n", a->coord, b->coord);
  exit(1);
}

int main(int argc, char **argv) {
  size_t len = 0, count = 0, asteroids = 0, total = 0;
  int i, best_i = 0, best_t;
  char *line;

  debug("");
  if (argc > 1)
    if (!(stdin = freopen(argv[1], "r", stdin))) {
      perror("failure");
      exit(2);
    }

  while ((i = getline(&line, &len, stdin)) >= 0) {
    if (bound == 0)
      bound = i - 1;
    else if (bound != i - 1) {
      printf("input not a square: column\n");
      exit(2);
    }
    for (i = 0; i < bound; i++)
      if (line[i] == '#') {
        grid[count][i].asteroid = true;
        asteroids++;
      }
    count++;
  }
  if (count != bound) {
    printf("input not a square: row\n");
    exit(2);
  }
  printf("Read %zu lines, %zd asteroids\n", count, asteroids);

  for (i = 0; i < bound * bound; i++) {
    total = check(i);
    if (total > best_t) {
      best_t = total;
      best_i = i;
    }
  }
  printf("Best %d at %zd.%zd\n", best_t, best_i % bound, best_i / bound);

  prep(best_i);
  qsort(list, asteroids, sizeof list[0], comp);
  for (i = 0; i < asteroids; i++)
    debug("list[%d] = %d %g %d\n", i, list[i].level, list[i].slope,
          list[i].coord);
  if (asteroids > 200)
    printf("200th vaporized at %d\n", list[200].coord);
  return 0;
}
