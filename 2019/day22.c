#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>

static int debug_level = -1;
static void
debug_init(void) {
  if (debug_level < 0)
    debug_level = atoi(getenv("DEBUG") ?: "0");
}

static void __attribute__((format(printf, 2, 3)))
debug_raw(int level, const char *fmt, ...) {
  va_list ap;
  if (debug_level < 0)
    debug_level = atoi(getenv("DEBUG") ?: "0");
  if (debug_level >= level) {
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
  }
}
#define debug(...) debug_raw(1, __VA_ARGS__)

static int __attribute__((noreturn)) __attribute__((format(printf, 1, 2)))
die(const char *fmt, ...)
{
  va_list ap;
  va_start(ap, fmt);
  vprintf(fmt, ap);
  va_end(ap);
  putchar('\n');
  exit(1);
}

#define LIMIT 100
enum op { NEW, CUT, DEAL };
static struct act {
  enum op op;
  int n;
  long long n1;
} actions[LIMIT];
static int nactions;

/* https://www.geeksforgeeks.org/multiplicative-inverse-under-modulo-m/
 * assumes a and m are co-prime, m > 1 */
static long long
modInverse(long long a, long long m) {
  long long m0 = m;
  long long y = 0, x = 1;

  while (a > 1) {
    long long q = a / m;
    long long t = m;
    m = a % m;
    a = t;
    t = y;
    y = x - q * y;
    x = t;
  }
  if (x < 0)
    x += m0;
  return x;
}

/* https://www.geeksforgeeks.org/how-to-avoid-overflow-in-modular-multiplication/ */
static long long
modMul(long long a, long long b, long long mod) {
  long long r = 0;

  while (b > 0) {
    if (b & 1)
      r = (r + a) % mod;
    a = (a * 2) % mod;
    b /= 2;
  }
  return r % mod;
}

/* https://en.wikipedia.org/wiki/Modular_exponentiation */
static long long
modPow(long long base, long long exp, long long m) {
  long long r = 1;
  while (exp) {
    if (exp & 1)
      r = modMul(r, base, m);
    exp /= 2;
    base = modMul(base, base, m);
  }
  return r;
}

static long long
shuffle(long long size, long long track) {
  int i;

  debug("shuffling deck size %lld, while tracking card %lld\n", size, track);
  for (i = 0; i < nactions; i++) {
    switch (actions[i].op) {
    case NEW:
      track = size - 1 - track;
      debug("new deck moved card to %lld\n", track);
      break;
    case CUT:
      track = (size + track - actions[i].n) % size;
      debug("cut %d moved card to %lld\n", actions[i].n, track);
      break;
    case DEAL:
      track = modMul(track, actions[i].n, size);
      debug("deal %d moved card to %lld\n", actions[i].n, track);
      break;
    }
  }
  return track;
}

static long long
unshuffle(long long size, long long track) {
  int i;
  static int iter;

  debug("on unshuffle %d, deck size %lld undoing pos %lld\n",
        iter, size, track);
  for (i = nactions - 1; i >= 0; i--) {
    switch (actions[i].op) {
    case NEW:
      track = size - 1 - track;
      debug("new deck moved card from %lld\n", track);
      break;
    case CUT:
      track = (size + track + actions[i].n) % size;
      debug("cut %d moved card from %lld\n", actions[i].n, track);
      break;
    case DEAL:
      track = modMul(track, actions[i].n1, size);
      debug("deal %d moved card from %lld\n", actions[i].n, track);
      break;
    }
  }
  return track;
}

int main(int argc, char **argv) {
  size_t len = 0;
  char *line;
  long long size = 10007, track = 2019, remaining = 101741582076661, pos = 2020;
  int i, part1;
  long long a, b, y, z, part2;

  debug_init();
  if (argc > 1 && strcmp(argv[1], "-"))
    if (!(stdin = freopen(argv[1], "r", stdin))) {
      perror("failure");
      exit(2);
    }

  if (argc > 2)
    size = atoi(argv[2]);
  if (argc > 3)
    track = atoi(argv[3]);
  if (0U + track > size)
    die("card %lld not in deck of size %lld\n", track, size);
  if (argc > 4)
    remaining = atoll(argv[4]);

  while ((i = getline(&line, &len, stdin)) >= 0) {
    if (nactions >= LIMIT)
      die("recompile with larger LIMIT");
    if (line[0] == 'c') { /* "cut N" */
      actions[nactions].op = CUT;
      actions[nactions].n = atoi(line + 4);
    } else if (line[5] == 'w') { /* "deal with increment N" */
      actions[nactions].op = DEAL;
      actions[nactions].n = atoi(line + 20);
    } else { /* "deal into new stack */
      actions[nactions].op = NEW;
    }
    nactions++;
  }
  part1 = shuffle(size, track);
  printf("after shuffling %lld cards %d times, card %lld is at position %d\n",
         size, nactions, track, part1);
  for (i = 0; i < nactions; i++)
    if (actions[i].op == DEAL)
      actions[i].n1 = modInverse(actions[i].n, size);
  printf("sanity check: pos %d started with card %lld\n", part1,
         unshuffle(size, part1));

  /* With hints from https://www.reddit.com/r/adventofcode/comments/ee0rqi/2019_day_22_solutions/fbnifwk/
     X = 2020
     Y = f(X)
     Z = f(Y) = f(f(X))
     f(i) = A*i + B
     A*X + B = Y
     A*Y + B = Z
     A*(X - Y) = (Y - Z)
     A = (Y - Z) * (X - Y)^-1
     B = Y - A*X
     f(f(x)) = A*(A*x + B)+ B = A^2*x + A*x*B + B
     f^n(x) = A^n*x + A^(n-1)*B + A^(n-2)*B ... + B
            = A^n*x + (A^(n-1) + A^(n-2) ... + 1) * B
            = A^n*x + (A^(n-1) + A^(n-2) ... + 1)*(A-1) / (A-1) * B
            = A^n*x + (A^n - 1) *(A-1)^-1 * B
  */
  size = 119315717514047;
  printf("for reference, shuffle(0)=%lld shuffle(1)=%lld\n",
         shuffle(size, 0), shuffle(size, 1));
  printf("preparing to unshuffle %lld cards %lld times\n", size, remaining);
  for (i = 0; i < nactions; i++)
    if (actions[i].op == DEAL)
      actions[i].n1 = modInverse(actions[i].n, size);
  y = unshuffle(size, pos);
  z = unshuffle(size, y);
  a = modMul((y - z + size) % size, modInverse((pos + size - y) % size,
                                               size), size);
  b = (y + size - modMul(a, pos, size)) % size;
  printf("computed y=%lld z=%lld a=%lld b=%lld\n", y, z, a, b);
  part2 = (modMul(modPow(a, remaining, size), pos, size) +
           modMul(modMul(modPow(a, remaining, size) - 1,
                         modInverse(a - 1, size), size), b, size)) % size;
  printf("position %lld contains %lld\n", pos, part2);
  return 0;
}
