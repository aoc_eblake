#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <ctype.h>
#include <limits.h>
#include <unistd.h>

static int debug_level = -1;
static void
debug_init(void) {
  if (debug_level < 0)
    debug_level = atoi(getenv("DEBUG") ?: "0");
}

static void __attribute__((format(printf, 2, 3)))
debug_raw(int level, const char *fmt, ...) {
  va_list ap;
  if (debug_level < 0)
    debug_level = atoi(getenv("DEBUG") ?: "0");
  if (debug_level >= level) {
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
  }
}
#define debug(...) debug_raw(1, __VA_ARGS__)

static int __attribute__((noreturn)) __attribute__((format(printf, 1, 2)))
die(const char *fmt, ...)
{
  va_list ap;
  va_start(ap, fmt);
  vprintf(fmt, ap);
  va_end(ap);
  putchar('\n');
  exit(1);
}

#define MAX 133
#define PORTALS 60
enum dir { U, R, D, L };
struct grid_elt {
  char tile;
  uint8_t idx;
  short distance;
};
typedef struct grid_elt grid_row[MAX + 1];
typedef grid_row grid_full[MAX];
static grid_full grids[PORTALS];
static int maxx, maxy;
static int oleft, oright, otop, obottom; /* inclusive corner coords */
static int ileft, iright, itop, ibottom;
static int thick, scans;
union pt {
  int i;
  struct {
    uint8_t x, y, z;
    uint8_t d; /* enum dir */
  };
};
static struct portal {
  char id[3];
  union pt p1, p2;
} portals[PORTALS];
static int nportals;
static uint8_t aa = -1, zz = -1;

static union pt (*next)(union pt p, enum dir d);

static union pt next_flat(union pt p, enum dir d) {
  int8_t idx = grids[p.z][p.y][p.x].idx;

  p.d = d;
  if (idx >= 0 && portals[idx].p1.i == p.i) {
    p = portals[idx].p2;
    p.d ^= 2;
    debug("traveling through portal %s\n", portals[idx].id);
    return p;
  }
  if (idx >= 0 && portals[idx].p2.i == p.i) {
    p = portals[idx].p1;
    p.d ^= 2;
    debug("traveling through portal %s\n", portals[idx].id);
    return p;
  }
  switch (p.d) {
  case U: p.y--; break;
  case R: p.x++; break;
  case D: p.y++; break;
  case L: p.x--; break;
  }
  return p;
}

static union pt next_recursive(union pt p, enum dir d) {
  int8_t idx = grids[p.z][p.y][p.x].idx;
  int z = p.z;

  p.d = d;
  if (idx >= 0 && portals[idx].p1.x == p.x && portals[idx].p1.y == p.y &&
      portals[idx].p1.d == p.d) {
    if (idx == aa || idx == zz) {
      if (!z)
        die("unexpected end-point visit");
      debug("outer portal %s blocked at inner level %d\n", portals[idx].id, z);
      p.x = p.y = 2;
    } else if (!z) {
      debug("outer portal %s blocked at outer level\n", portals[idx].id);
      p.x = p.y = 2;
    } else {
      debug("traveling from level %d into outer portal %s\n", z,
            portals[idx].id);
      p = portals[idx].p2;
      p.d ^= 2;
      p.z = z - 1;
    }
    return p;
  }
  if (idx >= 0 && portals[idx].p2.x == p.x && portals[idx].p2.y == p.y &&
      portals[idx].p2.d == p.d) {
    p = portals[idx].p1;
    p.d ^= 2;
    if (p.z == nportals - 1) {
      debug("too much recursion into inner portal %s\n", portals[idx].id);
      p.x = p.y = 2;
    } else {
      debug("traveling from level %d into inner portal %s\n", z,
            portals[idx].id);
      p.z = z + 1;
    }
    return p;
  }
  switch (p.d) {
  case U: p.y--; break;
  case R: p.x++; break;
  case D: p.y++; break;
  case L: p.x--; break;
  }
  return p;
}

static int
minu(unsigned int a, unsigned b) {
  return a < b ? a : b;
}

static void
dump(int l) {
  int x, y;
  grid_row *g;

  if (debug_level < 2)
    return;
  if (l >= nportals)
    die("too much recursion");
  g = grids[l];
  debug("\n *** level %d grid:\n", l);
  for (y = 0; y < maxy; y++) {
    for (x = 0; x <= maxx; x++)
      debug(" %c", g[y][x].tile);
    for (x = 0; x < maxx; x++) {
      if (isatty(fileno(stderr)))
        switch (g[y][x].distance / 100U) {
        case 0: break;
        case 1: debug("\x1b[32m"); break;
        case 2: debug("\x1b[33m"); break;
        case 3: debug("\x1b[34m"); break;
        default: debug("\x1b[35m"); break;
        }
      debug("%2d", g[y][x].distance % 100);
      if (isatty(fileno(stderr)))
        debug("\x1b[0m");
    }
    debug("\n\n");
  }
}

static int deepest;
static short
scan_one(union pt p, short d, int depth) {
  int r = -1;

  if (depth > deepest)
    deepest = depth;
  if (grids[p.z][p.y][p.x].tile != '.')
    return -1;
  scans++;
  if ((unsigned) grids[p.z][p.y][p.x].distance < d)
    return -1;
  grids[p.z][p.y][p.x].distance = d;
  if (!p.z && grids[p.z][p.y][p.x].idx == zz) {
    debug("found ZZ at distance %d\n", d);
    return d;
  }
  if (d++ == SHRT_MAX)
    die("recompile with 32-bit distance");
  if (p.d != D)
    r = minu(r, scan_one(next(p, U), d, depth + 1));
  if (p.d != L)
    r = minu(r, scan_one(next(p, R), d, depth + 1));
  if (p.d != U)
    r = minu(r, scan_one(next(p, D), d, depth + 1));
  if (p.d != R)
    r = minu(r, scan_one(next(p, L), d, depth + 1));
  return r;
}

static void
portal(int x, int y, enum dir d, bool outer) {
  char id[3] = "";
  int i;

  switch (d) {
  case U:
    id[0] = grids[0][y - 2][x].tile;
    id[1] = grids[0][y - 1][x].tile;
    break;
  case R:
    id[0] = grids[0][y][x + 1].tile;
    id[1] = grids[0][y][x + 2].tile;
    break;
  case D:
    id[0] = grids[0][y + 1][x].tile;
    id[1] = grids[0][y + 2][x].tile;
    break;
  case L:
    id[0] = grids[0][y][x - 2].tile;
    id[1] = grids[0][y][x - 1].tile;
    break;
  }
  debug("labeling %s portal %s at %d,%d %c\n", outer ? "outer" : "inner",
        id, x, y, "URDL"[d]);
  for (i = 0; i < nportals; i++)
    if (!strcmp(portals[i].id, id))
      break;
  grids[0][y][x].idx = i;
  if (i == nportals) {
    if (++nportals == PORTALS)
      die("recompile with larger PORTALS");
    if (!outer)
      die("portal %s has no outer counterpart", id);
    memcpy(portals[i].id, id, 3);
    portals[i].p1.x = x;
    portals[i].p1.y = y;
    portals[i].p1.d = d;
    if (!strcmp("AA", id)) {
      aa = i;
      portals[i].p2.x = x - (d == L) + (d == R);
      portals[i].p2.y = y - (d == U) + (d == D);
      portals[i].p2.d = d ^ 2;
    } else if (!strcmp("ZZ", id))
      zz = i;
  } else {
    if (outer || portals[i].p2.x || i == aa || i == zz)
      die("portal %s seen too many times", id);
    portals[i].p2.x = x;
    portals[i].p2.y = y;
    portals[i].p2.d = d;
  }
}

static bool
in_maze(int x, int y) {
  char c = grids[0][y][x].tile;

  return c == '.' || c == '#';
}

static int
find_portals(void) {
  int count = 0, i, j;

  j = maxy / 2;
  while (in_maze(thick + 2, j))
    thick++;
  if (thick * 2 + 9 > maxx)
    die("can't find hole");
  oleft = 2;
  oright = maxx - 3;
  otop = 2;
  obottom = maxy - 3;
  ileft = oleft + thick - 1;
  iright = oright - thick + 1;
  itop = otop + thick - 1;
  ibottom = obottom - thick + 1;

  for (i = 0; i < maxx; i++)
    if (in_maze(i, 0) || in_maze(i, 1) ||
        in_maze(i, obottom + 1) || in_maze(i, obottom + 2))
    die("upper/lower rows incorrect");
  for (j = otop; j <= obottom; j++)
    if (in_maze(0, j) || in_maze(1, j) ||
        in_maze(oright + 1, j) || in_maze(oright + 2, j))
      die("left/right columns incorrect");
  if (grids[0][otop][oleft].tile != '#' ||
      grids[0][otop][oright].tile != '#' ||
      grids[0][obottom][oleft].tile != '#' ||
      grids[0][obottom][oright].tile != '#')
    die("corners incorrect");
  for (j = itop + 1; j < ibottom; j++)
    for (i = ileft + 1; i < iright; i++)
      if (in_maze(i, j))
        die("hole incorrect");

  for (j = otop, i = oleft + 1; i < oright; i++)
    if (grids[0][j][i].tile == '.')
      portal(i, j, U, true);
  for (i = oright, j = otop + 1; j < obottom; j++)
    if (grids[0][j][i].tile == '.')
      portal(i, j, R, true);
  for (j = obottom, i = oleft + 1; i < oright; i++)
    if (grids[0][j][i].tile == '.')
      portal(i, j, D, true);
  for (i = oleft, j = otop + 1; j < obottom; j++)
    if (grids[0][j][i].tile == '.')
      portal(i, j, L, true);

  for (j = itop, i = ileft + 1; i < iright; i++)
    if (grids[0][j][i].tile == '.')
      portal(i, j, D, false);
  for (i = iright, j = itop + 1; j < ibottom; j++)
    if (grids[0][j][i].tile == '.')
      portal(i, j, L, false);
  for (j = ibottom, i = ileft + 1; i < iright; i++)
    if (grids[0][j][i].tile == '.')
      portal(i, j, U, false);
  for (i = ileft, j = itop + 1; j < ibottom; j++)
    if (grids[0][j][i].tile == '.')
      portal(i, j, R, false);

  if (aa > PORTALS || zz > PORTALS)
    die("can't find aa/zz");
  for (i = 0; i < nportals; i++) {
    if (i == aa || i == zz)
      continue;
    if (!portals[i].p2.x)
      die("no matching inner portal for %s", portals[i].id);
  }
  return count;
}

int
main(int argc, char **argv) {
  int ch, i;
  int x = 0, y = 0;
  int best;

  debug_init();
  if (argc > 1 && strcmp(argv[1], "-"))
    if (!(stdin = freopen(argv[1], "r", stdin))) {
      perror("failure");
      exit(2);
    }

  memset(grids[0], -1, sizeof grids[0]);
  while ((ch = getchar()) != EOF) {
    grids[0][y][x].tile = ch;
    if (ch == '\n') {
      if (y && x != maxx)
        die("uneven line lengths");
      x = 0;
      maxy = ++y;
      continue;
    }
    if (y >= MAX)
      die("recompile with larger MAX y");
    x++;
    if (!y) {
      if (x >= MAX + 1)
        die("recompile with larger MAX x");
      maxx = x;
    } else if (x > maxx) {
      die("uneven line length");
    }
  }
  if (maxx < 11 || maxy < 11)
    die("grid is too small");

  dump(0);
  find_portals();
  for (i = 1; i < nportals; i++)
    memcpy(grids[i], grids[0], sizeof grids[0]);
  next = next_flat;

  printf("maze loaded. thickness:%d portals:%d AA:%d,%d ZZ:%d,%d\n",
         thick, nportals, portals[aa].p1.x, portals[aa].p1.y,
         portals[zz].p1.x, portals[zz].p1.y);
  best = scan_one(next(portals[aa].p2, portals[aa].p2.d), 0, 0);
  dump(0);
  printf("best flat path in %d steps, with %d scans and call depth %d\n",
         best, scans, deepest);

  memcpy(grids[0], grids[1], sizeof grids[0]);
  next = next_recursive;
  scans = 0;
  best = scan_one(next(portals[aa].p2, portals[aa].p2.d), 0, 0);
  for (i = 0; i < nportals; i++)
    dump(i);
  if (best >= 0)
    printf("best recursive path in %d steps, with %d scans and call depth %d\n",
           best, scans, deepest);
  else
    printf("no recursive path found after %d scans and call depth %d\n",
           scans, deepest);

  return 0;
}
