#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>

static int debug_level = -1;
static void
debug_raw(int level, const char *fmt, ...) {
  va_list ap;
  if (debug_level < 0)
    debug_level = atoi(getenv("DEBUG") ?: "0");
  if (debug_level >= level) {
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
  }
}
#define debug(...) debug_raw(1, __VA_ARGS__)

#define MAXX 25
#define MAXY 6
static char grid[MAXY][MAXX];
static int x = MAXX;
static int y = MAXY;

static void
dump(int level) {
  int i, j;
  if (debug_level < level)
    return;
  putchar('\n');
  for (j = 0; j < y; j++) {
    for (i = 0; i < x; i++)
      putchar(" X-"[grid[j][i] - '0']);
    putchar('\n');
  }
}

int
main(int argc, char **argv) {
  int zero = 0, one = 0, two = 0, min, best, count = 0, page, ch;
  bool done = false;

  debug("");
  memset(grid, '2', sizeof grid);
  if (argc > 2) {
    x = atoi(argv[1]);
    y = atoi(argv[2]);
  }
  else if (argc > 1)
    if (!(stdin = freopen(argv[1], "r", stdin))) {
      perror("failure");
      exit(2);
    }

  min = page = x * y;

  while (!done) {
    ch = getchar();
    switch (ch) {
    case '0':
      zero++;
      break;
    case '1':
      one++;
      break;
    case '2':
      two++;
      break;
    default:
      done = true;
      break;
    }
    if (grid[(count/x)%y][count%x] == '2')
      grid[(count/x)%y][count%x] = ch;
    if (!(count++ % page)) {
      if (zero < min) {
        min = zero;
        best = one * two;
      }
      zero = one = two = 0;
      dump(1);
    }
  }
  printf("read %d bytes, best page value %d\n", count, best);
  dump(0);

  return 0;
}
