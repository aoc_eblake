divert(-1)dnl -*- m4 -*-
# Not intended for standalone usage. Instead, include it from dayXX.m4 as:
# include(`intcode.m4')ifelse(intcode(2), `ok', `',
# `errprint(`Missing IntCode initialization
# ')m4exit(1)')

include(`common.m4')ifdef(`common', `',
`errprint(`Missing common initialization
')m4exit(1)')
include(`math64.m4')

# Common utility functions
define(`oneshot', `pushdef(`$1', `popdef(`$1')$2')')
define(`oops', `errprintn(`unexpected state at pc='pc)m4exit(1)')
define(`run_after_read', `ifelse($1, 0, `run($2)', `oops()')')
define(`pause_on_read', `ifelse($1, 0, `', `oops()')')
define(`pause_after_read', `ifelse($1, 0, `define(`pc', $2)', `oops()')')
define(`run_after_write', `ifelse($1, 1, `run($2)', `oops()')')
define(`pause_after_write', `ifelse($1, 1, `define(`pc', $2)', `oops()')')

# Parse in the input file.
define(`parse', `define(`pos', 0)define(`base', 0)define(`pc',
  0)chew(len(`$1'), `$1')define(`pos', decr(pos))')
define(`_parse', `define(`mem'pos, $1)define(`pos', incr(pos))')
ifdef(`__gnu__', `
  define(`chew', `patsubst(`$2', `\([^;]*\);', `_parse(`\1')')')
', `
  define(`_chew', `_parse(substr(`$1', 0, index(`$1', `;')))define(`tail',
    substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(`tail'), `;'), -1,
    `', `$0(defn(`tail'))')')
  define(`chew', `ifelse(eval($1 < 35), 1, `_$0(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
')

define(`mem', `ifdef(`mem$1', `mem$1`'', 0)')
define(`get0', `mem(mem(eval(pc + $1)))')
define(`get', defn(`get0'))
define(`get1', `mem(eval(pc + $1))')
define(`get2', `mem(eval(base + mem(eval(pc + $1))))')
define(`_put', `define(`mem$1', $2)')
define(`put0', `_put(mem(eval(pc + $1)), $2)')
define(`put', defn(`put0'))
define(`put1', `errprintn(`unexpected mode at 'pc)')
define(`put2', `_put(eval(base + mem(eval(pc + $1))), $2)')

# Either use save/run/restore for rerunning a program, or copy/runs for
# running parallel versions of a program
define(`saved_put', `ifdef(`$1s', `define', `define(`$1s')pushdef(`_restore',
  `popdef(`$1s', `$1', `_restore')')pushdef')($@)')
define(`save', `pushdef(`base', base)pushdef(`pc', pc)pushdef(`_put',
  `saved_put(`mem'$'`1, $'`2)')')
define(`restore', `ifdef(`_$0', `$0(_$0())', `popdef(`base', `pc', `_put')')')
define(`copy', `pushdef(`s', ``mem$1_'')forloop_arg(0, pos,
  `_$0')define(`pc$1_', 0)define(`base$1_', 0)popdef(`s')')
define(`_copy', `define(s`'$1, mem$1)')
define(`_copy_mem', `ifdef(pre`'$1, pre`'$1`', 0)')
define(`_copy_put', `define(pre`'$@)')
oneshot(`copy', `define(`pre', ``mem'')define(`mem', defn(`_copy_mem'))define(
  `_put', defn(`_copy_put'))define(`oops',
  `errprintn(`unexpected state: pc='pc` during 'pre)m4exit(1)')$0($@)')
define(`runs', `pushdef(`pre', ``mem'$1_')pushdef(`s', $1)pushdef(`base',
  base$1_)pushdef(`pc', pc$1_)run(pc)define(`pc$1_', pc)define(`base$1_',
  base)popdef(`pre', `pc', `base', `s')')

define(`io', `run($2)')
define(`done')
define(`op1', `put$3(3, add64(get$1(1), get$2(2)))run(eval(pc + 4))')
define(`op2', `put$3(3, mul64(get$1(1), get$2(2)))run(eval(pc + 4))')
define(`op3', `put$1(1, read)io(0, eval(pc + 2))')
define(`op4', `write(get$1(1))io(1, eval(pc + 2))')
define(`op5', `ifelse(get$1(1), 0, `run(eval(pc + 3))', `run(get$2(2))')')
define(`op6', `ifelse(get$1(1), 0, `run(get$2(2))', `run(eval(pc + 3))')')
define(`op7', `put$3(3, lt64(get$1(1), get$2(2)))run(eval(pc + 4))')
define(`op8', `put$3(3, ifelse(get$1(1), get$2(2), 1, 0))run(eval(pc + 4))')
define(`op9', `define(`base', eval(base + get$1(1)))run(eval(pc + 2))')
define(`op99', `done()')
define(`decode', `ifelse(eval($4$3$2 > 0), 1,
  `define(`op'eval(1000$4$3$2 % 1000)`0$1', `op$1($2, $3, $4)')')')
define(`unary', `decode($1, 1)decode($1, 2)')
unary(3)
unary(4)
unary(9)
define(`binary', `forloop_var(`a', 0, 2, `forloop_var(`b', 0, 2,
  `decode($1, a, b)')')')
binary(5)
binary(6)
define(`ternary', `forloop_var(`a', 0, 2, `forloop_var(`b', 0, 2,
  `decode($1, a, b)decode($1, a, b, 2)')')')
ternary(1)
ternary(2)
ternary(7)
ternary(8)
define(`op', `ifdef(`op$1', `op$1', `opX')`'')
define(`opX', `errprintn(`unexpected opcode at 'pc)')

define(`_run', `ifelse($1, `', `', `define(`pc', $1)')')
define(`run', `_$0($1)op(mem(pc))')
define(`step', `_run($1)oneshot(`run', defn(`_run'))op(mem(pc))')

define(`intcode', `common($1)define(`input',
  translit((include(defn(`file'))), nl`,()', `;;'))')
