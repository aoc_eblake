divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day24.input] day24.m4
# Optionally use -Dlimit=NN for fewer generations in part 2
# Optionally use -Donly=[12] to skip a part

include(`common.m4')ifelse(common(24), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`input', translit(include(defn(`file'))
, `.#
', 01))
ifdef(`limit', `ifelse(eval(limit & 1), 1, `errprint(`Limit must be even
')m4exit(1)')', `define(`limit', 200)')

define(`mapping', `_$0($1, substr(alpha, $1, 1))')
define(`_mapping', `define(`map$1', `$2')define(`map$2', $1)ifelse(substr(
  input, $1, 1), 1, `define(`init', defn(`init')`$2')')')
forloop_arg(0, 24, `mapping')

define(`bit', `ifelse(index(`$2', defn(`map$1')), -1, 0, 1)')
define(`near', `ifelse(eval($1 > 4), 1, `define(`n$1', defn(`n$1')defn(
  `map'eval($1 - 5)))')ifelse(eval($1 % 5), 4, `', `define(`n$1',
  defn(`n$1')defn(`map'incr($1)))')ifelse(eval($1 < 20), 1, `define(`n$1',
  defn(`n$1')defn(`map'eval($1 + 5)))')ifelse(eval($1 % 5), 0, `',
  `define(`n$1', defn(`n$1')defn(`map'decr($1)))')')
forloop_arg(0, 24, `near')
define(`value', `eval(0forloop(0, 24, `_$0(', `, `$1')'))')
define(`_value', ` | (bit($1, `$2') << $1)')
define(`show', `forloop_var(`y_', 0, 4, `forloop_var(`x_', 0, 4,
  `eval(bit(eval(x_ + 5 * y_), `$1'))')
')')

define(`next', `ifelse($1, 01, `defn(`map$2')', $1, 011, `defn(`map$2')',
  $1, 11, `defn(`map$2')')')
define(`build', `next(bit($1, `$2')translit(translit(```$2$3''', defn(`n$1'),
  11111111), 'dquote(defn(`alpha')defn(`ALPHA')`_')`), $1)')
define(`hit')
define(`gen', `ifdef(`gen_$1$2$3', `hit()', `_$0(`$1',
  `$2$3')')defn(`gen_$1$2$3')')
define(`_gen_', ``build($1, $$2)'')
forloop(0, 24, `define(`_gen', defn(`_gen')_gen_(', `, @))')
define(`_gen', `define(`gen_$1$2', 'defn(`_gen')`)')
define(`loop', `ifdef(`gen_$1_', `define(`part1', value(`$1'))',
  `loop(gen(`$1', `_'))')')

ifelse(ifdef(`only', only, 1), 1, `
loop(defn(`init', `_'))
')

ifelse(ifdef(`only', only, 2), 2, `
define(`n12')
define(`n17', `swqUVWXY')
define(`n11', `gqkAFKPU')
define(`n7', `cigABCDE')
define(`n13', `iosEJOTY')
define(`edge', `define(`n'$1, defn(`n$1')`H')define(`n'eval($1 * 5 + 4),
  defn(`n'eval($1 * 5 + 4))`N')define(`n'eval($1 + 20), defn(`n'eval($1 +
  20))`R')define(`n'eval($1 * 5), defn(`n'eval($1 * 5))`L')')
forloop_arg(0, 4, `edge')

define(`g', `defn(`g$1_'eval($2 + 'limit` + 1))')
define(`setg', `define(`g$1_'eval($2 + 'limit` + 1), `$3')')
setg(0, 0, defn(`init'))
define(`multigen', `forloop(-$1, $1, `_$0(0, 1, ', `)')forloop(-$1, $1,
  `_$0(1, 0, ', `)')')
define(`_multigen', `setg($2, $3, gen(g($1, $3), translit(dquote(g($1,
  decr($3))), `hnrlabcdefgijkmopqstuvwxy', `HNRL'), translit(dquote(g($1,
  incr($3))), `abcdejotyxwvupkfghilmnqrs', `ABCDEJOTYXWVUPKF')))')
forloop_arg(1, eval(limit / 2), `multigen')

define(`count', `define(`part2', eval(part2 + _$0(-$1) + _$0($1)))')
define(`_count', `len(g(0, $1))')
define(`part2', _count(0))
forloop_arg(1, eval(limit / 2), `count')

')divert`'part1
part2
