divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day19.input] day19.m4

include(`intcode.m4')ifelse(intcode(19), `ok', `',
`errprint(`Missing IntCode initialization
')m4exit(1)')

parse(input)
define(`deployed', `0')
define(`killed', `0')
define(`count', `define(`deployed', incr(deployed))ifelse($1, 1,
  `define(`killed', incr(killed))')')
define(`try', `
  save()
  oneshot(`read', $2)
  oneshot(`read', $1)
  run(0)
  restore()')
# 0,0 is always on, but the rest of the beam might be discontinuous
define(`part1', 1)
# Sweep diagonally to find tip
define(`write', `count($1)define(`data', $1)')
define(`data', 0)
define(`x', 0)
define(`y', 0)
define(`loop', `ifelse(data, 1, `define(`tipx', x)define(`tipy', y)',
  `ifelse(x, 0, `define(`x', incr(y))define(`y', 0)',
    `define(`x', decr(x))define(`y', incr(y))')try(x, y)loop()')')
loop()
# Sweep left edge from tip
define(`x', tipx)
define(`y', tipy)
define(`write', `count($1)ifelse($1, 1, `pushdef(`l', x)define(`y', incr(y))',
  `define(`x', incr(x))')')
define(`loop', `ifelse(y, 50, `', `ifelse(x, 50,
  `pushdef(`l', x)define(`y', incr(y))', `try(x, y)')loop()')')
loop()
# Sweep right edge from tip
define(`x', tipx)
define(`y', tipy)
define(`write', `count($1)ifelse($1, 0, `pushdef(`r', x)define(`y', incr(y))',
  `define(`x', incr(x))')')
define(`loop', `ifelse(y, 50, `', `ifelse(x, 50,
  `pushdef(`r', x)define(`y', incr(y))', `try(x, y)')loop()')')
loop()
define(`coursex', eval(l - tipx))
define(`coursey', eval(y - tipy))
forloop_arg(tipy, 49, `define(`part1',
  eval(part1 + r - l))popdef(`l', `r')done')
define(`part1d', deployed)
define(`part1k', killed)

# Sweep left edge, and check diagonal of square
define(`x', eval(tipx + 99 / coursex))
define(`y', eval(tipy + 99))
define(`write', `count($1)define(`data', $1)')
define(`loop', `try(eval(x + coursex), eval(y + coursey))ifelse(data, 1,
  `try(eval(x + coursex + 99), eval(y + coursey - 99))ifelse(data, 1,
  `oneshot(`loop')', `define(`y', eval(y + coursey))define(`x',
  eval(x + coursex))')', `define(`x', incr(x))')loop()')
loop()
define(`loop', `try(x, y)ifelse(data, 1, `try(eval(x + 99),
  eval(y - 99))ifelse(data, 1, `oneshot(`loop')', `define(`y', incr(y))')',
  `define(`x', incr(x))')loop()')
loop()
define(`part2', eval((x * 10000) + y - 99))

divert`'part1`'ifelse(verbose, 0, `', ` (part1k / part1d)')
part2`'ifelse(verbose, 0, `', ` (killed / deployed)')
