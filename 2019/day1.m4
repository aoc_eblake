divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day1.input] day1.m4

include(`common.m4')ifelse(common(1), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`part1', 0)
define(`part2', 0)
define(`compute', `_$0($1)ifelse($2, `', `', `$0(shift($@))')')
define(`_compute',
  `define(`part1', eval(part1 + $1 / 3 - 2))do(eval($1 / 3 - 2))')
define(`do', `ifelse(eval($1 > 8), 1, `do(eval($1 / 3 - 2))')define(`part2',
  eval(part2 + $1))')
compute(translit(include(defn(`file')), `
', `,'))

divert`'part1
part2
