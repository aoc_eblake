#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <limits.h>

#define LIMIT 20000
static int a[LIMIT*2][LIMIT*2];

static int do_debug = -1;
void debug(const char *fmt, ...) {
  va_list ap;
  if (do_debug < 0)
    do_debug = !!getenv("DEBUG");
  if (do_debug > 0) {
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
  }
}

enum dir { U, R, D, L };

int main(int argc, char **argv) {
  int i, d, count = 0, line = 0, len = 0;
  int x = LIMIT, y = LIMIT;
  int best1 = LIMIT * 2, best2 = INT_MAX;

  if (argc > 1)
    if (!(stdin = freopen(argv[1], "r", stdin))) {
      perror("failure");
      exit(2);
    }

  while (line < 2) {
    count++;
    switch (getchar()) {
    case 'U': d = U; break;
    case 'R': d = R; break;
    case 'D': d = D; break;
    case 'L': d = L; break;
    default:
      printf("unexpected input at %d\n", count);
      exit(1);
    }
    if (scanf("%d", &i) != 1) {
      printf("unexpected input at %d\n", count);
      exit(1);
    }
    while (i--) {
      len++;
      switch (d) {
      case U: x++; break;
      case R: y++; break;
      case D: x--; break;
      case L: y--; break;
      }
      if (x < 0 || x >= LIMIT*2 || y < 0 || y >= LIMIT*2 || len > INT_MAX) {
	printf("recompile with larger LIMIT\n");
	exit(2);
      }
      if (line) {
	if (a[x][y]) {
	  debug("intersect at %d,%d time %d+%d=%d\n", x-LIMIT, y-LIMIT,
		a[x][y], len, a[x][y] + len);
	  if (abs(x-LIMIT) + abs(y-LIMIT) < best1)
	    best1 = abs(x-LIMIT) + abs(y-LIMIT);
	  if (a[x][y] + len < best2)
	    best2 = a[x][y] + len;
	}
      } else if (!a[x][y]) {
	a[x][y] = len;
      }
    }
    switch (getchar()) {
    case ',': break;
    case '\n':
      debug("parsed a line len=%d\n", len);
      ++line;
      len = 0;
      x = y = LIMIT;
      break;
    default:
      printf("unexpected input at %d\n", count);
      exit(1);
    }
  }
  printf("after %d segments, best distance %d, best time %d\n", count,
	 best1, best2);
  return 0;
}
