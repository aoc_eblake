divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day22.input] day22.m4
# Optionally use -Donly=[12] to skip a part

include(`common.m4')ifelse(common(22), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

include(`math64.m4')

define(`input', quote(translit(include(defn(`file')), `
', `,')))

# https://en.wikipedia.org/wiki/Montgomery_modular_multiplication
define(`splitinto', `pushdef(`i', 0)_foreach(`_$0(`$1',', `)', `', chunk($2,
  decr(len(B))))ifelse(eval(i <= 2 * r - 1), 1, `forloop(i, eval(2 * r - 1),
  `_$0(`$1', 0,', `)')')popdef(`i')')
define(`_splitinto', `define(`$1'i, `$2')define(`i', incr(i))')
define(`combine', `rebuild(decr(len(B))forloop(eval($2 + 0),
  eval(2 * r - 1), `, defn(`$1'', `)'))')
define(`divide', `_$0(eval((len($1) >= len(B)) * (len($1) - len(B) + 1)), $@,
  `$2')')
define(`_divide', `define(`$4', ifelse($1, 0, 0, `substr($2, 0,
  $1)'))define(`$3', ifelse($1, 0, $2, `trim(substr($2, $1))'))')
define(`redc', `splitinto(`T', $1)forloop_var(`i', 0, decr(r), `define(`c',
  0)divide(eval(defn(`T'i) * Np), `m')forloop_var(`j', 0, eval(2 * r - 1),
  `_$0()')')normalize(combine(`T', r), N)')
define(`_redc', `define(`x', eval(defn(`T'eval(i + j)) + m * defn(`N'j) +
  c))divide(x, `T'eval(i + j), `c')')
define(`normalize', `ifelse(lt64($1, $2), 1, $1, `sub64($1, $2)')')
define(`mont', `redc(mul64($1, R2_N))')

# Convert each line into f(x)=ax+b linear function with non-negative a and b.
# Store a and b in Montgomery form to avoid needing 64-bit divide for part 2.
define(`parse', `ifelse($1, `', `', `_$0(translit($1, ` ', `,'))')')
define(`_parse', `pushdef(`act', ifelse($1, cut, ``1, sub64(N, $2)'',
  $3, new, ``N_, N_'', ``$4, 0''))')
foreach(`parse', input)

define(`setup', `define(`B', $1)define(`r', $2)define(`N', $3)define(`Np',
  $4)define(`N_', sub64(N, 1))define(`R2_N', $5)splitinto(`N',
  N)define(`R_N', redc(R2_N))define(`a', R_N)define(`b', 0)stack_foreach(`act',
  `docompose')')
define(`apply', `redc(add64(redc(mul64($2, mont($1))), $3))')
define(`docompose', `compose($1)')
define(`compose', `_$0(a, b, mont($1), mont($2), `a', `b')')
define(`_compose', `define(`$5', redc(mul64($3, $1)))define(`$6',
  normalize(add64(redc(mul64($3, $2)), $4), N))')
define(`bits', `ifelse($1, 0, 0, $1, 1, 1, `_$0(mul64($1, 5)), eval(substr($1,
  decr(len($1))) & 1)')')
define(`_bits', `bits(substr($1, 0, decr(len($1))))')
define(`compose_n', `define(`an', a)define(`bn', b)foreach(`_$0',
  shift(bits($1)))')
define(`_compose_n', `_compose(an, bn, an, bn, `an', `bn')ifelse($1, 1,
  `_compose(an, bn, a, b, `an', `bn')')')

# precomputed: for R=1000000==B^r and N=10007, Nprime=57 and R^2%N=9664
ifelse(ifdef(`only', only, 1), 1, `
setup(100, 3, 10007, 57, 9664)
define(`part1', apply(2019, a, b))
')

# precomputed: for R=10000000000000000==B^r and N=119315717514047,
# Nprime=9617 and R^2%N=76941077250887
ifelse(ifdef(`only', only, 2), 2, `
setup(10000, 4, 119315717514047, 9617, 76941077250887)
compose_n(sub64(N_, 101741582076661))
define(`part2', apply(2020, an, bn))
')

divert`'part1
part2
