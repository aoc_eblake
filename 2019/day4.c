#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <limits.h>

static int do_debug = -1;
void debug(const char *fmt, ...) {
  va_list ap;
  if (do_debug < 0)
    do_debug = !!getenv("DEBUG");
  if (do_debug > 0) {
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
  }
}

void check(int i, int *a, int *b) {
  char buf[8];
  char count[10] = "";
  int j;
  bool pair = false;

  if (snprintf(buf, sizeof buf, "0%d", i) != 7) {
    printf("invalid input %d\n", i);
    exit(1);
  }
  for (j = 1; j < 7; j++) {
    if (buf[j] < buf[j-1])
      return;
    count[buf[j] - '0']++;
  }
  for (j = 0; j < 10; j++) {
    if (count[j] > 1) {
      pair = true;
      if (count[j] == 2) {
	++*b;
	break;
      }
    }
  }
  if (pair)
    ++*a;
}

int main(int argc, char **argv) {
  int low = 158126, high = 624574;
  int i, count1 = 0, count2 = 0, possible = 0;

  if (argc > 1)
    high = low = atoi(argv[1]);
  if (argc > 2)
    high = atoi(argv[2]);

  for (i = low; i <= high; i++, possible++)
    check(i, &count1, &count2);
  printf("%d / %d possibilities over %d in range\n", count1, count2, possible);
  return 0;
}
