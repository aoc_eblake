divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day7.input] day7.m4

include(`intcode.m4')ifelse(intcode(7), `ok', `',
`errprint(`Missing IntCode initialization
')m4exit(1)')

parse(input)
no64()

# Feed id to each machine, stop after input
define(`prep', `pushdef(`io', defn(`pause_after_read'))
  oneshot(`read', $1)copy(`a')runs(`a')
  oneshot(`read', $2)copy(`b')runs(`b')
  oneshot(`read', $3)copy(`c')runs(`c')
  oneshot(`read', $4)copy(`d')runs(`d')
  oneshot(`read', $5)copy(`e')runs(`e')
  define(`data', 0)popdef(`io')')

# Loop until E calls done, expect read then write, then pause before any read
# (expectations are a stack, create them in reverse order)
define(`write', `define(`data', $1)')
define(`read', `data')
define(`done', `define(`repeat')')
define(`loop', `
  oneshot(`io', defn(`pause_after_write'))
  oneshot(`io', defn(`run_after_read'))
  runs(`a')
  oneshot(`io', defn(`pause_after_write'))
  oneshot(`io', defn(`run_after_read'))
  runs(`b')
  oneshot(`io', defn(`pause_after_write'))
  oneshot(`io', defn(`run_after_read'))
  runs(`c')
  oneshot(`io', defn(`pause_after_write'))
  oneshot(`io', defn(`run_after_read'))
  runs(`d')
  oneshot(`io', defn(`pause_on_read'))
  oneshot(`io', defn(`run_after_write'))
  oneshot(`io', defn(`run_after_read'))
  runs(`e')
  repeat()')

define(`try', `prep(substr($1, 0, 1), substr($1, 1, 1), substr($1, 2, 1),
  substr($1, 3, 1), substr($1, 4, 1))define(`repeat', `loop()')loop()
  ifelse(eval(data > best), 1, `define(`best', data)')')

define(`swap', `define(`str',
  substr(str, 0, $1)substr(str, $2, 1)substr(str, incr($1),
  eval($2 - $1 - 1))substr(str, $1, 1)substr(str, incr($2)))str')
define(`permute', `pushdef(`str', $1)_$0($1, len($1))popdef(`str')')
define(`_permute', `ifelse($2, 1, `try($1)',
  `$0($1, decr($2))forloop_var(`x', 0, decr(decr($2)),
  `$0(swap(ifelse(eval($2 & 1), 1, 0, x), decr($2)), decr($2))')')')

define(`best', 0)
permute(01234)
define(`part1', best)
define(`best', 0)
permute(56789)
define(`part2', best)
divert`'part1
part2
