divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day14.input] day14.m4

include(`common.m4')ifelse(common(14), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

include(`math64.m4')

define(`goal', 1000000000000)
define(`input', translit((include(defn(`file'))), nl` >,()', `;.'))
define(`line', `_$0(translit(`$1', `=', `,'))')
define(`_line', `define(`l'target(translit(`$2', `.', `,')),
  build(translit(`$1', `.', `,')))')
define(`build', `ifelse(`$2', `', `', ``produce(`$2', $'`1, $1)'$0(
  shift(shift($@)))')')
define(`target', `define(`n$3', $2)`$3'')
ifdef(`__gnu__', `
  patsubst(defn(`input'), `\([^;]*\);', `line(`\1')')
', `
  define(`_chew', `line(substr(`$1', 0, index(`$1', `;')))define(`tail',
    substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(`tail'), `;'), -1,
    `', `$0(defn(`tail'))')')
  define(`chew', `ifelse(eval(`$1 < 175'), 1, `_$0(`$2')', `$0(eval(`$1/2'),
    substr(`$2', 0, eval(`$1/2')))$0(eval(len(defn(`tail'))` + $1 - $1/2'),
    defn(`tail')substr(`$2', eval(`$1/2')))')')
  chew(len(defn(`input')), defn(`input'))
')

# Use produce to create list as topological sort
define(`produce', `ifdef(`s$1', `', `l$1(0)define(`s$1')pushdef(`t', `$1')')')
define(`sORE')define(`l', `FUEL')
lFUEL(0)stack_reverse(`t', `l')

# Now use produce to accumulate
define(`produce', `define(`a$1', add64(a$1, mul64($2, $3)))')
define(`bits', `_$0(eval($1, 2))')
define(`_bits', ifdef(`__gnu__', ``shift(patsubst($1, ., `, \&'))'',
  ``ifelse(len($1), 1, `$1', `substr($1, 0, 1),$0(substr($1, 1))')''))
define(`bits64', `ifelse(eval(len($1) < 10), 1, `bits($1)', `_$0(mul64($1,
  5)), eval(substr($1, decr(len($1))) & 1)')')
define(`_bits64', `bits64(substr($1, 0, decr(len($1))))')
define(`div64', `ifelse($4, `', `$1,$2', `$0(_$0(add64($1, $1), add64(add64($2,
  $2), $4), $3), $3, shift(shift(shift(shift($@)))))')')
define(`_div64', `ifelse(lt64($2, $3), 0, `add64($1, 1), sub64($2, $3)',
  `$1, $2')')
define(`divup', `ifelse(eval(len($1) < 10), 1, `eval(($1+$2-1)/$2)',
  `_$0(div64(0, 0, $2, bits64($1)))')')
define(`_divup', `ifelse($2, 0, $1, `add64($1, 1)')')
define(`_use', `l$1(divup(a$1, n$1))')
define(`use', `define(`aORE', 0)stack_reverse(`l', `t', `define(`a'defn(`l'),
  0)')define(`aFUEL', $1)stack_reverse(`t', `l', `_$0(defn(`l'))')aORE')
define(`part1', use(1))

define(`cache', `ifdef(`u$1', `', `define(`u$1', use($1))')u$1')
define(`try', `_$0($1, cache($1), cache(add64($1, 1)), 'goal`)')
define(`_try', `ifelse(lt64($2, $4), 1, `ifelse(lt64($4, $3), 1, $1,
  `try(add64($1, divup(sub64($4, $3), divup($3, $1))))')',
  `try(sub64($1, divup(sub64($2, $4), divup($2, $1))))')')
define(`part2', try(divup(goal, part1)))

')divert`'part1
part2
