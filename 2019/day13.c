#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>

#define LIMIT 10000
static int64_t orig[LIMIT];
static int len;
struct state {
  int id;
  int64_t a[LIMIT];
  int pc;
  int base;
  bool has_in;
  int64_t in;
  bool has_out;
  int64_t out;
  int steps;
};

static int debug_level = -1;
static void
debug_raw(int level, const char *fmt, ...) {
  va_list ap;
  if (debug_level < 0)
    debug_level = atoi(getenv("DEBUG") ?: "0");
  if (debug_level >= level) {
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
  }
}
#define debug(...) debug_raw(1, __VA_ARGS__)

static void
dump(struct state *s) {
  if (debug_level < 2)
    return;
  debug(" state of id=%d: pc=%d base=%d in=%s%" PRId64 " out=%s%" PRId64
        " steps=%d\n", s->id, s->pc, s->base,
        s->has_in ? "" : "!", s->in, s->has_out ? "" : "!", s->out, s->steps);
  for (int i = 0; i < len; i++)
    debug_raw(3, "%" PRId64 ",", s->a[i]);
  debug_raw(3, "\n");
}

static void __attribute__((noreturn))
crash(struct state *s, const char *msg) {
  printf("invalid program id=%d, pc=%d: %s\n", s->id, s->pc, msg);
  exit(1);
}

static int64_t
get(struct state *s, int param) {
  int64_t op = s->a[s->pc];
  int scale = 10;
  int mode;
  int64_t value;

  if (op > 99999 || op < 0)
    crash(s, "unexpected opcode");
  if (s->pc + param > LIMIT)
    crash(s, "memory too short for opcode");
  value = s->a[s->pc + param];
  while (param--)
    scale *= 10;
  mode = (op / scale) % 10;
  debug_raw(3, "get op=%d mode=%d value=%d\n", op, mode, value);
  switch (mode) {
  case 0:
    if (value > LIMIT || value < 0)
      crash(s, "in position mode, param beyond memory");
    return s->a[value];
  case 1:
    return value;
  case 2:
    value += s->base;
    if (value > LIMIT || value < 0)
      crash(s, "in relative mode, param beyond memory");
    return s->a[value];
  default:
    crash(s, "unexpected mode");
  }
}

static void
put(struct state *s, int param, int64_t value) {
  int64_t op = s->a[s->pc];
  int scale = 10;
  int mode;
  int64_t offset;

  if (op > 99999 || op < 0)
    crash(s, "unexpected opcode");
  if (s->pc + param > LIMIT)
    crash(s, "memory too short for opcode");
  offset = s->a[s->pc + param];
  while (param--)
    scale *= 10;
  mode = (op / scale) % 10;
  debug_raw(3, "put op=%d mode=%d value=%d offset=%d\n", op, mode, value, offset);
  switch (mode) {
  case 0:
    if (offset > LIMIT || offset < 0)
      crash(s, "in position mode, param beyond memory");
    s->a[offset] = value;
    return;
  case 2:
    offset += s->base;
    if (offset > LIMIT || offset < 0)
      crash(s, "in relative mode, param beyond memory");
    s->a[offset] = value;
    return;
  default:
    crash(s, "unexpected mode");
  }
}

static void
init(struct state *s, int64_t in) {
  memset(s, 0, sizeof  *s);
  memcpy(s->a, orig, len * sizeof orig[0]);
  s->has_in = true;
  s->in = in;
  dump(s);
}

/* Returns -1 for stalled on input, 0 for done, 1 for stalled on output */
static int
run(struct state *s) {
  int jump;

  while (1) {
    debug_raw(2, "executing id=%d step=%d pc=%d base=%" PRId64 " %" PRId64
              ",%" PRId64 ",%" PRId64 ",%" PRId64 "\n", s->id,
              s->steps++, s->pc, s->base, s->a[s->pc], s->a[s->pc+1],
              s->a[s->pc+2], s->a[s->pc+3]);
    if (!(s->steps % 10000))
      debug(" steps=%d\n", s->steps);
    if (s->pc > LIMIT || s->pc < 0)
      crash(s, "program ran out of bounds");
    switch (s->a[s->pc] % 100) {
    case 1:
      put(s, 3, get(s, 1) + get(s, 2));
      jump = 4;
      break;
    case 2:
      put(s, 3, get(s, 1) * get(s, 2));
      jump = 4;
      break;
    case 3:
      if (!s->has_in) {
        debug_raw(2, "id=%d stalling for input\n", s->id);
        s->steps--;
        return -1;
      }
      put(s, 1, s->in);
      s->has_in = false;
      jump = 2;
      break;
    case 4:
      if (s->has_out) {
        debug_raw(2, "id=%d stalling for output\n", s->id);
        s->steps--;
        return 1;
      }
      s->has_out = true;
      s->out = get(s, 1);
      jump = 2;
      break;
    case 5:
      if (get(s, 1)) {
        s->pc = get(s, 2);
        jump = 0;
      } else
        jump = 3;
      break;
    case 6:
      if (!get(s, 1)) {
        s->pc = get(s, 2);
        jump = 0;
      } else
        jump = 3;
      break;
    case 7:
      put(s, 3, get(s, 1) < get(s, 2));
      jump = 4;
      break;
    case 8:
      put(s, 3, get(s, 1) == get(s, 2));
      jump = 4;
      break;
    case 9:
      s->base += get(s, 1);
      if (s->base < 0 || s->base > LIMIT)
        crash(s, "relative base out of bounds");
      jump = 2;
      break;
    case 99:
      debug_raw(2, "id=%d halting\n", s->id);
      return 0;
    default:
      crash(s, "unexpected opcode");
    }
    s->pc += jump;
    dump(s);
  }
}

#define GRID 50
enum tile {
  EMPTY,
  WALL,
  BLOCK,
  PADDLE,
  BALL,
};
static enum tile grid[GRID][GRID];
static int maxx, maxy;
static int score;
static int padx, ballx;

static struct state s;

static void
display(void) {
  int x, y;

  printf("score %d\n", score);
  for (y = 0; y <= maxy; y++) {
    for (x = 0; x <= maxx; x++)
      putchar(" #x_@"[grid[y][x]]);
    putchar('\n');
  }
}

int
main(int argc, char **argv) {
  int count = 0, blocks = 0;
  int begin = 1;
  int x, y;
  bool done;

  debug("");
  if (argc > 1)
    if (!(stdin = freopen(argv[1], "r", stdin))) {
      perror("failure");
      exit(2);
    }

  if (argc > 2)
    begin = atoi(argv[2]);

  while (scanf("%" SCNd64 "%*[,\n]", &orig[len]) == 1)
    if (len++ > LIMIT - 3) {
      printf("recompile with larger LIMIT\n");
      exit(2);
    }
  printf("Read %u slots\n", len);

  init(&s, 0);
  s.a[0] = begin;
  s.has_in = false;
  while (!done) {
    count++;
    switch (run(&s)) {
    case 0:
      crash(&s, "unexpected completion");
    case -1:
      s.in = (ballx > padx) - (ballx < padx);
      s.has_in = true;
      printf("tilting joystick %" PRId64 "\n", s.in);
      display();
      continue;
    }
    if (s.out < -1 || s.out > GRID)
      crash(&s, "recompile with larger GRID");
    x = s.out;
    s.has_out = false;
    if (x > maxx)
      maxx = x;

    if (run(&s) != 1)
      crash(&s, "unexpected completion/input request");
    if (!s.has_out)
      crash(&s, "missing expected output y");
    if (s.out < 0 || s.out > GRID)
      crash(&s, "recompile with larger GRID");
    y = s.out;
    if (x == -1 && y)
      crash(&s, "invalid y for displaying score");
    s.has_out = false;
    if (y > maxy)
      maxy = y;

    if (run(&s) == 0)
      done = true;
    if (!s.has_out)
      crash(&s, "missing expected output tile");
    if (x >= 0) {
      if (s.out < EMPTY || s.out > BALL)
        crash(&s, "unexpected output tile");
      if (s.out == BLOCK && grid[y][x] != BLOCK)
        blocks++;
      else if (s.out != BLOCK && grid[y][x] == BLOCK) {
        debug("hit a block\n");
        display();
      }
      grid[y][x] = s.out;
      if (s.out == BALL)
        ballx = x;
      else if (s.out == PADDLE)
        padx = x;
      debug("setting %d,%d to '%c'\n", x, y, " #X_@"[s.out]);
    } else {
      score = s.out;
      debug("setting score to %d\n", score);
      display();
    }
    s.has_out = false;
  }
  display();
  printf("program done: %d tuples, %d.%d grid, %d blocks, final score %d\n",
         count, maxx, maxy, blocks, score);
  return 0;
}
