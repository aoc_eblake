#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <math.h>

#define LIMIT 2000
static int a[LIMIT];
static int len;
static int pc;

static int do_debug = -1;
static void
debug(const char *fmt, ...) {
  va_list ap;
  if (do_debug < 0)
    do_debug = !!getenv("DEBUG");
  if (do_debug > 0) {
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
  }
}

static void
dump(void) {
  if (!do_debug)
    return;
  for (int i = 0; i < len; i++)
    debug("%d,", a[i]);
  debug("\n");
}

static void __attribute__((noreturn))
crash(const char *msg) {
  printf("invalid program, pc=%d: %s\n", pc, msg);
  exit(1);
}

static int
get(int param) {
  int op = a[pc];
  int scale = pow(10, param + 1);
  int mode = (op / scale) % 10;
  int value;

  debug("get mode=%d\n", mode);
  if (pc + param > len)
    crash("program too short for opcode");
  value = a[pc + param];
  switch (mode) {
  case 0:
    if (value > len || value < 0)
      crash("in position mode, param beyond end of program");
    return a[value];
  case 1:
    return value;
  default:
    crash("unexpected mode");
  }
}

static void
put(int param, int value) {
  int op = a[pc];
  int scale = pow(10, param + 1);
  int mode = (op / scale) % 10;
  int offset;

  debug("put mode=%d\n", mode);
  if (pc + param > len)
    crash("program too short for opcode");
  offset = a[pc + param];
  switch (mode) {
  case 0:
    if (offset > len)
      crash("in position mode, param beyond end of program");
    a[offset] = value;
    return;
  default:
    crash("unexpected mode");
  }
}

int
main(int argc, char **argv) {
  int i, count = 0, jump;
  bool done = false;
  int reads = 0, input = 1;

  if (argc > 1)
    if (!(stdin = freopen(argv[1], "r", stdin))) {
      perror("failure");
      exit(2);
    }

  while (scanf("%d%*[,\n]", &i) == 1) {
    a[len++] = i;
    if (len > LIMIT - 3) {
      printf("recompile with larger LIMIT\n");
      exit(2);
    }
  }
  printf("Read %u slots\n", len);
  dump();

  if (argc > 2)
    input = atoi(argv[2]);

  while (!done) {
    count++;
    debug("executing %d,%d,%d,%d\n", a[pc], a[pc+1], a[pc+2], a[pc+3]);
    if (pc > len || pc < 0)
      crash("program ran out of bounds");
    switch (a[pc] % 100) {
    case 1:
      put(3, get(1) + get(2));
      jump = 4;
      break;
    case 2:
      put(3, get(1) * get(2));
      jump = 4;
      break;
    case 3:
      if (++reads != 1)
	crash("too many reads");
      put(1, input);
      jump = 2;
      break;
    case 4:
      printf("%d\n", get(1));
      jump = 2;
      break;
    case 5:
      if (get(1)) {
	pc = get(2);
	jump = 0;
      } else
	jump = 3;
      break;
    case 6:
      if (!get(1)) {
	pc = get(2);
	jump = 0;
      } else
	jump = 3;
      break;
    case 7:
      put(3, get(1) < get(2));
      jump = 4;
      break;
    case 8:
      put(3, get(1) == get(2));
      jump = 4;
      break;
    case 99:
      debug("halting\n");
      done = true;
      jump = 1;
      break;
    default:
      crash("unexpected opcode");
    }
    pc += jump;
    dump();
  }
  printf("after %d opcodes, slot 0 holds %d\n", count, a[0]);
  dump();
  return 0;
}
