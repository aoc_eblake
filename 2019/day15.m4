divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day15.input] day15.m4

include(`intcode.m4')ifelse(intcode(15), `ok', `',
`errprint(`Missing IntCode initialization
')m4exit(1)')

parse(input)
define(`try', `try_$1()')
define(`try_n', 1)
define(`try_s', 2)
define(`try_w', 3)
define(`try_e', 4)
define(`right', `define(`dir', right_$1())')
define(`right_n', `e')
define(`right_e', `s')
define(`right_s', `w')
define(`right_w', `n')
define(`left', `define(`dir', left_$1())')
define(`left_n', `w')
define(`left_w', `s')
define(`left_s', `e')
define(`left_e', `n')
define(`move', `_move($1, incr(g($1, x, y))move_$2(), x, y)')
define(`_move', `ifdef(`g$1_$3_$4', `', `define(`g$1_$3_$4',
  $2)ifelse(eval($2 > part$1), 1, `define(`part$1', $2)')')')
define(`move_n', `define(`y', decr(y))')
define(`move_e', `define(`x', incr(x))')
define(`move_s', `define(`y', incr(y))')
define(`move_w', `define(`x', decr(x))')
define(`g', `ifdef(`g$1_$2_$3', `g$1_$2_$3')')

define(`read', `try(dir)')
define(`write', `ifelse($1, 2, `oneshot(`loop')')define(`data', $1)')
define(`loop', `
  oneshot(`io', defn(`pause_after_write'))
  oneshot(`io', defn(`run_after_read'))
  run()
  ifelse(data, 0, `right(dir)',
  `move($1, dir)ifelse(eval(x & y & 1), 1, `left(dir)')')
  loop($1)')

define(`x', 25)
define(`y', 25)
define(`dir', `n')
define(`g1_'x`_'y, 0)
define(`part1', 0)
loop(1)
define(`part1', g(1, x, y))

define(`g2_'x`_'y, 0)
define(`part2', 0)
loop(2)

divert`'part1
part2
