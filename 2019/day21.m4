divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day21.input] day21.m4
# Optionally use -Dverbose=[12] to see some progress

include(`intcode.m4')ifelse(intcode(21), `ok', `',
`errprint(`Missing IntCode initialization
')m4exit(1)')

ifelse(verbose, 0, `', `ifdef(`__gnu__', `',
  `errprintn(`verbose mode requires GNU m4')m4exit(1)')')

parse(input)
define(`write', `ifelse(verbose, 0, `', eval($1 < 128 && $1 != 96), 1,
  `_$0(format(``%c'',$1))')define(`data', $1)')
define(`_write', `divert`$1'divert(-1)')

define(`prepread', `ifelse($#, 1, `_$0(10)',
  `$0(shift($@))_$0(32)')ifdef(`$0$1', `$0$1()', `oops()')')
define(`_prepread', `oneshot(`read', `$1')')
define(`prepreadAND', `_prepread(68)_prepread(78)_prepread(65)')
define(`prepreadOR', `_prepread(82)_prepread(79)')
define(`prepreadNOT', `_prepread(84)_prepread(79)_prepread(78)')
define(`prepreadRUN', `_prepread(78)_prepread(85)_prepread(82)')
define(`prepreadWALK', `_prepread(75)_prepread(76)_prepread(65)_prepread(87)')
define(`prepreadA', `_prepread(65)')
define(`prepreadB', `_prepread(66)')
define(`prepreadC', `_prepread(67)')
define(`prepreadD', `_prepread(68)')
define(`prepreadE', `_prepread(69)')
define(`prepreadF', `_prepread(70)')
define(`prepreadG', `_prepread(71)')
define(`prepreadH', `_prepread(72)')
define(`prepreadI', `_prepread(73)')
define(`prepreadJ', `_prepread(74)')
define(`prepreadT', `_prepread(84)')
define(`loop', `ifdef(`script', `prepread(script)popdef(`script')loop()')')

pushdef(`script', `NOT, C, J')
pushdef(`script', `AND, D, J')
pushdef(`script', `NOT, A, T')
pushdef(`script', `OR, T, J')
pushdef(`script', `WALK')
loop()

save()
run(0)
define(`part1', data)
restore()

pushdef(`script', `NOT, H, J')
pushdef(`script', `OR, C, J')
pushdef(`script', `AND, A, J')
pushdef(`script', `AND, B, J')
pushdef(`script', `NOT, J, J')
pushdef(`script', `AND, D, J')
pushdef(`script', `RUN')
loop()

run(0)
define(`part2', data)

divert`'part1
part2
