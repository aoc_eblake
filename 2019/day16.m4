divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day16.input] day16.m4
# Optionally use -Dlimit=N to perform fewer than 100 phases
# Optionally use -Donly=[12] to skip a part, or -Donly=3 for bonus
# Optionally use -Dverbose=[12] to see some progress

include(`common.m4')ifelse(common(16), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`input', translit(include(defn(`file')), `
'))
ifdef(`limit', `ifelse(eval(limit & 1), 1, `errprint(`Limit must be even
')m4exit(1)')', `define(`limit', 100)')

define(`l', len(input))
define(`l1', decr(l))
define(`parse', `_$0($1, substr(input, $1, 1))')
define(`_parse', `define(`d0_$1', $2)define(`d2_$1', $2)')
forloop_arg(0, l1, `parse')
define(`d0_'l, 0)
define(`d1_'l, 0)
define(`o', eval(-10000000 + 1forloop(0, 6, `defn(`d0_'', `)')))
define(`prep', `define(`d2_'eval(l + $1), d2_$1)define(`p'$1, 0)')
forloop_arg(0, 7, `prep')
define(`prep', `define(`t'i`$1', eval((i + $1) % 10))')
forloop_var(`i', 0, 9, `forloop_arg(0, 9, `prep')')
define(`last', `substr($1, decr(len($1)), 1)')

ifelse(ifdef(`only', only, 1), 1, `

define(`m', `_m(eval(($1 + 1) / ($2 + 1) % 4), $1)')
define(`_m', `m$1($2, 2)')
define(`m0')
define(`m1', `` +d$$2_$1'')
define(`m2')
define(`m3', `` -d$$2_$1'')
define(`slow', `define(`set$1', `last(eval('forloop(0, l1,
  `m(', `, $1)')`))')')
define(`_medium', ``eval((d$$1_'incr($3)` + d$$2_$3 + 20 - d$$2_'eval($3 * 2 +
  1)` - d$$2_'eval($3 * 2 + 2)`) % 10)'')
define(`medium', `define(`set$1', _$0(1, 2, $1))')
define(`_fast', ``defn(`t'd$$1_'incr($3)``'d$$2_$3)'')
define(`fast', `define(`set$1', _$0(1, 2, $1))')
forloop_arg(0, l / 3 - 1, `slow')
forloop_arg(l / 3, l / 2 - 1, `medium')
forloop_arg(l / 2, l1, `fast')

define(`set', `define(`d$1_$3', set$3($1, $2))')
define(`phase', `output(2, eval($1 * 2))forloop_rev(l1, 0, `set(1, 0,',
  `)')forloop_rev(l1, 0, `set(0, 1,', `)')')

forloop_arg(1, limit / 2, `phase')
define(`part1', forloop(0, 7, `defn(`d0_'', `)'))
')

ifelse(eval(o > l * 10000 - 8 || o < l * 5000), 1,
`define(`part2', ``part2 is not practical for this input'')',
ifdef(`only', only, 2), 2, `

dnl https://github.com/akalin/advent-of-code-2019-python3/blob/master/day16.py
output(1, `starting part2...')

dnl precomputed binomial mod 5
define(`b', `define(`binom'n`$1', ifelse(eval($1 > n), 1, 0, n, 0, 1, $1, 0, 1,
  `eval((defn(`binom'decr(n)$1) + defn(`binom'decr(n)decr($1))) % 5)'))')
forloop_var(`n', 0, 4, `forloop_arg(0, 4, `b')')
define(`binomMod5', `ifelse($2, 0, 1, eval($1 < 5 && $2 < 5), 1,
  `binom$1$2', `_$0(defn(`binom'eval($1 % 5)eval($2 % 5)),
  `$0(eval($1 / 5), eval($2 / 5))')')')
define(`_binomMod5', `ifelse($1, 0, 0, `eval($1 * $2 % 5)')')
define(`binomMod10', `eval((!(~$1 & $2) * 5 +
  binomMod5($1, $2) * 6) % 10)')

define(`c', `_$0(0, 7, eval(($1 + o) % l), binomMod10(eval($1 + limit - 1),
  decr(limit)))')
define(`_c', `ifelse($4, 0, `', `define(`p$1', eval((p$1 + d2_$3 * $4)
  % 10))ifelse($1, $2, `', `$0(incr($1), $2, incr($3), $4)')')')
forloop_arg(0, eval(l * 10000 - o - 9), `c')
define(`c', `_$0(0, eval(7 - i), eval((l * 10000 + i - 8) % l),
  binomMod10(eval(l * 10000 - o + i - 8 + limit - 1), decr(limit)))')
forloop_var(`i', 0, 7, `c')
define(`part2', forloop(0, 7, `defn(`p'', `)'))

')
ifelse(ifdef(`only', only), 3, `

# Bonus: https://www.reddit.com/r/adventofcode/comments/ebb8w6/2019_day_16_part_three_a_fanfiction_by_askalski/
output(1, `starting part3...')
include(`math64.m4')

define(`limit', 287029238942)
define(`limit1', add64(limit, -1))
define(`o', eval(-10000000 + 1forloop(0, 6, `defn(`d0_'', `)')))

dnl precomputed binomial mod 5
define(`b', `define(`binom'n`$1', ifelse(eval($1 > n), 1, 0, n, 0, 1, $1, 0, 1,
  `eval((defn(`binom'decr(n)$1) + defn(`binom'decr(n)decr($1))) % 5)'))')
forloop_var(`n', 0, 4, `forloop_arg(0, 4, `b')')
define(`small', `ifelse(len($1$2), 2, `eval($1 < $3 && $2 < $3)')')
define(`mod2', `eval(last($1) & 1)')
define(`mod5', `eval(last($1) % 5)')
define(`div2', `ifelse(eval(len($1) < 10), 1, `eval($1 / 2)',
  `div10(mul64($1, 5))')')
define(`div5', `ifelse(eval(len($1) < 10), 1, `eval($1 / 5)',
  `div10(add64($1, $1))')')
define(`div10', `substr($1, 0, decr(len($1)))')
define(`binomMod', `ifelse($2, 0, 1, small($1, $2, $3), 1,
  `binom$1$2', `_$0(defn(`binom'mod$3($1)mod$3($2)),
  `$0$3(div$3($1), div$3($2))', $3)')')
define(`_binomMod', `ifelse($1, 0, 0, `mod$3(mul64($1, $2))')')
define(`binomMod2', `ifelse(eval(len($1) < 10 && len($2) < 10), 1,
  `eval(!(~$1 & $2))', `binomMod($1, $2, 2)')')
define(`binomMod5', `binomMod($1, $2, 5)')
define(`binomMod10', `eval((binomMod2($1, $2) * 5 +
  binomMod5($1, $2) * 6) % 10)')

define(`c', `ifelse(eval($1 % 10000), 0, `output(2, $1)')_$0(0, 7,
  eval(($1 + o) % l), binomMod10(add64($1, limit1), limit1))')
define(`_c', `ifelse($4, 0, `', `define(`p$1', eval((p$1 + d2_$3 * $4)
  % 10))ifelse($1, $2, `', `$0(incr($1), $2, incr($3), $4)')')')
forloop_arg(0, eval(l * 10000 - o - 9), `c')
define(`c', `_$0(0, eval(7 - i), eval((l * 10000 + i - 8) % l),
  binomMod10(add64(eval(l * 10000 - o + i - 8), limit1), limit1))')
forloop_var(`i', 0, 7, `c')
define(`part3', forloop(0, 7, `defn(`p'', `)'))

')divert`'part1
part2
ifdef(`part3', `part3', `dnl')
