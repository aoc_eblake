divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day8.input] day8.m4

include(`common.m4')ifelse(common(8), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`input', translit(include(defn(`file')), `
'))

define(`size', eval(25 * 6))
define(`zeroes', size)
forloop(0, decr(size), `define(`i'', `, 2)')
define(`layer', `_$0(substr(input, eval($1 * size), size))')
define(`_layer', `define(`c0', 0)define(`c1', 0)define(`c2', 0)define(`l',
  $1)forloop_arg(0, size - 1, `pixel')ifelse(eval(c0 < zeroes), 1,
  `define(`zeroes', c0)define(`part1', eval(c1 * c2))')')
define(`char', `define(`c$1', incr(c$1))')
define(`pixel', `_$0($1, substr(l, $1, 1))')
define(`_pixel', `char($2)ifelse(i$1, 2, `define(`i$1', $2)')')
forloop_arg(0, len(input) / size - 1, `layer')

include(`ocr.m4')
define(`let', `forloop(0, 5, `_$0($1, ', `)')')
define(`_let', `forloop($1, eval($1+4), `ifelse(defn(`i'eval($2 * 25 + ',
  `)), 1, ``X'', `` '')')')
define(`part2', forloop(0, 4, `ocr(let(eval(', `*5)))'))

define(`show', `ifelse(defn(`i'eval(y * 25 + $1)), 1, `X', ` ')')
divert`'part1
part2
