#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <ctype.h>
#include <limits.h>
#include <search.h>

static int debug_level = -1;
static void
debug_init(void) {
  if (debug_level < 0)
    debug_level = atoi(getenv("DEBUG") ?: "0");
}

static void __attribute__((format(printf, 2, 3)))
debug_raw(int level, const char *fmt, ...) {
  va_list ap;
  if (debug_level < 0)
    debug_level = atoi(getenv("DEBUG") ?: "0");
  if (debug_level >= level) {
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
  }
}
#define debug(...) debug_raw(1, __VA_ARGS__)

static int __attribute__((noreturn)) __attribute__((format(printf, 1, 2)))
die(const char *fmt, ...)
{
  va_list ap;
  va_start(ap, fmt);
  vprintf(fmt, ap);
  va_end(ap);
  putchar('\n');
  exit(1);
}

#define MAX 81
struct grid {
  char tile;
  short distance;
} grid[MAX][MAX + 1];
static int maxx, maxy;
struct progress {
  short distance;
  uint64_t seen; /* bitmap: 0-25 keys, 32-57 doors */
};
struct pt {
  char id;
  uint8_t x;
  uint8_t y;
  uint8_t q;
  struct progress to_keys[26];
};
static struct pt cursor[4];
static struct pt keys[26];
static struct pt doors[26];
static int k; /* number of keys */

enum dir { NONE = -1, U, R, D, L };

static int deepest;

static struct pt *
key(char c) {
  if (c < 'a' || c > 'z')
    die("invalid key '%c'", c);
  return &keys[c - 'a'];
}

static uint64_t key_mask(char c) {
  if (c < 'a' || c > 'z')
    die("invalid key '%c'", c);
  return 1 << (c - 'a');
}

static struct pt *
door(char c) {
  if (c < 'A' || c > 'Z')
    die("invalid door '%c'", c);
  return &doors[c - 'A'];
}

static uint64_t door_mask(char c) {
  if (c < 'A' || c > 'Z')
    die("invalid key '%c'", c);
  return 1ULL << (32 + (c - 'A'));
}

static int
minu(unsigned int a, unsigned b) {
  return a < b ? a : b;
}

static void
dump_grid(void) {
  int x, y;

  if (!debug_level)
    return;
  for (y = 0; y < maxy; y++) {
    for (x = 0; x <= maxx; x++)
      debug(" %c", grid[y][x].tile);
    for (x = 0; x < maxx; x++)
      debug("%2d", grid[y][x].distance);
    debug("\n\n");
  }
}

static const char *
mask_to_str(uint64_t mask) {
  static char buf[26 * 2 + 1];
  int i;
  char *p = buf;
  for (i = 0; i < 26; i++)
    if (mask & (1 << i))
      *p++ = 'a' + i;
  for (i = 0; i < 26; i++)
    if (mask & (1ULL << (32 + i)))
      *p++ = 'A' + i;
  *p = '\0';
  return buf;
}

static void
dump_point(struct pt *p, int k) {
  int i;

  debug("point %c at %d,%d quadrant %d:\n", p->id, p->x, p->y, p->q);
  for (i = 0; i < k; i++)
    if (p->to_keys[i].distance == -1)
      debug( "to key %c: impossible, reached by other bot\n", 'a' + i);
    else
      debug(" to key %c: %d steps, encounter '%s'\n", 'a' + i,
            p->to_keys[i].distance, mask_to_str(p->to_keys[i].seen));
}

static short
scan_one(int x, int y, enum dir from, short d, uint64_t seen,
         struct progress *prog, int q) {
  int t;
  int r = -1;

  if (d > deepest)
    deepest = d;
  if (grid[y][x].tile == '#')
    return -1;
  if ((unsigned) grid[y][x].distance < d)
    return -1;
  grid[y][x].distance = d;
  t = grid[y][x].tile;
  if (isupper(t))
    seen |= door_mask(t);
  else if (islower(t)) {
    seen |= key_mask(t);
    prog[t - 'a'].distance = d;
    prog[t - 'a'].seen = seen;
    key(t)->q = q;
    if (!(seen >> 32))
      r = d;
  }
  d++;
  if (from != D)
    r = minu(r, scan_one(x, y - 1, U, d, seen, prog, q));
  if (from != L)
    r = minu(r, scan_one(x + 1, y, R, d, seen, prog, q));
  if (from != U)
    r = minu(r, scan_one(x, y + 1, D, d, seen, prog, q));
  if (from != R)
    r = minu(r, scan_one(x - 1, y, L, d, seen, prog, q));
  return r;
}

static int
scan_from(struct pt *p, int k, int q) {
  int i, j, r;

  debug("scanning from %d,%d (%c)\n", p->x, p->y, p->id);
  for (j = 0; j < maxy; j++)
    for (i = 0; i < maxx; i++)
      grid[j][i].distance = -1;
  r = scan_one(p->x, p->y, NONE, 0, 0, p->to_keys, q);
  dump_point(p, k);
  return r;
}

/* https://en.wikipedia.org/wiki/A*_search_algorithm */
#define MAXWORK 10000
#define MAXNODE (128 * 1024)
#define QUAD(n) (((n) >> 28) & 3)
#define FROM_Q(n, q) (((n) >> (32 + (q) * 8)) & 0x1f)
#define FROM(n) FROM_Q(n, QUAD(n))
#define KEYS(n) ((n) & ((1 << 26) - 1))
static struct node {
  long long n; /* combination of QUAD, FROM and KEYS */
  long long parent; /* which node we were on previously */
  short g; /* cheapest known path from start to here */
  short f; /* estimate based on g + h(n) */
} nodes[MAXNODE];
static int nnodes;
static void *root; /* tree of nodes visited */

static struct work {
  long long node; /* node needing another visit */
  short next; /* index of next pending, sorted by decreasing f, -1 if none */
  short prev; /* index of prev node, -1 if none */
  short f; /* estimated distance */
} pending[MAXWORK];
static short head = -1; /* head of pending, -1 if empty */
static short unused; /* first unused index of pending */
static short avail = -1; /* head of free list for reuse of slots in pending */

static bool
drop(long long node) {
  int i = head;
  while (i != -1) {
    if (pending[i].node == node) {
      if (i == head) {
        head = pending[i].next;
      } else {
        pending[pending[i].prev].next = pending[i].next;
        if (pending[i].next != -1)
          pending[pending[i].next].prev = pending[i].prev;
      }
      pending[i].next = avail;
      avail = i;
      return true;
    }
    i = pending[i].next;
  }
  return false;
}

static void
add(long long node, int f) {
  int next;
  int i;

  if (avail != -1) {
    next = avail;
    avail = pending[next].next;
  } else {
    next = unused;
    if (++unused == MAXWORK)
      die("recompile with larger MAXWORK");
  }
  pending[next].node = node;
  pending[next].f = f;
  if (head == -1 || f < pending[head].f) {
    pending[next].next = head;
    pending[next].prev = -1;
    head = next;
  } else {
    i = head;
    while (pending[i].next != -1 && f >= pending[pending[i].next].f)
      i = pending[i].next;
    pending[next].next = pending[i].next;
    pending[next].prev = i;
    pending[i].next = next;
  }
  if (pending[next].next != -1)
    pending[pending[next].next].prev = next;
}

static int
compare(const void *pa, const void *pb) {
  const struct node *a = pa, *b = pb;

  return (b->n > a->n) - (b->n < a->n);
}

static struct node *
lookup(long long node) {
  struct node *n = &nodes[nnodes];
  void *p;

  n->n = node;
  p = tsearch(n, &root, compare);
  if (!p)
    die("out of memory");
  if (*(struct node **)p == n && ++nnodes == MAXNODE)
    die("recompile with larger MAXNODE");
  return *(struct node **)p;
}

static void
path(long long node, int q) {
  struct node *n;

  if (!KEYS(node))
    return;
  n = lookup(node);
  if (n->parent == -1) {
    puts("<uncomputed...>");
    return;
  }
  path(n->parent, q);
  if (node == -1)
      putchar('\n');
  else if (QUAD(n->n) == q)
    putchar(FROM_Q(n->n, q) + 'a');
  else
    putchar(' ');
}

static int
h(long long n, int bots) {
  int q, r = 0;

  for (q = 0; q < bots; q++) {
    int from = FROM_Q(n, q);
    struct pt *p = from == 0x1f ? &cursor[q] : &keys[from];
    int i, b = 0;

    for (i = 0; i < k; i++)
      if (!(n & (1 << i)) && p->q == q && p->to_keys[i].distance > b)
        b = p->to_keys[i].distance;
    r += b;
  }
  return r;
}

static void
nop(void *ignored) {
}

static const char *
from_str(long long n, int bots) {
  static char buf[5] = "";
  int i;

  for (i = 0; i < bots; i++)
    buf[i] = FROM_Q(n, i) == 0x1f ? '@' : FROM_Q(n, i) + 'a';
  buf[i] = '\0';
  return buf;
}

static int
astar(int bots) {
  struct node *n;
  int i, q, count = 0;
  long long next = 0;

  tdestroy(root, nop);
  root = NULL;
  memset(nodes, -1, sizeof nodes);
  nnodes = 0;
  for (q = 0; q < bots; q++)
    next |= 0x1fLL << (32 + q * 8);
  n = lookup(next);
  n->g = 0;
  n->f = h(next, bots);
  add(next, n->f);
  while (head != -1) {
    long long current = pending[head].node;
    struct pt *p;
    unsigned tentative;
    struct node *n2;

    n = lookup(current);
    count++;
    if (KEYS(n->n) == (1 << k) - 1) {
      printf("search done with %d iterations, %d nodes, %d depth\n",
            count, nnodes, unused);
      n2 = lookup(-1);
      n2->parent = current;
      n2->g = n->g;
      return n->g;
    }
    debug("considering %07llx at %s from quad %lld\n", KEYS(n->n),
          from_str(n->n, bots), QUAD(n->n));
    drop(current);
    for (i = 0; i < k; i++) {
      if (current & (1 << i))
        continue; /* already have key */
      q = keys[i].q;
      p = FROM_Q(n->n, q) == 0x1f ? &cursor[q] : &keys[FROM_Q(n->n, q)];
      if (((p->to_keys[i].seen >> 32) | (int)p->to_keys[i].seen)
          & ~(KEYS(n->n) | (1 << i)))
        continue; /* still blocked by door, or will reach another key first */
      tentative = n->g + p->to_keys[i].distance;
      next = current | (1 << i);
      next &= ~((0x1fLL << (32 + q * 8)) | (3 << 28));
      next |= ((0LL + i) << (32 + q * 8)) | (q << 28);
      n2 = lookup(next);
      if (tentative < n2->g) {
        n2->parent = current;
        n2->g = tentative;
        n2->f = tentative + h(next, bots);
        drop(next);
        add(next, n2->f);
        debug("need to visit %07llx through %s quad %d, steps %d + %d = %d\n",
              KEYS(n2->n), from_str(next, bots), q, n2->g, n2->f-n2->g, n2->f);
      }
    }
  }
  die("no path found");
}

int
main(int argc, char **argv) {
  int ch, i;
  int x = 0, y = 0;
  struct pt *p;
  int d = 0;
  int best, count;

  debug_init();
  if (argc > 1 && strcmp(argv[1], "-"))
    if (!(stdin = freopen(argv[1], "r", stdin))) {
      perror("failure");
      exit(2);
    }

  while ((ch = getchar()) != EOF) {
    grid[y][x].tile = ch;
    if (ch == '\n') {
      if (y && x != maxx)
        die("uneven line lengths");
      x = 0;
      maxy = ++y;
      continue;
    }
    if (y >= MAX)
      die("recompile with larger MAX y");
    p = NULL;
    if (ch == '@') {
      p = &cursor[0];
    } else if (ch >= 'a' && ch <= 'z') {
      p = key(ch);
      k++;
    } else if (ch >= 'A' && ch <= 'Z') {
      p = door(ch);
      d++;
    } else if (ch != '.' && ch != '#')
      die("unexpected character %c", ch);
    if (p) {
      p->id = ch;
      if (p->x)
        die("duplicate character %c", ch);
      p->x = x;
      p->y = y;
    }
    x++;
    if (!y) {
      if (ch != '#')
        die("expected wall in row 0");
      if (x >= MAX + 1)
        die("recompile with larger MAX x");
      maxx = x;
    } else if (x > maxx) {
      die("uneven line length");
    }
  }
  count = 0;
  for (i = 0; i < k; i++)
    if (!keys[i].x)
      die("nonconsecutive keys");
    else if (doors[i].x)
      count++;
  if (d != count || d > k)
    die("mismatched doors");
  if (!cursor[0].x)
    die ("missing cursor");
  printf("Operating on %dx%d grid from %d,%d, with %d keys, %d doors\n",
         maxx, maxy, cursor[0].x, cursor[0].y, k, d);

  for (i = 0; i < k; i++)
    if (doors[i].x)
      scan_from(door(i + 'A'), k, 0);
  for (i = 0; i < k; i++)
    scan_from(key(i + 'a'), k, 0);
  best = scan_from(&cursor[0], k, 0);
  printf("current closest key %d steps away\n", best);
  printf("all paths explored, furthest distance %d\n", deepest);
  if (deepest * k > SHRT_MAX)
    die("recompile with 32-bit distance");
  dump_grid();
  best = astar(1);
  printf("best path requires %d steps, using path ", best);
  path(-1, 0);

  x = cursor[0].x;
  y = cursor[0].y;
  if (grid[y - 1][x - 1].tile != '.' || grid[y - 1][x].tile != '.' ||
      grid[y - 1][x + 1].tile != '.' || grid[y][x - 1].tile != '.' ||
      grid[y][x + 1].tile != '.' || grid[y + 1][x - 1].tile != '.' ||
      grid[y + 1][x].tile != '.' || grid[y + 1][x + 1].tile != '.')
    die("grid not suitable for part 2");
  grid[y - 1][x].tile = '#';
  grid[y][x - 1].tile = '#';
  grid[y + 1][x].tile = '#';
  grid[y][x + 1].tile = '#';
  dump_grid();
  best = 0;
  for (i = 0; i < k; i++) {
    memset(key(i + 'a')->to_keys, -1, sizeof p->to_keys);
    scan_from(key(i + 'a'), k, 0);
  }
  cursor[0].x--;
  cursor[0].y--;
  cursor[1] = cursor[0];
  cursor[1].x += 2;
  cursor[2] = cursor[0];
  cursor[2].x += 2;
  cursor[2].y += 2;
  cursor[3] = cursor[0];
  cursor[3].y += 2;
  for (i = 0; i < 4; i++) {
    memset(cursor[i].to_keys, -1, sizeof cursor[i].to_keys);
    cursor[i].q = i;
    scan_from(&cursor[i], k, i);
  }
  head = -1;
  unused = 0;
  avail = -1;
  memset(pending, -1, sizeof pending);
  best += astar(4);
  printf("using 4 bots requires %d steps, with path:\n", best);
  for (i = 0; i < 4; i++)
    path(-1, i);
  return 0;
}
