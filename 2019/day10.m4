divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day10.input] day10.m4
# Optionally use -Dverbose=1 to see more details

include(`common.m4')ifelse(common(10), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`input', include(defn(`file')))
define(`rowlen', index(input, `
'))
ifelse(eval((rowlen + 1) * rowlen), len(input), `',
  `errprint(`input not square
')m4exit(1)')
define(`input', translit(input, `.#
', `01'))

define(`p', `defn(`p'eval($2 * rowlen + $1))')
define(`split', `eval($1 % rowlen),eval($1 / rowlen)')
define(`setup', `define(`p$1', substr(input, $1, 1))')
forloop_arg(0, rowlen * rowlen - 1, `setup')

define(`check', `define(`count', 0)_$0(split($1), $1)ifelse(
  eval(count > part1), 1, `define(`part1', count)define(`best', $1)')')
define(`_check', `ifelse(p($1, $2), 1, `define(`x', $1)define(`y',
  $2)define(`from', $3)forloop_rev($3 - 1, 0, `check_one(', `)')forloop_arg(
  $3 + 1, rowlen * rowlen - 1, `check_one')')')
define(`check_one', `ifelse(defn(`v$1'), from, `', `define(`dx',
  eval($1 % rowlen - x))define(`dy', eval($1 / rowlen - y))_$0(split($1),
  $1)act()')')
define(`_check_one', `ifelse(eval($1 >= 0 && $1 < rowlen && $2 >= 0 &&
  $2 < rowlen), 1, `ifelse(p$3.defn(`hit'), 1., `define(`hit',
  $3)')define(`v$3', from)$0(eval($1 + dx), eval($2 + dy),
  eval($3 + dx + dy * rowlen))')')

define(`part1', 0)
define(`act', `ifdef(`hit', `define(`count', incr(count))popdef(`hit')')')
forloop_arg(0, rowlen * rowlen - 1, `check')
define(`reset', `undefine(`v$1')')
forloop_arg(0, rowlen * rowlen - 1, `reset')

define(`list')
define(`quote', `ifelse($#, `0', `', ``$*'')')
define(`sign', `eval((($1) > 0) - (($1) < 0))')
define(`cross', `eval(($1) * ($4) - ($2) * ($3))')
define(`cmp', `_$0(split($1), split($2))')
define(`_cmp', `ifelse(eval(($1 < x) != ($3 < x)), 1, `sign($3 - $1)',
  eval(($1 == x) && ($3 == x)), 1, `sign($2 - $4)',
  `sign(cross($3 - x, $4 - y, $1 - x, $2 - y))')')
define(`insert', `define(`list', quote(_$0($1, list)))')
define(`_insert', `ifelse($#, 2, `$1,', `ifelse(cmp($1, $2), 1,
  `$2,$0($1, shift(shift($@)))', `$*')')')
define(`act', `ifdef(`hit', `insert(hit)popdef(`hit')')')
check(best)
ifdef(`__gnu__', `define(`pick', `pushdef(`t', `popdef(`t')$'`200')t($@)')',
  `define(`pick', `_$0(200, $@)')define(`_pick', `ifelse($1, 1, `$2',
  `$0(decr($1), shift(shift($@)))')')')
define(`part2', pick(list))
define(`part2', eval((part2 % rowlen) * 100 + part2 / rowlen))

divert`'part1()output(1, `' (at split(best)))
part2
