divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day11.input] day11.m4

include(`intcode.m4')ifelse(intcode(11), `ok', `',
`errprint(`Missing IntCode initialization
')m4exit(1)')

parse(input)
define(`write', `define(`data', $1)')
define(`done', `define(`repeat')')
define(`min', `ifelse(eval($1 < min$1), 1, `define(`min$1', $1)')')
define(`max', `ifelse(eval($1 > max$1), 1, `define(`max$1', $1)')')
define(`offset', 1000)
define(`visit', `_$0($1, eval($2 + offset), eval($3 + offset), $4)')
define(`_visit', `ifdef(`g$1_$2_$3', `', `define(`count',
  incr(count))')define(`g$1_$2_$3', $4)')
define(`g', defn(`visit'))
define(`_g', `ifdef(`g$1_$2_$3', `g$1_$2_$3', 0)')

# Loop until done, expect read then two writes, then pause before any read
# (expectations are a stack, create them in reverse order)
define(`loop', `
  oneshot(`io', defn(`pause_after_write'))
  oneshot(`io', defn(`run_after_read'))
  run(pc)
  visit($1, x, y, data)
  oneshot(`io', defn(`pause_on_read'))
  oneshot(`io', defn(`run_after_write'))
  run(pc)
  define(`d', eval((ifelse(data, 0, 3, 1) + d) % 4))
  ifelse(d, 0, `define(`y', decr(y))min(`y')',
         d, 1, `define(`x', incr(x))max(`x')',
	 d, 2, `define(`y', incr(y))max(`y')',
	 `define(`x', decr(x))min(`x')')
  repeat($1)')

define(`try', `
  save()
  define(`x', 0)
  define(`y', 0)
  define(`minx', 0)
  define(`maxx', 0)
  define(`miny', 0)
  define(`maxy', 0)
  define(`count', 0)
  define(`d', 0)
  visit($1, 0, 0, $1)
  define(`read', `g($1, x, y)')
  define(`repeat', `loop($1)')loop($1)
  restore()')
try(0)
define(`part1', count)
try(1)
include(`ocr.m4')
define(`char', `forloop('miny`, 'maxy`, `_$0($1, ', `)')')
define(`_char', `forloop($1, eval($1+4), `ifelse(g(1, ', `, $2), 1,
  ``X'', `` '')')')
define(`part2', forloop(0, eval((maxx - minx - 4)/5),
  `ocr(char(eval('minx`+1+', `*5)))'))

divert`'part1
part2
