divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day25.input] day25.m4
# Optionally use -Dverbose=[12] to see some progress
# m4 -Dinteractive to let user play the game (implies -Dverbose)
# m4 -Dcalltrace to debug function calls within intcode
# m4 -Dnowarp to skip hack of warping to final room
# m4 -Dnobrute to skip brute force loop

include(`intcode.m4')ifelse(intcode(25), `ok', `',
`errprint(`Missing IntCode initialization
')m4exit(1)')

parse(input)
ifdef(`interactive', `define(`verbose', 1)')
ifelse(verbose, 0, `', `ifdef(`__gnu__', `',
  `errprintn(`verbose mode requires GNU m4')m4exit(1)')')

define(`writev', ifelse(verbose, 0, `', ``ifelse(eval($1 < 128 && $1 != 96), 1,
  `_writev(format(``%c'',$1))')''))
define(`_writev', `divert`$1'divert(-1)')
define(`write', defn(`writev')`define(`window', quote(shift(window),
  $1))ifelse(eval($1 <= 32), 1, `check(quote(window))')')
define(`read', `oneshot(`io', `divert`'`Need more input!'
')0')

define(`part1')
define(`window', `0,0,0,0,0,0,0')
define(`typing', ``116,121,112,105,110,103,32'')
define(`heavier', ``101,97,118,105,101,114,32'')
define(`lighter', ``105,103,104,116,101,114,32'')
define(`check', `ifelse(`$1', typing, `pushdef(`write', defn(`collect'))',
  `$1', heavier, `rebalance(0)', `$1', lighter, `rebalance(1)')')
define(`collect', defn(`writev')`ifelse($1, 32, `popdef(`write')',
  eval($1 >= 48 && $1 <= 57), 1,
  `define(`part1', defn(`part1')eval($1 - 48))', `_collectX($1)')')
define(`_collectX', `errprintn(`missing collect for $1')m4exit(1)')
define(`rebalance')

# Observations via tracing my input:
# f1256 returns at 1268 after printing a character from a string at SP-1
# f2585 returns at 2629 with 0 or 1 based on string equality
# f1174 walks string at SP-1 using function pointer at SP-2:
# either f1256 to print string, or f2585 to compare string
# f1234 prints a string at SP-1
# strings are encoded as len then len bytes encoded
# by adding length, distance from length, and data (see findstr)
# f2525 parses array of known commands
# f1424 moves to a room at SP-1, then checks inventory with f1310, then
# invokes any function tied to the room
# f1130 is array walker with args Addr, Arraylen, Size, Fn, scratch x3
# Rooms (start at 3124) are encoded as Name, Descr, Fn, N, E, S, W
# For example, 4556 contains: 4563 (Pressure-Sensitive Floor), 4588 (Analyzing),
# 1553 (function), 0 (nothing north), 0, 4457 (room to south), 0
# Items (start at 4601) encoded as Room (or -1 if held), Name, Weight, Fn
# weights are 27 + item index greater than a power of 2
# f2722 checks weights, using bitwise iteration with f2763, f1702 helpers

define(`origop9', defn(`op9'))
define(`op9', `ifelse(pc, 1256, `', pc, 1268, `',
  $1, 1, `_$0(get1(1), pc, base)',
  `errprintn(pc` has unusual op9 use')')origop9($@)')
define(`_op9', `ifelse(eval($1 > 0), 1, `call', `return')($@)')
define(`depth')
define(`call', `pushdef(`depth', ` 'defn(`depth'))pushdef(`f',
  ``f$2'')errprintn(depth()f()` called with 'forloop_arg(1, $1 - 1, `arg'))')
define(`arg', `mem(eval(base + $1))`, '')
define(`return', `errprintn(depth()f()popdef(`f')` returned 'mem(
  eval($3 + $1 + 1))` at $2')popdef(`depth')')
ifdef(`calltrace', `oneshot', `pushdef')(`op9', defn(`origop9'))

define(`feed', `ifelse($#, 1, `_$0(10)',
  `$0(shift($@))_$0(32)')ifdef(`$0_$1', `$0_$1()', `oops()')')
define(`_feed', `oneshot(`read', `$1')')
define(`feed_east', `_feed(116)_feed(115)_feed(97)_feed(101)')
define(`feed_west', `_feed(116)_feed(115)_feed(101)_feed(119)')
define(`feed_north', `_feed(104)_feed(116)_feed(114)_feed(111)_feed(110)')
define(`feed_south', `_feed(104)_feed(116)_feed(117)_feed(111)_feed(115)')
define(`feed_inv', `_feed(118)_feed(110)_feed(105)')
define(`feed_drop', `_feed(112)_feed(111)_feed(114)_feed(100)')
define(`feed_take', `_feed(101)_feed(107)_feed(97)_feed(116)')
define(`loop', `ifdef(`script', `feed(script)popdef(`script')loop()')')
# The following are hardcoded to my puzzle, and would need adjusting
# or more code to be suitable for other puzzles...
define(`feed_mug', `_feed(103)_feed(117)_feed(109)')
define(`feed_loom', `_feed(109)_feed(111)_feed(111)_feed(108)')
define(`feed_food', `_feed(100)_feed(111)_feed(111)_feed(102)')
define(`feed_ration', `_feed(110)_feed(111)_feed(105)_feed(116)_feed(
  97)_feed(114)')
define(`feed_prime', `_feed(101)_feed(109)_feed(105)_feed(114)_feed(112)')
define(`feed_number', `_feed(114)_feed(101)_feed(98)_feed(109)_feed(
  117)_feed(110)')
define(`feed_manifold', `_feed(100)_feed(108)_feed(111)_feed(102)_feed(
  105)_feed(110)_feed(97)_feed(109)')
define(`feed_jam', `_feed(109)_feed(97)_feed(106)')
define(`feed_spool', `_feed(108)_feed(111)_feed(111)_feed(112)_feed(115)')
define(`feed_of', `_feed(102)_feed(111)')
define(`feed_cat6', `_feed(54)_feed(116)_feed(97)_feed(99)')
define(`feed_fuel', `_feed(108)_feed(101)_feed(117)_feed(102)')
define(`feed_cell', `_feed(108)_feed(108)_feed(101)_feed(99)')

# The following script solves my puzzle, but fails hard on others. While
# more elaborate coding could learn an arbitrary map, I instead chose for
# my general solution to hack the binary; see below.
define(`canned', `divert(-1)
pushdef(`script', ``east'')
pushdef(`script', ``take', `food', `ration'')
pushdef(`script', ``south'')
pushdef(`script', ``take', `prime', `number'')
pushdef(`script', ``north'')
pushdef(`script', ``east'')
pushdef(`script', ``take', `manifold'')
pushdef(`script', ``east'')
pushdef(`script', ``east'')
pushdef(`script', ``take', `jam'')
pushdef(`script', ``west'')
pushdef(`script', ``north'')
pushdef(`script', ``east'')
pushdef(`script', ``take', `spool', `of', `cat6'')
pushdef(`script', ``west'')
pushdef(`script', ``north'')
pushdef(`script', ``take', `fuel', `cell'')
pushdef(`script', ``south'')
pushdef(`script', ``south'')
pushdef(`script', ``west'')
pushdef(`script', ``west'')
pushdef(`script', ``west'')
pushdef(`script', ``north'')
pushdef(`script', ``north'')
pushdef(`script', ``west'')
pushdef(`script', ``take', `mug'')
pushdef(`script', ``east'')
pushdef(`script', ``north'')
pushdef(`script', ``east'')
pushdef(`script', ``east'')
pushdef(`script', ``take', `loom'')
pushdef(`script', ``west'')
pushdef(`script', ``west'')
pushdef(`script', ``south'')
pushdef(`script', ``south'')
pushdef(`script', ``west'')
pushdef(`script', ``north'')
pushdef(`script', ``west'')
pushdef(`script', ``inv'')
')

# The following brute forces my puzzle, but fails on other item names.
# This approach tries up to 255 combinations, and does not pay attention
# to feedback from previous attempts; it takes about 1 minute to run
# through enough combinations to solve my puzzle. See below for my
# generic solution which hacks the binary instead, to get a solution for
# any puzzle in at most 8 attempts.
define(`carrying', eval((1 << 8) - 1))
define(`graybit', `eval(($1 ^ ($1 >> 1)) ^ (decr($1) ^ (decr($1) >> 1)))')
define(`adjust', `_$0(graybit($1))pushdef(`script', ``north'')')
define(`_adjust', `pushdef(`script', ifelse(eval(carrying & $1), 0,
  ```take''', ```drop''')adjust$1())define(`carrying', eval(carrying ^ $1))')
define(`adjust0', `errprintn(bad gray)exit(1)')
define(`adjust256', defn(`adjust0'))
define(`adjust1', ``, jam'')
define(`adjust2', ``, loom'')
define(`adjust4', ``, mug'')
define(`adjust8', ``, spool, of, cat6'')
define(`adjust16', ``, prime, number'')
define(`adjust32', ``, food, ration'')
define(`adjust64', ``, fuel, cell'')
define(`adjust128', ``, manifold'')
define(`brute', `forloop_arg(1, carrying, `adjust')')

# These commands allow -Dinteractive the chance to try a bit more,
# but while it works for my puzzle, the inventory names are not supported
# for other puzzles, so more work would be needed to make it generic...
define(`west', `feed(`$0')run`'')
define(`north', defn(`west'))
define(`east', defn(`west'))
define(`south', defn(`west'))
define(`inv', defn(`west'))
define(`take', `feed(`$0', $@)run`'')
define(`drop', defn(`take'))

# Now for the generic solution.
# While some item names vary by puzzle, other strings are always present:
# Locate the room named "Hull Breach" (11 chars, ascii 72-11, 117-12, ...)
# Likewise for "Pressure-Sensitive Floor" (24 chars, ascii 80-24, 114-25...)
# And for the item "escape pod" (10 chars, 101-10, 115-11...)
define(`findmagic', `_$0(mem($1).mem(incr($1)).mem(incr(incr($1))), $1)')
define(`_findmagic', `ifelse($1, 11.61.105, `define(`hullstr', $2)',
  $1, 24.56.89, `define(`goalstr', $2)',
  $1, 10.91.104, `define(`podstr', $2)')')
forloop_arg(0, pos, `findmagic')
define(`hull', eval(hullstr - 7))
define(`goal', eval(goalstr - 7))
define(`findpod', `ifelse(mem($1), podstr, `define(`pod', decr($1))')')
forloop_arg(0, pos, `findpod')
define(`items', pod)
define(`finditems', `ifelse(eval(mem(eval(items - 3)) > items), 1,
  `define(`items', eval(items - 4))$0()')')
finditems()
ifelse(mem(hull).mem(goal).mem(incr(pod)), hullstr.goalstr.podstr, `',
  `errprintn(`unable to locate essential rooms/items')')
define(`_dir', `ifelse($1, 0111, ``south'', $1, 1011, ``west'', $1, 1101,
  ``north'', $1, 1110, ``east'', `oops()')')
define(`dir', _dir(eval(!mem(eval(goal + 3)))eval(!mem(eval(goal +
  4)))eval(!mem(eval(goal + 5)))eval(!mem(eval(goal + 6)))))
# And now for the hack: rewire to grab all non-function items, and hard-wire
# north of the hull breach to warp to the pressure-sensitive goal room
define(`nitems', 0)
define(`sortitems', `forloop_arg(0, 12, `_$0')')
define(`_sortitems', `ifelse(mem(eval(items + $1 * 4 + 3)), 0, `insert(
  eval(items + $1 * 4), eval(mem(eval(items + $1 * 4 + 2)) - $1 - 28))')')
define(`insert', `_$0(nitems, decr(nitems), $@)define(`nitems', incr(nitems))')
define(`_insert', `ifelse($1, 0, `$0_(0, $3, $4)', `ifelse(eval($4 < weight$2),
  1, `$0_($1, item$2, weight$2)$0($2, decr($2), $3, $4)', `$0_($1, $3, $4)')')')
define(`_insert_', `define(`item$1', $2)define(`weight$1', $3)')
sortitems()
define(`grabsafe', `forloop_arg(0, decr(nitems), `grab')')
define(`grab', `pushdef(`mem'item$1, -1)')
define(`ungrab', `pushdef(`mem'item$1, 0)')
ifdef(`nowarp', `', `
  define(`current', decr(nitems))
  grab(current)
  pushdef(`rebalance', `ifelse(current, -1, `oops()', $1, 1,
    `ungrab(current)')define(`current',  decr(current))grab(current)')
  pushdef(`mem'eval(hull + 3), goal)
  pushdef(`canned', `pushdef(`script', ``north'')')
  pushdef(`adjust', `pushdef(`script', dquote(defn(`dir')))')
  pushdef(`brute', `forloop_arg(0, nitems, `adjust')')
')
ifdef(`nobrute', `oneshot(`brute')')

ifdef(`interactive', `', `canned()brute()loop()')

run(0)
divert(-1)

# Debugging aids
define(`findmem', `pushdef(`_$0', `ifelse(mem($'`1), $1,
  $'`1`nl()')')forloop_arg(ifelse($2, `', 0, $2), pos, `_$0')popdef(`_$0')')
define(`isascii', `eval($1 == 10 || ($1 >= 32 && $1 < 128))')
define(`findstr', `_$0($1, mem($1), mem(incr($1)), mem(incr(incr($1))),
  mem(eval($1 + 3)))')
define(`_findstr', `ifelse(eval($2 > 0 && $2 + $1 <= pos &&
  isascii(eval($3 + $2)) && isascii(eval($4 + $2 + 1)) &&
  isascii(eval($5 + $2 + 2))), 1,
  `$1: dump($2, $2, incr($1))nl()')')
define(`dump', `ifelse($1, 0, `',
  `_$0(eval($2 + mem($3)))$0(decr($1), incr($2), incr($3))')')
define(`_dump', `ifelse($1, 10, ``\n'', isascii($1), 1,
  `changequote(<<, >>)format(<<<<%c>>>>, $1)changequote`'',
  `?oneshot(`dump')')')

define(`showroom', `_$0($1, mem($1), mem(incr($1)), mem(incr(incr($1))),
  mem(eval($1 + 3)), mem(eval($1 + 4)), mem(eval($1 + 5)), mem(eval($1 + 6)))')
define(`_showroom', `Room $1:nl() findstr($2) findstr($3) func: $4nl(
  ) n: $5nl() e: $6nl() s: $7nl() w: $8nl()')

define(`showitem', `_$0($1, mem($1), mem(incr($1)), mem(incr(incr($1))),
  mem(eval($1 + 3)), mem(eval($1 + 4)))')
define(`_showitem', `Item $1:nl() findstr($3) room: $2nl() weight: $4nl(
  ) func: $5nl()')

divert`'ifdef(`interactive', `', part1)
