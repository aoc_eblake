divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day18.input] day18.m4
# Optionally use -Dverbose=[12] to see some progress
# Optionally use -Donly=[12] to skip a part
# Optionally use -Dalgo=dynamic|astar to compare path-finding algorithms
# Optionally use -Dpriority=0|1|2|3|4|5 to choose priority queue algorithms

include(`common.m4')ifelse(common(18), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

# Careful: this code assumes that there are no single-letter macro names.
# Exploit fact that on initial read, every line is a comment, so even if
# keys `d', `n', `l' are adjacent, we don't eat the rest of the line.
define(`input', include(defn(`file')))
define(`input', translit(dquote(defn(`input')), `#@', `=_'))
ifdef(`algo', `', `define(`algo', `dynamic')')

define(`careful', `_careful(`$0')`$0'')
define(`_careful', `output(0, `unquoted use of single letter $1')')
define(`door', eval(1 << 26))
define(`val', `ifelse(`$1', ., 0, `val$1')')
define(`lookup', `defn(`lookup'eval($1))')
define(`makeval', `_$0(substr($1, $2, 1), eval((1 << $2) | $3))')
define(`_makeval', `define(`val$1', $2)define(`lookup$2', `$1')define(`$1',
  defn(`careful'))')
forloop(0, 25, `makeval(`abcdefghijklmnopqrstuvwxyz', ', `, 0)')
forloop(0, 25, `makeval(`ABCDEFGHIJKLMNOPQRSTUVWXYZ', ', `, door)')
define(`val_', 0)
define(`_', defn(`careful'))
define(`allkeys', 0)
define(`firstbit', `($1 & ~($1 - 1))')
define(`foreach_key', `ifelse($1, 0, `', `$2(lookup(firstbit($1)),
  `$3', `$4', `$5', `$6', `$7')$0(eval($1 - firstbit($1)), `$2', `$3', `$4',
  `$5', `$6', `$7')')')

define(`x_', 0)
define(`y_', 0)
define(`parse', `_$0(substr(defn(`input'), $1, 1))')
define(`_parse', `ifelse(`$1', nl, `define(`y_', incr(y_))define(`x_', 0)',
  `ifelse(`$1', =, `', `point(`$1')')define(`x_', incr(x_))')')
define(`point', `define(`g'x_`_'y_, `$1')ifelse(`$1', ., `', `$1', =, `',
  `define(`pos$1', x_`,'y_)ifelse(eval(val$1 & door), door, `',
  `define(`allkeys', eval(allkeys | val$1))define(`near$1', val$1)')')')
pushdef(`_careful')
forloop_arg(0, decr(decr(len(defn(`input')))), `parse')
popdef(`_careful')
define(`x_', decr(x_))
output(1, `parse complete')

# Assume coordinates of @ divide quadrant (true for puzzle, false for example)
define(`getq', `eval(($1 > $3) + 2 * ($2 > $4))')
define(`moveU', `$1, decr($2), `U'')
define(`moveR', `incr($1), $2, `R'')
define(`moveD', `$1, incr($2), `D'')
define(`moveL', `decr($1), $2, `L'')
define(`scans', 0)
define(`scan', `output(2, `scanning from $1')forloop_var(`x', 0, x_,
  `forloop_var(`y', 0, y_, `ifdef(`g'x`_'y, `define(`dist'x`_'y,
  999999)')')')ifelse(`$1', `_', `', `define(`q$1', getq(pos$1,
  pos_))')pushdef(`seen', 0)_$0(pos$1, `', 0, `$1')popdef(`seen')')
define(`_scan', `ifdef(`g$1_$2', `define(`scans', incr(scans))visit($@)')')
define(`visit', `ifelse(eval(dist$1_$2 < $4), 1, `',
  `define(`dist$1_$2', $4)pushdef(`seen', seen)ifelse(_$0(defn(`g$1_$2'),
  val(defn(`g$1_$2')), $4, `$5'), `', `ifelse(`$3', `D', `', `_scan(moveU($1,
  $2), incr($4), `$5')')ifelse(`$3', `L', `', `_scan(moveR($1, $2), incr($4),
  `$5')')ifelse(`$3', `U', `', `_scan(moveD($1, $2), incr($4), `$5')')ifelse(
  `$3', `R', `', `_scan(moveL($1, $2), incr($4), `$5')')')popdef(`seen')')')
define(`_visit', `ifelse($2, 0, `', `$1', `$4', `', eval($2 & door), door,
  `define(`seen', eval(seen | ($2 & ~door)))', `define(`path$4$1',
  $3)define(`seen$4$1', seen)define(`near$4', eval(near$4 | $2))-')')

foreach_key(allkeys, `scan')
scan(`_')
output(2, `scans:'scans)

define(`until', `$2`'ifelse($1, 0, `$0($@)')')
define(`closure', `foreach_key(eval(near$1 & ~near_), `_$0', $@)')
define(`_closure', `foreach_key(eval(near_ & ~val$2), `merge',
  $@)define(`near_', eval(near_ | val$1))define(`path_$1', 0)')
define(`merge', `ifdef(`path$1$2', `', `define(`path$1$2', eval(path$1$3 +
  path$2$3))define(`path$2$1', path$1$2)define(`seen$1$2', eval(seen$1$3 |
  seen$2$3 | val$3))define(`seen$2$1', seen$1$2)define(`near$1', eval(near$1 |
  val$2))define(`near$2', eval(near$2 | val$1))define(`q$2', q$3)')')
until(`eval(allkeys == near_)', `foreach_key(near_, `closure')')

ifelse(eval(verbose >= 3), 1, `
define(`show', ``$1'')
define(`decode', `foreach_key($1, `show')')
define(`temp', `output(3, `key $1')foreach_key(eval(allkeys - val$1),
  `_$0', `$1')')
define(`_temp', `output(3, ` to $1: 'path$2$1` via 'decode(seen$2$1))')
foreach_key(allkeys, `temp')
')

define(`hit', 0)
define(`miss', 0)
define(`iter', 0)
define(`progress', `define(`$1', incr($1))ifelse(eval((hit + miss + iter) %
  10000), 0, `output(2, `progress:'eval(hit + miss + iter))')')
define(`reach0', `ifdef(`seen$3$1', `eval(seen$3$1 & ~$2)')')
define(`reach1', `ifdef(`seen$4$1', `eval(seen$4$1 & ~$2)')')
define(`reach2', `ifdef(`seen$5$1', `eval(seen$5$1 & ~$2)')')
define(`reach3', `ifdef(`seen$6$1', `eval(seen$6$1 & ~$2)')')
define(`path0', `ifelse(`$3', `_', `eval(path$3$1 - 2)', `path$3$1')')
define(`path1', `ifelse(`$4', `_', `eval(path$4$1 - 2)', `path$4$1')')
define(`path2', `ifelse(`$5', `_', `eval(path$5$1 - 2)', `path$5$1')')
define(`path3', `ifelse(`$6', `_', `eval(path$6$1 - 2)', `path$6$1')')
define(`path4', `path$3$1')
define(`next0', ``$1', `$4', `$5', `$6'')
define(`next1', ``$3', `$1', `$5', `$6'')
define(`next2', ``$3', `$4', `$1', `$6'')
define(`next3', ``$3', `$4', `$5', `$1'')
define(`square', `ifelse(forloop_var(`x', decr($1), incr($1), `forloop_var(`y',
  decr($2), incr($2), `_$0')'), `...._....', 1, 0)')
define(`_square', `defn(`g'x`_'y)')
define(`changed', `ifelse(`$1', `$6', `', `0, `$6'`'')ifelse(`$2', `$7', `',
  `1, `$7'`'')ifelse(`$3', `$8', `', `2, `$8'`'')ifelse(`$4', `$9', `',
  `3, `$9'`'')')

ifelse(algo, astar, `
output(1, `Using A* algorithm')

include(`priority.m4')

define(`heur', `pushdef(`b0', 0)pushdef(`b1', 0)pushdef(`b2', 0)pushdef(`b3',
  0)foreach_key($3, `_$0', `', `$4', `$5', `$6', `$7')eval(b0 + b1 + b2 +
  b3)popdef(`b0', `b1', `b2', `b3')')
define(`_heur', `check(norm(q$1), path(q$1)($@))')
define(`check', `ifelse(eval($2 > b$1), 1, `define(`b$1', $2)')')

define(`addwork', `define(`g$4$5$6$7$3', ``$2', `$1'')_$0(eval($2 + heur($@)),
  `$3', `$4', `$5', `$6', `$7')')
define(`_addwork', `define(`f$3$4$5$6$2', $1)progress(`hit')insert($@)')
define(`distance', `addwork(`', 0, $@)loop(pop)clearall()define(`c$2$3$4$5$1',
  defn(`final'))popdef(`final')')
define(`loop', `ifelse(eval($1 > f$3$4$5$6$2), 1, `progress(`miss')loop(pop)',
  $2, 0, `$1pushdef(`final', `$3$4$5$6$2')', `progress(`iter')foreach_key($2, `_$0', $2, `$3', `$4', `$5',
  `$6')loop(pop)')')
define(`_loop', `ifelse(reach(q$1)(`$1', eval(allkeys - $2), `$3', `$4',
  `$5', `$6'), 0, `decide(`$3$4$5$6$2', eval(first(g$3$4$5$6$2) +
  path(q$1)($@)), eval($2 - val$1), next(q$1)($@))')')
define(`decide', `ifelse(eval($2 < ifdef(`g$4$5$6$7$3', `first(g$4$5$6$7$3)',
  999999)), 1, `addwork($@)')')

define(`follow', `ifdef(`c$2$3$4$5$1', `_$0(defn(`c$2$3$4$5$1'),
  len(`$2$3$4$5'))')')
define(`_follow', `$0_(shift(g$1), `$1', $2)')
define(`_follow_', `ifelse(`$1', `', `', `_follow(`$1', $3)lookup(substr(`$1',
  $3) - substr(`$2', $3))')')

', algo, dynamic, `
output(1, `Using dynamic programming algorithm')

define(`better', `ifelse(eval($1 < first(best)), 1, `define(`best',
  `$@')')')
define(`distance', `ifelse($1, 0, 0, ifdef(`c$2$3$4$5$1', 1), 1,
  `progress(`hit')first(c$2$3$4$5$1)', `progress(`miss')pushdef(`best',
  999999)foreach_key($1, `_$0', $@)define(`c$2$3$4$5$1',
  defn(`best'))first(best)popdef(`best')')')
define(`_distance', `ifelse(reach(q$1)(`$1', eval(allkeys - $2), `$3', `$4',
  `$5', `$6'), 0, `better(eval(path(q$1)($@) + distance(eval($2 - val$1),
  next(q$1)($@))), next(q$1)($@))')')

define(`follow', `ifdef(`c$2$3$4$5$1', `_$0(changed(`$2', `$3', `$4', `$5',
  c$2$3$4$5$1), $@)')')
define(`_follow', ``$2'follow(eval($3 - val$2), next$1(shift($@)))')

', `output(0, `unknown algorithm')m4exit(1)')

ifelse(ifdef(`only', only, 1), 1, `
output(1, `starting part 1')
define(`norm', `0')
define(`reach', `reach0')
define(`path', `path4')
define(`next', `next0')
define(`part1', distance(allkeys, `_'))
output(2, `hits:'hit` misses:'miss` iters:'iter)
')

ifelse(square(pos_), 0, `define(`part2', ``grid not suitable for part2'')',
ifdef(`only', only, 2), 2, `
output(1, `starting part 2')
define(`hit', 0)
define(`miss', 0)
define(`iter', 0)
define(`norm', `$1')
define(`reach', `reach$1')
define(`path', `path$1')
define(`next', `next$1')
define(`part2', distance(allkeys, `_', `_', `_', `_'))
output(2, `hits:'hit` misses:'miss` iters:'iter)
')

divert`'part1`'ifelse(verbose, 0, `', ` (follow(allkeys, `_'))')
part2`'ifelse(verbose, 0, `', ` (follow(allkeys, `_', `_', `_', `_'))')
