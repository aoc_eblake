divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day9.input] day9.m4

include(`intcode.m4')ifelse(intcode(9), `ok', `',
`errprint(`Missing IntCode initialization
')m4exit(1)')

parse(input)
define(`try', `
  save()
  define(`read', $1)
  define(`write', `define(part$1, $'`1)')
  run(0)
  restore()')
try(1)
try(2)
divert`'part1
part2
