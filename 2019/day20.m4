divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day20.input] day20.m4
# Optionally use -Dverbose=[12] to see some progress
# Optionally use -Donly=[12] to skip a part
# Optionally use -Dalgo=dfs|astar to compare path-finding algorithms
# Optionally use -Dpriority=0|1|2|3|4|5 to choose priority queue algorithms

include(`common.m4')ifelse(common(20), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`input', include(defn(`file')))
define(`input', translit(dquote(defn(`input')), `#', `='))
ifdef(`algo', `', `define(`algo', `astar')')

define(`x_', 0)
define(`y_', 0)
define(`portals', 0)
define(`moveu', ``u', $1, decr($2)')
define(`mover', ``r', incr($1), $2')
define(`moved', ``d', $1, incr($2)')
define(`movel', ``l', decr($1), $2')
define(`parse', `_$0(substr(defn(`input'), $1, 1))')
define(`_parse', `ifelse(`$1', nl, `define(`y_', incr(y_))define(`x_', 0)',
  `ifelse(`$1', =, `', `$1', ` ', `', `point(`$1')')define(`x_', incr(x_))')')
define(`point', `define(`g'x_`_'y_, `$1')ifelse(`$1', ., `', y_, 0, `',
  x_, 0, `', `_$0(`$1', defn(`g'decr(x_)`_'y_), `h')_$0($1,
  defn(`g'x_`_'decr(y_)), `v')')')
define(`_point', `ifelse(`$2', `', `', `$2', ., `', `pushdef(`portal',
  ``$2$1','x_`,'y_`,$3')define(`portals', incr(portals))')')
forloop_arg(0, decr(decr(len(defn(`input')))), `parse')
define(`x_', decr(x_))
define(`classify', `ifdef(`portal', `_$0(portal`'popdef(`portal'))$0()')')
define(`_classify', `$0$4(`$1', ifelse(eval($3 == 1 || $3 == y_ ||
  $2 == 1 || $2 == x_), 1, ``o',`i'', ``i',`o''), $2, $3)')
define(`_classifyh', `make(`$1', `$2', `$3', ifelse(defn(`g'incr($4)`_$5'),
  `.', `$4, $5, `r'', `decr($4), $5, `l''))')
define(`_classifyv', `make(`$1', `$2', `$3', ifelse(defn(`g$4_'incr($5)),
  `.', `$4, $5, `d'', `$4, decr($5), `u''))')
define(`make', `define(`$1$2', dquote(`$1', `$3', move$6($4, $5)))define(
  `g$4_$5', `$1$3')ifelse(`$1', `ZZ', `', `$2', `o', `define(`ports',
  ``$1','defn(`ports'))')')
classify()
define(`portals', eval(portals / 2))
define(`width', eval(len(translit(substr(defn(`input'), eval(y_ / 2 * (x_ + 1)),
  incr(x_)), ` ABCDEFGHIJKLMNOPQRSTUVWXYZ'nl)) / 2))
output(1, `parse complete, 'eval(portals - 1)` portal pairs beyond endpoints')

define(`resetprog', `define(`scans', 0)define(`hit', 0)define(`miss',
  0)define(`iter', 0)')
define(`progress', `define(`$1', incr($1))ifelse('verbose`, 0, `',
  `_$0(eval(scans + hit + miss + iter))')')
define(`_progress', `ifelse(eval($1 % 10000), 0, `output(2, `progress:$1')')')
resetprog()

ifelse(algo, dfs, `
output(1, `Using DFS algorithm')

define(`scan', `resetprog()pushdef(`best', 999999)_$0(shift(shift(AAo)), `$1',
  0)best(popdef(`best'))')
define(`_scan', `ifdef(`g$2_$3', `progress(`scans')visit($@, defn(`g$2_$3'))')')
define(`visit', `ifelse(eval($5 > best), 1, `', `$6', `ZZi', `_$0(`$4', $5)',
  `$6', `AAi', `', ifdef(`dist$2_$3_$4', `eval(dist$2_$3_$4 < $5)'), 1, `',
  `$6', `.', `define(`dist$2_$3_$4', $5)ifelse(`$1', `d', `', `_scan(moveu($2,
  $3), `$4', incr($5))')ifelse(`$1', `l', `', `_scan(mover($2, $3), `$4',
  incr($5))')ifelse(`$1', `u', `', `_scan(moved($2, $3), `$4', incr(
  $5))')ifelse(`$1', `r', `', `_scan(movel($2, $3), `$4', incr($5))')',
  `_scan(ifelse(`$4', `_', `move1', `move2')($6, `$4'), $5)')')
define(`_visit', `ifelse(ifelse(`$1', `_', 0, `$1'), 0, `define(`best',
  decr($2))')')
define(`move1', ``$3', $4, $5, `$6'')
define(`move2', `ifelse(`$6$2', `0o', ``', 0, 0, 0', `$6$2', portals`i',
  ``', 0, 0, $6', ``$3', $4, $5, ifelse(`$2', `i', `incr', `decr')($6)')')

define(`showpath')

', algo, astar, `
output(1, `Using A* algorithm')

include(`priority.m4')

# Assumption: no cycles except through portals, thus DFS preprocessing works
define(`scans', 0)
define(`findpath', `_$0(shift(shift($1i)), `$1i', 0)_$0(shift(shift($1o)),
  `$1o', 0)')
define(`_findpath', `ifelse(`$1', `', `', `ifdef(`g$2_$3',
  `progress(`scans')visit($@, defn(`g$2_$3'))')')')
define(`visit', `ifelse(`$6', `AAi', `', `$6', `.', `ifelse(`$1', `d', `',
  `_findpath(moveu($2, $3), `$4', incr($5))')ifelse(`$1', `l', `',
  `_findpath(mover($2, $3), `$4', incr($5))')ifelse(`$1', `u', `',
  `_findpath(moved($2, $3), `$4', incr($5))')ifelse(`$1', `r', `',
  `_findpath(movel($2, $3), `$4', incr($5))')', `_visit(`$4', `$6', $5)')')
define(`_visit', `define(`p$1$2', $3)define(`n$1', ``$2','defn(`n$1'))')
foreach(`findpath', ports)
output(2, `scans:'scans)

define(`heur', `ifelse(`$1', `_', 0, `eval($1 * ''width``)')')

define(`addwork', `define(`g$4$5', ``$3', `$1', `$2'')_$0(eval($3 + heur(`$5')),
  `$4', `$5')')
define(`_addwork', `define(`f$2$3', $1)progress(`hit')insert($@)')
define(`scan', `resetprog()addwork(`', `', 0, `AAo', `$1')loop(pop)clearall()')
define(`loop', `ifelse($1, `', ``no path'', `ifelse(eval($1 + 0 > f$2$3), 1,
  `progress(`miss')loop(pop)', `$2', `ZZi', `decr($1)',
  `progress(`iter')_foreach(`_$0(', `, $@)', `', n$2)loop(pop)')')')
define(`_loop', `ifelse(`$1', `', `', `decide(`$3', `$4', eval(first(g$3$4) +
  p$3$1), `$4', `$1', $1)')')
define(`decide', `ifelse(ifelse(`$4', `_', 1, `$4', portals, 0, `$4$5', `0ZZi',
  1, `$5', `ZZi', 0, `$4$7', `0o', 0, 1), 0, `', `_$0(`$1', `$2', $3, `$5',
  ifelse(`$4', `_', ``_'', `$5', `ZZi', 0, `$7', `o', `decr($4)',
  `incr($4)'))')')
define(`_decide', `ifelse(eval($3 < ifdef(`g$4$5', `first(g$4$5)', 999999)),
  1, `addwork($@)')')

define(`follow', `ifdef(`g$1$2', `_$0(`$1', g$1$2)')')
define(`_follow', `ifelse(`$3', `', ``$1'', `follow(`$3', `$4')`,$1'')')
ifelse(verbose, 0, `define(`showpath')', `define(`showpath', ` (follow(`ZZi',
  `$1'))')')

', `output(0, `unknown algorithm')m4exit(1)')

ifelse(ifdef(`only', only, 1), 1, `
define(`part1', scan(`_'))
output(2, `scans:'scans` iter':iter` hits:'hit` miss:'miss)
')

ifelse(ifdef(`only', only, 2), 2, `
define(`part2', scan(0))
output(2, `scans:'scans` iter':iter` hits:'hit` miss:'miss)
')

divert`'part1`'showpath(`_')
part2`'showpath(0)
