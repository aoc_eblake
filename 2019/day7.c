#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>

#define LIMIT 2000
static int orig[LIMIT];
static int len;
struct state {
  int id;
  int a[LIMIT];
  int pc;
  bool has_in;
  int in;
  bool has_out;
  int out;
  int steps;
};
struct state amps[5];
static int max;

static int debug_level = -1;
static void
debug_raw(int level, const char *fmt, ...) {
  va_list ap;
  if (debug_level < 0)
    debug_level = atoi(getenv("DEBUG") ?: "0");
  if (debug_level >= level) {
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
  }
}
#define debug(...) debug_raw(1, __VA_ARGS__)

static void
dump(struct state *s) {
  if (debug_level < 2)
    return;
  debug(" state of id=%d: pc=%d in=%s%d out=%s%d steps=%d\n", s->id, s->pc,
        s->has_in ? "" : "!", s->in, s->has_out ? "" : "!", s->out, s->steps);
  for (int i = 0; i < len; i++)
    debug_raw(3, "%d,", s->a[i]);
  debug_raw(3, "\n");
}

static void __attribute__((noreturn))
crash(struct state *s, const char *msg) {
  printf("invalid program id=%d, pc=%d: %s\n", s->id, s->pc, msg);
  exit(1);
}

static int
get(struct state *s, int param) {
  int op = s->a[s->pc];
  int scale = 10;
  int mode;
  int value;

  if (s->pc + param > len)
    crash(s, "program too short for opcode");
  value = s->a[s->pc + param];
  while (param--)
    scale *= 10;
  mode = (op / scale) % 10;
  debug_raw(3, "get op=%d mode=%d value=%d\n", op, mode, value);
  switch (mode) {
  case 0:
    if (value > len || value < 0)
      crash(s, "in position mode, param beyond end of program");
    return s->a[value];
  case 1:
    return value;
  default:
    crash(s, "unexpected mode");
  }
}

static void
put(struct state *s, int param, int value) {
  int op = s->a[s->pc];
  int scale = 10;
  int mode;
  int offset;

  if (s->pc + param > len)
    crash(s, "program too short for opcode");
  offset = s->a[s->pc + param];
  while (param--)
    scale *= 10;
  mode = (op / scale) % 10;
  debug_raw(3, "put op=%d mode=%d value=%d offset=%d\n", op, mode, value, offset);
  switch (mode) {
  case 0:
    if (offset > len)
      crash(s, "in position mode, param beyond end of program");
    s->a[offset] = value;
    return;
  default:
    crash(s, "unexpected mode");
  }
}

static int
run(struct state *s) {
  int jump;

  while (1) {
    debug_raw(2, "executing id=%d step=%d pc=%d %d,%d,%d,%d\n", s->id,
              s->steps++, s->pc, s->a[s->pc], s->a[s->pc+1], s->a[s->pc+2],
              s->a[s->pc+3]);
    if (s->pc > len || s->pc < 0)
      crash(s, "program ran out of bounds");
    switch (s->a[s->pc] % 100) {
    case 1:
      put(s, 3, get(s, 1) + get(s, 2));
      jump = 4;
      break;
    case 2:
      put(s, 3, get(s, 1) * get(s, 2));
      jump = 4;
      break;
    case 3:
      if (!s->has_in) {
        debug_raw(2, "id=%d stalling for input\n", s->id);
        return -1;
      }
      put(s, 1, s->in);
      s->has_in = false;
      jump = 2;
      break;
    case 4:
      if (s->has_out)
        printf("program id=%d overwriting output %d\n", s->id, s->out);
      s->has_out = true;
      s->out = get(s, 1);
      jump = 2;
      break;
    case 5:
      if (get(s, 1)) {
        s->pc = get(s, 2);
        jump = 0;
      } else
        jump = 3;
      break;
    case 6:
      if (!get(s, 1)) {
        s->pc = get(s, 2);
        jump = 0;
      } else
        jump = 3;
      break;
    case 7:
      put(s, 3, get(s, 1) < get(s, 2));
      jump = 4;
      break;
    case 8:
      put(s, 3, get(s, 1) == get(s, 2));
      jump = 4;
      break;
    case 99:
      debug_raw(2, "id=%d halting\n", s->id);
      return 0;
    default:
      crash(s, "unexpected opcode");
    }
    s->pc += jump;
    dump(s);
  }
}

static void
doit (int *p)
{
  int data = 0, i;
  bool done = false, has_out = true;

  debug("computing %d,%d,%d,%d,%d...\n", p[0], p[1], p[2], p[3], p[4]);
  for (i = 0; i < 5; i++) {
    memset(&amps[i], 0, sizeof amps[i]);
    amps[i].id = i;
    memcpy(amps[i].a, orig, len * sizeof orig[0]);
    amps[i].has_in = true;
    amps[i].in = p[i];
    if (run(&amps[i]) != -1 || amps[i].has_in)
      crash(&amps[i], "program finished too soon");
  }

  while (!done) {
    done = true;
    for (i = 0; i < 5; i++) {
      if (amps[i].has_in)
        printf("program id=%d skipping input %d\n", amps[i].id, amps[i].in);
      amps[i].has_in = has_out;
      amps[i].in = data;
      if (run(&amps[i]) == -1)
        done = false;
      else if (!amps[i].has_out)
        printf("program id=%d completed with stale output\n", amps[i].id);
      has_out = amps[i].has_out;
      data = amps[i].out;
      amps[i].has_out = false;
    }
  }
  data = amps[4].out;
  debug("...%d\n", data);
  if (data > max)
    max = data;
}

static void
permute(int *p, int size, int n)
{
  int i, t;
  if (size == 1) {
    doit(p);
    return;
  }
  permute(p, size - 1, n);
  for (i = 0; i < size - 1; i++) {
    if (size & 1) {
      t = p[0];
      p[0] = p[size - 1];
      p[size - 1] = t;
    } else {
      t = p[i];
      p[i] = p[size - 1];
      p[size - 1] = t;
    }
    permute(p, size - 1, n);
  }
}

int
main(int argc, char **argv) {
  int i;
  int phase[] = { 0, 1, 2, 3, 4 };

  debug("");
  if (argc > 1)
    if (!(stdin = freopen(argv[1], "r", stdin))) {
      perror("failure");
      exit(2);
    }

  while (scanf("%d%*[,\n]", &i) == 1) {
    orig[len++] = i;
    if (len > LIMIT - 3) {
      printf("recompile with larger LIMIT\n");
      exit(2);
    }
  }
  printf("Read %u slots\n", len);
  memcpy(amps[0].a, orig, len * sizeof orig[0]);
  dump(&amps[0]);

  permute(phase, 5, 5);
  printf("part 1 best output %d\n", max);

  for (i = 0; i < 5; i++)
    phase[i] += 5;
  max = 0;
  permute(phase, 5, 5);
  printf("part 2 best output %d\n", max);

  return 0;
}
