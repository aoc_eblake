divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day3.input] day3.m4

include(`common.m4')ifelse(common(3), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`offset', 20000)
define(`path',
  `define(`l', 0)define(`x', offset)define(`y', offset)foreach(`segment', $@)')
define(`segment', `_$0(substr($1, 0, 1), substr($1, 1))')
define(`_segment', `visit(l, x, y, move($1, $2)x, y)')
define(`move', `define(`l', eval(l + $2))move$1($2)')
define(`moveU', `define(`y', eval(y - $1))')
define(`moveR', `define(`x', eval(x + $1))')
define(`moveD', `define(`y', eval(y + $1))')
define(`moveL', `define(`x', eval(x - $1))')

define(`input', quote(include(defn(`file'))))
define(`segments', 0)
define(`visit', `define(`s'segments, `$*')define(`segments', incr(segments))')
path(substr(defn(`input'), 0, index(defn(`input'), `
')))
define(`part1', l)
define(`part2', eval(l * 2))
define(`s', `s$1')
define(`between',
  `eval(($1 < $2 && $3 > $1 && $3 < $2) || ($3 > $2 && $3 < $1))')
define(`ninth', `$9')
define(`abs', `ifelse(eval($1 > 0), 1, $1, -($1))')
define(`visit', `forloop(0, decr(segments), `_$0(s(', `), $@)')')
define(`_visit', `ifelse(ifelse(
  $2.$8, $4.ninth(shift($@)), `between($7, $9, $2).between($3, $5, $8)',
  $3.$7, $5.$9, `between($2, $4, $7).between($8, ninth(shift($@)), $3)'),
  1.1, `intersect($@)')')
define(`intersect', `ifelse($2, $4,
  `_$0($2 - offset, $8 - offset, $1 + abs($8 - $3), $6 + abs($2 - $7))',
  `_$0($7 - offset, $3 - offset, $1 + abs($7 - $2), $6 + abs($3 - $8))')')
define(`_intersect', `check(1, $1, $2)check(2, $3, $4)')
define(`check', `_$0(eval(abs($2) + abs($3)), `part$1')')
define(`_check', `ifelse(eval($1 < $2), 1, `define(`$2', $1)')')
path(translit(quote(substr(defn(`input'), index(defn(`input'), `
'))), `
'))

divert`'part1
part2
