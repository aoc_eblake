divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day23.input] day23.m4
# Optionally use -Dverbose=[12] to see some progress

include(`intcode.m4')ifelse(intcode(23), `ok', `',
`errprint(`Missing IntCode initialization
')m4exit(1)')

parse(input)
define(`prep', `oneshot(`io', defn(`pause_on_read'))
  oneshot(`io', defn(`run_after_read'))
  oneshot(`read', $1)copy($1)runs($1)')
forloop_arg(0, 49, `prep')

define(`read', -1)
define(`write', `define(`data', $1)')
define(`swap', `ifdef(`$1', `pushdef(`$2', defn(`$1'))popdef(`$1')$0($@)')')
define(`consume', `
  swap(`q$1y', `t')oneshot(`read', t)popdef(`t')swap(`t', `q$1y')
  swap(`q$1x', `t')oneshot(`read', t)popdef(`t')swap(`t', `q$1x')')
define(`io', `ifelse($1, 0,
  `ifdef(`q's`x', `consume(s)oneshot(`io', defn(`run_after_read'))
  oneshot(`io', defn(`run_after_read'))run(pc)', `oneshot(`io')run($2)')',
  `define(`a', data)ifelse(a, 255, `',
  `oneshot(`check', a)')oneshot(`io',
  defn(`pause_after_write'))run($2)pushdef(`q'a`x', data)oneshot(`io',
  defn(`pause_after_write'))run(pc)pushdef(`q'a`y', data)run(pc)')')
define(`i', 0)define(`j', 0)
define(`iter', `output(1, `iteration '$1)define(`$1', incr($1))')
define(`_loop', `ifdef(`check', `runs(check)_loop()')')
define(`loop', `iter(`i')_$0()ifdef(`part1', `', `define(`part1',
  q255y)')ifelse(part2, q255y, `oneshot(`loop')', `define(`part2',
  q255y)')pushdef(`q0x', q255x)pushdef(`q0y', q255y)oneshot(`check', 0)loop()')
forloop_arg(0, 49, `runs')
forloop_arg(0, 49, `runs')
loop()

divert`'part1
part2
