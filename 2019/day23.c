#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <limits.h>
#include <ctype.h>

static int debug_level = -1;
static void
debug_init(void) {
  if (debug_level < 0)
    debug_level = atoi(getenv("DEBUG") ?: "0");
}

static void __attribute__((format(printf, 2, 3)))
debug_raw(int level, const char *fmt, ...) {
  va_list ap;
  if (debug_level >= level) {
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
  }
}
#define debug(...) debug_raw(1, __VA_ARGS__)

static int __attribute__((noreturn)) __attribute__((format(printf, 1, 2)))
die(const char *fmt, ...)
{
  va_list ap;
  va_start(ap, fmt);
  vprintf(fmt, ap);
  va_end(ap);
  putchar('\n');
  exit(1);
}

#define QLEN 20
struct q {
  int64_t x, y;
};

#define LIMIT 10000
static int64_t orig[LIMIT];
static int len;
struct state {
  int id;
  int64_t a[LIMIT];
  int pc;
  int base;
  bool has_in;
  int64_t in;
  bool has_out;
  int64_t out;
  int steps;

  struct q q[QLEN];
  int head, tail;
};

static struct state s[50];

static void
dump(struct state *s) {
  if (debug_level < 2)
    return;
  debug(" state of id=%d: pc=%d base=%d in=%s%" PRId64 " out=%s%" PRId64
        " steps=%d\n", s->id, s->pc, s->base,
        s->has_in ? "" : "!", s->in, s->has_out ? "" : "!", s->out, s->steps);
  for (int i = 0; i < len; i++)
    debug_raw(3, "%" PRId64 ",", s->a[i]);
  debug_raw(3, "\n");
}

static void __attribute__((noreturn))
crash(struct state *s, const char *msg) {
  printf("invalid program id=%d, pc=%d: %s\n", s->id, s->pc, msg);
  exit(1);
}

static int64_t
get(struct state *s, int param) {
  int64_t op = s->a[s->pc];
  int scale = 10;
  int mode;
  int64_t value;

  if (op > 99999 || op < 0)
    crash(s, "unexpected opcode");
  if (s->pc + param > LIMIT)
    crash(s, "memory too short for opcode");
  value = s->a[s->pc + param];
  while (param--)
    scale *= 10;
  mode = (op / scale) % 10;
  debug_raw(3, "get op=%" PRId64 " mode=%d value=%" PRId64 "\n",
            op, mode, value);
  switch (mode) {
  case 0:
    if (value > LIMIT || value < 0)
      crash(s, "in position mode, param beyond memory");
    return s->a[value];
  case 1:
    return value;
  case 2:
    value += s->base;
    if (value > LIMIT || value < 0)
      crash(s, "in relative mode, param beyond memory");
    return s->a[value];
  default:
    crash(s, "unexpected mode");
  }
}

static void
put(struct state *s, int param, int64_t value) {
  int64_t op = s->a[s->pc];
  int scale = 10;
  int mode;
  int64_t offset;

  if (op > 99999 || op < 0)
    crash(s, "unexpected opcode");
  if (s->pc + param > LIMIT)
    crash(s, "memory too short for opcode");
  offset = s->a[s->pc + param];
  while (param--)
    scale *= 10;
  mode = (op / scale) % 10;
  debug_raw(3, "put op=%" PRId64 " mode=%d value=%" PRId64 " offset=%" PRId64
            "\n", op, mode, value, offset);
  switch (mode) {
  case 0:
    if (offset > LIMIT || offset < 0)
      crash(s, "in position mode, param beyond memory");
    s->a[offset] = value;
    return;
  case 2:
    offset += s->base;
    if (offset > LIMIT || offset < 0)
      crash(s, "in relative mode, param beyond memory");
    s->a[offset] = value;
    return;
  default:
    crash(s, "unexpected mode");
  }
}

static void
init(struct state *s, int64_t in) {
  memset(s, 0, sizeof  *s);
  memcpy(s->a, orig, len * sizeof orig[0]);
  s->has_in = true;
  s->in = in;
  s->id = in;
  dump(s);
}

/* Returns -1 for stalled on input, 0 for done, 1 for stalled after output */
static int
run(struct state *s) {
  int jump;

  while (1) {
    debug_raw(2, "executing id=%d step=%d pc=%d base=%d %" PRId64
              ",%" PRId64 ",%" PRId64 ",%" PRId64 "\n", s->id,
              s->steps++, s->pc, s->base, s->a[s->pc], s->a[s->pc+1],
              s->a[s->pc+2], s->a[s->pc+3]);
    if (!(s->steps % 10000))
      debug(" steps=%d\n", s->steps);
    if (s->pc > LIMIT || s->pc < 0)
      crash(s, "program ran out of bounds");
    switch (s->a[s->pc] % 100) {
    case 1:
      put(s, 3, get(s, 1) + get(s, 2));
      jump = 4;
      break;
    case 2:
      put(s, 3, get(s, 1) * get(s, 2));
      jump = 4;
      break;
    case 3:
      if (!s->has_in) {
        debug_raw(2, "id=%d stalling for input\n", s->id);
        s->steps--;
        return -1;
      }
      put(s, 1, s->in);
      s->has_in = false;
      jump = 2;
      break;
    case 4:
      if (s->has_out) {
        debug_raw(2, "id=%d stalling for output\n", s->id);
        s->steps--;
        return 1;
      }
      s->has_out = true;
      s->out = get(s, 1);
      jump = 2;
      break;
    case 5:
      if (get(s, 1)) {
        s->pc = get(s, 2);
        jump = 0;
      } else
        jump = 3;
      break;
    case 6:
      if (!get(s, 1)) {
        s->pc = get(s, 2);
        jump = 0;
      } else
        jump = 3;
      break;
    case 7:
      put(s, 3, get(s, 1) < get(s, 2));
      jump = 4;
      break;
    case 8:
      put(s, 3, get(s, 1) == get(s, 2));
      jump = 4;
      break;
    case 9:
      s->base += get(s, 1);
      if (s->base < 0 || s->base > LIMIT)
        crash(s, "relative base out of bounds");
      jump = 2;
      break;
    case 99:
      debug_raw(2, "id=%d halting\n", s->id);
      return 0;
    default:
      crash(s, "unexpected opcode");
    }
    s->pc += jump;
    dump(s);
  }
}

int
main(int argc, char **argv) {
  int i;
  bool done = false, idle;
  struct q q255, *q;
  int64_t addr;
  bool part1 = false;
  int count = 0;

  debug_init();
  if (argc > 1 && strcmp("-", argv[1]))
    if (!(stdin = freopen(argv[1], "r", stdin))) {
      perror("failure");
      exit(2);
    }

  while (scanf("%" SCNd64 "%*[,\n]", &orig[len]) == 1)
    if (len++ > LIMIT - 3)
      die("recompile with larger LIMIT");
  printf("Read %u slots\n", len);

  for (i = 0; i < 50; i++) {
    init(&s[i], i);
    if (run(&s[i]) != -1)
      crash(&s[i], "unexpected program behavior");
  }
  while (!done) {
    idle = true;
    for (i = 0; i < 50 && !done; i++) {
      if (!s[i].has_in) {
        if (s[i].head < s[i].tail) {
          s[i].in = s[i].q[s[i].head].x;
          idle = false;
        } else
          s[i].in = -1;
        s[i].has_in = true;
      }
      switch (run(&s[i])) {
      case -1:
        if (s[i].in != -1) {
          debug("program %d consuming %" PRId64 ",%" PRId64 "\n",
                i, s[i].q[s[i].head].x, s[i].q[s[i].head].y);
          s[i].in = s[i].q[s[i].head].y;
          s[i].head++;
          s[i].has_in = true;
          if (!run(&s[i]))
            crash(&s[i], "unexpected completion");
        }
        break;
      case 0: crash(&s[i], "unexpected completion");
      case 1:
        addr = s[i].out;
        idle = false;
        if (addr == 255) {
          q = &q255;
        } else if (addr < 50ULL) {
          q = &s[addr].q[s[addr].tail];
          if (++s[addr].tail == QLEN)
            crash(&s[addr], "recompile with larger QLEN");
        } else
          crash(&s[i], "addr out of bounds");
        s[i].has_out = false;
        if (run(&s[i]) != 1)
          crash(&s[i], "expecting output sequence");
        q->x = s[i].out;
        s[i].has_out = false;
        if (!run(&s[i]) || !s[i].has_out)
          crash(&s[i], "expecting output sequence");
        q->y = s[i].out;
        s[i].has_out = false;
        debug("program %d sending %" PRId64 ",%" PRId64 " to %" PRId64 "\n",
              i, q->x, q->y, addr);
        if (!part1 && addr == 255) {
          part1 = true;
          printf("first value to 255: %" PRId64 ", %" PRId64 "\n",
                 q255.x, q255.y);
        }
      }
    }
    if (idle) {
      count++;
      debug("detected idle, NAT contains %" PRId64 ", %" PRId64 "\n",
             q255.x, q255.y);
      for (i = 0; i < 50; i++)
        s[i].head = s[i].tail = 0;
      s[0].tail++;
      if (q255.y == s[0].q[0].y)
        done = true;
      s[0].q[0] = q255;
    }
  }
  printf("NAT repeated %" PRId64 ", %" PRId64 " after %d idle detections\n",
         q255.x, q255.y, count);
  return 0;
}
