#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <limits.h>
#include <ctype.h>

static int debug_level = -1;
static void
debug_init(void) {
  if (debug_level < 0)
    debug_level = atoi(getenv("DEBUG") ?: "0");
}

static void __attribute__((format(printf, 2, 3)))
debug_raw(int level, const char *fmt, ...) {
  va_list ap;
  if (debug_level >= level) {
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
  }
}
#define debug(...) debug_raw(1, __VA_ARGS__)

static int __attribute__((noreturn)) __attribute__((format(printf, 1, 2)))
die(const char *fmt, ...)
{
  va_list ap;
  va_start(ap, fmt);
  vprintf(fmt, ap);
  va_end(ap);
  putchar('\n');
  exit(1);
}

#define LIMIT 10000
static int64_t orig[LIMIT];
static int len;
struct state {
  int id;
  int64_t a[LIMIT];
  int pc;
  int base;
  bool has_in;
  int64_t in;
  bool has_out;
  int64_t out;
  int steps;
};
static struct state s;

static void
dump(struct state *s) {
  if (debug_level < 2)
    return;
  debug(" state of id=%d: pc=%d base=%d in=%s%" PRId64 " out=%s%" PRId64
        " steps=%d\n", s->id, s->pc, s->base,
        s->has_in ? "" : "!", s->in, s->has_out ? "" : "!", s->out, s->steps);
  for (int i = 0; i < len; i++)
    debug_raw(3, "%" PRId64 ",", s->a[i]);
  debug_raw(3, "\n");
}

static void __attribute__((noreturn))
crash(struct state *s, const char *msg) {
  printf("invalid program id=%d, pc=%d: %s\n", s->id, s->pc, msg);
  exit(1);
}

static int64_t
get(struct state *s, int param) {
  int64_t op = s->a[s->pc];
  int scale = 10;
  int mode;
  int64_t value;

  if (op > 99999 || op < 0)
    crash(s, "unexpected opcode");
  if (s->pc + param > LIMIT)
    crash(s, "memory too short for opcode");
  value = s->a[s->pc + param];
  while (param--)
    scale *= 10;
  mode = (op / scale) % 10;
  debug_raw(3, "get op=%" PRId64 " mode=%d value=%" PRId64 "\n",
            op, mode, value);
  switch (mode) {
  case 0:
    if (value > LIMIT || value < 0)
      crash(s, "in position mode, param beyond memory");
    return s->a[value];
  case 1:
    return value;
  case 2:
    value += s->base;
    if (value > LIMIT || value < 0)
      crash(s, "in relative mode, param beyond memory");
    return s->a[value];
  default:
    crash(s, "unexpected mode");
  }
}

static void
put(struct state *s, int param, int64_t value) {
  int64_t op = s->a[s->pc];
  int scale = 10;
  int mode;
  int64_t offset;

  if (op > 99999 || op < 0)
    crash(s, "unexpected opcode");
  if (s->pc + param > LIMIT)
    crash(s, "memory too short for opcode");
  offset = s->a[s->pc + param];
  while (param--)
    scale *= 10;
  mode = (op / scale) % 10;
  debug_raw(3, "put op=%" PRId64 " mode=%d value=%" PRId64 " offset=%" PRId64
            "\n", op, mode, value, offset);
  switch (mode) {
  case 0:
    if (offset > LIMIT || offset < 0)
      crash(s, "in position mode, param beyond memory");
    s->a[offset] = value;
    return;
  case 2:
    offset += s->base;
    if (offset > LIMIT || offset < 0)
      crash(s, "in relative mode, param beyond memory");
    s->a[offset] = value;
    return;
  default:
    crash(s, "unexpected mode");
  }
}

static void
init(struct state *s, int64_t in) {
  memset(s, 0, sizeof  *s);
  memcpy(s->a, orig, len * sizeof orig[0]);
  s->has_in = true;
  s->in = in;
  dump(s);
}

/* Returns -1 for stalled on input, 0 for done, 1 for stalled on output */
static int
run(struct state *s) {
  int jump;

  while (1) {
    debug_raw(2, "executing id=%d step=%d pc=%d base=%d %" PRId64
              ",%" PRId64 ",%" PRId64 ",%" PRId64 "\n", s->id,
              s->steps++, s->pc, s->base, s->a[s->pc], s->a[s->pc+1],
              s->a[s->pc+2], s->a[s->pc+3]);
    if (!(s->steps % 10000))
      debug(" steps=%d\n", s->steps);
    if (s->pc > LIMIT || s->pc < 0)
      crash(s, "program ran out of bounds");
    switch (s->a[s->pc] % 100) {
    case 1:
      put(s, 3, get(s, 1) + get(s, 2));
      jump = 4;
      break;
    case 2:
      put(s, 3, get(s, 1) * get(s, 2));
      jump = 4;
      break;
    case 3:
      if (!s->has_in) {
        debug_raw(2, "id=%d stalling for input\n", s->id);
        s->steps--;
        return -1;
      }
      put(s, 1, s->in);
      s->has_in = false;
      jump = 2;
      break;
    case 4:
      if (s->has_out) {
        debug_raw(2, "id=%d stalling for output\n", s->id);
        s->steps--;
        return 1;
      }
      s->has_out = true;
      s->out = get(s, 1);
      jump = 2;
      break;
    case 5:
      if (get(s, 1)) {
        s->pc = get(s, 2);
        jump = 0;
      } else
        jump = 3;
      break;
    case 6:
      if (!get(s, 1)) {
        s->pc = get(s, 2);
        jump = 0;
      } else
        jump = 3;
      break;
    case 7:
      put(s, 3, get(s, 1) < get(s, 2));
      jump = 4;
      break;
    case 8:
      put(s, 3, get(s, 1) == get(s, 2));
      jump = 4;
      break;
    case 9:
      s->base += get(s, 1);
      if (s->base < 0 || s->base > LIMIT)
        crash(s, "relative base out of bounds");
      jump = 2;
      break;
    case 99:
      debug_raw(2, "id=%d halting\n", s->id);
      return 0;
    default:
      crash(s, "unexpected opcode");
    }
    s->pc += jump;
    dump(s);
  }
}

static const char *
setup(int i) {
  static char buf[20 * 9];
  char *p = buf;
  const char *items[] = { "jam", "loom", "mug", "spool of cat6",
                          "prime number", "food ration", "fuel cell",
                          "manifold" };
  int j;

  for (j = 0; j < 8; j++) {
    if (i & (1 << j))
      p = stpcpy(p, "take ");
    else
      p = stpcpy(p, "drop ");
    p = stpcpy(p, items[j]);
    *p++ = '\n';
  }
  p = stpcpy(p, "north\n");
  return buf;
}

int
main(int argc, char **argv) {
  FILE *f;
  bool output = false, seen_bang = false;
  int i = 0;
  const char *p = "", prep[] =
    "north\n"
    "north\n"
    "west\n"
    "take mug\n"
    "east\n"
    "north\n"
    "east\n"
    "east\n"
    "take loom\n"
    "west\n"
    "west\n"
    "south\n"
    "south\n"
    "south\n"
    "east\n"
    "take food ration\n"
    "south\n"
    "take prime number\n"
    "north\n"
    "east\n"
    "take manifold\n"
    "east\n"
    "east\n"
    "take jam\n"
    "west\n"
    "north\n"
    "east\n"
    "take spool of cat6\n"
    "west\n"
    "north\n"
    "take fuel cell\n"
    "south\n"
    "south\n"
    "west\n"
    "west\n"
    "west\n"
    "north\n"
    "west\n"
    "north\n"
    "west\n"
    "north\n"
    "inv\n";

  debug_init();
  if (argc > 1 && strcmp("-", argv[1]))
    if (!(stdin = freopen(argv[1], "r", stdin))) {
      perror("failure");
      exit(2);
    }

  while (scanf("%" SCNd64 "%*[,\n]", &orig[len]) == 1)
    if (len++ > LIMIT - 3)
      die("recompile with larger LIMIT");
  printf("Read %u slots\n", len);
  f = fopen("/dev/tty", "r");
  if (!f)
    die("failed to open tty");
  if (argc <= 2)
    p = prep;

  init(&s, 0);
  s.has_in = false;
  while (run(&s) && !feof(f)) {
    if (s.has_out) {
      if (isascii(s.out)) {
        if (output)
          putchar(s.out);
        if (s.out == '!') {
          seen_bang = true;
          debug("seen bang\n");
        }
      } else
        die("unexpected output %" PRId64, s.out);
      s.has_out = false;
      continue;
    }
    if (s.has_in)
      die("expecting input to be consumed");
    if (*p)
      s.in = *p++;
    else {
      output = true;
      if (seen_bang && i <= 256) {
        p = setup(i++);
        debug("trying: %s\n", p);
        s.in = *p++;
        seen_bang = false;
      } else
        s.in = fgetc(f);
    }
    s.has_in = true;
  }
  return 0;
}
