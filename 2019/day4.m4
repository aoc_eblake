divert(-1)dnl -*- m4 -*-
# Usage: m4 { -Dlo=val -Dhi=val | -Dfile=day4.input } day4.m4
# If day4.input exists, its contents should be "lo-hi"

include(`common.m4')ifelse(common(4), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`parse', `define(`lo', $1)define(`hi', $2)')
exists(defn(`file'), `parse(translit(include(defn(`file')), `-'nl(), `,'))')
ifdef(`lo', `', `errprint(`Missing definition of lo
')m4exit(1)')
ifdef(`hi', `', `errprint(`Missing definition of hi
')m4exit(1)')
ifelse(len(lo).len(hi).eval(lo >= 100000 && lo < hi), 6.6.1, `',
  `errprint(`Inconsistent definition of lo/hi
')m4exit(1)')

define(`eq', `ifelse($1, $2, 1, 0)')
define(`check', `ifelse(eval($1$2$3$4$5$6 >= lo && $1$2$3$4$5$6 <= hi), 1,
  `_$0(0eq($1, $2)eq($2, $3)eq($3, $4)eq($4, $5)eq($5, $6)0, $1$2$3$4$5$6)')')
define(`_check', `ifelse(index($1, 1), -1, `',
  index($1, 010), -1, `define(`part1', incr(part1))',
  `define(`part2', incr(part2))')')

define(`part1', 0)
define(`part2', 0)
forloop_var(`a', 1, 9, `forloop_var(`b', a, 9, `forloop_var(`c', b, 9,
  `forloop_var(`d', c, 9, `forloop_var(`e', d, 9, `forloop_var(`f', e, 9,
  `check(a, b, c, d, e, f)')')')')')')

define(`part1', eval(part1 + part2))

divert`'part1
part2
