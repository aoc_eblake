#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <limits.h>

static int do_debug = -1;
void debug(const char *fmt, ...) {
  va_list ap;
  if (do_debug < 0)
    do_debug = !!getenv("DEBUG");
  if (do_debug > 0) {
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
  }
}

#define LIMIT 2000

struct orbit {
  char parent[4];
  char name[4];
  int idx;
  bool key;
};
struct orbit list[LIMIT] = { { "", "COM", -1 }, };

static void
dump(int count) {
  debug("\n");
  if (!do_debug)
    return;
  for (int i = 0; i < count; i++)
    debug("%s)%s %d\n", list[i].parent, list[i].name, list[i].idx);
}

int main(int argc, char **argv) {
  int i, j, count = 1, total = 0, trans = 0;
  int you = -1, san = -1, pivot = -1;

  if (argc > 1)
    if (!(stdin = freopen(argv[1], "r", stdin))) {
      perror("failure");
      exit(2);
    }

  /* Pass 1 - read relations: O(n) */
  while (scanf("%3[^)])%3s ", list[count].parent, list[count].name) == 2) {
    list[count++].idx = -1;
    if (count >= LIMIT) {
      printf("recompile with larger LIMIT\n");
      exit(2);
    }
  }
  printf("scanned %d direct relations\n", count);
  dump(count);

  /* Pass 2 - locate parents: O(n^2) */
  for (i = 1; i < count; i++) {
    for (j = 0; j < count; j++) {
      if (!strcmp(list[i].parent, list[j].name)) {
	list[i].idx = j;
	break;
      }
    }
    if (list[i].idx == -1) {
      printf("incomplete map, missing parent for %s\n", list[i].name);
      exit(1);
    }
    if (!strcmp(list[i].name, "YOU"))
      you = i;
    else if (!strcmp(list[i].name, "SAN"))
      san = i;
  }
  dump(count);

  /* Pass 3 - compute total: O(n+m) */
  for (i = 1; i < count; i++) {
    j = i;
    while (list[j].idx >= 0) {
      total++;
      if (i == you)
	list[j].key = true;
      j = list[j].idx;
      if (j == i) {
	printf("loop detected at %d %s\n", i, list[i].name);
	exit(1);
      }
    }
  }
  printf("%d total orbits\n", total);

  /* Pass 4 - count transfer orbits: O(m) */
  if (you == -1 || san == -1)
    printf("transfer computation not possible\n");
  else {
    for (j = san; !list[j].key; j = list[j].idx)
      trans++;
    pivot = j;
    for (j = you; j != pivot; j = list[j].idx)
      trans++;
    printf("%d total transfers\n", trans - 2);
  }
  return 0;
}
