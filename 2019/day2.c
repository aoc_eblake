#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>

#define LIMIT 400
static int a[LIMIT];
static int len;

static int do_debug = -1;
void debug(const char *fmt, ...) {
  va_list ap;
  if (do_debug < 0)
    do_debug = !!getenv("DEBUG");
  if (do_debug > 0) {
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
  }
}

void dump(void) {
  if (!do_debug)
    return;
  for (int i = 0; i < len; i++)
    debug("%d,", a[i]);
  debug("\n");
}

int main(int argc, char **argv) {
  int i, *pc, count = 0;
  bool done = false;

  if (argc > 1)
    if (!(stdin = freopen(argv[1], "r", stdin))) {
      perror("failure");
      exit(2);
    }

  while (scanf("%d%*[,\n]", &i) == 1) {
    a[len++] = i;
    if (len > LIMIT) {
      printf("recompile with larger LIMIT\n");
      exit(2);
    }
  }
  printf("Read %u slots\n", len);
  dump();

  if (argc > 3) {
    a[1] = atoi(argv[2]);
    a[2] = atoi(argv[3]);
  }

  pc = a;
  while (!done) {
    count++;
    if (pc[0] == 99) {
      debug("halting\n");
    } else {
      debug("executing %d,%d(%d),%d(%d),%d\n", pc[0], pc[1], a[pc[1]],
	    pc[2], a[pc[2]], pc[3]);
      if (pc - a >= len || pc[1] >= len || pc[2] >= len || pc[3] >= len) {
	printf("out-of-bounds reference at instruction %d\n", (int)(pc - a));
	exit(1);
      }
    }
    switch (*pc) {
    case 1:
      a[pc[3]] = a[pc[1]] + a[pc[2]];
      break;
    case 2:
      a[pc[3]] = a[pc[1]] * a[pc[2]];
      break;
    case 99:
      done = true;
      break;
    }
    pc += 4;
    dump();
  }
  printf("after %d opcodes, slot 0 holds %d\n", count, a[0]);
  dump();
  return 0;
}
