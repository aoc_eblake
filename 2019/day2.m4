divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day2.input] day2.m4

include(`intcode.m4')ifelse(intcode(2), `ok', `',
`errprint(`Missing IntCode initialization
')m4exit(1)')

parse(input)
no64()
define(`try', `save()define(`mem1', eval($1 / 100))define(`mem2',
  eval($1 % 100))run(0)mem0`'restore()')
define(`part1', try(1202))
# Looking at sample inputs shows a common preamble of 12 ints, then a sequence
# of instructions that each modify the previous instruction's result, usually
# by a constant, and exactly once by mem1 and mem2, culminating in a final
# write to mem0, which simplifies to a linear equation mem0 = a + mem1*b + mem2
# with a and b large enough that 2-digit mem1 and mem2 cause monotonically
# increasing results.  So we can do a binary search.
define(`goal', 19690720)
define(`search', `_$0($1, $2, eval(($1+$2)/2))')
define(`_search', `$0_($1, $3, $2, try($3))')
define(`_search_', `ifelse($4, 'goal`, $2, `search(ifelse(eval($4 < ''goal``),
  1, `incr($2), $3', `$1, decr($2)'))')')
define(`part2', search(0, 9999))

divert`'part1
part2
