#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>

#define LIMIT 10000
static int64_t orig[LIMIT];
static int len;
struct state {
  int id;
  int64_t a[LIMIT];
  int pc;
  int base;
  bool has_in;
  int64_t in;
  bool has_out;
  int64_t out;
  int steps;
};

static int debug_level = -1;
static void
debug_raw(int level, const char *fmt, ...) {
  va_list ap;
  if (debug_level < 0)
    debug_level = atoi(getenv("DEBUG") ?: "0");
  if (debug_level >= level) {
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
  }
}
#define debug(...) debug_raw(1, __VA_ARGS__)

static int __attribute__((noreturn))
die(const char *fmt, ...)
{
  va_list ap;
  va_start(ap, fmt);
  vprintf(fmt, ap);
  va_end(ap);
  putchar('\n');
  exit(1);
}

static void
dump(struct state *s) {
  if (debug_level < 2)
    return;
  debug(" state of id=%d: pc=%d base=%d in=%s%" PRId64 " out=%s%" PRId64
        " steps=%d\n", s->id, s->pc, s->base,
        s->has_in ? "" : "!", s->in, s->has_out ? "" : "!", s->out, s->steps);
  for (int i = 0; i < len; i++)
    debug_raw(3, "%" PRId64 ",", s->a[i]);
  debug_raw(3, "\n");
}

static void __attribute__((noreturn))
crash(struct state *s, const char *msg) {
  printf("invalid program id=%d, pc=%d: %s\n", s->id, s->pc, msg);
  exit(1);
}

static int64_t
get(struct state *s, int param) {
  int64_t op = s->a[s->pc];
  int scale = 10;
  int mode;
  int64_t value;

  if (op > 99999 || op < 0)
    crash(s, "unexpected opcode");
  if (s->pc + param > LIMIT)
    crash(s, "memory too short for opcode");
  value = s->a[s->pc + param];
  while (param--)
    scale *= 10;
  mode = (op / scale) % 10;
  debug_raw(3, "get op=%d mode=%d value=%d\n", op, mode, value);
  switch (mode) {
  case 0:
    if (value > LIMIT || value < 0)
      crash(s, "in position mode, param beyond memory");
    return s->a[value];
  case 1:
    return value;
  case 2:
    value += s->base;
    if (value > LIMIT || value < 0)
      crash(s, "in relative mode, param beyond memory");
    return s->a[value];
  default:
    crash(s, "unexpected mode");
  }
}

static void
put(struct state *s, int param, int64_t value) {
  int64_t op = s->a[s->pc];
  int scale = 10;
  int mode;
  int64_t offset;

  if (op > 99999 || op < 0)
    crash(s, "unexpected opcode");
  if (s->pc + param > LIMIT)
    crash(s, "memory too short for opcode");
  offset = s->a[s->pc + param];
  while (param--)
    scale *= 10;
  mode = (op / scale) % 10;
  debug_raw(3, "put op=%d mode=%d value=%d offset=%d\n", op, mode, value, offset);
  switch (mode) {
  case 0:
    if (offset > LIMIT || offset < 0)
      crash(s, "in position mode, param beyond memory");
    s->a[offset] = value;
    return;
  case 2:
    offset += s->base;
    if (offset > LIMIT || offset < 0)
      crash(s, "in relative mode, param beyond memory");
    s->a[offset] = value;
    return;
  default:
    crash(s, "unexpected mode");
  }
}

static void
init(struct state *s, int64_t in) {
  memset(s, 0, sizeof  *s);
  memcpy(s->a, orig, len * sizeof orig[0]);
  s->has_in = true;
  s->in = in;
  dump(s);
}

/* Returns -1 for stalled on input, 0 for done, 1 for stalled on output */
static int
run(struct state *s) {
  int jump;

  while (1) {
    debug_raw(2, "executing id=%d step=%d pc=%d base=%" PRId64 " %" PRId64
              ",%" PRId64 ",%" PRId64 ",%" PRId64 "\n", s->id,
              s->steps++, s->pc, s->base, s->a[s->pc], s->a[s->pc+1],
              s->a[s->pc+2], s->a[s->pc+3]);
    if (!(s->steps % 10000))
      debug(" steps=%d\n", s->steps);
    if (s->pc > LIMIT || s->pc < 0)
      crash(s, "program ran out of bounds");
    switch (s->a[s->pc] % 100) {
    case 1:
      put(s, 3, get(s, 1) + get(s, 2));
      jump = 4;
      break;
    case 2:
      put(s, 3, get(s, 1) * get(s, 2));
      jump = 4;
      break;
    case 3:
      if (!s->has_in) {
        debug_raw(2, "id=%d stalling for input\n", s->id);
        s->steps--;
        return -1;
      }
      put(s, 1, s->in);
      s->has_in = false;
      jump = 2;
      break;
    case 4:
      if (s->has_out) {
        debug_raw(2, "id=%d stalling for output\n", s->id);
        s->steps--;
        return 1;
      }
      s->has_out = true;
      s->out = get(s, 1);
      jump = 2;
      break;
    case 5:
      if (get(s, 1)) {
        s->pc = get(s, 2);
        jump = 0;
      } else
        jump = 3;
      break;
    case 6:
      if (!get(s, 1)) {
        s->pc = get(s, 2);
        jump = 0;
      } else
        jump = 3;
      break;
    case 7:
      put(s, 3, get(s, 1) < get(s, 2));
      jump = 4;
      break;
    case 8:
      put(s, 3, get(s, 1) == get(s, 2));
      jump = 4;
      break;
    case 9:
      s->base += get(s, 1);
      if (s->base < 0 || s->base > LIMIT)
        crash(s, "relative base out of bounds");
      jump = 2;
      break;
    case 99:
      debug_raw(2, "id=%d halting\n", s->id);
      return 0;
    default:
      crash(s, "unexpected opcode");
    }
    s->pc += jump;
    dump(s);
  }
}

static struct state s;

#define GRID 50
static int16_t grid[GRID][GRID]; /* -1 unvisited, -2 wall, else distance */
static int minx, maxx, miny, maxy;
enum dir { N = 1, S, W, E };

static void
display(int x1, int y1) {
  int x, y;

  if (!debug_level)
    return;
  for (y = miny; y <= maxy; y++) {
    for (x = minx; x <= maxx; x++) {
      if (x1 == x && y1 == y) {
        debug("D");
        continue;
      }
      switch (grid[y][x]) {
      case -2: debug("#"); break;
      case -1: debug(" "); break;
      case 0: debug("_"); break;
      default: debug("."); break;
      }
    }
    debug("\n");
  }
  debug("currently at %d,%d and %d steps away from origin\n",
        x1, y1, grid[y1][x1]);
}

int
main(int argc, char **argv) {
  int count = 0;
  enum dir last = E;
  int x, y, nextx, nexty;
  int visits = 0;
  int part1 = 0, part2 = 0;

  debug("");
  if (argc > 1)
    if (!(stdin = freopen(argv[1], "r", stdin))) {
      perror("failure");
      exit(2);
    }

  while (scanf("%" SCNd64 "%*[,\n]", &orig[len]) == 1)
    if (len++ > LIMIT - 3)
      die("recompile with larger LIMIT");
  printf("Read %u slots\n", len);

  init(&s, E);
  s.has_in = false;
  memset(grid, -1, sizeof grid);
  minx = maxx = x = miny = maxy = y = GRID / 2;
  grid[y][x] = 0;
  while (visits < 5) {
    count++;
    switch (run(&s)) {
    case 0:
      die("unexpected exit");
    case -1:
      if (s.has_out)
        break;
      switch (last) {
      case N:
        if (grid[y][x - 1] > -2) { s.in = W; break; }
        if (grid[y - 1][x] > -2) { s.in = N; break; }
        if (grid[y][x + 1] > -2) { s.in = E; break; }
        if (grid[y + 1][x] > -2) { s.in = S; break; }
        die("no path possible from N");
      case E:
        if (grid[y - 1][x] > -2) { s.in = N; break; }
        if (grid[y][x + 1] > -2) { s.in = E; break; }
        if (grid[y + 1][x] > -2) { s.in = S; break; }
        if (grid[y][x - 1] > -2) { s.in = W; break; }
        die("no path possible from E");
      case S:
        if (grid[y][x + 1] > -2) { s.in = E; break; }
        if (grid[y + 1][x] > -2) { s.in = S; break; }
        if (grid[y][x - 1] > -2) { s.in = W; break; }
        if (grid[y - 1][x] > -2) { s.in = N; break; }
        die("no path possible from S");
      case W:
        if (grid[y + 1][x] > -2) { s.in = S; break; }
        if (grid[y][x - 1] > -2) { s.in = W; break; }
        if (grid[y - 1][x] > -2) { s.in = N; break; }
        if (grid[y][x + 1] > -2) { s.in = E; break; }
        die("no path possible from W");
      }
      s.has_in = true;
      display(x, y);
      continue;
    }
    switch (s.in) {
    case N: nextx = x; nexty = y - 1; break;
    case S: nextx = x; nexty = y + 1; break;
    case W: nextx = x - 1; nexty = y; break;
    case E: nextx = x + 1; nexty = y; break;
    default: die("invalid input");
    }
    debug("tried %c, got %" PRId64 "\n", " NSWE"[s.in], s.out);
    if (nextx < minx)
      minx = nextx;
    if (nextx > maxx)
      maxx = nextx;
    if (nexty < miny)
      miny = nexty;
    if (nexty > maxy)
      maxy = nexty;
    if (nextx < 0 || nextx >= GRID || nexty < 0 || nexty >= GRID)
      die("recompile with larger GRID");
    switch (s.out) {
    case 0:
      grid[nexty][nextx] = -2;
      break;
    case 1:
      if (grid[nexty][nextx] < 0)
        grid[nexty][nextx] = grid[y][x] + 1;
      if (grid[nexty][nextx] > part2)
        part2 = grid[nexty][nextx];
      last = s.in;
      x = nextx;
      y = nexty;
      break;
    case 2:
      display(nextx, nexty);
      if (!visits++) {
        printf("found it at %d,%d, distance %d\n", nextx, nexty,
               grid[y][x] + 1);
        part1 = grid[y][x] + 1;
        part2 = 0;
        memset(grid, -1, sizeof grid);
        grid[nexty][nextx] = 0;
      }
      last = s.in;
      x = nextx;
      y = nexty;
      break;
    default: die("unexpected output");
    }
    s.has_out = false;
  }
  printf("%d steps to start, %d minutes to fill\n", part1, part2);
  return 0;
}
