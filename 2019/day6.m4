divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day6.input] day6.m4

include(`common.m4')ifelse(common(6), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

# The input files contain bare ), which is a pain to deal with in m4.
# We tackle this by reading input twice: on the first read, the ) in the
# input terminates our define() early, but we are diverting to -1, and
# the rest of the input is harmless.  On the second read, we redefine
# comments to begin with the start of input and end with double newline
# (one in the file, one that we supply), thus reading the entire input as
# a single comment, at which point we can then post-process as desired
# into a saner form.  At least the input does not also contain characters
# like [(`',] or macro names, which would have made this worse.

ifdef(`file', `', `errprint(`Missing definition of file
')m4exit(1)')
define(`input', include(defn(`file'))
changecom(defn(`input'), `

')
define(`input', include(defn(`file'))
)
changecom(`#')
define(`input', translit(dquote(defn(`input')), `)
', `.,'))

define(`add', `ifelse($1, `', `', `_$0(translit($1, `.', `,'))')')
define(`_add', `pushdef(`nodes', `$2')define(`n$2', `$1')')
foreach(`add', input)

define(`part1', 0)
define(`dCOM', 0)
define(`loop', `ifdef(`nodes', `define(`part1',
  eval(part1 + chase(defn(`nodes'))))popdef(`nodes')loop()')')
define(`chase', `ifdef(`d$1', `', `define(`d$1',
  incr(chase(defn(`n$1'))))')d$1')
loop()

define(`part2', ``part2 not possible'')
define(`merge', `ifdef(`n$1', `ifdef(`m$1', `define(`part2',
  eval(dYOU + dSAN - 2 - 2 * d$1))', `define(`m$1')merge(defn(`n$1'))')')')
ifelse(ifdef(`dYOU', 1).ifdef(`dSAN', 1), 1.1, `merge(`YOU')merge(`SAN')')

divert`'part1
part2
