#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>

static int debug_level = -1;
static void __attribute__((unused))
debug_raw(int level, const char *fmt, ...) {
  va_list ap;
  if (debug_level < 0)
    debug_level = atoi(getenv("DEBUG") ?: "0");
  if (debug_level >= level) {
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
  }
}
#define debug(...) debug_raw(1, __VA_ARGS__)

struct point {
  int px, py, pz;
  int vx, vy, vz;
};
static struct point a[4], orig[4];
static int perx, pery, perz;

static void
dump(int step) {
  int i;

  printf("After %d steps:\n", step);
  for (i = 0; i < 4; i++)
    printf("pos=<x=%3d, y=%3d, z=%3d>, vel=<x=%3d, y=%3d, z=%3d>\n",
           a[i].px, a[i].py, a[i].pz, a[i].vx, a[i].vy, a[i].vz);
}

static void
gravity_one(int one, int two) {
  if (a[one].px < a[two].px) {
    a[one].vx++;
    a[two].vx--;
  } else if (a[one].px > a[two].px) {
    a[one].vx--;
    a[two].vx++;
  }
  if (a[one].py < a[two].py) {
    a[one].vy++;
    a[two].vy--;
  } else if (a[one].py > a[two].py) {
    a[one].vy--;
    a[two].vy++;
  }
  if (a[one].pz < a[two].pz) {
    a[one].vz++;
    a[two].vz--;
  } else if (a[one].pz > a[two].pz) {
    a[one].vz--;
    a[two].vz++;
  }
}

static void
gravity(void) {
  gravity_one(0, 1);
  gravity_one(0, 2);
  gravity_one(0, 3);
  gravity_one(1, 2);
  gravity_one(1, 3);
  gravity_one(2, 3);
}

static void
velocity(void) {
  int i;

  for (i = 0; i < 4; i++) {
    a[i].px += a[i].vx;
    a[i].py += a[i].vy;
    a[i].pz += a[i].vz;
  }
}

static void
energy(int i) {
  int t = 0;

  printf("after %d steps, ", i);
  for (i = 0; i < 4; i++)
    t += (abs(a[i].px) + abs(a[i].py) + abs(a[i].pz)) *
      (abs(a[i].vx) + abs(a[i].vy) + abs(a[i].vz));
  printf("energy is %d\n", t);
}

static struct collision {
  int x;
  int px;
  int nx;
  int y;
  int py;
  int ny;
  int z;
  int pz;
  int nz;
} collide[6];

static void check_collide(struct point *p1, struct point *p2,
                          struct collision *c, int i) {
  if (!perx && p1->px == p2->px && i % 6 == 5) {
    if (c->x)
      c->nx++;
    else {
      c->x = i;
      c->px = p1->px;
    }
  }
  if (!pery && p1->py == p2->py && i % 6 == 5) {
    if (c->y)
      c->ny++;
    else {
      c->y = i;
      c->py = p1->py;
    }
  }
  if (!perz && p1->pz == p2->pz && i % 6 == 5) {
    if (c->z)
      c->nz++;
    else {
      c->z = i;
      c->pz = p1->pz;
    }
  }
  if (p1->px == p2->px && p1->py == p2->py && p1->pz == p2->pz) {
    printf("collision occurred!");
    exit(1);
  }
}

static bool
check(int i) {
  check_collide(&a[0], &a[1], &collide[0], i);
  check_collide(&a[0], &a[2], &collide[1], i);
  check_collide(&a[0], &a[3], &collide[2], i);
  check_collide(&a[1], &a[2], &collide[3], i);
  check_collide(&a[1], &a[3], &collide[4], i);
  check_collide(&a[2], &a[3], &collide[5], i);
  if (!perx && !(a[0].vx | a[1].vx | a[2].vx | a[3].vx) &&
      a[0].px == orig[0].px && a[1].px == orig[1].px &&
      a[2].px == orig[2].px && a[3].px == orig[3].px) {
    perx = i;
  }
  if (!pery && !(a[0].vy | a[1].vy | a[2].vy | a[3].vy) &&
      a[0].py == orig[0].py && a[1].py == orig[1].py &&
      a[2].py == orig[2].py && a[3].py == orig[3].py) {
    pery = i;
  }
  if (!perz && !(a[0].vz | a[1].vz | a[2].vz | a[3].vz) &&
      a[0].pz == orig[0].pz && a[1].pz == orig[1].pz &&
      a[2].pz == orig[2].pz && a[3].pz == orig[3].pz) {
    perz = i;
  }
  return perx && pery && perz;
}

static long long
gcd(long long one, long long two) {
  if (one == two)
    return one;
  if (one > two)
    return gcd(one - two, two);
  return gcd(one, two - one);
}

int main(int argc, char **argv) {
  int i, limit = 1000000, progress;

  debug("");
  if (argc > 1 && strcmp(argv[1], "-"))
    if (!(stdin = freopen(argv[1], "r", stdin))) {
      perror("failure");
      exit(2);
    }

  if (argc > 2)
    limit = atoi(argv[2]);
  if (argc > 3)
    progress = atoi(argv[3]);
  else
    progress = limit / 10;

  for (i = 0; i < 4; i++)
    if (scanf("<x=%d, y=%d, z=%d>\n", &a[i].px, &a[i].py, &a[i].pz) != 3) {
      printf("unexpected input\n");
      exit(2);
    }
  memcpy(orig, a, sizeof a);
  for (i = 0; i < limit; i++) {
    if (argc > 2) {
      if (!(i % progress))
        dump(i);
    } else if (i == 1000)
      energy(i);
    if (i && check(i))
      break;
    gravity();
    velocity();
  }
  if (argc > 2) {
    dump(i);
    energy(i);
  }
  if (perx && pery && perz) {
    long long tmp;
    printf("cycles at %d %d %d\n", perx, pery, perz);
    tmp = perx / gcd(perx, pery) * pery;
    printf("overall at %lld\n", tmp / gcd(tmp, perz) * perz);
  } else
    printf("must run longer to find cycles\n");

  for (i = 0; i < 6; i++)
    if (collide[i].x && collide[i].y && collide[i].z)
      debug("potential collision in pair %d? %d(+%d)@%d, %d(+%d)@%d, "
            "%d(+%d)@%d\n", i,
            collide[i].x, collide[i].nx, collide[i].px,
            collide[i].y, collide[i].ny, collide[i].py,
            collide[i].z, collide[i].nz, collide[i].pz);
  return 0;
}
