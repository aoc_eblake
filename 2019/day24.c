#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <ctype.h>
#include <limits.h>

static int debug_level = -1;
static void
debug_init(void) {
  if (debug_level < 0)
    debug_level = atoi(getenv("DEBUG") ?: "0");
}

static void __attribute__((format(printf, 2, 3)))
debug_raw(int level, const char *fmt, ...) {
  va_list ap;
  if (debug_level < 0)
    debug_level = atoi(getenv("DEBUG") ?: "0");
  if (debug_level >= level) {
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
  }
}
#define debug(...) debug_raw(1, __VA_ARGS__)

static int __attribute__((noreturn)) __attribute__((format(printf, 1, 2)))
die(const char *fmt, ...)
{
  va_list ap;
  va_start(ap, fmt);
  vprintf(fmt, ap);
  va_end(ap);
  putchar('\n');
  exit(1);
}

#define LIMIT 200
static bool seen[1<<25];
static int chain[2][LIMIT + 3];

static void
dump(int gen, int val) {
  int i;

  debug("at generation/level %d 0x%x:\n", gen, val);
  for (i = 0; i < 25; i++) {
    debug("%c", ".#"[!!(val & (1 << i))]);
    if (i % 5 == 4)
      debug("\n");
  }
}

static int
generation1(int val) {
  unsigned long long l = 0;
  int i, m, t;

  for (i = 0; i < 5; i++)
    l |= (val & (0x1fULL << (5 * i))) << (8 + 2 * i);
  for (val = i = 0; i < 25; i++) {
    m = i / 5 * 7 + i % 5;
    t = __builtin_popcountll(l & (0x8382ULL << m));
    val |= (t == 2 || t == 2 - !(l & (1ULL << (m + 8)))) << i;
  }
  return val;
}

static int
generation2_one(int *p) {
  int r = 0, t;

  if (!(p[-1] | p[0] | p[1]))
    return 0;

  /* 1: [-1]12, [-1]8, [0]2, [0]6 */
  t = 4 - (!(p[-1] & (1 << 11)) + !(p[-1] & (1 << 7)) +
           !(p[0] & (1 << 1)) + !(p[0] & (1 << 5)));
  r |= (t == 1 || t == 1 + !(p[0] & (1 << 0))) << 0;

  /* 2: [0]1, [-1]8, [0]3, [0]7 */
  t = 4 - (!(p[0] & (1 << 0)) + !(p[-1] & (1 << 7)) +
           !(p[0] & (1 << 2)) + !(p[0] & (1 << 6)));
  r |= (t == 1 || t == 1 + !(p[0] & (1 << 1))) << 1;

  /* 3: [0]2, [-1]8, [0]4, [0]8 */
  t = 4 - (!(p[0] & (1 << 1)) + !(p[-1] & (1 << 7)) +
           !(p[0] & (1 << 3)) + !(p[0] & (1 << 7)));
  r |= (t == 1 || t == 1 + !(p[0] & (1 << 2))) << 2;

  /* 4: [0]3, [-1]8, [0]5, [0]9 */
  t = 4 - (!(p[0] & (1 << 2)) + !(p[-1] & (1 << 7)) +
           !(p[0] & (1 << 4)) + !(p[0] & (1 << 8)));
  r |= (t == 1 || t == 1 + !(p[0] & (1 << 3))) << 3;

  /* 5: [0]4, [-1]8, [-1]14, [0]10 */
  t = 4 - (!(p[0] & (1 << 3)) + !(p[-1] & (1 << 7)) +
           !(p[-1] & (1 << 13)) + !(p[0] & (1 << 9)));
  r |= (t == 1 || t == 1 + !(p[0] & (1 << 4))) << 4;

  /* 6: [-1]12, [0]1, [0]7, [0]11 */
  t = 4 - (!(p[-1] & (1 << 11)) + !(p[0] & (1 << 0)) +
           !(p[0] & (1 << 6)) + !(p[0] & (1 << 10)));
  r |= (t == 1 || t == 1 + !(p[0] & (1 << 5))) << 5;

  /* 7: [0]6, [0]2, [0]8, [0]12 */
  t = 4 - (!(p[0] & (1 << 5)) + !(p[0] & (1 << 1)) +
           !(p[0] & (1 << 7)) + !(p[0] & (1 << 11)));
  r |= (t == 1 || t == 1 + !(p[0] & (1 << 6))) << 6;

  /* 8: [0]7, [0]3, [0]9, [1]1, [1]2, [1]3, [1]4, [1]5 */
  t = 8 - (!(p[0] & (1 << 6)) + !(p[0] & (1 << 2)) +
           !(p[0] & (1 << 8)) + !(p[1] & (1 << 0)) +
           !(p[1] & (1 << 1)) + !(p[1] & (1 << 2)) +
           !(p[1] & (1 << 3)) + !(p[1] & (1 << 4)));
  r |= (t == 1 || t == 1 + !(p[0] & (1 << 7))) << 7;

  /* 9: [0]8, [0]4, [0]10, [0]14 */
  t = 4 - (!(p[0] & (1 << 7)) + !(p[0] & (1 << 3)) +
           !(p[0] & (1 << 9)) + !(p[0] & (1 << 13)));
  r |= (t == 1 || t == 1 + !(p[0] & (1 << 8))) << 8;

  /* 10: [0]9, [0]5, [-1]14, [0]15 */
  t = 4 - (!(p[0] & (1 << 8)) + !(p[0] & (1 << 4)) +
           !(p[-1] & (1 << 13)) + !(p[0] & (1 << 14)));
  r |= (t == 1 || t == 1 + !(p[0] & (1 << 9))) << 9;

  /* 11: [-1]12, [0]6, [0]12, [0]16 */
  t = 4 - (!(p[-1] & (1 << 11)) + !(p[0] & (1 << 5)) +
           !(p[0] & (1 << 11)) + !(p[0] & (1 << 15)));
  r |= (t == 1 || t == 1 + !(p[0] & (1 << 10))) << 10;

  /* 12: [0]11, [0]7, [1]1, [1]6, [1]11, [1]16, [1]21, [0]17 */
  t = 8 - (!(p[0] & (1 << 10)) + !(p[0] & (1 << 6)) +
           !(p[1] & (1 << 0)) + !(p[1] & (1 << 5)) +
           !(p[1] & (1 << 10)) + !(p[1] & (1 << 15)) +
           !(p[1] & (1 << 20)) + !(p[0] & (1 << 16)));
  r |= (t == 1 || t == 1 + !(p[0] & (1 << 11))) << 11;

  /* 14: [1]5, [1]10, [1]15, [1]20, [1]25, [0]9, [0]15, [0]19 */
  t = 8 - (!(p[1] & (1 << 4)) + !(p[1] & (1 << 9)) +
           !(p[1] & (1 << 14)) + !(p[1] & (1 << 19)) +
           !(p[1] & (1 << 24)) + !(p[0] & (1 << 8)) +
           !(p[0] & (1 << 14)) + !(p[0] & (1 << 18)));
  r |= (t == 1 || t == 1 + !(p[0] & (1 << 13))) << 13;

  /* 15: [0]14, [0]10, [-1]14, [0]20 */
  t = 4 - (!(p[0] & (1 << 13)) + !(p[0] & (1 << 9)) +
           !(p[-1] & (1 << 13)) + !(p[0] & (1 << 19)));
  r |= (t == 1 || t == 1 + !(p[0] & (1 << 14))) << 14;

  /* 16: [-1]12, [0]11, [0]17, [0]21 */
  t = 4 - (!(p[-1] & (1 << 11)) + !(p[0] & (1 << 10)) +
           !(p[0] & (1 << 16)) + !(p[0] & (1 << 20)));
  r |= (t == 1 || t == 1 + !(p[0] & (1 << 15))) << 15;

  /* 17: [0]16, [0]12, [0]18, [0]22 */
  t = 4 - (!(p[0] & (1 << 15)) + !(p[0] & (1 << 11)) +
           !(p[0] & (1 << 17)) + !(p[0] & (1 << 21)));
  r |= (t == 1 || t == 1 + !(p[0] & (1 << 16))) << 16;

  /* 18: [0]17, [1]21, [1]22, [1]23, [1]24, [1]25, [0]19, [0]23 */
  t = 8 - (!(p[0] & (1 << 16)) + !(p[1] & (1 << 20)) +
           !(p[1] & (1 << 21)) + !(p[1] & (1 << 22)) +
           !(p[1] & (1 << 23)) + !(p[1] & (1 << 24)) +
           !(p[0] & (1 << 18)) + !(p[0] & (1 << 22)));
  r |= (t == 1 || t == 1 + !(p[0] & (1 << 17))) << 17;

  /* 19: [0]18, [0]14, [0]20, [0]24 */
  t = 4 - (!(p[0] & (1 << 17)) + !(p[0] & (1 << 13)) +
           !(p[0] & (1 << 19)) + !(p[0] & (1 << 23)));
  r |= (t == 1 || t == 1 + !(p[0] & (1 << 18))) << 18;

  /* 20: [0]19, [0]15, [-1]14, [0]25 */
  t = 4 - (!(p[0] & (1 << 18)) + !(p[0] & (1 << 14)) +
           !(p[-1] & (1 << 13)) + !(p[0] & (1 << 24)));
  r |= (t == 1 || t == 1 + !(p[0] & (1 << 19))) << 19;

  /* 21: [-1]12, [0]16, [0]22, [-1]18 */
  t = 4 - (!(p[-1] & (1 << 11)) + !(p[0] & (1 << 15)) +
           !(p[0] & (1 << 21)) + !(p[-1] & (1 << 17)));
  r |= (t == 1 || t == 1 + !(p[0] & (1 << 20))) << 20;

  /* 22: [0]21, [0]17, [0]23, [-1]18 */
  t = 4 - (!(p[0] & (1 << 20)) + !(p[0] & (1 << 16)) +
           !(p[0] & (1 << 22)) + !(p[-1] & (1 << 17)));
  r |= (t == 1 || t == 1 + !(p[0] & (1 << 21))) << 21;

  /* 23: [0]22, [0]18, [0]24, [-1]18 */
  t = 4 - (!(p[0] & (1 << 21)) + !(p[0] & (1 << 17)) +
           !(p[0] & (1 << 23)) + !(p[-1] & (1 << 17)));
  r |= (t == 1 || t == 1 + !(p[0] & (1 << 22))) << 22;

  /* 24: [0]23, [0]19, [0]25, [-1]18 */
  t = 4 - (!(p[0] & (1 << 22)) + !(p[0] & (1 << 18)) +
           !(p[0] & (1 << 24)) + !(p[-1] & (1 << 17)));
  r |= (t == 1 || t == 1 + !(p[0] & (1 << 23))) << 23;

  /* 25: [0]24, [0]20, [-1]14, [-1]18 */
  t = 4 - (!(p[0] & (1 << 23)) + !(p[0] & (1 << 19)) +
           !(p[-1] & (1 << 13)) + !(p[-1] & (1 << 17)));
  r |= (t == 1 || t == 1 + !(p[0] & (1 << 24))) << 24;

  return r;
}

static int
generation2(int g) {
  int i;
  int t = 0;

  for (i = 1; i <= LIMIT + 1; i++)
    t += __builtin_popcount(chain[1 - g % 2][i] =
                            generation2_one(&chain[g % 2][i]));
  return t;
}

int
main(int argc, char **argv) {
  int ch, i = 0, val = 0;
  int limit = LIMIT;

  debug_init();
  if (argc > 1 && strcmp(argv[1], "-"))
    if (!(stdin = freopen(argv[1], "r", stdin))) {
      perror("failure");
      exit(2);
    }
  if (argc > 2) {
    limit = atoi(argv[2]);
    if (limit % 2 || limit > LIMIT)
      die("need even limit less than %d", LIMIT);
  }

  while ((ch = getchar()) != EOF) {
    if (ch == '\n')
      continue;
    val |= (ch == '#') << i++;
  }
  if (i != 25)
    die("incorrect input");
  chain[0][limit / 2 + 1] = val;

  dump(0, val);
  for (i = 0; i < limit; i++) {
    seen[val] = true;
    val = generation1(val);
    if (seen[val])
      break;
    dump(i + 1, val);
  }
  //  if (i == LIMIT)
  //    die("recompile with larger LIMIT");
  printf("part1: first repeating generation %d has score %d\n", i, val);

  dump(-1, chain[0][limit / 2]);
  dump(0, chain[0][limit / 2 + 1]);
  dump(1, chain[0][limit / 2 + 2]);
  for (i = 0; i < limit; i++) {
    val = generation2(i);
    dump(-1, chain[1 - i % 2][limit / 2]);
    dump(0, chain[1 - i % 2][limit / 2 + 1]);
    dump(1, chain[1 - i % 2][limit / 2 + 2]);
  }
  printf("part2: after %d generations, there are %d bugs\n", i, val);
  return 0;
}
