divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day13.input] day13.m4

include(`intcode.m4')ifelse(intcode(13), `ok', `',
`errprint(`Missing IntCode initialization
')m4exit(1)')

parse(input)
define(`part1', 0)
define(`write', `define(`data', $1)')
define(`done', `oneshot(`loop')')
save()
define(`loop', `
  oneshot(`io', defn(`pause_after_write'))
  oneshot(`io', defn(`run_after_write'))
  oneshot(`io', defn(`run_after_write'))
  run()
  ifelse(data, 2, `define(`part1', incr(part1))')
  loop()')
loop()
restore()

define(`part2', 0)
define(`pad', 0)
define(`ball', 0)
define(`read', `eval((ball > pad) - (ball < pad))')
define(`mem0', 2)
define(`io', `define(`pc', $2)ifelse($1, 0, `run($2)')')
define(`loop', `
  run()
  define(`x', data)
  run()
  define(`y', data)
  run()
  ifelse(x.y, -1.0, `define(`part2', data)output(1, `score:'data)',
    data, 3, `define(`pad', x)', data, 4, `define(`ball', x)')
  loop()')
loop()

divert`'part1
part2
