#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <limits.h>
#include <ctype.h>

#define LIMIT 10000
static int64_t orig[LIMIT];
static int len;
struct state {
  int id;
  int64_t a[LIMIT];
  int pc;
  int base;
  bool has_in;
  int64_t in;
  bool has_out;
  int64_t out;
  int steps;
};

static int debug_level = -1;
static void
debug_init(void) {
  if (debug_level < 0)
    debug_level = atoi(getenv("DEBUG") ?: "0");
}

static void __attribute__((format(printf, 2, 3)))
debug_raw(int level, const char *fmt, ...) {
  va_list ap;
  if (debug_level >= level) {
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
  }
}
#define debug(...) debug_raw(1, __VA_ARGS__)

static int __attribute__((noreturn)) __attribute__((format(printf, 1, 2)))
die(const char *fmt, ...)
{
  va_list ap;
  va_start(ap, fmt);
  vprintf(fmt, ap);
  va_end(ap);
  putchar('\n');
  exit(1);
}

static void
dump(struct state *s) {
  if (debug_level < 2)
    return;
  debug(" state of id=%d: pc=%d base=%d in=%s%" PRId64 " out=%s%" PRId64
        " steps=%d\n", s->id, s->pc, s->base,
        s->has_in ? "" : "!", s->in, s->has_out ? "" : "!", s->out, s->steps);
  for (int i = 0; i < len; i++)
    debug_raw(3, "%" PRId64 ",", s->a[i]);
  debug_raw(3, "\n");
}

static void __attribute__((noreturn))
crash(struct state *s, const char *msg) {
  printf("invalid program id=%d, pc=%d: %s\n", s->id, s->pc, msg);
  exit(1);
}

static int64_t
get(struct state *s, int param) {
  int64_t op = s->a[s->pc];
  int scale = 10;
  int mode;
  int64_t value;

  if (op > 99999 || op < 0)
    crash(s, "unexpected opcode");
  if (s->pc + param > LIMIT)
    crash(s, "memory too short for opcode");
  value = s->a[s->pc + param];
  while (param--)
    scale *= 10;
  mode = (op / scale) % 10;
  debug_raw(3, "get op=%" PRId64 " mode=%d value=%" PRId64 "\n",
            op, mode, value);
  switch (mode) {
  case 0:
    if (value > LIMIT || value < 0)
      crash(s, "in position mode, param beyond memory");
    return s->a[value];
  case 1:
    return value;
  case 2:
    value += s->base;
    if (value > LIMIT || value < 0)
      crash(s, "in relative mode, param beyond memory");
    return s->a[value];
  default:
    crash(s, "unexpected mode");
  }
}

static void
put(struct state *s, int param, int64_t value) {
  int64_t op = s->a[s->pc];
  int scale = 10;
  int mode;
  int64_t offset;

  if (op > 99999 || op < 0)
    crash(s, "unexpected opcode");
  if (s->pc + param > LIMIT)
    crash(s, "memory too short for opcode");
  offset = s->a[s->pc + param];
  while (param--)
    scale *= 10;
  mode = (op / scale) % 10;
  debug_raw(3, "put op=%" PRId64 " mode=%d value=%" PRId64 " offset=%" PRId64
            "\n", op, mode, value, offset);
  switch (mode) {
  case 0:
    if (offset > LIMIT || offset < 0)
      crash(s, "in position mode, param beyond memory");
    s->a[offset] = value;
    return;
  case 2:
    offset += s->base;
    if (offset > LIMIT || offset < 0)
      crash(s, "in relative mode, param beyond memory");
    s->a[offset] = value;
    return;
  default:
    crash(s, "unexpected mode");
  }
}

static void
init(struct state *s, int64_t in) {
  memset(s, 0, sizeof  *s);
  memcpy(s->a, orig, len * sizeof orig[0]);
  s->has_in = true;
  s->in = in;
  dump(s);
}

/* Returns -1 for stalled on input, 0 for done, 1 for stalled on output */
static int
run(struct state *s) {
  int jump;

  while (1) {
    debug_raw(2, "executing id=%d step=%d pc=%d base=%d %" PRId64
              ",%" PRId64 ",%" PRId64 ",%" PRId64 "\n", s->id,
              s->steps++, s->pc, s->base, s->a[s->pc], s->a[s->pc+1],
              s->a[s->pc+2], s->a[s->pc+3]);
    if (!(s->steps % 10000))
      debug(" steps=%d\n", s->steps);
    if (s->pc > LIMIT || s->pc < 0)
      crash(s, "program ran out of bounds");
    switch (s->a[s->pc] % 100) {
    case 1:
      put(s, 3, get(s, 1) + get(s, 2));
      jump = 4;
      break;
    case 2:
      put(s, 3, get(s, 1) * get(s, 2));
      jump = 4;
      break;
    case 3:
      if (!s->has_in) {
        debug_raw(2, "id=%d stalling for input\n", s->id);
        s->steps--;
        return -1;
      }
      put(s, 1, s->in);
      s->has_in = false;
      jump = 2;
      break;
    case 4:
      if (s->has_out) {
        debug_raw(2, "id=%d stalling for output\n", s->id);
        s->steps--;
        return 1;
      }
      s->has_out = true;
      s->out = get(s, 1);
      jump = 2;
      break;
    case 5:
      if (get(s, 1)) {
        s->pc = get(s, 2);
        jump = 0;
      } else
        jump = 3;
      break;
    case 6:
      if (!get(s, 1)) {
        s->pc = get(s, 2);
        jump = 0;
      } else
        jump = 3;
      break;
    case 7:
      put(s, 3, get(s, 1) < get(s, 2));
      jump = 4;
      break;
    case 8:
      put(s, 3, get(s, 1) == get(s, 2));
      jump = 4;
      break;
    case 9:
      s->base += get(s, 1);
      if (s->base < 0 || s->base > LIMIT)
        crash(s, "relative base out of bounds");
      jump = 2;
      break;
    case 99:
      debug_raw(2, "id=%d halting\n", s->id);
      return 0;
    default:
      crash(s, "unexpected opcode");
    }
    s->pc += jump;
    dump(s);
  }
}

static struct state s;

#define GRID 80
static char grid[GRID][GRID];
static int maxx, maxy;

static void
display(void) {
  int x, y;

  if (!debug_level)
    return;
  for (y = 0; y <= maxy; y++)
    for (x = 0; x <= maxx; x++)
      debug("%c", grid[y][x]);
}

enum dir { U = '^', R = '>', D = 'v', L = '<', NONE = 0 };

static enum dir
next(int x, int y, enum dir d) {
  switch (d) {
  case U:
    if (x && grid[y][x - 1] == '#')
      return L;
    if (x < maxx && grid[y][x + 1] == '#')
      return R;
  case R:
    if (y && grid[y - 1][x] == '#')
      return U;
    if (y < maxy && grid[y + 1][x] == '#')
      return D;
    break;
  case D:
    if (x && grid[y][x - 1] == '#')
      return L;
    if (x < maxx && grid[y][x + 1] == '#')
      return R;
    break;
  case L:
    if (y && grid[y - 1][x] == '#')
      return U;
    if (y < maxy && grid[y + 1][x] == '#')
      return D;
    break;
  case NONE:
    die("unexpected case");
  }
  return NONE;
}

static int
walk(int *x, int *y, enum dir d) {
  int count = 0;
  switch (d) {
  case U:
    while (*y && grid[*y - 1][*x] == '#') {
      count++;
      --*y;
    }
    break;
  case R:
    while (*x < maxx && grid[*y][*x + 1] == '#') {
      count++;
      ++*x;
    }
    break;
  case D:
    while (*y < maxy && grid[*y + 1][*x] == '#') {
      count++;
      ++*y;
    }
    break;
  case L:
    while (*x && grid[*y][*x - 1] == '#') {
      count++;
      --*x;
    }
    break;
  case NONE:
    die("unexpected case");
  }
  return count;
}

int
main(int argc, char **argv) {
  int count = 0;
  int x = 0, y = 0, i, j, dir = NONE;
  int part1 = 0;
  char *p;

  debug_init();
  if (argc > 1 && strcmp("-", argv[1]))
    if (!(stdin = freopen(argv[1], "r", stdin))) {
      perror("failure");
      exit(2);
    }

  while (scanf("%" SCNd64 "%*[,\n]", &orig[len]) == 1)
    if (len++ > LIMIT - 3)
      die("recompile with larger LIMIT");
  printf("Read %u slots\n", len);

  init(&s, 0);
  s.has_in = false;
  while (run(&s)) {
    count++;
    if (!s.has_out)
      die("expecting output");
    grid[y][x] = s.out;
    if (s.out == '\n') {
      maxy = ++y;
      x = 0;
    } else {
      if (s.out != '.' && s.out != '#') {
        i = x;
        j = y;
        dir = s.out;
      }
      if (x > 1 && y > 0 && s.out == '#' && grid[y][x - 1] == '#' &&
          grid[y][x - 2] == '#' && grid[y - 1][x - 1] == '#')
        part1 += (x - 1) * y;
      if (++x > maxx)
        maxx = x;
    }
    if (x >= GRID || y >= GRID)
      die("recompile with larger GRID");
    s.has_out = false;
  }
  display();
  printf("%d outputs, start at %d,%d, checksum %d\n", count, i, j, part1);
  if (dir == NONE)
    die("could not find start location");

  /* Compute the path */
#define MAXFN 21
  char scratch[MAXFN * MAXFN] = "";
  char a[MAXFN], b[MAXFN], c[MAXFN], m[MAXFN];
  char *pa, *pb, *pc, *pm;
  int nextdir = next(i, j, dir);
  bool done = false;
  int paira, pairb, pairc;

  p = scratch;
  while (nextdir != NONE) {
    count = 0;
    if ((dir == U && nextdir == R) || (dir == R && nextdir == D) ||
        (dir == D && nextdir == L) || (dir == L && nextdir == U))
      p += sprintf(p, "R,");
    else
      p += sprintf(p, "L,");
    dir = nextdir;
    p += sprintf(p, "%d,", walk(&i, &j, dir));
    nextdir = next(i, j, dir);
  }
  printf("computed %s\n", scratch);
  /* Brute force compression: assumes that each function contains 1-5 pairs */
  for (paira = 1; !done && paira <= 5; paira++) {
    p = scratch;
    memset(m, 0, sizeof m);
    pm = m;
    i = 0;
    pa = a;
    do {
      if ((*pa++ = *p++) == ',')
        i++;
    } while (i < paira * 2);
    *pa = '\0';
    if (pa - a >= MAXFN)
      continue;
    *pm++ = 'A';
    *pm++ = ',';
    for (pairb = 1; !done && pairb <= 5; pairb++) {
      p = scratch;
      pm = m;
      while (!strncmp(p, a, pa - a)) {
        p += pa - a;
        *pm++ = 'A';
        *pm++ = ',';
      }
      i = 0;
      pb = b;
      do {
        if ((*pb++ = *p++) == ',')
          i++;
      } while (i < pairb * 2);
      *pb = '\0';
      if (pb - b >= MAXFN)
        continue;
      *pm++ = 'B';
      *pm++ = ',';
      for (pairc = 1; !done && pairc <= 5; pairc++) {
        p = scratch;
        pm = m;
        while (!strncmp(p, a, pa - a) ||
               !strncmp(p, b, pb - b)) {
          if (!strncmp(p, a, pa - a)) {
            p += pa - a;
            *pm++ = 'A';
          } else {
            p += pb - b;
            *pm++ = 'B';
          }
          *pm++ = ',';
        }
        i = 0;
        pc = c;
        do {
          if ((*pc++ = *p++) == ',')
            i++;
        } while (i < pairc * 2);
        *pc = '\0';
        if (pc - c >= MAXFN)
          continue;
        *pm++ = 'C';
        *pm++ = ',';
        debug("trying A=%s B=%s C=%s ...", a, b, c);
        while (*p) {
          if (!strncmp(p, a, pa - a)) {
            p += pa - a;
            *pm++ = 'A';
            *pm++ = ',';
          } else if (!strncmp(p, b, pb - b)) {
            p += pb - b;
            *pm++ = 'B';
            *pm++ = ',';
          } else if (!strncmp(p, c, pc - c)) {
            p += pc - c;
            *pm++ = 'C';
            *pm++ = ',';
          } else if (*p) {
            break;
          }
        }
        if (*p)
          debug("nope\n");
        else {
          debug("success\n");
          done = true;
        }
      }
    }
  }
  if (!done)
    die("unable to compress");
  debug("compressed: %s\n", m);
  p = stpcpy(scratch, m);
  p[-1] = '\n';
  p = stpcpy(p, a);
  p[-1] = '\n';
  p = stpcpy(p, b);
  p[-1] = '\n';
  p = stpcpy(p, c);
  p[-1] = '\n';
  *p++ = argc > 2 ? argv[2][0] : 'n';
  *p++ = '\n';
  *p++ = '\0';
  printf("input will be:\n%s", scratch);

  /* And finally use the input */
  init(&s, 0);
  s.a[0] = 2;
  s.has_in = false;
  p = scratch;
  while (run(&s) && *p) {
    if (s.has_out) {
      if (isascii(s.out))
        debug("read '%c'\n", (char)s.out);
      else
        die("unexpected output %" PRId64, s.out);
      s.has_out = false;
      continue;
    }
    if (s.has_in)
      die("expecting input to be consumed");
    s.in = *p++;
    s.has_in = true;
  }
  if (*p)
    die("unconsumed input: %s", p);
  printf("begin output stream\n");
  while (s.has_out && isascii(s.out)) {
    putchar(s.out);
    s.has_out = false;
    if (run(&s) == -1)
      die("no input to provide");
  }
  if (!s.has_out)
    die("expecting output");
  printf("collected %" PRId64 " dust\n", s.out);
  return 0;
}
