#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <ctype.h>

static int debug_level = -1;
static void
debug_raw(int level, const char *fmt, ...) {
  va_list ap;
  if (debug_level < 0)
    debug_level = atoi(getenv("DEBUG") ?: "0");
  if (debug_level >= level) {
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
  }
}
#define debug(...) debug_raw(1, __VA_ARGS__)

static int __attribute__((noreturn))
die(const char *fmt, ...)
{
  va_list ap;
  va_start(ap, fmt);
  vprintf(fmt, ap);
  va_end(ap);
  putchar('\n');
  exit(1);
}

#define MAX 650
static char orig[MAX];
static char data[2][MAX * 10000];
static int len;
static int phase;

static void
dump(char *s) {
  printf("after phase %d, signal begins with %.8s\n", phase, s);
}

static void
do_phase(char *old, char *new, int offset) {
  int i, j, t;
  static int8_t m[] = { 0, 1, 0, -1 };

  /* First half is painful */
  for (i = offset; i < len / 2; i++) {
    for (t = 0, j = offset; j < len; j++)
      t += (old[j] - '0') * m[(j + 1) / (i + 1) % 4];
    new[i] = (abs(t) % 10) + '0';
    debug(" i=%d: %c\n", i, new[i]);
  }
  /* Second half is easy */
  for (t = 0, i = len - 1; i >= offset && i >= len / 2; i--) {
    t += old[i] - '0';
    new[i] = t % 10 + '0';
    debug(" i=%d: %c\n", i, new[i]);
  }
  phase++;
  if (debug_level)
    dump(new);
}

int
main(int argc, char **argv) {
  int ch, i;
  int iter = 100;
  int reps = 10000;
  int offset;

  debug("");
  if (argc > 1 && strcmp(argv[1], "-"))
    if (!(stdin = freopen(argv[1], "r", stdin))) {
      perror("failure");
      exit(2);
    }

  if (argc > 2)
    iter = atoi(argv[2]);
  if (argc > 3)
    reps = atoi(argv[3]);
  if (reps > sizeof data[0] / sizeof orig)
    die("too many reps");

  while (isdigit(ch = getchar())) {
    orig[len] = ch;
    if (len++ >= MAX)
      die("recompile with larger MAX");
  }
  printf("Operating on length %d\n", len);
  dump(orig);

  /* Part 1 */
  memcpy(data[0], orig, len);
  for (i = 0; i < iter; i++)
    do_phase(data[phase % 2], data[1 - phase % 2], 0);
  dump(data[phase % 2]);

  /* Part 2 */
  ch = orig[7];
  orig[7] = '\0';
  offset = atoi(orig);
  orig[7] = ch;
  if (argc > 4) {
    printf("ignoring offset %d\n", offset);
    offset = 0;
  } else {
    if (offset >= len * 10000)
      die("offset too large");
    printf("need offset %d among len %d, try %d reps\n", offset, len * 10000,
           (len * 10000 - offset + (len / 2 - 1)) / (len / 2));
    reps = (len * 10000 - offset + (len / 2 - 1)) / (len / 2);
    offset -= (10000 - reps) * len;
    printf("trying offset %d among len %d instead\n", offset, len * reps);
  }

  for (i = 0; i < reps; i++)
    memcpy(&data[0][i * len], orig, len);
  phase = 0;
  len *= reps;
  for (i = 0; i < iter; i++) {
    if (offset < len / 2)
      printf("%d\n", i);
    do_phase(data[phase % 2], data[1 - phase % 2], offset);
  }

  if (argc > 4)
    printf("%.*s\n", len, data[phase % 2]);
  dump(&data[phase % 2][offset]);
  return 0;
}
