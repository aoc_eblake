divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day5.input] day5.m4

include(`intcode.m4')ifelse(intcode(5), `ok', `',
`errprint(`Missing IntCode initialization
')m4exit(1)')

parse(input)
no64()
define(`write', `ifelse($1, 0, `', `divert`'$1
divert(-1)')')
define(`try', `
  save()
  define(`read', $1)
  run(0)
  restore()')
try(1)
try(5)
