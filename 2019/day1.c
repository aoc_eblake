#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>

void debug(const char *fmt, ...) {
  va_list ap;
  if (getenv("DEBUG")) {
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
  }
}

int main(int argc, char **argv) {
  size_t len = 0, count = 0;
  int i, sum1 = 0, sum2 = 0;
  char *line;

  if (argc > 1)
    if (!(stdin = freopen(argv[1], "r", stdin))) {
      perror("failure");
      exit(2);
    }

  while (getline(&line, &len, stdin) >= 0) {
    ++count;
    i = atoi(line);
    if (i < 6) {
      fprintf(stderr, "bad input\n");
      exit(1);
    }
    sum1 += i / 3 - 2;
    while (i > 8) {
      sum2 += i / 3 - 2;
      i = i / 3 - 2;
    }
  }
  printf("Read %zu lines, sum1 %d, sum2 %d\n", count, sum1, sum2);
  return 0;
}
