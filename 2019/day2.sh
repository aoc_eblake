#!/bin/bash
for i in $(seq 0 99); do
    for j in $(seq 0 99); do
	case $(./foo day2.input $i $j) in
	    *holds\ 19690720) break 2 ;;
	esac
    done
done
echo winner at $((100*i+j))
