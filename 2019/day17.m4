divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day17.input] day17.m4

include(`intcode.m4')ifelse(intcode(17), `ok', `',
`errprint(`Missing IntCode initialization
')m4exit(1)')

parse(input)
define(`g', `ifelse($1, -1, 0, $2, -1, 0, `ifdef(`g_$1_$2', 1, 0)')')
define(`write', `ifelse(process($1), 1, `define(`g_'x`_'y)')define(`x',
  incr(x))')
define(`process', `ifdef(`process$1', `process$1()', `oops()')')
define(`process10', `ifelse(x, 0, `define(`x', -1)',
  `define(`y', incr(y))define(`x', -1)')0') # \n
define(`process46', 0) # .
define(`process94', `define(`d', `U,' x`,' y)1') # ^
define(`process62', `define(`d', `R,' x`,' y)1') # >
define(`process118', `define(`d', `D,' x`,' y)1') # v
define(`process60', `define(`d', `L,' x`,' y)1') # <
define(`process35', `ifelse(eval(x > 1 && y), 1, `check(decr(x), y)')1') # #
define(`check', `ifelse(g($1, $2)g(decr($1), $2)g($1, decr($2)), 111,
  `define(`part1', eval(part1 + $1 * $2))')')

save()
define(`part1', 0)
define(`x', 0)
define(`y', 0)
run(0)
restore()

define(`turn', `$0$1($2, $3)')
define(`_turn', `ifelse($1, 10, ``L,'walk($2)', $1, 01, ``R,'walk($3)')')
define(`turnU', `_turn(g(decr($1), $2)g(incr($1), $2), `L,$@', `R,$@')')
define(`turnR', `_turn(g($1, decr($2))g($1, incr($2)), `U,$@', `D,$@')')
define(`turnD', `_turn(g(incr($1), $2)g(decr($1), $2), `R,$@', `L,$@')')
define(`turnL', `_turn(g($1, incr($2))g($1, decr($2)), `D,$@', `U,$@')')
define(`walk', `$0$1($2, $3, 0)')
define(`_walk', `ifelse(g($2, $3), 1, `walk$1($2, $3, incr($6))',
  ``$6,'turn($1, $4, $5)')')
define(`walkU', `_walk(U, $1, decr($2), $@)')
define(`walkR', `_walk(R, incr($1), $2, $@)')
define(`walkD', `_walk(D, $1, incr($2), $@)')
define(`walkL', `_walk(L, decr($1), $2, $@)')
define(`path', turn(d))

define(`tuple', `oops()')
forloop_var(`a', 2, 5, `forloop_var(`b', 2, 5, `forloop_var(`c', 2, 5,
  `oneshot(`tuple', ``A_','a`,`B_','b`,`C_','c)')')')
define(`compress', `ifelse(_$0(defn(`path'), tuple), `', `', `$0()')')
define(`_compress', `ifelse($#, 2, ``$1'', `$0(set(`$1', `$2', $3),
  shift(shift(shift($@))))')')
define(`set', `define(`$2', quote(pair$3($1)))ifelse(eval(len(defn(`$2'))
  <= 20), 1, `replace(`$1', defn(`$2'))', ``$1'')')
define(`pair2', `$1,$2,$3,$4,')
define(`pair3', `$1,$2,pair2(shift(shift($@)))')
define(`pair4', `$1,$2,pair3(shift(shift($@)))')
define(`pair5', `$1,$2,pair4(shift(shift($@)))')
define(`replace', `_$0(index(`$1', `$2'), $@)')
define(`_replace', `ifelse($1, -1, ``$2'', `replace(quote(substr(`$2', 0,
  $1)`$4'substr(`$2', eval($1 + len(`$3')))), `$3', `$4')')')

define(`prepread', `ifelse($#, 2, `_$0(10)',
  `$0(shift($@))_$0(44)')ifdef(`$0$1', `$0$1()', `oops()')')
define(`_prepread', `oneshot(`read', `$1')')
define(`prepreadA', `_prepread(65)')
define(`prepreadB', `_prepread(66)')
define(`prepreadC', `_prepread(67)')
define(`prepreadL', `_prepread(76)')
define(`prepreadR', `_prepread(82)')
define(`prepreadn', `_prepread(110)')
define(`prepready', `_prepread(121)')
define(`prepread4', `_prepread(52)')
define(`prepread6', `_prepread(54)')
define(`prepread8', `_prepread(56)')
define(`prepread10', `_prepread(48)_prepread(49)')
define(`prepread12', `_prepread(50)_prepread(49)')

# Compute the path compression, then prep read in LIFO order
compress()
define(`M_', replace(replace(replace(defn(`path'), defn(`A_'), `A,'),
  defn(`B_'), `B,'), defn(`C_'), `C,'))
prepread(n,)
prepread(C_)
prepread(B_)
prepread(A_)
prepread(M_)
define(`write', `define(`data', $1)')
define(`mem0', 2)
run(0)
define(`part2', data)

divert`'part1
part2
