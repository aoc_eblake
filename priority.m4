divert(-1)dnl -*- m4 -*-
# Minimum Priority Queue implementations for Dijkstra/A* searching
# assumes common.m4 is already loaded

# Each implementation has a common interface:
# insert(priority, data) queues up data with a priority 0-99999
#  (negative ranges would need GNU m4; implementation 1 is width-limited)
# pop() returns the lowest-such queued data, or nothing if queue is empty
# clearall() reclaims all memory in the priority queue

# Under the hood, each implementation stores nodes with the same key in
# a pushdef stack `prio$1'; this means implementing a lower() interface to
# move an already-queued item to a lower priority would be O(n) in the
# number of items with the old priority; instead, I went with the idea that
# it is easier to check after pop() whether a node was already visited at a
# lower priority.  The implemenations do not guarantee whether pop() will see
# LIFO or FIFO order for multiple insertions at the same priority.  The real
# meat of each implementation is how the set of current distinct priorities
# is stored (tradeoffs between insertion vs. retrieval efforts)

# In general, implementation 5 performs best, although 0 and 2 have less
# overhead if all current priorities fall in a narrow range; while 1, 3,
# and 4 are more for demonstrations of other techniques
# (For grins, see also implementation 6 in 2023/day17.m4 that uses syscmd
# for external sorting, to demonstrate why an implementation in m4 matters)

ifdef(`priority', `', `define(`priority', 5)')
ifelse(priority, 0, `
output(1, `Using radix-sorted buckets as priority queue')
# O(1) insertion, worst-case O(range) pop, when searching for the next
# bucket; but this approaches amortized O(1) when the range is small.
# limited overhead, works well when spread of min to max is small
define(`insert', `ifelse(ifdef(`prlo', `eval($1<prlo)', 1), 1, `define(`prlo',
  $1)')ifelse(ifdef(`prhi', `eval($1>prhi)', 1), 1, `define(`prhi',
  $1)')pushdef(`prio$1', `$@')')
define(`pop', `ifdef(`prlo', `_$0(prlo)')')
define(`_pop', `prio$1`'popdef(`prio$1')ifdef(`prio$1', `', `ifelse($1, prhi,
  `popdef(`prlo')popdef(`prhi')', `define(`prlo', prfind(incr($1)))')')')
define(`prfind', `ifdef(`prio$1', `$1', `$0(incr($1))')')
define(`clearall', `ifdef(`prlo', `forloop(prlo, prhi, `undefine(`prio'',
  `)')popdef(`prlo')popdef(`prhi')')')

', priority, 1, `
output(1, `Using O(log n) insertion priority queue')
# Binary search of list of priorities for algorithmic O(log n) insertion;
# algorithmic O(1) pop; but both insertion and pop suffer from O(n) overhead
# of parsing the list in full

define(`oops', `output(0, `error: unexpected priority $1')m4exit(1)')
define(`prio', `')
define(`priolen', 0)
define(`insert', `ifelse(eval(len(`$1')>5), 1, `oops($1)')ifdef(`prio$1', `',
  `_$0(substr(`$1     ', 0, 5), locate($1, 0, priolen))')pushdef(`prio$1',
  `$@')')
define(`_insert', `define(`prio', substr(prio, 0, $2)$1substr(prio,
  $2))define(`priolen', eval(priolen + 5))')
define(`locate', `ifelse($2, $3, $2, `_$0($@, eval(($2 + $3) / 10 * 5))')')
define(`_locate', `ifelse(eval(substr(prio, $4, 5) < $1), 1, `locate($1,
  eval($4 + 5), $3)', `locate($1, $2, $4)')')
define(`pop', `ifelse(priolen, 0, `', `_$0(eval(substr(prio, 0, 5)))')')
define(`_pop', `prio$1`'popdef(`prio$1')ifdef(`prio$1', `',
  `define(`prio', substr(prio, 5))define(`priolen', eval(priolen - 5))')')
define(`clearall', `foreach(`_$0', translit(prio, ` ', `,'))define(`priolen',
  0)define(`prio', `')')
define(`_clearall', `ifelse($1, `', `', `undefine(`prio$1')')')

', priority, 2, `
output(1, `Using pushdef stack priority queue')
# O(n) insertion by storing priorities in sorted order, O(1) pop
# minimal overhead, works well when spread of min to max is small

define(`insert', `ifdef(`prio$1', `', `saveto($1)restore()')pushdef(`prio$1',
  `$@')')
define(`saveto', `ifdef(`prio', `ifelse(prio, $1, `', eval(prio < $1), 1,
  `pushdef(`tmp', prio)popdef(`prio')$0($1)', `pushdef(`prio', $1)')',
  `pushdef(`prio', $1)')')
define(`restore', `ifdef(`tmp', `pushdef(`prio', tmp)popdef(`tmp')$0()')')
define(`pop', `ifdef(`prio', `_$0(prio)')')
define(`_pop', `prio$1`'popdef(`prio$1')ifdef(`prio$1', `',
  `popdef(`prio')')')
define(`clearall', `ifdef(`prio', `undefine(`prio'prio)popdef(`prio')$0()')')

', priority, 3, `
output(1, `Using unsorted list plus cache for priority queue')
# algorithmic O(1) insertion, while pop is worst-case O(n) to rebuild,
# but closer to amortized O(1) the fewer times a rebuild is needed

define(`insert', `ifdef(`prio$1', `', `_$0($1)')pushdef(`prio$1', `$@')')
define(`_insert', `ifdef(`prio', `define(`prio', `$1,'defn(`prio'))ifelse(
  ifdef(`cache', `eval($1 < cache)'), 1, `define(`cache', $1)')',
  `define(`prio', $1)define(`cache', $1)')')
define(`pop', `ifdef(`prio', `_$0(ifdef(`cache', `',
  `rebuild()')defn(`cache'))')')
define(`_pop', `ifelse($1, `', `', `prio$1`'popdef(`prio$1')ifdef(`prio$1', `',
  `popdef(`cache')')')')
define(`rebuild', `foreach(`_$0', prio()popdef(`prio'))')
define(`_rebuild', `ifdef(`prio$1', `_insert($1)')')
define(`clearall', `ifdef(`prio', `foreach(`_$0', prio())popdef(`prio')')')
define(`_clearall', `undefine(`prio$1')')

', priority, 4, `
output(1, `Using sorted list for priority queue')
# O(n) insertion, algorithmic O(1) pop coupled with m4 O(n) parse effects

define(`prio')
define(`insert', `ifdef(`prio$1', `', `_$0($1)')pushdef(`prio$1', `$@')')
define(`_insert', `pushdef(`t', $1)define(`prio', foreach(`$0_',
  prio))popdef(`t')')
define(`_insert_', `ifelse(t`$1', `', `', t, `', `$1`,'', $1, `', `t`,'',
  eval(t + 0 < $1 + 0), 1, `t`,'define(`t')$1`,'', `$1`,'')')
define(`pop', `_$0(prio)')
define(`_pop', `ifelse($1, `', `', `prio$1`'popdef(`prio$1')ifdef(`prio$1', `',
  `define(`prio', quote(shift(prio)))')')')
define(`clearall', `foreach(`_$0', prio)define(`prio')')
define(`_clearall', `ifelse(`$1', `', `', `undefine(`prio$1')')')

', priority, 5, `
output(1, `Using min-heap array for priority queue')
# textbook min-heap priority queue, using sequential NNNN in prNNNNN to
# serve as an underlying array. True O(log n) insertion, and amortized
# O(1) pop, but requires more macro overhead to maintain bookkeeping

define(`prlen', 0)
define(`insert', `ifdef(`prio$1', `pushdef(`prio$1', `$@')', `define(`prio$1',
  `$@')define(`prlen', incr(prlen))define(`pr'prlen, $1)prup(prlen)')')
define(`prswap', `define(`pr$2', pr$1`'define(`pr$1', pr$2))')
define(`prup', `ifelse($1, 1, `', `_$0($1, eval($1/2))')')
define(`_prup', `ifelse(eval(pr$1 < pr$2), 1, `prswap($1, $2)prup($2)')')
define(`pop', `ifelse(prlen, 0, `', `_$0(pr1, prlen)')')
define(`_pop', `prio$1`'popdef(`prio$1')ifdef(`prio$1', `', `define(`pr1',
  pr$2)popdef(`pr$2')define(`prlen', decr($2))prdown(1, prlen)')')
define(`prdown', `_$0_1($1, pr$1, eval(2*$1), $2)')
define(`_prdown_1', `ifelse(eval($3 <= $4), 1, `_prdown_2($1, ifelse(eval(
  pr$3 < $2), 1, `$3, pr$3', `$1, $2'), incr($3), $4)')')
define(`_prdown_2', `_prdown_3($1, ifelse(eval($4 <= $5 && defn(`pr$4')+0 < $3),
  1, $4, $2), $5)')
define(`_prdown_3', `ifelse($1, $2, `', `prswap($1, $2)prdown($2, $3)')')
define(`clearall', `ifelse(prlen, 0, `', `forloop_arg(1, prlen, `_$0')define(
  `prlen', 0)')')
define(`_clearall', `undefine(`prio'pr$1)popdef(`pr$1')')

', `output(0, `unknown priority')m4exit(1)')
