#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>

void debug(const char *fmt, ...) {
  va_list ap;
  if (getenv("DEBUG")) {
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
  }
}

static void compare(const char *a, const char *b) {
  const char *p = a, *q = b;
  int diff = 0;
  while (*p) {
    if (*p != *q)
      diff++;
    p++;
    q++;
  }
  debug(" comparing %s and %s: %d\n", a, b, diff);
  if (diff == 1) {
    printf("Nearby: %s"
	   "        %s", a, b);
    exit(0);
  }
}

int main(int argc, char **argv) {
  char **list = NULL;
  size_t size = 0, len = 0;
  char *line;
  size_t i, j;

  if (argc > 1)
    stdin = freopen(argv[1], "r", stdin);

  while (getline(&line, &len, stdin) >= 0) {
    list = reallocarray(list, ++size, sizeof *list);
    list[size - 1] = strdup(line);
  }
  printf("read %zd lines\n", size);
  for (i = 0; i < size; i++)
    for (j = i + 1; j < size; j++)
      compare(list[i], list[j]);
  printf("no nearby found\n");
  return 1;
}
