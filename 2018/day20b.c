#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <inttypes.h>
#include <assert.h>

bool debug(const char *fmt, ...) {
  va_list ap;
  if (getenv("DEBUG")) {
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
    return true;
  }
  return false;
}

#define LIMIT 1000
#define OFFSET (LIMIT / 2)
static short grid[LIMIT][LIMIT];
typedef struct state {
  int depth;
  int x;
  int y;
  const char *p;
} state;
static const char *orig;

static void set(int x, int y, int d) {
  if (grid[y][x])
    debug("skipping %d,%d to %d, already visited at %d\n",
	  x, y, d, grid[y][x]);
  else
    grid[y][x] = d;
}

static void visit(state *s) {
  short d = grid[s->y][s->x];
  state branches[4];
  int b = -1;
  int i;

  debug("depth %d, visiting %c (%tu) at %d,%d distance %d\n",
	s->depth, *s->p, s->p - orig, s->x, s->y, d);
  if (s->x < 1 || s->x >= LIMIT - 1 || s->y < 1 || s->y >= LIMIT - 1) {
    fprintf(stderr, "recompile with larger limit\n");
    exit(1);
  }
  assert(d);
  switch (*s->p) {
  default:
    assert(false);
  case '^':
    assert(!s->depth);
    break;
  case '$':
    assert(!s->depth);
    return;
  case 'N':
    set(s->x, --s->y, d + 1);
    break;
  case 'S':
    set(s->x, ++s->y, d + 1);
    break;
  case 'E':
    set(++s->x, s->y, d + 1);
    break;
  case 'W':
    set(--s->x, s->y, d + 1);
    break;
  case '(':
    do {
      if (++b == sizeof branches / sizeof *branches) {
	fprintf(stderr, "recompile for more alternates\n");
	exit(1);
      }
      branches[b] = *s;
      branches[b].depth++;
      branches[b].p = b ? branches[b - 1].p + 1 : s->p + 1;
      visit(&branches[b]);
    } while (*branches[b].p == '|');
    for (i = 0; i < b; i++) {
      if (branches[i].x != branches[b].x || branches[i].y != branches[b].y) {
	branches[i].depth--;
	branches[i].p = branches[b].p + 1;
	visit(&branches[i]);
      } else
	debug("skipping round-trip alternative\n");
    }
    branches[b].depth--;
    *s = branches[b];
    break;
  case '|': case ')':
    assert(s->depth);
    return;
  }
  s->p++;
  visit(s);
}

int main(int argc, char **argv) {
  size_t len = 0, count = 0;
  char *line = NULL;
  int score = 0;
  state s = { .x = OFFSET, .y = OFFSET };
  int x, y;
  int far = 1000;

  if (argc > 2)
    far = atoi(argv[2]);
  if (argc > 1 && *argv[1] && *argv[1] == '^')
    s.p = argv[1];
  else {
    if (argc > 1 && *argv[1] && !(stdin = freopen(argv[1], "r", stdin))) {
      perror("failure");
      exit(2);
    }
    if (getline(&line, &len, stdin) < 0) {
      fprintf(stderr, "invalid input\n");
      exit(1);
    }
    s.p = line;
  }
  orig = s.p;
  grid[s.y][s.x] = 1;
  visit(&s);
  for (y = 0; y < LIMIT; y++)
    for (x = 0; x < LIMIT; x++) {
      if (grid[y][x] > far)
	count++;
      if (grid[y][x] > score)
	score = grid[y][x];
    }
  printf("Furthest point is %d doors away, %zu rooms beyond %d\n",
	 score - 1, count, far);
  return 0;
}
