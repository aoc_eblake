#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <inttypes.h>
#include <assert.h>
#include <ctype.h>

bool __attribute__((format(printf, 1, 2)))
debug(const char *fmt, ...) {
  va_list ap;
  if (getenv("DEBUG")) {
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
    return true;
  }
  return false;
}

#define LIMIT 1300

static struct data {
  int x;
  int y;
  int z;
  int t;
  int c;
} array[LIMIT];
static bool used[LIMIT + 1];

int main(int argc, char **argv) {
  size_t len = 0, count = 0;
  char *line;
  int i, j, k, t;

  /* Part 1 - read data */
  if (argc > 1)
    if (!(stdin = freopen(argv[1], "r", stdin))) {
      perror("failure");
      exit(2);
    }

  while (getline(&line, &len, stdin) >= 0) {
    if (count >= LIMIT) {
      fprintf(stderr, "recompile with larger LIMIT!\n");
      exit(1);
    }
    if (sscanf(line, "%d,%d,%d,%d\n", &array[count].x, &array[count].y,
	       &array[count].z, &array[count].t) != 4) {
      fprintf(stderr, "bad input\n");
      exit(1);
    }
    ++count;
  }
  printf("Read %zu lines\n", count);

  array[0].c = 1;
  used[1] = true;
  for (i = 1; i < count; i++) {
    for (j = 0; j < i; j++) {
      if (abs(array[i].x - array[j].x) + abs(array[i].y - array[j].y)
	  + abs(array[i].z - array[j].z) + abs(array[i].t - array[j].t) <= 3) {
	debug("point at index %d near index %d\n", j, i);
	if (array[i].c) {
	  if (array[i].c != array[j].c) {
	    /* merge */
	    t = array[j].c;
	    for (k = 0; k < i; k++)
	      if (array[k].c == t)
		array[k].c = array[i].c;
	    used[t] = false;
	  }
	} else {
	  assert(array[j].c);
	  array[i].c = array[j].c;
	}
      }
    }
    if (!array[i].c) {
      for (j = 1; j <= i + 1; j++)
	if (!used[j]) {
	  used[j] = true;
	  array[i].c = j;
	  break;
	}
    }
  }
  j = 0;
  for (i = 1; i < count + 1; i++)
    if (used[i])
      j++;
  printf("Found %d constellations\n", j);
  return 0;
}
