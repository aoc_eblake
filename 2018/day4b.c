#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>

void debug(const char *fmt, ...) {
  va_list ap;
  if (getenv("DEBUG")) {
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
  }
}

#define LIMIT 3500

short array[LIMIT][60];
char list[LIMIT];

static void process(int guard, int start, int end) {
  debug("processing %d: %d-%d\n", guard, start, end);
  for (int i = start; i < end; i++)
    array[guard][i]++;
}

int main(int argc, char **argv) {
  size_t len = 0, count = 0;
  char *line;
  int i, j;
  int guard;
  int sleep, wake;
  char *p;
  bool pair = false;

  if (argc > 1)
    if (!(stdin = freopen(argv[1], "r", stdin))) {
      perror("failure");
      exit(2);
    }

  while (getline(&line, &len, stdin) >= 0) {
    count++;
    if (pair) {
      p = strchr(line, ':');
      if (!p) {
	fprintf(stderr, "bad input\n");
	exit(1);
      }
      wake = (p[1] - '0') * 10 + p[2] - '0';
      process(guard, sleep, wake);
      pair = false;
    } else {
      p = strchr(line, '#');
      if (p) {
	if (sscanf(p, "#%d ", &guard) != 1) {
	  fprintf(stderr, "bad input\n");
	  exit(1);
	}
	if (guard >= LIMIT) {
	  printf("recompile with larger LIMIT!\n");
	  exit(1);
	}
	list[guard]++;
      } else {
	p = strchr(line, ':');
	if (!p) {
	  fprintf(stderr, "bad input\n");
	  exit(1);
	}
	sleep = (p[1] - '0') * 10 + p[2] - '0';
	pair = true;
      }
    }
  }
  printf("read %zd lines\n", count);
  int best_guard = 0;
  int total_sleep = 0;
  int best_minute = 0;
  for (i = 0; i < LIMIT; i++) {
    int local_minute = 0;
    int local_sleep = 0;
    if (!list[i])
      continue;
    for (j = 0; j < 60; j++) {
      local_sleep += array[i][j];
      if (array[i][j] > array[i][local_minute])
	local_minute = j;
    }
    debug("guard %d slept %d best minute %d: %d\n", i, local_sleep,
	  local_minute, array[i][local_minute]);
    if (array[i][local_minute] > total_sleep) {
      total_sleep = array[i][local_minute];
      best_guard = i;
      best_minute = local_minute;
    }
  }
  printf("best guard %d at minute %d = %d\n", best_guard, best_minute,
	 best_guard * best_minute);
  return 0;
}
