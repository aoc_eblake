#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>

void debug(const char *fmt, ...) {
  va_list ap;
  if (getenv("DEBUG")) {
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
  }
}

static struct data {
  char state;
  char wait[26];
} list[26];

int main(int argc, char **argv) {
  size_t len = 0, count = 0;
  char *line;
  int i, j;
  char a, b;
  char *p;
  int offset = 60;
  int workers = 5;
  char work[5] = "";

  /* Part 1 - read in lines */
  if (argc > 2)
    offset = atoi(argv[2]);
  if (argc > 3)
    workers = atoi(argv[3]);
  if (argc > 1)
    if (!(stdin = freopen(argv[1], "r", stdin))) {
      perror("failure");
      exit(2);
    }

  while (getline(&line, &len, stdin) >= 0) {
    ++count;
    if (sscanf(line, "Step %c must be finished before step %c can begin. ", &a,
	       &b) != 2 || (unsigned)(a-'A') > 25 || (unsigned)(b-'A') > 25) {
      fprintf(stderr, "bad input\n");
      exit(1);
    }
    list[a - 'A'].state = a - 'A' + 1 + offset;
    list[b - 'A'].state = b - 'A' + 1 + offset;
    strchrnul(list[b - 'A'].wait, '\0')[0] = a;
  }
  printf("Read %zu lines\n", count);

  /* Part 2 - determine output */
  count = 0;
  do {
    count++;
    for (j = 0; j < workers; j++)
      if (work[j])
	if (!++list[work[j] - 'A'].state)
	  work[j] = 0;
    for (j = 0; j < workers; j++) {
      if (work[j])
	continue;
      for (i = 0; i < 26; i++) {
	if (list[i].state <= 0)
	  continue;
	p = list[i].wait;
	while (*p) {
	  if (list[*p - 'A'].state)
	    break;
	  p++;
	}
	if (*p)
	  continue;
	list[i].state = -list[i].state;
	work[j] = i + 'A';
	debug("time %zu worker %d starting job %c\n", count - 1, j, i + 'A');
	break;
      }
    }
  } while (work[0] || work[1] || work[2] || work[3] || work[4]);
  printf("took %zu seconds\n", count - 1);
  return 0;
}
