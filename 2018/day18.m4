divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dhashsize=H] [-Dfile=day18.input] day18.m4
# Optionally use -Dverbose=1 to see some progress

include(`common.m4')ifelse(common(18, 1000003), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

include(`math64.m4')

define(`input', translit(include(defn(`file')), `.|#'nl, `0123'))
define(`width', incr(index(defn(`input'), 3)))
define(`prep', `define(`g$1', 0)define(`g'eval('width*width`+$1), 0)')
forloop_arg(0, width, `prep')
define(`prep', `_$0($1, len(translit($1, 02)), len(translit($1, 01)))')
define(`_prep', `define(`r0$1', $0_(eval(`$2>=3'), 1, `$'1, 1, 0))define(
  `r1$1', $0_(eval(`$3>=3'), 1, `$'1, 2, 1))define(`r2$1', $0_(eval(`$2*$3'),
  0, `$'1, 0, 2))')
define(`_prep_', `ifelse($1, $2, ``pushdef(`M', `define(`g$3', $4)')'$4', $5)')
forloop(0, eval(9*27*27-1), `prep(eval(', `, 3, 8))')

define(`x', incr(width))
define(`visit', `ifelse($2, 3, `define(`g$1', 0)define(`n$1', 000000000)',
  `define(`g$1', $2)define(`n$1', _$0($1)_$0(eval($1-''width``))_$0(
  eval($1+''width``)))')define(`x', incr($1))')
define(`_visit', ``g$1`'g'decr($1)``'g'incr($1)``''')
ifdef(`__gnu__', `
  patsubst(defn(`input'), `.', `visit(x, \&)')
',`
  define(`chew', `ifelse($1, 1, `visit(x, $2)', `$0(eval($1/2), substr(
    `$2', 0, eval($1/2)))$0(eval($1-$1/2), substr(`$2', eval($1/2)))')')
  chew(len(defn(`input')), defn(`input'))
')

define(`m', `ifdef(`M', `M()popdef(`M')m()')')
define(`round', `ifdef(`count$1', `count$1', `count')($1, forloop_arg(
  'incr(width)`, 'eval(width*width-1)`, `_$0')m())')
define(`_round', `first(`r'n$1`($1)')')
define(`rename', `define(`$2', defn(`$1'))popdef(`$1')')
define(`count10', `define(`part1', eval(len(translit($2, 02)) *
  len(translit($2, 01))))count($@)')
define(`countend', `define(`part2', eval(len(translit($2, 02)) *
  len(translit($2, 01))))')
define(`count100', `output(1, `...$1')rename(`$0',
  `count'eval($1+100))count($@)')
define(`count', `ifdef(`h$2', `_$0($1, eval($1-h$2))',
  `define(`h$2', $1)')round(incr($1))')
define(`_count', `output(1, `cycle of $2 found at $1')rename(`countend',
  `count'eval($1+rem(sub64(1000000000, $1), $2)))pushdef(`$0')')
define(`bits', `_$0(eval($1, 2))')
define(`_bits', ifdef(`__gnu__', ``shift(patsubst($1, ., `, \&'))'',
  ``ifelse(len($1), 1, `$1', `substr($1, 0, 1),$0(substr($1, 1))')''))
define(`bits64', `ifelse(eval(len($1) < 10), 1, `bits($1)', `_$0(mul64($1,
  5)), eval(substr($1, decr(len($1))) & 1)')')
define(`_bits64', `bits64(substr($1, 0, decr(len($1))))')
define(`rem', `_$0(0, $2, bits64($1))')
define(`_rem', `ifelse($1$3, 0, $2, $3, `', $1, `$0(eval(($1*2+$3)%$2), $2,
  shift(shift(shift($@))))')')
round(1)

divert`'part1
part2
