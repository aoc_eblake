#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <inttypes.h>
#include <assert.h>

bool debug(const char *fmt, ...) {
  va_list ap;
  if (getenv("DEBUG")) {
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
    return true;
  }
  return false;
}

#define LIMIT 200000

static char list[LIMIT] = "37";

static void dump(int iter, int size, int idx1, int idx2) {
  if (!debug("iter %d:", iter))
    return;
  for (int i = 0; i < size; i++)
    if (i == idx1)
      debug("(%c)", list[i]);
    else if (i == idx2)
      debug("[%c]", list[i]);
    else
      debug(" %c", list[i]);
  debug("\n");
}

int main(int argc, char **argv) {
  int goal = 77201;
  int max;
  int size = 2;
  int idx1 = 0;
  int idx2 = 1;
  int iter = 0;

  if (argc > 1)
    goal = atoi(argv[1]);
  if (argc > 2)
    max = atoi(argv[2]);
  else
    max = goal + 12;
  if (goal > max - 11) {
    fprintf(stderr, "recompile with larger limit to hit goal of %d\n", goal);
    exit(1);
  }

  while (size < max) {
    dump(iter++, size, idx1, idx2);
    size += sprintf(list + size, "%d", list[idx1] - '0' + list[idx2] - '0');
    idx1 = (list[idx1] - '0' + idx1 + 1) % size;
    idx2 = (list[idx2] - '0' + idx2 + 1) % size;
    if (size % 1000 < 2)
      printf("%d...\n", size);
  }
  dump(iter, size, idx1, idx2);
  printf("score: %.10s\n", list + goal);
  return 0;
}
