#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <inttypes.h>
#include <assert.h>

void debug(const char *fmt, ...) {
  va_list ap;
  if (getenv("DEBUG")) {
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
  }
}

#define LIMIT 160
#define MAX 20

static uint8_t map[LIMIT][LIMIT];
static struct cart {
  char id;
  uint8_t x; /* coordinate on map to learn current direction */
  uint8_t y;
  char current; /* what character to put back into map */
  uint8_t turn; /* next intersection: 0 left, 1 straight, 2 right, 3 dead */
} carts[MAX];

static int sorter(const void *one, const void *two) {
  const struct cart *a = one;
  const struct cart *b = two;
  if (a->turn != 3 && b->turn == 3)
    return -1;
  if (a->turn == 3 && b->turn != 3)
    return 1;
  if (a->y < b->y)
    return -1;
  if (a->y > b->y)
    return 1;
  if (a->x < b->x)
    return -1;
  if (a->x > b->x)
    return 1;
  return 0;
}

static void dump(int tick, int max, int ncarts) {
  int i;
  debug("\ntick %d:\n", tick);
  for (i = 0; (getenv("DEBUG") ?: "1")[0] == '2' && i < max; i++)
    debug("%s\n", map[i]);
  debug("%d carts at:\n", ncarts);
  for (i = 0; i < ncarts; i++) {
    char p = map[carts[i].y][carts[i].x];
    debug("%d: %d,%d %c on %c next %c turn %c\n", carts[i].id, carts[i].x,
	  carts[i].y, map[carts[i].y][carts[i].x], carts[i].current,
	  map[carts[i].y - (p == '^') + (p == 'v')]
	  [carts[i].x - (p == '<') + (p == '>')],
	  "lsrX"[carts[i].turn]);
  }
}

int move(int ticks, struct cart *cart) {
  uint8_t c = map[cart->y][cart->x];
  uint8_t *p;
  map[cart->y][cart->x] = cart->current;
  switch (c) {
  case '^':
    assert(cart->y);
    p = &map[--cart->y][cart->x];
    switch (cart->current = *p) {
    case '|': *p = '^'; break;
    case '\\': *p = '<'; break;
    case '/': *p = '>'; break;
    case '+':
      switch (cart->turn) {
      case 0: *p = '<'; cart->turn = 1; break;
      case 1: *p = '^'; cart->turn = 2; break;
      case 2: *p = '>'; cart->turn = 0; break;
      default: assert(0);
      }
      break;
    default:
      goto collide;
    }
    break;
  case 'v':
    assert(cart->y < LIMIT - 1);
    p = &map[++cart->y][cart->x];
    switch (cart->current = *p) {
    case '|': *p = 'v'; break;
    case '\\': *p = '>'; break;
    case '/': *p = '<'; break;
    case '+':
      switch (cart->turn) {
      case 0: *p = '>'; cart->turn = 1; break;
      case 1: *p = 'v'; cart->turn = 2; break;
      case 2: *p = '<'; cart->turn = 0; break;
      default: assert(0);
      }
      break;
    default:
      goto collide;
    }
    break;
  case '<':
    assert(cart->x);
    p = &map[cart->y][--cart->x];
    switch (cart->current = *p) {
    case '-': *p = '<'; break;
    case '\\': *p = '^'; break;
    case '/': *p = 'v'; break;
    case '+':
      switch (cart->turn) {
      case 0: *p = 'v'; cart->turn = 1; break;
      case 1: *p = '<'; cart->turn = 2; break;
      case 2: *p = '^'; cart->turn = 0; break;
      default: assert(0);
      }
      break;
    default:
      goto collide;
    }
    break;
  case '>':
    assert(cart->x < LIMIT - 1);
    p = &map[cart->y][++cart->x];
    switch (cart->current = *p) {
    case '-': *p = '>'; break;
    case '\\': *p = 'v'; break;
    case '/': *p = '^'; break;
    case '+':
      switch (cart->turn) {
      case 0: *p = '^'; cart->turn = 1; break;
      case 1: *p = '>'; cart->turn = 2; break;
      case 2: *p = 'v'; cart->turn = 0; break;
      default: assert(0);
      }
      break;
    default:
      goto collide;
    }
    break;
  default:
    assert(0);
  }
  return 0;
 collide:
  printf("tick %d: collision at coordinate %d,%d\n", ticks, cart->x, cart->y);
  debug("removing %d\n", cart->id);
  cart->turn = 3;
  return 1;
}

int main(int argc, char **argv) {
  size_t len = 0, count = 0;
  char *line;
  char *p;
  int ncarts = 0;
  int ticks = 0;
  int i;

  /* Part 1 - read in map */
  if (argc > 1 && !(stdin = freopen(argv[1], "r", stdin))) {
    perror("failure");
    exit(2);
  }

  while (getline(&line, &len, stdin) >= 0) {
    p = strspn(line, "^v<> /\\|-+") + line;
    if (*p != '\n' || p - line > LIMIT) {
      fprintf(stderr, "recompile with larger limit\n");
      exit(1);
    }
    p = strcspn(line, "^v<>\n") + line;
    while (*p != '\n') {
      carts[ncarts].x = p - line;
      carts[ncarts].y = count;
      if (*p == '^' || *p == 'v')
	carts[ncarts].current = '|';
      else
	carts[ncarts].current = '-';
      carts[ncarts].id = ncarts;
      ncarts++;
      p++;
      p = strcspn(p, "^v<>\n") + p;
    }
    memcpy(map[count++], line, p - line);
    if (count == LIMIT) {
      fprintf(stderr, "recompile with larger limit\n");
      exit(1);
    }
  }
  printf("read %zu lines, found %d carts\n", count, ncarts);
  qsort(carts, ncarts, sizeof *carts, sorter);
  dump(ticks, count, ncarts);
  if (!(ncarts % 2)) {
    fprintf(stderr, "expecting odd number of carts\n");
    exit(1);
  }

  /* Part 2 - iterate until last collision */
  int n = ncarts;
  while (n > 1) {
    ticks++;
    for (i = 0; i < ncarts; i++)
      if (carts[i].turn < 3 && move(ticks, &carts[i]))
	for (int j = 0; j < ncarts; j++)
	  if (i != j && carts[j].turn < 3 &&
	      carts[i].x == carts[j].x && carts[i].y == carts[j].y) {
	    debug("removing %d\n", carts[j].id);
	    carts[j].turn = 3;
	    map[carts[i].y][carts[i].x] = carts[j].current;
	    n -= 2;
	  }
    qsort(carts, ncarts, sizeof *carts, sorter);
    dump(ticks, count, n);
  }
  printf("completed in %d ticks, last cart at %d,%d\n", ticks, carts->x,
	 carts->y);
  return 0;
}
