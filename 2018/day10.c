#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <assert.h>

void debug(const char *fmt, ...) {
  va_list ap;
  if (getenv("DEBUG")) {
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
  }
}

#define LIMIT 300

static struct data {
  int x;
  int y;
  int vx;
  int vy;
} list[LIMIT];

void display(int count, int y, int minx, int maxx) {
  int i, j;
  for (i = minx; i <= maxx; i++) {
    char c = '.';
    for (j = 0; j < count; j++)
      if (list[j].y == y && list[j].x == i)
	c = '#';
    putchar(c);
  }
  putchar('\n');
}

int main(int argc, char **argv) {
  size_t len = 0, count = 0;
  char *line;
  int i;
  int minx, miny, maxx, maxy, gap;
  int iters = 0;

  /* Part 1 - read in lines */
  if (argc > 1)
    if (!(stdin = freopen(argv[1], "r", stdin))) {
      perror("failure");
      exit(2);
    }

  while (getline(&line, &len, stdin) >= 0) {
    if (sscanf(line, "position=<%d,%d> velocity=<%d,%d> ", &list[count].x,
	       &list[count].y, &list[count].vx, &list[count].vy) != 4) {
      fprintf(stderr, "bad input\n");
      exit(1);
    }
    ++count;
  }
  printf("Read %zu lines\n", count);

  /* Part 2 - minimize gap */
  minx = miny = 100000;
  maxx = maxy = -100000;
  gap = 200000;
  for (i = 0; i < count; i++) {
    if (list[i].y > maxy)
      maxy = list[i].y;
    if (list[i].y < miny)
      miny = list[i].y;
    if (list[i].x > maxx)
      maxx = list[i].x;
    if (list[i].x < minx)
      minx = list[i].x;
  }
  printf("Starting with gap %d, column %d\n", maxy - miny, minx);
  do {
    iters++;
    gap = maxy - miny;
    minx = miny = 100000;
    maxx = maxy = -100000;
    for (i = 0; i < count; i++) {
      list[i].x += list[i].vx;
      list[i].y += list[i].vy;
      if (list[i].y > maxy)
	maxy = list[i].y;
      if (list[i].y < miny)
	miny = list[i].y;
      if (list[i].x > maxx)
	maxx = list[i].x;
      if (list[i].x < minx)
	minx = list[i].x;
    }
  } while (maxy - miny < gap);
  printf("After %d iters, gap is %d, column %d\n", iters, gap, minx);

  /* Part 3 - undo last iteration, and print array */
  for (i = 0; i < count; i++) {
    list[i].x -= list[i].vx;
    list[i].y -= list[i].vy;
  }
  for (i = miny - 2; i < maxy + 2; i++)
    display(count, i, minx - 3, maxx + 3);
  return 0;
}
