#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <inttypes.h>
#include <assert.h>

void debug(const char *fmt, ...) {
  va_list ap;
  if (getenv("DEBUG")) {
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
  }
}

#define MAXX 700
#define MAXY 2000
#define MIN(a,b) ((a)<(b)?(a):(b))
#define MAX(a,b) ((a)>(b)?(a):(b))

static char grid[MAXY][MAXX];
static int calls;
static int tiles;

static void dump(int minx, int miny, int maxx, int maxy) {
  int i;
  if (!getenv("DEBUG"))
    return;
  debug("%4d: % *c\n", 0, 500 - minx + 1, '+');
  for (i = miny; i <= maxy; i++)
    debug("%4d: %.*s\n", i, maxx - minx + 1, &grid[i][minx]);
}

static void fill(int x, int y, int minx, int miny, int maxx, int maxy) {
  int l, r, i;
  calls++;
  debug("fill(%d,%d) at %c above %c\n", x, y, grid[y][x], grid[y + 1][x]);
  if (y > maxy)
    return;
  if (grid[y + 1][x] == '|') {
    debug("point %d,%d already being filled elsewhere\n", x, y + 1);
    dump(MAX(minx, x - 5), MAX(0, y - 5), MIN(maxx, x + 5), y + 2);
    return;
  }
  if (grid[y + 1][x] == '.') {
    if (y + 1 >= miny && y + 1 <= maxy)
      tiles++;
    grid[y + 1][x] = '|';
    fill(x, y + 1, minx, miny, maxx, maxy);
    return;
  }
  assert(y >= miny && y <= maxy);
  for (l = x; l >= minx; l--) {
    if (grid[y][l - 1] == '#' ||
	grid[y + 1][l - 1] == '.' || grid[y + 1][l - 1] == '|')
      break;
  }
  for (r = x; r <= maxx; r++) {
    if (grid[y][r + 1] == '#' ||
	grid[y + 1][r + 1] == '.' || grid[y + 1][r + 1] == '|')
      break;
  }
  debug("found bounds %d to %d\n", l, r);
  if (grid[y][l - 1] == '#' && grid[y][r + 1] == '#') {
    for (i = l; i <= r; i++) {
      if (grid[y][i] == '.')
	tiles++;
      grid[y][i] = '~';
    }
    dump(MAX(minx, l - 2), MAX(0, y - 5), MIN(maxx, r + 2), y + 2);
    fill(x, y - 1, minx, miny, maxx, maxy);
    return;
  }
  for (i = l; i <= r; i++) {
    if (grid[y][i] == '.')
      tiles++;
    grid[y][i] = '|';
  }
  if (grid[y][l - 1] == '.') {
    tiles++;
    grid[y][l - 1] = '|';
    debug("spilling left from %d,%d\n", x, y);
    dump(MAX(minx, l - 2), MAX(0, y - 5), MIN(maxx, r + 2), y + 2);
    fill(l - 1, y, minx, miny, maxx, maxy);
  }
  if (grid[y][r + 1] == '.') {
    tiles++;
    grid[y][r + 1] = '|';
    debug("spilling right from %d,%d\n", x, y);
    dump(MAX(minx, l - 2), MAX(0, y - 5), MIN(maxx, r + 2), y + 2);
    fill(r + 1, y, minx, miny, maxx, maxy);
  }
}

int main(int argc, char **argv) {
  size_t len = 0, count = 0;
  char *line;
  char a, b;
  int minx = 500, maxx = 500, miny = 10000, maxy = 0;
  int q, r, s;
  int x1, x2, y1, y2;

  /* Part 0 - read in map */
  if (argc > 1 && !(stdin = freopen(argv[1], "r", stdin))) {
    perror("failure");
    exit(2);
  }

  memset(grid, '.', sizeof grid);
  while (getline(&line, &len, stdin) >= 0) {
    count++;
    if (sscanf(line, "%c=%d, %c=%d..%d\n", &a, &q, &b, &r, &s) != 5
	|| a == b || r >= s) {
      fprintf(stderr, "bad input: %s\n", line);
      exit(1);
    }
    if (a == 'x') {
      x1 = x2 = q;
      y1 = r;
      y2 = s;
      for (q = y1; q <= y2; q++)
	grid[q][x1] = '#';
    } else {
      assert(b == 'x');
      x1 = r;
      x2 = s;
      y1 = y2 = q;
      for (q = x1; q <= x2; q++)
	grid[y1][q] = '#';
    }
    debug("clay at %d,%d to %d,%d\n", x1, y1, x2, y2);
    if (x1 < minx)
      minx = x1;
    if (x2 > maxx)
      maxx = x2;
    if (y1 < miny)
      miny = y1;
    if (y2 > maxy)
      maxy = y2;
    if (minx < 1 || maxx > MAXX - 1 || miny < 1 || maxy > MAXY - 1) {
      fprintf(stderr, "recompile with larger limit\n");
      exit(1);
    }
  }
  minx--;
  maxx++;
  printf("read %zu lines, bounds: %d,%d to %d,%d\n",
	 count, minx, miny, maxx, maxy);
  dump(minx, miny, maxx, maxy);

  /* Part 2 - fill grid */
  if (argc > 2)
    maxy = atoi(argv[2]);
  grid[0][500] = '|';
  fill(500, 0, minx, miny, maxx, maxy);
  dump(minx, miny, maxx, maxy);
  printf("completed %d calls, change %d tiles\n", calls, tiles);
  q = count = 0;
  for (s = miny; s <= maxy; s++)
    for (r = minx; r <= maxx; r++) {
      if (grid[s][r] == '~' || grid[s][r] == '|')
	count++;
      if (grid[s][r] == '~')
	q++;
    }
  printf("found %zu points touched by water, %d retained\n", count, q);
  return 0;
}
