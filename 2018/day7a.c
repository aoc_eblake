#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>

void debug(const char *fmt, ...) {
  va_list ap;
  if (getenv("DEBUG")) {
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
  }
}

static struct data {
  char state;
  char wait[26];
} list[26];

int main(int argc, char **argv) {
  size_t len = 0, count = 0;
  char *line;
  int i, j;
  char a, b;
  char *p;

  /* Part 1 - read in lines */
  if (argc > 1)
    if (!(stdin = freopen(argv[1], "r", stdin))) {
      perror("failure");
      exit(2);
    }

  while (getline(&line, &len, stdin) >= 0) {
    ++count;
    if (sscanf(line, "Step %c must be finished before step %c can begin. ", &a,
	       &b) != 2 || (unsigned)(a-'A') > 25 || (unsigned)(b-'A') > 25) {
      fprintf(stderr, "bad input\n");
      exit(1);
    }
    list[a - 'A'].state = 1;
    list[b - 'A'].state = 1;
    strchrnul(list[b - 'A'].wait, '\0')[0] = a;
  }
  printf("Read %zu lines\n", count);

  /* Part 2 - determine output */
  for (i = 0; i < 26; i++)
    for (j = 0; j < 26; j++) {
      if (list[j].state != 1)
	continue;
      p = list[j].wait;
      while (*p) {
	if (list[*p - 'A'].state < 2)
	  break;
	p++;
      }
      if (*p)
	continue;
      list[j].state = 2;
      printf("%c", j + 'A');
      break;
    }
  printf("\n");
  return 0;
}
