divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day16.input] day16.m4

include(`common.m4')ifelse(common(16), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`r', `r$1')define(`i', `$1_')define(`z', `0_')define(`_')
define(`r0', `$1')define(`r1', `$2')define(`r2', `$3')define(`r3', `$4')
define(`o0', ``r',+,`r'')define(`o1', ``r',+,`i'')
define(`o2', ``r',*,`r'')define(`o3', ``r',*,`i'')
define(`o4', ``r',&,`r'')define(`o5', ``r',&,`i'')
define(`o6', ``r',|,`r'')define(`o7', ``r',|,`i'')
define(`o8', ``r',-,`z'')define(`o9', ``i',-,`z'')
define(`o10', ``i',>,`r'')define(`o11', ``r',>,`i'')
define(`o12', ``r',>,`r'')define(`o13', ``i',==,`r'')
define(`o14', ``r',==,`i'')define(`o15', ``r',==,`r'')
define(`o', `eval($1($4)$6$2$3($5)$6)')
define(`test', `ifelse(eval(len('forloop(0, 15, ``_$0('', ``$'@`)'')`) >= 3),
  1, `define(`part1', incr(part1))')')
define(`_test', `ifelse(o(o$1(), $4, $5, $2), r$6$9, -, `define(`p$3',
  eval(p$3 & ~(1 << $1)))')')
define(`s0', `define(`R', `$5,$2,$3,$4')')
define(`s1', `define(`R', `$1,$5,$3,$4')')
define(`s2', `define(`R', `$1,$2,$5,$4')')
define(`s3', `define(`R', `$1,$2,$3,$5')')
define(`R', `0,0,0,0')
forloop(0, 16, `define(`p'', `, `65535')')
define(`settle', `ifelse(p16, 0, `', `forloop_arg(0, 15, `_$0')$0()')')
define(`_settle', `$0_((p$1&p16), $1)')
define(`_settle_', `ifelse(eval(`$1&&($1&-$1)==$1'), 1, `define(`p16',
  eval(p16&~$1))define(`m$2', `o'decr(len(eval(`$1', 2))))')')

define(`Before')define(`After')
define(`input', translit((include(defn(`file'))), nl`,:()', `;'))
define(`init', substr(defn(`input'), 0, incr(incr(index(defn(`input'),
  `;;;;')))))
define(`list', substr(defn(`input'), eval(4+index(defn(`input'), `;;;;'))))
define(`part1', 0)
define(`visit', `test(translit(`$1', ` ;[]', `,,()'))')
define(`op', `s$4(R, o(m$1, $2, $3, (R)))')
ifdef(`__gnu__', `
  patsubst(defn(`init'), `\([^;]*;[^;]*;[^;]*\);;', `visit(`\1')')
  settle()
  define(`d', `\([0-9][0-9]*\)')
  patsubst(defn(`list'), d` 'd` 'd` 'd`;', `op(`\1', `\2', `\3', `\4')')
', `
  define(`_chew', `visit(substr(`$1', 0, index(`$1', `;;')))define(`tail',
    substr(`$1', incr(incr(index(`$1', `;;')))))ifelse(index(defn(`tail'),
    `;;'), -1, `', `$0(defn(`tail'))')')
  define(`chew', `ifelse(eval(`$1 < 85'), 1, `_$0(`$2')', `$0(eval(`$1/2'),
    substr(`$2', 0, eval(`$1/2')))$0(eval(len(defn(`tail'))` + $1 - $1/2'),
    defn(`tail')substr(`$2', eval(`$1/2')))')')
  chew(len(defn(`init')), defn(`init'))
  settle()
  define(`_chew', `op(translit(substr(`$1', 0, index(`$1', `;')), ` ',
    `,'))define(`tail', substr(`$1', incr(index(`$1', `;'))))ifelse(index(
    defn(`tail'), `;'), -1, `', `$0(defn(`tail'))')')
  define(`chew', `ifelse(eval(`$1 < 20'), 1, `_$0(`$2')', `$0(eval(`$1/2'),
    substr(`$2', 0, eval(`$1/2')))$0(eval(len(defn(`tail'))` + $1 - $1/2'),
    defn(`tail')substr(`$2', eval(`$1/2')))')')
  chew(len(defn(`list')), defn(`list'))
')
define(`part2', r0(R))

divert`'part1
part2
