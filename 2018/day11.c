#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <assert.h>

void debug(const char *fmt, ...) {
  va_list ap;
  if (getenv("DEBUG")) {
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
  }
}

#define LIMIT 300

static struct point {
  int8_t value;
  int row[LIMIT];
} grid[LIMIT][LIMIT];
static struct best {
  int x;
  int y;
  int sum;
} best[LIMIT];

static int8_t level(int x, int y, int s) {
  int id = x + 10;
  int p = (id * y + s) * id;
  return (p % 1000) / 100 - 5;
}

static void rows(int x, int y) {
  grid[x][y].row[0] = grid[x][y].value;
  for (int i = 1; i < LIMIT - y; i++)
    grid[x][y].row[i] = grid[x][y].row[i - 1] + grid[x][y + i].value;
}

static void find(int size) {
  int i, j;
  bool dup = false;
  best[size - 1].sum = -5 * size * size;
  for (j = 0; j < LIMIT - size + 1; j++) {
    int sum = 0;
    for (i = 0; i < size; i++)
      sum += grid[i][j].row[size - 1];
    for (i = 0; i < LIMIT - size + 1; i++) {
      if (i)
	sum += grid[i + size - 1][j].row[size - 1] -
	  grid[i - 1][j].row[size - 1];
      if (sum > best[size - 1].sum) {
	best[size - 1].sum = sum;
	best[size - 1].x = i + 1;
	best[size - 1].y = j + 1;
	dup = false;
      } else if (sum == best[size - 1].sum)
	dup = true;
    }
  }
  if (dup)
    debug("ignoring duplicate best candidates for size %d\n", size);
}

int main(int argc, char **argv) {
  int s = 1955;
  int i, j;
  int max = 0;

  if (argc > 1)
    s = atoi(argv[1]);
  for (j = 0; j < LIMIT; j++)
    for (i = 0; i < LIMIT; i++)
      grid[i][j].value = level(i + 1, j + 1, s);
  for (j = 0; j < LIMIT; j++)
    for (i = 0; i < LIMIT; i++)
      rows(i, j);
  for (i = 0; i < LIMIT; i++) {
    debug("%d\n", i);
    find(i + 1);
    if (best[i].sum > best[max].sum)
      max = i;
    else if (best[i].sum == best[max].sum)
      debug("size %d has same best as %d\n", i, max);
  }
  printf("best at %d,%d,%d with %d\n", best[max].x, best[max].y, max + 1,
	 best[max].sum);
  return 0;
}
