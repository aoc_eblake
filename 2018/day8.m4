divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day8.input] day8.m4

include(`common.m4')ifelse(common(8), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`input', translit(include(defn(`file')), nl` ', `;;'))
define(`part1', 0)
define(`nodes', 1)define(`children', 1)
define(`rdmeta', `define(`part1', eval(part1 + $1))ifelse(children, 0,
  `define(`sum', eval(sum + $1))', $1, 0, `', eval($1 > children), 1, `',
  `define(`sum', eval(sum + c$1))')define(`meta', decr(meta))')
define(`rdhead', `ifdef(`n', `define(`nodes', decr(nodes))pushdef(`nodes',
  n)pushdef(`children', n)pushdef(`meta', $1)pushdef(`sum', 0)popdef(`n')',
  `define(`n', $1)')')
define(`clean', `ifelse(children, 0, `', `forloop(1, children, `popdef(`c'',
  `)')')popdef(`children')popdef(`nodes')popdef(`meta')pushdef(`c'eval(
  children - nodes), sum)popdef(`sum')')
define(`visit', `ifelse(nodes, 0, `rdmeta', `rdhead')(`$1')ifelse(nodes`'meta,
  00, `clean()')')
ifdef(`__gnu__', `
  patsubst(defn(`input'), `\([^;]*\);', `visit(`\1')')
',`
  define(`_chew', `visit(substr(`$1', 0, index(`$1', `;')))define(`tail',
    substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(`tail'), `;'), -1,
    `', `$0(defn(`tail'))')')
  define(`chew', `ifelse(eval(`$1 < 6'), 1, `_$0(`$2')', `$0(eval(`$1/2'),
    substr(`$2', 0, eval(`$1/2')))$0(eval(len(defn(`tail'))` + $1 - $1/2'),
    defn(`tail')substr(`$2', eval(`$1/2')))')')
  chew(len(defn(`input')), defn(`input'))
')
define(`part2', c1)

divert`'part1
part2
