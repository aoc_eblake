divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day20.input] day20.m4

include(`common.m4')ifelse(common(20), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`input', translit(include(defn(`file')), `(|)^$'nl, `OPC'))
define(`x', 5000)define(`y', 5000)define(`d', 0)
define(`part1', 0)define(`part2', 0)
define(`door', `ifdef(`g$1_$2', `define(`d', g$1_$2)', `define(`g$1_$2',
  $3)define(`d', $3)ifelse(eval($3 > part1), 1, `define(`part1',
  $3)')ifelse(len($3), 4, `define(`part2', incr(part2))')')define(`x',
  $1)define(`y', $2)')
define(`N_', `door($1, decr($2), incr($3))')
define(`E_', `door(incr($1), $2, incr($3))')
define(`S_', `door($1, incr($2), incr($3))')
define(`W_', `door(decr($1), $2, incr($3))')
define(`O_', `pushdef(`x', $1)pushdef(`y', $2)pushdef(`d', $3)')
define(`P_', `C_()O_(x, y, d)')
define(`C_', `popdef(`x')popdef(`y')popdef(`d')')
ifdef(`__gnu__', `
  patsubst(defn(`input'), `.', `\&_(x, y, d)')
',`
  define(`chew', `ifelse($1, 1, `$2_(x, y, d)', `$0(eval($1/2), substr(
    `$2', 0, eval($1/2)))$0(eval($1-$1/2), substr(`$2', eval($1/2)))')')
  chew(len(defn(`input')), defn(`input'))
')

divert`'part1
part2
