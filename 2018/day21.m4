divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day21.input] day21.m4

include(`common.m4')ifelse(common(21), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`input', translit(include(defn(`file')), nl`#', `;'))
define(`ip', `r'substr(defn(`input'), 3, 1))
define(`list', substr(defn(`input'), 5))
define(`cnt', 0)
define(`visit', `_$0(cnt, translit(`$1', ` ', `,'))')
define(`_visit', `define(`i$1', `$2')define(`a$1', `$3,$4,$5')define(
  `cnt', incr($1))')
ifdef(`__gnu__', `
  patsubst(defn(`list'), `\([^;]*\);', `visit(`\1')')
', `
  define(`_chew', `visit(substr(`$1', 0, index(`$1', `;')))define(`tail',
    substr(`$1', incr(index(`$1', `;'))))ifelse(index(
    defn(`tail'), `;'), -1, `', `$0(defn(`tail'))')')
  define(`chew', `ifelse(eval(`$1 < 20'), 1, `_$0(`$2')', `$0(eval(`$1/2'),
    substr(`$2', 0, eval(`$1/2')))$0(eval(len(defn(`tail'))` + $1 - $1/2'),
    defn(`tail')substr(`$2', eval(`$1/2')))')')
  chew(len(defn(`list')), defn(`list'))
')
forloop(0, 5, `define(`r'', `, 0)')
define(`addr_', `define(`r$3', eval(r$1+r$2))')
define(`addi_', `define(`r$3', eval(r$1+$2))')
define(`mulr_', `define(`r$3', eval(r$1*r$2))')
# Peephole optimization: rather than loop by 1 until one register * 256 is
# greater than another, just perform the truncating division on the first pass
define(`muli_', `define(`r$3', eval(r$1*$2))ifelse($2, 256, `hack(decr(ip),
  incr(ip))')')
define(`hack', `ifelse(defn(`i$1')defn(`i$2'), `addigtrr', `_$0(a$1, a$2)')')
define(`_hack', `define(`r$1', eval(r$5>>8))define(`r$4', incr(r$5))')
define(`banr_', `define(`r$3', eval(r$1&r$2))')
define(`bani_', `define(`r$3', eval(r$1&$2))')
define(`borr_', `define(`r$3', eval(r$1|r$2))')
define(`bori_', `define(`r$3', eval(r$1|$2))')
define(`setr_', `define(`r$3', r$1)')
define(`seti_', `define(`r$3', $1)')
define(`gtir_', `define(`r$3', eval($1>r$2))')
define(`gtri_', `define(`r$3', eval(r$1>$2))')
define(`gtrr_', `define(`r$3', eval(r$1>r$2))')
define(`eqir_', `define(`r$3', eval($1==r$2))')
define(`eqri_', `define(`r$3', eval(r$1==$2))')
# Peephole optimization: instead of trying different r0 starting values,
# see what value r0 is expected to have, and pretend it did.
define(`eqrr_', `define(`r$3', eval(r$1==r$2))ifelse($2, 0, `hack2(r$1, $3)')')
define(`hack2', `ifdef(`part1', `ifdef(`h$1', `define(`r$2', 1)', `define(
  `part2', $1)define(`h$1')')', `define(`part1', $1)')')
define(`run', `_$0(ip)')
define(`_run', `ifdef(`i$1', `i$1()_(a$1)define(defn(`ip'), incr(ip))$0(ip)')')
run()

divert`'part1
part2
