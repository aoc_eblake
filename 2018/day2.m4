divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dhashsize=H] [-Dfile=day2.input] day2.m4

include(`common.m4')ifelse(common(2, 65537), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`list', translit(include(defn(`file')), alpha`'nl, ALPHA`;'))
define(`d2', 0)define(`d3', 0)
define(`visit', `_$0(forloop(0, 25, `count(', `, `$1')').)')
define(`_visit', `ifelse(index(`$1', `.2.'), `-1', `', `define(`d2',
  incr(d2))')ifelse(index(`$1', `.3.'), `-1', `', `define(`d3', incr(d3))')')
define(`count', `.len(translit(`$2', substr('dquote(ALPHA)`, `$1',
  1)`''dquote(ALPHA)`, `-'))_$0(substr(`$2', 0, `$1')`_'substr(`$2',
  incr(`$1')))')
define(`_count', `ifdef(`$1', `define(`part2', translit(`$1',
  ALPHA`_', alpha))', `define(`$1')')')
ifdef(`__gnu__', `
  patsubst(defn(`list'), `\([^;]*\);', `visit(`\1')')
', `
  define(`half', `eval($1/26/2*26)')
  define(`chew', `ifelse(`$1', `26', `visit(`$2')', `$0(half(`$1'), substr(
    `$2', 0, half(`$1')))$0(eval($1-half(`$1')), substr(`$2', half(`$1')))')')
  chew(eval(len(defn(`list'))/27*26), translit(defn(`list'), `;'))
')
define(`part1', eval(d2*d3))

divert`'part1
part2
