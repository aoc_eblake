#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>

void debug(const char *fmt, ...) {
  va_list ap;
  if (getenv("DEBUG")) {
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
  }
}

#define LIMIT 1000

char array[LIMIT][LIMIT];

static void process(const char *str) {
  int a, b, c, d, e;
  int i, j;
  if (sscanf(str, "#%d @ %d,%d: %dx%d\n", &a, &b, &c, &d, &e) != 5) {
    printf("bad input!\n");
    exit(1);
  }
  if (b + d > LIMIT || c + e > LIMIT) {
    printf("recompile with larger LIMIT!\n");
    exit(1);
  }
  debug("processing %d,%d: %dx%d\n", b, c, d, e);
  for (i = b; i < b + d; i++)
    for (j = c; j < c + e; j++)
      array[j][i]++;
}

int main(int argc, char **argv) {
  size_t len = 0, count = 0;
  char *line;
  int i, j;

  if (argc > 1)
    if (!(stdin = freopen(argv[1], "r", stdin))) {
      perror("failure");
      exit(2);
    }

  while (getline(&line, &len, stdin) >= 0) {
    count++;
    process(line);
  }
  printf("read %zd lines\n", count);
  count = 0;
  for (i = 0; i < LIMIT; i++)
    for (j = 0; j < LIMIT; j++) {
      debug("%d %d %d\n", i, j, array[j][i]);
      if (array[j][i] > 1)
	count++;
    }
  printf("found %zd overlaps\n", count);
  return 0;
}
