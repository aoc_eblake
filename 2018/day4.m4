divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dhashsize=H] [-Dfile=day4.input] day4.m4

include(`common.m4')ifelse(common(4, 65537), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`n0228', `0301')
define(`n0331', `0401')
define(`n0430', `0501')
define(`n0531', `0601')
define(`n0630', `0701')
define(`n0731', `0801')
define(`n0831', `0901')
define(`n0930', `1001')
define(`n1031', `1101')
define(`norm', `ifelse(`$3', `23', `ifdef(`n$1$2', `n$1$2',
  `$1eval(`(1$2+1)%100', 10, 2)')', `$1$2')')
define(`sort', `ifelse(`$2', `', ``,`$1''', `ifelse(eval(`$1 < $2'), 1,
  ``,$@'', ``,`$2''$0(`$1', shift(shift($@)))')')')
define(`line', `ifelse(`$6', `Guard', `guard(norm($2, $3, $4), $7)',
  `time($2$3, incr(decr($5)))')')
define(`guard', `define(`g$1', `$2')ifdef(`G$2', `', `define(`G$2',
  0)pushdef(`guards', `$2')')')
define(`time', `define(`l$1', sort(`$2'ifdef(`l$1', `l$1', `pushdef(`days',
  `$1')')))')
define(`_1518', `line(')
translit(include(defn(`file')), `- :'nl`[]#', `,,,)_')

define(`bestt', 0)define(`bestv', 0)
define(`addg', `_$0(`$1', eval(G$1` + $3 - $2'))')
define(`_addg', `define(`G$1', `$2')ifelse(eval($2 > bestt), 1,
  `define(`bestt', `$2')define(`bestg', `$1')')')
define(`bump', `define(`m$1_$2', incr(0defn(`m$1_$2')))ifelse(eval(
  m$1_$2 > bestv), 1, `define(`bestv', m$1_$2)define(`bestG',
  `$1')define(`bestm', `$2')')')
define(`map', `_$0(g$1()l$1)')
define(`_map', `ifelse(`$2', `', `', `forloop($2, decr($3), `bump(`$1', ',
  `)')addg(`$1', `$2', `$3')$0(`$1', shift(shift(shift($@))))')')
stack_foreach(`days', `map')
define(`part2', eval(bestG * bestm))
define(`bestv', 0)
define(`find', `ifdef(`m$1_$2', `ifelse(eval(m$1_$2 > bestv), 1,
  `define(`bestv', m$1_$2)define(`bestm', `$2')')')')
forloop(0, 59, `find('bestg`, ', `)')
define(`part1', eval(bestg * bestm))

divert`'part1
part2
