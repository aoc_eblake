divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day25.input] day25.m4
# Optionally use -Dverbose=1 to see some progress
# Optionally use -Dalgo=quad|linear to choose search algorithm
# Optionally use -Dunion=direct|rank to choose union-find algorithm

include(`common.m4')ifelse(common(25), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

ifdef(`algo', `', `define(`algo', `linear')')
ifdef(`union', `', `define(`union', `direct')')

define(`input', translit((include(defn(`file'))), nl`,()', `;.'))
define(`cnt', 0)define(`min', 0)define(`max', 0)
define(`bump', `ifelse(eval($1<min), 1, `define(`min', $1)', eval($1>max), 1,
  `define(`max', $1)')')
define(`line', `_$0(cnt, translit(`$1', `.', `,'))')
define(`_line', `define(`p$1', `$2,$3,$4,$5')define(`cnt', incr($1))bump(
  $2)bump($3)bump($4)bump($5)')
ifdef(`__gnu__', `
  patsubst(defn(`input'), `\([^;]*\);', `line(`\1')')
', `
  define(`_chew', `line(substr(`$1', 0, index(`$1', `;')))define(`tail',
    substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(`tail'), `;'), -1,
    `', `$0(defn(`tail'))')')
  define(`chew', `ifelse(eval(`$1 < 24'), 1, `_$0(`$2')', `$0(eval(`$1/2'),
    substr(`$2', 0, eval(`$1/2')))$0(eval(len(defn(`tail'))` + $1 - $1/2'),
    defn(`tail')substr(`$2', eval(`$1/2')))')')
  chew(len(defn(`input')), defn(`input'))
')

define(`progress', `ifelse(eval($1%100), 0, `output(1, ...$1)')')
define(`diff', ``($1<$2)*($2- $1)+($2<$1)*($1- $2)'')
define(`dist', `diff($1, $5)+diff($2, $6)+diff($3, $7)+diff($4, $8)')

ifelse(union, `direct', `
output(1, `using linear union-find by child chaining')
# Naive union-find, with worst-case O(n) find
define(`makeset', `define(`c$1', $1)')
define(`merge', `_$0(c$1, c$2)')
define(`_merge', `ifelse($1, $2, `', eval($2>$1), 1, `define(`c$2', `c$1')',
  `define(`c$1', `c$2')')')
define(`_sets', `ifdef(`C$1', `', `-define(`C$1')')')
define(`sets', `len(forloop(0, decr(cnt), `_$0(first(`c'', `))'))')

', union, `rank', `
output(1, `using amortized constant union-find by rank and path compression')
# Amortized O(1) Union-find with path compression and union by rank
# A `r'oot node has a rank; a `c'hild node has a parent; also track sets count
define(`sets', 0)
define(`makeset', `define(`sets', incr(sets))define(`r$1', 0)')
define(`find', `ifdef(`r$1', `$1, r$1', `_$0($1, c$1)')')
define(`_find', `ifdef(`r$2', `$2, r$2', `$0_($1, c$2)')')
define(`_find_', `define(`c$1', $2)find($2)')
define(`merge', `_$0(find($1), find($2))')
define(`_merge', `ifelse($1, $3, `', `$0_(ifelse(eval($2 < $4), 1, `$3, $1',
  $2, $4, `$1, $3define(`r$1', incr($2))', `$1, $3'))')')
define(`_merge_', `define(`c$2', $1)popdef(`r$2')define(`sets', decr(sets))')

', `fatal(`unrecognized -Dunion= value')')

ifelse(algo, `quad', `
output(1, `using quadratic pair-wise comparison')
define(`check', `progress($1)makeset($1)forloop(0, decr($1), `_$0(', `, $1)')')
define(`_check', `ifelse(eval(dist(p$1, p$2) <= 3), 1, `merge($1, $2)')')
makeset(0)
forloop_arg(1, decr(cnt), `check')

', algo, `linear', `
output(1, `using linear grid comparison')
# Valid input files have abs(coord)<=8; the sample file has coords [0-12]
ifelse(eval(max - min > 17), 1, `fatal(`input grid too large')')
define(`offset', eval(3 - min))
# Map 4D points into 1D space with all positive coordinates
define(`map', `_$0(p$1, 'offset`)')
define(`_map', `eval(($1+$5)+($2+$5)*23+($3+$5)*23*23+($4+$5)*23*23*23)')
define(`mark', `define(`g'map($1), $1)makeset($1)')
forloop_arg(0, decr(cnt), `mark')
# Pre-compute the list of all base-23 offsets that form all 128 neighbors
# within Manhattan distance 3 of the given point. This comes from:
# 0,0,0,3 * 4 orderings * 2 signs
# 0,0,0,1 * 4 orderings * 2 signs
# 0,0,1,1 * 6 orderings * 4 signs
# 0,0,0,2 * 4 orderings * 2 signs
# 0,0,1,2 * 12 orderings * 4 signs
# 0,1,1,1 * 4 orderings * 8 signs
# But rather than list all 128 variations manually, comb through a
# 5x5x5x5 hypercube for 120 of the neighbors.
_foreach(`pushdef(`list', _map(first(', `), 0))', `', `0,0,0,3', `0,0,0,-3',
  `0,0,3,0', `0,0,-3,0', `0,3,0,0', `0,-3,0,0', `3,0,0,0', `-3,0,0,0')
define(`prep', `ifelse($1, 2222, `', `translit(`_$0(A,B,C,D)', `ABCD', $1)')')
define(`_prep', `ifelse(eval(dist($1,$2,$3,$4,2,2,2,2)<=3), 1, `pushdef(`list',
  _map($1, $2, $3, $4, -2))')')
forloop(0, eval(5*5*5*5-1), `prep(eval(', `, 5, 4))')
define(`_near', ``check(eval($4$1+$3), $4$2)'')
define(`near', _stack_foreach(`list', `_near(1, 2, ', `, `$')', `t'))
define(`process', `progress($1)near(map($1), $1)')
define(`check', `ifdef(`g$1', `merge($2, g$1)')')
forloop_arg(0, decr(cnt), `process')

', `fatal(`unrecognized -Dalgo= value')')

define(`part1', sets)

divert`'part1
no part2
