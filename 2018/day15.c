#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <inttypes.h>
#include <assert.h>

void debug(const char *fmt, ...) {
  va_list ap;
  if (getenv("DEBUG")) {
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
  }
}

#define LIMIT 33 /* Grid size */
#define MAX 30 /* Initial units */

static uint8_t map[LIMIT][LIMIT + 1];
static uint8_t distance[LIMIT][LIMIT];
static uint8_t from[LIMIT][LIMIT];
static struct unit {
  char id;
  char type; /* E, G, or X */
  uint8_t x; /* coordinate on map */
  uint8_t y;
  short hp;
  short attack;
} units[MAX];

static int sorter(const void *one, const void *two) {
  const struct unit *a = one;
  const struct unit *b = two;
  if (a->type != 'X' && b->type == 'X')
    return -1;
  if (a->type == 'X' && b->type != 'X')
    return 1;
  if (a->y < b->y)
    return -1;
  if (a->y > b->y)
    return 1;
  if (a->x < b->x)
    return -1;
  if (a->x > b->x)
    return 1;
  return 0;
}

/* Return character type of enemy */
static char enemy(struct unit *u) {
  assert(u->type != 'X');
  return "EG"[u->type == 'E'];
}

/* Return bitmap of spots where c is found: 1 up, 2 left, 4 right, 8 down */
static int nextto(int x, int y, char c) {
  int ret = 0;
  assert(x && x < LIMIT && y && y < LIMIT);
  if (map[y - 1][x] == c)
    ret |= 1;
  if (map[y][x - 1] == c)
    ret |= 2;
  if (map[y][x + 1] == c)
    ret |= 3;
  if (map[y + 1][x] == c)
    ret |= 4;
  return ret;
}

/* Check for a best candidate */
static int check(int old, int x, int y) {
  if (old == 0 || y < (old & 0xff) ||
      (y == (old & 0xff) && (x << 8 < (old & 0xff00))))
    return (from[y][x] << 16) | (x << 8) | y;
  return old;
}

/* Return (dir << 16) | (x << 8) | y of the current position or best '.'
   position that neighbors @c, else -1 */
static int locate(int startx, int starty, char c) {
  int i, x, y;
  int best = 0;
  bool found = true;
  if (nextto(startx, starty, c))
    return (startx << 8) | starty;
  memset(distance, -1, sizeof(distance));
  memset(from, 0, sizeof(from));
  i = distance[starty][startx] = 1;
  from[starty - 1][startx] = 1;
  from[starty][startx - 1] = 2;
  from[starty][startx + 1] = 4;
  from[starty + 1][startx] = 8;
  while (!best && found) {
    found = false;
    for (y = 1; y < LIMIT - 1; y++)
      for (x = 1; x < LIMIT - 1; x++)
	if (distance[y][x] == i) {
	  if (map[y - 1][x] == '.' && distance[y - 1][x] > i) {
	    from[y - 1][x] |= from[y][x];
	    if (nextto(x, y - 1, c))
	      best = check(best, x, y - 1);
	    else
	      found = (distance[y - 1][x] = i + 1);
	  }
	  if (map[y][x - 1] == '.' && distance[y][x - 1] > i) {
	    from[y][x - 1] |= from[y][x];
	    if (nextto(x - 1, y, c))
	      best = check(best, x - 1, y);
	    else
	      found = (distance[y][x - 1] = i + 1);
	  }
	  if (map[y][x + 1] == '.' && distance[y][x + 1] > i) {
	    from[y][x + 1] |= from[y][x];
	    if (nextto(x + 1, y, c))
	      best = check(best, x + 1, y);
	    else
	      found = (distance[y][x + 1] = i + 1);
	  }
	  if (map[y + 1][x] == '.' && distance[y + 1][x] > i) {
	    from[y + 1][x] |= from[y][x];
	    if (nextto(x, y + 1, c))
	      best = check(best, x, y + 1);
	    else
	      found = (distance[y + 1][x] = i + 1);
	  }
	}
    i++;
  }
  return best ? best : -1;
}

static void dump(int tick, int rows, int nunits) {
  int i;
  debug("\ntick %d:\n", tick);
  for (i = 0; (getenv("DEBUG") ?: "1")[0] == '2' && i < rows; i++)
    debug("%s\n", map[i]);
  debug("%d units at:\n", nunits);
  for (i = 0; i < nunits; i++) {
    if (units[i].type == 'X')
      debug("%d: dead\n", units[i].id);
    else {
      assert(map[units[i].y][units[i].x] == units[i].type);
      debug("%d: %d,%d %c with hp %d attack %d\n", units[i].id, units[i].x,
	    units[i].y, units[i].type, units[i].hp, units[i].attack);
    }
  }
}

int main(int argc, char **argv) {
  size_t len = 0, count = 0;
  char *line;
  char *p;
  int nunits = 0;
  int ticks = 0;
  int i, j;
  int sum = 0;
  int attack = 3;
  int ret = 0;

  /* Part 1 - read in map */
  if (argc > 2)
    attack = atoi(argv[2]);
  if (argc > 1 && !(stdin = freopen(argv[1], "r", stdin))) {
    perror("failure");
    exit(2);
  }

  while (getline(&line, &len, stdin) >= 0) {
    p = strspn(line, "#.GE") + line;
    if (*p != '\n' || p - line > LIMIT) {
      fprintf(stderr, "recompile with larger limit\n");
      exit(1);
    }
    p = strcspn(line, "GE\n") + line;
    while (*p != '\n') {
      units[nunits].id = nunits + 1;
      units[nunits].type = *p;
      units[nunits].x = p - line;
      units[nunits].y = count;
      units[nunits].hp = 200;
      units[nunits].attack = *p == 'G' ? 3 : attack;
      nunits++;
      p++;
      p = strcspn(p, "GE\n") + p;
    }
    memcpy(map[count++], line, p - line);
    if (count == LIMIT) {
      fprintf(stderr, "recompile with larger limit\n");
      exit(1);
    }
  }
  printf("read %zu lines, found %d units\n", count, nunits);
  qsort(units, nunits, sizeof *units, sorter);
  dump(ticks, count, nunits);

  /* Part 2 - iterate until one side wins */
  while (1) {
    for (i = 0; i < nunits; i++) {
      /* Part 2a: Check */
      if (units[i].type == 'X')
	continue;
      for (j = 0; j < nunits; j++) {
	if (i == j)
	  continue;
	if (units[j].type == enemy(&units[i]))
	  break;
      }
      if (j == nunits) {
	printf("combat ended during tick %d\n", ticks);
	goto complete;
      }

      /* Part 2b: Move */
      int xy = locate(units[i].x, units[i].y, enemy(&units[i]));
      if (xy < 0)
	debug("No move possible for unit %d\n", units[i].id);
      else if ((xy & 0xffff) != ((units[i].x << 8) | units[i].y)) {
	debug("Moving unit %d closer to %d,%d", units[i].id,
	      xy >> 8 & 0xff, xy & 0xff);
	assert(xy >> 16);
	map[units[i].y][units[i].x] = '.';
	if (xy & 0x10000)
	  units[i].y--;
	else if (xy & 0x20000)
	  units[i].x--;
	else if (xy & 0x40000)
	  units[i].x++;
	else
	  units[i].y++;
	debug(" via %d,%d\n", units[i].x, units[i].y);
	map[units[i].y][units[i].x] = units[i].type;
      } else
	debug("Unit %d already in range\n", units[i].id);

      /* Part 2c: Attack */
      if (nextto(units[i].x, units[i].y, enemy(&units[i]))) {
	int best = i;
	int hp = 201;
	for (j = 0; j < nunits; j++) {
	  if (units[j].type != enemy(&units[i]))
	    continue;
	  if (abs(units[i].x - units[j].x) +
	      abs(units[i].y - units[j].y) == 1) {
	    if (units[j].hp < hp) {
	      best = j;
	      hp = units[j].hp;
	    } else if (units[j].hp == hp &&
		       units[j].y * LIMIT + units[j].x <
		       units[best].y * LIMIT + units[best].x) {
	      best = j;
	    }
	  }
	}
	if (best != i) {
	  units[best].hp -= units[i].attack;
	  debug("unit %d attacking %d to %d\n", units[i].id, units[best].id,
		units[best].hp);
	  if (units[best].hp <= 0) {
	    debug("%c unit %d killed\n", units[best].type, units[best].id);
	    if (units[best].type == 'E') {
	      printf("attack %d too low: elf casualty in tick %d\n",
		     attack, ticks);
	      ret = 1;
	    }
	    units[best].type = 'X';
	    units[best].hp = 0;
	    map[units[best].y][units[best].x] = '.';
	  }
	}
      }
    }
    qsort(units, nunits, sizeof *units, sorter);
    dump(++ticks, count, nunits);
  }
 complete:
  qsort(units, nunits, sizeof *units, sorter);
  dump(ticks + 1, count, nunits);
  for (i = 0; i < nunits; i++) {
    sum += units[i].hp;
  }
  printf("%c wins, remaining hitpoints %d, score %d\n", units->type, sum,
	 sum * ticks);
  return ret;
}
