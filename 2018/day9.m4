divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dplayers=N -Dend=N] [-Dhashsize=H] [-Dfile=day9.input] day9.m4
# Optionally use -Dverbose=1 to see some progress

include(`common.m4')ifelse(common(9, 8388617), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`parse', `define(`players', `$1')define(`end', `$7')')
ifdef(`players', `', `parse(translit(include(defn(`file')), ` ', `,'))')

include(`math64.m4')
define(`p'eval(23%players), 9)define(`P'eval(23%players), 1)
define(`c', 18)define(`n18', 2)define(`n2', 10)define(`n10', 5)
define(`n5', 11)define(`n11', 1)define(`n1', 12)define(`n12', 6)
define(`n6', 13)define(`n13', 3)define(`n3', 14)define(`n14', 7)
define(`n7', 15)define(`n15', 0)define(`n0', 16)define(`n16', 8)
define(`n8', 17)define(`n17', 4)define(`n4', 18)

define(`D', defn(`define'))define(`I', defn(`incr'))define(`E', defn(`eval'))
define(`do', `_do1(E(`23*$1+1'),c,E(`23*$1-4'))ifdef(`do$1',`do$1(`$1')')')
define(`_do1', `_do2(I(I(`$1')),D(`n$3',n$2)D(`n$2',`$3')n$3,`$1','dnl
`E(`$1-4'),I(`$1'))')
define(`_do2', `_do3(I(I(`$1')),D(`n$5',n$2)D(`n$4',`$5')D(`n$3',`$4')'dnl
`D(`n$2',`$3')n$5,`$1',E(`$1-5'),I(`$1'))')
define(`_do3', `_do4(I(I(`$1')),D(`n$5',n$2)D(`n$4',`$5')D(`n$3',`$4')'dnl
`define(`n$2',`$3')n$5,`$1',E(`$1-6'),I(`$1'))')
define(`_do4', `_do5(I(`$1'),D(`n$5',n$2)D(`n$4',`$5')D(`n$3',`$4')'dnl
`D(`n$2',`$3')n$5,`$1')')
define(`_do5', `_do6(I(`$1'),D(`n$3',n$2)D(`n$2',`$3')n$3,`$1')')
define(`_do6', `_do7(I(`$1'),D(`n$3',n$2)D(`n$2',`$3')n$3,`$1')')
define(`_do7', `_do8(I(`$1'),D(`n$3',n$2)D(`n$2',`$3')n$3,`$1')')
define(`_do8', `_do9(I(`$1'),D(`n$3',n$2)D(`n$2',`$3')n$3,`$1')')
define(`_do9', `_do10(I(`$1'),D(`n$3',n$2)D(`n$2',`$3')n$3,`$1')')
define(`_do10', `_do11(I(`$1'),D(`n$3',n$2)D(`n$2',`$3')n$3,`$1')')
define(`_do11', `_do12(I(`$1'),D(`n$3',n$2)D(`n$2',`$3')n$3,`$1')')
define(`_do12', `_do13(I(`$1'),D(`n$3',n$2)D(`n$2',`$3')n$3,`$1')')
define(`_do13', `_do14(I(`$1'),D(`n$3',n$2)D(`n$2',`$3')n$3,`$1')')
define(`_do14', `_do15(I(`$1'),D(`n$3',n$2)D(`n$2',`$3')n$3,`$1')')
define(`_do15', `_do16(`($1+5)',D(`n$3',n$2)D(`n$2',`$3')n$3,`$1')')
define(`_do16', `_do17(E(`$1%''players`),n$2,`$1/23',`$2',`$3')')
define(`_do17', `D(`p$1',E(defn(`p$1')`+$2'))D(`P$1',E(defn(`P$1')`+$3'))'dnl
`D(`n$4',`$5')D(`c',`$5')D(`n$5',n$2)popdef(`n$2')')
define(`rename', `define(`$2', defn(`$1'))popdef(`$1')')
define(`do10000', `output(1, `...$1')rename(`$0', `do'eval(`$1+10000'))')

define(`_find', `ifdef(`p$1', `$0_(add64(p$1, mul64(23, P$1)))')')
define(`_find_', `ifelse(lt64(best, `$1'), 1, `define(`best', `$1')')')
define(`find', `define(`best', 0)forloop_arg(0, players-1, `_$0')best')

forloop_arg(1, end/23-1, `do')
define(`part1', find)
forloop_arg(end/23, end*100/23-1, `do')
define(`part2', find)

divert`'part1
part2
