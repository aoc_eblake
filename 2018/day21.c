#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <inttypes.h>
#include <assert.h>

bool __attribute__((format(printf, 1, 2)))
debug(const char *fmt, ...) {
  va_list ap;
  if (getenv("DEBUG")) {
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
    return true;
  }
  return false;
}

static int64_t regs[6];
static uint8_t ip;
typedef struct op {
  char op;
  int a;
  int b;
  int c;
  int visited;
} op;
static op *program;
static unsigned int max;

static int lookup(const char *opcode) {
  switch (*opcode) {
  case 'a':
    return opcode[3] == 'i';
  case 'm':
    return 2 + (opcode[3] == 'i');
  case 'b':
    return 4 + 2 * (opcode[1] == 'o') + (opcode[3] == 'i');
  case 's':
    return 8 + (opcode[3] == 'i');
  case 'g':
    return 10 + (opcode[2] == 'r') + (opcode[3] == opcode[2]);
  case 'e':
    return 13 + (opcode[2] == 'r') + (opcode[3] == opcode[2]);
  }
  fprintf(stderr, "failed decoding instruction %s\n", opcode);
  exit(1);
}

static int64_t get(int r) {
  assert(0 <= r && r < sizeof regs / sizeof *regs);
  return regs[r];
}

static void set(int r, int64_t v) {
  assert(0 <= r && r < sizeof regs / sizeof *regs);
  regs[r] = v;
  assert(v >= 0 || !"resize regs to be larger type");
}

static const char *step(int op, int a, int b, int c) {
  switch (op) {
  case 0:
    set(c, get(a) + get(b)); return "addr";
  case 1:
    set(c, get(a) + b); return "addi";
  case 2:
    set(c, get(a) * get(b)); return "mulr";
  case 3:
    set(c, get(a) * b); return "muli";
  case 4:
    set(c, get(a) & get(b)); return "banr";
  case 5:
    set(c, get(a) & b); return "bani";
  case 6:
    set(c, get(a) | get(b)); return "borr";
  case 7:
    set(c, get(a) | b); return "bori";
  case 8:
    set(c, get(a)); return "setr";
  case 9:
    set(c, a); return "seti";
  case 10:
    set(c, a > get(b)); return "gtir";
  case 11:
    set(c, get(a) > b); return "gtri";
  case 12:
    set(c, get(a) > get(b)); return "gtrr";
  case 13:
    set(c, a == get(b)); return "eqir";
  case 14:
    set(c, get(a) == b); return "eqri";
  case 15:
    set(c, get(a) == get(b)); return "eqrr";
  default:
    return NULL;
  }
}

static uint32_t shuffle(uint32_t in) {
  int r1 = 1;
  int r2 = 1;
  int r3 = in >> 24;
  int r4 = in & 0xffffff;

  r3 = r4 | 65536; // bori 4 65536 3
  r4 = 4332021; // seti 4332021 4 4
 l7:
  r2 = r3 & 255; // bani 3 255 2
  r4 += r2; // addr 4 2 4
  r4 &= 16777215; // bani 4 16777215 4
  r4 *= 65899; // muli 4 65899 4
  r4 &= 16777215; // bani 4 16777215 4
  r2 = 256 > r3; // gtir 256 3 2
  if (r2) // addr 2 5 5; addi 5 1 5
    goto l27; // seti 27 5 5
  r2 = 0; // seti 0 2 2
 l17:
  r1 = r2 + 1; // addi 2 1 1
  r1 *= 256; // muli 1 256 1
  r1 = r1 > r3; // gtrr 1 3 1
  if (r1) // addr 1 5 5; addi 5 1 5
    goto l25; // seti 25 2 5
  r2++; // addi 2 1 2
  goto l17; // seti 17 3 5
 l25:
  r3 = r2; // setr 2 7 3
  goto l7; // seti 7 1 5
 l27:
  assert((r3 & 0xff) == r3 && (r4 & 0xffffff) == r4);
  return (r3 << 24) | r4;
}

int main(int argc, char **argv) {
  size_t len = 0, count = 0;
  char *line;
  op *o;
  uint32_t limit = 100000;

  /* Part 1 - read in entire program */
  if (argc > 3)
    limit = atoll(argv[3]);
  if (argc > 2)
    regs[0] = atoi(argv[2]);
  if (argc > 1 && !(stdin = freopen(argv[1], "r", stdin))) {
    perror("failure");
    exit(2);
  }

  if (getline(&line, &len, stdin) < 0 ||
      sscanf(line, "#ip %hhu\n", &ip) != 1) {
    fprintf(stderr, "missing #ip line\n");
    exit(1);
  }
  while (getline(&line, &len, stdin) > 0) {
    program = reallocarray(program, ++count, sizeof *program);
    o = &program[count - 1];
    o->op = lookup(line);
    if (sscanf(line + 5, "%d %d %d\n", &o->a, &o->b, &o->c) != 3) {
      fprintf(stderr, "corrupt program\n");
      exit(1);
    }
    o->visited = 0;
  }
  printf("program has %u instructions\n", max = count);

  /* Part 2 - run it */
  count = 0;
  bool d = false;
  while (regs[ip] < max && count < limit) {
    d = regs[ip] == 28;
    const char *s;
    if (!(count % 1000000))
      printf("%8zu...\n", count);
    if (d)
      debug("%5zu: ip=%"PRId64" [%"PRIx64", %"PRIx64", %"PRIx64", %"PRIx64
	    ", %"PRIx64", %"PRIx64"] ", count, regs[ip], regs[0],
	    regs[1], regs[2], regs[3], regs[4], regs[5]);
    count++;
    o = &program[regs[ip]];
    s = step(o->op, o->a, o->b, o->c);
    if (d)
      debug("%s %d %d %d ", s, o->a, o->b, o->c);
    o->visited++;
    if (d)
      debug("[%"PRIx64", %"PRIx64", %"PRIx64", %"PRIx64", %"PRIx64", %"PRIx64
	    "] (%d)\n", regs[0], regs[1], regs[2],
	    regs[3], regs[4], regs[5], o->visited);
    regs[ip]++;
  }
  printf("%s execution after %zu steps\n",
	 regs[ip] < max ? "gave up" : "completed", count);
  for (int i = 0; 0 && i < max; i++) {
    o = &program[i];
    if (!debug("program line %s %d %d %d executed %d times\n",
	       step(o->op, 0, 0, 0), o->a, o->b, o->c, o->visited))
      break;
  }

  /* Part 3: look for cycle with hand-rolled retype of program for speed */
  uint32_t slow, fast;
  uint32_t prev;
  count = 0;
  slow = fast = 0x0191f7da;
  do {
    count++;
    slow = shuffle(slow);
    prev = shuffle(fast);
    fast = shuffle(prev);
    if (count < 10 || count > 6300)
      debug(" %08zu: %08x %08x %08x\n", count, slow, prev, fast);
  } while ((slow & 0xffffff) != (fast & 0xffffff));
  printf("found loop after %zu cycles, now finding last integer in loop\n",
	 count);
  slow = 0x0191f7da;
  count = 0;
  while ((slow & 0xffffff) != (fast & 0xffffff)) {
    count++;
    prev = fast;
    slow = shuffle(slow);
    fast = shuffle(fast);
    if (count > 4390)
      debug(" %08zu: %08x %08x %08x\n", count, slow, prev, fast);
  }
  printf("repeat state after %zu cycles, previous state %d\n",
	 count, prev & 0xffffff);
  printf("checking: %x %x %x\n", prev, shuffle(prev), slow);
  return 0;
}
