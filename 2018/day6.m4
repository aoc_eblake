divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dlimit=N] [-Dhashsize=H] [-Dfile=day6.input] day6.m4
# Optionally use -Dverbose=1 to see some progress

include(`common.m4')ifelse(common(6, 65537), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

ifdef(`limit', `', `define(`limit', 10000)')

define(`input', translit((include(defn(`file'))), nl`,()', `;'))
define(`minx', 400)define(`miny', 400)define(`maxx', 0)define(`maxy', 0)
define(`setmax', `ifelse(eval(`$1 > '$2), 1, `define(`$2', `$1')')')
define(`setmin', `ifelse(eval(`$1 < '$2), 1, `define(`$2', `$1')$3')')
define(`diff', ``($1-$2)*(1-2*($1<$2))'')
define(`dist', `eval(diff(`$1', `$3')+diff(`$2', `$4'))')
define(`cnt', 0)
define(`point', `setmin(`$1', `minx')setmax(`$1', `maxx')setmin(`$2',
  `miny')setmax(`$2', `maxy')define(`cnt', incr(cnt))define(`p'cnt,
  `$1,$2')define(`x$1')define(`y$2')')
ifdef(`__gnu__', `
  patsubst(defn(`input'), `\([^;]*\) \([^;]*\);', `point(`\1', `\2')')
',`
  define(`_chew', `point(translit(substr(`$1', 0, index(`$1', `;')), ` ',
    `,'))define(`tail', substr(`$1', incr(index(`$1', `;'))))ifelse(index(
    defn(`tail'), `;'), -1, `', `$0(defn(`tail'))')')
  define(`chew', `ifelse(eval(`$1 < 16'), 1, `_$0(`$2')', `$0(eval(`$1/2'),
    substr(`$2', 0, eval(`$1/2')))$0(eval(len(defn(`tail'))` + $1 - $1/2'),
    defn(`tail')substr(`$2', eval(`$1/2')))')')
  chew(len(defn(`input')), defn(`input'))
')
define(`part1', 0)
define(`closest', `ifdef(`c$1_$2', `', `define(`d', 0)define(`best',
  800)forloop(1, 'cnt`, `_$0(`$1', `$2', ', `)')define(`c$1_$2',
  p)define(`d$1_$2', d)')c$1_$2')
define(`total', `ifelse(closest(`$1', `$2'))d$1_$2')
define(`_closest', `$0_(dist(`$1', `$2', p$3), `$3')')
define(`_closest_', `ifelse(`$1', best, `define(`p', 0)', `setmin(`$1', `best',
  `define(`p', `$2')')')define(`d', eval(d + $1))')
define(`edge', `ifdef(ifelse(`$3', `-', ``x$1'', ``y$2''),
  `define(`i'closest(`$1', `$2'))')')
forloop(minx, maxx, `edge(', `, 'miny`, `-')')
forloop(minx, maxx, `edge(', `, 'maxy`, `-')')
forloop(incr(miny), decr(maxy), `edge('minx`, ', `)')
forloop(incr(miny), decr(maxy), `edge('maxx`, ', `)')
define(`fill', `output(1, `...$1')ifdef(`i$1', `', `fill1(decr(first(p$1)),
  p$1, `$1', `close')')')
define(`close', `ifelse(closest(`$1', `$2'), `$3', 1, 0)')
define(`fill1', `ifelse($5(`$1', `$3', `$4'), 1, `$0(decr(`$1'), `$2', `$3',
  `$4', `$5')', `fill2(incr(`$1'), incr(`$2'), `$3', `$4', `$5')')')
define(`fill2', `ifelse($5(`$2', `$3', `$4'), 1, `$0(`$1', incr(`$2'), `$3',
  `$4', `$5')', `define(`s', eval(`$2 - $1'))fill3(`$1', decr(`$2'), decr(`$3'),
  `$4', `decr', `$5')fill3(`$1', decr(`$2'), incr(`$3'), `$4', `incr', `$5')')')
define(`fill3', `ifelse($6(`$1', `$3', `$4'), 1, `ifelse(`$1', `$2',
  `define(`s', incr(s))$0(`$1', `$2', $5(`$3'), `$4', `$5', `$6')', `fill4(
  $@)')', `$1', `$2', `', `$0(incr(`$1'), `$2', `$3', `$4', `$5', `$6')')')
define(`fill4', `ifelse($6(`$2', `$3', `$4'), 1, `define(`s', eval(s +
  $2 - $1 + 1))fill3(`$1', `$2', $5(`$3'), `$4', `$5', `$6')', `$0(`$1',
  decr(`$2'), `$3', `$4', `$5', `$6')')')
forloop(1, cnt, `define(`s', 0)fill(', `)setmax(s, `part1')')
define(`range', `eval(total(`$1', `$2')` < $3')')
define(`fill0', `_$0(total(`$1', decr(`$2')), total(`$1', `$2'),
  total(`$1', incr(`$2')), `$1', `$2')')
define(`_fill0', `ifelse(eval(`$1 < $2'), 1, `fill0(`$4', decr(`$5'))',
  eval(`$3 < $2'), 1, `fill0(`$4', incr(`$5'))', `fill1(decr(`$4'), `$4', `$5',
  limit, `range')')')
define(`s', 0)
fill0(eval((maxx-minx)/2), eval((maxy-miny)/2))
define(`part2', s)

divert`'part1
part2
