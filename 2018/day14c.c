#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <inttypes.h>
#include <assert.h>

bool debug(const char *fmt, ...) {
  va_list ap;
  if (getenv("DEBUG")) {
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
    return true;
  }
  return false;
}

#define LIMIT 5000000

#define PACK(a, b) (((a)<<4)|(b))
#define NEXT(a) a&0xf
static uint8_t array[LIMIT] = {
  [0] = PACK(3, 4),
  [1] = PACK(7, 8),
  [2] = PACK(3, 2),
  [3] = PACK(0, 1),
  [4] = PACK(1, 2),
  [5] = PACK(0, 1),
  [6] = PACK(1, 2),
  [7] = PACK(2, 3),
  [8] = PACK(4, 4),
  [9] = PACK(5, 4),
  [10] = PACK(1, 1),
  [11] = PACK(8, 3),
  [12] = PACK(9, 4),
  [13] = PACK(6, 2),
  [14] = PACK(1, 2),
  [15] = PACK(0, 1),
  [16] = PACK(7, 1),
};

static unsigned need; /* iterations needed for part 1 */
static char part1[11] = "unknown";
static unsigned pattern; /* goal for part 2 */
static unsigned mask;
static unsigned recipes; /* recipes processed so far */
static uint64_t recent; /* fifo of most-recently-seen hex digits */
static unsigned next; /* next array index awaiting assignment */
static uint8_t remain; /* recipes to skip before assigning next */

static bool e(uint8_t value) {
  if (value >= 10)
    return e(1) || e(value - 10);
  if (recipes++ == need)
    sprintf(part1, "%010llx", recent & 0xffffffffffULL);
  assert(next < LIMIT);
  if (remain)
    remain--;
  else
    array[next++] = PACK(remain = value, 1);
  recent = (recent << 4) | value;
  return (recent & mask) == pattern;
}

int main(int argc, char **argv) {
  const char *goal = "077201";
  char buf[7] = "";
  int len;
  int idx1, idx2;
  int val;
  int iter = 0;
  FILE *f;

  if (argc > 1)
    goal = argv[1];
  f = fopen(goal, "r");
  if (f) {
    if (fread(buf, 1, 6, f) != 6) {
      fprintf(stderr, "error reading file %s\n", goal);
      exit(1);
    }
    goal = buf;
    fclose(f);
  }
  need = strtol(goal, NULL, 10) + 10;
  len = strlen(goal);
  pattern = strtol(goal, NULL, 16);
  mask = (1 << 4*len) - 1;

  idx1 = 4;
  idx2 = 12;
  recipes = 24;
  recent = 0x1677925107ULL;
  next = 17;
  remain = 7;
  do {
    iter++;
    if (!array[idx1])
      idx1 = remain;
    if (!array[idx2])
      idx2 = remain;
    val = (array[idx1] + array[idx2]) >> 4;
    idx1 += NEXT(array[idx1]);
    idx2 += NEXT(array[idx2]);
  } while (!e(val));

  printf("after %s recipes, next ten recipes are %s\n", goal, part1);
  printf("%s found after %d recipes (%d iterations, array len %d)\n", goal,
         recipes - len, iter, next);
  return 0;
}
