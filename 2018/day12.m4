divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day12.input] day12.m4

include(`common.m4')ifelse(common(12), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

include(`math64.m4')
define(`input', translit(include(defn(`file')), nl`.# =>', `;01'))
define(`init', substr(defn(`input'), incr(index(defn(`input'), `:')),
  eval(index(defn(`input'), `;;') - index(defn(`input'), `:') - 1)))
define(`list', substr(defn(`input'), incr(incr(index(defn(`input'), `;;')))))
define(`visit', `translit(`define(`rABCDE', `F')define(`REDCBA', `F')',
  `ABCDEF', `$1')')
ifdef(`__gnu__', `
  patsubst(defn(`init'), `.', `pushdef(`st0', `\&')')
  patsubst(defn(`list'), `\([^;]*\);', `visit(`\1')')
', `
  define(`chew', `ifelse(`$1', `1', `pushdef(`st0', `$2')', `$0(eval(`$1/2'),
    substr(`$2', 0, eval(`$1/2')))$0(eval(`$1-$1/2'), substr(`$2',
    eval(`$1/2')))')')
  chew(len(defn(`init')), defn(`init'))
  define(`half', `eval($1/6/2*6)')
  define(`chew', `ifelse(`$1', `6', `visit(`$2')', `$0(half(`$1'), substr(
    `$2', 0, half(`$1')))$0(eval($1-half(`$1')), substr(`$2', half(`$1')))')')
  chew(eval(len(defn(`list'))/7*6), translit(defn(`list'), `;'))
')
define(`offset', 0)
define(`prune', `ifelse($1, 0, `popdef(`$1')$2$0($@)')')
define(`rev', `_$0(0, 0, 0, 0, st0`'popdef(`st0'))prune(`st1',
  `define(`offset', incr(offset))')')
define(`_rev', `pushdef(`st1', R$1$2$3$4$5)ifdef(`st0', `$0($2, $3, $4, $5,
  st0`'popdef(`st0'))', `$0_($2, $3, $4, $5, 0)')')
define(`_rev_', `pushdef(`st1', R$1$2$3$4$5)pushdef(`st1',
  R$2$3$4$5$5)pushdef(`st1', R$3$4$5$5$5)pushdef(`st1', R$4$5$5$5$5)define(
  `offset', decr(decr(offset)))')
define(`fwd', `define(`sum', _$0(define(`offset', decr(decr(offset)))offset,
  0, 0, 0, 0, 0, st1`'popdef(`st1')))prune(`st0')')
define(`_fwd', `pushdef(`st0', r$3$4$5$6$7)ifdef(`st1', `$0(incr($1),
  eval($2+st0*$1), $4, $5, $6, $7, st1`'popdef(`st1'))', `$0_(incr($1),
  eval($2+st0*$1), $4, $5, $6, $7, 0)')')
define(`_fwd_', `eval($2 + pushdef(`st0', r$3$4$5$6$7)st0*$1 +
  pushdef(`st0', r$4$5$6$7$7)st0*($1+1) +
  pushdef(`st0', r$5$6$7$7$7)st0*($1+2) +
  pushdef(`st0', r$6$7$7$7$7)st0*($1+3))')
forloop_arg(0, 9, `rev()fwd')
define(`part1', sum)
define(`diff', 0)
define(`do', `define(`prev', sum)rev()fwd()ifelse(eval(sum - prev), diff,
  `define(`part2', add64(mul64(add64(`50000000000', `-$1'), eval(diff/2)),
  sum))', `define(`diff', eval(sum - prev))$0(incr(incr(`$1')))')')
do(22)

divert`'part1
part2
