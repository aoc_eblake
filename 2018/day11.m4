divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dserial=N] [-Dhashsize=H] [-Dfile=day11.input] day11.m4
# Optionally use -Dverbose=1 to see some progress

include(`common.m4')ifelse(common(11, 131071), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

ifdef(`serial', `', `define(`serial', translit(include(defn(`file')), nl))')

define(`D', defn(`define'))define(`E', defn(`eval'))define(`I', defn(`ifelse'))
define(`P', defn(`incr'))define(`M', defn(`decr'))
define(`prep', `D(`g0_$1',0)D(`g$1_0',0)')
forloop_arg(0, 300, `prep')
define(`p', ``($1*$2+$3)*$1%1000/100-5'')
define(`g', `(g$1_$2)')
define(`_prep', `D(`g$1_$2',E(p(`(10+$1)',`$2','serial`)+g($1,M(`$2'))+g('dnl
`M(`$1'),`$2')-g(M(`$1'),M(`$2'))))')
define(`prep', `forloop(1, 300, `_$0(', `, `$1')')')
forloop_arg(1, 300, `prep')
define(`b', 0)
define(`c', `_$0(g$2_$4+g$1_$3-(g$2_$3)-(g$1_$4),`$5')')
define(`_c', `I(E(`$1>'b),1,`D(`b',E(`$1'))$2')')
define(`_find', `c(M(`$1'),P(P(`$1')),M(`$2'),P(P(`$2')),`D(`part1',`$1,$2')')')
define(`find', `forloop(1,298,`_$0(',`,`$1')')')
output(1, `...3')
forloop_arg(1, 298, `find')

define(`total', b)
define(`_find', `c(M(`$1'),E(`$1+$3-1'),M(`$2'),E(`$2+$3-1'),`D(`try',
`$1,$2,$3')')')
define(`find', `forloop(1,E(`301-$2'),`_$0(',`,`$1',`$2')')')
define(`round', `output(1, `...$1')D(`b', 0)forloop(1,E(`301-$1'),
  `find(',`,`$1')')I(E(b>total),1,`D(`total',b)D(`part2',defn(`try'))')I(E(
  b>total*8/9),1,`$0(P(`$1'))')')
round(4)

divert`'part1
part2
