#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <inttypes.h>
#include <assert.h>
#include <ctype.h>

bool __attribute__((format(printf, 1, 2)))
debug(const char *fmt, ...) {
  va_list ap;
  if (getenv("DEBUG")) {
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
    return true;
  }
  return false;
}

#define MAXX 30
#define MAXY 820
#define SLOP 20
static struct spot {
  int idx;
  int ero;
  char type; /* '.', '=', or '|'; thanks to ASCII, risk is (type/4+1)%3 */
  int t;
  int c;
  int n;
} grid[MAXY][MAXX];
static int maxx;
static int maxy;

static char lookup(int i) {
  if (i == -1)
    return '-';
  if (i < 10)
    return '0' + i;
  if (i < 10 + 26)
    return 'a' + i - 10;
  if (i < 10 + 26 + 26)
    return 'A' + i - 10 - 26;
  return '+';
}

static void dump(int level) {
  int i, j;
  if (!debug("\niter %d:\n", level))
    return;
  for (j = 0; j <= maxy; j++) {
    for (i = 0; i <= maxx; i++)
      debug("%c%c%c%c ", grid[j][i].type, lookup(grid[j][i].t),
	    lookup(grid[j][i].c), lookup(grid[j][i].n));
    debug("\n");
  }
}

static bool try(int *p, int other, int add) {
  if (other == -1)
    return false;
  if (*p == -1 || other + add < *p) {
    *p = other + add;
    return true;
  }
  return false;
}

static bool process(int x, int y) {
  int *p;
  bool ret = false;
  switch (grid[y][x].type) {
  case '.':
    p = &grid[y][x].t;
    ret |= try(p, grid[y][x].c, 7);
    if (y)
      ret |= try(p, grid[y - 1][x].t, 1);
    if (x)
      ret |= try(p, grid[y][x - 1].t, 1);
    if (x < maxx)
      ret |= try(p, grid[y][x + 1].t, 1);
    if (y < maxy)
      ret |= try(p, grid[y + 1][x].t, 1);
    p = &grid[y][x].c;
    ret |= try(p, grid[y][x].t, 7);
    if (y)
      ret |= try(p, grid[y - 1][x].c, 1);
    if (x)
      ret |= try(p, grid[y][x - 1].c, 1);
    if (x < maxx)
      ret |= try(p, grid[y][x + 1].c, 1);
    if (y < maxy)
      ret |= try(p, grid[y + 1][x].c, 1);
    break;
  case '=':
    p = &grid[y][x].c;
    ret |= try(p, grid[y][x].n, 7);
    if (y)
      ret |= try(p, grid[y - 1][x].c, 1);
    if (x)
      ret |= try(p, grid[y][x - 1].c, 1);
    if (x < maxx)
      ret |= try(p, grid[y][x + 1].c, 1);
    if (y < maxy)
      ret |= try(p, grid[y + 1][x].c, 1);
    p = &grid[y][x].n;
    ret |= try(p, grid[y][x].c, 7);
    if (y)
      ret |= try(p, grid[y - 1][x].n, 1);
    if (x)
      ret |= try(p, grid[y][x - 1].n, 1);
    if (x < maxx)
      ret |= try(p, grid[y][x + 1].n, 1);
    if (y < maxy)
      ret |= try(p, grid[y + 1][x].n, 1);
    break;
  case '|':
    p = &grid[y][x].n;
    ret |= try(p, grid[y][x].t, 7);
    if (y)
      ret |= try(p, grid[y - 1][x].n, 1);
    if (x)
      ret |= try(p, grid[y][x - 1].n, 1);
    if (x < maxx)
      ret |= try(p, grid[y][x + 1].n, 1);
    if (y < maxy)
      ret |= try(p, grid[y + 1][x].n, 1);
    p = &grid[y][x].t;
    ret |= try(p, grid[y][x].n, 7);
    if (y)
      ret |= try(p, grid[y - 1][x].t, 1);
    if (x)
      ret |= try(p, grid[y][x - 1].t, 1);
    if (x < maxx)
      ret |= try(p, grid[y][x + 1].t, 1);
    if (y < maxy)
      ret |= try(p, grid[y + 1][x].t, 1);
    break;
  default:
    abort();
  }
  return ret;
}

int main(int argc, char **argv) {
  int depth = 510;
  int x = 10;
  int y = 10;
  int i, j;
  int risk = 0;

  /* Part 1 - populate grid */
  if (argc == 3) {
    depth = atoi(argv[1]);
    x = atoi(argv[2]);
    y = atoi(argv[3]);
  } else if (argc > 1 &&
	     (!(stdin = freopen(argv[1], "r", stdin)) ||
	      scanf("depth: %d\ntarget: %d,%d\n", &depth, &x, &y) != 3)) {
    fprintf(stderr, "bad input\n");
    exit(2);
  }
  maxx = x + SLOP;
  maxy = y + SLOP;
  if (maxx >= MAXX || maxy >= MAXY) {
    fprintf(stderr, "bad input\n");
    exit(2);
  }

  for (j = 0; j <= maxy; j++)
    for (i = 0; i <= maxx; i++) {
      if (i == 0 && j == 0)
	grid[j][i].idx = 0;
      else if (i == x && j == y)
	grid[j][i].idx = 0;
      else if (j == 0)
	grid[j][i].idx = i * 16807;
      else if (i == 0)
	grid[j][i].idx = j  * 48271;
      else
	grid[j][i].idx = grid[j][i - 1].ero * grid[j - 1][i].ero;
      grid[j][i].ero = (grid[j][i].idx + depth) % 20183;
      if (grid[j][i].ero % 3 == 0)
	grid[j][i].type = '.';
      else if (grid[j][i].ero % 3 == 1)
	grid[j][i].type = '=';
      else
	grid[j][i].type = '|';
      if (i <= x && j <= y)
	risk += (grid[j][i].type / 4 + 1) % 3;
      grid[j][i].t = -1;
      grid[j][i].c = -1;
      grid[j][i].n = -1;
    }
  dump(0);
  printf("resulting risk %d\n", risk);

  int iter = 0;
  bool done = false;
  grid[0][0].t = 0;
  while (!done) {
    done = true;
    for (j = 0; j <= maxy; j++)
      for (i = 0; i <= maxx; i++)
	if (process(i, j))
	  done = false;
    dump(++iter);
  }
  printf("after %d cycles to settle, shortest path to %d,%d torch is %d\n",
	 iter, x, y, grid[y][x].t);
  return 0;
}
