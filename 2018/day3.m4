divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dhashsize=H] [-Dfile=day3.input] day3.m4
# Optionally use -Dverbose=1 to see some progress
# Optionally use -Dalgo=brute|sweep to control algorithm

include(`common.m4')ifelse(common(3, 65537), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

ifdef(`algo', `', `define(`algo', `sweep')')

define(`input', translit((include(defn(`file'))), nl`@,x# ()', `;:::'))
define(`part1', 0)
define(`line', `_$0(translit(`$1', `:', `,'))')
define(`_line', `define(`cnt', `$1')pushdef(`inst', ``$1',`$2','eval(
  `$2+$4-1')`,`$3','eval(`$3+$5-1'))')
ifdef(`__gnu__', `
  patsubst(defn(`input'), `\([^;]*\);', `line(`\1')')
',`
  define(`_chew', `line(substr(`$1', 0, index(`$1', `;')))define(`tail',
    substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(`tail'),
    `;'), -1, `', `$0(defn(`tail'))')')
  define(`chew', `ifelse(eval(`$1 < 45'), 1, `_$0(`$2')', `$0(eval(`$1/2'),
    substr(`$2', 0, eval(`$1/2')))$0(eval(len(defn(`tail'))` + $1 - $1/2'),
    defn(`tail')substr(`$2', eval(`$1/2')))')')
  chew(len(defn(`input')), translit(defn(`input'), nl, `;'))
')

ifelse(algo, `brute', `
output(1, `using brute-force cell visits')

define(`bump', ``define(`part1', incr(part1))pushdef(`$2$3')define(`c$1')'')
define(`claim', `forloop($3, $4, `_$0(`$1', `$2', ', `)')')
define(`_claim', `ifdef(`g$2_$3', `g$2_$3`'define(`c$1')', `define(`g$2_$3',
  bump(`$1', `$', `0'))')')
define(`loop', `ifdef(`inst', `_$0(inst)popdef(`inst')$0()')')
define(`_loop', `forloop($2, $3, `claim(`$1', ', `, `$4', `$5')')')
loop()

', algo, `sweep', `
output(1, `using sweep lines tracing')

define(`w', 0)
define(`merge', `forloop($3, $4, `_$0(`$1', `$2',', `)')')
define(`_merge', `$1(`$2', $3, defn(`x$3'))')
define(`use', `ifelse(`$2', `', `popdef(`x$1')', `define(`x$1', `$2')')')

define(`claim', `use(`$2', ifelse(`$3', `', `$1', `ifelse(index(`$3', `,'),
  `-1', `define(`c$3')define(`w', incr(w))')define(`c$1')`$3,$1''))')
define(`drop', `use(`$2', ifelse(`$3', `$1', `', `ifelse(len(translit(`$3',
  `,0123456789', -)), 1, `define(`w', decr(w))')quote(shift(_$0(`$1',$3)))'))')
define(`_drop', `ifelse(`$3', `', `', `$1', `$2', `,shift(shift($@))',
  `,`$2'$0(`$1', shift(shift($@)))')')
define(`do', `_$0($1)define(`part1', eval(part1 + w))')
define(`_do', `ifdef(`row$1', `row$1`'popdef(`row$1')$0(`$1')')')

define(`divvy', `ifdef(`inst', `_$0(inst)popdef(`inst')$0()')')
define(`_divvy', `pushdef(`row$2', `merge(`claim', `$1', `$4', `$5')')pushdef(
  `row'incr(`$3'), `merge(`drop', `$1', `$4', `$5')')')
divvy()
forloop_arg(0, 999, `do')

', `
errprintn(`unrecognized -Dalgo= value')m4exit(1)
')

define(`find', `ifdef(`c$1', `', `define(`part2', `$1')')')
forloop_arg(1, cnt, `find')

divert`'part1
part2
