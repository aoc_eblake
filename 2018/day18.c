#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <inttypes.h>
#include <assert.h>

bool debug(const char *fmt, ...) {
  va_list ap;
  if (getenv("DEBUG")) {
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
    return true;
  }
  return false;
}

#define LIMIT 50
static char grida[2][LIMIT + 2][LIMIT + 2]; /* slow */
static char gridb[2][LIMIT + 2][LIMIT + 2]; /* fast */

static void dump(int ticks, int max) {
  if (!debug("time %d\n", ticks))
    return;
  for (int i = 1; i <= max; i++)
    debug("%s\n", &grida[ticks % 2][i][1]);
  debug("\n");
}

static char compute(char grid[LIMIT + 2][LIMIT + 2], int x, int y,
		    int *wooded, int *lumber) {
  int o = 0, w = 0, l = 0;
  switch (grid[y - 1][x - 1]) {
  case '.': o++; break;
  case '|': w++; break;
  case '#': l++; break;
  }
  switch (grid[y - 1][x]) {
  case '.': o++; break;
  case '|': w++; break;
  case '#': l++; break;
  }
  switch (grid[y - 1][x + 1]) {
  case '.': o++; break;
  case '|': w++; break;
  case '#': l++; break;
  }
  switch (grid[y][x - 1]) {
  case '.': o++; break;
  case '|': w++; break;
  case '#': l++; break;
  }
  switch (grid[y][x + 1]) {
  case '.': o++; break;
  case '|': w++; break;
  case '#': l++; break;
  }
  switch (grid[y + 1][x - 1]) {
  case '.': o++; break;
  case '|': w++; break;
  case '#': l++; break;
  }
  switch (grid[y + 1][x]) {
  case '.': o++; break;
  case '|': w++; break;
  case '#': l++; break;
  }
  switch (grid[y + 1][x + 1]) {
  case '.': o++; break;
  case '|': w++; break;
  case '#': l++; break;
  }
  switch (grid[y][x]) {
  case '.':
    if (w >= 3) {
      if (wooded)
	++*wooded;
      return '|';
    }
    return '.';
  case '|':
    if (l >= 3) {
      if (lumber)
	++*lumber;
      return '#';
    }
    if (wooded)
      ++*wooded;
    return '|';
  case '#':
    if (w && l) {
      if (lumber)
	++*lumber;
      return '#';
    }
    return '.';
  default:
    assert(false);
  }
}

int main(int argc, char **argv) {
  size_t len = 0, count = 0;
  char *line;
  char *p;
  int x, y;
  int ticks;
  int wooded, lumber;

  /* Part 1 - read in initial map */
  if (argc > 1 && !(stdin = freopen(argv[1], "r", stdin))) {
    perror("failure");
    exit(2);
  }

  while (getline(&line, &len, stdin) >= 0) {
    p = stpcpy(&grida[0][++count][1], line);
    *--p = '\0';
    if (p - &grida[0][count][1] >= LIMIT + 1) {
      fprintf(stderr, "LIMIT too small\n");
      exit(1);
    }
  }
  printf("Read %zu lines\n", count);
  if(p - &grida[0][count][1] != count) {
    fprintf(stderr, "non-square grid read in\n");
    exit(1);
  }
  memcpy(gridb, grida, sizeof grida);
  dump(0, count);

  /* Part 2 - first 10 mintes */
  for (ticks = 0; ticks < 10; ticks++) {
    wooded = lumber = 0;
    for (y = 1; y <= count; y++)
      for (x = 1; x <= count; x++) {
	grida[(ticks + 1) % 2][y][x] = compute(grida[ticks % 2], x, y,
					       &wooded, &lumber);
	gridb[1][y][x] = compute(gridb[0], x, y, NULL, NULL);
      }
    for (y = 1; y <= count; y++)
      for (x = 1; x <= count; x++)
	gridb[0][y][x] = compute(gridb[1], x, y, NULL, NULL);
    dump(ticks + 1, count);
    printf("After %d minutes, resource value %d*%d = %d\n", ticks + 1,
	   wooded, lumber, wooded * lumber);
  }

  /* Part 3 - iterate until cycle detected */
  while (memcmp(grida[ticks % 2], gridb[0], sizeof gridb[0])) {
    if (!(ticks % 10))
      printf("%5d\n", ticks);
    for (y = 1; y <= count; y++)
      for (x = 1; x <= count; x++) {
	grida[(ticks + 1) % 2][y][x] = compute(grida[ticks % 2], x, y,
					       NULL, NULL);
	gridb[1][y][x] = compute(gridb[0], x, y, NULL, NULL);
      }
    for (y = 1; y <= count; y++)
      for (x = 1; x <= count; x++)
	gridb[0][y][x] = compute(gridb[1], x, y, NULL, NULL);
    ticks++;
  }
  printf("detected cycle at minute %d\n", ticks);

  /* Part 4 - fast forward, then finish the task */
  memcpy(gridb[1], gridb[0], sizeof gridb[0]);
  for (ticks = 1000000000 - 1000000000 % ticks; ticks < 1000000000; ticks++) {
    wooded = lumber = 0;
    for (y = 1; y <= count; y++)
      for (x = 1; x <= count; x++)
	gridb[(ticks + 1) % 2][y][x] = compute(gridb[ticks % 2], x, y,
					       &wooded, &lumber);
    printf("After %d minutes, resource value %d*%d = %d\n", ticks + 1,
	   wooded, lumber, wooded * lumber);
  }
  return 0;
}
