#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <inttypes.h>
#include <assert.h>

bool debug(const char *fmt, ...) {
  va_list ap;
  if (getenv("DEBUG")) {
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
    return true;
  }
  return false;
}

char visit(int depth, char end, int *score) {
  int c = getchar();
  debug("depth %d, visiting %c while looking for %c, score %d\n",
	depth, c, end, *score);
  switch (c) {
  case 'N': case 'E': case 'S': case 'W':
    ++*score;
    return visit(depth, end, score);
  case '$':
    if (end != c || depth != 0) {
      fprintf(stderr, "unbalanced regex\n");
      exit(1);
    }
    return c;
  case '(':
    {
      int tmp = 0;
      int best = 0;
      c = visit(depth + 1, '|', &best);
      assert(c == '|' && best);
      while (visit(depth + 1, ')', &tmp) == '|') {
	assert(tmp);
	if (tmp > best)
	  best = tmp;
	tmp = 0;
      }
      if (tmp > best)
	best = tmp;
      if (tmp) {
	*score += best;
	return visit(depth, end, score);
      }
      debug("checking after empty alternation at depth %d\n", depth);
      best = *score + best / 2;
      c = visit(depth, end, score);
      if (best > *score)
	*score = best;
      return c;
    }
  case '|': case ')':
    if (end != ')' && end != c) {
      fprintf(stderr, "unbalanced regex\n");
      exit(1);
    }
    return c;
  default:
    fprintf(stderr, "unexpected EOF\n");
    exit(1);
  }
}

int main(int argc, char **argv) {
  int score = 0;

  if (argc > 1 && !(stdin = freopen(argv[1], "r", stdin))) {
    perror("failure");
    exit(2);
  }
  if (getchar() != '^') {
    fprintf(stderr, "invalid start\n");
    exit(1);
  }
  visit(0, '$', &score);
  printf("Furthest point is %d doors away\n", score);
  return 0;
}
