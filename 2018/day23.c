#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <inttypes.h>
#include <assert.h>
#include <ctype.h>

bool __attribute__((format(printf, 1, 2)))
debug(const char *fmt, ...) {
  va_list ap;
  if (getenv("DEBUG")) {
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
    return true;
  }
  return false;
}

#define LIMIT 1000

static struct data {
  int x;
  int y;
  int z;
  int r;
} array[LIMIT];

static int near[3][3][3];

static bool inrange(int minx, int miny, int minz, int step, struct data *d) {
  int dx, dy, dz;
  minx -= step / 2;
  miny -= step / 2;
  minz -= step / 2;
  if (0 && d == &array[0])
    debug(" inrange(%d,%d,%d - %d,%d,%d)\n",
	minx, miny, minz, minx + step, miny + step, minz + step);
  if (d->x >= minx && d->x <= minx + step) {
    if (d->y >= miny && d->y <= miny + step) {
      if (d->z >= minz && d->z <= minz + step) {
	// x,y,z
	return true;
      } else {
	// x,y but not z
	dz = abs((d->z < minz ? minz : minz + step) - d->z);
	return dz <= d->r;
      }
    } else {
      if (d->z >= minz && d->z <= minz + step) {
	// x,z but not y
	dy = abs((d->y < miny ? miny : miny + step) - d->y);
	return dy <= d->r;
      } else {
	// x but not y,z
	dy = abs((d->y < miny ? miny : miny + step) - d->y);
	dz = abs((d->z < minz ? minz : minz + step) - d->z);
	return dy + dz <= d->r;
      }
    }
  } else {
    if (d->y >= miny && d->y <= miny + step) {
      if (d->z >= minz && d->z <= minz + step) {
	// y,z but not x
	dx = abs((d->x < minx ? minx : minx + step) - d->x);
	return dx <= d->r;
      } else {
	// y but not x,z
	dx = abs((d->x < minx ? minx : minx + step) - d->x);
	dz = abs((d->z < minz ? minz : minz + step) - d->z);
	return dx + dz <= d->r;
      }
    } else {
      if (d->z >= minz && d->z <= minz + step) {
	// z but not x,y
	dx = abs((d->x < minx ? minx : minx + step) - d->x);
	dy = abs((d->y < miny ? miny : miny + step) - d->y);
	return dx + dy <= d->r;
      } else {
	// not x,y,z
	dx = abs((d->x < minx ? minx : minx + step) - d->x);
	dy = abs((d->y < miny ? miny : miny + step) - d->y);
	dz = abs((d->z < minz ? minz : minz + step) - d->z);
	return dx + dy + dz <= d->r;
      }
    }
  }
}

static bool closer(int x1, int y1, int z1, int x2, int y2, int z2, int step) {
  int d = step / 2;
  return abs(x1 + d) + abs(y1 + d) + abs(z1 + d)
    > abs(x2 + d) + abs(y2 + d) + abs(z2 + d);
}

static bool search(int count, int *x, int *y, int *z, int step, int *found) {
  int i, j, k, l;
  struct data best = {0};
  bool move;
  memset(near, 0, sizeof near);
  for (i = 0; i <= 2; i++)
    for (j = 0; j <= 2; j++)
      for (k = 0; k <= 2; k++) {
	int tryx = step > 1 ? *x + step / 2 * (i - 1) : *x + i - 1;
	int tryy = step > 1 ? *y + step / 2 * (j - 1) : *y + j - 1;
	int tryz = step > 1 ? *z + step / 2 * (k - 1) : *z + k - 1;
	for (l = 0; l < count; l++)
	  if (inrange(tryx, tryy, tryz, step, &array[l]))
	    near[i][j][k]++;
	if (near[i][j][k] > best.r
	    || (near[i][j][k] == best.r
		&& closer(best.x, best.y, best.z, tryx, tryy, tryz, step))) {
	  best.x = tryx;
	  best.y = tryy;
	  best.z = tryz;
	  best.r = near[i][j][k];
	  move = i != 1 || j != 1 || k != 1;
	}
      }
  if (debug("after search %d,%d,%d step %d:\n", *x, *y, *z, step)) {
    for (j = 0; j <= 2; j++)
      debug("%5d %5d %5d   %5d %5d %5d   %5d %5d %5d\n",
	    near[0][j][0], near[1][j][0], near[2][j][0],
	    near[0][j][1], near[1][j][1], near[2][j][1],
	    near[0][j][2], near[1][j][2], near[2][j][2]);
    debug("best %d at %d,%d,%d, move %d, next step %d\n", best.r, best.x,
	  best.y, best.z, move, move ? step : step / 2);
  }
  *x = best.x;
  *y = best.y;
  *z = best.z;
  *found = best.r;
  return move;
}

int main(int argc, char **argv) {
  size_t len = 0, count = 0;
  char *line;
  int best = 0;
  int i;
  int nearby = 0;
  int x = 0, y = 0, z = 0;
  int step = -1;
  int iter = 0;

  /* Part 1 - read data */
  if (argc > 5)
    step = atoi(argv[5]);
  if (argc > 4) {
    x = atoi(argv[2]);
    y = atoi(argv[3]);
    z = atoi(argv[4]);
  }
  if (argc > 1)
    if (!(stdin = freopen(argv[1], "r", stdin))) {
      perror("failure");
      exit(2);
    }

  while (getline(&line, &len, stdin) >= 0) {
    if (count >= LIMIT) {
      fprintf(stderr, "recompile with larger LIMIT!\n");
      exit(1);
    }
    if (sscanf(line, "pos=<%d,%d,%d>, r=%d\n", &array[count].x,
	       &array[count].y, &array[count].z, &array[count].r) != 4) {
      fprintf(stderr, "bad input\n");
      exit(1);
    }
    if (array[count].r > array[best].r)
      best = count;
    ++count;
  }
  printf("Read %zu lines, largest r %d at %d,%d,%d\n", count, array[best].r,
	 array[best].x, array[best].y, array[best].z);

  for (i = 0; i < count; i++)
    if (abs(array[i].x - array[best].x) + abs(array[i].y - array[best].y)
	+ abs(array[i].z - array[best].z) <= array[best].r)
      nearby++;
  printf("Found %d nodes within range of %d,%d,%d\n", nearby,
	 array[best].x, array[best].y, array[best].z);
  if (step < 0)
    step = 1 << (32 - __builtin_clz(array[best].r));
  printf("Beginning search at %d,%d,%d with step of %d\n", x, y, z, step);

  /* Part 2 - find best spot */
  while (step) {
    if (!(iter % 1000) || step < 2)
      printf("%d iterations, currently at %d,%d,%d nearby %d step %d\n",
	     iter, x, y, z, nearby, step);
    iter++;
    if (!search(count, &x, &y, &z, step, &nearby))
      step = step * 3 / 4;
  }
  while (search(count, &x, &y, &z, step, &nearby)) {
    iter++;
    printf("%d iterations, currently at %d,%d,%d nearby %d step %d\n",
	   iter, x, y, z, nearby, step);
  }
  printf("After %d iterations, best is %d,%d,%d with %d nearby, or %d\n",
	 iter, x, y, z, nearby, abs(x) + abs(y) + abs(z));
  return 0;
}
