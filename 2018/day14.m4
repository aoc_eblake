divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dgoal=N] [-Dhashsize=H] [-Dfile=day14.input] day14.m4
# Optionally use -Dverbose=1 to see some progress
# Optionally use -Donly=[12] to skip a part

include(`common.m4')ifelse(common(14, 4999999), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

ifdef(`goal', `', `define(`goal', translit(include(defn(`file')), nl))')

define(`I', defn(`incr'))define(`D', defn(`define'))define(`N', defn(`defn'))
define(`S', defn(`substr'))define(`F', defn(`ifdef'))
define(`E', defn(`ifelse'))define(`X', defn(`index'))
define(`rev', `ifelse(`$1', `', `', `$0(substr(`$1', 1))substr(`$1', 0, 1)')')
# Define E0 through E9, where En(recipes, recent, nexta, remaining) pushes
# recipe N by bumping recipes, extending recent, and updating nexta/remaining
define(`prep', `_$0(`$1', `$', `1', `2', `3', `4')')
define(`_prep', `define(`E$1', `I(`$2$3'),`$1$2$4',A$2$6(`$1',`$2$5')')')
forloop_arg(0, 9, `prep')
# Enhance E1 (if goal ends in 1) to end iteration if recent matches goal
define(`prep', `_$0(substr(goal, decr(len(goal))), `$', `1', `2', `3')')
define(`_prep', `define(`E$1', defn(`E$1')`E(X(`$2$4','dquote(substr(rev(
  goal), 1))`),`-1',,`c(`$2$3',`$2$4',`$2$5')')')')
prep()
# Define E10 through E18 to push 1 then the second digit
define(`prep', `define(`E1$1', `E$1('defn(`E1')`)')')
forloop_arg(0, 8, `prep')
# Map eMN($@) to E[M+N]($@)
define(`prep', `_$0(eval(`$1', 10, 2), eval(`$1/10+$1%10'))')
define(`_prep', `define(`e$1', defn(`E$2'))')
forloop_arg(0, 99, `prep')
# Define A0 through A9, where A[remaining](value, nexta) either reduces
# remaining or defines a[nexta], producing new nexta/remaining
define(`A0', `a$2(`$1',I(`$2'))I(`$2'),$1')
define(`prep', `define(`A$1', $`2,'decr(`$1'))')
forloop_arg(1, 9, `prep')
# Initial contents of the final array entry. When called as aN(wrap) (from
# do), it forwards to a[wrap]; when called as aN(nexta, nexta+1) (from A0),
# it defines a[nexta] and sets nexta+1 as the next unused entry
define(`a', `E(`$2',,`a$1',`D(`a$2',N(`a'))D(`$0',``$1',`$2'')')')
# Prepopulate the leading array entries, where aN => value, nexta
define(`a0', ``3',`4'')define(`a1', ``7',`9'')define(`a2', ``3',`4'')
define(`a3', ``0',`4'')define(`a4', ``1',`6'')define(`a5', ``0',`6'')
define(`a6', ``1',`8'')define(`a7', ``2',`10'')define(`a8', ``4',`13'')
define(`a9', ``5',`15'')define(`a10', ``1',`12'')define(`a12', ``8',`21'')
define(`a13', ``9',`23'')define(`a15', ``6',`22'')define(`a21', ``1',`23'')
define(`a22', ``0',`23'')define(`a23', ``7',`24'')define(`a24', defn(`a'))
define(`need', eval(incr(goal)+9))
define(`rename', `define(`$2', defn(`$1'))popdef(`$1')')
define(`do100000', `output(1, `...$1')rename(`$0', `do'eval(
  `$1+100000'))rename(`do'incr(`$1'), `do'eval(`$1+100001'))')
define(`do100001', `output(1, `...$1')rename(`$0', `do'eval(
  `$1+100000'))rename(`do'decr(`$1'), `do'eval(`$1+99999'))')
# do(idx1, idx2, recipes, recent, nexta, remaining)
define(`do', `F(`$0$3',`$0$3(`$3',`$4')')_$0(a$1(`$6'),a$2(`$6'),`$3','dnl
`S(`$4',0,s),`$5',`$6')')
# _do(val1, nextidx1, val2, nextidx2, recipes, recent, nexta, remaining)
define(`_do', `do(`$2',`$4',e$1$3(`$5',`$6',`$7',`$8'))')
define(`s', decr(len(goal)))pushdef(`s', 9)
define(`do'need, `define(`part1', rev(substr(`$2', 0, 10)))popdef(`do'incr(
  need))popdef(`s')')
define(`do'incr(need), `define(`part1', rev(substr(`$2', 1, 10)))popdef(`s')')
ifelse(defn(`only'), 1, `
define(`do'need, defn(`do'need)`pushdef(`_do')')
define(`do'incr(need), defn(`do'incr(need))`pushdef(`_do')')
define(`c')
', `
define(`c', `define(`part2', eval($1 + 1 - len(goal) - index(`$2',
  'substr(rev(goal), 1)`)))output(1, `array grew to $3')pushdef(`_do')')
')
# Begin iterating after 24 recipes are known: elf1 is at a4, elf2 is
# at a13, the 10 most recently added recipes are 1 at slot 14 to 7 at
# slot 23, and we have 7 steps to go before a24 gets marked used
do(`4', `13', `24', `7015297761', `24', `7')

divert`'part1
part2
