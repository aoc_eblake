divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day10.input] day10.m4

include(`common.m4')ifelse(common(10), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

include(`ocr.m4')
define(`cnt', 0)define(`miny', 0)define(`maxy', 0)
define(`pos', `define(`p'cnt, ``$1',`$2'')ifelse(eval($2 > maxy), 1,
  `define(`maxy', `$2')', eval($2 < miny), 1, `define(`miny',
  `$2')define(`botp', cnt)')')
define(`vel', `define(`v'cnt, ``$1',`$2'')define(`cnt', incr(cnt))')
translit((include(defn(`file'))), `<>=', `()'define(`position',
  defn(`pos'))define(`velocity', defn(`vel')))
define(`part2', eval((maxy - miny)/10))
define(`_adjust', `$0_(`$1', eval(`$2+$6*$4'), eval(`$3+$6*$5'))')
define(`_adjust_', `define(`p$1', ``$2',`$3'')ifelse(ifdef(`minx',
  `eval($2 < minx)', 1), 1, `define(`minx', `$2')')ifelse(`$1', 'botp`,
  `define(`miny', `$3')')')
define(`adjust', `_$0(`$1', p$1, v$1, 'part2`)')
forloop_arg(0, decr(cnt), `adjust')
define(`_adjust_', `define(`g$2_$3')')
define(`adjust', `_$0(`$1', p$1, 'minx`, 'miny`, `-1')')
forloop_arg(0, decr(cnt), `adjust')

define(`_grab', `forloop($2, eval($2+6), `ifdef(`g'', ``_$1', `X', ` ')')')
define(`grab', `ocr(forloop(0, 9, `_$0(', `, 'eval($1*8)`)'))')
define(`part1', forloop_arg(0, 7, `grab'))

divert`'part1
part2
