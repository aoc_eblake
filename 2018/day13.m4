divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dhashsize=H] [-Dfile=day13.input] day13.m4
# Optionally use -Dverbose=1 to see some progress

include(`common.m4')ifelse(common(13, 65537), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`D', defn(`define'))define(`U', defn(`popdef'))
define(`I', defn(`ifelse'))define(`F', defn(`ifdef'))
define(`P', defn(`incr'))define(`M', defn(`decr'))
define(`N', defn(`defn'))define(`E', defn(`eval'))define(`S', defn(`shift'))
define(`input', translit(include(defn(`file')), nl` +\/^>v<|-', `01234nesw11'))
define(`x', 0)define(`y', 0)define(`cnt', 0)
define(`v0', `D(`x', 0)D(`y',P(`$2'))')
define(`v1', `D(`x',P(`$1'))')
define(`v2', `D(`g$1_$2',`2')v1(`$1')')
define(`v3', `D(`g$1_$2',`3')v1(`$1')')
define(`v4', `D(`g$1_$2',`4')v1(`$1')')
define(`cart', `D(`c$4',`$1,$2,`$3',`l'')D(`c$1_$2',`$4')D(`l',
  N(`l')`,$4')D(`cnt',P(`$4'))v1(`$1')')
define(`vn', `cart($@,`n',cnt)')
define(`ve', `cart($@,`e',cnt)')
define(`vs', `cart($@,`s',cnt)')
define(`vw', `cart($@,`w',cnt)')
ifdef(`__gnu__', `
  patsubst(defn(`input'), `.', `v\&(x,y)')
', `
  define(`chew', `ifelse(`$1', `1', `v$2(x,y)', `$0(eval(`$1/2'), substr(
    `$2', 0, eval(`$1/2')))$0(eval(`$1-$1/2'), substr(`$2', eval(`$1/2')))')')
  chew(len(defn(`input')), defn(`input'))
')
define(`lt', `E(`$2<$6||($2==$6&&$1<$5)')')
define(`sort', `I(`$2',,`,`$1'',`I(lt(c$1,c$2),1,`,$@',
  `,`$2'$0(`$1',S(S($@)))')')')
define(`act', `I(`$2',,,`F(`d$2',,`move(c$2,`$2')')$0(S($@))')')
define(`crash', `F(`part1', `', `D(`part1', `$1,$2')')D(`d'c$1_$2)D(`d$3')U(
  `c$1_$2')D(`left', M(M(left)))')
define(`move', `U(`c$1_$2')_$0($0$3(`$1',`$2'),`$3',`$4',`$5')')
define(`_move', `F(`c$1_$2',`crash(`$1',`$2',`$5')',`D(`c$1_$2',
  `$5')D(`c$5',`$1,$2,'turn(`$3',`$4',N(`g$1_$2')))D(`l',N(`l')`,$5')')')
define(`moven', ``$1',M(`$2')')
define(`movee', `P(`$1'),`$2'')
define(`moves', ``$1',P(`$2')')
define(`movew', `M(`$1'),`$2'')
define(`turn', `I(`$3',,```$1',`$2''',`$3',`2',`N(`t$1$2')',
  `N(`t$3$1')`,`$2''')')
define(`tnl', ``w',`f'')define(`tnf', ``n',`r'')define(`tnr', ``e',`l'')
define(`tel', ``n',`f'')define(`tef', ``e',`r'')define(`ter', ``s',`l'')
define(`tsl', ``e',`f'')define(`tsf', ``s',`r'')define(`tsr', ``w',`l'')
define(`twl', ``s',`f'')define(`twf', ``w',`r'')define(`twr', ``n',`l'')
define(`t3n', ``w'')define(`t3e', ``s'')
define(`t3s', ``e'')define(`t3w', ``n'')
define(`t4n', ``e'')define(`t4e', ``n'')
define(`t4s', ``w'')define(`t4w', ``s'')
define(`_tick', `I(`$2', `', `D(`l')', `sort($2$0(S($@)))')')
define(`tick', `act(_$0(l))I($2, 1, `P(`$1')', `I(E(
  `$1%1000'), 0, `output(1, `...$1 'left)')$0(P(`$1'), `$2')')')
define(`left', cnt)
tick(tick(0, `ifdef(`part1', 1)'), `left')
define(`find', `ifdef(`d$1', `', `_$0(c$1)')')
define(`_find', `define(`part2', `$1,$2')')
forloop_arg(0, cnt-1, `find')

divert`'part1
part2
