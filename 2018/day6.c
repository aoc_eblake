#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>

void debug(const char *fmt, ...) {
  va_list ap;
  if (getenv("DEBUG")) {
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
  }
}

#define LIMIT 1000
#define LINES 60

static struct data {
  unsigned char line; /* non-zero only for coordinates in list */
  unsigned char nearby; /* 0 for unchecked or tie, else line id of nearest */
} array[LIMIT][LIMIT];
static struct line {
  unsigned short x;
  unsigned short y;
  bool edge;
  unsigned short count;
} list[LINES]; /* line 0 unused */
static unsigned short minx = LIMIT, miny = LIMIT, maxx = 0, maxy = 0;

/* Return 0 to keep looking, non-zero for tie */
static int check(int x, int y, int *r) {
  if (x < minx || x > maxx || y < miny || y > maxy)
    return 0;
  if (array[x][y].line) {
    if (*r) {
      debug("point %d,%d has at least two nearest neighbors\n", x, y);
      return 1;
    }
    *r = array[x][y].line;
  }
  return 0;
}

/* Return 0 for tie, else line of nearest coordinate */
static unsigned char compute(int x, int y) {
  int d = 1, i;
  int r = 0;
  if (array[x][y].line)
    return array[x][y].line;
  while (true) {
    int tryx = x + d;
    int tryy = y;
    r = 0;
    for (i = 0; i < d; i++) {
      if (check(tryx, tryy, &r))
	return 0;
      tryx--;
      tryy++;
    }
    for (i = 0; i < d; i++) {
      if (check(tryx, tryy, &r))
	return 0;
      tryx--;
      tryy--;
    }
    for (i = 0; i < d; i++) {
      if (check(tryx, tryy, &r))
	return 0;
      tryx++;
      tryy--;
    }
    for (i = 0; i < d; i++) {
      if (check(tryx, tryy, &r))
	return 0;
      tryx++;
      tryy++;
    }
    if (r)
      return r;
    debug("no neighbors of %d,%d within %d\n", x, y, d);
    d++;
  }
}

int main(int argc, char **argv) {
  size_t len = 0, count = 0;
  char *line;
  int i, j;
  int x, y;

  /* Part 1 - read in lines */
  if (argc > 1)
    if (!(stdin = freopen(argv[1], "r", stdin))) {
      perror("failure");
      exit(2);
    }

  while (getline(&line, &len, stdin) >= 0) {
    ++count;
    if (count >= LINES) {
      fprintf(stderr, "recompile with larger LINES!\n");
      exit(1);
    }
    if (sscanf(line, "%d, %d ", &x, &y) != 2) {
      fprintf(stderr, "bad input\n");
      exit(1);
    }
    if (x > LIMIT || y > LIMIT) {
      fprintf(stderr, "recompile with larger LIMIT!\n");
      exit(1);
    }
    list[count].x = x;
    list[count].y = y;
    array[x][y].line = count;
    if (x > maxx)
      maxx = x;
    if (x < minx)
      minx = x;
    if (y > maxy)
      maxy = y;
    if (y < miny)
      miny = y;
  }
  printf("Read %zu lines, bounding box %d,%d to %d,%d\n", count,
	 minx, miny, maxx, maxy);
  minx--;
  miny--;
  maxx++;
  maxy++;

  /* Part 2 - compute nearest neighbors */
  for (j = miny; j <= maxy; j++)
    for (i = minx; i <= maxx; i++) {
      unsigned char nearest = compute(i, j);
      if (nearest) {
	array[i][j].nearby = nearest;
	list[nearest].count++;
	if (i == minx || i == maxx || j == miny || j == maxy)
	  list[nearest].edge = true;
      }
    }

  /* Part 3 - determine non-edge point with most neighbors */
  int best = 0;
  for (i = 1; i <= count; i++) {
    debug("point %d,%d has %d neighbors\n", list[i].x, list[i].y,
	  list[i].count);
    if (list[i].edge)
      continue;
    if (list[i].count > list[best].count)
      best = i;
  }
  printf("best point %d,%d has %d neighbors\n", list[best].x, list[best].y,
	 list[best].count);

  /* Part 4 - determine number of points within 10000 of all list members */
  int good = 0;
  for (j = miny; j <= maxy; j++)
    for (i = minx; i <= maxx; i++) {
      int sum = 0;
      for (x = 1; x <= count; x++)
	sum += abs(i - list[x].x) + abs(j - list[x].y);
      debug("point %d,%d sum %d\n", i, j, sum);
      if (sum < 10000)
	good++;
    }
  printf("found %d points within 10000\n", good);
  return 0;
}
