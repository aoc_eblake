divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day24.input] day24.m4
# Optionally use -Dverbose=1 to see some progress
# Optionally use -Dpriority=0|1|2|3|4|5 to choose priority queue algorithms

include(`common.m4')ifelse(common(24), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`radiation', 1)define(`cold', 2)define(`fire', 4)
define(`bludgeoning', 8)define(`slashing', 16)
define(`ignore')define(`hit', `(')
define(`units', `ignore(')define(`with', `)')
define(`an', `ignore(')define(`does', `)')
define(`damage', `ignore(')define(`initiative', `)')
define(`input', translit((include(defn(`file'))), nl`( );,', `:<.>'))
define(`type', 1)define(`max', 0)
define(`bump', `ifelse(eval($1<$2), 1, `define(`$1', $2)')')
define(`map', `eval(0_$0$3)')
define(`_map', `ifelse(`$1', `', `', `ifelse(`$1', `weak', `define(`scale',
  5)', `$1', `immune', `define(`scale', 0)', `$1', `to', `',
  `|($1<<scale)')$0(shift($@))')')
define(`line', `ifelse(`$1', `Infection', `define(`type', 0)',
  eval(len(`$1')>20), 1, `_$0(translit(`$1', `<.>', `(,)'))')')
define(`_line', `setup(type, $1, $3, map$4, $6, $7, $9)')
define(`setup', `bump(`max', $7)define(`type$7', $1)define(`def$7', 0)define(
  `off$7', 0)define(`units$7', $2)define(`hp$7', $3)define(`mod$7',
  $4)define(`atk$7', $5)define(`dam$7', $6)')
ifdef(`__gnu__', `
  patsubst(defn(`input'), `\([^:]*\):', `line(`\1')')
', `
  define(`_chew', `line(substr(`$1', 0, index(`$1', `:')))define(`tail',
    substr(`$1', incr(index(`$1', `:'))))ifelse(index(defn(`tail'), `:'), -1,
    `', `$0(defn(`tail'))')')
  define(`chew', `ifelse(eval(`$1 < 200'), 1, `_$0(`$2')', `$0(eval(`$1/2'),
    substr(`$2', 0, eval(`$1/2')))$0(eval(len(defn(`tail'))` + $1 - $1/2'),
    defn(`tail')substr(`$2', eval(`$1/2')))')')
  chew(len(defn(`input')), defn(`input'))
')

include(`priority.m4')

define(`prep', `define(`Atk$2', eval(atk$2+$1*type$2))define(`eff$2',
  eval(units$2*Atk$2))pushdef(`list'eval(1-type$2), $2)')
define(`reset', `undefine(`list0')undefine(`list1')forloop(1, 'max`,
  `prep($1, ', `)')')
define(`push', `_$0($1, eff$1)')
define(`_push', `ifelse($2, 0, `',
  `insert(eval(`100000000-$1-$2*'''max``), $1)')')
define(`damage', `_$0(eff$1, dam$1, mod$2)')
define(`_damage', `eval(`$1*!($2&$3)*(1+!!(($2<<5)&$3))')')
define(`try', `ifelse(eff$2, 0, `popdef(`list'type$1)', off$2, 0,
  `_$0(damage($1, $2), bestd, $2, eff$2)')')
define(`_try', `ifelse(eval(`$1>$2||($1&&$1==$2&&$4>='beste`)'), 1,
  `define(`bestd', $1)define(`bestu', $3)define(`beste', $4)')')
define(`select', `ifelse($1, `', `', `define(`bestu', 0)define(`bestd',
  0)define(`beste', 0)_$0($2)$0(pop)')')
define(`_select', `_stack_foreach(`list'type$1, `try($1, ', `)', `t')pair($1,
  bestu)')
define(`pair', `ifelse($2, 0, `', `define(`def$1', $2)define(`off$2', $1)')')
define(`attack', `forloop_rev('max`, 1, `_$0(', `)')')
define(`_attack', `kill($1, def$1)')
define(`kill', `ifelse($2, `0', `', `_$0($1, $2, eff$2, damage($1, $2),
  hp$2, Atk$2)define(`def$1', 0)define(`off$2', 0)')')
define(`_kill', `ifelse(eval(`$4>=$5'), 1, `-define(`eff$2',
  eval(`($3/$6>$4/$5)*($3/$6-$4/$5)*$6'))')')
define(`count', 0)
define(`round', `ifelse(forloop_arg(1, max, `push')select(pop)attack(), `', `',
  `ifelse(eval(count%1000), 0, `output(1, `...boost $2, round $1')')define(
  `count', incr(count))$0(incr($1), $2)')')
define(`tally', `+eff$1/Atk$1')
define(`battle', `reset($1)round(1, $1)ifelse(ifdef(`list1', $1, 0), 0,
  `eval(forloop_arg(1, max, `tally'))', `$0(incr($1))')')
define(`part1', battle(0))
define(`part2', battle(1))

divert`'part1
part2
