#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <inttypes.h>
#include <assert.h>

bool debug(const char *fmt, ...) {
  va_list ap;
  if (getenv("DEBUG")) {
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
    return true;
  }
  return false;
}

#define LIMIT 25000000

static char list[LIMIT] = "37";

static void dump(int iter, int size, int idx1, int idx2) {
  if (!debug("iter %d:", iter))
    return;
  for (int i = 0; i < size; i++)
    if (i == idx1)
      debug("(%c)", list[i]);
    else if (i == idx2)
      debug("[%c]", list[i]);
    else
      debug(" %c", list[i]);
  debug("\n");
}

int main(int argc, char **argv) {
  const char *goal = "077201";
  char buf[7] = "";
  int max = LIMIT;
  int size = 2;
  int idx1 = 0;
  int idx2 = 1;
  int iter = 0;
  FILE *f;

  if (argc > 1)
    goal = argv[1];
  f = fopen(goal, "r");
  if (f) {
    if (fread(buf, 1, 6, f) != 6) {
      fprintf(stderr, "error reading file %s\n", goal);
      exit(1);
    }
    goal = buf;
    fclose(f);
  }
  if (argc > 2)
    max = atoi(argv[2]);
  if (max > LIMIT) {
    fprintf(stderr, "recompile with larger limit\n");
    exit(1);
  }

  while (!strstr(list + size - 10, goal) && size < max - 1) {
    iter++;
    int e = list[idx1] - '0' + list[idx2] - '0';
    if (e > 9) {
      list[size++] = '1';
      e -= 10;
    }
    list[size++] = e + '0';
    idx1 = (list[idx1] - '0' + idx1 + 1) % size;
    idx2 = (list[idx2] - '0' + idx2 + 1) % size;
    if (!(size & 0xffffe))
      printf("%d...\n", size);
  }
  dump(iter, size, idx1, idx2);
  char *p = strstr(list + size - 10, goal);
  if (p)
    printf("%td recipes before goal %s (%d iterations)\n",
	   p - list, goal, iter);
  else
    printf("%d iterations and size %d insufficient to find goal %s\n",
	   iter, size, goal);
  return 0;
}
