divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Ddelay=N] [-Dworkers=N] [-Dfile=day7.input] day7.m4

include(`common.m4')ifelse(common(7), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

ifdef(`delay', `', `define(`delay', 60)')
ifdef(`workers', `', `define(`workers', 5)')

define(`prep', `define(`l$1', substr('ALPHA`, `$1', 1))define(`d'l$1,
  eval(delay`+1+$1'))')
forloop_arg(0, 25, `prep')
define(`last', `A')
define(`add', `define(`pre$8', defn(`pre$8')`$2')ifelse(eval(d$8 >
  defn(`d'last)), 1, `define(`last', `$8')')')
translit(include(defn(`file')), ` ', `,'define(`Step',
  `add(')define(`can', `)'))
define(`steps', eval(defn(`d'last) - delay))
define(`find', `define(`part1', `$1'_$0(1, l0, `$1'))')
define(`_find', `ifelse(`$1', 'incr(steps)`, `', translit(translit(`$2',
  defn(`q'))`'defn(`pre$2'), `$3'), `$2', ``$2'', `$0(incr(`$1'),
  l$1, `$3')')')
forloop(0, decr(steps), `find(defn(`part1'),', `)')
forloop(1, workers, `define(`w'', ``t', 0)')
define(`_next', `ifelse(eval(`$2 && $2 < $1'), 1, `$2', `$1')')
define(`next', `ifelse($2, 'incr(workers)`, `$1', `$0(ifelse($1, 0, `w$2t',
  `_$0(`$1', w$2t)'), incr($2))')')
define(`do', `define(`q')forloop(1, 'workers`, `$0_1(', `, `$1')')ifelse(
  len(defn(`done')), 'steps`, `$1', `forloop(1, 'workers`, `$0_2(',
  `, 'defn(`done')`, defn(`q'), `$1')')$0(next(0, 1))')')
define(`do_1', `ifelse(w$1t, `$2', `define(`done', defn(`done')defn(
  `w$1l'))define(`w$1t', 0)define(`w$1l')', `define(`q',
  defn(`q')defn(`w$1l'))')')
define(`do_2', `ifelse(w$1t, 0, `define(`w$1l', _find(1, l0, `$2'))ifelse(w$1l,
  `', `', `define(`q', defn(`q')defn(`w$1l'))define(`w$1t',
  eval($4 + defn(`d'w$1l)))')')')
define(`part2', do(0))

divert`'part1
part2
