#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <inttypes.h>
#include <assert.h>

void debug(const char *fmt, ...) {
  va_list ap;
  if (getenv("DEBUG")) {
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
  }
}

static int regs[6];
static uint8_t ip;
typedef struct op {
  char op;
  int a;
  int b;
  int c;
  int visited;
} op;
static op *program;
static unsigned int max;

static int lookup(const char *opcode) {
  switch (*opcode) {
  case 'a':
    return opcode[3] == 'i';
  case 'm':
    return 2 + (opcode[3] == 'i');
  case 'b':
    return 4 + 2 * (opcode[1] == 'o') + (opcode[3] == 'i');
  case 's':
    return 8 + (opcode[3] == 'i');
  case 'g':
    return 10 + (opcode[2] == 'r') + (opcode[3] == opcode[2]);
  case 'e':
    return 13 + (opcode[2] == 'r') + (opcode[3] == opcode[2]);
  }
  fprintf(stderr, "failed decoding instruction %s\n", opcode);
  exit(1);
}

static int get(int r) {
  assert(0 <= r && r < sizeof regs / sizeof *regs);
  return regs[r];
}

static void set(int r, int v) {
  assert(0 <= r && r < sizeof regs / sizeof *regs);
  regs[r] = v;
  assert(v >= 0 || !"resize regs to be larger type");
}

static const char *step(int op, int a, int b, int c) {
  switch (op) {
  case 0:
    set(c, get(a) + get(b)); return "addr";
  case 1:
    set(c, get(a) + b); return "addi";
  case 2:
    set(c, get(a) * get(b)); return "mulr";
  case 3:
    set(c, get(a) * b); return "muli";
  case 4:
    set(c, get(a) & get(b)); return "banr";
  case 5:
    set(c, get(a) & b); return "bani";
  case 6:
    set(c, get(a) | get(b)); return "borr";
  case 7:
    set(c, get(a) | b); return "bori";
  case 8:
    set(c, get(a)); return "setr";
  case 9:
    set(c, a); return "seti";
  case 10:
    set(c, a > get(b)); return "gtir";
  case 11:
    set(c, get(a) > b); return "gtri";
  case 12:
    set(c, get(a) > get(b)); return "gtrr";
  case 13:
    set(c, a == get(b)); return "eqir";
  case 14:
    set(c, get(a) == b); return "eqri";
  case 15:
    set(c, get(a) == get(b)); return "eqrr";
  default:
    return NULL;
  }
}

int main(int argc, char **argv) {
  size_t len = 0, count = 0;
  char *line;
  op *o;

  /* Part 1 - read in entire program */
  if (argc > 2)
    regs[0] = atoi(argv[2]);
  if (argc > 1 && !(stdin = freopen(argv[1], "r", stdin))) {
    perror("failure");
    exit(2);
  }

  if (getline(&line, &len, stdin) < 0 ||
      sscanf(line, "#ip %hhu\n", &ip) != 1) {
    fprintf(stderr, "missing #ip line\n");
    exit(1);
  }
  while (getline(&line, &len, stdin) > 0) {
    program = reallocarray(program, ++count, sizeof *program);
    o = &program[count - 1];
    o->op = lookup(line);
    if (sscanf(line + 5, "%d %d %d\n", &o->a, &o->b, &o->c) != 3) {
      fprintf(stderr, "corrupt program\n");
      exit(1);
    }
    o->visited = 0;
  }
  printf("program has %u instructions\n", max = count);

  /* Part 2 - run it */
  count = 0;
  while (regs[ip] < max) {
    bool d = regs[ip] == 7;
    const char *s;
    if (!(count % 1000000))
      printf("%8zu...\n", count);
    if (d)
    debug("%5d: ip=%d [%d, %d, %d, %d, %d, %d] ", count, regs[ip], regs[0],
	  regs[1], regs[2], regs[3], regs[4], regs[5]);
    count++;
    o = &program[regs[ip]];
    s = step(o->op, o->a, o->b, o->c);
    if (d)
    debug("%s %d %d %d ", s, o->a, o->b, o->c);
    o->visited++;
    if (d)
    debug("[%d, %d, %d, %d, %d, %d] (%d)\n", regs[0], regs[1], regs[2],
	  regs[3], regs[4], regs[5], o->visited);
    regs[ip]++;
  }
  printf("completed execution after %zu steps, reg 0 contains %d, 5 is %d\n",
	 count, regs[0], regs[5]);
  for (int i = 0; i < max; i++) {
    o = &program[i];
    printf("program line %s %d %d %d executed %d times\n",
	   step(o->op, 0, 0, 0), o->a, o->b, o->c, o->visited);
  }
  return 0;
}
