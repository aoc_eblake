divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day22.input] day22.m4
# Optionally use -Dverbose=1 to see some progress
# Optionally use -Dalgo=dijkstra|astar to choose search algorithm
# Optionally use -Dpriority=0|1|2|3|4|5 to choose priority queue algorithms

include(`common.m4')ifelse(common(22), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`parse', `define(`d', $2)define(`tx', $4)define(`ty', $5)')
parse(translit((include(defn(`file'))), nl` ()', `,,'))
define(`g', `ifdef(`g$1_$2', `', `_g($1, $2)')g$1_$2`'')
define(`_g', `$0_($1, $2, ifelse($1$2, 00, 0, $1.$2, 'tx.ty`, 0, $2, 0,
  `eval($1*16807)', $1, 0, `eval($2*48271)', `n($1, $2, decr($1), decr($2))'))')
define(`_g_', `define(`e$1_$2', eval(($3+'d`)%20183))define(`g$1_$2',
  eval(e$1_$2%3))')
define(`n', `ifelse(g($3,$2)g($1,$4))eval(e$3_$2*e$1_$4)')

define(`tally', `forloop(0, 'tx`, `+g(', `, $1)')')
define(`part1', eval(forloop_arg(0, ty, `tally')))

# Pending priorities fall in narrow range
ifdef(`priority', `', `define(`priority', 2)')
include(`priority.m4')
ifdef(`algo', `', `define(`algo', `dijkstra')')

# rocky=neither=0, wet=torch=1, narrow=gear=2. addwork(x, y, equip, type, cost)
define(`swap10', `2, 0')define(`swap20', `1, 0')
define(`swap01', `2, 1')define(`swap21', `0, 1')
define(`swap02', `1, 2')define(`swap12', `0, 2')
define(`cnt', 0)

ifelse(algo, `dijkstra', `
output(1, `Using Dijkstra search')
define(`addwork', `ifelse(ifdef(`d$1_$2_$3', `eval($5 < d$1_$2_$3)', 1), 1,
  `define(`d$1_$2_$3', $5)insert($5, $1, $2, $3, $4)define(`cnt',
  incr(cnt))ifelse(eval(cnt%10000), 0, `output(1, ...cnt:`$*')')')')
define(`loop', `ifelse(`$2.$3.$4', `$6', $1, `neighbors($2, $3, $4, $5,
  incr(d$2_$3_$4))loop(pop, `$6')')')

', algo, `astar', `
output(1, `Using A* search')
define(`diff', `ifelse(eval($1 < $2), 1, $2 - $1, $1 - $2)')
define(`heur', `diff('tx`, $1) + diff('ty`, $2) + ($3 != 1) * 7')
define(`addwork', `ifelse(ifdef(`d$1_$2_$3', `eval($5 < d$1_$2_$3)', 1), 1,
  `define(`d$1_$2_$3', $5)_$0(eval($5 + heur($1, $2, $3)), $1, $2, $3, $4)')')
define(`_addwork', `define(`f$2_$3_$4', $1)insert($@)define(`cnt',
  incr(cnt))ifelse(eval(cnt%10000), 0, `output(1, ...cnt:`$*')')')
define(`loop', `ifelse(`$2.$3.$4', `$6', $1, eval($1 > f$2_$3_$4), 1, `loop(
  pop, `$6')', `neighbors($2, $3, $4, $5, incr(d$2_$3_$4))loop(pop, `$6')')')

', `output(0, `unknown search algorithm')m4exit(1)')

define(`distance', `addwork(0, 0, 1, 0, 0)loop(pop, 'tx.ty.1`)clearall()')
define(`check', `ifelse($1, -1, `', $1, 'eval(tx+30)`, `', $2, -1, `',
  `_$0($1, $2, $3, g($1, $2), $4)')')
define(`_check', `ifelse($3, $4, `', `addwork($@)')')
define(`neighbors', `addwork($1, $2, swap$3$4, eval($5+6))check(decr($1), $2,
  $3, $5)check($1, decr($2), $3, $5)check(incr($1), $2, $3, $5)check($1,
  incr($2), $3, $5)')
define(`part2', distance())

divert`'part1
part2
