#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <inttypes.h>
#include <assert.h>

void debug(const char *fmt, ...) {
  va_list ap;
  if (getenv("DEBUG")) {
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
  }
}

#define LIMIT 200
static bool next[32];
static int size;

static void dump(int g, bool *a) {
  for (int i = 0; i < size; i++)
    debug("%c", a[i] ? '#' : '.');
  debug("\n");
}

static void gen(int g, bool *a, bool *b) {
  int i;
  uint8_t act = 0;

  debug("beginning generation %d\n", g);
  assert(!(a[0] | a[1] | a[size - 2] | a[size - 1]));
  for (i = 0; i < size - 2; i++) {
    act = ((act << 1) & 0x1f) | a[i + 2];
    b[i] = next[act];
  }
}

static int sum(int offset, bool *a) {
  int s = 0;
  int i;
  for (i = 0; i < size; i++)
    if (a[i])
      s += i - offset + 1;
  return s;
}

int main(int argc, char **argv) {
  size_t len = 0, count = 0;
  char *line;
  char *p;
  int offset;
  int generations = 20;
  bool *state[2];
  int i;

  /* Part 1 - read in initial line */
  if (argc > 2)
    generations = atoi(argv[2]);
  if (argc > 1 && !(stdin = freopen(argv[1], "r", stdin))) {
    perror("failure");
    exit(2);
  }

  if (getline(&line, &len, stdin) < 0 || getchar() != '\n') {
    fprintf(stderr, "bad input\n");
    exit(1);
  }
  p = strchr(line, ':');
  if (p[1] != ' ') {
    fprintf(stderr, "bad input\n");
    exit(1);
  }
  p++;
  offset = 3 + LIMIT;
  size = strchr(p, '\n') - p + 2 * offset;
  state[0] = calloc(size, sizeof(bool));
  state[1] = calloc(size, sizeof(bool));
  count = offset - 2;
  while (*p != '\n')
    state[0][count++] = *p++ == '#';

  /* Part 2 - read in rules */
  count = 0;
  while (getline(&line, &len, stdin) >= 0) {
    ++count;
    int idx = 0;
    for (i = 0; i < 5; i++)
      idx = (idx << 1) | (line[i] == '#');
    next[idx] = line[9] == '#';
  }
  printf("Read %zu lines\n", count);
  assert(!(next[0] | next[1] | next[16])); /* Otherwise offset is too small */
  debug("%*c\n", offset, '0');
  dump(0, state[0]);

  /* Part 3 - determine output */
  int s = sum(offset, state[0]);
  int diff = 0;
  count = 0;
  for (i = 1; i <= LIMIT; i++) {
    int s2;
    gen(i, state[(i + 1) % 2], state[i % 2]);
    dump(i, state[i % 2]);
    s2 = sum(offset, state[i % 2]);
    if (generations == i)
      printf("sum at generation %d is %d\n", i, s2);
    if (s2 - s == diff)
      count++;
    else {
      count = 0;
      diff = s2 - s;
    }
    s = s2;
  }
  printf("pattern locked at diff of %d for last %zu iterations of %d\n",
	 diff, count, LIMIT);
  printf("sum(%d) is %d, projection is %" PRId64 "\n", LIMIT, s,
	 diff * (UINT64_C(50000000000) - LIMIT) + s);
  return 0;
}
