divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day19.input] day19.m4

include(`common.m4')ifelse(common(19), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`input', translit(include(defn(`file')), nl`#', `;'))
define(`ip', `r'substr(defn(`input'), 3, 1))
define(`list', substr(defn(`input'), 5))
define(`cnt', 0)
define(`visit', `_$0(cnt, translit(`$1', ` ', `,'))')
define(`_visit', `define(`i$1', `$2')define(`a$1', `$3,$4,$5')define(
  `cnt', incr($1))')
ifdef(`__gnu__', `
  patsubst(defn(`list'), `\([^;]*\);', `visit(`\1')')
', `
  define(`_chew', `visit(substr(`$1', 0, index(`$1', `;')))define(`tail',
    substr(`$1', incr(index(`$1', `;'))))ifelse(index(
    defn(`tail'), `;'), -1, `', `$0(defn(`tail'))')')
  define(`chew', `ifelse(eval(`$1 < 20'), 1, `_$0(`$2')', `$0(eval(`$1/2'),
    substr(`$2', 0, eval(`$1/2')))$0(eval(len(defn(`tail'))` + $1 - $1/2'),
    defn(`tail')substr(`$2', eval(`$1/2')))')')
  chew(len(defn(`list')), defn(`list'))
')
forloop(0, 5, `define(`r'', `, 0)')
define(`addr_', `define(`r$3', eval(r$1+r$2))')
define(`addi_', `define(`r$3', eval(r$1+$2))')
define(`mulr_', `define(`r$3', eval(r$1*r$2))')
define(`muli_', `define(`r$3', eval(r$1*$2))')
define(`banr_', `define(`r$3', eval(r$1&r$2))')
define(`bani_', `define(`r$3', eval(r$1&$2))')
define(`borr_', `define(`r$3', eval(r$1|r$2))')
define(`bori_', `define(`r$3', eval(r$1|$2))')
define(`setr_', `define(`r$3', r$1)')
define(`seti_', `define(`r$3', $1)')
define(`gtir_', `define(`r$3', eval($1>r$2))')
define(`gtri_', `define(`r$3', eval(r$1>$2))')
define(`gtrr_', `define(`r$3', eval(r$1>r$2))')
define(`eqir_', `define(`r$3', eval($1==r$2))')
define(`eqri_', `define(`r$3', eval(r$1==$2))')

# Peephole optimization: instead of computing sum of factors with O(value^2)
# pairs, compute it with O(sqrt(value)) effort.
define(`n2', 3)define(`n3', 5)define(`n5', 7)
define(`nextp', `ifdef(`n$1', `', `define(`n$1', _$0(incr(incr($1))))')n$1')
define(`_nextp', `ifelse(eval($1 % 3 && $1 % 5), 0, `$0(incr(incr($1)))',
  `ifelse(nargs(factor($1)), 2, $1, `$0(incr(incr($1)))')')')
define(nargs, `$#')
define(`factor', `ifelse($1, 1, `', `ifelse(eval($1 & 1), 0,
  `,2$0(eval($1 >> 1))', `_$0($1, 3)')')')
define(`_factor', `ifelse($1, 1, `', `ifelse(eval($2 * $2 > $1), 1, `,$1',
  `ifelse(eval($1 % $2), 0, `,$2$0(eval($1 / $2), $2)',
  `$0($1, nextp($2))')')')')

define(`part', `ifelse($1, $2, `incr($1)', `((($1 * $2) - 1) / ($2 - 1))')')
define(`total', `_$0(1, $2, shift($@))')
define(`_total', `ifelse(`$#', 3, `eval($1 * part($2, $3))',
  $3, $4, `$0($1, $2 * $3, shift(shift(shift($@))))',
  `$0(eval($1 * part($2, $3)), $4, shift(shift(shift($@))))')')

define(`eqrr_', `define(`r$3', eval(r$1==r$2))ifelse($1, $3, `hack(decr(ip),
  r$2)')')
define(`hack', `ifelse(defn(`i$1'), `mulr', `_$0(a$1, $2, incr($2))')')
define(`_hack', `define(`r0', total($4factor($4)))define(`r$1', $5)define(
  `r$2', $5)')
define(`run', `_$0(ip)')
define(`_run', `ifdef(`i$1', `i$1()_(a$1)define(defn(`ip'), incr(ip))$0(ip)')')
define(`part1', run()r0)
forloop(1, 5, `define(`r'', `, 0)')define(`r0', 1)
define(`part2', run()r0)

divert`'part1
part2
