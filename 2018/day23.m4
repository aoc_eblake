divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day23.input] day23.m4
# Optionally use -Dpriority=0|1|2|3|4|5 to choose priority queue algorithms

include(`common.m4')ifelse(common(23), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

include(`priority.m4')

define(`count', 0)define(`bestp', 0)define(`bestr', 0)
define(`Pos', `_$0(count, $@)')
define(`_Pos', `define(`p$1', `$2,$3,$4,$5')ifelse(eval($5>bestr), 1,
  `define(`bestp', $1)define(`bestr', $5)')define(`count', incr($1))')
translit((include(defn(`file'))), `p<'nl` =r>', `P()')
define(`diff', ``($1<$2)*($2- $1)+($2<$1)*($1- $2)'')
define(`prep', `_$0(p$1, $1, 'defn(`p'bestp)`)')
define(`_prep', `+eval(diff(`$1', `$6')+diff(`$2', `$7')+diff(`$3',
  `$8')`<=$9')store(eval(translit(``$1+$2+$3'', -)), `$4', `$5')')
define(`store', `insert(eval(`($1-$2)*($1>$2)'), $3)insert(eval(`$1+$2+1'))')
define(`part1', eval(forloop_arg(0, decr(count), `prep')))

define(`loop', `ifelse(`$4', `', `$3define(`max', $2)', `_$0(ifelse($5, `',
  `decr($1)', `incr($1)define(`i$1', defn(`i$1')`,$5,$4')'), $2, $3, $4)')')
define(`_loop', `loop($1, ifelse(eval($1>$2), 1, `$1, $4', `$2, $3'), pop)')
define(`part2', loop(0, 0, 0, pop))
# We rely on an assumption that the input files have bots aligned such that
# the highest overlap in Manhattan distance to origin without regard to
# direction will work. There are smaller counterexamples where this does
# not hold; try and detect those. If an actual input file fails, then the
# solution can be made more robust by checking which of the top 10 or so
# points starting from max downward has the most intersections, but it will
# add some runtime.  The most robust would be an O(n^2) check of intersections
# seen from each point, then maximize M for the set of M points with at least
# M intersections, picking the distance of the furthest of those M points,
# but as long as an O(n) filter gives the right answer for our input, use it!
define(`sanity', `ifelse(`$4', `', `', `define(`part2',
  `fatal(`more than one potential best overlap: 'max`$*')')')')
sanity(first(`i'decr(max)))

divert`'part1
part2
