#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <inttypes.h>
#include <assert.h>
#include <ctype.h>

bool __attribute__((format(printf, 1, 2)))
debug(const char *fmt, ...) {
  va_list ap;
  if (getenv("DEBUG")) {
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
    return true;
  }
  return false;
}

#define LIMIT 10

typedef enum Type {
		   RADIATION,
		   COLD,
		   FIRE,
		   BLUDGEONING,
		   SLASHING,
		   _MAX,
} Type;

typedef struct group group;
struct group {
  bool infect;
  int id;
  int units;
  int hp;
  int weak;
  int immune;
  int attack;
  Type type;
  int initiative;
  group *attacker;
  group *attacking;
};

static group immune[LIMIT];
static group infect[LIMIT];
static group *order[LIMIT * 2];

static int list(int round) {
  int i;
  int count1 = 0, count2 = 0;
  debug("\nBeginning round %d\n", round);
  debug("Immune:\n");
  for (i = 0; i < LIMIT; i++)
    if (immune[i].units > 0) {
      immune[i].attacker = NULL;
      immune[i].attacking = NULL;
      debug("Group %d(%d) contains %d units, effective power %d\n",
            immune[i].id, immune[i].initiative,
	    immune[i].units, immune[i].units * immune[i].attack);
      order[count1++] = &immune[i];
    }
  debug("Infect:\n");
  for (i = 0; i < LIMIT; i++)
    if (infect[i].units > 0) {
      infect[i].attacker = NULL;
      infect[i].attacking = NULL;
      debug("Group %d(%d) contains %d units, effective power %d\n",
            infect[i].id, infect[i].initiative,
	    infect[i].units, infect[i].units * infect[i].attack);
      order[count1 + count2++] = &infect[i];
    }
  return count1 && count2 ? count1 + count2 : 0;
}

static int sort_effective(const void *one, const void *two) {
  const group *a = *(const group**)one;
  const group *b = *(const group**)two;
  assert(a->units > 0);
  assert(b->units > 0);
  if (b->units * b->attack - a->units * a->attack)
    return b->units * b->attack - a->units * a->attack;
  return b->initiative - a->initiative;
}

static int sort_initiative(const void *one, const void *two) {
  const group *a = *(const group**)one;
  const group *b = *(const group**)two;
  assert(a->units > 0);
  assert(b->units > 0);
  return b->initiative - a->initiative;
}

static void __attribute__((noreturn)) die(void) {
  fprintf(stderr, "unexpected input\n");
  exit(1);
}

static const char *lookup(Type t) {
  switch (t) {
  case RADIATION: return "radiation";
  case COLD: return "cold";
  case FIRE: return "fire";
  case BLUDGEONING: return "bludgeoning";
  case SLASHING: return "slashing";
  default:
    abort();
  }
}

static Type decode(const char *s) {
  if (!strcmp(s, "radiation"))
    return RADIATION;
  if (!strcmp(s, "cold"))
    return COLD;
  if (!strcmp(s, "fire"))
    return FIRE;
  if (!strcmp(s, "bludgeoning"))
    return BLUDGEONING;
  if (!strcmp(s, "slashing"))
    return SLASHING;
  die();
}

static void parse(const char *line, group *g, int boost) {
  char *p, *q;
  char buf[20];

  if (sscanf(line, "%d units each with %d hit points", &g->units,
	     &g->hp) != 2)
    die();
  if ((p = strstr(line, "immune to"))) {
    p += strlen("immune to");
    do {
      while (*p == ',' || *p == ' ')
	p++;
      q = buf;
      while (isalpha(*p))
	*q++ = *p++;
      *q = '\0';
      g->immune |= 1 << decode(buf);
    } while (*p == ',');
  }
  if ((p = strstr(line, "weak to"))) {
    p += strlen("weak to");
    do {
      while (*p == ',' || *p == ' ')
	p++;
      q = buf;
      while (isalpha(*p))
	*q++ = *p++;
      *q = '\0';
      g->weak |= 1 << decode(buf);
    } while (*p == ',');
  }
  p = strstr(line, "with an attack");
  if (!p || sscanf(p, "with an attack that does %d %20[a-z] damage at "
		   "initiative %d\n", &g->attack, buf, &g->initiative) != 3)
    die();
  g->type = decode(buf);
  g->attack += boost;
  if (debug("%s group %d: %d units, %d hit points, attack %d %s, "
	    "initiative %d\n", g->infect ? "infect" : "immune", g->id,
	    g->units, g->hp, g->attack, lookup(g->type), g->initiative)) {
    for (int i = 0; i < _MAX; i++) {
      assert(!(g->weak & (1 << i)) || !(g->immune & (1 << i)));
      debug(" %s %s,", lookup(i), g->weak & (1 << i) ? "weak" :
	    g->immune & (1 << i) ? "immune" : "normal");
    }
    debug("\n");
  }
}

static void selection(int count) {
  int i, j;
  group *attacker;
  static group dummy;
  group *target;
  group *enemies;
  int best;
  int damage;

  debug("\n");
  for (i = 0; i < count; i++) {
    attacker = order[i];
    target = &dummy;
    best = 0;
    enemies = attacker->infect ? immune : infect;
    for (j = 0; j < LIMIT; j++) {
      if (enemies[j].units > 0 && !enemies[j].attacker
	  && !(enemies[j].immune & (1 << attacker->type))) {
	damage = attacker->attack * attacker->units;
	if (enemies[j].weak & (1 << attacker->type))
	  damage *= 2;
	debug("%s group %d(%d) would deal group %d(%d) %d damage\n",
	      attacker->infect ? "infect" : "immune", attacker->id,
              attacker->initiative, enemies[j].id, enemies[j].initiative,
              damage);
	if (damage > best ||
	    (damage == best && enemies[j].attack * enemies[j].units >
	     target->attack * target->units) ||
	    (damage == best && enemies[j].attack * enemies[j].units ==
	     target->attack * target->units
	     && enemies[j].initiative > target->initiative)) {
	  best = damage;
	  target = &enemies[j];
	}
      }
    }
    if (target != &dummy) {
      attacker->attacking = target;
      target->attacker = attacker;
    }
  }
}

static int attack(int count) {
  int i;
  int damage;
  int killed;
  group *attacker;
  int ret = 0;

  debug("\n");
  for (i = 0; i < count; i++) {
    attacker = order[i];
    if (!attacker->attacking || attacker->units <= 0)
      continue;
    damage = attacker->attack * attacker->units;
    if (attacker->attacking->weak & (1 << attacker->type))
      damage *= 2;
    ret += killed = damage / attacker->attacking->hp;
    debug("%s group %d(%d) attacks group %d(%d), killing %d of %d units\n",
	  attacker->infect ? "infect" : "immune", attacker->id,
          attacker->initiative, attacker->attacking->id,
          attacker->attacking->initiative, killed, attacker->attacking->units);
    attacker->attacking->units -= killed;
  }
  return ret;
}

int main(int argc, char **argv) {
  size_t len = 0, count = 0;
  char *line;
  int round = 0;
  int sum = 0;
  int i;
  int boost = 0;

  /* Part 1 - read data */
  if (argc > 2)
    boost = atoi(argv[2]);
  if (argc > 1)
    if (!(stdin = freopen(argv[1], "r", stdin))) {
      perror("failure");
      exit(2);
    }

  if (getline(&line, &len, stdin) < 0 || (strcmp(line, "Immune System:\n")))
    die();
  while (getline(&line, &len, stdin) > 0) {
    if (*line == '\n')
      break;
    if (count >= LIMIT) {
      fprintf(stderr, "recompile with larger LIMIT!\n");
      exit(1);
    }
    immune[count].id = count + 1;
    parse(line, &immune[count++], boost);
  }
  printf("Read %zu immune lines\n", count);
  count = 0;
  if (getline(&line, &len, stdin) < 0 || (strcmp(line, "Infection:\n")))
    die();
  while (getline(&line, &len, stdin) > 0) {
    if (count >= LIMIT) {
      fprintf(stderr, "recompile with larger LIMIT!\n");
      exit(1);
    }
    infect[count].infect = true;
    infect[count].id = count + 1;
    parse(line, &infect[count++], 0);
  }
  printf("Read %zu infect lines\n", count);

  /* Part 2 - battle */
  while ((count = list(round))) {
    round++;
    qsort(order, count, sizeof *order, sort_effective);
    selection(count);
    qsort(order, count, sizeof *order, sort_initiative);
    if (!attack(count)) {
      printf("Stalemate after %d rounds\n", round);
      exit(1);
    }
  }
  for (i = 0; i < LIMIT; i++) {
    if (immune[i].units > 0)
      sum -= immune[i].units;
    if (infect[i].units > 0)
      sum += infect[i].units;
  }
  printf("After %d rounds, %d %s units remain\n", round, abs(sum),
	 sum > 0 ? "infect" : "immune");
  return sum > 0;
}
