read line < ${1-/dev/null}
printf "Starting with %d characters\n" ${#line}
adj=$line
line+=aA
while [[ $line != $adj ]]; do
    line=$adj
    for l in {a..z}; do
	eval adj=\${adj//$l${l^}}
	eval adj=\${adj//${l^}$l}
    done
done
printf "Ended with %d characters\n" ${#line}
