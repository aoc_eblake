#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <assert.h>
#include <inttypes.h>

void debug(const char *fmt, ...) {
  va_list ap;
  if (getenv("DEBUG")) {
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
  }
}

typedef struct marble marble;
struct marble {
  int value;
  marble *l;
  marble *r;
};

static void dump1(marble *c) {
  debug("%d,%d,%d\n", c->l->value, c->value, c->r->value);
}

static void dump(int i, marble *c) {
  if (i > 25)
    return;
  debug(" Iteration %d:\n", i);
  dump1(c->l->l);
  dump1(c->l);
  dump1(c);
  dump1(c->r);
  dump1(c->r->r);
}

int main(int argc, char **argv) {
  int players = 9;
  int marbles = 25;
  int i;
  uint64_t best = 0;
  uint64_t *list;
  marble *current;
  marble *tmp;

  if (argc > 1)
    players = atoi(argv[1]);
  if (argc > 2)
    marbles = atoi(argv[2]);

  current = calloc(1, sizeof *current);
  current->l = current->r = current;
  list = calloc(players, sizeof *list);
  assert(list);

  dump(0, current);
  for (i = 1; i <= marbles; dump(i++, current)) {
    if (i % 23) {
      tmp = calloc(1, sizeof *tmp);
      assert(tmp);
      tmp->value = i;
      tmp->l = current->r;
      tmp->r = current->r->r;
      current = current->r->r = tmp->r->l = tmp;
    } else {
      tmp = current->l->l->l->l->l->l->l;
      current = tmp->l->r = tmp->r;
      tmp->r->l = tmp->l;
      list[i % players] += i + tmp->value;
      free(tmp);
    }
  }

  for (i = 0; i < players; i++)
    if (list[i] > best)
      best = list[i];
  printf("best: %" PRIu64 "\n", best);
  return 0;
}
