#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>

void debug(const char *fmt, ...) {
  va_list ap;
  if (getenv("DEBUG")) {
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
  }
}

typedef struct node node;
struct node {
  char n_children;
  char n_data;
  node *children;
  char *data;
  int value;
};
static node head;
static int count;
static int sum;

void process(FILE *f, node *n) {
  int i;
  count++;
  n->value = 0;
  if (fscanf(f, "%hhd %hhd ", &n->n_children, &n->n_data) != 2) {
    fprintf(stderr, "unexpected input\n");
    exit(1);
  }
  if (!(n->children = malloc(n->n_children * sizeof(node))) ||
      !(n->data = malloc(n->n_data))) {
    perror("malloc");
    exit(1);
  }
  for (i = 0; i < n->n_children; i++)
    process(f, &n->children[i]);
  for (i = 0; i < n->n_data; i++) {
    if (fscanf(f, "%hhd ", &n->data[i]) != 1) {
      fprintf(stderr, "unexpected input\n");
      exit(1);
    }
    sum += n->data[i];
    if (n->n_children) {
      if (n->data[i] - 1U < n->n_children)
	n->value += n->children[n->data[i] - 1].value;
    } else
      n->value += n->data[i];
  }
}

int main(int argc, char **argv) {
  if (argc > 1)
    if (!(stdin = freopen(argv[1], "r", stdin))) {
      perror("failure");
      exit(2);
    }

  process(stdin, &head);
  printf("read %d nodes, metadata sum %d\n", count, sum);
  printf("root value %d\n", head.value);
  return 0;
}
