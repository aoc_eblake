divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dhashsize=H] [-Dfile=day17.input] day17.m4

include(`common.m4')ifelse(common(17, 1000003), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`input', translit((include(defn(`file'))), nl`= ,()', `;..'))
define(`miny', `99999')define(`maxy', `0')
define(`part1', 0)define(`part2', 0)
define(`point', `define(`g$1_$2', 1)')
define(`visit', `_$0(translit(`$1', `.', `,'))')
define(`_visit', `forloop(`$4', `$6', `point('ifelse(`$1', `x',
  ``$2,',', `,`,$2'')`)')ifelse(`$1', `x', `ifelse(eval($6 > maxy), 1,
  `define(`maxy', `$6')')ifelse(eval($4 < miny), 1, `define(`miny', `$4')')')')
ifdef(`__gnu__', `
  patsubst(defn(`input'), `\([^;]*\);', `visit(`\1')')
', `
  define(`_chew', `visit(substr(`$1', 0, index(`$1', `;')))define(`tail',
    substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(`tail'), `;'), -1,
    `', `$0(defn(`tail'))')')
  define(`chew', `ifelse(eval(`$1 < 40'), 1, `_$0(`$2')', `$0(eval(`$1/2'),
    substr(`$2', 0, eval(`$1/2')))$0(eval(len(defn(`tail'))` + $1 - $1/2'),
    defn(`tail')substr(`$2', eval(`$1/2')))')')
  chew(len(defn(`input')), defn(`input'))
')
define(`bump', `define(`g$1_$2', 2)define(`part1', incr(part1))')
define(`flow', `ifelse(`$2', 'incr(maxy)`, `', `first(`$0'defn(
  `g$1_$2')`_'defn(`g$1_$3'))($@)')')
define(`flow_', `bump(`$1', `$2')flow(`$1', `$3', incr(`$3'))')
define(`flow2_1', `first(`spill'fill(decr(`$1'), `$2', `$3', `decr',
  `l')fill(incr(`$1'), `$2', `$3', `incr', `r'))(l, `$1', r, `$2', `$3')')
define(`flow2_3', defn(`flow2_1'))
define(`flow_1', `bump(`$1', `$2')flow2_1($@)')
define(`flow_2', `bump(`$1', `$2')')
define(`flow_3', defn(`flow_1'))
define(`fill', `first(`$0'defn(`g$1_$2')`_'defn(`g$1_$3'))($@)')
define(`fill2_1', `fill($4(`$1'), `$2', `$3', `$4', `$5')')
define(`fill2_3', defn(`fill2_1'))
define(`fill_1', `bump(`$1', `$2')fill2_1($@)')
define(`fill_3', defn(`fill_1'))
define(`fill1_', `define(`$5', `$1')1')
define(`fill1_1', defn(`fill1_'))
define(`fill1_2', defn(`fill1_'))
define(`fill1_3', defn(`fill1_'))
define(`fill_', `define(`$5', `$1')0')
define(`fill_2', defn(`fill_'))
define(`fill2_2', defn(`fill_'))
define(`spill00', `flow(`$1', `$4', `$5')flow(`$3', `$4', `$5')')
define(`spill01', `flow(`$1', `$4', `$5')')
define(`spill10', `flow(`$3', `$4', `$5')')
define(`spill11', `forloop(incr(`$1'), decr(`$3'), `define(`g'',
  ``_$4', `3')define(`part2', incr(part2))')flow(`$2', decr(`$4'), `$4')')
flow(500, miny, incr(miny))

divert`'part1
part2
