#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <inttypes.h>
#include <assert.h>
#include <endian.h>
#include <math.h>

void debug(const char *fmt, ...) {
  va_list ap;
  if (getenv("DEBUG")) {
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
  }
}

static int16_t regs[4];
static uint16_t possible[16];
static char known[16];

static int get(int r) {
  assert(0 <= r && r < sizeof regs / sizeof *regs);
  return regs[r];
}

static void set(int r, int v) {
  assert(0 <= r && r < sizeof regs / sizeof *regs);
  regs[r] = v;
  assert(regs[r] == v || !"resize regs to be larger type");
}

static const char *step(int op, int a, int b, int c) {
  switch (op) {
  case 0:
    set(c, get(a) + get(b)); return "addr";
  case 1:
    set(c, get(a) + b); return "addi";
  case 2:
    set(c, get(a) * get(b)); return "mulr";
  case 3:
    set(c, get(a) * b); return "muli";
  case 4:
    set(c, get(a) & get(b)); return "banr";
  case 5:
    set(c, get(a) & b); return "bani";
  case 6:
    set(c, get(a) | get(b)); return "borr";
  case 7:
    set(c, get(a) | b); return "bori";
  case 8:
    set(c, get(a)); return "setr";
  case 9:
    set(c, a); return "seti";
  case 10:
    set(c, a > get(b)); return "gtir";
  case 11:
    set(c, get(a) > b); return "gtri";
  case 12:
    set(c, get(a) > get(b)); return "gtrr";
  case 13:
    set(c, a == get(b)); return "eqir";
  case 14:
    set(c, get(a) == b); return "eqri";
  case 15:
    set(c, get(a) == get(b)); return "eqrr";
  default:
    return NULL;
  }
}

static int try(int b0, int b1, int b2, int b3, int op, int a, int b, int c,
	       int a0, int a1, int a2, int a3) {
  int r = 0;
  const char *str = NULL;
  debug("Want %d %d %d %d into %d %d %d %d given %d %d %d\n",
	b0, b1, b2, b3, a0, a1, a2, a3,	a, b, c);
  for (int i = 0; i < 16; i++) {
    regs[0] = b0;
    regs[1] = b1;
    regs[2] = b2;
    regs[3] = b3;
    const char *tmp = step(i, a, b, c);
    debug("op %2d converted to %d %d %d %d", i,
	  regs[0], regs[1], regs[2], regs[3]);
    if (regs[0] == a0 && regs[1] == a1 && regs[2] == a2 && regs[3] == a3) {
      debug(", matching %s\n", str = tmp);
      r++;
    } else {
      debug("\n");
      possible[op] &= ~(1 << i);
    }
  }
  debug("%d possible ops as decode\n", r);
  return r;
}

int main(int argc, char **argv) {
  size_t len = 0, count = 0;
  char *line;
  int ret = 0;
  int b0, b1, b2, b3;
  int a0, a1, a2, a3;
  int op, a, b, c;
  int i, j;
  bool reduced = false;

  /* Part 1 - read in samples */
  memset(possible, -1, sizeof possible);
  memset(known, -1, sizeof known);
  if (argc > 1 && !(stdin = freopen(argv[1], "r", stdin))) {
    perror("failure");
    exit(2);
  }

  while (1) {
    if (getline(&line, &len, stdin) <= 0)
      break;
    if (sscanf(line, "Before: [%1d, %1d, %1d, %1d]\n",
	       &b0, &b1, &b2, &b3) != 4)
      break;

    if (getline(&line, &len, stdin) <= 0)
      break;
    if (sscanf(line, "%d %d %d %d\n", &op, &a, &b, &c) != 4)
      break;

    if (getline(&line, &len, stdin) <= 0)
      break;
    if (sscanf(line, "After:  [%1d, %1d, %1d, %1d]\n",
	       &a0, &a1, &a2, &a3) != 4)
      break;

    if (getline(&line, &len, stdin) < 0)
      break;

    if (try(b0, b1, b2, b3, op, a, b, c, a0, a1, a2, a3) >= 3)
      ret++;
    count++;
  }
  printf("%zu stanzas read, found %d states with 3+ opcodes\n", count, ret);

  /* Part 2 - decode */
  for (i = 0; i < 16; i++)
    printf("op %2d could be %04x\n", i, possible[i]);
  printf("Reducing possible...\n");
  while (!reduced) {
    reduced = true;
    for (i = 0; i < 16; i++) {
      if (known[i] != -1 || (possible[i] & (possible[i] - 1)))
	continue;
      reduced = false;
      known[i] = log2(possible[i]);
      printf("op %d is %s\n", i, step(known[i], 0, 0, 0));
      for (j = 0; j < 16; j++)
	if (i != j)
	  possible[j] &= ~possible[i & 0xf];
    }
  }

  /* Part 3 - run program */
  memset(regs, 0, sizeof regs);
  count = 0;
  while (1) {
    if (getline(&line, &len, stdin) < 0)
      break;
    if (line[0] == '\n')
      continue;
    count++;
    if (sscanf(line, "%d %d %d %d\n", &op, &a, &b, &c) != 4)
      break;
    step(known[op], a, b, c);
  }
  printf("After %zu instructions, reg 0 contains %d\n", count, regs[0]);
  return 0;
}
