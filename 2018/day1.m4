divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day1.input] day1.m4

include(`common.m4')ifelse(common(1), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`input', include(defn(`file')))
define(`part1', eval(input))
ifelse(eval(part1 <= 0), 1, `errprintn(`corner case not handled')m4exit(1)')
define(`offset', `100000')define(`s', 0)define(`c', 0)
define(`line', `_$0(eval(s` + $1'), c)define(`c', incr(c))')
define(`_line', `define(`s', `$1')mark(eval(($1+'offset`)%'part1`), `$1', `$2')')
define(`mark', `ifdef(`l$1', `', `pushdef(`list', `$1')')pushdef(`l$1',
  ``$2',`$3'')')
ifdef(`__gnu__', `
  patsubst(translit(defn(`input'), nl, `;'), `\([^;]*\);', `line(`\1')')
',`
  define(`_chew', `line(substr(`$1', 0, index(`$1', `;')))define(`tail',
    substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(`tail'),
    `;'), -1, `', `$0(defn(`tail'))')')
  define(`chew', `ifelse(eval(`$1 < 15'), 1, `_$0(`$2')', `$0(eval(`$1/2'),
    substr(`$2', 0, eval(`$1/2')))$0(eval(len(defn(`tail'))` + $1 - $1/2'),
    defn(`tail')substr(`$2', eval(`$1/2')))')')
  chew(len(defn(`input')), translit(defn(`input'), nl, `;'))
')
define(`diff', 999999)define(`idx', c)
define(`pair', `ifelse(`$3', `', `', `_$0($@)$0(`$1', shift(shift($@)))')')
define(`_pair', `ifelse(`$3', `', `', `$1($2, $3)$0(`$1', `$2',
  shift(shift(shift($@))))')')
define(`use', `ifdef(`$1', `,defn(`$1')popdef(`$1')$0(`$1')')')
define(`do', `pair(`_$0'use(`l$1'))')
define(`_do', `$0_(ifelse(eval(`$1>$3'), 1, ``($1 - $3)',`$4',`$1'',
  ``($3 - $1)',`$2',`$3''), diff, idx)')
define(`_do_', `ifelse(eval(`$1<$4||($1==$4&&$2<$5)'), 1, `define(`diff',
  eval(`$1'))define(`idx', `$2')define(`part2', `$3')')')
define(`loop', `ifdef(`list', `do(list)popdef(`list')$0()')')
loop()

divert`'part1
part2
