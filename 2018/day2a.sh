two=0
three=0
while read line; do
    unset a
    declare -A a
    while [[ $line =~ (.)(.*) ]]; do
	letter=${BASH_REMATCH[1]}
	: $((a[$letter]++))
	line=${BASH_REMATCH[2]}
    done
    have2=0 have3=0
    for letter in {a..z}; do
	if test "${a[$letter]}" = 2; then
	    have2=1
	elif test "${a[$letter]}" = 3; then
	    have3=1
	fi
    done
    : $(( two += have2 )) $(( three += have3 ))
done
echo checksum: $(( two * three ))
