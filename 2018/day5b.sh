read line < ${1-/dev/tty}
echo "Starting with ${#line} characters"
orig=$line
best_len=${#line}
for outer in {a..z}; do
    adj=${orig//[$outer${outer^}]/}
    line=$orig
    while [[ $line != $adj ]]; do
	line=$adj
	for l in {a..z}; do
	    eval adj=\${adj//$l${l^}}
	    eval adj=\${adj//${l^}$l}
	done
    done
    echo "Letter $outer gives ${#line} characters"
    if [[ ${#line} -lt $best_len ]]; then
	best_len=${#line}
	best_letter=$outer
    fi
done
echo "Best letter $best_letter gives $best_len"
