divert(-1)dnl -*- m4 -*-
# Build arbitrary-length math on top of 32-bit
# assumes common.m4 is already loaded
define(`chunk', `_$0($1, eval(len($1) - $2), substr(00000000, 0, $2))')
define(`_chunk', `ifelse(eval($2 > 0), 1, `eval(-1$3 + 1substr($1, $2)), $0(
  substr($1, 0, $2), eval($2 - len($3)), $3)', $1)')
define(`rebuild', `trim(_$0($@))')
define(`_rebuild', `ifelse(`$#', 2, `$2', `$0($1, shift(shift($@)))eval($2,
  `', $1)')')
define(`trim', `_$0($1, index(translit($1, 123456789, .........), .))')
define(`_trim', `ifelse($2, -1, 0, `substr($1, $2)')')
define(`split', `ifelse(index($1, -), -1, ``p', $1', ``n', substr($1, 1)')')
define(`perform', `_$0(`$1', split($2), split($3))')
define(`_perform', `$1$2$4($3, $5)')

define(`neg64', `ifelse($1, 0, 0, index($1, -), 0, `substr($1, 1)', -$1)')
define(`add64', `ifelse(eval(len($1) < 10 && len($2) < 10), 1, `eval($1 + $2)',
  `perform(`_$0', $@)')')
define(`sub64', `add64($1, neg64($2))')
define(`_add64', `ifelse($1, 0, $2, $2, 0, $1, `rebuild(8a1(0, (chunk($1, 8)),
  chunk($2, 8)))')')
define(`_a1', `, ifelse(len($1), 9, `eval($1 - 100000000)', $1)a1(eval($1 >=
  100000000),')
define(`a1', `ifelse($1$2$3, 0(), `', `_$0(eval(first$2 + $3 + $1)) (shift$2),
  shift(shift(shift($@))))')')
define(`_sub64', `ifelse($2, 0, $1, $1, $2, 0, `rebuild(8`'s1(0, (chunk($1, 8)),
  chunk($2, 8)))')')
define(`_s1', `, ifelse(eval($1 < 0), 1, `eval($1 + 100000000)s1(-1,',
  `$1`'s1(0,')')
define(`s1', `ifelse($1$2$3, 0(), `', `_$0(eval(first$2 - $3 + 0 +
  $1)) (shift$2), shift(shift(shift($@))))')')
define(`_add64pp', defn(`_add64'))
define(`_add64pn', `ifelse(_lt64pp($1, $2), 1, `-_sub64($2, $1)',
  `_sub64($1, $2)')')
define(`_add64np', `_add64pn($2, $1)')
define(`_add64nn', `-_add64($1, $2)')
define(`add', `ifelse($#, 1, $1, $#, 2, `add64($1, $2)',
  `add(add64($1, $2), shift(shift($@)))')')
define(`mul64', `ifelse(eval(len($1$2) < 10), 1, `eval($1 * $2)',
  `perform(`_$0', $@)')')
define(`_mul64', `ifelse($1, 0, 0, $1, 1, $2, $2, 0, 0, $1, -1, `neg64($2)',
  $2, 1, $1, $2, -1, `neg64($1)', `add(m1($2, `', chunk($1, 4)))')')
define(`m1', `mul64($1, $3)$2ifelse($#, 3, `', `, m1($1, $2`'0000,
  shift(shift(shift($@))))')')
define(`mul', `ifelse($#, 1, $1, $#, 2, `mul64($1, $2)',
  `mul(mul64($1, $2), shift(shift($@)))')')
define(`_mul64pp', defn(`_mul64'))
define(`_mul64pn', `-'defn(`_mul64'))
define(`_mul64np', `-'defn(`_mul64'))
define(`_mul64nn', defn(`_mul64'))
define(`lt64', `ifelse($1, $2, 0, `ifelse(eval(len($1) < 10 && len($2) < 10),
  1, `eval($1 < $2)', `perform(`_lt64', $@)')')')
define(`_lt64', `ifelse($3, $4, `ifelse(eval($5 || $3 < 10), 1,
  `eval($1 < $2)', `$0(1substr($1, 9), 1substr($2, 9), substr($1, 0, 9),
  substr($2, 0, 9), eval(len($1) < 17))')', `eval($3 < $4)')')
define(`_lt64pp', `_lt64($@, len($1), len($2), 0)')
define(`_lt64pn', 0)
define(`_lt64np', 1)
define(`_lt64nn', `_lt64pp($2, $1)')

define(`no64', `pushdef(`add64', `eval($'`1 + $'`2)')pushdef(`sub64',
  `eval($'`1 - $'`2)')pushdef(`mul64', `eval($'`1 * $'`2)')pushdef(`lt64',
  `eval($'`1 < $'`2)')')
