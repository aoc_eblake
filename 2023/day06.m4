divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day06.input] day06.m4

include(`common.m4')ifelse(common(06), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

# TODO: Idea for optimization: implement arbitrary-width division and
# square root, then directly land on the result by solving the
# quadratic equation, rather than binary searching for an answer.

include(`math64.m4')
define(`input', translit(include(defn(`file')), Tsemantic))
define(`half', `_$0(mul(5, $1))')define(`_half', `substr($1, 0, decr(len($1)))')
define(`_search', `ifelse(, $@, lt64($2, mul64($4, eval($1 - $4))), 1,
  `search($1, $2, $3, $4)', `search($1, $2, $4, $5)')')
define(`search', `_$0($1, $2, $3, half(add($3, $4)), $4)')
define(`count', `eval($1-search($1, $2, 0, half($1))*2+1)')
define(`zip', `ifelse($1$2, ()(), , first$1,, `zip((shift$1),$2)', first$2,,
  `zip($1,(shift$2))', `,count(first$1, first$2)zip((shift$1),(shift$2))')')
define(`part1', mul(1zip(translit(input, :D nl, (,,)))))
define(`part2', count(translit(input, nl :D, `,')))

divert`'part1
part2
