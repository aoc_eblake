divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day23.input] day23.m4
# Optionally use -Dverbose=1 to see some progress
# Optionally use -Dalgo=[dfs|dp] for path search algorithm

include(`common.m4')ifelse(common(23), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

ifdef(`algo', `', `define(`algo', `dp')')

# Macros used as variables during parsing:
# x, y: int, coordinate while parsing the grid, 0-based
# maxx: width of grid
# gX_Y: int, contents of grid at x,y (undefined if wall or already visited)
# n: int, number of junction nodes in graph
# nX_Y: int, node index at x,y
# nN: int pair, x,y coordinate of node n
# cN: int, count of neighbors of node n

define(`input', translit(include(defn(`file')), `#.>v'nl, `01234'))
define(`x', 0)define(`y', 0)define(`n', 1)
define(`check', `ifdef(`$1', `define(`c'$1, incr(defn(`c'$1)))1')')
define(`do0', `define(`x', incr($1))')
define(`do1', `define(`g$1_$2')do0($@)')
define(`do2', `ifelse(check(`n'decr($1)`_$2'), 1, `', check(`n'incr($1)`_$2'),
  1, `', `define(`n'incr($1)`_$2', n)define(`c'n, 1)define(`n'n,
  incr($1)`,$2')define(`n', incr(n))')do1($@)')
define(`do3', `ifelse(check(`n$1_'decr($2)), 1, `', `define(`n$1_'incr($2),
  n)define(`c'n, 1)define(`n'n, `$1,'incr($2))define(`n', incr(n))')do1($@)')
define(`do4', `define(`maxx', $1)define(`x', 0)define(`y', incr($2))')

ifdef(`__gnu__', `
  patsubst(defn(`input'), `.', `do\&(x, y)')
',`
  define(`chew', `ifelse($1, 1, `do$2(x, y)', `$0(eval($1/2), substr(
    `$2', 0, eval($1/2)))$0(eval($1-$1/2), substr(`$2', eval($1/2)))')')
  chew(len(defn(`input')), defn(`input'))
')

# Macros used as variables during compression:
# Ep_n: int list, neighbors to visit for part p from node n; for dfs
# en_m: int, weight of edge from node n to m
# GRC: int, node at r,c; for dp
# Nn: int pair, r,c coordinates of node; for dp
# base: cost out of node 0; for dp
# dnRC: down cost of node at r, c, possibly empty; for dp
# rtRC: right cost of node at r, c, possibly empty; for dp

# Assumption: all decision points are entered from top or left, and
# exit to bottom or right. Reduce the graph to just the distances between
# nodes of interest, for a much smaller problem to solve.  In part 2, a
# naive brute force DFS with no constraints on edges visits over 102 million
# edges, but given that we are a planar graph and cannot visit a node twice,
# any node with fewer than 4 neighbors is on a perimeter; edges between two
# perimeter nodes should always be directional to avoid cutting off a path
# to the end node. This change alone reduces work to 14.7 million edge visits.
#
# When turning nodes into a graph, it matters both how we leave the from
# node, and how we enter the to node. Referencing the example grid, node 1
# exits to the right to reach node 3 from the top, and exits down to reach
# node 5 from the top; in turn node 5 exits right to reach node 3 from the
# left.  This means we want node 1 to be G01, node 5 to be G11, and node 3
# to be G12 (and not G02).  For the actual input, this leaves G05 and G50
# unoccupied.  Official inputs always enter G00 from the top, and exit G55
# to the bottom, and the dp solution depends on this; but it is not much
# harder to support entering G00 from the left or exiting G55 to the right.

ifdef(`g1_0', `', `fatal(`failed assumption about start point')')
ifdef(`g'decr(decr(maxx))`_'decr(y), `',
  `fatal(`failed assumption about end point')')
define(`n0', `1,0')define(`c0', 1)
define(`n'n, decr(decr(maxx))`,'decr(y))define(`c'n, 1)
define(`n'decr(decr(maxx))`_'decr(y), n)
# next(from_r, from_c, out_r, out_c, in_r, in_c)
define(`next', `eval($1+($3|$5)), eval($2+($4|$6))')
# pair(from_n, to_n, dist, to_x, to_y, to_r, to_c)
define(`pair', `ifelse($1, $2, `', `define(`E1_$1', defn(`E1_$1')`,$2')define(
  `E2_$1', defn(`E2_$1')`,$2')ifelse(eval(c$1==4 || c$2==4), 1, `define(`E2_$2',
  defn(`E2_$2')`,$1')')define(`e$1_$2',  $3)define(`e$2_$1', $3)ifdef(`G$6$7',
  `', `define(`G$6$7', $2)define(`N$2', `$6,$7')define(`rt$6$7',
  _visit(incr($4), $5, $2, 1, 0, 1))define(`dn$6$7', _visit($4, incr($5), $2,
  1, 1, 0))')$3')')
# visit(x, y, from_n, dist, out_r, out_c, in_r, in_c)
define(`_visit', `ifdef(`g$1_$2', `popdef(`g$1_$2')visit(incr($1), $2, $3,
  incr($4), $5, $6, 0, 1)visit($1, incr($2), $3, incr($4), $5, $6, 1, 0)visit(
  decr($1), $2, $3, incr($4), $5, $6, 0, 0)visit($1, decr($2), $3, incr($4),
  $5, $6, 0, 0)')')
define(`visit', `ifdef(`n$1_$2', `pair($3, n$1_$2, $4, $1, $2, next(N$3, $5,
  $6, $7, $8))', `_$0($@)')')
output(1, `...compressing graph')
define(`N0', `-1, -1')
define(`base', _visit(1, 1, 0, 1, 1, 1))
define(`_prep', `$1define(`dn$1$1', rt$1$1)popdef(`rt$1$1')')
define(`prep', `define(`dim', ifelse($1, $2, `_$0(decr($1))', $2))')
prep(first(`N'n))

ifelse(defn(`algo'), `dfs', `
output(1, `...using dfs search')
# Heuristic: best possible path remaining
# b, B: int, starting best for part 1, 2
# bN, BN: int, best edge out of node n for part 1, 2
# tN, TN: code, what to do when visiting node n during part 1, 2

define(`b', 0)define(`B', 0)
define(`_best', `ifelse($3, `', $2, `$0($1, ifelse(eval(e$1_$3>$2), 1, e$1_$3,
  $2), shift(shift(shift($@))))')')
define(`best', `define(`b$1', _$0($1, 0E1_$1))define(`b', eval(b+b$1))define(
  `B$1', _$0($1, 0E2_$1))define(`B', eval(B+B$1))')
forloop_arg(0, decr(n), `best')

# Create tN(dist, best) and TN(dist, best) for each node N
define(`_travel_', ``$2$4(eval($''1+e$1_$4``), eval($''2-$3$1``))'')
define(`_travel', `define(`$2$1', ifelse(eval(verbose > 0), 1,
  ``show(p)'')`ifelse(eval($'`1+$'`2>=part$3), 1, 'dquote(
  `pushdef(`$2$1')'_foreach(`$0_($1, `$2', `$4', ', `)',
  shift(shift(shift($@))))`popdef(`$2$1')')`)')')
define(`travel', `_$0($1, `t', 1, `b'E1_$1)_$0($1, `T', 2, `B'E2_$1)')
forloop_arg(0, decr(n), `travel')
define(`t'n, `ifelse(eval($1 > part1), 1, `define(`part1', $1)')')
define(`T'n, `ifelse(eval($1 > part2), 1, `define(`part2', $1)')')

define(`p', 0)
define(`rename', `define(`$2', defn(`$1'))popdef(`$1')')
define(`show0', `output(1, `...$1')rename(`show$1', `show'eval($1+200000))')
define(`show', `ifdef(`$0$1', `$0$1($1)')define(`p', incr($1))')
ifelse(eval(verbose > 0), 1, `define(`_travel', `show(p)'defn(`_travel'))')

define(`part1', 0)t0(0, b)
define(`part2', 0)T0(0, B)

', defn(`algo'), `dp', `
output(1, `...using dynamic programming search')
# Dynamic programming solution based heavily on this design:
# https://www.reddit.com/r/adventofcode/comments/18ysozp/comment/kgdcc0p
# The idea is that because we have a planar graph, Courcelles theorem says
# that max path can be computed with O(fn(k)) time and storage per row where
# k is the maximum tree-width 3 in the example, 6 in the actual input,
# making the overall computation a linear algorithm in the number of rows,
# rather than DFS exponential in the number of nodes. The overhead per row
# is constant but somewhat high (for k=6, fn(k) can produce up to 76
# combinations of 0 or more valid nested pairs of outgoing edges), where
# the real benefits would be more visible if there were even more rows, but
# the effect is noticeable even for our 6x6 graph.
#
# For simplicity, assume the graph is square other than the root and goal
# node (both the example and my input meets this criteria).
# Additional variables used here:
# dim: int, max tree-width of the graph - 1; also internal rows per assumption
# upC: int, part 1 max weight from incoming path above column c in given row
# left: int, part 1 max weight of incoming path from left
# dp: key stack, part 2 list of keys for next row
# dpKEY: int, part 2 max cost seen for reaching the given KEY
# do: work stack, part 2 keys to search for a given row

# The part 1 DP is simple because the graph is a DAG: the state at each node
# is the maximum of the two possible paths into the node.  This can be tracked
# in just c+1 (dim+2) variables.

ifelse(eval(dim > 5), 1, `fatal(`unexpected grid shape: 'defn(`N'n))')
forloop(0, dim, `define(`up'', `, 0)')
define(`set', `define(`up$2', eval(defn(`dn$1$2')+$3))define(`left',
  eval(defn(`rt$1$2')+$3))')
define(`max', `ifelse(eval($1>$2), 1, $1, $2)')
define(`_dorow', `set($1, $2, max(left, up$2))')
define(`dorow', `define(`left', 0)forloop(0, 'dim`, `_$0($1, ', `)')')
forloop_arg(0, dim, `dorow')
define(`part1', eval(base + defn(`up'dim)))

# The state for part 2 is a bit more complex, but boils down to
# tracking well-nested pairs of downward lines out of a row, and where
# there are at most two possible arrangements for the next row given
# any two characters of the state representing the current row and
# column.  Characters for the current row can be L, _, or R, where LR
# pair together via an earlier or later row, and where _ represents
# in-row traversal or an ignored node.  The output for each row is the
# list of possible configurations of downward edges, and the max score
# possible in that configuration.  When used as a macro name, the keys
# are concatenated into a string; but for deciding what to do for a
# transition, it is easier to represent keys as a quoted list of
# characters, with keyN(keys...) and set(N, value, keys...) as
# accessors.

define(`edge', forloop(1, dim, `shift(', `)`_''))
define(`list', `translit(```A',`B',`C',`D',`E',`F',`G''', `ABCDEFG', `$1')')
define(`prep', `define(`key$1', dquote(`$'incr($1)))')
forloop_arg(0, 6, `prep')
define(`set', `set$1(shift($@))')
define(`set0', ```$1',`$3',`$4',`$5',`$6',`$7',`$8''')
define(`set1', ```$2',`$1',`$4',`$5',`$6',`$7',`$8''')
define(`set2', ```$2',`$3',`$1',`$5',`$6',`$7',`$8''')
define(`set3', ```$2',`$3',`$4',`$1',`$6',`$7',`$8''')
define(`set4', ```$2',`$3',`$4',`$5',`$1',`$7',`$8''')
define(`set5', ```$2',`$3',`$4',`$5',`$6',`$1',`$8''')
define(`set6', ```$2',`$3',`$4',`$5',`$6',`$7',`$1''')
# find(`keys', step, col, nest)
define(`_find', `$2, ifelse(key$2($1), `L', `incr($3)', key$2($1), `R',
  `decr($3)', $3)')
define(`find', `ifelse($4, 0, $3, `$0(`$1', `$2', _$0(`$1', $2($3), $4))')')
define(`findR', `find(`$1', `incr', $2, 1)')
define(`findL', `find(`$1', `decr', $2, -1)')

# addstate(cost, keys...) set or update dp[keys]
define(`addstate', `ifdef(`dp$2$3$4$5$6$7$8', `ifelse(eval($1>dp$2$3$4$5$6$7$8),
  1, `define(`dp$2$3$4$5$6$7$8', $1)')', `define(`dp$2$3$4$5$6$7$8',
  $1)pushdef(`dp', ``$2$3$4$5$6$7$8'')')')

# nextcol(`keys', cost, row, col, down, right)
define(`nextcol', `transition(set($4, $5, $1), eval(ifelse(`$5', `_', `',
  `defn(`dn$3$4') + ')ifelse(`$6', `_', `', `defn(`rt$3$4') + ')$2), $3,
  incr($4), `$6')')

# transition(`keys', cost, row, col, left)
define(`trans_L', `nextcol(`$1', $2, $3, $4, `$6', `$5')nextcol(`$1', $2, $3,
  $4, `$5', `$6')')
define(`trans_R', defn(`trans_L'))
define(`transL_', defn(`trans_L'))
define(`transR_', defn(`trans_L'))
define(`trans__', `nextcol(`$1', $2, $3, $4, `L', `R')nextcol(`$1', $2, $3, $4,
  `_', `_')')
define(`transLL', `nextcol(set(findR(`$1', $4), `L', $1), $2, $3, $4, `_', `_')')
define(`transRR', `nextcol(set(findL(`$1', $4), `R', $1), $2, $3, $4, `_', `_')')
define(`transRL', `nextcol(`$1', $2, $3, $4, `_', `_')')
define(`_transition', `trans$5$6($@)')
define(`transition', `ifelse($4, 'incr(dim)`, `ifelse(`$5', `_', `addstate($2,
  $1)')', `_$0(`$1', $2, $3, $4, `$5', key$4($1))')')

define(`_dorow', `ifdef(`do', `do`'popdef(`do')$0($1)')')
define(`dorow', `ifdef(`dp', `pushdef(`do', `transition(list('defn(`dp')`),
  'defn(`dp'dp)`, $1, 0, `_')')popdef(`dp'dp)popdef(`dp')$0($1)', `_$0($1)')')
addstate(base, `L', edge, `R')
forloop_arg(0, dim, `dorow')
define(`part2', defn(`dp'edge`LR'))

', `fatal(`unrecognized algo 'defn(`algo'))')

divert`'part1
part2
