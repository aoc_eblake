divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day09.input] day09.m4

include(`common.m4')ifelse(common(09), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

# Observation from the megathread: all rows of the input file have the
# same number of elements.  Instead of performing O(n^2) merge() on each
# row (actually O(n^3) because of m4's overhead in reparsing arguments
# each iteration), we can perform a single merge on the O(n) summation of
# all input rows, and still get the same result.

define(`input', translit(include(defn(`file')), nl` ', `;.'))
define(`_do', `define(`n$1', eval(defn(`n$1')+$2))define(`n', incr($1))')
define(`do', `define(`n', 0)_foreach(`_$0(defn(`n'), ', `)', translit(`.$1',
  `.', `,'))')

ifdef(`__gnu__', `
  patsubst(defn(`input'), `\([^;]*\);', `do(`\1')')
', `
  define(`_chew', `do(substr(`$1', 0, index(`$1', `;')))define(
    `tail', substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(`tail'),
    `;'), -1, `', `$0(defn(`tail'))')')
  define(`chew', `ifelse(eval($1 < 240), 1, `_$0(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  chew(len(defn(`input')), defn(`input'))
')

# This approach uses an O(n^2) recursion to compute both answers using only
# differences.  The megathread mentions other approaches using Lagrange's
# interpolation using binomial coefficients for a different amount of work
# (computing comb(n,k) will still require recursion, and 20! doesn't fit in
# 32 bits, so it may not be worth trying)

define(`_merge', `eval($1 - $2), eval($3 + $4)')
define(`merge', `ifelse(`$1$4', `', `$2,$3', `$1', `', `$0(`$2',
  eval($3 - $2)`,', shift(shift($@)))', `$4', `', `_$0($1, $0(`', $2), $3)',
  `$0(`$1', `$2'eval($4 - $3)`,', shift(shift(shift($@))))')')
define(`run', `define(`part2', `$1')define(`part1', `$2')')
run(merge(forloop(0, decr(n), `,defn(`n'', `)')))

divert`'part1
part2
