all: day10.csv
	libreoffice --calc day10.csv
	rm day10.csv
day10.csv: day10.m4 common.m4
	@test -f "$(file)" || { echo 'specify file=input first'; exit 1; }
	m4 "-Dfile=$(file)" -Dcsv=1 day10.m4 > day10.csv
