divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day01.input] day01.m4

include(`common.m4')ifelse(common(01), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`do', ` +substr(`$1', 0, 1)`'substr(`$1', decr(len(`$1')))')
define(`clean', `translit(map(map(map(map(map(map(map(map(map(``$1'', `ONE',
  `O1E'), `TWO', `T2O'), `THREE', `T3E'), `FOUR', `4'), `FIVE', `5E'), `SIX',
  `6'), `SEVEN', `7N'), `EIGHT', `E8T'), `NINE', `N9E'), defn(`ALPHA'))')
define(`input', translit(include(defn(`file')), defn(`alpha')nl,
  defn(`ALPHA')`;'))
ifdef(`__gnu__', `
  define(`map', `patsubst(``$1'', `$2', `$3')')
  define(`do2', defn(`do'))
  define(`prep', defn(`clean'))
  define(`eat', `patsubst(`$1', `\([^;]*\);', `do(`\1')')')
', `
  define(`_map', `ifelse(`$1', `-1', ``$2'', `map(substr(`$2', 0,
    `$1')`$4'substr(`$2', incr($1)), `$3', `$4')')')
  define(`map', `_$0(index($1, $2), $*)')
  define(`do1', defn(`do'))
  define(`do2', `do1(clean(`$1'))')
  define(`prep', ``$1'')
  define(`_chew', `do(substr(`$1', 0, index(`$1', `;')))define(
    `tail', substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(`tail'),
    `;'), -1, `', `$0(defn(`tail'))')')
  define(`chew', `ifelse(eval($1 < 110), 1, `_$0(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  define(`eat', `chew(len(`$1'), `$1')')
')
define(`part1', eval(eat(translit(defn(`input'), defn(`ALPHA')))))
define(`do', defn(`do2'))
define(`part2', eval(eat(prep(defn(`input')))))

divert`'part1
part2
