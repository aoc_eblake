divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day20.input] day20.m4
# Optionally use -Dverbose=1 to see some progress

include(`common.m4')ifelse(common(20), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

# Other than example and broadcaster, names are two lower letters; avoid
# collisions with the nl macro.
define(`Nl', defn(`nl'))undefine(`nl')
define(`errprintn', `errprint(`$1'Nl())')
define(`output', `ifelse(`$#', 0, ``$0'', 'dquote(defn(`output'))`)')

define(`input', translit((include(defn(`file'))), Nl`,> ()-', `;..'))
define(`bump', `define(`$1', incr($1))')
define(`q_h', 0)define(`q_t', 0)
define(`sent0', 0)define(`sent1', 0)
define(`_send', `bump(`sent$2')define(`q_'q_t,
  `define(`$1_$3', $2)$3_($2)')bump(`q_t')')
define(`send', `_foreach(`_$0(`$1', $2, ', `)', o_$1)')
define(`_broad', `bump(`sent0')send(`brd', 0)')
define(`_flip', `define(`s_$1', $2)send(`$1', $2)')
define(`flip', `ifelse($1, 0, `_$0(`$2', eval(!s_$2))')')
define(`_conj', `send(`$1', $2)ifelse($2.f_$1, 1.0, `define(`f_$1', cycle)')')
define(`conj', `_$0(`$2', eval(!(1_foreach(`first(`*'', `_$2)', i_$2))))')

define(`_wire', `define(`o_$1', defn(`o_$1')`,`$2'')define(`i_$2',
  defn(`i_$2')`,`$1'')define(`$1_$2', 0)ifdef(`$2_', `', `define(`$2_')')')
define(`wire', `_foreach(`_$0(`$1', ', `)', $@)')
define(`_do', `ifelse(`$1', `b', `wire(`brd', shift(shift($@)))', `$1', `%',
  `define(`$2_', `flip('$`1, `$2')')wire(shift($@))define(`s_$2', 0)', `$1',
  `&', `define(`$2_', `conj('$`1, `$2')')define(`f_$2', 0)wire(shift($@))')')
define(`do', `_$0(substr(`$1', 0, 1), translit(`$1', `.%&', `,'))')

ifdef(`__gnu__', `
  patsubst(defn(`input'), `\([^;]*\);', `do(`\1')')
', `
  define(`_chew', `do(substr(`$1', 0, index(`$1', `;')))define(
    `tail', substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(`tail'),
    `;'), -1, `', `$0(defn(`tail'))')')
  define(`chew', `ifelse(eval($1 < 60), 1, `_$0(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  chew(len(defn(`input')), defn(`input'))
')

include(`math64.m4')
define(`_round', `ifelse(q_h, q_t, `', `first(`q_'q_h`()')popdef(`q_'q_h)bump(
  `q_h')$0()')')
define(`round', `ifelse(eval($1%500), 0, `output(1, ...$1)')define(`cycle',
  $1)_broad()_$0()')
forloop_arg(1, 1000, `round')
define(`part1', mul64(sent0, sent1))

# Assumption for part 2: rx is fed by a single conjunction, which in turn is
# fed only by other conjunctions that each have a prime period.  The LCM is
# thus the first time all feeders will cycle high at the same time.
ifdef(`i_rx', `
define(`grab', ``,f_$1'')
define(`part2', `mul(1'foreach(`grab'first(`i_'shift(i_rx)))`)')
define(`finish', `round($1)ifelse(part2, 0, `$0(incr($1))')')
finish(1001)
', `define(`part2', ``no rx output'')')

divert`'part1
part2
