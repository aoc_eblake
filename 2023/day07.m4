divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day07.input] day07.m4
# Optionally use -Dverbose=1 to see some progress

include(`common.m4')ifelse(common(07), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

# Careful not to collide with hex digits.  eval(12,16) outputs unquoted c.
define(`card', 0)
define(`input', translit(include(defn(`file')), nl` ', `;.'))

# https://www.reddit.com/r/adventofcode/comments/18cnzbm/comment/kcc4azi/
# Summing the histograms of each letter, and accounting for J in part 2,
# is sufficient to come up with a 2-digit number (25, 17, 13, 11, 9, 7, 5)
# that correctly ranks all possible hands.  One step further and we get
# a 1-digit hex number (a, 6, 4, 3, 2, 1, 0), for smaller integers.
define(`histo', `len(translit(`$1', `$2$1', `$2'))')
define(`best', `ifelse($1, 5, 5, $1, 4, 4, $2, 4, 4, $1, 3, 3, $2, 3, 3,
  $3, 3, 3, $1, 2, 2, $2, 2, 2, $3, 2, 2, $4, 2, 2, $1)')
define(`_hand', `eval(`($1+$2+$3+$4+$5 + $6*$6+$6*2*'best($@)`) / 2 - 2', 16)')
define(`hand', `_$0(translit(`histo(`ABCDE', `A'), histo(`ABCDE', `B'),
  histo(`ABCDE', `C'), histo(`ABCDE', `D'), histo(`ABCDE', `E')', `ABCDE',
  `$1'), ($2+0))')
define(`Hand', `hand(translit(`$1', `J'), histo(`$1', `J'))')
define(`_do', `define(`s$1', eval(`0x'hand(`$2')`'translit(`$2', `AKQJT',
  `edcba')))define(`S$1', eval(`0x'Hand(`$2')`'translit(`$2', `AKQJT',
  `edc1a')))')
define(`do', `translit(`_$0($1, `ABCDE')define(`bid$1', `GHIJ')', `ABCDEFGHIJ',
  `$2')define(`card', `$1')')

ifdef(`__gnu__', `
  define(`eat', `patsubst(`$1', `\([^;]*\);', `do(incr(card), `\1')')')
', `
  define(`_chew', `do(incr(card), substr(`$1', 0, index(`$1', `;')))define(
    `tail', substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(`tail'),
    `;'), -1, `', `$0(defn(`tail'))')')
  define(`chew', `ifelse(eval($1 < 20), 1, `_$0(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  define(`eat', `chew(len(`$1'), `$1')')
')
eat(defn(`input'))

# Writing sort in m4 is a chore,
# And I know that from my days of yore,
# But I remembered last year
# Writing A* with cheer
# So crib my min-heap for the score!
define(`priority', 5)
include(`priority.m4')

define(`build', `insert(s$1, $1)')
forloop_arg(1, card, `build')
define(`value', `+$1*defn(`bid'shift(pop))')
define(`part1', eval(forloop_arg(1, card, `value')))

define(`build', `insert(S$1, $1)')
forloop_arg(1, card, `build')
define(`part2', eval(forloop_arg(1, card, `value')))

divert`'part1
part2
