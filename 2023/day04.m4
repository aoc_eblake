divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day04.input] day04.m4

include(`common.m4')ifelse(common(04), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`do', `ifelse($1, |, , `+(len($1)*index(`$*,', `,$1,')>0)$0(
  shift($@))')')
define(`bump', `define(`$1', eval(defn(`$1')+$2))')
define(`_C', `bump(`part1', `(1<<$1)/2')bump(`part2', `$3')forloop($2,
  eval($1+$2), `bump(`S'', `, $3)')')
define(`C', `ifelse($1,,`C(shift($@))',`_C(eval(do(shift($@))),$1,incr(
  0defn(`S$1')))')')
translit(include(defn(`file')), `a 'nl`:rd', `(,)')

divert`'part1
part2
