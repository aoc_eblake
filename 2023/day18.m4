divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day18.input] day18.m4

include(`common.m4')ifelse(common(18), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

changecom
define(`input', translit(include(defn(`file')), nl`RDLUabcdef ()#',
  `;0123ABCDEF.'))
changecom(`#', nl)

include(`math64.m4')

# https://en.wikipedia.org/wiki/Shoelace_formula is close for twice area;
# but we are digging 1x1 squares, not points. There is length/2 additional
# area oustside each line, plus 1 for all corners combined (a closed
# non-intersecting loop necessarily has 4 more convex than concave corners,
# since the sum of exterior angles of a polygon is always 360 degrees).
define(`area1', 0)define(`perim1', 0)define(`x1', 0)define(`y1', 0)
define(`area2', 0)define(`perim2', 0)define(`x2', 0)define(`y2', 0)
define(`visit', `define(`area$1', add64(area$1, sub64(mul64($2, $5),
  mul64($3, $4))))define(`perim$1', eval(perim$1+$6))define(`x$1',
  $4)define(`y$1', $5)')
define(`do0', `visit($1, $2, $3, eval(`$2 + $4'), $3, $4)')
define(`do1', `visit($1, $2, $3, $2, eval(`$3 + $4'), $4)')
define(`do2', `visit($1, $2, $3, eval(`$2 - $4'), $3, $4)')
define(`do3', `visit($1, $2, $3, $2, eval(`$3 - $4'), $4)')
define(`_do', `do$1(1, x1, y1, $2)first(`do'eval(`0x$3%4'))(2, x2, y2,
  eval(`0x$3/16'))')
define(`do', `_$0(translit(`$1', `.', `,'))')

ifdef(`__gnu__', `
  patsubst(defn(`input'), `\([^;]*\);', `do(`\1')')
', `
  define(`_chew', `do(substr(`$1', 0, index(`$1', `;')))define(
    `tail', substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(`tail'),
    `;'), -1, `', `$0(defn(`tail'))')')
  define(`chew', `ifelse(eval($1 < 30), 1, `_$0(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  chew(len(defn(`input')), defn(`input'))
')

define(`_half', `substr(`$1', 0, decr(len(`$1')))')
define(`half', `_$0(mul64(`$1', 5))')
define(`part1', eval(translit(area1, `-')/2 + perim1/2 + 1))
define(`part2', add64(half(translit(area2, `-')), eval(perim2/2 + 1)))

divert`'part1
part2
