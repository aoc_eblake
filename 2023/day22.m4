divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Ddata=day22.input] day22.m4

include(`common.m4')ifelse(common(22), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

dnl Transition away from fifth glyph
define(`macro', defn(`define'))
macro(`guts', defn(`defn'))
macro(`window', guts(`divert'))
macro(`push', guts(`pushdef'))
macro(`pop', guts(`popdef'))
macro(`load', `include($@)')dnl thanks to common.m4
macro(`math', guts(`eval'))
macro(`count', guts(`len'))
macro(`spot', guts(`index'))
ifdef(`data', `', `macro(`data', guts(`file'))')

macro(`input', translit((load(guts(`data'))), nl`~,()', `;..'))
macro(`grow', `macro(`$1', guts(`$1')`$2')')
macro(`bricks', 0)macro(`dim1', 0)macro(`dim2', 0)macro(`dim3', 0)
macro(`saturation', `ifelse(math($2>$1), 1, `macro(`$1', $2)')')
macro(`_do', `macro(`brick$1', `$2,$3,$4,$5,$6,$7')macro(`supports$1')macro(
  `atop$1')saturation(`dim1', $5)saturation(`dim2', $6)saturation(`dim3',
  $4)push(`rank$4', $1)')
macro(`do', `_$0($1, translit(`$2', `.', `,'))macro(`bricks', $1)')

ifdef(`__gnu__', `
  patsubst(guts(`input'), `\([^;]*\);', `do(incr(bricks), `\1')')
', `
  macro(`_chomp', `do(incr(bricks), substr(`$1', 0, spot(`$1', `;')))macro(
    `tail', substr(`$1', incr(spot(`$1', `;'))))ifelse(spot(guts(`tail'),
    `;'), -1, `', `$0(guts(`tail'))')')
  macro(`chomp', `ifelse(math($1 < 32), 1, `_$0(`$2')', `$0(math($1/2),
    substr(`$2', 0, math($1/2)))$0(math(count(guts(`tail')) + $1 - $1/2),
    guts(`tail')substr(`$2', math($1/2)))')')
  chomp(count(guts(`input')), guts(`input'))
')

macro(`_init', `macro(`tall$1_$2', 0)')
macro(`init', `forloop(0, dim2, `_$0($1, ', `)')')
forloop_arg(0, dim1, `init')
macro(`_findtop', `forloop($2, $3, `saturation(`top', guts(`tall$1_'', `))')')
macro(`findtop', `macro(`top', 0)forloop($1, $2, `_$0(', `, $3, $4)')top')
macro(`_land', `ifdef(`at$2_$3_$4', `grow(`atop$1', at$2_$3_$4`,')grow(
  `supports'at$2_$3_$4, `$1,')')macro(`tall$2_$3', $5)macro(`at$2_$3_$5', $1)')
macro(`land', `forloop($3, $4, `_$0($1, $2, ', `, $5, $6)')')
macro(`_position', `forloop($2, $5, `land($1, ', `, $3, $6, $8,
  'math($8+$7-$4+1)`)')')
macro(`position', `_$0($@, findtop($2, $5, $3, $6))')
macro(`fall', `ifdef(`rank$1', `position(rank$1, first(`brick'rank$1))pop(
  `rank$1')$0($@)')')
forloop_arg(1, dim3, `fall')

macro(`assay', `ifelse($1, `', `', $1, $2, `$0(shift($@))', $2, `',
  `macro(`risky$1')')')
forloop(1, bricks, `assay(first(`atop'', `))')
macro(`part1', count(forloop(1, bricks, `ifdef(`risky'', `, `', `-')')))

macro(`part2', 0)
macro(`mop', `ifdef(`list', `pop(guts(`list'))pop(`list')$0()')')
macro(`_audit', `ifelse($2, `', `macro(`part2', incr(part2))pull($1)',
  `ifdef(`bad$2', `$0($1, shift(shift($@)))')')')
macro(`audit', `ifelse($2, `', `', $2, $3, `$0($1, shift(shift($@)))',
  `_$0($2, atop$2)$0($1, shift(shift($@)))')')
macro(`pull', `ifdef(`bad$1', `', `push(`bad$1')push(`list',
  `bad$1')audit(`$1', supports$1)')')
forloop(1, bricks, `pull(', `)mop()')

window`'part1
part2
