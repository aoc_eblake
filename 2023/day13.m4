divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day13.input] day13.m4

include(`common.m4')ifelse(common(13), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`input', translit(include(defn(`file'))2, `.#'nl, `012'))
define(`x', 0)define(`y', 0)define(`g', 0)
define(`append', `define(`$1', eval((defn(`$1')+0)*2|$2))')
define(`do0', `append(`r$1_$3', $4)append(`c$1_$2', $4)define(`x', incr($2))')
define(`do1', defn(`do0'))
define(`do2', `ifelse($2, 0, `define(`g$1', g$1`,'decr($3))define(`g',
  incr($1))define(`y', 0)', `define(`g$1', decr($2))define(`x', 0)define(`y',
  incr($3))')')

ifdef(`__gnu__', `
  patsubst(defn(`input'), `.', `do\&(g, x, y, \&)')
',`
  define(`chew', `ifelse($1, 1, `do$2(g, x, y, $2)', `$0(eval($1/2), substr(
    `$2', 0, eval($1/2)))$0(eval($1-$1/2), substr(`$2', eval($1/2)))')')
  chew(len(defn(`input')), defn(`input'))
')

define(`part1', 0)define(`part2', 0)
define(`bump', `define(`$1', eval($1+$2))')
define(`smudge', `eval(`($1)&(($1)-1)')')

# try(seen, lo, hi, name, bound, amt)
define(`_try', `ifelse($1$2, 0, `bump(`part1', $6)', $1$3, $5, `bump(`part1',
  $6)', $1$2, !0, `bump(`part2', $6)', $1$3, !$5, `bump(`part2', $6)',
  `try($1, decr($2), incr($3), `$4', $5, $6)')')
define(`try', `ifelse($4_$2, $4_$3, `_$0($@)', $1smudge($4_$2 ^ $4_$3), 0,
  `_$0(!$@)')')
define(`_reflect', `try(`', decr($1), $@*$1)')
define(`reflect', `forloop(1, $2, `_$0(', `, $@)')')
define(`_check', `reflect(`r$1', $3, 100)reflect(`c$1', $2, 1)')
define(`check', `_$0($1, g$1)')
forloop_arg(0, decr(g), `check')

divert`'part1
part2
