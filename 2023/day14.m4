divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day14.input] day14.m4
# Optionally use -Dlimit=N to stop part 2 at N spins
# Optionally use -Dverbose=1 to see some progress

include(`common.m4')ifelse(common(14), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

# Using the following macros:
# p: 1-D coordinate while parsing grid, 1-based (implicit square rock border),
#  p = x+y*maxx
# maxx, maxy: size of grid (maxx includes newline)
# [nwse]P: P,d collector spot to use when rock at P needs to move in
#  direction d, only populated for grid spots that were . or O at beginning
# cP[nwse]: number of rocks in collector spot for direction
# q[nwse]: queue of collector spots with rocks in direction
# n: number of round rocks
# lW_E: spin at which west/east load pair was last seen
# lS: load after spin S

define(`input', translit(include(defn(`file')), `.#'nl, `123'))
define(`maxx', incr(index(defn(`input'), 3)))
define(`maxy', len(translit(defn(`input'), `O12')))
define(`p', maxx)define(`n', 0)
define(`collect', `ifdef(`c$1$2', `define(`c$1$2', incr(c$1$2))',
  `define(`c$1$2', 1)pushdef(`q$2', `$1')')')
define(`propn', `ifdef(`n$1', `defn(`n$1')', ``$2,`n''')')
define(`propw', `ifdef(`w$1', `defn(`w$1')', ``$2,`w''')')
define(`props', `ifdef(`n$1', `define(`s$1', `$2,`s'')$0(eval($1-''maxx``),
  $2)')')
define(`prope', `ifdef(`w$1', `define(`e$1', `$2,`e'')$0(decr($1), $2)')')
define(`do0', `define(`p', incr($1))')
define(`doO', `do1($1)collect(n$1)define(`n', incr(n))')
define(`do1', `define(`n$1', propn(eval($1-'maxx`), $1))define(`w$1',
  propw(decr($1), $1))do0($1)')
define(`do2', `props(eval($1-'maxx`), eval($1-'maxx`))prope(decr($1),
  decr($1))do0($1)')
define(`do3', `prope(decr($1), decr($1))do0($1)')

ifdef(`__gnu__', `
  patsubst(defn(`input'), `.', `do\&(p)')
',`
  define(`chew', `ifelse($1, 1, `do$2(p)', `$0(eval($1/2), substr(
    `$2', 0, eval($1/2)))$0(eval($1-$1/2), substr(`$2', eval($1/2)))')')
  chew(len(defn(`input')), defn(`input'))
')

# Part 1 falls out of the first spin round. Part 2 requires running
# spins until a cycle is detected, and extrapolating that out to the limit.
define(`prep', `props($1, $1)')
forloop_arg(eval(p-maxx), decr(p), `prep')
define(`_placen', `collect(w$1)ifelse($2, 1, `', `$0(eval($1+''maxx``),
  decr($2))')')
define(`_placew', `-$1/'maxx`collect(s$1)ifelse($2, 1, `', `$0(incr($1),
  decr($2))')')
define(`_places', `collect(e$1)ifelse($2, 1, `', `$0(eval($1-''maxx``),
  decr($2))')')
define(`_placee', `-$1/'maxx`collect(n$1)ifelse($2, 1, `', `$0(decr($1),
  decr($2))')')
define(`_place', `$0$1($2, c$2$1)popdef(`c$2$1')')
define(`place', `ifdef(`q$1', `_$0(`$1', q$1)popdef(`q$1')$0(`$1')')')
define(`_spin', `ifelse(eval($1%20), 0, `output(1, `...$1')')ifdef(`l$2_$3',
  `ifelse(eval($1-l$2_$3), defn(`cycle'), `-', `define(`cycle',
  eval($1-l$2_$3))')', `popdef(`cycle')')define(`l$2_$3', $1)define(`l$1', $3)')
pushdef(`_spin', `define(`part1', $2)popdef(`$0')')
define(`spin', `ifelse(_$0($1, eval('n*incr(maxy)`place(`n')place(`w')),
  eval('n*incr(maxy)`place(`s')place(`e'))), `-', `defn(`l'eval($1-cycle+(
  1000000000-$1)%cycle))', `$0(incr($1))')')
define(`part2', spin(1))

divert`'part1
part2
