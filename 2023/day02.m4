divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day02.input] day02.m4

include(`common.m4')ifelse(common(02), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`part1', 0)define(`part2', 0)
define(`try', `ifelse(`$2', `red', `$1 > 12 || $0(shift(shift($@)))',
  `$2', `green', `$1 > 13 || $0(shift(shift($@)))',
  `$2', `blue', `$1 > 14 || $0(shift(shift($@)))', 0)')
define(`_game1', `ifelse(eval(try(translit(`$2', ` ', `,'))), `0',
  `game1(`$1', shift(shift($@)))')')
define(`game1', `ifelse(`$2', `', `define(`part1', eval(part1+$1))',
  `_$0($@)')')
define(`max', `ifelse(eval(`$1>$2'), 1, `$1', `$2')')
define(`_red', `max($1, $4)`,$2,$3'')
define(`_green', ``$1,'max($2, $4)`,$3'')
define(`_blue', ``$1,$2,'max($3, $4)')
define(`splat', ``$1*$2*$3'')
define(`game2', `ifelse(`$2', `', `define(`part2', eval(part2+splat($1)))',
  `$0(_$3($1, $2), shift(shift(shift($@))))')')
define(`game', `game1($@)game2(`0,0,0', translit((shift($@)), ` ()', `,'))')
translit((include(defn(`file'))), define(`Game', `game(')nl`:;,', `),,')

ifelse(`
dnl Here is my entry for Allez Cuisine: Cooking with C-food!
changequote(🐟,🐠)define(C,🐟ifelse(index($1,^),0,🐟shift($@)🐠,$1,><>,🐟C(
^C(^C(^C(^C(^C(^$@))))))🐠,$1,~,🐟eval(($2>$3)*$2+($2<=$3)*$3)🐠,$4$5,,🐟) C(
~,0,$1*$2*$3🐠,$4,,🐟C($1,$2,$3,C(><>,,$@))🐠,$5,ray,🐟*($4<13)C(C(~,$1,$4),
$2,$3,C(><>,$@))🐠,$5,craab,🐟*($4<14)C($1,C(~,$2,$4),$3,C(><>,$@))🐠,$5,
orca,🐟*($4<15)C($1,$2,C(~,$3,$4),C(><>,$@))🐠,$4,tuna,🐟+$5C(0,0,0,C(><>,
$@))+$1*$2*$3🐠)🐠)translit(_EeL(s(0,0,0,include(I))), (medusa_EGg
nlbiL ):;, (naycCuevtc,broil,))
')

divert`'part1
part2
