divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day24.input] day24.m4
# Optionally use -Dskip1 to skip part 1
# Optionally use -Dlo=N -Dhi=N to alter bounds
# Optionally use -Dverbose=1 to see some progress

include(`common.m4')ifelse(common(24), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

include(`math64.m4')

# Set up default bounds based on size of input, along with chop() and
# fudge() for converting part 1 numbers into a more manageable range
# (since m4 only has native 32-bit signed math).
#
# 400000000000000 is 49 bits; doing 44850 pairs in arbitrary-precision
# integers turns into a LOT of work. Part 2 is O(1); fast enough to
# remain numerically accurate (although one of my intermediate
# computations hits a 45-digit decimal number), but since part 1 is
# O(n^2), the overhead of repeated mul64() is so painfully slow that
# anything I can do to reduce the problem into just signed 32-bit math
# is will worth the effort.  Thankfully, no hailstones have 0 or
# infinite velocity in any dimension.
#
# My goal is to quantize things so that endpoints are still unique;
# the initial step is to truncate down to the nearest 1000000000,
# leaving a position in the range [0,999999].  Since all velocities
# are in the range [-999,999], this means my maximum velocity will
# compute a fudged time that is within 1% of the actual floating point
# value.  Next, shift the positions so that the center of interest is
# at the origin (changing the bounds to -100000 to 100000).  At that
# point, I can solve for the time of intersection with a boundary line
# using 32-bit division, but keep track of the remainder so as to
# compute the nearest integer times of the actual endpoints of the
# segment.  I then need one more round of reduction by scaling so that
# multiplying endpoints still remains within signed 32-bit.
#
# I'm hopeful, but not 100% certain, that this simplification will
# work on all inputs; on my input, I had at least one pair of lines
# where the actual X,Y intersection in floating point (after
# quantizing and shifting, but before scaling) was (64062.5,99948.8);
# when using just integer math and ignoring the remainder, I computed
# an endpoint of one of the two lines at (64061, 99923) which fell
# outside the intersection; but with the remainder, the endpoint was
# (64065, 100000).  So tracking the remainder is essential.  As it is,
# I still had one pair of inputs both produce rounded endpoints at
# (-75496,-100000) pre-fudging but luckily did not give me an
# off-byone; if other inputs fail, I may need to try to make chop()
# round to nearest rather than to zero, and/or fudge by as little as 5
# rather than 10 while still staying in the signed integer sweet spot.
define(`input', translit((include(defn(`file'))), nl`,@ ()', `;..'))
ifelse(len(index(defn(`input'), `.')), 1, `
  ifdef(`lo', `', `define(`lo', 7)')
  ifdef(`hi', `', `define(`hi', 27)')
  define(`chop', `eval($1-('hi+lo`)/2)')
  define(`fudge', `$1')
', `
  ifdef(`lo', `', `define(`lo', `200000000000000')')
  ifdef(`hi', `', `define(`hi', `400000000000000')')
  define(`chophalf', `substr(`$1', 0, eval(len(`$1')-len(`000000000'`0')))')
  define(`mid', chophalf(mul64(add64(lo, hi), 5)))
  define(`chop', `eval(substr(`$1', 0, eval(len(`$1')-'len(
    `000000000')`))-'mid`)')
  define(`fudge', `eval($1/10)')
')
define(`lo', chop(lo))
define(`hi', chop(hi))

# For each row of input (x, y, z, dx, dy, dz), I have sufficient
# information to compute the approximate time of intersection with
# each of the four X and Y boundaries, then turn that back into an
# integer approximation of (X,Y) segment endpoints.  Out of 5 points
# sampled, only 2 will be within or very near the window; later
# processing doesn't care which of the two points is listed first in
# ptN.  (I found it simpler to always probe 5 points, rather than
# trying to write up a messy if-chain to get the right 2 points on the
# first two computations; and it helps that this part of processing is
# still O(n) compared to the later O(n^2) comparisons.)  For the
# example input, endpoints will be in the range [-10,10] (instead of
# [7,27]); for actual input, endpoints will be in the range
# [-10000,10000].
define(`p', 0)
define(`check', `ifelse(eval(`$3 < 0 || $1 < ''lo`` - $4 || $1 > ''hi`` + $4
  || $2 < ''lo`` - $4 || $2 > ''hi`` + $4'), 1, `', `,$1,$2')')
define(`time', `$3, eval(($2 - $1)/$3), eval(($2 - $1)%$3)')
define(`points_', `check(eval(`$1 + $7*$3 + $3*$8/$6'),
  eval(`$2 + $7*$4 + $4*$8/$6'), $7, decr(translit($6, `-')))')
define(`_points', `ifelse(`$#', 5, `define(`pt$1', fudge($2)`, 'fudge(
  $3)`, 'fudge($4)`, 'fudge($5))',
  `fatal(`oops, wrong number of points found')')')
define(`points', `_$0($5check($1, $2, 0, 0)$0_($@, time($1, 'lo`, $3))$0_($@,
  time($1, 'hi`, $3))$0_($@, time($2, 'lo`, $4))$0_($@, time($2, 'hi`, $4)))')
ifdef(`skip1', `pushdef(`points')')
define(`_do', `define(`p$7', `$@')points(chop($1), chop($2), $4, $5,
  $7)define(`p', incr($7))')
define(`do', `_$0(translit(`$1', `.', `,'), p)')

ifdef(`__gnu__', `
  patsubst(defn(`input'), `\([^;]*\);', `do(`\1')')
', `
  define(`_chew', `do(substr(`$1', 0, index(`$1', `;')))define(
    `tail', substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(`tail'),
    `;'), -1, `', `$0(defn(`tail'))')')
  define(`chew', `ifelse(eval($1 < 140), 1, `_$0(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  chew(len(defn(`input')), defn(`input'))
')

# For part 1, check each pair of line segments for intersections.
# This can be done without division, and without even computing the
# coordinates of the actual intersection, by merely checking whether
# each grouping of 3 of the 4 endpoints is oriented clockwise or
# counter-clockwise:
# https://www.geeksforgeeks.org/check-if-two-given-line-segments-intersect/
# If all my endpoints are unique, then the constraints imposed by part
# 2 having a solution are probably sufficient to assume I'll never
# have to worry about collinearity; but actually proving that by
# writing _orient() to cause a division by zero on equality was noisy
# for one pair of segments in my input.  If other inputs are even more
# problematic, I'll need to pay more attention to lines (nearly)
# sharing an endpoint.
#
# TODO: Possible Part 1 optimizations: The
# https://en.wikipedia.org/wiki/Bentley%E2%80%93Ottmann_algorithm
# takes only O((n+k)log n) time and O(n) space, where k is the number
# of intersections, and mentions the Balaban algorithm
# (https://dl.acm.org/doi/10.1145/220279.220302) that does the same in
# even faster O(n log n + k) time and O(n) space.  Sadly, for my input,
# k is about 59% of n^2, so O(k) may still be slower than O(n log n).
ifdef(`skip1', `', `output(1, `...endpoints computed')')
define(`_orient', `eval(($1>0)-($1<0))')
define(`orient', `_$0(($4 - $2)*($5 - $3) - ($3 - $1)*($6 - $4))')
define(`compare', `ifelse(orient($1, $2, $3, $4, $5, $6), orient($1, $2, $3,
  $4, $7, $8), `', orient($5, $6, $7, $8, $1, $2), orient($5, $6, $7, $8, $3,
  $4), `', `define(`part1', incr(part1))')')
define(`_probe', `compare(pt$1, pt$2)')
define(`probe', `forloop(incr($1), 'decr(p)`, `_$0($1, ', `)')')
ifdef(`skip1', `', `define(`part1', 0)forloop_arg(0, decr(decr(p)), `probe')')

# Part 2 is much less work, although still a mouthful to code up.
# At time t, our rock's position will be r(t) = (x + t*dx, y + t*dy, z + t*dz)
# Each hailstone pN(t) is likewise at (xN + t*dxN, yN + t*dyN, zN + t*dzN)
# Since the rock and a hailstone intersect at some time tN, we now have these
# equalities across 6 wanted unknowns x, y, z, dx, dy, and dz:
# tN = (xN-x)/(dx-dxN) = (yN-y)/(dy-dyN) = (zN-z)/(dz-dzN)
# Since we know the lines will intersect in all dimensions, we really only
# need to solve for four of the unknowns, then extrapolate z based on p(t).
# By itself, our stone and one hailstone produce a non-linear equation:
# xN*dy - xN*dyN + x*dyN - yN*dx + yN*dxN - y*dxN = x*dy - y*dx
# But with a second hailstone pM(t), we can subtract out that non-linearity:
# xM*dy - xM*dyM + x*dyM - yM*dx + yM*dxM - y*dxM = x*dy - y*dx
# More precisely, we can build the augmented row matrix [x y dx dy | k] as:
# [ dyN-dyM  -dxN+dxM  -yN+yM  xN-xM | xN*dyN - yN*dxN - xM*dyM + yM*dxM ]
#
# Repeating this with 3 more pairs adds more rows, to create a 4x4 augmented
# matrix, which we can then solve to determine 2/3 of the rock setup.  The
# remaining coordinate can be solved with the 2x2 augmented matrix:
# tN = (xN-x)/(dx-dxN), tM = (xM-x)/(dx-dxM)  (both integers, phew)
# [ 1  tN | zN + dzN*tN ]
# [ 1  tM | zM + dzM*tM ]
#
# We assume the first hailstone will not be parallel with any of the next
# four; if so, the matrix will not yield a solution without looking at more.
# We likewise assume the intersections will land at integers, but since
# the division implemented here is suboptimal (not generic enough for
# inclusion in math64.m4 itself), it is easier to just use really large
# numbers and avoiding division until in reduced row echelon form.
output(1, `...building matrix')
ifelse(eval(p<5), 1, `fatal(`insufficient input')')
define(`bits', `_$0(eval($1, 2))')
define(`_bits', ifdef(`__gnu__', ``shift(patsubst($1, ., `, \&'))'',
  ``ifelse(len($1), 1, `$1', `substr($1, 0, 1),$0(substr($1, 1))')''))
define(`bits64', `ifelse(eval(len($1) < 10), 1, `bits($1)', `_$0(mul64($1,
  5)), eval(substr($1, decr(len($1))) & 1)')')
define(`_bits64', `bits64(substr($1, 0, decr(len($1))))')
define(`remquo64', `ifelse(`$4', `', ``$1',`$2'', `$0(_$0(add64(`$1', `$1'),
  add64(add64(`$2', `$2'), `$4'), `$3'), $3, shift(shift(shift(shift($@)))))')')
define(`_remquo64', `ifelse(lt64($2, $3), 0, `add64($1, 1), sub64($2, $3)',
  ``$1',`$2'')')
define(`_div64pp', `first(remquo64(0, 0, $2, bits64(`$1')))')
define(`_div64pn', `-'defn(`_div64pp'))
define(`_div64np', defn(`_div64pn'))
define(`_div64nn', defn(`_div64pp'))
define(`div64', `ifelse($1, 0, 0, `perform(`_$0', $@)')')

# makerow(M) -> M x0 dx0 y0 dy0 xM dxM yM dyM
define(`_makerow', `define(`m$1_1', eval($5 - $9))define(`m$1_2', eval($7 -
  $3))define(`m$1_3', sub64(`$8', `$4'))define(`m$1_4', sub64(`$2',
  `$6'))define(`m$1_5', add(mul64(`$2', $5), mul64(`-$4', $3), mul64(`-$6',
  $9), mul64(`$8', $7)))define(`row$1', m$1_1`,'m$1_2`,'m$1_3`,'m$1_4`,'m$1_5)')
define(`makerow', `_$0($1, extract(,p0), extract(p0), extract(,p$1),
  extract(p$1))')
define(`extract', `$2, $5')
forloop_arg(1, 4, `makerow')
# m -> dump current matrix
define(`_m', `m$1_1, m$1_2, m$1_3, m$1_4, m$1_5`'nl')
define(`m', `forloop_arg(1, 4, `_$0')')
# addrow(dstrow, srcrow, col) - add srcrow into dstrow so that dst_col is 0
define(`_addrow', `define(`m$1_$5', sub64(mul64($3, m$2_$5), mul64($4,
  m$1_$5)))')
define(`addrow', `forloop(1, 5, `_$0($1, $2, 'm$1_$3`, 'm$2_$3`, ', `)')')
# scale(row) -> final scale of row in reduced echelon form
define(`scale', `define(`m$1_5', div64(m$1_5, m$1_$1))define(`m$1_$1', 1)')

# Gauss-Jordan elimination, exploiting that results will be integers
addrow(2, 1, 1)
addrow(3, 1, 1)
addrow(4, 1, 1)
addrow(3, 2, 2)
addrow(4, 2, 2)
addrow(4, 3, 3)
scale(4)
addrow(3, 4, 4)
addrow(2, 4, 4)
addrow(1, 4, 4)
scale(3)
addrow(2, 3, 3)
addrow(1, 3, 3)
scale(2)
addrow(1, 2, 2)
scale(1)
# Now use x, y, dx, and dy to determine z
# gettime(x0, dx0, x1, dx1, x, dx)
define(`gettime', `div64(sub64(`$1', `$5'), eval($6 - $2)), div64(sub64(`$3',
  `$5'), eval($6 - $4))')
# getz(z0, dz0, z1, dz1, t0, t1)
define(`getz', `add64(add64(`$1', mul64($2, `$5')), mul64(`-$5',
  div64(sub64(add64(`$3', mul64($4, `$6')), add64(`$1', mul64($2, `$5'))),
  add64(`$6', `-$5'))))')
define(`part2', add64(m1_5, add64(m2_5, getz(extract(shift(p0)),
  extract(shift(p1)), gettime(extract(, p0), extract(, p1), m1_5, m3_5)))))

divert`'part1
part2
