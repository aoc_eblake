divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day15.input] day15.m4

include(`common.m4')ifelse(common(15), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

# Translit to all caps to avoid macro collisions
# m4 lacks native byte->ASCII, so open-code it ;)
# Longest input string is 6 letters, =, then digit
define(`Nl', defn(`nl'))
pushdef(`nl', ``$0'')pushdef(`dnl', ``$0'')
define(`input', translit((include(defn(`file'))), defn(`alpha')Nl`,()',
  defn(`ALPHA')`;;'))
popdef(`nl')popdef(`dnl')

define(`part1', 0)define(`part2', 0)
define(`_ascii', `ifelse(`$2', `=', 61, `$2', `-', 45, `$1', -1, `$2+48',
  `$1+97')')
define(`ascii', `_$0(index('dquote(defn(`ALPHA'))`, `$1'), `$1')')
define(`char', `eval(($1+$2)*17%256)')
define(`_add', `ifelse(`$1', `$2', `pushdef(`seen')')')
define(`add', `define(`v$2', `$3')_stack_foreach(`s$1', `_$0(', `, `$2')',
  `t')ifdef(`seen', `popdef(`seen')', `pushdef(`s$1', `$2')')')
define(`_rem', `ifelse(`$1', `$3', `popdef(`s$2')')')
define(`rem', `define(`v$2', 0)_stack_foreach(`s$1', `_$0(', `, $@)', `t')')
define(`hash', `ifelse(`$3', `-', `rem($1, `$2')', `$3', `=', `add($1, `$2',
  $4)')ifelse(`$3', `', `$1', `$0(char($1, ascii(`$3')), `$2$3',
  shift(shift(shift($@))))')')
define(`do', `define(`part1', eval(part1 + translit(`hash(0, `', `1', `2', `3',
  `4', `5', `6', `7', `8')', `12345678', `$1')))')

ifdef(`__gnu__', `
  patsubst(defn(`input'), `\([^;]*\);', `do(`\1')')
', `
  define(`_chew', `do(substr(`$1', 0, index(`$1', `;')))define(
    `tail', substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(`tail'),
    `;'), -1, `', `$0(defn(`tail'))')')
  define(`chew', `ifelse(eval($1 < 20), 1, `_$0(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  chew(len(defn(`input')), defn(`input'))
')

define(`_power', `define(`part2', eval(part2+$2*$3*v$1))define(`i', incr($3))')
define(`power', `define(`i', 1)_stack_foreach(`s$1', `_$0(', `, 'incr($1)`, i)',
  `t')')
forloop_arg(0, 255, `power')

ifelse(`
dnl For my reference, this is my golfed part 1, in POSIX...
define(C,`eval(($1+$2)*17%256)')define(B,`A(index(abcdefghijklmnopqrstuvwxyz,
$1),$1)')define(A,`ifelse($2,=,61,$2,-,45,$1,-1,$2+48,$1+97)')define(H,`ifelse(
$2,,$1,`H(C($1,B(substr($2,0,1))),substr($2,1))')')define(_,`+H(,$1)ifelse($2,,
,`_(shift($@))')')eval(_(include(I)))

dnl ...or in GNU
define(C,`eval(($1+$2)*17%256)')define(H,`ifelse($2,,$1,`H(C($1,
esyscmd(printf %d \"$2)),substr($2,1))')')define(_,`+H(,
$1)ifelse($2,,,`_(shift($@))')')eval(_(include(I)))
')

divert`'part1
part2
