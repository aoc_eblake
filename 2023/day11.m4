divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day11.input] [-Dgap=N] day11.m4
# where gap=N defaults to 1000000

include(`common.m4')ifelse(common(11), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

ifdef(`gap', `', `define(`gap', 1000000)')

# https://www.geeksforgeeks.org/sum-manhattan-distances-pairs-points/
# Using radix sort, since number of galaxies > number of rows

define(`input', translit(include(defn(`file')), nl`#', `;*'))
define(`x', 0)define(`y', 0)
define(`bump', `define(`$1', eval(defn(`$1')+$2))')
define(`do', `define(`x', incr($1))ifelse(`$3', `;', `define(`maxx', $1)define(
  `x', 0)define(`y', incr($2))', `$3', `*', `bump(`x$1', 1)bump(`y$2', 1)')')

ifdef(`__gnu__', `
  patsubst(defn(`input'), `.', `do(x, y, `\&')')
',`
  define(`chew', `ifelse($1, 1, `do(x, y, `$2')', `$0(eval($1/2), substr(
    `$2', 0, eval($1/2)))$0(eval($1-$1/2), substr(`$2', eval($1/2)))')')
  chew(len(defn(`input')), defn(`input'))
')

include(`math64.m4')
define(`visit', `,add64(mul64($2,i),-s)bump(`s', $2)bump(`i', 1)ifelse($1, 1,
  `', `$0(decr($1), $2)')')
define(`_dsum', `ifdef(`$1$2', `visit($1$2, eval(o+$2))', `bump(`o', `$3')')')
define(`dsum', `define(`i', 0)define(`o')define(`s', 0)forloop(0, $1,
  `define(`$3', add($3`'_$0(`$2', ', `, 'decr(`$4')`)))')')
define(`part1', 0)define(`part2', 0)
dsum(maxx, `x', `part1', 2)dsum(y, `y', `part1', 2)
dsum(maxx, `x', `part2', gap)dsum(y, `y', `part2', gap)

divert`'part1
part2
