divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day16.input] day16.m4
# Optionally use -Dverbose=1 to see some progress

include(`common.m4')ifelse(common(16), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

# Easier to map tiles to valid macro name components:
# \n./\|-
#  012345

define(`input', translit(include(defn(`file')), nl`./\|-', `012345'))
define(`x', 1)define(`y', 1)
define(`do', `ifelse(`$3', 0, `define(`maxx', $1)define(`x', 1)define(`y',
  incr($2))', `define(`x', incr($1))define(`g$1_$2', $3)')')

ifdef(`__gnu__', `
  patsubst(defn(`input'), `.', `do(x, y, \&)')
',`
  define(`chew', `ifelse($1, 1, `do(x, y, $2)', `$0(eval($1/2), substr(
    `$2', 0, eval($1/2)))$0(eval($1-$1/2), substr(`$2', eval($1/2)))')')
  chew(len(defn(`input')), defn(`input'))
')

define(`vr1', `visit(incr($1), $2, `r')')
define(`vu1', `visit($1, decr($2), `u')')
define(`vl1', `visit(decr($1), $2, `l')')
define(`vd1', `visit($1, incr($2), `d')')

define(`vr2', `visit($1, decr($2), `u')')
define(`vu2', `visit(incr($1), $2, `r')')
define(`vl2', `visit($1, incr($2), `d')')
define(`vd2', `visit(decr($1), $2, `l')')

define(`vr3', `visit($1, incr($2), `d')')
define(`vu3', `visit(decr($1), $2, `l')')
define(`vl3', `visit($1, decr($2), `u')')
define(`vd3', `visit(incr($1), $2, `r')')

define(`vr4', `ifelse(probe($1, $2), `', `visit($1, decr($2), `u')visit(
  $1, incr($2), `d')')')
define(`vu4', `visit($1, decr($2), `u')')
define(`vl4', `ifelse(probe($1, $2), `', `visit($1, decr($2), `u')visit(
  $1, incr($2), `d')')')
define(`vd4', `visit($1, incr($2), `d')')

define(`vr5', `visit(incr($1), $2, `r')')
define(`vu5', `ifelse(probe($1, $2), `', `visit(incr($1), $2, `r')visit(
  decr($1), $2, `l')')')
define(`vl5', `visit(decr($1), $2, `l')')
define(`vd5', `ifelse(probe($1, $2), `', `visit(incr($1), $2, `r')visit(
  decr($1), $2, `l')')')

forloop(1, decr(maxx), `define(`e'', ``_0_u')')
forloop(1, decr(y), `define(`e0_'', ``_l')')
forloop(1, decr(maxx), `define(`e'', ``_''y``_d')')
forloop(1, decr(y), `define(`e''maxx``_'', ``_r')')

define(`use', `pushdef(`$1')pushdef(`all', `$1')')
define(`clean', `ifdef(`all', `popdef(defn(`all'))popdef(`all')$0()')')
define(`visit', `ifdef(`e$1_$2_$3', `define(`s$1_$2')', `_$0($@, g$1_$2)')')
define(`track', `len(visit($@))clean()')
define(`startr', `decr($1),$2')
define(`startu', `$1,incr($2)')
define(`startl', `incr($1),$2')
define(`startd', `$1,decr($2)')
define(`_start', `ifdef(`s$1_$2', 0, `define(`hit', 0)eval(track($3, $4,
  `$5') + core*hit)')')
define(`start', `_$0($0$3($1, $2), $@)')

# Any exit energized by a prior entrance cannot produce a higher
# score.  Additionally, I assume that part 1 visits more than half the
# grid, and the bulk of that visit occurs after the first splitter hit
# from the side and appearing in a loop.  Find the number of cells
# energized by that core, at which point, the rest of the problem
# (including part 1) consists of finding how many additional cells are
# energized on the way into the core, rather than having to trace the
# full path from each start.

define(`_visit', `ifdef(`c$1_$2_$3', `ifdef(`e$1_$2', `', `.define(
  `e$1_$2')')define(`e$1_$2_$3')define(`c$1_$2')', `use(`c$1_$2_$3')')v$3$4($1,
  $2)')
define(`probe')
define(`core', track(1, 1, `r'))

define(`_visit', `ifdef(`e$1_$2', `', `.use(`e$1_$2')')use(`e$1_$2_$3')v$3$4($1,
  $2)')
define(`probe', `ifdef(`c$1_$2', `define(`hit', 1)-')')
define(`part1', start(1, 1, `r'))
ifelse(eval(part1 < maxx*y/2), 1,
  `fatal(`input is suspect, central core missing')')

define(`part2', part1)
define(`check', `ifelse(eval(part2 < $1), 1, `define(`part2', $1)')')
output(1, `d...')
forloop(1, decr(maxx), `check(start(', `, 1, `d'))')
output(1, `r...')
forloop(2, decr(y), `check(start(1, ', `, `r'))')
output(1, `u...')
forloop(1, decr(maxx), `check(start(', `, 'decr(y)`, `u'))')
output(1, `l...')
forloop(1, decr(y), `check(start('decr(maxx)`, ', `, `l'))')

divert`'part1
part2
