divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day10.input] day10.m4
# Optionally with -Dcsv to enable CSV output

include(`common.m4')ifelse(common(10), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

# Remap into characters usable in macros.
# -|LJFS7.\n
# 123456708

# Another potential optimization not considered here:
# https://www.reddit.com/r/adventofcode/comments/18evyu9/comment/kcqmhwk/
# Use the shoelace formula to calculate area (determinate at each LJF7),
# then Pick's formula to determine interior points

define(`input', translit(include(defn(`file')), `-|LJFS.'nl, `12345608'))
define(`x', 1)define(`y', 1)
define(`p0')
define(`p1', `define(`p$3e$4', `incr($$1),$$2,`e'')define(`p$3w$4',
  `decr($$1),$$2,`w'')')
define(`p2', `define(`p$3n$4', `$$1,decr($$2),`n'')define(`p$3s$4',
  `$$1,incr($$2),`s'')define(`d$3_$4')')
define(`p3', `define(`p$3s$4', `incr($$1),$$2,`e'')define(`p$3w$4',
  `$$1,decr($$2),`n'')define(`d$3_$4')')
define(`p4', `define(`p$3s$4', `decr($$1),$$2,`w'')define(`p$3e$4',
  `$$1,decr($$2),`n'')define(`d$3_$4')')
define(`p5', `define(`p$3n$4', `incr($$1),$$2,`e'')define(`p$3w$4',
  `$$1,incr($$2),`s'')')
define(`p6', `ifdef(`p$3n'decr($4), `define(`d$3_$4')define(`start',
  `$3,'decr($4)`,`n'')', `ifdef(`p'decr($3)`w$4', `define(`start',
  decr($3)`,$4,`w'')', `define(`start', incr($3)`,$4,`e'')')')define(
  `p$3n$4')define(`p$3e$4')define(`p$3s$4')define(`p$3w$4')')
define(`p7', `define(`p$3n$4', `decr($$1),$$2,`w'')define(`p$3e$4',
  `$$1,incr($$2),`s'')')
define(`p8', `define(`maxx', `$3')define(`x', 1)define(`y', incr($4))')
define(`do', `define(`x', incr($1))p$3(1, 2, $@)')

ifdef(`__gnu__', `
  patsubst(defn(`input'), `.', `do(x, y, `\&')')
',`
  define(`chew', `ifelse($1, 1, `do(x, y, `$2')', `$0(eval($1/2), substr(
    `$2', 0, eval($1/2)))$0(eval($1-$1/2), substr(`$2', eval($1/2)))')')
  chew(len(defn(`input')), defn(`input'))
')

define(`run', `ifelse(`$2', `', `eval($1/2)',
  `define(`v$2_$3', ``$4'')$0(incr($1), p$2$4$3($2, $3))')')
define(`part1', run(0, start))

define(`dump', `ifdef(`csv',
  `divert(1)ifelse(`$1', nl, `', `,')$1$2`'divert(-1)', `$2')')
define(`_scan', `ifdef(`v$1_$2', `dump(v$1_$2)ifdef(`d$1_$2', `ifdef(`i',
  `popdef(`i')', `define(`i', `1')')')', `dump(`', defn(`i'))')')
define(`scan', `forloop(1, 'decr(maxx)`, `_$0(', `,$1)')dump(nl)')
define(`collect', `forloop_arg(1, decr(y), `scan')')
ifdef(`csv', `collect`'define(`part2', ``=COUNT(a3:ez''incr(y)``)'')',
  `define(`part2', len(collect))')

divert`'part1
part2
