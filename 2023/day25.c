#include <stdio.h>
#include <stdbool.h>

/* https://en.wikipedia.org/wiki/Stoer%E2%80%93Wagner_algorithm
 * Modified by the fact that we KNOW mincut is 3, and we instead
 * want the count of nodes on each side of the cut.  So, instead
 * of running the algorithm to the end, we track an additional
 * node-count metric, and stop when we hit the desired mincount.
 */

#define maxn 1700 /* 1484 in my input */
int map[26*26*26]; /* map input 3-alpha into node number */
int edge[maxn][maxn]; /* weighted adjacency matrix */
int dist[maxn]; /* weight of each node */
int count[maxn]; /* number of nodes merged together */
bool vis[maxn]; /* true if vertex visited */
bool bin[maxn]; /* true for vertex deleted by merging */
int n; /* vertex count; for convenience first vertex is 1 */

int contract(int *s, int *t)
{
  int i, j, k, mincut, maxc;
  for (i = 1; i <= n; i++) {
    dist[i] = vis[i] = 0;
  }
  for (i = 1; i <= n; i++) {
    k = -1; maxc = -1;
    for (j = 1; j <= n; j++)
      if (!bin[j] && !vis[j] && dist[j] > maxc) {
        k = j; maxc = dist[j];
      }
    if (k == -1)
      return mincut;
    *s = *t; *t = k;
    mincut = maxc;
    vis[k] = true;
    for (j = 1; j <= n; j++)
      if (!bin[j] && !vis[j])
        dist[j] += edge[k][j];
  }
  return mincut;
}

int stoer_wagner(void)
{
  int i, j, s, t, ans;
  for (i = 1; i < n; i++) {
    ans = contract(&s, &t);
    bin[t] = true;
    if (ans == 3) {
      printf("%d %d %d %d\n", s, t, count[s], count[t]);
      ans = count[s] + count[t];
      printf("%d or ", count[t] * (n - count[t]));
      return ans * (n - ans);
    }
    count[s] += count[t];
    for (j = 1; j <= n; j++)
      if (!bin[j])
        edge[s][j] = (edge[j][s] += edge[j][t]);
  }
  return 0;
}

int value(char name[3]) {
  int v = (name[0]-'a')*26*26 + (name[1]-'a')*26 + name[2]-'a';
  if (!map[v]) {
    count[n] = 1;
    map[v] = ++n;
  }
  return map[v];
}

int main(int argc, char **argv)
{
  char name[3];
  int a, b;
  FILE *in;

  if (argc != 2) {
    printf("usage: %s day25.input\n", argv[0]);
    return 1;
  }
  in = fopen(argv[1], "r");
  if (!in) {
    printf("failure to open file\n");
    return 1;
  }
  while (fscanf(in, "%3c:", name) == 1) {
    a = value(name);
    while (getc(in) == ' ') {
      fscanf(in, "%3c", name);
      b = value(name);
      edge[a][b] = edge[b][a] = 1;
    }
  }
  printf("%d\n", stoer_wagner());
  return 0;
}
