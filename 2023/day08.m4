divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day08.input] day08.m4
# Optionally use -Dverbose=1 to see some progress

include(`common.m4')ifelse(common(08), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`input', translit((include(defn(`file'))), nl`(=, )', `;'))
define(`offset', index(defn(`input'), `;;'))
define(`dirs', substr(defn(`input'), 0, offset))
define(`nodes', substr(defn(`input'), incr(incr(offset))))

define(`d', 0)
define(`dir', `define(`d$1', `$2')define(`d', incr($1))')
define(`do', `translit(`define(`L_123', `456')define(`R_123', `789')ifelse(`3',
  `A', `pushdef(`starts', `123')', `3', `Z', `define(`z123')')',
  `123456789', `$1')')

ifdef(`__gnu__', `
  patsubst(defn(`dirs'), `.', `dir(d, `\&')')
  patsubst(defn(`nodes'), `\([^;]*\);', `do(`\1')')
', `
  define(`chew', `ifelse($1, 1, `dir(d, `$2')', `$0(eval($1/2), substr(
    `$2', 0, eval($1/2)))$0(eval($1-$1/2), substr(`$2', eval($1/2)))')')
  chew(len(defn(`dirs')), defn(`dirs'))
  define(`_chew', `do(substr(`$1', 0, index(`$1', `;')))define(
    `tail', substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(`tail'),
    `;'), -1, `', `$0(defn(`tail'))')')
  define(`chew', `ifelse(eval($1 < 20), 1, `_$0(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  chew(len(defn(`nodes')), defn(`nodes'))
')

define(`move', `ifelse(end($@), `ZZZ', `$3',
  `$0(defn(defn(`d$2')`_$1'), eval(`($2+1)%'''d``), incr($3))')')

# Observations from megathread: all input files are nice: each __A ends in
# exactly one __Z on an exact multiple of the directions string.  No need
# to look for cycles when we can instead look for the first hit of __Z.
#ifdef(`L_AAA', `
#define(`end', `$1')
#define(`part1', move(`AAA', 0, 0))
#', ``missing AAA'')
define(`part1', ``missing AAA'')

define(`_end', `ifelse(`$1', `ZZZ', `define(`part1', `$3')')define(`cycles',
  defn(`cycles')`,$3')`ZZZ'')
define(`end', `ifdef(`z$1', `_$0', `ifelse')($@)')
define(`run', `popdef(`starts')output(1, `$1... 'move(`$1', 0, 0))ifdef(
  `starts', `$0(starts)')')
run(starts)

include(`math64.m4')
# For my input, all cycles happen to be prime multiples of the length of dirs.
# But if inputs have composite-length cycles, I might need to use the
# following lcm/gcd implementations, and possibly code up 64-bit division.
# define(`gcd', `ifelse($2, 0, $1, `$0($2, eval(`$1 % $2'))')')
# define(`lcm', `mul64(eval($1 / gcd($1, $2)), $2)')
define(`check', `ifelse(eval($1%d), 0, `,eval($1/d)',
  `fatal(`off-kilter cycle')')')
define(`part2', mul(d`'foreach(`check'cycles)))

divert`'part1
part2
