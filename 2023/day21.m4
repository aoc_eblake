divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day21.input] day21.m4
# Optionally use -Dstop1=N -Dstop2=N to stop each part after N steps

include(`common.m4')ifelse(common(21), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`input', translit(include(defn(`file')), `#.S'nl, `0123'))
define(`x', 1)define(`y', 1)
define(`do0', `define(`x', incr($1))')
define(`do1', `define(`g$1_$2')do0($@)')
define(`do2', `define(`start', `$@')do1($@)')
define(`do3', `define(`maxx', $1)define(`x', 1)define(`y', incr($2))')

ifdef(`__gnu__', `
  patsubst(defn(`input'), `.', `do\&(x, y)')
',`
  define(`chew', `ifelse($1, 1, `do$2(x, y)', `$0(eval($1/2), substr(
    `$2', 0, eval($1/2)))$0(eval($1-$1/2), substr(`$2', eval($1/2)))')')
  chew(len(defn(`input')), defn(`input'))
')

define(`t0', 1)define(`t1', `0')
define(`p', `ifdef(`g$1_$2', `ifdef(`d$1_$2', `', `addwork($@)')')')
define(`visit', `p(incr($1), $2, $3)p($1, incr($2), $3)p(decr($1), $2,
  $3)p($1, decr($2), $3)')
define(`addwork', `define(`step$3', incr(step$3))define(`d$1_$2')pushdef(`d',
  `d$1_$2')pushdef(`set$3', `visit($1, $2, t$3)')')
define(`_round', `ifdef(`set$1', `set$1()popdef(`set$1')$0($@)')')
define(`round', `_$0(eval(1^$1&1), $1)')
define(`_reset', `ifdef(`d', `popdef(defn(`d'))popdef(`d')$0()',
  `undefine(`set0')undefine(`set1')')')
define(`reset', `_$0()define(`step0', 0)define(`step1', 0)addwork($1, $2, 0)')
define(`pick', `ifelse(eval(($1)&1), 0, `$2'0, `$2'1)')

# Assumption: regular input has nice property of being a square tile, with
# odd number of rows and columns, and with all center and edge row/columns
# being open.  The example input does not meet this property.  I'm not
# interested in solving the example input for part 2, as that's harder.
ifelse(ifdef(`g'first(start)`_'incr(shift(start)), 1), 0, `
ifdef(`stop1', `', `define(`stop1', 6)')
define(`part2', ``detected sample input, skipping part 2'')
reset(start)
forloop_arg(1, stop1, `round')
define(`part1', pick(stop1, `step'))
', eval(maxx != y || first(start) * 2 != y).first(start), 1.shift(start),
`fatal(`unexpected input layout')', `

ifdef(`stop1', `', `define(`stop1', 64)')
ifdef(`stop2', `', `define(`stop2', 26501365)')
reset(start)
forloop_arg(1, stop1, `round')
define(`part1', pick(stop1, `step'))

# Because of the assumptions, each interior tile will be completely
# visited; the problem then boils down to counting how many interior
# tiles there are, plus the partial contributions of each frontier
# tile based on the point where the tile is first reached.
#
#        4      5--1--6
#       847     |..|..|
#      88477    |..|..|
#     333S222   2--S--3
#      66155    |..|..|
#       615     |..|..|
#        1      7--4--8
#
# When maxx/y is 8, there are 7 rows/columns.  Step 0 is at S, step 4 enters
# the four side tiles [1234], step 8 enters the four diagonal tiles [5678],
# step 11 starts the next sides, step 15 starts the next set of diagonal tiles,
# and so on.  For my input, y is 132; starting at S covers all possible
# positions in the tile within y moves; but starting at any corner takes 261
# steps before full coverage.  There are probably inputs with worse cases
# (think of a tight spiral in one quarter), but for now I will only worry
# about the outer two shells, assuming all other shells reached full coverage.
# Because there are an odd number of rows/columns, every other full shell
# alternates between step0 and step1 coverage values.
define(`finish', `round($1)ifelse(step0.step1, $2.$3, $1, `$0(incr($1),
  step0, step1)')')
define(`mid', first(start)) # also y/2
define(`wide', decr(y))
ifelse(eval(finish(incr(stop1), step0, step1)>2*wide || stop2<2*wide+mid), 1,
  `fatal(`unexpected bound')')
define(`full0', step0)define(`full1', step1)
define(`full', `pick($1, `$0')')
include(`math64.m4')

# Contribution from tile S
define(`part2', full(stop2))

# Contribution from tiles 1,2,3,4; occur at mid + wide*N, in row or column
define(`reached', eval((stop2+mid-1)/wide))
define(`part2', add64(part2, mul64(4, add64(mul64(eval(reached/2 - 1),
  eval(full0+full1)), eval((reached&1)*full(1^mid^stop2))))))
define(`limit1', eval((stop2-mid)%wide+1))
define(`limit2', eval(limit1+wide))
define(`addedge', `define(`part2', add64(part2, add64(reset($@)forloop_arg(1,
  limit1, `round')pick(limit2, `step'), forloop_arg(incr(limit1), limit2,
  `round')pick(limit1, `step'))))')
addedge(1, mid)addedge(mid, 1)addedge(mid, wide)addedge(wide, mid)

# Contribution from tiles 5,6,7,8; occur at y + wide*N, on diagonals
define(`dreached', eval((stop2-1)/wide))
define(`part2', add64(part2, mul64(4, add64(mul64(mul64(eval(dreached/2),
  eval((dreached-1)/2)), full(1^dreached^stop2)), mul64(mul64(
  eval((dreached-1)/2), eval((dreached-2)/2)), full(dreached^stop2))))))
define(`limit3', eval((stop2-1)%wide+1))
define(`limit4', eval(limit3+wide))
define(`adddiag', `define(`part2', add64(part2, add64(reset($@)forloop_arg(1,
  limit3, `round')mul64(pick(limit4, `step'), dreached), forloop_arg(incr(
  limit3), limit4, `round')mul64(pick(limit3, `step'), decr(dreached)))))')
adddiag(1, 1)adddiag(1, wide)adddiag(wide, 1)adddiag(wide, wide)

')

divert`'part1
part2
