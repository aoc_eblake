divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day25.input] day25.m4

include(`common.m4')ifelse(common(25), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

# The input contains the string dnl, but no upper case or vowels.  But upper
# is annoying to type, so temporarily hack dnl into a comment so that I can
# use uppercase node names instead.
changecom(`dnl', nl)
define(`input', translit(include(defn(`file')), defn(`alpha')nl` :',
  defn(`ALPHA')`;.'))
changecom(`#', nl)
define(`n', 0)define(`e', 0)
define(`to_n', `ifdef(`n$1', `', `define(`n$1', n)define(`n',
  incr(n))define(`s'n$1)')n$1()')
define(`_join', `define(`c$1', defn(`c$1')`v($1, $2)')')
define(`join', `_$0($1, $2)_$0($2, $1)define(`e', incr(e))')
define(`_do', `_foreach(`join(to_n(`$1'), to_n(', `))', $@)')
define(`do', `_$0(translit(`$1', `.', `,'))')

ifdef(`__gnu__', `
  patsubst(defn(`input'), `\([^;]*\);', `do(`\1')')
', `
  define(`_chew', `do(substr(`$1', 0, index(`$1', `;')))define(
    `tail', substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(`tail'),
    `;'), -1, `', `$0(defn(`tail'))')')
  define(`chew', `ifelse(eval($1 < 140), 1, `_$0(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  chew(len(defn(`input')), defn(`input'))
')

# Find two nodes relatively far apart, maximizing the chance they are on
# opposite sides of the cut. Do this by two BFS searches: one from the
# first node, then one from the last node seen.  It is possible to design
# a graph where this does not work, but not likely in a random setup.
define(`v', `ifdef(`d$2', `', `pushdef(`q', $2)')')
define(`_bfs', `ifdef(`q', `define(`d'q, $1)`c'q``''popdef(`q')$0($1)')')
define(`bfs', `ifdef(`q', `define(`best', q)first(_$0($1))$0(incr($1))')')
pushdef(`q', 0)
bfs(0)
forloop(0, decr(n), `popdef(`d'', `)')
define(`src', best)
pushdef(`q', best)
bfs(0)
define(`dst', best)

# Now perform three BFS tracking the shorted path from src to dst, and
# removing those edges.
define(`v', `ifdef(`e$1_$2', `', `ifdef(`p$2', `', `define(`p$2', $1)pushdef(
  `q', $2)')')')
define(`_bfs', `ifdef(`q', ``c'q``''popdef(`q')$0($1)')')
define(`wipe', `ifelse(`$1', 'src`, `', `define(`e$1_$2')define(`e$2_$1')$0($2,
  defn(`p'$2))')')
define(`round', `define(`p'src)pushdef(`q', src)bfs(0)wipe(dst,
  defn(`p'dst))forloop(0, decr(n), `popdef(`p'', `)')')
round()round()round()

# If src and dst do indeed straddle the min-cut, this final BFS from src
# can only reach its side of the graph.
define(`v', `ifdef(`e$1_$2', `', `ifdef(`s$2', `popdef(`s$2')pushdef(`q',
  $2)')')')
pushdef(`q', src)
bfs(0)
ifdef(`s'dst, `', `fatal(`heuristic for selecting src and dst failed')')
define(`size', len(forloop(0, decr(n), `ifdef(`s'', `, `-')')))
define(`part1', eval(size * (n - size)))

divert`'part1
no part2
