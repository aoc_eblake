divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day12.input] day12.m4
# Optionally use -Dverbose=1 to see some progress

include(`common.m4')ifelse(common(12), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

# Idea for optimization: write this as an NFA:
# https://github.com/ConcurrentCrab/AoC/blob/main/solutions/12-2.go

# Assumes that the longest input line has at most 6 groups

define(`input', translit((include(defn(`file'))), nl`.?# ,()', `;012//'))
include(`math64.m4')
define(`part1', 0)define(`part2', 0)
define(`min', `ifelse(eval($1<$2), 1, $1, $2)')

# lone(len(str), str, arg1)->int # lone arg solution
define(`_lone', `ifelse(eval($1<$5), 1, 0, $3$4, $1-1, `eval($1-$5+1)', $4, -1,
  `eval(lone($3, substr(`$2', 0, $3), $5) + lone(eval($1 - $3 - 1),
  substr(`$2', incr($3)), $5))', eval($4>=$5), 1, `lone(eval($1-$4+$5-1),
  substr(`$2', eval($4-$5+1)), $5)', eval($3<$4), 1, `lone(eval($1-$3+1),
  substr(`$2', incr($3)), $5)', eval($3<$5), 1, 0, index(substr(`$2',
  $5), `2'), -1, `eval(min($4, $3-$5)+1)', $4, 0, 0, `lone(decr($1), substr(
  `$2', 1), $5)')')
define(`lone', `_$0(`$1', `$2', index(`$2'0, `0'), index(`$2', `2'), `$3')')

# check(len(str), str, minarglen, arg1, (arg2, ...argN))->int
define(`_check', `ifelse(index($1, 2), -1, `score(len(`$2'), shift($@))', 0)')
define(`check', `ifelse($3, $4, `lone($1, `$2', $4)', eval(index(`$2'0, 0)<$4),
  1, `_$0(substr(`$2', 0, index(`$2', 0)), substr(`$2', incr(index(`$2', 0))),
  $3, $4, `$5')', substr(`$2', $4, 1), 2, `ifelse(substr(`$2', 0, 1), 2, 0,
  `score(decr($1), substr(`$2', 1), $3, $4, `$5')')', `add64(score(eval(
  $1-$4-1), substr(`$2', incr($4)), eval($3-$4), first$5, (shift$5)),
  ifelse(substr(`$2', 0, 1), 2, `0', `score(decr($1), substr(`$2', 1), $3, $4,
  `$5')'))')')
define(`progress', 0)
ifelse(eval(verbose > 0), 1, `define(`check', defn(`check')`ifelse(eval(progress
  % 10000), 0, `output(1, ...progress)')define(`progress', incr(progress))')')

# score(len(str), str, minarglen, arg1, (arg2, ...argN))->int, memoized
define(`arg', `ifelse(`$1', `', 0, `$#')')
define(`_score', `ifdef(`$1', `', `define(`$1', check(shift($@)))')$1()')
define(`score', `ifelse(eval($3+arg$5>$1 || len(translit(`$2', 01))>$3), 1, 0,
  `_$0(`s$2_$4_'translit(`$5', `(,)', ``_''), $@)')')

# pack(str)->len, str
define(`_pack', `substr(`$1', 0, $2)substr(`$1', incr($2))')
define(`pack', `ifelse(index(`$1', `0'), 0, `$0(substr(`$1', 1))', index(
  `$1'0, `00'), -1, `len($1), `$1'', `$0(_$0(`$1', index(`$1'0, `00')))')')
define(`_run', `define(`part1', eval(part1+$1))define(`part2', add64(part2,
  $2))')
define(`run', `ifelse($3, 1, `_$0(lone(pack($1), $4), score(pack(
  `$1'1`$1'1`$1'1`$1'1`$1'), eval($2*5), $4, ($4,$4,$4,$4)), $1)',
  `_$0(score(pack($1), $2, first($4), (shift($4))),
  score(pack(`$1'1`$1'1`$1'1`$1'1`$1'), eval($2*5), first($4),
  (shift($4),$4,$4,$4,$4)), $1)')')
define(`_do', `run(`$1', eval(`$7 + $6 + $5 + $4 + $3 + $2'),
  decr(`$#'), quote(shift($@)))')
define(`do', `_$0(translit(`$1', `/', `,'))')

ifdef(`__gnu__', `
  patsubst(defn(`input'), `\([^;]*\);', `do(`\1')')
', `
  define(`_chew', `do(substr(`$1', 0, index(`$1', `;')))define(
    `tail', substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(`tail'),
    `;'), -1, `', `$0(defn(`tail'))')')
  define(`chew', `ifelse(eval($1 < 100), 1, `_$0(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  chew(len(defn(`input')), defn(`input'))
')

divert`'part1
part2
