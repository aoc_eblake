divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day19.input] day19.m4

include(`common.m4')ifelse(common(19), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

# The input contains the substring nl, but no upper case other than
# "A" or "R"; "in" as the only node name with lower case vowels, and
# bare "a" for a category.  Rename the macros that would interfere.
define(`Nl', defn(`nl'))undefine(`nl')
define(`Dnl', defn(`dnl'))undefine(`dnl')
define(`errprintn', `errprint(`$1'Nl())')

define(`x', `$`1'')define(`m', `$`2'')define(`a', `$`3'')define(`s', `$`4'')
define(`input', translit((include(defn(`file'))), Nl`,()', `;.'))
popdef(`x')popdef(`m')popdef(`a')popdef(`s')
define(`part1', 0)define(`part2', 0)
define(`R_')
define(`A_', `define(`part1', eval(part1+$1+$2+$3+$4))')
define(`use', `ifelse(`$3', `', `$2_($1)', `ifelse(eval($2), 1, `$3_($1)',
  `$0(`$1', shift(shift(shift($@))))')')')
define(`run', `in_($2,$4,$6,$8)')
define(`do', `run(translit(`$1', `.=${}', `,,'))')
define(`build', `define(`$2_', `use'(``$$1','shift(shift($@))))')
pushdef(`do', `ifelse(`$1', `', `popdef(`$0')', `build(`@', translit(`$1',
  `{:.}', `,,,'))')')

ifdef(`__gnu__', `
  patsubst(defn(`input'), `\([^;]*\);', `do(`\1')')
', `
  define(`_chew', `do(substr(`$1', 0, index(`$1', `;')))define(
    `tail', substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(`tail'),
    `;'), -1, `', `$0(defn(`tail'))')')
  define(`chew', `ifelse(eval($1 < 145), 1, `_$0(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  chew(len(defn(`input')), defn(`input'))
')

include(`math64.m4')
define(`diff', `($3-$2+1)')
define(`A_', `define(`part2', add64(part2, mul64(eval(diff$1*diff$2),
  eval(diff$3*diff$4))))')
define(`cmp', `eval($1$4)eval($2$4), $1, $2, $3')
define(`_fork', `ifelse($3, 00, `,`$1(,$4,$5,$6,)$2'', $3, 11,
  ``$1(,$4,$5,$6,)$2',', $3, 01, ``$1(,incr($7),$5,$6,)$2',`$1(,$4,$7,$6,)$2'',
  ``$1(,$4,decr($7),$6,)$2',`$1(,$7,$5,$6,)$2'')')
define(`fork', `_$0(fork$7($1, $2, $3, $4, $8), substr($8, 1))')
define(`fork1', ``', `,$2,$3,$4', cmp(shift$1$5)')
define(`fork2', ``$1,', `,$3,$4', cmp(shift$2$5)')
define(`fork3', ``$1,$2,', `,$4', cmp(shift$3$5)')
define(`fork4', ``$1,$2,$3,', `', cmp(shift$4$5)')
define(`_use', `ifelse(`$1', `', `', `$3_($1)')use(`$2',
  shift(shift(shift($@))))')
define(`use', `ifelse(`$1', `', `', `$3', `', `$2_($1)', `_$0(fork($1,
  shift$2), shift(shift($@)))')')
in_((,1,4000,1,), (,1,4000,2,), (,1,4000,3,), (,1,4000,4,))

divert`'part1
part2
