divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day17.input] day17.m4
# Optionally use -Dverbose=1 to see some progress
# Optionally use -Dpriority=0|1|2|3|4|5|6 to choose priority queue algorithm
# Optionally use -Dalgo=dijkstra|astar to choose search algorithm

include(`common.m4')ifelse(common(17, 1000003), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

# Some hilarity for Allez Cuisine!  Why implement a priority stack in
# m4, when you can go an order of magnitude slower by doing it in
# shell.  Uses the obligatory "goto" for gluing across languages.
ifelse(defn(`priority'), 6, `
output(1, `Using syscmd for priority queue')
define(`Goto', `syscmd(`$1')ifelse(sysval, 0, `', `fatal(`$2')')')
define(`gOto', `define(`$1', dquote(mkstemp(`$1.XXXXXX')))ifelse($1, `',
  `fatal(`Unable to create temporary file')')m4wrap(`syscmd(`rm -f '$1)')')
gOto(`goto')
gOto(`GOTO')
define(`insert', `Goto(`printf %s\\n "'translit(`\{{$@}}', `{}`'',
  ``'[]')`" >> 'goto, `Unable to insert $* into priority queue')')
define(`pop', `Goto(`sort -t[ -k2,2n 'goto` > 'GOTO` &&
  head -n1 'GOTO` > 'goto, `Unable to pop from priority queue')translit(
  (_include(goto)), `[]''nl``()', ``'')Goto(`tail -n+2 'GOTO` > 'goto,
  `Unable to pop from priority queue')')
define(`clearall', `Goto(: > goto && : > GOTO, `Unable to wipe files')')
', `
# Priorities are fairly close in range; lower overhead of O(n) sorting of
# priority pushdef stack beats complexity of O(log n) min-heap
ifdef(`priority', `', `define(`priority', 2)')
include(`priority.m4')
')
ifdef(`algo', `', `define(`algo', `astar')')

define(`input', translit(include(defn(`file')), nl, `0'))
define(`x', 1)define(`y', 1)
define(`do', `ifelse(`$3', 0, `define(`maxx', $1)define(`x', 1)define(`y',
  incr($2))', `define(`x', incr($1))define(`g$1_$2', $3)')')

ifdef(`__gnu__', `
  patsubst(defn(`input'), `.', `do(x, y, \&)')
',`
  define(`chew', `ifelse($1, 1, `do(x, y, $2)', `$0(eval($1/2), substr(
    `$2', 0, eval($1/2)))$0(eval($1-$1/2), substr(`$2', eval($1/2)))')')
  chew(len(defn(`input')), defn(`input'))
')

# Comparison of heuristics:
# None (aka Dijkstra search): 763k addwork, 170k insert, 158k visit
# Manhattan without current node: 762k addwork, 164k insert, 158k visit
# Manhattan + g$1_$2: 762k addwork, 159k insert, 158k visit
# unconstrained min-path: 608k addwork, 140k insert, 122k visit
# In other words, the extra startup time of running a separate Dijkstra
# to populate our heuristic (23K insert) ultimately makes our A* search
# perform more effectively, for less overall time.
ifelse(defn(`algo'), `dijkstra', `
output(1, `using only Dijkstra search')
define(`dist', `$3')

', defn(`algo'), `astar', `
output(1, `using A* search, finding min-path heuristic')
# Dijkstra search for unconstrained min path to destination.  This
# makes for a more useful heuristic for the A* in dealing with the
# constraints below.
define(`_visit', `ifdef(`g$2_$3', `ifelse(ifdef(`h$2_$3', `eval($1<h$2_$3)', 1),
  1, `define(`h$2_$3', $1)insert($@)')')')
define(`visit', `_$0($1, decr($2), $3)_$0($1, $2, decr($3))_$0($1, incr($2),
  $3)_$0($1, $2, incr($3))')
define(`minpath', `ifelse($1, `', `', `visit(eval($1+g$2_$3), $2, $3)$0(pop)')')
define(`h'decr(maxx)`_'decr(y), 0)
minpath(-defn(`g'decr(maxx)`_'decr(y)), decr(maxx), decr(y))
define(`dist', `eval($3+h$1_$2)')

', `fatal(`unknown value for algo')')

# Rather than explicitly tracking how many steps in a row we have
# moved straight, it's easier to enqueue only the points where we turn
# (up to 6 neighbors in part 1, up to 14 in part 2).  State is then
# x,y coordinate and h/v entry direction.  It's easiest to add a
# point's weight when leaving it; but this would miss the weight of
# the goal and overcount the weight of the first point.  Hack around
# that by counting the goal weight at the beginning of the search.
pushdef(`g1_1', defn(`g'decr(maxx)`_'decr(y)))
define(`prep', `define(`d$1', decr($1))')
forloop_arg(1, 10, `prep')
define(`d0', 0)
# visitD(part, x, y, dist, op, lo, hi)
define(`visith', `ifdef(`g$2_$3', `ifelse($6, 0, `addwork(`$1', $2, $3, `v',
  $4)')ifelse($7, 0, `', `$0(`$1', $5($2), $3, eval($4+g$2_$3), `$5', d$6,
  d$7)')')')
define(`visitv', `ifdef(`g$2_$3', `ifelse($6, 0, `addwork(`$1', $2, $3, `h',
  $4)')ifelse($7, 0, `', `$0(`$1', $2, $5($3), eval($4+g$2_$3), `$5', d$6,
  d$7)')')')

define(`addwork', `ifelse(ifdef(`$1$2_$3$4', `eval($5<$1$2_$3$4)', 1), 1,
  `define(`$1$2_$3$4', `$5')insert(dist($2, $3, $5), $2, $3, `$4', $5,
  `$1$2_$3$4')')')
define(`_round', `ifelse(`$2.$3', 'decr(maxx).decr(y)`, `$5clearall()',
  `ifelse($5, $6, `visit($2, $3, `$4', $5, `incr')visit($2, $3, `$4',
  $5, `decr')')$0(pop)')')
define(`round', `addwork(`$1', 1, 1, `h', 0)addwork(`$1', 1, 1, `v',
  0)_$0(pop)')

define(`progress', 0)
define(`rename', `define(`$2', defn(`$1'))popdef(`$1')')
define(`show0', `output(1, `...$1')rename(`show$1', `show'eval($1+10000))')
define(`show', `ifdef(`$0$1', `$0$1($1)')define(`progress', incr($1))')
ifelse(eval(verbose >1), 1, `define(`_round', `show(progress)'defn(`_round'))')

output(1, `starting part 1 traversal')
define(`visit', `$0$3(`p', $1, $2, $4, `$5', 1, 3)')
define(`part1', round(`p'))

output(1, `starting part 2 traversal')
define(`visit', `$0$3(`P', $1, $2, $4, `$5', 4, 10)')
define(`part2', round(`P'))

divert`'part1
part2
