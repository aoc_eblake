divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day12.input] day12.m4

include(`common.m4')ifelse(common(12), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

# `n'ode name->number, `a'djacent neighbors, `s'mall
define(`count', 0)
define(`_node', `define(`n$1', $2)define(`s$2', eval(len(translit(alpha,
  `$1')) < 26))')
define(`node', `ifdef(`n$1', `', `_$0(`$1', eval(1<<count))define(`count',
  incr(count))')')
define(`adj', `ifelse(`$4', `start', `', `$2', `end', `', `define(`a$1',
  defn(`a$1')`,$3')')')
define(`parse', `ifelse(`$1', `', `', `node(`$1')node(`$2')adj(n$1, `$1',
  n$2, `$2')adj(n$2, `$2', n$1, `$1')$0(shift(shift($@)))')')
parse(translit(include(defn(`file')), -nl, `,,'))
node(`dbl')

define(`_visit', `_foreach(`+cache(', `, $2, $3)', a$1)')
define(`visit', `ifelse($1, 'nend`, 1, eval($2&$1), 0, `0_$0($1,
  eval($2|($1*s$1)), $3)', $3eval($2&'ndbl`), 20, `0_$0($1,
  eval($2|''ndbl``), $3)', 0)')
define(`cache', `ifdef(`c$3_$1_$2', `', `define(`c$3_$1_$2', eval(visit(
  $@)))')c$3_$1_$2`'')
define(`part1', cache(nstart, 0, 1))
define(`part2', cache(nstart, 0, 2))

divert`'part1
part2
