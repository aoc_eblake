divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day18.input] day18.m4
# Optionally use -Dverbose=1 to see some progress
# Optionally use -Dalgo=recurse|flat to choose an algorithm

include(`common.m4')ifelse(common(18), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

ifdef(`algo', `', `define(`algo', `flat')')

define(`input', translit((include(defn(`file'))), nl`,()', `;.'))
define(`cat', `$*')
define(`reduce', `_$0(split(explode(`$1')))')
define(`_reduce', `ifelse($2, 1, `reduce(`$1')', `$1')')

ifelse(algo, `recurse', `
output(1, `Using recursive algorithm')
define(`convert', `translit(`$1', `[.]', `(,)')')

define(`explode', `_$0(exp($1, 0, `', 0, 0))')
define(`_explode', `ifelse($2, 0, `$1', `explode(`$1')')')
define(`exp', `ifelse(index(`$1', `('), -1, `eval($1+$4+$5), $2,0,0', $2$3,
  0----, `0, 1,cat$1', $2$4$5, 100, ``$1', 1,0,0',
  `_$0(cat$1, $2, $3-, $4, $5)')')
define(`_exp', `build(exp(`$1', $3, $4, $5, 0), `$2', $4, $6)')
define(`build', `_$0(`$1', exp(`$5', $2, $6, $4, $7), $3, $6)')
define(`_build', `ifelse($4, 0, `($1,$2), $3,$6,$5', `(first(exp(`$1', 1, $7,
  0, $4)),`$2'), 1,$6,$5')')

define(`split', `spl($1, 0)')
define(`spl', `ifelse($2, 1, `$1, 1', index(`$1', `('), 0, `_$0(cat$1)',
  len(`$1'), 1, `$1, 0', `(eval($1/2),eval(($1+1)/2)), 1')')
define(`_spl', `join(spl(`$1', 0), `$2')')
define(`join', `ifelse($2, 0, `_$0($1, spl(`$3', 0))', `($1,$3), 1')')
define(`_join', `($1,$2), $3')

define(`magnitude', `ifelse(len(`$1'), 1, $1, _$0$1)')
define(`_magnitude', `eval(3*magnitude(`$1')+2*magnitude(`$2'))')
define(`add', `reduce(($1,$2))')

', algo, `flat', `
output(1, `Using flat algorithm')
define(`convert', `flatten(0, translit(`$1', `[.]', `(,)'))')
define(`d0', `~')define(`d1', `!')define(`d2', `@')
define(`d3', `%')define(`d4', `^')define(`d5', `&')
define(`flatten', `ifelse(len(`$2'), 1, `d$1`'1$2', `_$0(incr($1), cat$2)')')
define(`_flatten', `flatten($1, $2)flatten($1, $3)')

define(`explode', `exp(`$1', index(`$1', `&'))')
define(`exp', `ifelse($2, -1, `$1', `_$0(substr(`10$1', 0, $2),
  translit(`eval(AB+DE-10)^10ieval(GH+JK-10)', `ABcDEfGHiJK', substr(`10$1~10',
  $2, 11)), substr(`$1~10', eval($2+9)))')')
define(`_exp', `explode(substr(`$1$2$3', 2, eval(len(`$1$2$3')-5)))')

define(`split', `spl(`$1', index(translit(`$1', `@%^&34567', `!!!!22222'),
  `!2'))')
define(`spl', `ifelse($2, -1, `$1, 0', `_$0(substr(`$1', 0, $2),
  translit(substr(`$1', $2, 1), `!@%^', `@%^&'), substr(`$1', incr($2), 2),
  substr(`$1', eval($2+3))), 1')')
define(`_spl', `$1$2eval(($3+10)/2)$2eval(($3+11)/2)$4')

define(`magnitude', `pushdef(`t', `0,0')_m(`$1')t()')
define(`_m', `ifelse(`$1', `', `',
  `translit(`pr(A,C)_m(`DEFGHIJKLMNOPQRSTUVWXYZabcdefghijklnoqstuvwxy')',
  `ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklnoqstuvwxy', `$1')')')
define(`pr', `_$0(t, translit(`$1', `!@%^', `1234'), $2)')
define(`_pr', `ifelse($3, 0, `define(`t', `$4popdef(`t')')', $1, $3,
  `popdef(`t')$0(t, decr($3), eval(3*$2+2*$4))', `pushdef(`t', `$3,$4')')')
define(`add', `reduce(translit($1`'$2, `!@%^', `@%^&'))')

', `errprintn(`unrecognized algorithm 'algo)m4exit(1)')

define(`count', 0)
define(`line', `define(`l'count, convert(`$1'))define(`count', incr(count))')
ifdef(`__gnu__', `
  patsubst(defn(`input'), `\([^;]*\);', `line(`\1')')
', `
  define(`_chew', `line(substr(`$1', 0, index(`$1', `;')))define(
    `tail', substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(`tail'),
    `;'), -1, `', `$0(defn(`tail'))')')
  define(`chew', `ifelse(eval($1 < 120), 1, `_$0(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  chew(len(defn(`input')), defn(`input'))
')

define(`root', defn(`l0'))
define(`merge', `define(`root', add(`root', `l$1'))')
forloop_arg(1, decr(count), `merge')
define(`part1', magnitude(root))

define(`part2', 0)
define(`compare', `ifelse(eval($1>part2), 1, `define(`part2', $1)')')
define(`_try', `ifelse($1, $2, `', `compare(magnitude(add(`l$1', `l$2')))')')
define(`try', `output(1, `...$1')forloop(0, decr(count), `_$0(', `, $1)')')
forloop_arg(0, decr(count), `try')

divert`'part1
part2
