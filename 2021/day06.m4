divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day06.input] day06.m4
# Optionally use -Dverbose=1 to see some progress
# Optionally use -Dalgo=full|sparse to choose an algorithm

include(`common.m4')ifelse(common(06), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

ifdef(`algo', `', `define(`algo', `sparse')')

include(`math64.m4')
forloop(0, 8, `define(`l'', `, 0)')
define(`bump', `define(`l$1', incr(l$1))')
foreach(`bump', translit((include(defn(`file'))), nl`()'))

ifelse(algo, `full', `
output(1, `Using full matrix multiply, O(log n)')
define(`_prep', `define(`m1_$1_$2', 0)')
define(`prep', `forloop(0, 8, `_$0($1,', `)')')
forloop_arg(0, 8, `prep')
define(`prep', `define(`m1_$1', 1)')
foreach(`prep', `0_6', `0_8', `1_0', `2_1', `3_2', `4_3', `5_4', `6_5',
  `7_6', `8_7')

define(`_dot', `, mul64(m$1_$3_$5, m$2_$5_$4)')
define(`dot', `define(`m$3_$4_$5', add(0forloop(0, 8, `_$0($1, $2, $4, $5, ',
  `)')))')
define(`_mult', `forloop(0, 8, `dot($1, $2, $3, $4, ', `)')')
define(`mult', `output(1, $3)forloop(0, 8, `_$0($1, $2, $3, ', `)')')
mult(1, 1, 2)
mult(2, 2, 4)
mult(1, 4, 5)
mult(5, 5, 10)
mult(10, 10, 20)
mult(20, 20, 40)
mult(40, 40, 80)
define(`_prod', `, m$1_$2_$3')
define(`prod', `, mul64(l$2, add(0forloop(0, 8, `_$0($1, $2, ', `)')))')
define(`part1', add(0forloop(0, 8, `prod(80, ', `)')))
define(`scale', `mult(eval(1<<$1), eval(1<<$1), eval(2<<$1))')
forloop_arg(2, 7, `scale')
define(`part2', add(0forloop(0, 8, `prod(256, ', `)')))

', algo, `sparse', `
output(1, `Using sparse matrix lookup, O(n)')
define(`prep', `define(`c0_$1', l$1)')
forloop_arg(0, 8, `prep')
define(`iter', `_$0(decr($1), $1)')
define(`_iter', `forloop(1, 8, `bump(', `, $1, $2)')define(`c$2_8',
  c$1_0)define(`c$2_6', add64(c$2_6, c$1_0))popdef(`c$1_0')')
define(`bump', `define(`c$3_'decr($1), c$2_$1)popdef(`c$2_$1')')
define(`_tally', `,c$1_$2')
define(`tally', `add(0forloop(0, 8, `_$0($1, ', `)'))')
forloop_arg(1, 80, `iter')
define(`part1', tally(80))
forloop_arg(81, 256, `iter')
define(`part2', tally(256))

', `errprintn(`unrecognized algorithm 'algo)m4exit(1)')

divert`'part1
part2
