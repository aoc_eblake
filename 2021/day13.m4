divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day13.input] day13.m4

include(`common.m4')ifelse(common(13), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`input', translit((include(defn(`file'))), nl`,fold along()', `;.'))
define(`offset', incr(index(defn(`input'), `;;')))
define(`points', substr(defn(`input'), 0, offset))
define(`folds', substr(defn(`input'), incr(offset)))
define(`count', 0)
define(`point', `define(`count', incr(count))define(`p$1_$2')pushdef(`p0',
  `$1,$2')')
ifdef(`__gnu__', `
  patsubst(defn(`points'), `\([^.]*\)\.\([^;]*\);', `point(`\1', `\2')')
', `
  define(`visit', `point(translit($1, `.', `,'))')
  define(`_chew', `visit(substr(`$1', 0, index(`$1', `;')))define(
    `tail', substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(`tail'),
    `;'), -1, `', `$0(defn(`tail'))')')
  define(`chew', `ifelse(eval($1 < 20), 1, `_$0(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  chew(len(defn(`points')), defn(`points'))
')
define(`swap', `popdef(`p$1_$2')ifdef(`p$3_$4', `define(`count', decr(count))',
  `define(`p$3_$4')')`$3,$4'')
define(`foldx', `ifelse(eval($1 > $3), 1, `swap($1, $2, eval(2*$3-$1), $2)',
  ``$1,$2'')')
define(`foldy', `ifelse(eval($2 > $3), 1, `swap($1, $2, $1, eval(2*$3-$2))',
  ``$1,$2'')')
define(`fold', `ifdef(`p$1', `pushdef(`p$2', $0$3(p$1, $4))popdef(
  `p$1')$0($@)', `define(`last$3', $4)')')
define(`hook', `define(`part1', count)pushdef(`hook')')
define(`do', `ifelse(`$2', `', `', `fold($1, incr($1), `$2', $3)hook()$0(
  incr($1), shift(shift(shift($@))))')')
do(0, translit(defn(`folds'), `=;', `,,'))
include(`ocr.m4')
define(`char', `forloop(0, 5, `_$0(', `, $1)')')
define(`_char', `forloop($2, eval($2+4), `ifdef(`p'', ``_$1', ``X'', `` '')')')
define(`part2', forloop(0, 7, `ocr(char(eval(', `*5)))'))

divert`'part1
part2
