divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day09.input] day09.m4

include(`common.m4')ifelse(common(09), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`input', translit(include(defn(`file')), nl, `;'))
define(`width', incr(index(defn(`input'), `;')))
define(`g', `g$1()')define(`idx', width)
forloop(0, decr(width), `define(`g'', `, 9)')
define(`do', `ifelse(`$2', `;', `define(`g$1', 9)', `define(`g$1',
  `$2')ifelse(eval($2<g(decr($1)) && $2<g(eval($1-''width``))), 1,
  `pushdef(`v', $1)')')define(`idx', incr(idx))')
ifdef(`__gnu__', `
  patsubst(defn(`input'), `.', `do(idx, `\&')')
',`
  define(`chew', `ifelse($1, 1, `do(idx, `$2')', `$0(eval($1/2), substr(
    `$2', 0, eval($1/2)))$0(eval($1-$1/2), substr(`$2', eval($1/2)))')')
  chew(len(defn(`input')), defn(`input'))
')
forloop(idx, eval(idx+width), `define(`g'', `, 9)')
define(`part1', 0)define(`b1', 0)define(`b2', 0)define(`b3', 0)
define(`rank', `ifelse(eval($1>$2), 1, `define(`b3', $3)define(`b2',
  $2)define(`b1', $1)', eval($1>$3), 1, `define(`b3', $3)define(`b2',
  $1)', eval($1>$4), 1, `define(`b3', $1)')')
define(`_basin', `basin($1, g($1))')
define(`basin', `ifelse($2, 9, `', `ifdef(`s$1', `', `-define(`s$1')_$0(
  incr($1))_$0(decr($1))_$0(eval($1-''width``))_$0(eval($1+''width``))')')')
define(`check', `ifelse(eval($2<g(incr($1)) && $2<g(eval($1+'width`))), 1,
  `define(`part1', eval(part1+1+$2))rank(len(basin($1, $2)), b1, b2, b3)')')
define(`loop', `ifdef(`v', `check(v, g(v))popdef(`v')loop()')')
loop()define(`part2', eval(b1*b2*b3))

divert`'part1
part2
