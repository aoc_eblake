divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day14.input] day14.m4
# Optionally use -Dverbose=1 to see some progress
# Optionally use -Dalgo=full|sparse to choose an algorithm

include(`common.m4')ifelse(common(14), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

ifdef(`algo', `', `define(`algo', `sparse')')

include(`math64.m4')
define(`elts', `BCFHKNOPSV')
define(`input', translit(include(defn(`file')), nl` -', `;'))
define(`offset', index(defn(`input'), `;;'))
define(`rules', substr(defn(`input'), eval(offset+2)))
define(`bump', `ifdef(`n$1$2', `define(`n$1$2', add64(n$1$2, $3))', `define(
  `n$1$2', $3)pushdef(`n$1', `$2')')')
define(`init', `ifelse(len(`$1'), 1, `bump(0, `$1_', 1)define(`r$1_',
  `bump($'`1, `$1_', 1)')define(`last', `$1')', `bump(0, substr(`$1', 0, 2),
  1)$0(substr(`$1', 1))')')
init(substr(defn(`input'), 0, offset))
define(`rule', `define(`r$1$2',
  `bump($'`1, `$1$3', $'`2)bump($'`1, `$3$2', $'`2)')')
define(`visit', `translit(`rule(`1', `2', `3')', `12>3', `$1')')
ifdef(`__gnu__', `
  patsubst(defn(`rules'), `\([^;]*\);', `visit(`\1')')
', `
  define(`_chew', `visit(substr(`$1', 0, index(`$1', `;')))define(
    `tail', substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(`tail'),
    `;'), -1, `', `$0(defn(`tail'))')')
  define(`chew', `ifelse(eval($1 < 10), 1, `_$0(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  chew(len(defn(`rules')), defn(`rules'))
')

ifelse(algo, `full', `
output(1, `Using full matrix multiply, O(log n)')
define(`_prep', `define(`m1_$1_$2', 0)')
define(`prep', `forloop(0, 99, `_$0($1,', `)')')
forloop_arg(0, 99, `prep')
define(`map', `eval(index(defn(`elts'), substr(`$1', 0, 1)) * len(defn(`elts'))
  + index(defn(`elts'), substr(`$1', 1)))')
define(`bump', `define(`m1_$1_'map(`$2'), 1)')
define(`_prep_', `ifdef(`r$1', `r$1($2)')')
define(`_prep', `$0_(substr(defn(`elts'), $1, 1)`'substr(defn(`elts'), $2, 1),
  eval($1*len(defn(`elts'))+$2))')
define(`prep', `forloop(0, decr(len(defn(`elts'))), `_$0($1,', `)')')
forloop_arg(0, decr(len(defn(`elts'))), `prep')

define(`_dot', `, mul64(m$1_$3_$5, m$2_$5_$4)')
define(`dot', `define(`m$3_$4_$5', add(0forloop(0, 99, `_$0($1, $2, $4, $5, ',
  `)')))')
define(`_mult', `forloop(0, 99, `dot($1, $2, $3, $4, ', `)')')
define(`mult', `output(1, $3)forloop(0, 99, `_$0($1, $2, $3, ', `)')')
mult(1, 1, 2)
mult(2, 2, 4)
mult(1, 4, 5)
mult(5, 5, 10)
define(`bump', `define(`c$1', add64(c$1, $2))')
define(`_comp', `ifelse(`$1', 0, `', `ifelse(min, 0, `define(`min', $1)',
  lt64($1, min), 1, `define(`min', $1)')ifelse(lt64(max, $1), 1,
  `define(`max', $1)')')')
define(`comp', `_$0(defn(`c'substr(defn(`elts'), $1, 1)))')
define(`prod', `bump(substr(defn(`elts'), eval($4/len(defn(`elts'))), 1),
  mul64($3, m$1_$2_$4))')
define(`_tally', `ifelse(`$2', defn(`last')`_', `bump(defn(`last'), 1)',
  `forloop(0, 99, `prod($1, 'map(`$2')`, 'n0$2`, ', `)')')')
define(`tally', `define(`min', 0)define(`max', 0)forloop(0, decr(len(defn(
  `elts'))), `define(`c'substr(defn(`elts'), ', `, 1), 0)')_stack_foreach(`n0',
  `_$0($1, ', `)', `t')forloop_arg(0, decr(len(defn(`elts'))),
  `comp')add64(max, -min)')
define(`part1', tally(10))
mult(10, 10, 20)
mult(20, 20, 40)
define(`part2', tally(40))

', algo, `sparse', `
output(1, `Using sparse matrix lookup, O(n)')
define(`use', `r$1($3, n$2$1)popdef(`n$2$1')')
define(`_round', `ifdef(`n$1', `use(defn(`n$1'), $1, $2)popdef(`n$1')$0($@)')')
define(`round', `_$0(decr($1), $1)')
define(`_comp', `ifelse(`$1', `', `', `ifdef(`min', `ifelse(lt64($1, min), 1,
  `define(`min', $1)')ifelse(lt64(max, $1), 1, `define(`max', $1)')',
  `define(`min', $1)define(`max', $1)')')')
define(`comp', `_$0(defn(`c'substr(defn(`elts'), $1, 1)))')
define(`collect', `define(`c$1', ifdef(`c$1', `add64(c$1, $2)', $2))')
define(`_tally', `ifdef(`n$2$1', `collect(substr(`$1', 0, 1), n$2$1)')')
define(`tally', `popdef(`min')popdef(`max')forloop(0, decr(len(defn(`elts'))),
  `popdef(`c'substr(defn(`elts'), ', `, 1))')_stack_foreach(`n$1', `_$0(',
  `, $1)', `t')forloop_arg(0, decr(len(defn(`elts'))), `comp')')
forloop_arg(1, 10, `round')
tally(10)define(`part1', eval(max-min))
forloop_arg(11, 40, `round')
tally(40)define(`part2', add64(max, -min))

', `errprintn(`unrecognized algorithm 'algo)m4exit(1)')

divert`'part1
part2
