divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day24.input] day24.m4

include(`common.m4')ifelse(common(24), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`M')define(`A', `,$3')
define(`_1', `pushdef(`part1')pushdef(`part2')pair($2,')define(`_26', `,$1)')
define(`P', `_$3($4, $8)')
define(`E1', `ifelse(len($1), 2, 9, $1)')
define(`E2', `ifelse(len($1), 2, 1, incr($1))')
define(`build', `$4$2$1$3')
define(`pair', `define(`part1', build(part1, E1(eval(9 - $1 - $3)), E1(eval(
  9 + $1 + $3)), popdef(`part1')defn(`part1')))define(`part2', build(part2,
  E2(eval(-$1 - $3)), E2(eval($1 + $3)), popdef(`part2')defn(`part2')))')
E1(translit(include(defn(`file')), ` 'nl`wxyz', `,)'define(`mul',
  `M(')define(`mod', `M(')define(`eql', `M(')define(`add', `A(')define(`div',
  `A(')define(`inp', `)P((')))

ifelse(`
dnl golfed version of part 1, requires GNU m4, 178 bytes
define(D,defn(define))D(div)D(_1,`q($2,')D(_26,`,$1)')D(q,`E(eval(9-$1- $3))$2E(eval(9+$1+$3))')D(E,`ifelse(len($1),2,9,$1)')E(translit(include(f),p n
i,(,P,)D(P,_$14($17,$47))))
dnl golfed version of part 1, POSIX m4, 225 bytes
define(D,defn(`define'))D(M)D(A,`,$3')D(_1,`p($2,')D(_26,`,$1)')D(P,_$3($4,$8))D(p,`E(eval(9-$1- $3))$2E(eval(9+$1+$3))')D(E,`ifelse(len($1),2,9,$1)')E(translit(include(f),v q
deilopuwxyz,`a,m)'D(n,`)P((')D(a,`A(')D(m,`M(')))
dnl golfed version of part 2, GNU m4, 222 bytes
define(D,defn(define))D(M)D(A,`,$3')D(_1,`p($2,')D(_26,`,$1)')D(P,_$3($4,$8))D(p,`E(eval(-$1- $3))$2E(eval($1+$3))')D(E,`ifelse(len($1),2,1,incr($1))')M(translit(include(f),v mnq
d-z,`a,mnm)'D(n,`)P((')D(a,`A(')D(m,`M(')))
')

divert`'part1
part2
