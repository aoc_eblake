divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day05.input] day05.m4

include(`common.m4')ifelse(common(05), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`input', translit((include(defn(`file'))), nl`, >()', `;-'))
ifdef(`__gnu__', `
  patsubst(defn(`input'), `\([^;]*\);', `pushdef(`line', `\1')')
', `
  define(`_chew', `pushdef(`line', substr(`$1', 0, index(`$1', `;')))define(
    `tail', substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(`tail'),
    `;'), -1, `', `$0(defn(`tail'))')')
  define(`chew', `ifelse(eval($1 < 40), 1, `_$0(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  chew(len(defn(`input')), defn(`input'))
')
define(`canon', `ifelse(eval($1 < $2), 1, `$1, $2', `$2, $1')')
define(`point', `ifdef(`p$1_$2', `ifdef(`P$1_$2', `', `define(`P$1_$2')define(
  `part$3', incr(part$3))')', `define(`p$1_$2')')')
define(`processx', `forloop($2, $3, `point($1, ', `, $4)')')
define(`processy', `forloop($2, $3, `point(', `, $1, $4)')')
define(`process', `ifelse($1, $3, `$0x($1, canon($2, $4), $5)', $2, $4,
  `$0y($2, canon($1, $3), $5)', `pushdef(`diag', `$1-$2-$3-$4')')')
define(`do', `ifdef(`$1', `process(translit($1, `-', `,'), $2)popdef(
  `$1')$0($@)')')
define(`part1', 0)do(`line', 1)
define(`cmp', `ifelse(eval($1 < $2), 1, ``incr'', ``decr'')')
define(`processd', `point($1, $2, 2)ifelse($1, $3, `',
  `$0($4($1), $5($2), $3, `$4', `$5')')')
define(`process', `processd($1, $2, $3, cmp($1, $3), cmp($2, $4))')
define(`part2', part1)do(`diag', 2)

divert`'part1
part2
