divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day11.input] day11.m4

include(`common.m4')ifelse(common(11), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`input', translit(include(defn(`file')), nl))
define(`part1', 0)define(`idx', 0)
define(`parse', `define(`g'idx, $1)define(`idx', incr(idx))')
ifdef(`__gnu__', `
  patsubst(defn(`input'), `.', `parse(`\&')')
',`
  define(`chew', `ifelse($1, 1, `parse(`$2')', `$0(eval($1/2), substr(
    `$2', 0, eval($1/2)))$0(eval($1-$1/2), substr(`$2', eval($1/2)))')')
  chew(len(defn(`input')), defn(`input'))
')
define(`_prep', `define(`n$1', `b($2)b($3)b($4)b($5)b($6)b($7)b($8)b($9)')')
define(`prep', `_$0($1, eval($1-11), eval($1-10), eval($1-9), decr($1),
  incr($1), eval($1+9), eval($1+10), eval($1+11))')
forloop_arg(11, 88, `prep')
define(`_prep', `define(`n$1', `b($2)b($3)b($4)b($5)b($6)')')
define(`prep', `_$0($1, decr($1), incr($1), eval($1+9), eval($1+10),
  eval($1+11))_$0(eval($1*10), eval($1*10-10), eval($1*10-9), eval($1*10+1),
  eval($1*10+10), eval($1*10+11))_$0(eval($1*10+9), eval($1*10-2),
  eval($1*10-1), eval($1*10+8), eval($1*10+18), eval($1*10+19))_$0(eval($1+90),
  eval($1+79), eval($1+80), eval($1+81), eval($1+89), eval($1+91))')
forloop_arg(1, 8, `prep')
define(`n0', `b(1)b(10)b(11)')define(`n9', `b(8)b(18)b(19)')
define(`n90', `b(80)b(81)b(91)')define(`n99', `b(88)b(89)b(98)')

define(`_b', `define(`g$1', incr($2))ifelse($2, 9, `pushdef(`f', $1)n$1()')')
define(`b', `_$0($1, g$1)')
define(`flash', `ifdef(`f', `-define(`g'f, 0)popdef(`f')flash()')')
define(`round', `use($1, forloop_arg(0, 99, `b')len(flash()))')
define(`use', `define(`part1', eval(part1+$2))')
forloop_arg(1, 100, `round')
define(`use', `ifelse($2, 100, `pushdef(`round')define(`part2', $1)',
  `round(incr($1))')')
round(101)

divert`'part1
part2
