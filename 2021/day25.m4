divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day25.input] day25.m4
# Optionally use -Dverbose=1 to see some progress

include(`common.m4')ifelse(common(25), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`input', translit(include(defn(`file')), `.>v'nl, `0123'))
define(`x', 0)define(`y', 0)
define(`v', `ifdef(`$1$2_$3', `', `define(`$1$2_$3')pushdef(`$1$4',
  `$2,$3,l$2,u$3,$5')')')
define(`visit', `define(`g$1_$2', $3)define(`x', incr($1))')
define(`visit0', `visit($1, $2, 0)')
define(`visit1', `v(`e', $1, $2, 0, `r'$1)visit($1, $2, 1)')
define(`visit2', `v(`s', $1, $2, 0, `d'$2)visit($1, $2, 2)')
define(`visit3', `define(`y', incr($2))define(`x', 0)')
pushdef(`visit3', `define(`width', $1)popdef(`$0')$0($@)')
ifdef(`__gnu__', `
  patsubst(defn(`input'), `.', `visit\&(x, y)')
',`
  define(`chew', `ifelse($1, 1, `visit$2(x, y)', `$0(eval($1/2), substr(
    `$2', 0, eval($1/2)))$0(eval($1-$1/2), substr(`$2', eval($1/2)))')')
  chew(len(defn(`input')), defn(`input'))
')
define(`prep', `define(`$2$1', decr($1))define(`$3$1', incr($1))')
forloop(0, decr(width), `prep(', `, `l', `r')')
define(`l0', decr(width))define(`r'decr(width), 0)
forloop(0, decr(y), `prep(', `, `u', `d')')
define(`u0', decr(y))define(`d'decr(y), 0)

define(`p', `ifdef(`$1$2', `p$1($1$2, $3, $4)popdef(`$1$2')p($@)')')
define(`pe', `popdef(`e$1_$2')ifelse(g$5_$2, 0, `pushdef(`M', `define(`g$1_$2',
  0)define(`g$5_$2', 1)v(`e', $5, $2, $7, r$5)'ifelse(g$1_$4, 2,
  ``v(`s', $1, $4, $6, $2)'', g$3_$2, 1, ``v(`e', $3, $2, $7, $1)''))')')
define(`ps', `popdef(`s$1_$2')ifelse(g$1_$5, 0, `pushdef(`M', `define(`g$1_$2',
  0)define(`g$1_$5', 2)v(`s', $1, $5, $7, d$5)'ifelse(g$3_$2, 1,
  ``v(`e', $3, $2, $6, $1)'', g$1_$4, 2, ``v(`s', $1, $4, $7, $2)''))')')
define(`m', `ifdef(`M', `M()popdef(`M')m()')')
define(`round', `ifelse(ifdef(`e$1', `', 0)ifdef(`s$1', `', 0), 00, $1,
  `$0(_$0($1, incr($1)))')')
define(`_round', `ifelse(eval($2%10), 0, `output(1, `...$2')')p(`e', $1,
  $1, $2)m()p(`s', $1, $2, $2)m()$2')
define(`part1', round(0))

divert`'part1
no part2
