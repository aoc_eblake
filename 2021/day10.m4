divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day10.input] day10.m4

include(`common.m4')ifelse(common(10), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

# Mismatched () are evil. Assume that the first '(' will come before any ')',
# and abuse changecom to read in everything long enough to translit it.
# For that to work, we can't use common.m4's include() wrapper.
exists(defn(`file'), `', `errprintn(`Could not locate file $1')m4exit(1)')
define(`input', translit(_include(defn(`file')changecom(`(',
  nl-))-changecom`'changecom(`#'), `([{<>}])'nl-, `ABCDEFGH;'))
include(`math64.m4')
define(`part1', 0)define(`count', 0)
define(`sH', 1)
define(`sG', 2)
define(`sF', 3)
define(`sE', 4)
define(`incomplete', `ifdef(`s', `$0(add64(mul64($1, 5), defn(`s's))popdef(
  `s'))', `$1')')
define(`score', `define(`arr'count, $1)define(`count', incr(count))')
define(`illegal', `ifelse(s, `$1', `popdef(`s')', `define(`part1',
  eval(part1+$2))pushdef(`do', defn(`bad'))')')
define(`bad', `ifelse(`$1', `;', `popdef(`$0')undefine(`s')')')
define(`checkA', `pushdef(`s', `H')')
define(`checkB', `pushdef(`s', `G')')
define(`checkC', `pushdef(`s', `F')')
define(`checkD', `pushdef(`s', `E')')
define(`checkH', `illegal(`H', `3')')
define(`checkG', `illegal(`G', `57')')
define(`checkF', `illegal(`F', `1197')')
define(`checkE', `illegal(`E', `25137')')
define(`do', `ifelse(`$1', `;', `score(incomplete(0))', `check$1()')')
ifdef(`__gnu__', `
  patsubst(defn(`input'), `.', `do(`\&')')
',`
  define(`chew', `ifelse($1, 1, `do(`$2')', `$0(eval($1/2), substr(
    `$2', 0, eval($1/2)))$0(eval($1-$1/2), substr(`$2', eval($1/2)))')')
  chew(len(defn(`input')), defn(`input'))
')
define(`swap', `define(`arr$1', arr$2`'define(`arr$2', arr$1))')
define(`_partition', `ifelse(lt64(x, arr$1), 1, `swap(i, $1)define(`i',
  incr(i))')')
define(`partition', `define(`x', arr$2)define(`i', $1)forloop_arg($1, decr($2),
  `_$0')swap(i, $2)')
define(`kth', `partition($1, $2)ifelse(eval(i - $1 == $3 - 1), 1,
  `defn(`arr'i)', eval(i - $1 > $3 - 1), 1, `kth($1, decr(i), $3)',
  `kth(incr(i), $2, eval($3 - i + $1 - 1))')')
define(`part2', kth(0, decr(count), eval(count/2 + 1)))

divert`'part1
part2
