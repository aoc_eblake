divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day02.input] day02.m4

include(`common.m4')ifelse(common(02), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`x', 0)define(`y1', 0)define(`y2', 0)
translit(include(defn(`file')), ` 'nl, `()'
  define(`forward', `define(`x', eval(x+$1))define(`y2', eval(y2+$1*y1))')
  define(`up', `define(`y1', eval(y1-$1))')
  define(`down', `define(`y1', eval(y1+$1))'))
define(`part1', eval(x*y1))
define(`part2', eval(x*y2))

ifelse(`dnl golfing variants of parts 1, 2:
define(d,defn(define))d(x)d(y)translit(include(f),` '
forwdp,()d(a,`d(`x',x+$1)')d(u,`d(`y',y-$1)')d(n,`d(`y',y+$1)'))eval((x)*(y))
define(d,defn(define))d(x)d(y)d(z,0)translit(include(f),` '
forwdp,()d(a,`d(`x',x+$1)d(`y',y+$1*(z))')d(u,`d(`z',z-$1)')d(n,`d(`z',z+$1)'))eval((x)*(y))
')

divert`'part1
part2
