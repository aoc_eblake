divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day08.input] day08.m4

include(`common.m4')ifelse(common(08), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`input', translit(include(defn(`file')), nl, `;'))
define(`_reset', `define(`l$1', 0)popdef(`d$1')popdef(`s$1')popdef(
  `n'm$1)popdef(`m$1')')
define(`reset', `define(`state', 0)forloop_arg(0, 9, `_$0')define(`buf')')
reset()forloop(11, 14, `define(`l'', `, 0)')
define(`part1', 0)define(`part2', 0)

define(`dolet', `define(`buf', defn(`buf')`+$2')define(`l$1', incr(l$1))')
define(`doa', `dolet($1, `1')')
define(`dob', `dolet($1, `10')')
define(`doc', `dolet($1, `100')')
define(`dod', `dolet($1, `1000')')
define(`doe', `dolet($1, `10000')')
define(`dof', `dolet($1, `100000')')
define(`dog', `dolet($1, `1000000')')
define(`do0', `define(`d$1', eval(buf))define(`buf')')
forloop(1, 9, `define(`do'', `, defn(`do0'))')
define(`map', `define(`n$1', $2)define(`m$2', $1)')
define(`pass1', `_$0(l$1, d$1)')
define(`_pass1', `ifelse($1, 2, `map($2, 1)', $1, 3, `map($2, 7)', $1, 4,
  `map($2, 4)', $1, 7, `map($2, 8)')')
define(`pass2', `define(`s2', _$0($1, 6))define(`s5', _$0($1, 4))define(
  `s6', _$0($1, 9))define(`s3', eval(m1-s6))define(`s4', eval(m4-s2-s3-s6))')
define(`_pass2', `substr(1000000, 0, eval(7-index($1, $2)))')
define(`pass3', `_$0(l$1, d$1)')
define(`_pass3', `ifelse($1, 5, `$0$1($2)', $1, 6, `$0$1($2)')')
define(`_pass35', `ifelse(eval(!(`0x$1&0x's5)), 0, `map($1, 2)',
  eval(!(`0x$1&0x's2)), 0, `map($1, 5)', `map($1, 3)')')
define(`_pass36', `ifelse(eval(`0x$1&0x's4), 0, `map($1, 0)',
  eval(`0x$1&0x's3), 0, `map($1, 6)', `map($1, 9)')')
define(`do10', `forloop_arg(0, 9, `pass1')pass2(eval(forloop(0, 9, `+defn(`d'',
  `)')))forloop_arg(0, 9, `pass3')')
define(`use', `ifelse(index(`023569', $1), -1, `define(`part1',
  incr(part1))')define(`part2', eval(part2+$1substr(`___________000', $2)))')
define(`do11', `use(defn(`n'eval(buf)), $1)define(`buf')')
forloop(12, 14, `define(`do'', `, defn(`do11'))')
define(`do', `ifelse(`$2', `;', `do$1($1)reset()', `$2', ` ', `do$1($1)define(
  `state', incr($1))', `$2', `|', `', `do$2($1)')')

ifdef(`__gnu__', `
  patsubst(defn(`input'), `.', `do(state, `\&')')
',`
  define(`chew', `ifelse($1, 1, `do(state, `$2')', `$0(eval($1/2), substr(
    `$2', 0, eval($1/2)))$0(eval($1-$1/2), substr(`$2', eval($1/2)))')')
  chew(len(defn(`input')), defn(`input'))
')

ifelse(`dnl golfing variant of part 1:
eval(define(d,defn(define))translit(include(f),b-g|d(aa,+1)d(aaa,+1)d(aaaa,+1)d(aaaaa)d(aaaaaa)d(aaaaaaa,+1)d(x,-4),aaaaaax))
dnl the same in sh
# cut -b61-<f|tr \  \\n|egrep -c "^...?.?.{5}?$"
')

divert`'part1
part2
