divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day17.input] day17.m4
# Optionally use -Dverbose=1 to see some progress

include(`common.m4')ifelse(common(17), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`parse', `define(`minx', `$2')define(`maxx', $4)define(`miny',
  $6)define(`maxy', $8)')
parse(translit((include(defn(`file'))), `=.()'nl, `,,'))
define(`triangle', `eval(($1)*(($1)+1)/2)')
define(`lowx', `ifelse(eval(triangle($1) >= minx), 1, $1, `$0(incr($1))')')
define(`x', lowx(1))
ifelse(eval(triangle(x) <= maxx), 1, `define(`part1', triangle(-1 - miny))')
define(`part2', 0)
define(`_good', `ifelse(eval($1 > maxx || $2 < miny), 1, `0',
  eval($1 >= minx && $1 <= maxx && $2 <= maxy && $2 >= miny), 1, `$5',
  eval($3 == 0 && $1 < minx), 1, `0', `$0(eval($1 + $3), eval($2 + $4),
  ifelse($3, 0, 0, decr($3)), decr($4), incr($5))')')
define(`good', `define(`t', _$0(0, 0, $@, 0))ifelse(t, 0, `', `define(`part2',
  incr(part2))ifdef(`part1', `', `define(`best', $2)')')output(1, `$1, $2: 't)')
define(`try', `forloop(x, maxx, `good(', `, $1)')')
forloop_arg(miny, eval(-1 - miny), `try')
ifdef(`best', `define(`part1', triangle(best))')

divert`'part1
part2
