divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day20.input] day20.m4
# Optionally use -Dverbose=1 to see some progress

include(`common.m4')ifelse(common(20), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`input', translit(include(defn(`file')), nl`.#', `;01'))
ifdef(`__gnu__', `
  define(`parse', `patsubst(`$1', `.', `prep(`\&')')')
',`
  define(`chew', `ifelse($1, 1, `prep(`$2')', `$0(eval($1/2), substr(
    `$2', 0, eval($1/2)))$0(eval($1-$1/2), substr(`$2', eval($1/2)))')')
  define(`parse', `chew(len(`$1'), `$1')')
')
define(`count', 0)
define(`prep', `define(`a'eval(count, 2, 9), $1)define(`count', incr(count))')
parse(substr(defn(`input'), 0, 512))
define(`a0', a000000000)define(`a1', a111111111)
ifelse(eval(a0 ^ a1), 1, `', `fatal(`unexpected rules')')
define(`offset', 51)
define(`x', offset)define(`y', offset)
define(`prep', `ifelse(`$1', `;', `define(`x', 'offset`)define(`y', incr(y))',
  `define(`g0_'x`_'y, $1)define(`x', incr(x))')')
parse(substr(defn(`input'), 514))
define(`width', eval(y-x))

define(`do', `forloop($1, $2, `_$0(', `, $@)')')
define(`_do', `forloop($2, $3, `$4($1, ', `, `$5', $6)')')
define(`prep', `define(`g1_$1_$2', $3)')
do(0, eval(offset*2+width), `prep', a0)
define(`prep', `ifdef(`g0_$1_$2', `', `define(`g0_$1_$2', 0)')_$0(`$3',
  decr($1), $1, incr($1), decr($2), $2, incr($2))')
define(`_prep', `define(`n$3_$6', `defn(`a'g$1_$2_$5()g$1_$3_$5()''dnl
``g$1_$4_$5()g$1_$2_$6()g$1_$3_$6()g$1_$4_$6()g$1_$2_$7()''dnl
``g$1_$3_$7()g$1_$4_$7())')')
do(1, eval(offset*2+width-1), `prep', `$'1)

define(`round', `output(1, `...$1')do(eval('offset`-$1),
  eval('offset+width`+$1-1), `point', eval($1%2), eval(($1+1)%2))')
define(`D', defn(`define'))
define(`point', `D(`g$3_$1_$2', n$1_$2($4))')
define(`tally', `len(do(eval(offset-$1), eval(offset+width+$1-1), `_$0'))')
define(`_tally', `ifelse(g0_$1_$2, 1, `-')')
round(1)round(2)
define(`part1', tally(2))
forloop_arg(3, 50, `round'))
define(`part2', tally(50))

divert`'part1
part2
