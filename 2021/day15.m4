divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dhashsize=H] [-Dfile=day15.input] day15.m4
# Optionally use -Dverbose=1 to see some progress
# Optionally use -Dpriority=0|1|2|3|4|5 to choose priority queue algorithms

include(`common.m4')ifelse(common(15, 1000003), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

# Pending priorities fall in narrow range
ifdef(`priority', `', `define(`priority', 2)')
include(`priority.m4')

define(`input', translit(include(defn(`file')), nl, `;'))
define(`width', index(defn(`input'), `;'))
define(`x', 0)define(`y', 0)
define(`do', `ifelse(`$3', `;', `define(`y', incr($2))define(`x', 0)',
  `define(`g$1_$2', $3)define(`x', incr($1))')')
ifdef(`__gnu__', `
  patsubst(defn(`input'), `.', `do(x, y, `\&')')
',`
  define(`chew', `ifelse($1, 1, `do(x, y, `$2')', `$0(eval($1/2), substr(
    `$2', 0, eval($1/2)))$0(eval($1-$1/2), substr(`$2', eval($1/2)))')')
  chew(len(defn(`input')), defn(`input'))
')

define(`_g', `eval(1 + (defn(`g'eval($1%'width`)`_'eval(
  $2%'width`)) - 1 + $1/'width` + $2/'width`) % 9)')
define(`g', `ifdef(`g$1_$2', `g$1_$2()', `_g($1, $2)')')
define(`edge1', decr(width))
define(`edge2', eval(5*width-1))
define(`count', 0)
define(`addwork', `define(`d$1_$2_$3', $4)insert($4, $1, $2, $3)define(`count',
  incr(count))ifelse(eval(count%10000), 0, `output(1, ...count)')')
define(`distance', `addwork($1, 0, 0, 0)loop(pop, edge$1)clearall()')
define(`loop', `ifelse(`$3,$4', `$5,$5', $1, `neighbors($2, $3, $4,
  d$2_$3_$4, $5)loop(pop, $5)')')
define(`check', `ifdef(`d$1_$2_$3', `', `addwork($1, $2, $3,
  eval($4 + g($2, $3)))')')
define(`neighbors', `ifelse($2, 0, `', `check($1, decr($2), $3, $4)')ifelse($3,
  0, `', `check($1, $2, decr($3), $4)')ifelse($2, $5, `', `check($1,
  incr($2), $3, $4)')ifelse($3, $5, `', `check($1, $2, incr($3), $4)')')
define(`part1', distance(1))
define(`part2', distance(2))

divert`'part1
part2
