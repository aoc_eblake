divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dpacket=HEX | -Dfile=day16.input] day16.m4

include(`common.m4')ifelse(common(16), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

include(`math64.m4')
ifdef(`packet', `define(`input', defn(`packet'))',
  `define(`input', translit(include(defn(`file')), nl))')
ifdef(`__gnu__', `
  patsubst(defn(`input'), `.', `pushdef(`hex', `\&')')
',`
  define(`chew', `ifelse($1, 1, `pushdef(`hex', `$2')', `$0(eval($1/2), substr(
    `$2', 0, eval($1/2)))$0(eval($1-$1/2), substr(`$2', eval($1/2)))')')
  chew(len(defn(`input')), defn(`input'))
')
define(`_h2b', `pushdef(`bits', $4)pushdef(`bits', $3)pushdef(`bits',
  $2)pushdef(`bits', $1)')
define(`h2b', `_$0(translit(`A,B,C,D', `ABCD', eval(`0x'$1, 2, 4)))')
define(`loop', `ifdef(`hex', `h2b(hex)popdef(`hex')loop()')')
loop()define(`part1', 0)
define(`remain', eval(len(defn(`input'))*4))define(`need', 0)
define(`pop', `define(`remain', decr(remain))bits`'popdef(`bits')')
define(`pop3', `eval(pop*4+pop*2+pop)')
define(`pop4', `eval(pop*8+pop*4+pop*2+pop)')
define(`pop11', `eval((pop3<<8)+(pop4<<4)+pop4)')
define(`pop15', `eval((pop3<<12)+(pop4<<8)+(pop4<<4)+pop4)')

define(`_parse', `parse$1(pop)')
define(`parse', `define(`part1', eval(part1+pop3))_$0(pop3)define(`need',
  decr(need))ifelse(cond, `popdef(`cond')popdef(`need')', `,parse()')')
define(`parselit', `ifelse($1, 1, `$0(pop, add64(mul64(16, $2), pop4))', $2)')
define(`parseop', `ifelse($1, 0, `parselen(pop15)', `parsecnt(pop11)')')
define(`parse0', `add(parseop($1))')
define(`parse1', `mul(parseop($1))')
define(`parse2', `min(parseop($1))')
define(`min', `ifelse(`$2', `', $1, `$0(ifelse(lt64($1, $2), 1, $1, $2),
  shift(shift($@)))')')
define(`parse3', `max(parseop($1))')
define(`max', `ifelse(`$2', `', $1, `$0(ifelse(lt64($2, $1), 1, $1, $2),
  shift(shift($@)))')')
define(`parse4', `parselit($1, pop4)')
define(`parse5', `gt(parseop($1))')
define(`gt', `lt64($2, $1)')
define(`parse6', `lt64(parseop($1))')
define(`parse7', `ifelse(parseop($1), 1, 0)')
define(`parsecnt', `pushdef(`cond', `need,0')pushdef(`need', $1)parse()')
define(`parselen', `pushdef(`cond', `remain,'eval(remain-$1))pushdef(`need',
  remain)parse()')
define(`part2', parsecnt(1))

divert`'part1
part2
