divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day22.input] day22.m4
# Optionally use -Dverbose=1 to see some progress

include(`common.m4')ifelse(common(22), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

include(`math64.m4')

define(`count', 0)
define(`bump', `define(`ia$1', $2)define(`ix$1', `$3,$4')define(`iy$1',
  `$5,$6')define(`iz$1', `$7,$8')define(`count', incr($1))')
define(`inst', `ifelse(eval(len($2$3$4$5$6$7) <= 18), 1, `define(`mark',
  count)')bump(count, $@)')
define(`Off', `inst(0, $1, incr($3), $4, incr($6), $7, incr($9))')
define(`On', `inst(1, $1, incr($3), $4, incr($6), $7, incr($9))')
translit((include(defn(`file'))), `o .'nl`xyz=', `O(,)')

define(`progress', 0)
define(`push', `ifelse(eval(progress % 1000), 0, `output(1,
  `...'progress)')define(`progress', incr(progress))ifelse(`$1', `t',
  `_$0(defn(`listt'),', `pushdef(')`list$1', `$2,$3,$4,$5,$6,$7,$8')')
define(`_push', `ifelse(`$1', `-$3', `popdef(`$2')', `-$1', `$3',
  `popdef(`$2')', `pushdef(`$2', `$3')')')
define(`min', `ifelse(eval($1 < $2), 1, $1, $2)')
define(`max', `ifelse(eval($1 > $2), 1, $1, $2)')
define(`range', `max($1, $3), min($2, $4)')
define(`intersect', `_$0($1, $3, range(ix$2, $4, $5), range(iy$2, $6, $7),
  range(iz$2, $8, $9))')
define(`_intersect', `ifelse(eval($4>$3 && $6>$5 && $8>$7), 1,
  `push(`t', eval(-1*$2), $3, $4, $5, $6, $7, $8, $1)')')
define(`touch', `eval(_$0(ix$1, ix$2) && _$0(iy$1, iy$2) && _$0(iz$1, iz$2))')
define(`_touch', `($4>=$1 && $3<=$2)')
define(`visit', `ifelse($1, 0, `', `forloop(0, decr($1), `_$0(',
  `, $1)')')ifelse(ia$1, 1, `push($1, 1, ix$1, iy$1, iz$1)')')
define(`_visit', `ifelse(touch($1, $2), 1, `_stack_foreach(`list$1',
  `intersect($1, $2, first(', `))', `t')stack_reverse(`listt', `list$1')')')
define(`tally', `ifdef(`list$2', `$0(add64($1, _$0(list$2()popdef(
  `list$2'), $2)), $2, $3)', `ifelse($2, $3, `$1', `$0($1, incr($2), $3)')')')
define(`_tally', `mul64(eval($1 *($3 - $2)), mul64(eval($5 - $4),
  eval($7 - $6)))')

# Assume all part1 values come first in the input file, up to the marked line
forloop_arg(0, mark, `visit')
define(`part1', tally(0, 0, mark))
output(1, `...part 1 done')
ifelse(mark, count, `', `forloop_arg(incr(mark), decr(count), `visit')')
output(1, `...tallying part 2')
define(`part2', tally(part1, mark, count))

divert`'part1
part2
