divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day01.input] day01.m4

include(`common.m4')ifelse(common(01), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`part1', 0)define(`part2', 0)
define(`input', translit(include(defn(`file')), nl, `;'))
ifdef(`__gnu__', `
  patsubst(defn(`input'), `\([^;]*\);', `pushdef(`v', `\1')')
', `
  define(`_chew', `pushdef(`v', substr(`$1', 0, index(`$1', `;')))define(
    `tail', substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(`tail'),
    `;'), -1, `', `$0(defn(`tail'))')')
  define(`chew', `ifelse(eval($1 < 10), 1, `_$0(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  chew(len(defn(`input')), defn(`input'))
')
define(`bumpif', `ifelse(eval($1 > $2), 1, `define(`$3', incr($3))')')
define(`do', `bumpif($1, $2, `part1')bumpif($2, $3, `part1')_$0($@,
  popdef(`v')v)')
define(`_do', `bumpif($3, $4, `part1')bumpif($1, $4, `part2')popdef(
  `v')ifdef(`v', `$0($2, $3, $4, v)')')
do(v, popdef(`v')v, popdef(`v')v)

ifelse(`dnl golfing variants of parts 1, 2:
eval(define(m,`ifelse($2,,,+($2>$1)`m(shift($@))')')m(translit(include(f),`'
,`,')))
eval(define(m,`ifelse($4,,,+($4>$1)`m(shift($@))')')m(translit(include(f),`'
,`,')))
')

divert`'part1
part2
