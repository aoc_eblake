divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day07.input] day07.m4

include(`common.m4')ifelse(common(07), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`input', translit((include(defn(`file'))), nl`,()', `;;'))
define(`sum', 0)define(`count', 0)define(`min', 9999)define(`max', -1)
define(`visit', `define(`sum', eval(sum+$1))define(`count', incr(
  count))ifdef(`v$1', `define(`v$1', incr(v$1))', `pushdef(`v', $1)define(
  `v$1', 1)ifelse(eval($1 < min), 1, `define(`min', $1)')ifelse(eval($1 > max),
  1, `define(`max', $1)')')')
ifdef(`__gnu__', `
  patsubst(defn(`input'), `\([^;]*\);', `visit(`\1')')
', `
  define(`_chew', `visit(substr(`$1', 0, index(`$1', `;')))define(
    `tail', substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(`tail'),
    `;'), -1, `', `$0(defn(`tail'))')')
  define(`chew', `ifelse(eval($1 < 10), 1, `_$0(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  chew(len(defn(`input')), defn(`input'))
')
define(`score', `eval(_stack_foreach(`v', `dist($1, ', `)'))')
define(`dist', `+_$0(translit(eval($1 - $2), -))*v$2')
define(`search', `_$0($2($1), `$2', score($2($1)))')
define(`_search', `ifelse(eval($3 <= best), 1, `define(`best',
  $3)search($1, `$2')')')
define(`find', `define(`best', score($1))search($1, `decr')search($1,
  `incr')best')

define(`idx', 0)
define(`try', `ifdef(`v$1', `ifdef(`med', `', `ifelse(eval(idx + v$1 >=
  count / 2), 1, `define(`med', $1)', `define(`idx', eval(idx + v$1))')')')')
forloop_arg(min, max, `try')
define(`_dist', `$1')
define(`part1', find(med))

define(`_dist', `($1*($1+1)/2)')
define(`part2', find(eval(sum / count)))

divert`'part1
part2
