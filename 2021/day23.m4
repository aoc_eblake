divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dhashsize=H] [-Dfile=day23.input] day23.m4
# Optionally use -Dverbose=1 to see some progress
# Optionally use -Dalgo=dfs|dijkstra|astar to choose search algorithm
# Optionally use -Dpriority=0|1|2|3|4|5 to choose priority queue algorithms

include(`common.m4')ifelse(common(23, 1000003), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

include(`priority.m4')
ifdef(`algo', `', `define(`algo', `astar')')

define(`init', translit(include(defn(`file')), `ABCD .#'nl, `1234'))

# At most 28 possible valid moves in a turn, board can be represented as:
# AB_C_D_E_FG
#   H I J K
#   L M N O
#   P Q R S
#   T U V W
define(`count', 0)
define(`r1', `HLPT')
define(`r2', `IMQU')
define(`r3', `JNRV')
define(`r4', `KOSW')
define(`setup', `_$0(count, $@)')
define(`_setup', `define(`p$1', `$2, `$3', `$4', 'dquote(defn(
  `r$5'))`, $5, $6')define(`count', incr($1))')
setup(   0,    `B', `A', 1, 2)
setup(    ,     `', `B', 1, 1)
setup(    ,     `', `C', 1, 1)
setup(   0,    `C', `D', 1, 3)
setup(  00,   `CD', `E', 1, 5)
setup( 000,  `CDE', `F', 1, 7)
setup(0000, `CDEF', `G', 1, 8)
setup(  00,   `BC', `A', 2, 4)
setup(   0,    `C', `B', 2, 3)
setup(    ,     `', `C', 2, 1)
setup(    ,     `', `D', 2, 1)
setup(   0,    `D', `E', 2, 3)
setup(  00,   `DE', `F', 2, 5)
setup( 000,  `DEF', `G', 2, 6)
setup( 000,  `BCD', `A', 3, 6)
setup(  00,   `CD', `B', 3, 5)
setup(   0,    `D', `C', 3, 3)
setup(    ,     `', `D', 3, 1)
setup(    ,     `', `E', 3, 1)
setup(   0,    `E', `F', 3, 3)
setup(  00,   `EF', `G', 3, 4)
setup(0000, `BCDE', `A', 4, 8)
setup( 000,  `CDE', `B', 4, 7)
setup(  00,   `DE', `C', 4, 5)
setup(   0,    `E', `D', 4, 3)
setup(    ,     `', `E', 4, 1)
setup(    ,     `', `F', 4, 1)
setup(   0,    `F', `G', 4, 2)
define(`s01', 1)
define(`s02', 10)
define(`s03', 100)
define(`s04', 1000)
define(`s10', 1)
define(`s20', 10)
define(`s30', 100)
define(`s40', 1000)
define(`v1C', `translit(`eval((A>1)+(B>1)+(H>1)+(L>1)+(P>1)+(T>1) >= 3)',
  'dquote(defn(`ALPHA'))`, $1)')
define(`x', `+($1!=0&&$1!=2)')
define(`v2D', `translit(`eval(x(A)x(B)x(C)x(I)x(M)x(Q)x(U) >= 4)',
  'dquote(defn(`ALPHA'))`, $1)')
define(`y', `+($1!=0&&$1!=3)')
define(`v3D', `translit(`eval(y(E)y(F)y(G)y(J)y(N)y(R)y(V) >= 4)',
  'dquote(defn(`ALPHA'))`, $1)')
define(`v4E', `translit(`eval(!!(F%4)+!!(G%4)+!!(K%4)+!!(O%4)+!!(S%4)+!!(W%4)
  >= 3)', 'dquote(defn(`ALPHA'))`, $1)')

ifelse(algo, `dfs', `
output(1, `Using unsorted depth-first-search')
define(`addwork', `define(`d$1', `$2')neighbors($1, $2)')
define(`_distance', `d00000001234123412341234$1')

', algo, `dijkstra', `
output(1, `Using Dijkstra search')
define(`addwork', `define(`d$1', `$2')insert($2, $1)')
define(`_distance', `loop(pop, $1)clearall()')
define(`loop', `ifelse($2, `00000001234123412341234$3', $1, `neighbors($2,
  d$2)loop(pop, $3)')')

', algo, `astar', `
output(1, `Using A* search')
define(`setup', `_$0(index(-ALPHA, `$1'), $2, $3, $4, $5, $6)')
define(`_setup', `define(`h$1'0, $2)define(`h$1'1, $3)define(`h$1'2,
  $4)define(`h$1'3, $5)define(`h$1'4, $6)')
setup(`A', 0, 3, 50, 700, 9000)
setup(`B', 0, 2, 40, 600, 8000)
setup(`C', 0, 2, 20, 400, 6000)
setup(`D', 0, 4, 20, 200, 4000)
setup(`E', 0, 6, 40, 200, 2000)
setup(`F', 0, 8, 60, 400, 2000)
setup(`G', 0, 9, 70, 500, 3000)
setup(`H', 0, 0, 40, 600, 8000)
setup(`I', 0, 4, 0, 400, 6000)
setup(`J', 0, 6, 40, 0, 4000)
setup(`K', 0, 8, 60, 400, 0)
setup(`L', 1, 0, 51, 701, 9001)
setup(`M', 10, 15, 0, 510, 7010)
setup(`N', 100, 107, 150, 0, 5100)
setup(`O', 1000, 1009, 1070, 1500, 0)
setup(`P', 2, 0, 62, 802, 10002)
setup(`Q', 20, 26, 0, 620, 8020)
setup(`R', 200, 208, 260, 0, 6200)
setup(`S', 2000, 2010, 2080, 2600, 0)
setup(`T', 3, 0, 73, 903, 11003)
setup(`U', 30, 37, 0, 730, 9030)
setup(`V', 300, 309, 370, 0, 7300)
setup(`W', 3000, 3011, 3090, 3700, 0)

define(`heur', `translit(`eval(h1A+h2B+h3C+h4D+h5E+h6F+h7G+h8H+h9I+h10J+
  h11K+h12L+h13M+h14N+h15O+h16P+h17Q+h18R+h19S+h20T+h21U+h22V+h23W)',
  'dquote(defn(`ALPHA'))`, `$1')')
define(`addwork', `define(`d$1', `$2')_$0(eval($2 + heur($1)), $1)')
define(`_addwork', `define(`f$2', $1)insert($@)')
define(`_distance', `loop(pop, $1)clearall()')
define(`loop', `ifelse($2, `00000001234123412341234$3', $1, ifdef(`f$2',
  `eval($1 > f$2)', 0), 1, `loop(pop, $3)', `neighbors($2, d$2)loop(pop, $3)')')

', `output(0, `unknown search algorithm')m4exit(1)')

define(`drop')
define(`check', `_$0(p$1, $2, $3)')
define(`_check', `path($1, translit(`$2, $3$4', 'dquote(defn(`ALPHA'))`, $7),
  `$3', `$4', $5, $6, $7, $8)')
define(`path', `ifelse($1, $2, `_$0(translit(`V, W, X, Y, Z', `VWXYZ', $3),
  $6), `$4', `$5', $7, $8, $9)')')
define(`_path', `ifelse($1$2$3$4$5, $6`0000', `use(4,$1,0', $1$2$3$4$5,
  $6`000'$6, `use(3,$1,0', $1$2$3$4$5, $6`00'$6$6, `use(2,$1,0', $1$2$3$4$5,
  $6`0'$6$6$6, `use(1,$1,0', $1$2$3$4$5, 00000, `drop(', $1$2$3$4$5, 0000$6,
  `drop(', $1$2$3$4, 0000, `use(4,0,$5', $1$2$3$4$5, 000$6$6, `drop(',
  $1$2$3, 000, `use(3,0,$4', $1$2$3$4$5, 00$6$6$6, `drop(', $1$2, 00,
  `use(2,0,$3', $1$2$3$4$5, 0$6$6$6$6, `drop(', $1, 0, `use(1,0,$2', `drop(')')
define(`use', `pushdef(`n'ifelse($3, 0, 0, 1), `_$0(translit(''dquote(dquote(
  defn(`ALPHA')))``, translit(``0$1'', 01234, `$4$5')''dquote(dquote(
  defn(`ALPHA')))``, $3$2$7), eval($8+($1+$6)*s$2$3), `$3$4')')')
define(`_use', `ifelse(ifdef(`v$3', `v$3($1)', 0), 1, `', ifdef(`d$1',
  `eval($2 < d$1)', 1), 1, `addwork($1, $2)show(progress)')')
define(`progress', 0)
define(`rename', `define(`$2', defn(`$1'))popdef(`$1')')
define(`show1000', `output(1, `...$1')rename(`show$1', `show'eval($1+1000))')
define(`show', `ifdef(`$0$1', `$0$1($1)')define(`progress', incr($1))')
define(`neighbors', forloop(0, decr(count), ``check('', ``, $'`1, $'`2)'')dnl
`first(ifdef(`n0', `defn(`n0')undefine(`n0')undefine(`n1')', `_$0()'))')
define(`_neighbors', `ifdef(`n1', `defn(`n1')popdef(`n1')$0()')')
define(`distance', `output(1, `...part $2')addwork(`0000000$1$2', 0)_$0($2)')
define(`part1', distance(init`'12341234, 1))
define(`part2', distance(translit(`IJKL43214213MNOP', `IJKLMNOP', init), 2))

divert`'part1
part2
