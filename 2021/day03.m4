divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day03.input] day03.m4

include(`common.m4')ifelse(common(03), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`input', translit(include(defn(`file')), nl, `;'))
define(`width', index(defn(`input'), `;'))
define(`max', eval((1<<width)-1))
ifdef(`__gnu__', `
  define(`b2d', `eval(`0b$1')')
  patsubst(defn(`input'), `\([^;]*\);', `define(`v'eval(`0b\1'))')
', `
  define(`b2d', `_$0(0, `$1')')define(`_b2d', `ifelse(`$2', `', `$1',
    `$0(eval($1*2+substr(`$2', 0, 1)), substr(`$2', 1))')')
  define(`half', `eval($1/'width`/2*'width`)')
  define(`chew', `ifelse($1, 'width`, `define(`v'b2d(`$2'))', `$0(half($1),
    substr(`$2', 0, half($1)))$0(eval($1-half($1)), substr(`$2', half($1)))')')
  chew(eval(len(defn(`input'))/(width+1)*width), translit(defn(`input'), `;'))
')
define(`bump', `define(`$1', incr($1))')
define(`search', `define(`z', 0)define(`o', 0)forloop($1, $2, `_$0(', `, $3)')')
define(`_search', `ifdef(`v$1', `bump(ifelse(eval($1&$2), 0, ``z'', ``o''))')')

define(`bit', `search(0, 'max`, (1<<('width`-$1)))eval(o > z)')
define(`do', `eval($1*(max^$1))')
define(`part1', do(b2d(forloop_arg(1, width, `bit'))))

define(`bit', `search(lo, hi, (1<<('width`-$1)))ifelse(eval($2), 1,
  `define(`lo', eval(lo+(1<<(''width``-$1))))',
  `define(`hi', eval(hi-(1<<(''width``-$1))))')')
define(`do', `define(`lo', 0)define(`hi', max)forloop(1, width,
  `bit(', `, `$1')')lo')
define(`part2', eval(do(`o>=z') * do(`o&&(!z||z>o)')))

ifelse(`dnl golfing variant of part 1:
define(d,defn(pushdef))d(B,`d(`$1',1defn(`$1'))')d(b,`B(`x'y)a()')d(a,`B(`y')')d(c,`B(`z')d(`y')')patsubst(translit(include(f),01
,abc),.,\&())popdef(`y')d(l,`ifelse($2,y,$1*(`0b'y^$1),`l(($1+$1+(len(x$2)>len(z)/2)),1$2)')')eval(l(0))
')

divert`'part1
part2
