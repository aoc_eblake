#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>

#define MAX 100 // cheat by pre-inspecting input

int
main (int argc, char **argv)
{
  int rows = 40;
  if (argc > 1)
    rows = atoi (argv[1]);
  int safe = 0;
  bool grid[2][MAX + 2] = { 0 }; // true for trap
  int c;
  int width = 0;
  while ((c = getchar ()) != '\n') {
    if (c == '^')
      grid[0][width + 1] = true;
    width++;
  }
  for (int i = 0; i < rows; i++)
    for (int j = 1; j <= width; j++) {
      if (!grid[i & 1][j])
	safe++;
      grid[!(i & 1)][j] = grid[i & 1][j - 1] ^ grid[i & 1][j + 1];
    }
  printf ("found %d safe tiles in %d rows\n", safe, rows);
  return 0;
}
