divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day8.input] day8.m4

include(`common.m4')ifelse(common(8), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`prep', `forloop(0, 5, `define(`d$1_'', `)')')
forloop_arg(0, 49, `prep')
define(`set', `forloop(0, decr($2), `define(`d$1_'', `, 1)')')
define(`Rect', `forloop(0, decr($2), `set(', `, $3)')')
define(`Row', `first(forloop(0, 49, `_$0(', `, $3, $5)'))')
define(`_Row', ``define(`d$1_$2', 'defn(`d'eval((50+$1-$3)%50)`_$2')`)'')
define(`Col', `first(forloop(0, 5, `_$0(', `, $4, $6)'))')
define(`_Col', ``define(`d$2_$1', 'defn(`d$2_'eval((6+$1-$3)%6))`)'')
translit(include(defn(`file')), `x= 'nl, `,,,)'define(`rect',
  `Rect(')define(`row', `Row(')define(`column', `Col('))
define(`cnt', `forloop(0, 5, `defn(`d$1_'', `)')')
define(`part1', len(forloop_arg(0, 49, `cnt')))
include(`ocr.m4')
define(`char', `forloop(0, 5, `_$0(', `, $1)')')
define(`_char', `forloop($2, eval($2+4), `ifelse(defn(`d'', ``_$1'), 1,
  ``X'', `` '')')')
define(`part2', forloop(0, 9, `ocr(char(eval(', `*5)))'))

divert`'part1
part2
