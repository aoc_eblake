#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <ctype.h>

#define COLS 50
#define ROWS 6
bool grid[ROWS][COLS];
bool row[COLS];
bool col[ROWS];

long long
grow (const char *str, int n)
{
  long long count = 0;
  const char *p = str;
  while (p < str + n) {
    if (*p++ == '(') {
      int x, y, l;
      if (sscanf (p, "%dx%d)%n", &x, &y, &l) != 2)
	exit (1);
      p += l;
      count += y * grow (p, x);
      p += x;
    } else {
      count++;
    }
  }
  return count;
}

int main(int argc, char **argv)
{
  long long count = 0;
  int c;
  while (!isspace (c = getchar ())) {
    if (c == '(') {
      int x, y;
      if (scanf ("%dx%d)", &x, &y) != 2)
	return 1;
      char *s = malloc (x);
      fread (s, 1, x, stdin);
      count += y * grow (s, x);
      free (s);
    } else
      count++;
  }
  printf ("decompressed length: %lld\n", count);
  return 0;
}
