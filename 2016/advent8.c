#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <ctype.h>

#define COLS 50
#define ROWS 6
bool grid[ROWS][COLS];
bool row[COLS];
bool col[ROWS];

int main(int argc, char **argv)
{
  ssize_t nread;
  size_t len = 0;
  char *line = NULL;
  int count = 0;
  int i, j;
  while ((nread = getline(&line, &len, stdin)) >= 0) {
    char *end;
    int x, y;
    count++;
    if (!strncmp (line, "rect ", 5)) {
      x = strtol (line + 5, &end, 10);
      y = strtol (end + 1, NULL, 10);
      for (i = 0; i < y; i++)
	for (j = 0; j < x; j++)
	  grid[i][j] = true;
    } else if (!strncmp (line, "rotate row y=", 13)) {
      x = strtol (line + 13, &end, 10);
      y = strtol (end + 3, NULL, 10);
      for (i = 0; i < COLS; i++)
	row[i] = grid[x][i];
      for (i = 0; i < COLS; i++)
	grid[x][(i + y) % COLS] = row[i];
    } else { // rotate column x=
      x = strtol (line + 16, &end, 10);
      y = strtol (end + 3, NULL, 10);
      for (i = 0; i < ROWS; i++)
	col[i] = grid[i][x];
      for (i = 0; i < ROWS; i++)
	grid[(i + y) % ROWS][x] = col[i];
    }
  }
  int lit = 0;
  for (i = 0; i < ROWS; i++) {
    for (j = 0; j < COLS; j++) {
      putchar (grid[i][j] ? '#' : ' ');
      if (grid[i][j])
	lit++;
    }
    putchar ('\n');
  }
  printf ("after %d instructions, %d pixels are lit\n", count, lit);
  return 0;
}
