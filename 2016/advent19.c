#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <ctype.h>

#define MAX 3005290
bool list[MAX];

int
main (int argc, char **argv)
{
  int max = MAX;
  if (argc > 1)
    max = atoi (argv[1]);
  memset (list, true, max);
  int cur = 0, steal = max / 2 - 1, i, j;
  for (i = 0; i < max - 1; i++) {
    if (i && !(i % 20))
      printf ("\b\b\b\b\b\b\b\b\b%9d", i), fflush (stdout);
    for (j = steal + 1; j < steal + max; j++)
      if (list[j % max]) {
	if (max < 10)
	  printf ("position %d taking from %d\n", cur + 1, (j % max) + 1);
	list[j % max] = false;
	steal = j % max;
	break;
      }
    if ((i & 1) ^ (max & 1))
      for (j = steal + 1; j < steal + max; j++)
	if (list[j % max]) {
	  steal = j % max;
	  break;
	}
    for (j = cur + 1; j < cur + max; j++)
      if (list[j % max]) {
	cur = j % max;
	break;
      }
  }
  printf ("\na group of %d ends on position %d\n", max, cur + 1);
  return 0;
}
