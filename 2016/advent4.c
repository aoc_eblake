#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <ctype.h>

int main(void)
{
  ssize_t nread;
  size_t len = 0;
  char *line = NULL;
  int sum = 0;
  while ((nread = getline(&line, &len, stdin)) >= 0) {
    line[nread - 1] = '\0';
    int freq[26] = { 0 };
    int count = 0;
    int id = 0;
    char *p = line;
    while (*p != '[') {
      if (isalpha (*p)) {
	freq[*p - 'a']++;
	if (freq[*p - 'a'] > count)
	  count = freq[*p - 'a'];
      } else if (isdigit (*p))
	id = id * 10 + *p - '0';
      p++;
    }
    p++;
    for (int i = 0; *p != ']'; i++) {
      if (i == 26) {
	count--;
	i = 0;
      }
      if (freq[i] == count)
	if (*p++ != i + 'a') {
	  id = 0;
	  if (getenv ("DEBUG"))
	    printf ("room %s is bogus\n", line);
	  break;
	}
    }
    if (*p == ']') {
      if (getenv ("DEBUG"))
	printf ("room %s is valid\n", line);
      p = line;
      while (!isdigit (*p)) {
	if (*p == '-')
	  *p = ' ';
	else
	  *p = 'a' + (*p - 'a' + id) % 26;
	p++;
      }
      *p = '\0';
      printf ("valid rooms include %d: %s\n", id, line);
    }
    sum += id;
  }
  printf ("sum of ids is %d\n", sum);
  return 0;
}
