divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day6.input] day6.m4

include(`common.m4')ifelse(common(6), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`list', translit(include(defn(`file')), alpha`'nl, ALPHA`_'))
define(`bump', `define(`$1', incr(0defn(`$1')))')
define(`n', 0)
define(`visit', `ifelse(`$1', `_', `define(`n', 0)', `bump(`$1'n)bump(`n')')')
ifdef(`__gnu__', `
  patsubst(defn(`list'), `.', `visit(`\&')')
',`
  define(`split', `ifelse($1, 1, `visit(`$2')', `$0(eval($1/2), substr(`$2',
    0, eval($1/2)))$0(eval($1 - $1/2), substr(`$2', eval($1/2)))')')
  split(len(defn(`list')), defn(`list'))
')
define(`n', index(defn(`list'), `_'))
define(`find', `define(`mv', 0)forloop(0, 25, `_$0(substr(ALPHA, ', `, 1),
  $1)')ml`'')
define(`_find', `ifelse(eval(defn(`$1$2') + 0 > mv), 1, `define(`ml',
  `$1')define(`mv', $1$2)')')
define(`part1', translit(forloop_arg(0, decr(n), `find'), ALPHA, alpha))
define(`find', `define(`mv', 1000)forloop(0, 25, `_$0(substr(ALPHA, ', `, 1),
  $1)')ml`'')
define(`_find', `ifdef(`$1$2', `ifelse(eval($1$2 < mv), 1, `define(`ml',
  `$1')define(`mv', $1$2)')')')
define(`part2', translit(forloop_arg(0, decr(n), `find'), ALPHA, alpha))

divert`'part1
part2
