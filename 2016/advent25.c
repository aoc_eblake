#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <ctype.h>

int regs[4];
int cache[4];
char *program[50]; // sized based on pre-inspecting file

int
get (const char *value)
{
  if (isalpha(*value))
    return regs[*value - 'a'];
  return atoi (value);
}

void
set (const char *reg, int value)
{
  if (getenv ("DEBUG"))
    printf (" setting %s to %d\n", reg, value);
  regs[*reg - 'a'] = value;
}

int main(int argc, char **argv)
{
  int instr = 0;
  char buf[250]; // sized based on pre-inspecting file
  int nread = fread (buf, 1, sizeof buf, stdin);
  char *p = buf;
  while (p < buf + nread) {
    program[instr++] = p;
    p = strchr (p, '\n');
    *p++ = '\0';
  }
  printf ("program consists of %d instructions\n", instr);
  int initial = -1;
 retry:
  regs[0] = ++initial;
  memset (cache, 0, sizeof cache);
  if (getenv ("DEBUG"))
    printf ("attempting execution with a=%d\n", initial);
  unsigned int pc = 0;
  long long count = 0;
  int expected = 0;
  while (pc < instr && expected < 50) {
    char arg1[10], arg2[10];
    unsigned int line;
    count++;
    if (getenv ("DEBUG"))
      printf ("count=%lld pc=%d a=%d b=%d c=%d d=%d, executing %s\n", count,
	      pc, regs[0], regs[1], regs[2], regs[3], program[pc]);
    sscanf (program[pc], "%*s %9s %9s", arg1, arg2);
    switch (program[pc][0]) {
    case 'c': // cpy x y
      if (isalpha (*arg2))
	set (arg2, get (arg1));
      break;
    case 'i': // inc x
      set (arg1, get (arg1) + 1);
      break;
    case 'd': // dec x
      set (arg1, get (arg1) - 1);
      break;
    case 'j': // jnz x y
      if (get (arg1))
	pc += get (arg2) - 1;
      break;
    case 't': // tgl x
      line = get (arg1) + pc;
      if (line < instr) {
	if (getenv ("DEBUG"))
	  printf (" rewriting instruction %d, was %s\n", line, program[line]);
	switch (*program[line]) {
	case 'i':
	  memcpy (program[line], "dec", 3);
	  break;
	case 'd':
	case 't':
	  memcpy (program[line], "inc", 3);
	  break;
	case 'c':
	  memcpy (program[line], "jnz", 3);
	  break;
	case 'j':
	  memcpy (program[line], "cpy", 3);
	  break;
	default:
	  return 1;
	}
      }
      break;
    case 'o': // out x
      line = get (arg1);
      if (line != (expected & 1)) {
	//if (getenv ("DEBUG"))
	  printf ("after %lld ops and %d out, got %d, with initial a=%d\n",
		  count, expected, line, initial);
	goto retry;
      }
      if (initial == 175)
	printf (" %d %d %d %d: %d\n", regs[0], regs[1], regs[2], regs[3], line);
      if (!line) {
	if (!memcmp (regs, cache, sizeof regs))
	  printf ("same state seen, looping sequence found?\n");
	memcpy (cache, regs, sizeof regs);
      }
      expected++;
      break;
    default:
      return 1;
    }
    pc++;
  }
  int potential = 0;
  if (pc < instr) {
    printf ("potential candidate for start %d, with %d out\n", initial,
	    expected);
    if (potential++ < 10)
      goto retry;
  }
  printf ("after %lld operations, final content of register a: %d\n",
	  count, regs[0]);
  return 0;
}
