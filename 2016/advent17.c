#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include "md5.h"

void
generate (int x, int y, const char *prefix, FILE *f)
{
  unsigned char sum[16];
  md5_buffer(prefix, strlen (prefix), sum);
  if ((sum[0] >> 4) > 10 && y < 3)
    fprintf (f, "%d%d%sU\n", x, y + 1, prefix);
  if ((sum[0] & 15) > 10 && y)
    fprintf (f, "%d%d%sD\n", x, y - 1, prefix);
  if ((sum[1] >> 4) > 10 && x)
    fprintf (f, "%d%d%sL\n", x - 1, y, prefix);
  if ((sum[1] & 15) > 10 && x < 3)
    fprintf (f, "%d%d%sR\n", x + 1, y, prefix);
}

int
main (int argc, char **argv)
{
  char *salt = "edjrjqaa";
  if (argc == 2)
    salt = argv[1];
  int gen = 0;
  char *str1, *str2 = NULL;
  size_t size1, size2 = 0;
  size1 = asprintf (&str1, "03%s\n", salt);
  FILE *f;
  int max = 0;
  while (1) {
    gen++;
    if (getenv ("DEBUG"))
      printf (" starting generation %d\n", gen);
    char *p = str1;
    int seen = 0;
    if (!size1) {
      printf ("no longer path possible\n");
      break;
    }
    f = open_memstream (&str2, &size2);
    do {
      int x = *p++ - '0';
      int y = *p++ - '0';
      char *q = strchr (p, '\n');
      *q = '\0';
      if (getenv ("DEBUG"))
	printf ("  visiting %d %d %s\n", x, y, p);
      seen++;
      if (x == 3 && !y) {
	static bool print = true;
	if (print)
	  printf ("shortest solution: %s\n", p);
	print = false;
	max = gen - 1;
      } else
	generate (x, y, p, f);
      p = q + 1;
    } while (p < str1 + size1);
    if (getenv ("DEBUG"))
      printf (" generation %d visited %d paths\n", gen, seen);
    fflush (f);
    free (str1);
    str1 = str2;
    size1 = size2;
    str2 = NULL;
    size2 = 0;
  }
  printf ("completed after %d steps, longest at %d\n", gen, max);
  return 0;
}
