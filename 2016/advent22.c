#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <stdbool.h>

#define ROWS 29 // cheat by pre-inspecting input file
#define COLS 35
typedef struct node node;
struct node {
  int size;
  int used;
  int avail;
};
node grid[ROWS][COLS];

static void
move(int x1, int y1, int x2, int y2)
{
  printf ("moving data from %d-%d to %d-%d\n", x2, y2, x1, y1);
  grid[y1][x1].used += grid[y2][x2].used;
  assert (grid[y1][x1].used <= grid[y1][x1].size);
  grid[y1][x1].avail -= grid[y2][x2].used;
  grid[y2][x2].avail += grid[y2][x2].avail;
  grid[y2][x2].used = 0;
}

int main(int argc, char **argv)
{
  ssize_t nread;
  size_t len = 0;
  char *line = NULL;
  int maxx = 0, maxy = 0;
  while ((nread = getline(&line, &len, stdin)) >= 0) {
    if (*line != '/')
      continue;
    int x, y, s, u, a;
    if (sscanf (line, "/dev/grid/node-x%d-y%d %dT %dT %dT", &x, &y, &s, &u,
		&a) != 5)
      assert (false);
    if (x >= maxx)
      maxx = x + 1;
    if (y >= maxy)
      maxy = y + 1;
    grid[y][x].size = s;
    grid[y][x].used = u;
    grid[y][x].avail = a;
  }
  printf ("parsed %d columns over %d rows, for %d out of %d grid spots\n",
	  maxx, maxy, maxx * maxy, ROWS * COLS);
  int startx = -1, starty = -1;
  int viable = 0;
  int x, y;
  for (y = 0; y < maxy; y++)
    for (x = 0; x < maxx; x++)
      for (int i = 0; i < maxy; i++)
	for (int j = 0; j < maxx; j++) {
	  if (x == j && y == i)
	    continue;
	  if (!grid[y][x].used)
	    continue;
	  if (grid[y][x].used < grid[i][j].avail) {
	    viable++;
	    if (startx < 1) {
	      startx = j;
	      starty = i;
	    }
	  }
	}
  printf ("found %d viable pairs, all leading to %d-%d\n", viable,
	  startx, starty);
  int steps = 0;
  x = startx;
  y = starty;
  // I really don't feel like coding up an exploratory forking algorithm;
  // pre-inspecting the data shows a brick wall from 24-9 through 34-9.
  // Route around it by getting x to 23 before moving y to 0
  while (x > 23) {
    steps++;
    move (x, y, x - 1, y);
    x--;
  }
  while (y) {
    steps++;
    move (x, y, x, y - 1);
    y--;
  }
  while (x < maxx - 1) {
    steps++;
    move (x, y, x + 1, y);
    x++;
  }
  while (x > 1) {
    steps += 5;
    move (x, y, x, y + 1);
    y++;
    move (x, y, x - 1, y);
    x--;
    move (x, y, x - 1, y);
    x--;
    move (x, y, x, y - 1);
    y--;
    move (x, y, x + 1, y);
    x++;
  }
  printf ("total steps: %d\n", steps);
  return 0;
}
