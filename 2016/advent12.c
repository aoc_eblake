#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <ctype.h>

int regs[4];
char *program[50]; // sized based on pre-inspecting file

int
get (const char *value)
{
  if (isalpha(*value))
    return regs[*value - 'a'];
  return atoi (value);
}

void
set (const char *reg, int value)
{
  regs[*reg - 'a'] = value;
}

int main(int argc, char **argv)
{
  int instr = 0;
  char buf[200]; // sized based on pre-inspecting file
  int nread = fread (buf, 1, sizeof buf, stdin);
  char *p = buf;
  while (p < buf + nread) {
    program[instr++] = p;
    p = strchr (p, '\n');
    *p++ = '\0';
  }
  printf ("program consists of %d instructions\n", instr);
  unsigned int pc = 0;
  regs[2] = 1;
  int count = 0;
  while (pc < instr) {
    char arg1[10], arg2[10];
    count++;
    sscanf (program[pc], "%*s %9s %9s", arg1, arg2);
    switch (program[pc][0]) {
    case 'c': // cpy
      set (arg2, get (arg1));
      pc++;
      break;
    case 'i': // inc
      set (arg1, get (arg1) + 1);
      pc++;
      break;
    case 'd': // dec
      set (arg1, get (arg1) - 1);
      pc++;
      break;
    case 'j': // jnz
      if (get (arg1))
	pc += atoi (arg2);
      else
	pc++;
      break;
    default:
      return 1;
    }
  }
  printf ("after %d operations, final content of register a: %d\n",
	  count, regs[0]);
  return 0;
}
