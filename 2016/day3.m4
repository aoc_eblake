divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day3.input] day3.m4

include(`common.m4')ifelse(common(3), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`list', translit(include(defn(`file')), nl))
define(`part1', 0)define(`part2', 0)
define(`valid', `_$0($1, $2, $3, 1)_$0($4, $5, $6, 1)_$0($7, $8, $9, 1)_$0($1,
  $4, $7, 2)_$0($2, $5, $8, 2)_$0($3, $6, $9, 2)')
define(`_valid', `ifelse(eval($1 + $2 > $3 && $2 + $3 > $1 && $3 + $1 > $2),
  1, `define(`part$4', incr(part$4))')')
ifdef(`__gnu__', `
  define(`d', ` *\([0-9][0-9]*\)')
  patsubst(defn(`list'), d()d()d()d()d()d()d()d()d(),
    `valid(\1, \2, \3, \4, \5, \6, \7, \8, \9)')
', `
  define(`prep', `valid(shift(forloop(0, 8, `, substr(`$1', eval(', ` * 5),
    5)')))')
  define(`half', `eval($1/45/2*45)')
  define(`chew', `ifelse($1, 45, `prep(`$2')', `$0(half($1), substr(`$2', 0,
    half($1)))$0(eval($1-half($1)), substr(`$2', half($1)))')')
  chew(len(defn(`list')), defn(`list'))
')

divert`'part1
part2
