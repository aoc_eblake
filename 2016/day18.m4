divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day18.input] day18.m4
# Optionally use -Dverbose=1 to see some progress
# Optionally use -Donly=[12] to skip a part

include(`common.m4')ifelse(common(18), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`input', translit(include(defn(`file')), `^.'nl, `10'))
# Input is width 100, operate 25 bits at a time
ifdef(`__gnu__', `
  define(`n', 0)
  define(`set', `define(`i'n, eval(0r2:$1))define(`n', incr(n))')
  patsubst(input, `.........................', `set(\&)')
',`
  define(`bit', `|(substr(`$2', $1, 1) << (24 - $1%25))')
  define(`set', `define(`i$1', eval(0forloop(eval($1*25), eval($1*25+24),
    `bit(', `, `$2')')))')
  forloop(0, 3, `set(', `, input)')
')
ifelse(verbose, 1, `
  define(`rename', `ifdef(`$2', `', `define(`$2', defn(`$1'))popdef(`$1')')')
  define(`o0', `output(1, `...'decr($1))rename(`$0', `o'E(`$1+9999'))o($@)')')
define(`o'decr(40), `define(`part1', $2)')
ifelse(ifdef(`only', `only', 0), 1, `',
  `define(`o'decr(40), defn(`o'decr(40))`o($@)')')
define(`o'decr(400000), `define(`part2', $2)')
define(`D', defn(`ifdef'))define(`I', defn(`incr'))define(`E', defn(`eval'))
define(`L', defn(`len'))define(`T', defn(`translit'))
define(`o', `D(`$0$1',`$0$1',`$0')(I($1),E(`$2+'L(T(E(`$3',2,25)'dnl
`E(`$4',2,25)E(`$5',2,25)E(`$6',2,25),1))),O(0,`$3',`$4'),'dnl
`O(`$3',`$4',`$5'),O(`$4',`$5',`$6'),O(`$5',`$6',0))')
define(`O', `E(`(($1<<25|$2)^($2<<2|$3>>23))>>1&0x1ffffff')')
o(0, 0, i0, i1, i2, i3)

divert`'part1
part2
