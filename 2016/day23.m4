divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day23.input] day23.m4

include(`common.m4')ifelse(common(23), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`input', translit((include(defn(`file'))), nl`,()', `;'))
define(`cnt', 0)
define(`line', `_$0(translit(`$1', ` ', `,'))')
define(`_line', `define(`i'cnt, `$1')define(`a'cnt, quote(shift($@),
  cnt))define(`cnt', incr(cnt))')
ifdef(`__gnu__', `
  patsubst(defn(`input'), `\([^;]*\);', `line(`\1')')
', `
  define(`_chew', `line(substr(`$1', 0, index(`$1', `;')))define(`tail',
    substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(`tail'), `;'), -1,
    `', `$0(defn(`tail'))')')
  define(`chew', `ifelse(eval($1 < 20), 1, `_$0(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  chew(len(defn(`input')), defn(`input'))
')

define(`r_', `ifelse(len(`$1'), 1, `ifdef(`r$1', `r')')$1')
define(`cpy_', `define(`r$2', r_(`$1'))1')
define(`inc_', `define(`r$1', incr(r$1))1')
define(`dec_', `define(`r$1', decr(r$1))1')
define(`jnz_', `ifelse(r_(`$1'), 0, 1, $2, -2, `hackadd(`$1', `$2',
  decr(decr($3)), decr($3))', $2, -5, `hackmul(`$1', `$2'forloop(eval($3 - 5),
  decr($3), `,'))', `r_(`$2')')')
define(`hackadd', `ifelse(i$3.i$4, `dec.inc', `$0(`$1', `$2', $4, $3)',
  i$3.i$4.first(a$4), `inc.dec.$1', `define(`r'first(a$3),
  eval(defn(`r'first(a$3)) + r$1))define(`r$1', 0)1', `r_(`$2')')')
define(`hackmul', `ifelse(i$3.i$4.i$5.i$6.i$7, `cpy.inc.dec.jnz.dec',
  `define(`r'first(a$4), eval(defn(`r'first(a$4)) + r_(first(a$3)) *
  r$1))define(`r'first(a$5), 0)define(`r$1', 0)1', `r_(`$2')')')
define(`tgl_', `toggle(eval($2 + r_(`$1')))1')
define(`toggle', `ifdef(`i$1', `pushdef(`mod', `i$1')pushdef(`i$1',
  defn(`t'i$1))')')
define(`tcpy', `jnz')
define(`tinc', `dec')
define(`tdec', `inc')
define(`tjnz', `cpy')
define(`ttgl', `inc')
define(`run', `ifdef(`i$1', `$0(_$0(i$1, $1))')')
define(`_run', `eval($2 + $1_(a$2))')
define(`reset', `ifdef(`mod', `popdef(defn(`mod'))popdef(`mod')$0()',
  `define(`ra', 0)define(`rb', 0)define(`rc', 0)define(`rd', 0)')')

reset()define(`ra', 7)
run(0)
define(`part1', ra)
reset()define(`ra', 12)
run(0)
define(`part2', ra)

divert`'part1
part2
