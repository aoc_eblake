divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dprefix=XXX] [-Dhashsize=H] [-Dfile=day17.input] day17.m4
# Optionally use -Donly=[12] to skip a part
# Optionally use -Dverbose=[12] to see some progress

include(`common.m4')ifelse(common(17), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

ifdef(`prefix', `', `define(`prefix', translit(include(defn(`file')), nl))')

# Assume that prefix is [a-z]*.  Path uses U, D, L, R; but D and L are
# macros we use elsewhere.  Work around it by mapping U, W, Y, Z to the
# desired ascii values, then translit them for display.  Use _ for string end.
define(`N', defn(`define'))define(`E', defn(`eval'))
define(`L', ``($1<<$2|($1>>1&0x7fffffff)>>31-$2)'')
define(`F', `N(`$1',E(`$2+'L(E($1`+($4^$2&($3^$4))+$5+$7'),`$6')))')
define(`G', `N(`$1',E(`$2+'L(E($1`+($3^$4&($2^$3))+$5+$7'),`$6')))')
define(`H', `N(`$1',E(`$2+'L(E($1`+($2^$3^$4)+$5+$7'),`$6')))')
define(`I', `N(`$1',E(`$2+'L(E($1`+($3^($2|~$4))+$5+$7'),`$6')))')
define(`set', `define(`V$1', eval($1 + $2))')
forloop(0, 9, `set(', `, 48)')
define(`set', `define(`V'eval(9 + $1, 36), eval($1 + $2))')
forloop(1, 26, `set(', `, 96)')
define(`VU', 85)define(`VW', 68)define(`VY', 76)define(`VZ', 82)
define(`V_', 128)
define(`S0', `N(`X$2',$1)')
define(`S1', `N(`X$2',E(X$2|$1`<<8'))')
define(`S2', `N(`X$2',E(X$2|$1`<<16'))')
define(`S3', `N(`X$2',E(X$2|$1`<<24'))')
define(`_P', `ifelse(`$1',,,`S$3(`V'substr(`$1',0,1),$2,$3)$0(
  substr(`$1',1),ifelse($3,3,`incr($2),0',`$2,incr($3)'))')')
define(`P', forloop(0, 15, ``N(`X''', ``,0)'')`_$0(`$1',0,0)')
define(`K', `E(`($1&0x00f0)>0x00a0'),E(`($1&0x000f)>0x000a'),'dnl
`E(`($1&0xf000)>0xa000'),E(`($1&0x0f00)>0x0a00')')
define(`md5', `K($0_(translit(`$1_',`DLR',`WYZ'),len(`$1'),`1732584193','dnl
``-271733879',`-1732584194',`271733878'))')
define(`md5_', `ifelse(E(len(`$1')<=56),1,`P(`$1')N(`X14',E(`$2*8'))_md5(''dnl
```$3',`$4',`$5',`$6')',`$0(substr(`$1',64),`$2',P(substr(`$1',0,64))_md5(''dnl
```$3',`$4',`$5',`$6'))')')
define(`_md5', `N(`A',`$1')N(`B',`$2')N(`C',`$3')N(`D',`$4')'dnl
`F(`A',B,C,D,X0,7,`0xd76aa478')'dnl
`F(`D',A,B,C,X1,12,`0xe8c7b756')'dnl
`F(`C',D,A,B,X2,17,`0x242070db')'dnl
`F(`B',C,D,A,X3,22,`0xc1bdceee')'dnl
`F(`A',B,C,D,X4,7,`0xf57c0faf')'dnl
`F(`D',A,B,C,X5,12,`0x4787c62a')'dnl
`F(`C',D,A,B,X6,17,`0xa8304613')'dnl
`F(`B',C,D,A,X7,22,`0xfd469501')'dnl
`F(`A',B,C,D,X8,7,`0x698098d8')'dnl
`F(`D',A,B,C,X9,12,`0x8b44f7af')'dnl
`F(`C',D,A,B,X10,17,`0xffff5bb1')'dnl
`F(`B',C,D,A,X11,22,`0x895cd7be')'dnl
`F(`A',B,C,D,X12,7,`0x6b901122')'dnl
`F(`D',A,B,C,X13,12,`0xfd987193')'dnl
`F(`C',D,A,B,X14,17,`0xa679438e')'dnl
`F(`B',C,D,A,X15,22,`0x49b40821')'dnl
`G(`A',B,C,D,X1,5,`0xf61e2562')'dnl
`G(`D',A,B,C,X6,9,`0xc040b340')'dnl
`G(`C',D,A,B,X11,14,`0x265e5a51')'dnl
`G(`B',C,D,A,X0,20,`0xe9b6c7aa')'dnl
`G(`A',B,C,D,X5,5,`0xd62f105d')'dnl
`G(`D',A,B,C,X10,9,`0x02441453')'dnl
`G(`C',D,A,B,X15,14,`0xd8a1e681')'dnl
`G(`B',C,D,A,X4,20,`0xe7d3fbc8')'dnl
`G(`A',B,C,D,X9,5,`0x21e1cde6')'dnl
`G(`D',A,B,C,X14,9,`0xc33707d6')'dnl
`G(`C',D,A,B,X3,14,`0xf4d50d87')'dnl
`G(`B',C,D,A,X8,20,`0x455a14ed')'dnl
`G(`A',B,C,D,X13,5,`0xa9e3e905')'dnl
`G(`D',A,B,C,X2,9,`0xfcefa3f8')'dnl
`G(`C',D,A,B,X7,14,`0x676f02d9')'dnl
`G(`B',C,D,A,X12,20,`0x8d2a4c8a')'dnl
`H(`A',B,C,D,X5,4,`0xfffa3942')'dnl
`H(`D',A,B,C,X8,11,`0x8771f681')'dnl
`H(`C',D,A,B,X11,16,`0x6d9d6122')'dnl
`H(`B',C,D,A,X14,23,`0xfde5380c')'dnl
`H(`A',B,C,D,X1,4,`0xa4beea44')'dnl
`H(`D',A,B,C,X4,11,`0x4bdecfa9')'dnl
`H(`C',D,A,B,X7,16,`0xf6bb4b60')'dnl
`H(`B',C,D,A,X10,23,`0xbebfbc70')'dnl
`H(`A',B,C,D,X13,4,`0x289b7ec6')'dnl
`H(`D',A,B,C,X0,11,`0xeaa127fa')'dnl
`H(`C',D,A,B,X3,16,`0xd4ef3085')'dnl
`H(`B',C,D,A,X6,23,`0x04881d05')'dnl
`H(`A',B,C,D,X9,4,`0xd9d4d039')'dnl
`H(`D',A,B,C,X12,11,`0xe6db99e5')'dnl
`H(`C',D,A,B,X15,16,`0x1fa27cf8')'dnl
`H(`B',C,D,A,X2,23,`0xc4ac5665')'dnl
`I(`A',B,C,D,X0,6,`0xf4292244')'dnl
`I(`D',A,B,C,X7,10,`0x432aff97')'dnl
`I(`C',D,A,B,X14,15,`0xab9423a7')'dnl
`I(`B',C,D,A,X5,21,`0xfc93a039')'dnl
`I(`A',B,C,D,X12,6,`0x655b59c3')'dnl
`I(`D',A,B,C,X3,10,`0x8f0ccc92')'dnl
`I(`C',D,A,B,X10,15,`0xffeff47d')'dnl
`I(`B',C,D,A,X1,21,`0x85845dd1')'dnl
`I(`A',B,C,D,X8,6,`0x6fa87e4f')'dnl
`I(`D',A,B,C,X15,10,`0xfe2ce6e0')'dnl
`I(`C',D,A,B,X6,15,`0xa3014314')'dnl
`I(`B',C,D,A,X13,21,`0x4e0811a1')'dnl
`I(`A',B,C,D,X4,6,`0xf7537e82')'dnl
`I(`D',A,B,C,X11,10,`0xbd3af235')'dnl
`I(`C',D,A,B,X2,15,`0x2ad7d2bb')'dnl
`I(`B',C,D,A,X9,21,`0xeb86d391')($1+A),($2+B),($3+C),($4+D)')

define(`cnt', 0)define(`part2', 0)
define(`visit', `ifelse(E(cnt%1000), 0, `output(1, `...'cnt)')N(`cnt',
  incr(cnt))ifelse($1$2,  33, `ifelse(ifdef(`part1', `eval(len(`$3') <
  len(part1))', 1), 1, `define(`part1', `$3')')ifelse(eval(len(`$3') > part2),
  1, `define(`part2', len(`$3'))')', `_$0($1, $2, `$3', md5(''prefix```$3'))')')
define(`_visit', `ifelse($2, 0, `', $4, 1, `visit($1, decr($2),
  `$3U')')ifelse($2, 3, `', $5, 1, `visit($1, incr($2), `$3D')')ifelse($1, 0,
  `', $6, 1, `visit(decr($1), $2, `$3L')')ifelse($1, 3, `', $7, 1,
  `visit(incr($1), $2, `$3R')')')
ifelse(ifdef(`only', `only', 0), 1, `
  define(`_visit', `ifelse(ifdef(`part1', `eval(len(`$3') > len(part1))', 0),
  1, `','dquote(defn(`_visit'))`)')
')
visit(0, 0, `')

divert`'part1
part2
