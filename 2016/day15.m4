divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day15.input] day15.m4

include(`common.m4')ifelse(common(15), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`positions', `(')define(`position', `)')
define(`discs', 0)
define(`disc', `define(`d$2', $4)define(`o$2',
  eval(`(10 * $4 - $2 - $6) % $4'))define(`discs', incr(discs))')
translit(include(file), define(`Disc', `disc(')` .#', `,)')

define(`bits', `_$0(eval($1, 2))')
define(`_bits', ifdef(`__gnu__', ``shift(patsubst($1, ., `, \&'))'',
  ``ifelse(len($1), 1, `$1', `substr($1, 0, 1),$0(substr($1, 1))')''))
define(`modmul', `define(`d', 0)define(`a', ifelse(index($1, -), 0,
  `eval(`$1 + $3')', $1))define(`b', ifelse(index($2, -), 0, `eval(`$2 + $3')',
  $2))pushdef(`m', $3)foreach(`_$0', bits(a))popdef(`m')d`'')
define(`_modmul', `define(`d', eval(d + d))ifelse(eval(m < d), 1, `define(`d',
  eval(d - m))')ifelse($1, 1, `define(`d', eval(d + b))')ifelse(eval(m < d), 1,
  `define(`d', eval(d - m))')')
define(`pair', `define(`$1', $3)define(`$2', $4)')
define(`ext', `pair(`r_', `r', $1, $2)pair(`m', `s', 1, 0)pair(`n', `T', 0,
  1)_$0()')
define(`_ext', `ifelse(r, 0, `', `define(`q', eval(r_ / r))pair(`r_', `r', r,
  eval(r_ - q * r))pair(`m', `s', s, eval(m - q * s))pair(`n', `T', T,
  eval(n - q * T))$0()')')
define(`p', 1)define(`r', 0)
define(`crt', `_$0($1, $2, `p', `r', eval($1 * p), r)')
define(`_crt', `ifelse($3, 1, `pair(`$3', `$4', $1, $2)', `ext($3,
  $1)pair(`$3', `$4', $5, eval((modmul(modmul($6, n, $5), $1, $5) +
  modmul(modmul($2, m, $5), $3, $5)) % $5))')')
define(`do', `crt(d$1, o$1)')
forloop_arg(1, discs, `do')
define(`part1', r)
crt(11, eval((`2 * 11 - 1 - 'discs) % 11))
define(`part2', r)

divert`'part1
part2
