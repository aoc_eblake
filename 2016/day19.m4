divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dmax=NNN] [-Dfile=day19.input] day19.m4

include(`common.m4')ifelse(common(19), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

ifdef(`max', `', `define(`max', translit(include(defn(`file')), nl))')
# Josephus problem has a closed form - bitwise rotate left by 1:
# https://en.wikipedia.org/wiki/Josephus_problem
define(`msb', `eval(1 << len(eval($1, 2)) - 1)')
define(`part1', eval(2 * (max - msb(max)) + 1))
# Even the modified problem has a pattern: between each power of 3,
# the first half increments by 1, the second half by 2
# (ie. 10-18 -> 1-9, 19-27 -> 11-27; 28-54 -> 1-27, 55-81 -> 29-81)
define(`while', `ifelse($1, 1, `$2`'$0($@)')')
define(`n', 3)
while(`eval(n <= max)', `define(`n', eval(n * 3))')
define(`part2', ifelse(eval(max <= n/3*2), 1, `eval(max - n/3)',
  `eval(2 * max - n)'))

divert`'part1
part2
