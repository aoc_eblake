divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day20.input] day20.m4

include(`common.m4')ifelse(common(20), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`list', translit(include(defn(`file')), nl, `;'))
define(`merge', `ifelse($1, $3, `_stack_foreach(`l$1', `_$0(first(', `), $2,
  $4, $1)', `t')ifdef(`in', `popdef(`in')', `pushdef(`l$1', `$2,$4')')',
  `$0($1, $2, $1, 16777215)$0(incr($1), 0, $3, $4)')')
define(`_merge', `ifdef(`in', `ifelse(eval(`$2+1 >= $3'), 1, `popdef(`in')$0(
  l$5, popdef(`l$5')l$5, $5)')', `ifelse(eval(`$3 > $2+1'),
  1, `define(`l$5', `$3,$4')pushdef(`l$5', `$1,$2')define(`in')', eval(
  `$3 >= $1'), 1, `ifelse(eval(`$4-1 > $2'), 1, `define(`l$5',
  `$1,$4')')define(`in')', eval(`$4 > $2'), 1, `define(`l$5',
  `$3,$4')define(`in')', eval(`$4+1 >= $1'), 1, `define(`l$5',
  `$3,$2')define(`in')')')')
define(`line', `_$0(translit(`$1', `-', `,'))')
define(`_line', `merge(eval(`$1 >> 24 & 255'), eval(`$1 & 0xffffff'),
  eval(`$2 >> 24 & 255'), eval(`$2 & 0xffffff'))')
ifdef(`__gnu__', `
  patsubst(defn(`list'), `\([^;]*\);', `line(`\1')')
',`
  define(`chew', `line(substr(`$1', 0, index(`$1', `;')))define(`tail',
    substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(`tail'), `;'), -1,
    `', `$0(defn(`tail'))')')
  define(`split', `ifelse(eval($1 < 45), 1, `chew(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  split(len(defn(`list')), defn(`list'))
')
define(`part2', 0)
define(`_find', `ifelse($2.$3, 0.16777215, `', `define(`part1',
  eval(`$1<<24|$3+1'))')ifdef(`prev', `define(`part2', eval(part2 + prev -
  ($1<<24|$3) - 1))')define(`prev', `($1<<24|$2)')')
define(`find', `_stack_foreach(`l$1', `_$0($1, first(', `))', `t')')
forloop_rev(255, 0, `find(', `)')

divert`'part1
part2
