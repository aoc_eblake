#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "md5.h"

char buf[1000][33];

void
hash (const char *start, char *out)
{
  unsigned char sum[16];
  int i, j;
  md5_buffer(start, strlen (start), sum);
  for (j = 0; j < 16; j++) {
    out[j * 2] = "0123456789abcdef"[sum[j] >> 4];
    out[j * 2 + 1] = "0123456789abcdef"[sum[j] & 0xf];
  }
  for (i = 0; i < 2016; i++) {
    md5_buffer(out, 32, sum);
    for (j = 0; j < 16; j++) {
      out[j * 2] = "0123456789abcdef"[sum[j] >> 4];
      out[j * 2 + 1] = "0123456789abcdef"[sum[j] & 0xf];
    }
  }
}

int
main (int argc, char **argv)
{
  char *salt = "zpqevtbw";
  if (argc == 2)
    salt = argv[1];
  char str[] = "xxxxxxxx99999999";
  char *p = stpcpy (str, salt);
  int i, j;
  for (i = 0; i < 1000; i++) {
    sprintf (p, "%d", i);
    hash (str, buf[i]);
  }
  int keys = 0;
  i = 0;
  while (keys < 64) {
    char candidate[5] = "";
    for (j = 0; j < 32 - 2; j++)
      if (buf[i % 1000][j] == buf[i % 1000][j + 1] &&
	  buf[i % 1000][j] == buf[i % 1000][j + 2]) {
	printf ("found a triple at index %d\n", i);
	memset (candidate, buf[i % 1000][j], 5);
	break;
      }
    sprintf (p, "%d", i + 1000);
    hash (str, buf[i % 1000]);
    i++;
    if (*candidate && memmem (buf, 33 * 1000, candidate, 5)) {
      printf ("candidate was a key\n");
      keys++;
    }
  }
  printf ("final index: %d\n", i - 1);
  return 0;
}
