#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <ctype.h>

unsigned int grid[8][26];

int main(int argc, char **argv)
{
  int count = -1;
  int limit = 8;
  if (argc == 2) {
    limit = atoi (argv[1]);
    if (limit > 8)
      return 1;
  }
  int c = '\n';
  while (c == '\n') {
    count++;
    for (int i = 0; i < limit; i++) {
      if ((c = getchar ()) < 0)
	break;
      grid[i][c - 'a']++;
    }
    c = getchar ();
  }
  printf ("after reading %d repetitions, message was: ", count);
  for (int i = 0; i < limit; i++) {
    int min = 0;
    int j;
    for (j = 0; j < 26; j++)
      if (grid[i][j] - 1 < grid[i][min] - 1)
	min = j;
    putchar (min + 'a');
  }
  putchar ('\n');
  return 0;
}
