divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day10.input] day10.m4

include(`common.m4')ifelse(common(10), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`goes', `eat')define(`gives', `eat')define(`and', `eat')
pushdef(`output', `out')define(`eat', `Eat(')define(`to', `)')define(`Eat')
define(`list', translit(include(defn(`file')), nl, `;'))
define(`max', -1)
define(`givebot', `define(`b$1', ifdef(`b$1', `defn(`b$1')`,'')`$2')')
define(`giveout', `define(`o$1', `$2')ifelse(eval($1 > max), 1,
  `define(`max', $1)')')
define(`line', `_$0(translit(`$1', ` ', `,'))')
define(`_line', `ifelse(`$1', `value', `give$4($5, $2)', `give$4($5,
  `lo($2)')give$7($8, `hi($2)')')')
ifdef(`__gnu__', `
  patsubst(defn(`list'), `\([^;]*\);', `line(`\1')')
',`
  define(`chew', `line(substr(`$1', 0, index(`$1', `;')))define(`tail',
    substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(`tail'), `;'), -1,
    `', `$0(defn(`tail'))')')
  define(`split', `ifelse(eval($1 < 70), 1, `chew(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  split(len(defn(`list')), defn(`list'))
')
define(`lo', `ifdef(`l$1', `', `define(`l$1', _$0(b$1, $1))')l$1')
define(`_lo', `ifelse(index(.17.61.17., .$1.$2.), -1, `', `define(`part1',
  $3)')ifelse(eval($1 < $2), 1, $1, $2)')
define(`hi', `ifdef(`h$1', `', `define(`h$1', _$0(b$1, $1))')h$1')
define(`_hi', `ifelse(eval($1 > $2), 1, $1, $2)')
define(`o', `o$1')
forloop_arg(0, max, `o')
define(`part2', eval(o(0) * o(1) * o(2)))

divert`'part1
part2
