#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#define MAX 7 // cheat, by pre-inspecting input

int
main (int argc, char **argv)
{
  int sizes[MAX] = { 0 };
  int offsets[MAX] = { 0 };
  ssize_t nread;
  size_t len = 0;
  char *line = NULL;
  int count = 0;
  while ((nread = getline(&line, &len, stdin)) >= 0) {
    char *p = strstr (line, "has") + 3;
    sizes[count] = atoi (p);
    p = strstr (line, "at position") + 11;
    offsets[count] = atoi (p);
    count++;
  }
  sizes[count++] = 11;
  printf ("read/synthesized details for %d discs\n", count);
  int time = -1;
 retry:
  time++;
  for (int i = 0; i < count; i++)
    if ((time + offsets[i] + i) % sizes[i])
      goto retry;
  printf ("start at time %d\n", time - 1);
  return 0;
}
