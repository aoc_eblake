divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dhashsize=H] [-Dfile=day11.input] day11.m4
# Optionally use -Dverbose=[12] to see some progress
# Optionally use -Donly=[12] to skip a part
# Optionally use -Dpriority=0|1|2|3|4|5 to choose priority queue algorithms

include(`common.m4')ifelse(common(11, 65537), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

include(`priority.m4')

# Encode state as 8 hex digits: first is floor (0-3), remaining 7 cover pairs
# Each pair is bbBB, bb for chip floor, BB for generator floor
# Pairs are interchangeable, so normalize packed pairs in descending order
# Packing in hex requires more eval, so favor exploded lists instead
define(`state', 0)define(`floor', 0)define(`pairs', 0)
define(`explode', `_$0($1, pairs)')
define(`_explode', `eval(`($1&0x30000000)>>28')forloop(0, decr($2),
  `,eval(($1>>4*', `)&15)')')
define(`pack', ``0x'eval(($1<<28)|_$0(shift($@)), 16, 8)')
define(`_pack', `ifelse(`$#', 1, `$1', `(($0(shift($@)))<<4)|$1')')
define(`map0', `40')define(`map1', `41')define(`map2', `42')
define(`map3', `43')define(`map4', `50')define(`map5', `51')
define(`map6', `52')define(`map7', `53')define(`map8', `60')
define(`map9', `61')define(`map10', `62')define(`map11', `63')
define(`map12', `70')define(`map13', `71')define(`map14', `72')
define(`map15', `73')
define(`map', `ifelse(`$#', 1, `', `$0$2`'$0(shift($@))')')
define(`perfloor', `_$0(map($@))')
define(`_perfloor', `$0_($1, `40')$0_($1, `51')$0_($1, `62')$0_($1, `73')')
define(`_perfloor_', `,len(translit($1, $2`'01234567, --))')
define(`heur', `eval(_$0($1perfloor($@)))')
define(`_heur', `ifelse($2$3$4, 000, 0, $2$3, 00, `$0_1(decr(decr($1)), $4)',
  $2, 0, `$0_2(decr($1), $3, $4)', $1$2, 01, `1 + $0_2(0, incr($3), $4)',
  $1, 0, `(2*$2-3) + $0_2(0, eval($2+$3), $4)', `(2*$2+$1-1) + $0_2(0,
  eval($2+$3+($1>1)), eval($4-($1==2)))')')
define(`_heur_2', `ifelse($1, 0, `(2*$2-3) + _heur_1(0, eval($2+$3))',
  `(2*$2+$1-1) + _heur_1(0, eval($2+$3+($1>1)))')')
define(`_heur_1', `ifelse($1, 0, `(2*$2-3)', `(2*$2)')')
define(`prep', `forloop(0, 15, `_$0(', `, $1)')')
define(`_prep', `define(`sort_$1_$2', eval($1 <= $2))')
forloop_arg(0, 15, `prep')
define(`sort_', `ifdef(`$0$1_$2', `$0$1_$2', `eval(`$1 <= $2')')')
define(`sort', `ifelse(`$2', `', ``,$1'', `ifelse($0_($1, $2), 1,
  ``,$@'', ``,$2'$0(`$1', shift(shift($@)))')')')
define(`addpair', `define(`state', first(state)sort(eval($1),
  shift(state)))define(`pairs', incr(pairs))')

define(`canon', `$1_$0(,shift($@))')
define(`_canon', `ifelse(`$2', `', `$1', `$0(sort($2, shift($1)),
  shift(shift($@)))')')
define(`mapat0', `40')define(`mapat1', `51')define(`mapat2', `62')
define(`mapat3', `73')
define(`mapat', `translit(map($@), mapat$1`'40516273, `CGcgcgcgcg')')
define(`safeat', `_$0(mapat($@))')
define(`_safeat', `ifelse(index(`$1', `G'), -1, 1, `eval((`0x'translit(`$1',
  `CcGg', `10')` & 0x'translit(`$1', `GgCc', `01')) == 0)')')
define(`safe', `ifelse(safeat(0,shift($@))safeat(1,shift($@))safeat(2,
  shift($@))safeat(3,shift($@)), 1111, 1, 0)')
define(`neighbors', `ifelse($1, 0, `', `_$0(decr($1), $@)')ifelse($1, 3, `',
  `_$0(incr($1), $@)')')
define(`_neighbors1', `ifelse($3, eval(`$2*5'), `try($1, eval(`$1*5'),
  $4,$5,$6,$7,$8,$9)')ifelse(eval(`$3/4'), $2, `try($1, eval(`$1*4+$3%4'),
  $4,$5,$6,$7,$8,$9)')ifelse(eval(`$3%4'), $2, `try($1, eval(`$1+($3&12)'),
  $4,$5,$6,$7,$8,$9)')')
define(`_neighbors2', `ifelse(eval(`$3/4*10+$4/4', 10, 2), $2$2,
  `try($1,eval(`$1*4+$3%4'),eval(`$1*4+$4%4'),$5,$6,$7,$8,$9)')ifelse(eval(
  `$3%4*10+$4%4', 10, 2), $2$2, `try($1,eval(`$1+($3&12)'), eval(`$1+($4&12)'),
  $5,$6,$7,$8,$9)')')
define(`_neighbors',
`_neighbors1($1,$2,$3,$4,$5,$6,$7,$8,$9)'dnl
`_neighbors1($1,$2,$4,$3,$5,$6,$7,$8,$9)'dnl
`ifelse(`$5', `', `', `_neighbors1($1,$2,$5,$3,$4,$6,$7,$8,$9)')'dnl
`ifelse(`$6', `', `', `_neighbors1($1,$2,$6,$3,$4,$5,$7,$8,$9)')'dnl
`ifelse(`$7', `', `', `_neighbors1($1,$2,$7,$3,$4,$5,$6,$8,$9)')'dnl
`ifelse(`$8', `', `', `_neighbors1($1,$2,$8,$3,$4,$5,$6,$7,$9)')'dnl
`ifelse(`$9', `', `', `_neighbors1($1,$2,$9,$3,$4,$5,$6,$7,$8)')'dnl
dnl
`_neighbors2($1,$2,$3,$4,$5,$6,$7,$8,$9)'dnl
`ifelse(`$5', `', `', `_neighbors2($1,$2,$3,$5,$4,$6,$7,$8,$9)')'dnl
`ifelse(`$6', `', `', `_neighbors2($1,$2,$3,$6,$4,$5,$7,$8,$9)')'dnl
`ifelse(`$7', `', `', `_neighbors2($1,$2,$3,$7,$4,$5,$6,$8,$9)')'dnl
`ifelse(`$8', `', `', `_neighbors2($1,$2,$3,$8,$4,$5,$6,$7,$9)')'dnl
`ifelse(`$9', `', `', `_neighbors2($1,$2,$3,$9,$4,$5,$6,$7,$8)')'dnl
dnl
`ifelse(`$5', `', `', `_neighbors2($1,$2,$4,$5,$3,$6,$7,$8,$9)')'dnl
`ifelse(`$6', `', `', `_neighbors2($1,$2,$4,$6,$3,$5,$7,$8,$9)')'dnl
`ifelse(`$7', `', `', `_neighbors2($1,$2,$4,$7,$3,$5,$6,$8,$9)')'dnl
`ifelse(`$8', `', `', `_neighbors2($1,$2,$4,$8,$3,$5,$6,$7,$9)')'dnl
`ifelse(`$9', `', `', `_neighbors2($1,$2,$4,$9,$3,$5,$6,$7,$8)')'dnl
dnl
`ifelse(`$6', `', `', `_neighbors2($1,$2,$5,$6,$3,$4,$7,$8,$9)')'dnl
`ifelse(`$7', `', `', `_neighbors2($1,$2,$5,$7,$3,$4,$6,$8,$9)')'dnl
`ifelse(`$8', `', `', `_neighbors2($1,$2,$5,$8,$3,$4,$6,$7,$9)')'dnl
`ifelse(`$9', `', `', `_neighbors2($1,$2,$5,$9,$3,$4,$6,$7,$8)')'dnl
dnl
`ifelse(`$7', `', `', `_neighbors2($1,$2,$6,$7,$3,$4,$5,$8,$9)')'dnl
`ifelse(`$8', `', `', `_neighbors2($1,$2,$6,$8,$3,$4,$5,$7,$9)')'dnl
`ifelse(`$9', `', `', `_neighbors2($1,$2,$6,$9,$3,$4,$5,$7,$8)')'dnl
dnl
`ifelse(`$8', `', `', `_neighbors2($1,$2,$7,$8,$3,$4,$5,$6,$9)')'dnl
`ifelse(`$9', `', `', `_neighbors2($1,$2,$7,$9,$3,$4,$5,$6,$8)')'dnl
dnl
`ifelse(`$9', `', `', `_neighbors2($1,$2,$8,$9,$3,$4,$5,$6,$7)')'dnl
)

define(`parse_second', `define(`floor', 1)')
define(`parse_third', `define(`floor', 2)')
define(`parse_fourth', `define(`floor', 3)')
define(`parse_generator', `ifdef(`type$1', `addpair(type$1 + floor)',
  `define(`type$1', floor)')')
define(`parse_compatible', `ifdef(`type$1', `addpair(floor * 4 + type$1)',
  `define(`type$1', floor * 4)')')
define(`parse', `ifelse(`$1$2', `', `', `parse_$2(`$1')$0(shift($@))')')
parse(translit((include(defn(`file'))), -nl` .()', `,,,'))

output(1, `parse complete: 'pairs` pairs, initial state 'pack(state))

define(`hit', 0)
define(`miss', 0)
define(`iter', 0)
define(`progress', `define(`$1', incr($1))ifelse(eval((hit + miss + iter) %
  10000), 0, `output(2, `progress:'eval(hit + miss + iter))')')

define(`addwork', `define(`g$4', ``$2', `$1'')_$0(eval($2 + heur($3)),
  `$3', `$4')')
define(`_addwork', `define(`f$3', $1)progress(`hit')insert($@)')
define(`distance', `define(`goal', quote(_explode(-1, decr(`$#'))))addwork(`',
  0, `$*', translit(``$*'', `,', `_'))loop(pop)clearall()')
define(`loop', `ifelse($1, `', ``no solution possible'', eval($1 > f$3), 1,
  `progress(`miss')loop(pop)', `$2', defn(`goal'), `$1', `progress(
  `iter')pushdef(`current', ``$3','incr(first(g$3)))neighbors($2)popdef(
  `current')loop(pop)')')
define(`follow', `_$0(translit(quote(_explode(-1, decr(`$#'))), `,', `_'))')
define(`_follow', `$0_(shift(g$1), `$1')')
define(`_follow_', `ifelse(`$1', `', `$2', `_follow(`$1')` $2'')')

define(`try', `_$0(canon($@))')
define(`_try', `ifdef(translit(``g$*'', `,', `_'), `', `ifelse(safe($@), 1,
  `addwork(current, `$*', translit(``$*'', `,', `_'))')')')

ifelse(ifdef(`only', only, 1), 1, `
output(1, `starting part 1')
define(`part1', distance(state))
output(1, `hits:'hit` misses:'miss` iters:'iter)
')
ifelse(ifdef(`only', only, 2), 2, `
output(1, `starting part 2')
define(`hit', 0)
define(`miss', 0)
define(`iter', 0)
define(`part2', distance(state, 0, 0))
output(1, `hits:'hit` misses:'miss` iters:'iter)
')

divert`'part1`'ifelse(verbose, 2, ` (follow(state))')
part2`'ifelse(verbose, 2, ` (follow(state, 0, 0))')
