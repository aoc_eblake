#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>

bool check (int s1, int s2, int s3)
{
  int t;
  if (s1 > s2) {
    t = s1;
    s1 = s2;
    s2 = t;
  }
  if (s2 > s3) {
    t = s2;
    s2 = s3;
    s3 = t;
  }
  return s1 + s2 > s3;
}

int main(void)
{
  int count = 0;
  int s[9];
  while (scanf (" %d %d %d\n %d %d %d\n %d %d %d\n", &s[0], &s[1], &s[2], &s[3],
		&s[4], &s[5], &s[6], &s[7], &s[8]) == 9) {
    if (check (s[0], s[3], s[6]))
      count++;
    if (check (s[1], s[4], s[7]))
      count++;
    if (check (s[2], s[5], s[8]))
      count++;
  }
  printf ("found %d possible triangles\n", count);
  return 0;
}
