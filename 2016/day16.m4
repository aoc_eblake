divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dinit=BBB] [-Dfile=day16.input] day16.m4

include(`common.m4')ifelse(common(16), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

# See https://www.reddit.com/r/adventofcode/comments/5ititq/2016_day_16_c_how_to_tame_your_dragon_in_under_a/
ifdef(`init', `', `define(`init', translit(include(defn(`file')), nl))')
define(`rev', `ifelse(`$1', `', `', `$0(substr(`$1', 1))ifelse(substr(`$1', 0,
  1), 1, 0, 1)')')
define(`p00', 0)define(`p01', 1)define(`p10', 1)define(`p11', 0)
define(`n0', 1)define(`n1', 0)
define(`i0', 0)
define(`prep', `ifelse(`$2', `', `', `define(`i'incr($1),
  defn(`p'i$1`'substr(`$2', 0, 1)))$0(incr($1), substr(`$2', 1))')')
prep(0, init`'rev(init))
define(`solve', `_$0(0, eval(`$1 & -$1'), eval(`$1 & -$1'), $1, len(`$2'))')
define(`_solve', `define(`p', chunk(eval(`$3 / ($5 + 1)'), $3, $5))defn(
  `n'defn(`p$1'p))ifelse($3, $4, `', `$0(p, $2, eval($2 + $3), $4, $5)')')
define(`chunk', `_$0($1, eval(`($2 - $1) / ($3 * 2)'),
  eval(`($2 - $1) % ($3 * 2)'), eval(`$1 ^ ($1 >> 1)'), $3)')
define(`_chunk', `eval(($4 ^ len(translit(eval($1 & $4, 2), 0)) ^ ($2 & $5)
  ^ defn(`i'$3)) & 1)')

define(`part1', solve(272, init))
define(`part2', solve(35651584, init))

divert`'part1
part2
