#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <ctype.h>

#define MAX 1200 // cheat, by using wc on input file
typedef struct entry entry;
struct entry {
  unsigned int lo;
  unsigned int hi;
};
entry list[MAX];

int
compare (const void *a, const void *b)
{
  const entry *e1 = a, *e2 = b;
  return e1->lo < e2->lo ? -1 : e1->lo > e2->lo;
}

int
main (int argc, char **argv)
{
  int count = 0;
  unsigned int lo, hi;
  while (scanf ("%d-%d\n", &lo, &hi) == 2) {
    list[count].lo = lo;
    list[count].hi = hi;
    count++;
  }
  qsort (list, count, sizeof *list, compare);
  printf ("read and sorted %d entries\n", count);
  unsigned int min = 0;
  unsigned int total = 0;
  for (int i = 0; i < count; i++) {
    if (min < list[i].lo) {
      total += list[i].lo - min;
      min = list[i].lo;
    }
    if (list[i].lo <= min && list[i].hi >= min) {
      min = list[i].hi + 1;
      if (!min)
	break;
    }
  }
  if (list[count - 1].hi < -1U)
    total -= min;
  printf ("total %u\n", total);
  return 0;
}
