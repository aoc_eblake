divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day7.input] day7.m4

include(`common.m4')ifelse(common(7), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`list', translit(include(defn(`file')), alpha`'nl, ALPHA`_'))
define(`reset', `define(`a0', 0)define(`a1', 0)define(`b', 0)define(`last')')
reset()define(`part1', 0)define(`part2', 0)
define(`check', `ifelse(`$2', `$3', `', `$2$3', `$5$4', `define(`a'b,
  1)')ifelse(`$#', 4, `_$0(`$2', `$3', `$4')', `$#', 5, `_$0(`$3', `$4',
  `$5')')define(`last', ifelse(`$#', 5, ``,`$3',`$4',`$5''', ``$@''))')
define(`_check', `ifelse(`$1', `$2', `', `$1', `$3', `ifelse(b, 0,
  `pushdef(`c0', `$1$2')', `define(`c1', defn(`c1')`-$2$3')')')')
define(`match', `ifelse(_$0(c1), `', `', `define(`part2', incr(part2))')')
define(`_match', `ifdef(`c0', `ifelse(index(`$1', c0), -1, `',
  `-')popdef(`c0')$0(`$1')', `define(`c1')')')
define(`visit', `ifelse(`$1', `[', `define(`b', 1)define(`last')', `$1', `]',
  `define(`b', 0)define(`last')', `$1', `_', `ifelse(a0`'a1, 10, `define(
  `part1', incr(part1))')match()reset()', `check(last, `$1')')')
ifdef(`__gnu__', `
  patsubst(defn(`list'), `.', `visit(`\&')')
',`
  define(`split', `ifelse($1, 1, `visit(`$2')', `$0(eval($1/2), substr(`$2',
    0, eval($1/2)))$0(eval($1 - $1/2), substr(`$2', eval($1/2)))')')
  split(len(defn(`list')), defn(`list'))
')

divert`'part1
part2
