divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day22.input] day22.m4

include(`common.m4')ifelse(common(22), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`list', translit(include(defn(`file')), nl`#', `;'))
define(`list', substr(defn(`list'), eval(5 + index(defn(`list'), `Use%;'))))
define(`cnt', 0)
define(`oops', `errprintn(`data different than assumptions')m4exit(1)')
define(`line', `define(`u'cnt, $1)define(`a'cnt, $2)ifelse($1, 0, `ifdef(`z',
  `oops', `define(`z', cnt)')', len($1), 3, `ifdef(`f', `ifdef(`Y',
  `ifelse(eval((cnt - f) % Y), 0, `', `oops')', `define(`Y', eval(cnt - f))')',
  `ifdef(`z', `oops')define(`f', cnt)')')define(`cnt', incr(cnt))')
ifdef(`__gnu__', `
  define(`d', ` *\([0-9][0-9]*\)')
  patsubst(defn(`list'), `[^;]*'d`T'd`T'd`T'd`%;', `line(\2, \3)')
', `
  define(`prep', `line(substr(`$1', 29, 4), substr(`$1', 36, 4))')
  define(`half', `eval($1/48/2*48)')
  define(`chew', `ifelse($1, 48, `prep(`$2')', `$0(half($1), substr(`$2', 0,
    half($1)))$0(eval($1-half($1)), substr(`$2', half($1)))')')
  chew(len(defn(`list')), defn(`list'))
')
define(`X', eval(cnt / Y))

# Per part 2, we only have one empty enough node, so we really only have
# to count other nodes that fit, rather than checking every pair
define(`do', `eval(u$1 && u$1 < defn(`a'z))')
define(`part1', len(translit(forloop_arg(0, decr(cnt), `do'), 0)))
# We asserted above that the output resembles:
# () . G
#  . . .
#  . # #
#  . . _
# Solve by starting at z, moving west past f, north to 0, east to G, then
# west to goal.
define(`part2', eval((z/Y - f/Y + 1) + z%Y + (X - f/Y) + (5 * (X - 2))))

divert`'part1
part2
