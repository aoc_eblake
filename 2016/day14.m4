divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dmax=N] [-Dstart=NNN] [-Dprefix=XXX] [-Dfile=day14.input] day14.m4
# Optionally use -Donly=[12] to skip a part
# Optionally use -Dverbose=1 to see some progress

include(`common.m4')ifelse(common(14), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

ifdef(`start', `', `define(`start', 0)')
ifdef(`max', `', `define(`max', 64)')
ifdef(`prefix', `', `define(`prefix', translit(include(defn(`file')), nl))')

# Index is [0-9]*.  Assume that prefix is [a-z]*.  Also assume prefix +
# index < 56 bytes, thus only 1 md5 pass needed.
define(`N', defn(`define'))define(`E', defn(`eval'))
define(`L', ``($1<<$2|($1>>1&0x7fffffff)>>31-$2)'')
define(`F', `N(`$1',E(`$2+'L(E($1`+($4^$2&($3^$4))+$5+$7'),`$6')))')
define(`G', `N(`$1',E(`$2+'L(E($1`+($3^$4&($2^$3))+$5+$7'),`$6')))')
define(`H', `N(`$1',E(`$2+'L(E($1`+($2^$3^$4)+$5+$7'),`$6')))')
define(`I', `N(`$1',E(`$2+'L(E($1`+($3^($2|~$4))+$5+$7'),`$6')))')
define(`set', `define(`V$1', eval($1 + $2))')
forloop(0, 9, `set(', `, 48)')
define(`set', `define(`V'eval(9 + $1, 36), eval($1 + $2))')
forloop(1, 26, `set(', `, 96)')
define(`S0', `N(`X$2',$1)')
define(`S1', `N(`X$2',E(X$2|$1`<<8'))')
define(`S2', `N(`X$2',E(X$2|$1`<<16'))')
define(`S3', `N(`X$2',E(X$2|$1`<<24'))')
forloop(0, 15, `define(`X'eval(', `,16), 0)')
define(`_P', `ifelse(`$1',,`S$3(128,$2)',`S$3(`V'substr(`$1',0,1),$2,$3)$0(
  substr(`$1',1),ifelse($3,3,`incr($2),0',`$2,incr($3)'))')')
define(`P', forloop(0, 8, ``N(`X''', ``,0)'')dnl
`N(`Xe',E(len($1)`*8'))_$0(`$1',0,0)')
define(`set', `define(`Q$1', `,'defn(`V'E(`$1>>4', 16))`,'defn(`V'E(`$1&15',
  16)))')
forloop_arg(0, 255, `set')
define(`Q', `defn(`Q'E(`$1&0xff'))defn(`Q'E(`$1>>8&0xff'))defn(`Q'E('dnl
``$1>>16&0xff'))defn(`Q'E(`$1>>24&0xff'))')
define(`md5', `P(`$1')_$0(`1732584193',`-271733879',`-1732584194',`271733878')')
define(`_md5', `N(`A',`$1')N(`B',`$2')N(`C',`$3')N(`D',`$4')'dnl
`F(`A',B,C,D,X0,7,`0xd76aa478')'dnl
`F(`D',A,B,C,X1,12,`0xe8c7b756')'dnl
`F(`C',D,A,B,X2,17,`0x242070db')'dnl
`F(`B',C,D,A,X3,22,`0xc1bdceee')'dnl
`F(`A',B,C,D,X4,7,`0xf57c0faf')'dnl
`F(`D',A,B,C,X5,12,`0x4787c62a')'dnl
`F(`C',D,A,B,X6,17,`0xa8304613')'dnl
`F(`B',C,D,A,X7,22,`0xfd469501')'dnl
`F(`A',B,C,D,X8,7,`0x698098d8')'dnl
`F(`D',A,B,C,X9,12,`0x8b44f7af')'dnl
`F(`C',D,A,B,Xa,17,`0xffff5bb1')'dnl
`F(`B',C,D,A,Xb,22,`0x895cd7be')'dnl
`F(`A',B,C,D,Xc,7,`0x6b901122')'dnl
`F(`D',A,B,C,Xd,12,`0xfd987193')'dnl
`F(`C',D,A,B,Xe,17,`0xa679438e')'dnl
`F(`B',C,D,A,Xf,22,`0x49b40821')'dnl
`G(`A',B,C,D,X1,5,`0xf61e2562')'dnl
`G(`D',A,B,C,X6,9,`0xc040b340')'dnl
`G(`C',D,A,B,Xb,14,`0x265e5a51')'dnl
`G(`B',C,D,A,X0,20,`0xe9b6c7aa')'dnl
`G(`A',B,C,D,X5,5,`0xd62f105d')'dnl
`G(`D',A,B,C,Xa,9,`0x02441453')'dnl
`G(`C',D,A,B,Xf,14,`0xd8a1e681')'dnl
`G(`B',C,D,A,X4,20,`0xe7d3fbc8')'dnl
`G(`A',B,C,D,X9,5,`0x21e1cde6')'dnl
`G(`D',A,B,C,Xe,9,`0xc33707d6')'dnl
`G(`C',D,A,B,X3,14,`0xf4d50d87')'dnl
`G(`B',C,D,A,X8,20,`0x455a14ed')'dnl
`G(`A',B,C,D,Xd,5,`0xa9e3e905')'dnl
`G(`D',A,B,C,X2,9,`0xfcefa3f8')'dnl
`G(`C',D,A,B,X7,14,`0x676f02d9')'dnl
`G(`B',C,D,A,Xc,20,`0x8d2a4c8a')'dnl
`H(`A',B,C,D,X5,4,`0xfffa3942')'dnl
`H(`D',A,B,C,X8,11,`0x8771f681')'dnl
`H(`C',D,A,B,Xb,16,`0x6d9d6122')'dnl
`H(`B',C,D,A,Xe,23,`0xfde5380c')'dnl
`H(`A',B,C,D,X1,4,`0xa4beea44')'dnl
`H(`D',A,B,C,X4,11,`0x4bdecfa9')'dnl
`H(`C',D,A,B,X7,16,`0xf6bb4b60')'dnl
`H(`B',C,D,A,Xa,23,`0xbebfbc70')'dnl
`H(`A',B,C,D,Xd,4,`0x289b7ec6')'dnl
`H(`D',A,B,C,X0,11,`0xeaa127fa')'dnl
`H(`C',D,A,B,X3,16,`0xd4ef3085')'dnl
`H(`B',C,D,A,X6,23,`0x04881d05')'dnl
`H(`A',B,C,D,X9,4,`0xd9d4d039')'dnl
`H(`D',A,B,C,Xc,11,`0xe6db99e5')'dnl
`H(`C',D,A,B,Xf,16,`0x1fa27cf8')'dnl
`H(`B',C,D,A,X2,23,`0xc4ac5665')'dnl
`I(`A',B,C,D,X0,6,`0xf4292244')'dnl
`I(`D',A,B,C,X7,10,`0x432aff97')'dnl
`I(`C',D,A,B,Xe,15,`0xab9423a7')'dnl
`I(`B',C,D,A,X5,21,`0xfc93a039')'dnl
`I(`A',B,C,D,Xc,6,`0x655b59c3')'dnl
`I(`D',A,B,C,X3,10,`0x8f0ccc92')'dnl
`I(`C',D,A,B,Xa,15,`0xffeff47d')'dnl
`I(`B',C,D,A,X1,21,`0x85845dd1')'dnl
`I(`A',B,C,D,X8,6,`0x6fa87e4f')'dnl
`I(`D',A,B,C,Xf,10,`0xfe2ce6e0')'dnl
`I(`C',D,A,B,X6,15,`0xa3014314')'dnl
`I(`B',C,D,A,Xd,21,`0x4e0811a1')'dnl
`I(`A',B,C,D,X4,6,`0xf7537e82')'dnl
`I(`D',A,B,C,Xb,10,`0xbd3af235')'dnl
`I(`C',D,A,B,X2,15,`0x2ad7d2bb')'dnl
`I(`B',C,D,A,X9,21,`0xeb86d391')Q($1+A)Q($2+B)Q($3+C)Q($4+D)')

define(`sort', `ifelse(`$2', `', ``,$1'', `ifelse(eval($1 > $2), 1, ``,$@'',
  ``,$2'$0(`$1', shift(shift($@)))')')')
define(`clean', `forloop($1, eval($1+2000), `popdef(`do'', `)')define(`found',
  0)forloop(48, 57, `define(`T'', `)')forloop(97, 102, `define(`T'', `)')')
define(`list')clean(0)
define(`O', `ifelse($6,,`output(1, `candidate at $1')',`$2$3$4$5',`$3$4$5$6',
  `OO($1,$2T$2)$0($1, shift(shift($@)))', `$0($1,shift(shift($@)))')')
define(`OO', `ifelse(`$3', `', `N(`T$2')', `$1', `$3', `N(`T$2', `,$1')',
  `ifelse(E($1 - 1000 <= $3), 1, `output(1, `found $3 at $1')N(`found',
  incr(found))N(`list', sort($3list))ifelse(E(found >= max), 1,
  `N(`do'E($3 + 1000), `clean($3)')')')$0($1, $2, shift(shift(shift($@))))')')
define(`M', `_M($1)')
define(`_M', `ifelse($4,,,`$2$3',`$3$4',`N(`T$2',defn(`T$2')`,$1')O($@)','dnl
``$0($1,shift(shift($@)))')')
define(`rename', `ifdef(`$2', `', `define(`$2', defn(`$1'))popdef(`$1')')')
define(`grab', `ifelse(eval(`$# > 'max), 1, `$0(shift($@))',
  `$1define(`list')')')

ifelse(ifdef(`only', `only', 1), 1, `
define(`do'start, `output(1, `...'decr($1))rename(`$0', `do'E(`$1+999'))do($@)')
define(`do', `M($1md5('dquote(defn(`prefix'))dnl
``$1'))ifdef(`$0$1',`$0$1',`$0')(incr(`$1'))')
do(start)
define(`part1', grab(list))
')

define(`R', `ifelse(`$3',,,`S$2($3,$1,$2)$0(ifelse($2,3,`incr($1),0',''dnl
```$1,incr($2)'),shift(shift(shift($@))))')')
define(`Md5', `_Md5(2016,md5(`$1')S0(128,8)N(`Xe',256))')
define(`_Md5', `ifelse(`$1',0,`$2',`$0(decr($1),_md5(R(0,0$2)''dnl
```1732584193',`-271733879',`-1732584194',`271733878'))')')
ifelse(ifdef(`only', `only', 2), 2, `
define(`do'start, `output(1, `...'decr($1))rename(`$0', `do'E(`$1+9'))do($@)')
define(`do', `_M($1Md5('dquote(defn(`prefix'))dnl
``$1'))ifdef(`$0$1',`$0$1',`$0')(incr(`$1'))')
do(start)
define(`part2', grab(list))
')

divert`'part1
part2
