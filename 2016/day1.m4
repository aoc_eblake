divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day1.input] day1.m4

include(`common.m4')ifelse(common(1), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`list', translit((include(defn(`file')),), `,() 'nl, `;'))
define(`x', 0)define(`y', 0)define(`dir', `n')
define(`Ln', `w')define(`Le', `n')define(`Ls', `e')define(`Lw', `s')
define(`Rn', `e')define(`Re', `s')define(`Rs', `w')define(`Rw', `n')
define(`in', `ifelse(eval($2 < $3), 1, `eval($1 >= $2 && $1 <= $3)',
  `eval($1 >= $3 && $1 <= $2)')')
define(`check', `_stack_foreach(`$1', `_$0(first(', `), $2, $3, $4)', `t')')
define(`_check', `ifelse(in($4, $2, $3), 1, `ifelse(in($1, $5, $6), 1,
  `define(`part2', dist($1, $4))pushdef(`check')')')')
define(`abs', `ifelse(index($1, -), 0, `substr($1, 1)', $1)')
define(`dist', `eval(abs($1) + abs($2))')
define(`moven', `pushdef(`ns', x`,'y`,'check(`ew', x, decr(y),
  define(`y', eval(y - $1))y)y)')
define(`movee', `pushdef(`ew', y`,'x`,'check(`ns', y, incr(x),
  define(`x', eval(x + $1))x)x)')
define(`moves', `pushdef(`nw', x`,'y`,'check(`ew', x, incr(y),
  define(`y', eval(y + $1))y)y)')
define(`movew', `pushdef(`ew', y`,'x`,'check(`ns', y, decr(x),
  define(`x', eval(x - $1))x)x)')
define(`move', `$0$1($2)')
define(`visit', `define(`dir', defn(`$1'dir))move(dir, $2)')
ifdef(`__gnu__', `
  patsubst(defn(`list'), `\(.\)\([0-9]*\);', `visit(`\1', \2)')
',`
  define(`chew', `visit(substr(`$1', 0, 1), substr(`$1', 1, decr(index(`$1',
    `;'))))define(`tail', substr(`$1', incr(index(`$1', `;'))))ifelse(index(
    defn(`tail'), `;'), -1, `', `$0(defn(`tail'))')')
  define(`split', `ifelse(eval($1 < 10), 1, `chew(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  split(len(defn(`list')), defn(`list'))
')
define(`part1', dist(x, y))

divert`'part1
part2
