#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "md5.h"

int main(int argc, char **argv) {
  if (argc != 2)
    return 1;
  unsigned char sum[16];
  int i = 0;
  char out[9] = "________";
  while (strchr (out, '_')) {
    if (!(++i % 10000))
      printf ("\b\b\b\b\b\b\b\b\b\b\b %d", i);
    char *str;
    int len = asprintf(&str, "%s%d", argv[1], i);
    md5_buffer(str, len, sum);
    if (!(sum[0] | sum[1]) && sum[2] <= 0xf) {
      if (out[sum[2]] == '_')
	out[sum[2]] = "0123456789abcdef"[sum[3] >> 4];
      printf ("\n%s potential match on %s\n", out, str);
    }
  }
  printf ("final password: %s\n", out);
  return 0;
}
