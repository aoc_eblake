divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day9.input] day9.m4

include(`common.m4')ifelse(common(9), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

include(`math64.m4')
define(`list', translit(include(defn(`file')), `()'nl, `<>'))
define(`part1', 0)define(`part2', 0)define(`c', 0)
define(`_visit_0', `ifelse(`$1', `<', `define(`a', 0)define(`$0',
  defn(`$0_1'))', `ifdef(`g', `', `define(`part1', incr(part1))')define(
  `part2', add64(part2, 1))')')
define(`_visit_1', `ifelse(`$1', `x', `define(`b', 0)define(`$0',
  defn(`$0_2'))', `define(`a', eval(a*10 + $1))')')
define(`_visit_2', `ifelse(`$1', `>', `ifdef(`g', `', `define(`part1',
  eval(part1 + a*b))')define(`$0', defn(`$0_0'))pushdef(`part1',
  0)pushdef(`part2', 0)pushdef(`g', eval(c + a))pushdef(`b')', `define(`b',
  eval(b*10 + $1))')')
define(`check', `ifelse(c, g, `define(`part2', add64(mul64(part2,
  popdef(`b')b), popdef(`part2')part2))popdef(`part1')popdef(`g')$0()')')
define(`_visit', defn(`_visit_0'))
define(`visit', `define(`c', incr(c))_$0(`$1')check()')
ifdef(`__gnu__', `
  patsubst(defn(`list'), `.', `visit(`\&')')
',`
  define(`Split', `ifelse($1, 1, `visit(`$2')', `$0(eval($1/2), substr(`$2',
    0, eval($1/2)))$0(eval($1 - $1/2), substr(`$2', eval($1/2)))')')
  Split(len(defn(`list')), defn(`list'))
')

divert`'part1
part2
