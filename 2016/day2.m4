divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day2.input] day2.m4

include(`common.m4')ifelse(common(2), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`list', translit(include(defn(`file')), nl, `N'))
define(`part1')define(`part2')define(`k', 5)define(`K', 5)
define(`map', `define(`U$1', `$2')define(`R$1', `$3')define(`D$1',
  `$4')define(`L$1', `$5')define(`U_$1', `$6')define(`R_$1', `$7')define(
  `D_$1', `$8')define(`L_$1', `$9')')
map(1, 1, 2, 4, 1, 1, 1, 3, 1)
map(2, 2, 3, 5, 1, 2, 3, 6, 2)
map(3, 3, 3, 6, 2, 1, 4, 7, 2)
map(4, 1, 5, 7, 4, 4, 4, 8, 3)
map(5, 2, 6, 8, 4, 5, 6, 5, 5)
map(6, 3, 6, 9, 5, 2, 7, a, 5)
map(7, 4, 8, 7, 7, 3, 8, b, 6)
map(8, 5, 9, 8, 7, 4, 9, c, 7)
map(9, 6, 9, 9, 8, 9, 9, 9, 8)
map(a,  ,  ,  ,  , 6, b, a, a)
map(b,  ,  ,  ,  , 7, c, d, a)
map(c,  ,  ,  ,  , 8, c, c, b)
map(d,  ,  ,  ,  , b, d, d, d)
define(`visit', `ifelse(`$1', `N', `define(`part1', defn(`part1')k)define(
  `part2', defn(`part2')translit(K, `abcd', `ABCD'))', `define(`k',
  defn(`$1'k))define(`K', defn(`$1_'K))')')
ifdef(`__gnu__', `
  patsubst(defn(`list'), `.', `visit(`\&')')
',`
  define(`split', `ifelse($1, 1, `visit(`$2')', `$0(eval($1/2), substr(`$2',
    0, eval($1/2)))$0(eval($1 - $1/2), substr(`$2', eval($1/2)))')')
  split(len(defn(`list')), defn(`list'))
')

divert`'part1
part2
