#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <ctype.h>

#if 0 // sample problem
#define PAIRS 2
char initial[] = "1.11.23";
//  "hydrogen", "lithium",

#elif 0 // day11.input, part 1
#define PAIRS 5
char initial[] = "1.13333.12222";
//  "promethium", "cobalt", "Curium", "ruthenium", "Plutonium",

#else // day11.input, part 2
#define PAIRS 7
char initial[] = "1.1333311.1222211";
//  "promethium", "cobalt", "Curium", "ruthenium", "Plutonium", "elerium",
//  "dilithium",

#endif

typedef struct state state;
struct state {
  char elevator;
  union {
    struct {
      char chips[PAIRS];
      char gens[PAIRS];
    };
    char items[PAIRS * 2];
  };
};

char cache[1 << ((PAIRS * 2 + 1) * 2 - 3)];

int hash (state *s)
{
  int h = s->elevator - 1;
  for (int i = 0; i < PAIRS * 2; i++)
    h = (h << 2) + s->items[i] - 1;
  return h;
}

bool
valid (state *s)
{
  if (s->elevator < 1 || s->elevator > 4)
    return false;
  for (int i = 0; i < PAIRS; i++) {
    if (s->chips[i] == s->gens[i]) // A chip with own generator is safe
      continue;
    for (int j = 0; j < PAIRS; j++) {
      if (i == j)
	continue;
      if (s->chips[i] == s->gens[j]) // A chip with any other generator fails
	return false;
    }
  }
  // No point revisiting a configuration already seen
  int h = hash (s);
  if (cache[h >> 3] & (1 << (h & 3)))
    return false;
  cache[h >> 3] |= 1 << (h & 3);
  return true;
}

bool
goal (state *s)
{
  for (int i = 0; i < PAIRS * 2; i++)
    if (s->items[i] != 4)
      return false;
  return true;
}

state *
parse (const char *str)
{
  state *s = malloc (sizeof *s);
  const char *p = str;
  s->elevator = *p++ - '0';
  if (*p++ != '.')
    exit (1);
  for (int i = 0; i < PAIRS; i++)
    s->chips[i] = *p++ - '0';
  if (*p++ != '.')
    exit (1);
  for (int i = 0; i < PAIRS; i++)
    s->gens[i] = *p++ - '0';
  if (*p && *p != '\n')
    exit (1);
  return s;
}

bool
out (state *s, FILE *f)
{
  putc (s->elevator + '0', f);
  putc ('.', f);
  for (int i = 0; i < PAIRS; i++)
    putc (s->chips[i] + '0', f);
  putc ('.', f);
  for (int i = 0; i < PAIRS; i++)
    putc (s->gens[i] + '0', f);
  putc ('\n', f);
  return goal (s);
}

bool
generate (state *s, FILE *f)
{
  int count = 0;
  int e = s->elevator;
  int i, j;
  bool ret = false;
  for (i = 0; i < PAIRS * 2; i++)
    if (e == s->items[i])
      count++;
  if (getenv ("DEBUG"))
    printf ("%d items to choose from, producing up to %d states\n", count,
	    count * (count + 1));
  s->elevator++;
  for (i = 0; i < PAIRS * 2; i++) {
    if (e == s->items[i]) {
      s->items[i]++;
      if (valid (s))
	ret |= out (s, f);
      for (j = i + 1; j < PAIRS * 2; j++)
	if (e == s->items[j]) {
	  s->items[j]++;
	  if (valid (s))
	    ret |= out (s, f);
	  s->items[j]--;
	}
      s->items[i]--;
    }
  }
  s->elevator -= 2;
  for (i = 0; i < PAIRS * 2; i++) {
    if (e == s->items[i]) {
      s->items[i]--;
      if (valid (s))
	ret |= out (s, f);
      for (j = i + 1; j < PAIRS * 2; j++)
	if (e == s->items[j]) {
	  s->items[j]--;
	  if (valid (s))
	    ret |= out (s, f);
	  s->items[j]++;
	}
      s->items[i]++;
    }
  }
  s->elevator++;
  return ret;
}

int main(int argc, char **argv)
{
  int generations = 0;
  char *str1 = initial, *str2 = NULL;
  size_t size1 = strlen (str1), size2 = 0;
  bool done = false;
  FILE *f;
  while (!done) {
    printf (" starting generation %d\n", ++generations);
    char *p = str1;
    int seen = 0;
    f = open_memstream (&str2, &size2);
    do {
      char *q = strchrnul (p, '\n');
      state *s = parse (p);
      if (getenv ("DEBUG"))
	out (s, stdout);
      if (generate (s, f))
	done = true;
      free (s);
      seen++;
      p = q + 1;
    } while (p < str1 + size1 - 1);
    printf (" generation %d visited %d states\n", generations, seen);
    fflush (f);
    if (str1 != initial)
      free (str1);
    str1 = str2;
    size1 = size2;
    str2 = NULL;
    size2 = 0;
  }
  printf ("found solution in %d generations\n", generations);
  return 0;
}
