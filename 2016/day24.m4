divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dhashsize=H] [-Dfile=day24.input] day24.m4
# Optionally use -Dverbose=[12] to see some progress
# Optionally use -Dpriority=0|1|2|3|4|5 to choose priority queue algorithms

include(`common.m4')ifelse(common(24, 65537), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

include(`priority.m4')
define(`input', translit(include(defn(`file')), `#'nl, `@;'))
define(`x', 0)define(`y', 0)define(`max', 0)
define(`visit', `ifelse(`$1', `;', `define(`x', 0)define(`y', incr(y))',
  `ifelse(`$1', `@', `', `define(`p'x`_'y, $1)ifelse(`$1', `.', `',
  `define(`c$1', x`,'y)ifelse(eval($1 > max), 1, `define(`max',
  $1)')')')define(`x', incr(x))')')
ifdef(`__gnu__', `
  patsubst(defn(`input'), `.', `visit(`\&')')
',`
  define(`split', `ifelse($1, 1, `visit(`$2')', `$0(eval($1/2), substr(`$2',
    0, eval($1/2)))$0(eval($1 - $1/2), substr(`$2', eval($1/2)))')')
  split(len(defn(`input')), defn(`input'))
')

define(`goal', eval((1 << (max + 1)) - 1))
pushdef(`clearall')
define(`clean', `ifdef(`s', `popdef(`g'defn(`s'))popdef(`f'defn(`s'))popdef(
  `s')$0()', `clearall()')')
define(`scans', 0)
define(`scan', `output(2, `scanning from $1')pushdef(`seen',
  eval((1 << ($1 + 1)) - 1))add(c$1, 0)_$0(0, 1, $1)popdef(`seen')clean()')
define(`_scan', `ifelse(seen, 'goal`, `undefine(`s$1')', `ifdef(`s$1',
  `near(s$1, $1, $2, $3)popdef(`s$1')$0($@)', `$0($2, incr($2), $3)')')')
define(`add', `ifdef(`p$1_$2', `ifdef(`g$1_$2', `', `define(`scans',
  incr(scans))define(`g$1_$2')pushdef(`s', `$1_$2')pushdef(`s$3',
  `$1,$2')')')')
define(`near', `_$0(p$1_$2, $3, $5)add($1, decr($2), $4)add(incr($1), $2,
  $4)add($1, incr($2), $4)add(decr($1), $2, $4)')
define(`_near', `ifelse($1, $3, `', $1, `.', `', `ifdef(`d$1_$3', `',
  `define(`d$1_$3', $2)define(`d$3_$1', $2)define(`seen',
  eval(seen | (1 << $1)))')')')
forloop_arg(0, decr(max), `scan')
output(1, `distances between pairs determined, scans:'scans)

define(`hit', 0)
define(`miss', 0)
define(`iter', 0)
define(`progress', `define(`$1', incr($1))ifelse(eval((hit + miss + iter) %
  10000), 0, `output(2, `progress:'eval(hit + miss + iter))')')

popdef(`clearall')
define(`addwork', `ifelse(ifdef(`g$1_$2', `eval($3 < g$1_$2)', 1), 1,
  `pushdef(`s', `$1_$2')define(`g$1_$2', `$3')_$0(eval($3 + heur($1, $2)),
  $1, $2)')')
define(`_addwork', `define(`f$2_$3', $1)progress(`hit')insert($@)')
define(`loop', `ifelse(eval($1 > f$2_$3), 1, `progress(`miss')$0(pop)',
  `ifelse(match($2, $3), `$1', `progress(`iter')neighbors($2, $3,
  g$2_$3)$0(pop)')')')

# State: bitmap of nodes still needed, current node
# Heuristic: distance to closest node of interest
define(`path', `addwork(eval((1 << (max+1)) - 2), 0, 0)loop(pop)clean()')
define(`match', `$1, 0')
define(`check', `ifelse(eval($1 & (1 << $2)), 0, `', `d$3_$2,')')
define(`heur', `ifelse(`$1', 0, 0, $1, 1, `d0_$2', `_$0(forloop(1, max,
  `check(`$1', ', `, `$2')'))')')
define(`_heur', `ifelse($2, `', $1, `$0(ifelse(eval($1 < $2), 1, $1, $2),
  shift(shift(@)))')')
define(`neighbors', `ifelse($1, 1, `_$0($1, 0, $2, $3)', `forloop(1, max,
  `_$0($1,', `, $2, $3)')')')
define(`_neighbors', `ifelse(eval($1 & (1 << $2)), 0, `', `addwork(eval(
  $1 & ~(1 << $2)), $2, eval(d$2_$3 + $4))')')
define(`part1', path)

define(`path', `addwork(eval((1 << (max+1)) - 1), 0, 0)loop(pop)clean()')
define(`part2', path)

output(1, `hits:'hit` misses:'miss` iters:'iter)

divert`'part1
part2
