#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <ctype.h>

// cheat by pre-inspecting: all values and bots between 0 and 255 exclusive
#define MAX 255
typedef struct bot bot;
struct bot {
  unsigned char v1;
  unsigned char v2;
  unsigned char lo;
  unsigned char hi;
  bool out_lo;
  bool out_hi;
  bool done;
} bots[MAX];
unsigned char out[MAX];

int main(int argc, char **argv)
{
  ssize_t nread;
  size_t len = 0;
  char *line = NULL;
  int inputs = 0;
  int tasks = 0;
  while ((nread = getline(&line, &len, stdin)) >= 0) {
    if (*line == 'v') {
      int v, b;
      inputs++;
      if (sscanf (line, "value %d goes to bot %d\n", &v, &b) != 2)
	return 1;
      if (bots[b].v1)
	bots[b].v2 = v;
      else
	bots[b].v1 = v;
    } else {
      int b0, b1, b2;
      char t1, t2;
      tasks++;
      if (sscanf (line, "bot %d gives low to %c%*s %d and high to %c%*s %d\n",
		  &b0, &t1, &b1, &t2, &b2) != 5)
	return 1;
      if (t1 == 'o')
	bots[b0].out_lo = true;
      if (t2 == 'o')
	bots[b0].out_hi = true;
      bots[b0].lo = b1;
      bots[b0].hi = b2;
    }
  }
  printf ("%d inputs are sorted among %d bots\n", inputs, tasks);
  bool change = true;
  int goal = 0;
  int iter = 0;
  while (change) {
    printf (" iteration %d\n", iter++);
    change = false;
    for (int i = 0; i < MAX; i++)
      if (!bots[i].done && bots[i].v1 && bots[i].v2) {
	printf ("tracing bot %d, v1 %d, v2 %d, lo %d, hi %d\n",
		i, bots[i].v1, bots[i].v2, bots[i].lo, bots[i].hi);
	bots[i].done = change = true;
	if (bots[i].v1 > bots[i].v2) {
	  unsigned char tmp = bots[i].v1;
	  bots[i].v1 = bots[i].v2;
	  bots[i].v2 = tmp;
	}
	unsigned char *l, *h;
	if (bots[i].out_lo)
	  l = &out[bots[i].lo];
	else if (bots[bots[i].lo].v1)
	  l = &bots[bots[i].lo].v2;
	else
	  l = &bots[bots[i].lo].v1;
	if (bots[i].out_hi)
	  h = &out[bots[i].hi];
	else if (bots[bots[i].hi].v1)
	  h = &bots[bots[i].hi].v2;
	else
	  h = &bots[bots[i].hi].v1;
	*l = bots[i].v1;
	*h = bots[i].v2;
	if (bots[i].v1 == 17 && bots[i].v2 == 61)
	  goal = i;
      }
  }
  printf ("values 17 and 61 are compared by bot %d\n", goal);
  printf ("first three outputs have product %d\n", out[0] * out[1] * out[2]);
  return 0;
}
