#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <ctype.h>

bool
check (const char *s)
{
  int len = strlen (s);
  printf ("checking %s\n", s);
  for (int i = 0; i < len - 3; i++)
    if (s[i] != s[i + 1] && s[i + 1] == s[i + 2] && s[i] == s[i + 3])
      return true;
  printf ("no abba\n");
  return false;
}

char *
find (char *s)
{
  while (*s && s[1] && s[2]) {
    if (s[0] != s[1] && s[0] == s[2])
      return s;
    s++;
  }
  return NULL;
}

bool
match (const char *h, const char *patt)
{
  char n[4] = { patt[1], patt[0], patt[1] };
  printf ("looking for %s in %s\n", n, h);
  return strstr (h, n);
}

int main(int argc, char **argv)
{
  ssize_t nread;
  size_t len = 0;
  char *line = NULL;
  int count1 = 0, count2 = 0;
  char super[80], hyper[80];
  while ((nread = getline(&line, &len, stdin)) >= 0) {
    line[nread - 1] = '\0';
    char *p = line;
    printf ("parsing %s\n", line);
    super[0] = hyper[0] = '\0';
    char *s = super, *h = hyper;
    char goal = '[';
    while (*p) {
      char *q = strchrnul (p, goal);
      if (goal == '[')
	s += sprintf (s, "%.*s[]", (int) (q - p), p);
      else
	h += sprintf (h, "%.*s ", (int) (q - p), p);
      p = q + !!*q;
      goal ^= 6; // cute ascii toggle between [ and ]
    }
    if (check (s = super) && ! check (hyper))
      count1++;
    while ((s = find (s)))
      if (match (hyper, s++)) {
	count2++;
	break;
      }
  }
  printf ("final count: TLS %d, SSL %d\n", count1, count2);
  return 0;
}
