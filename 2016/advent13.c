#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <ctype.h>

#define MAX 75
int grid[MAX][MAX];

int main(int argc, char **argv)
{
  int seed = 1350;
  if (argc == 2)
    seed = atoi (argv[1]);
  int i, j;
  for (i = 0; i < MAX; i++)
    for (j = 0; j < MAX; j++)
      grid[i][j] = (~__builtin_popcount (j*j + 3*j + 2*i*j + i + i*i
					 + seed) & 1) - 1;
  int gen = 1;
  grid[1][1] = gen;
  int set = 1;
  bool change = true;
  while (change) {
    change = false;
    for (i = 0; i < MAX; i++)
      for (j = 0; j < MAX; j++)
	if (grid[i][j] == gen) {
	  if (i && !grid[i - 1][j]) {
	    change = true;
	    if (gen < 51)
	      set++;
	    grid[i - 1][j] = gen + 1;
	  }
	  if (i + 1 < MAX && !grid[i + 1][j]) {
	    change = true;
	    if (gen < 51)
	      set++;
	    grid[i + 1][j] = gen + 1;
	  }
	  if (j && !grid[i][j - 1]) {
	    change = true;
	    if (gen < 51)
	      set++;
	    grid[i][j - 1] = gen + 1;
	  }
	  if (j + 1 < MAX && !grid[i][j + 1]) {
	    change = true;
	    if (gen < 51)
	      set++;
	    grid[i][j + 1] = gen + 1;
	  }
	}
    gen++;
  }
  if (getenv ("DEBUG"))
    for (i = 0; i < MAX; i++) {
      for (j = 0; j < MAX; j++)
	putchar (grid[i][j] > 0 ? 'O' : grid[i][j] ? '#' : '.');
      putchar ('\n');
    }
  printf ("steps to x=31,y=39 is %d\n", grid[39][31] - 1);
  printf ("%d places reachable within 50 steps\n", set);
  return 0;
}
