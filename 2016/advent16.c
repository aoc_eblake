#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#define GOAL 35651584
char buf[GOAL * 2];

int
main (int argc, char **argv)
{
  int goal = GOAL;
  char *seed = "01110110101001000";
  if (argc > 1)
    seed = argv[1];
  if (argc == 3)
    goal = atoi (argv[2]);
  int len = stpcpy (buf, seed) - buf;
  while (len < goal) {
    buf[len] = '0';
    for (int i = 0; i < len; i++)
      buf[2 * len - i] = buf[i] ^ 1;
    len = 2 * len + 1;
  }
  len = goal;
  if (len < 50)
    printf ("buf contents: %.*s\n", len, buf);
  do {
    for (char *p = buf, *q = buf; p - buf < len; p += 2, q++)
      *q = '0' + (p[0] == p[1]);
    buf[len >>= 1] = '\0';
    printf ("checksum length %d\n", len);
  } while (!(len & 1));
  printf ("final checksum: %s\n", buf);
  return 0;
}
