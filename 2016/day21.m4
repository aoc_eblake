divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day21.input] day21.m4

include(`common.m4')ifelse(common(21), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`list', translit(include(defn(`file')), nl, `;'))
define(`line', `_$0(translit(`$1', ` ', `,'))')
define(`_line', `pushdef(`inst', `$@')')
ifdef(`__gnu__', `
  patsubst(defn(`list'), `\([^;]*\);', `line(`\1')')
',`
  define(`chew', `line(substr(`$1', 0, index(`$1', `;')))define(`tail',
    substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(`tail'), `;'), -1,
    `', `$0(defn(`tail'))')')
  define(`split', `ifelse(eval($1 < 75), 1, `chew(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  split(len(defn(`list')), defn(`list'))
')
define(`swapposition_', `swapletter_(`$1', `', `', substr(`$1', $4, 1), `', `',
  substr(`$1', $7, 1))')
define(`swapletter_', `translit(`$1', `$4$7', `$7$4')')
define(`rotateleft_', `substr(`$1', $4)`'substr(`$1', 0, $4)')
define(`rotateright_', `rotateleft_(`$1', `', `', eval(8 - $4))')
define(`rotatebased_', `rotateright_(`$1', `', `', eval((5 * index(`$1',
  $8) + 4) / 4 % 8))')
define(`reversepositions_', `ifelse(eval($6 > $4), 1, `$0(swapposition_(`$1',
  `', `', `$4', `', `', `$6'), `', `', incr($4), `', decr($6))', `$1')')
define(`moveposition_', `_$0(substr(`$1', 0, $4)`'substr(`$1', incr($4)),
  substr(`$1', $4, 1), $7)')
define(`_moveposition_', `substr(`$1', 0, $3)`$2'substr(`$1', $3)')

define(`rswapposition_', `swapposition_($@)')
define(`rswapletter_', `swapletter_($@)')
define(`rrotateleft_', `rotateright_($@)')
define(`rrotateright_', `rotateleft_($@)')
define(`rrotatebased_', `rotateleft_(`$1', `', `', defn(`rrb'index(`$1', $8)))')
define(`rrb0', 1)define(`rrb1', 1)define(`rrb2', 6)define(`rrb3', 2)
define(`rrb4', 7)define(`rrb5', 3)define(`rrb6', 0)define(`rrb7', 4)
define(`rreversepositions_', `reversepositions_($@)')
define(`rmoveposition_', `moveposition_(`$1', `', `', $7, `', `', $4)')

define(`w', `fbgdceah')
define(`do', `define(`w', r$1$2_(w, $@))')
stack_reverse(`inst', `rev', `do(inst)')
define(`part2', w)

define(`w', `abcdefgh')
define(`do', `define(`w', $1$2_(w, $@))')
stack_reverse(`rev', `inst', `do(inst)')
define(`part1', w)

divert`'part1
part2
