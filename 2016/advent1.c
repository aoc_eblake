#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>

typedef enum dir {
  N,
  E,
  S,
  W,
} dir;

#define BASE 220
bool grid[2*BASE][2*BASE]; // cheat: size it by analysis done in part 1
int x = BASE, y = BASE;

void travel(int dx, int dy, int n)
{
  static bool found;
  while (n--) {
    x += dx;
    y += dy;
    if (grid[x][y] && !found) {
      found = true;
      printf ("revisiting spot %d,%d, distance of %d\n", x - BASE, y - BASE,
	      abs (x - BASE) + abs (y - BASE));
    }
    grid[x][y] = true;
  }
}

int main(void)
{
  dir d = N;
  char c;
  int l;
  int maxx = BASE, minx = BASE, maxy = BASE, miny = BASE;
  grid[x][y] = true;
  while (scanf ("%c%d, ", &c, &l) == 2) {
    if (c == 'R')
      d++;
    else
      d--;
    switch (d % 4) {
    case N:
      travel (0, 1, l);
      if (y > maxy)
	maxy = y;
      break;
    case E:
      travel (1, 0, l);
      if (x > maxx)
	maxx = x;
      break;
    case S:
      travel (0, -1, l);
      if (y < miny)
	miny = y;
      break;
    case W:
      travel (-1, 0, l);
      if (x < minx)
	minx = x;
      break;
    }
  }
  printf ("final distance is %d\n", abs (x - BASE) + abs (y - BASE));
  printf ("grid size x=%d to %d y=%d to %d\n", minx - BASE, maxx - BASE,
	  miny - BASE, maxy - BASE);
  return 0;
}
