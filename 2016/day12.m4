divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day12.input] day12.m4

include(`common.m4')ifelse(common(12), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`input', translit((include(defn(`file'))), nl`,()', `;'))
define(`cnt', 0)
define(`_inc_', ```$1''')
define(`line', `_$0(translit(`$1', ` ', `,'))')
define(`_line', `define(`i'cnt, `$1_(`$2', `$3')')ifelse(`$1$3', `jnz-2',
  `pushdef(`i'cnt, `hck_('first(`_'defn(`i'decr(decr(cnt))))`, `$2')')')define(
  `cnt', incr(cnt))')
ifdef(`__gnu__', `
  patsubst(defn(`input'), `\([^;]*\);', `line(`\1')')
', `
  define(`_chew', `line(substr(`$1', 0, index(`$1', `;')))define(`tail',
    substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(`tail'), `;'), -1,
    `', `$0(defn(`tail'))')')
  define(`chew', `ifelse(eval($1 < 20), 1, `_$0(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  chew(len(defn(`input')), defn(`input'))
')

define(`r', `ifdef(`r$1', `r$1', `$1')')
define(`cpy_', `define(`r$2', r(`$1'))1')
define(`inc_', `define(`r$1', incr(r$1))1')
define(`dec_', `define(`r$1', decr(r$1))1')
define(`jnz_', `ifelse(r(`$1'), 0, 1, $2)')
define(`hck_', `define(`r$1', eval(r$1 + r$2))define(`r$2', 0)1')
define(`run', `ifdef(`i'pc, `_$0(pc)$0`'')')
define(`_run', `define(`pc', eval(pc + i$1()))')

define(`ra', 0)define(`rb', 0)define(`rc', 0)define(`rd', 0)define(`pc', 0)
run()
define(`part1', ra)
define(`ra', 0)define(`rb', 0)define(`rc', 1)define(`rd', 0)define(`pc', 0)
run()
define(`part2', ra)

divert`'part1
part2
