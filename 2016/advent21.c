#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>

#define MAX 100 // cheat by using wc on input file
char buf[30 * MAX];
char *lines[MAX];

int main(int argc, char **argv)
{
  char array[9] = "fbgdceah";
  char tmp[9];
  int limit = strlen (array);
  if (argc > 1)
    limit = atoi (argv[1]);
  if (argc > 2)
    freopen (argv[2], "r", stdin);
  array[limit] = '\0';
  int nread = fread (buf, 1, sizeof buf, stdin);
  char *p = buf;
  int count = 0;
  do {
    lines[count++] = p;
    p = strchr (p, '\n');
    *p++ = '\0';
  } while (p - buf < nread);
  printf ("parsed %d lines, now reversing them\n", count);
  int a, b;
  char c, x, y;
  for (int i = count; i--; ) {
    printf ("iteration %d %s: ", i, lines[i]);
    if (!strncmp (lines[i], "swap", 4)) {
      a = sscanf (lines[i], "swap %c%*s %c with %*s %c", &c, &x, &y);
      assert (a == 3);
      if (c == 'p') {
	c = array[x - '0'];
	array[x - '0'] = array[y - '0'];
	array[y - '0'] = c;
      } else {
	assert (c == 'l');
	for (a = 0; a < limit; a++)
	  if (array[a] == x)
	    array[a] = y;
	  else if (array[a] == y)
	    array[a] = x;
      }
    } else if (!strncmp (lines[i], "rotate", 6)) {
      a = sscanf (lines[i], "rotate %c", &c);
      assert (a == 1);
      if (c == 'b') {
	a = sscanf (&lines[i][7], "%*s on position of letter %c", &c);
	assert (a == 1);
	b = strchr (array, c) - array;
	assert (b < limit);
	b = (b & 1) ? limit - (b + 1) / 2 : b ? 3 - b / 2 : 7;
      } else {
	a = sscanf (&lines[i][7], "%*s %d s%*s", &b);
	assert (a == 1);
	if (c == 'r')
	  b = limit - b;
      }
      for (a = 0; a < limit; a++)
	tmp[(a + b) % limit] = array[a];
      memcpy (array, tmp, limit);
    } else if (!strncmp (lines[i], "reverse", 7)) {
      c = sscanf (lines[i], "reverse positions %d through %d", &a, &b);
      assert (c == 2);
      for (c = 0; c < (b - a + 1) / 2; c++) {
	x = array[c + a];
	array[c + a] = array[b - c];
	array[b - c] = x;
      }
    } else {
      assert (!strncmp (lines[i], "move", 4));
      c = sscanf (lines[i], "move position %d to position %d", &b, &a);
      assert (c == 2);
      x = array[a];
      if (a < b)
	memmove (array + a, array + a + 1, b - a);
      else
	memmove (array + b + 1, array + b, a - b);
      array[b] = x;
    }
    printf ("%s\n", array);
  }
  printf ("original string: %s\n", array);
  return 0;
}
