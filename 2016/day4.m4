divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day4.input] day4.m4

include(`common.m4')ifelse(common(4), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`list', translit(include(defn(`file')), alpha`'nl`[]', ALPHA`;'))
define(`part1', 0)
define(`letter', `defn(`$0$1')')
define(`setup', `define(`letter$1', translit(eval($1 + 9, 36), alpha,
  ALPHA))define(`_'letter$1, $1)')
forloop_arg(1, 26, `setup')define(`letter0', defn(`letter26'))
define(`pair', `ifelse(index(`$2', `$1'), -1, `_$0(`$3', `$1')')')
define(`_pair', `ifelse(eval($1_ < $2_ || (_$1 > _$2 && $1_ == $2_)), 1, -)')
define(`room', `forloop(1, 26, `_$0(letter(', `), `$1', $2)')ifelse(_pair(`$4',
  `$5')_pair(`$5', `$6')_pair(`$6', `$7')_pair(`$7', `$8')forloop(1, 26,
  `pair(letter(', `), `$4$5$6$7$8', `$8')'), `', `decode(`$1', `$3')')')
define(`decode', `define(`part1', eval(part1 + $2))ifelse(index(_$0($2,
  substr(`$1', 0, 1), substr(`$1', 1)), `NORTH'), -1, `', `define(`part2',
  $2)')')
define(`_decode', `ifelse(`$2', `', `', `ifelse(`$2', `-', ` ', `letter(eval(
  (_$2 + $1) % 26))')$0($1, substr(`$3', 0, 1), substr(`$3', 1))')')
define(`_room', `define(`$1_', eval($3 - len(translit(`$2', `$1'))))')
ifdef(`__gnu__', `
  patsubst(defn(`list'), `\([A-Z-]*\)\([0-9]*\)\(.\)\(.\)\(.\)\(.\)\(.\);',
    `room(`\1', len(`\1'), \2, `\3', `\4', `\5', `\6', `\7')')
',`
  define(`prep', `_$0(`$1', eval(len(`$1') - 8))')
  define(`_prep', `room(substr(`$1', 0, $2), $2, substr(`$1', $2, 3)forloop(0,
    4, `, substr(`$1', eval($2 + 3 + ', `), 1)'))')
  define(`chew', `prep(substr(`$1', 0, index(`$1', `;'))))define(`tail',
    substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(`tail'), `;'),
    -1, `', `$0(defn(`tail'))')')
  define(`split', `ifelse(eval($1 < 140), 1, `chew(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  split(len(defn(`list')), defn(`list'))
')

divert`'part1
part2
