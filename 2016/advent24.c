#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <ctype.h>
#include <assert.h>

// determined by pre-inspecting input with wc
#define COLS 181
#define ROWS 37
#define KEYS 8

typedef struct point point;
typedef struct key key;
struct point {
  char c;
  int distance;
};
static point grid[ROWS][COLS];
struct key {
  int x;
  int y;
  int d[KEYS];
};
static key keys[KEYS];

static int min = 99999999;

static void
permute (int *a, int size, int n)
{
  int i, t;
  if (size == 1) {
    int k = 0;
    int d = 0;
    if (getenv ("DEBUG"))
      printf ("path 0");
    for (i = 0; i < n; i++) {
      if (getenv ("DEBUG"))
	printf (" %d", a[i]);
      d += keys[k].d[a[i]];
      k = a[i];
    }
    d += keys[k].d[0];
    if (getenv ("DEBUG"))
      printf (": %d\n", d);
    if (d < min)
      min = d;
    return;
  }
  for (i = 0; i < size; i++) {
    permute (a, size - 1, n);
    if (size & 1) {
      t = a[0];
      a[0] = a[size - 1];
      a[size - 1] = t;
    } else {
      t = a[i];
      a[i] = a[size - 1];
      a[size - 1] = t;
    }
  }
}

int main(int argc, char **argv)
{
  int i = 0, j = 0;
  int c;
  int count = 0;
  while ((c = getchar ()) >= 0) {
    switch (c) {
    case '0' ... '7':
      keys[c - '0'].x = j;
      keys[c - '0'].y = i;
      count++;
      // fallthrough
    case '#':
    case '.':
      grid[i][j++].c = c;
      break;
    case '\n':
      i++;
      assert (j <= COLS);
      j = 0;
      break;
    default:
      assert (false);
    }
  }
  assert (i <= ROWS);
  printf ("parsed grid, found %d interesting points to visit\n", count);
  for (int k = 0; k < count; k++) {
    printf ("finding distances from key %d:", k);
    for (i = 0; i < ROWS; i++)
      for (j = 0; j < COLS; j++)
	grid[i][j].distance = -(grid[i][j].c == '#');
    grid[keys[k].y][keys[k].x].distance = 1;
    int found = 0;
    int gen = 1;
    while (found < count) {
      for (i = 0; i < ROWS; i++)
	for (j = 0; j < COLS; j++)
	  if (grid[i][j].distance == gen) {
	    if (grid[i][j].c != '.') {
	      found++;
	      keys[k].d[grid[i][j].c - '0'] = gen - 1;
	    }
	    if (!grid[i - 1][j].distance)
	      grid[i - 1][j].distance = gen + 1;
	    if (!grid[i + 1][j].distance)
	      grid[i + 1][j].distance = gen + 1;
	    if (!grid[i][j - 1].distance)
	      grid[i][j - 1].distance = gen + 1;
	    if (!grid[i][j + 1].distance)
	      grid[i][j + 1].distance = gen + 1;
	  }
      gen++;
    }
    for (i = 0; i < count; i++)
      printf ("%4d", keys[k].d[i]);
    printf ("\n");
  }
  int a[] = { 1, 2, 3, 4, 5, 6, 7 };
  permute (a, count - 1, count - 1);
  printf ("best case visit requires %d steps\n", min);
  return 0;
}
