#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>

int main(void)
{
  int key = '5';
  printf ("code is ");
  int c;
  while ((c = getchar ()) >= 0) {
    key = key >= 'A' ? key - 'A' + 9 : key - '1';
    switch (c) {
    case 'U':
      key = key["121452349678B"];
      break;
    case 'R':
      key = key["134467899BCCD"];
      break;
    case 'D':
      key = key["36785ABC9ADCD"];
      break;
    case 'L':
      key = key["122355678AABD"];
      break;
    case '\n':
      putchar (key = key["123456789ABCD"]);
    }
  }
  printf ("\n");
  return 0;
}
