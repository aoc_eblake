#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <ctype.h>

#define MAX 1000 // determined by wc on the input

typedef struct particle particle;
struct particle {
  int px;
  int py;
  int pz;
  int vx;
  int vy;
  int vz;
  int ax;
  int ay;
  int az;
  bool dead;
} field[MAX];

int
main (int argc, char **argv)
{
  int minrow;
  int minvalue = 999999;
  particle *p = field;
  while (scanf ("p=<%d,%d,%d>, v=<%d,%d,%d>, a=<%d,%d,%d>\n", &p->px, &p->py,
		&p->pz, &p->vx, &p->vy, &p->vz, &p->ax, &p->ay, &p->az) == 9) {
    int value = abs (p->ax) + abs (p->ay) + abs (p->az);
    if (value < minvalue) {
      minvalue = value;
      minrow = p - field;
    }
    p++;
  }
  printf ("closest particle among %d is %d\n", (int) (p - field), minrow);
  int gen = 0, i, j;
  bool done = false;
  while (!done) {
    // remove collisions
    done = true;
    for (i = 0; i < MAX - 1; i++) {
      bool dead = false;
      if (field[i].dead)
	continue;
      for (j = i + 1; j < MAX; j++)
	if (!field[j].dead && field[i].px == field[j].px &&
	    field[i].py == field[j].py && field[i].pz == field[j].pz)
	  dead = field[j].dead = true;
      field[i].dead = dead;
    }
    // move particles; keep iterating if position or velocity decreases
    int alive = 0;
    for (i = 0; i < MAX; i++) {
      p = &field[i];
      if (p->dead)
	continue;
      alive++;
      int oldp = abs (p->px) + abs (p->py) + abs (p->pz);
      int oldv = abs (p->vx) + abs (p->vy) + abs (p->vz);
      p->vx += p->ax;
      p->vy += p->ay;
      p->vz += p->az;
      p->px += p->vx;
      p->py += p->vy;
      p->pz += p->vz;
      if (oldp > abs (p->px) + abs (p->py) + abs (p->pz) ||
	  oldv > abs (p->vx) + abs (p->vy) + abs (p->vz))
	done = false;
    }
    printf ("at time %d with %d particles\n", gen, alive);
    gen++;
  }
  printf ("all particles resolved\n");
  return 0;
}
