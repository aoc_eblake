#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <ctype.h>

typedef struct state state;
typedef struct elt elt;
struct elt {
  long long v;
  elt *next;
};
struct state {
  long long regs[26];
  unsigned int pc;
  elt *head;
  elt *tail;
  int sent;
  int count;
} states[2];

char *program[50]; // sized based on pre-inspecting file
int instr;

long long
get (state *s, const char *value)
{
  if (isalpha(*value))
    return s->regs[*value - 'a'];
  return atoi (value);
}

void
set (state *s, const char *reg, long long value)
{
  s->regs[*reg - 'a'] = value;
  if (getenv ("DEBUG"))
    printf (" %s=%lld\n", reg, value);
}

bool
run (state *s, int id)
{
  elt *e;
  while (s->pc < instr) {
    char arg1[10], arg2[10];
    if (getenv ("DEBUG"))
      printf ("p%d instr %d at pc %d: %s\n", id, s->count, s->pc,
	      program[s->pc]);
    sscanf (program[s->pc], "%*s %9s %9s", arg1, arg2);
    switch ((program[s->pc][0] << 8) + program[s->pc][1]) {
    case ('s' << 8) + 'n': // snd X
      e = malloc (sizeof *e);
      e->v = get (s, arg1);
      e->next = NULL;
      if (s->tail)
	s->tail->next = e;
      else
	s->head = e;
      s->tail = e;
      s->sent++;
      break;
    case ('s' << 8) + 'e': // set X Y
      set (s, arg1, get (s, arg2));
      break;
    case ('a' << 8) + 'd': // add X Y
      set (s, arg1, get (s, arg1) + get (s, arg2));
      break;
    case ('m' << 8) + 'u': // mul X Y
      set (s, arg1, get (s, arg1) * get (s, arg2));
      break;
    case ('m' << 8) + 'o': // mod X Y
      set (s, arg1, get (s, arg1) % get (s, arg2));
      break;
    case ('r' << 8) + 'c': // rcv X
      if (!states[!id].head)
	return true;
      e = states[!id].head;
      states[!id].head = e->next;
      if (!e->next)
	states[!id].tail = NULL;
      set (s, arg1, e->v);
      free (e);
      break;
    case ('j' << 8) + 'g': // jgz X Y
      if (get (s, arg1) > 0)
	s->pc += get (s, arg2) - 1;
      break;
    default:
      return 1;
    }
    s->count++;
    s->pc++;
  }
  return false;
}

int main(int argc, char **argv)
{
  char buf[400]; // sized based on pre-inspecting file
  int nread = fread (buf, 1, sizeof buf, stdin);
  char *p = buf;
  while (p < buf + nread) {
    program[instr++] = p;
    p = strchr (p, '\n');
    *p++ = '\0';
  }
  printf ("program consists of %d instructions\n", instr);
  states[1].regs['p' - 'a'] = 1;
  int swap = 0;
  while ((run (&states[0], 0) + run (&states[1], 1)) &&
	 (states[0].head || states[1].head)) {
    swap++;
    printf ("%d+%d operations, %d swaps, %d+%d sent\n", states[0].count,
	    states[1].count, swap, states[0].sent, states[1].sent);
  }
  printf ("after %d+%d operations and %d swaps, p1 sent %d times\n",
	  states[0].count, states[1].count, swap, states[1].sent);
  return 0;
}
