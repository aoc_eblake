divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dinput=NNN] [-Dfile=day3.input] day3.m4

include(`common.m4')ifelse(common(3), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

ifdef(`input', `', `define(`input', translit(include(defn(`file')), nl))')
define(`find', `ifelse(eval(`$1*$1<$2'), 1, `$0(incr(incr($1)), $2)', $1)')
define(`abs', `ifelse(eval($1 < 0), 1, -($1), $1)')
define(`distance', `define(`base', find(1, $1))define(`last',
  eval(base*base))define(`side', eval(base-1))ifelse($1, 1, 0, `eval(base/2 +
  abs((last - $1)%side - side/2))')')
define(`part1', distance(input))
define(`idx', `eval(100*($2+50)+$1)')
define(`set', `define(`g'idx($1, $2), $3)eval($3 > 'input`)')
define(`get', `_$0(`g'idx($1, $2))')
define(`_get', `ifdef(`$1', `$1', 0)')
set(0, 0, 1)
define(`near', `eval(get(decr($1), decr($2)) + get(decr($1), $2) +
  get(decr($1), incr($2)) + get($1, decr($2)) + get($1, incr($2)) +
  get(incr($1), decr($2)) + get(incr($1), $2) + get(incr($1), incr($2)))')
define(`next', `first(`$0'eval(`4*($4==$3*$3) + ($3*$3-$4-1)/($3-1)'))($@)')
define(`next0', `incr($1), $2, $3')
define(`next1', `$1, decr($2), $3')
define(`next2', `decr($1), $2, $3')
define(`next3', `$1, incr($2), $3')
define(`next4', `incr($1), $2, incr(incr($3))')
define(`do', `ifelse(set($1, $2, near($1, $2)), 1, `define(`part2', get($1,
  $2))', `$0(next($1, $2, $3, $4), incr($4))')')
do(1, 0, 3, 2)

divert`'part1
part2
