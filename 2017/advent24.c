#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <ctype.h>
#include <assert.h>

#define MAX 60 // pre-inspect input
typedef struct pair pair;
static struct pair {
  int a;
  int b;
} pairs[MAX];
static int npairs;
static int chains;
static int max_d;
static int max;

void
visit (int depth, int sum, int current, unsigned long long remaining)
{
  bool found = false;
  printf ("%d, %d, %d, %llx\n", depth, sum, current, remaining);
  for (int i = 0; i < 64; i++)
    if (remaining & (1ULL << i)) {
      if (pairs[i].a == current) {
	found = true;
	visit (depth + 1, sum + pairs[i].a + pairs[i].b, pairs[i].b,
	       remaining & ~(1ULL << i));
      } else if (pairs[i].b == current) {
	found = true;
	visit (depth + 1, sum + pairs[i].a + pairs[i].b, pairs[i].a,
	       remaining & ~(1ULL << i));
      }
    }
  if (!found) {
    chains++;
    if (depth > max_d) {
      max_d = depth;
      max = 0;
    }
    if (depth == max_d && sum > max)
      max = sum;
  }
}

int main(int argc, char **argv)
{
  while (scanf ("%d/%d\n", &pairs[npairs].a, &pairs[npairs].b) == 2)
    npairs++;
  assert(npairs < MAX);
  visit (0, 0, 0, (1ULL << npairs) - 1);
  printf ("found %d chains, max %d\n", chains, max);
  return 0;
}
