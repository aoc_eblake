#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

void hash (const char *in, char hash[16])
{
  const int size = 256;
  unsigned char list[size];
  int i;
  for (i = 0; i < size; i++)
    list[i] = i;
  int skip = 0;
  int pos = 0;
  const char suffix[6] = { 17, 31, 73, 47, 23 };
  char *str = malloc(strlen(in) + 5 + 1);
  strcpy(stpcpy(str, in), suffix);
  for (int j = 0; j < 64; j++) {
    for (char *p = str; *p; p++) {
      int length = *p;
      for (i = 0; i < length / 2; i++) {
	int tmp = list[(i + pos) % size];
	list[(i + pos) % size] = list[(length - i - 1 + pos) % size];
	list[(length - i - 1 + pos) % size] = tmp;
      }
      pos += length + skip++;
    }
  }
  char *chunk = hash;
  for (i = 0; i < size; i++) {
    if (i % 16 == 0)
      *chunk = 0;
    *chunk ^= list[i];
    if (i % 16 == 15)
      chunk++;
  }
  free (str);
}

int grid[128][128];

void visit (int n, int x, int y)
{
  if (x < 0 || y < 0 || x > 127 || y > 127)
    return;
  if (grid[x][y] == 1) {
    grid[x][y] = n;
    visit (n, x + 1, y);
    visit (n, x - 1, y);
    visit (n, x, y + 1);
    visit (n, x, y - 1);
  }
}

int main(int argc, char **argv)
{
  if (argc != 2)
    return 1;
  char h[16];
  char *str;
  int len = asprintf(&str, "%s-127", argv[1]);
  char *p = str + len - 3;
  int count = 0;
  int i, j;
  for (i = 0; i < 128; i++) {
    sprintf(p, "%d", i);
    hash (str, h);
    if (getenv ("DEBUG")) {
      printf ("hash of %s was ", str);
      for (j = 0; j < sizeof h; j++)
	printf ("%02hhx", h[j]);
      putchar ('\n');
    }
    for (j = 0; j < sizeof h; j++)
      for (int k = 0; k < 8; k++)
	if ((1 << (7 - k)) & h[j])
	  grid[i][j * 8 + k] = !!++count;
  }
  printf ("total of %d dirty blocks\n", count);
  int regions = 1;
  for (i = 0; i < 128; i++)
    for (j = 0; j < 128; j++)
      if (grid[i][j] == 1) {
	if (getenv ("DEBUG"))
	  printf ("marking region %d at %d,%d\n", regions, i, j);
	visit (++regions, i, j);
      }
  printf ("total of %d regions\n", regions - 1);
  return 0;
}
