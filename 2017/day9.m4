divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day9.input] day9.m4

include(`common.m4')ifelse(common(9), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`input', translit((include(defn(`file'))), `,`'()'nl, `;[]'))
define(`depth', 0)define(`part1', 0)define(`part2', 0)
define(`visitc', `popdef(`$0')')
define(`visitg', `ifelse(`$1', `>', `popdef(`$0')', `$1', `!', `pushdef(`$0',
  defn(`visitc'))', `define(`part2', incr(part2))')')
define(`visit', `ifelse(`$1', `{', `define(`depth', incr(depth))define(`part1',
  eval(part1 + depth))', `$1', `}', `define(`depth', decr(depth))', `$1', `<',
  `pushdef(`$0', defn(`visitg'))')')
ifdef(`__gnu__', `
  patsubst(defn(`input'), `.', `visit(`\&')')
',`
  define(`split', `ifelse($1, 1, `visit(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)), `$3')$0(eval($1 - $1/2), substr(`$2',
    eval($1/2)), `$3')')')
  split(len(defn(`input')), defn(`input'))
')

divert`'part1
part2
