divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dlimit=N] [-Dhashsize=H] [-Dfile=day22.input] day22.m4
# Optionally use -Dverbose=1 to see some progress

include(`common.m4')ifelse(common(22, 65537), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

ifdef(`limit', `', `define(`limit', 10000000)')

define(`F', defn(`ifdef'))define(`D', defn(`define'))define(`E', defn(`eval'))
define(`P', defn(`popdef'))define(`I', defn(`incr'))define(`C', defn(`decr'))
define(`input', translit(include(defn(`file')), `.#'nl, `01_'))
define(`offset', 500)define(`width', index(defn(`input'), `_'))
define(`p', eval((offset - width/2)*offset*2+(offset - width/2)))
define(`v0', `D(`p', I(`$1'))')
define(`v1', `D(`g$1')D(`G$1', `n2')v0($@)')
define(`v_', `D(`p', eval($1+offset*2-width))')
ifdef(`__gnu__', `
  patsubst(defn(`input'), `.', `v\&(p)')
',`
  define(`split', `ifelse($1, 1, `v$2(p)', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)), `$3')$0(eval($1 - $1/2), substr(`$2',
    eval($1/2)), `$3')')')
  split(len(defn(`input')), defn(`input'))
')
define(`part1', 0)define(`T', 0)
define(`do10000', `popdef(`$0')')
define(`n0', `D(`$1')D(`part1', I(part1))l$2')
define(`n1', `P(`$1')r$2')
define(`do', `_$0(`$1',F(`g$1',`n1',`n0')(`g$1',`$2'),`$3')')
define(`_do', `F(`do$3',`do$3',`do')(m$2(`$1'),`$2',I(`$3'))')
define(`r0', ``1'')define(`r1', ``2'')define(`r2', ``3'')define(`r3', ``0'')
define(`l0', ``3'')define(`l1', ``0'')define(`l2', ``1'')define(`l3', ``2'')
define(`m0', `E(`$1-''dquote(eval(offset*2))`)')
define(`m1', `I(`$1')')
define(`m2', `E(`$1+''dquote(eval(offset*2))`)')
define(`m3', `C(`$1')')
do(eval(offset*offset*2+offset), 0, 1)

define(`rename', `ifdef(`$2', `', `define(`$2', defn(`$1'))popdef(`$1')')')
define(`do'100000, `output(1, `...'decr($3))rename(`$0',
  `do'eval(`$3+99999'))do($@)')
define(`do'limit)
define(`b0', ``2'')define(`b1', ``3'')define(`b2', ``0'')define(`b3', ``1'')
define(`n0', `D(`$1',`n1')l$2')
define(`n1', `D(`$1',`n2')D(`T',I(T))`$2'')
define(`n2', `D(`$1',`n3')r$2')
define(`n3', `P(`$1')b$2')
define(`do', `_$0(`$1',F(`G$1',`G$1()',`n0')(`G$1',`$2'),`$3')')
do(eval(offset*offset*2+offset), 0, 1)
define(`part2', T)

divert`'part1
part2
