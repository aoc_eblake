#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>

int main(int argc, char **argv)
{
  int step = 328;
  if (argc == 2)
    step = atoi (argv[1]);
  int len = 1;
  int value = 0;
  int cur = 0;
  for (int i = 1; i <= 50000000; i++) {
    if (!(i % 10000))
      printf ("\b\b\b\b\b\b\b\b\b%9d", i), fflush (stdout);
    int next = (cur + step) % len;
    if (!next)
      value = i;
    len++;
    cur = next + 1;
    if (getenv ("DEBUG") && i < 10) {
      printf ("iter %d: cur/len is %d/%d, val after 0 is %d\n", i, cur, len,
	      value);
    }
  }
  printf ("\nnext is %d\n", value);
  return 0;
}
