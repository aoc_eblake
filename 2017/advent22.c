#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <ctype.h>
#include <assert.h>

#define MAX 3000  // guessing that 10000 iterations won't grow that large

enum dir {
  U,
  R,
  D,
  L,
} d;

enum state {
  C,
  W,
  I,
  F,
};

static enum state grid[MAX][MAX];

int
main (int argc, char **argv)
{
  int cycles = 10000000;
  if (argc > 1)
    cycles = atoi (argv[1]);
  ssize_t nread;
  size_t len = 0;
  char *line = NULL;
  bool first = true;
  int x, y;
  int count = 0;
  while ((nread = getline(&line, &len, stdin)) >= 0) {
    if (first) {
      assert ((nread & 1) == 0);
      first = false;
      x = y = MAX / 2 - nread / 2;
      printf ("populating starting at %d,%d\n", x, y);
    }
    char *p = line;
    while (*p != '\n') {
      if (*p++ == '#')
	grid[y][x] = I;
      x++;
    }
    y++;
    x -= p - line;
    count++;
  }
  x += count / 2;
  y -= count / 2 + 1;
  printf ("parsed %dx%d inputs, starting at %d,%d\n", count, count, x, y);
  int infected = 0;
  for (int i = 0; i < cycles; i++) {
    switch (grid[y][x]) {
    case C:
      d += 3;
      break;
    case W:
      break;
    case I:
      d++;
      break;
    case F:
      d += 2;
      break;
    default:
      assert (false);
    }
    infected += (grid[y][x] = (grid[y][x] + 1) % 4) == I;
    switch (d % 4) {
    case U:
      y--;
      break;
    case R:
      x++;
      break;
    case D:
      y++;
      break;
    case L:
      x--;
      break;
    default:
      assert (false);
    }
    if (!x || !y || x == MAX - 1 || y == MAX - 1) {
      printf ("grid is too small at iteration %d\n", i);
      return 1;
    }
  }
  printf ("after %d cycles, infected %d nodes\n", cycles, infected);
  return 0;
}
