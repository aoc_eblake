divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dhashsize=H] [-Dfile=day8.input] day8.m4

include(`common.m4')ifelse(common(8, 65537), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`inc', +)define(`dec', -)
define(`part2', 0)
define(`input', translit(include(defn(`file')), nl, `;'))
define(`get', `ifdef(`r_$1', `r_$1', 0)')
define(`set', `ifdef(`r_$1', `', `pushdef(`r_', `$1')')define(`r_$1',
  eval(defn(`r_$1') + 0 $2 $3))ifelse(eval(r_$1 > part2), 1,
  `define(`part2', r_$1)')')
define(`line', `_$0(translit(`$1', ` ', `,'))')
define(`_line', `ifelse(eval(get(`$5') $6 $7), 1, `set(`$1', $2, $3)')')
ifdef(`__gnu__', `
  patsubst(defn(`input'), `\([^;]*\);', `line(`\1')')
',`
  define(`chew', `line(substr(`$1', 0, index(`$1', `;')))define(
    `tail', substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(
    `tail'), `;'), -1, `', `$0(defn(`tail'))')')
  define(`split', `ifelse(eval($1 < 60), 1, `chew(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  split(len(defn(`input')), defn(`input'))
')
define(`part1', 0)
define(`find', `ifelse(eval(r_$1 > part1), 1, `define(`part1', r_$1)')')
_stack_foreach(`r_', `find(', `)', `tmp')

divert`'part1
part2
