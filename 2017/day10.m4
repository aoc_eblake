divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day10.input] day10.m4
# Optionally use -Dalgo=array|stack|pack to experiment with implementation.

include(`common.m4')ifelse(common(10), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

ifdef(`algo', `', `define(`algo', `array')')

define(`input', translit((include(defn(`file'))), `,()'nl, `_'))
define(`prep', `define(`A$1', $1)')
define(`reset', `forloop_arg(0, 255, `prep')define(`p', 0)define(`s', 0)')
define(`swap', `_$0(`A$1', A$1, `A$2', A$2)')
define(`_swap', `define(`$1', $4)define(`$3', $2)')
define(`round', `ifelse(`$1', `', `', `_$0($1, p)define(`p',
  eval((p+$1+s)&255))define(`s', incr(s))$0(shift($@))')')
define(`_round', `ifelse($1, 0, `', $1, 1, `', `swap($2,
  eval(($1+$2-1)&255))$0(decr(decr($1)), ifelse($2, 255, 0, `incr($2)'))')')
reset()
round(translit(defn(`input'), `_', `,'))
define(`part1', eval(A0*A1))

define(`set', `define(`V$1', eval($1 + $2))')
forloop(0, 9, `set(', `, 48)')
define(`V_', 44)
define(`split', `ifelse(`$1', `', ``17,31,73,47,23'', `defn(`V'substr(`$1',
  0, 1))`,'$0(substr(`$1', 1))')')
define(`list', split(defn(`input')))

ifelse(defn(`algo'), `array', `
output(1, `Using per-element array swaps')
reset()
forloop(0, 63, `round(list)')
define(`dense', `eval(0forloop(eval($1*16), eval($1*16+15), `^defn(`A'', `)'),
  16, 2)')
define(`part2', forloop_arg(0, 15, `dense'))

', defn(`algo'), `stack', `
output(1, `Using stack one element at a time')
define(`head', `l0')
define(`_rd', `head`'popdef(defn(`head'))')
pushdef(`l0', `popdef(`l0')define(`head', `l1')head()')
forloop_rev(255, 0, `pushdef(`l0',',`)')
define(`rv1', `pushdef(`l$1', _rd)')
define(`prep', `define(`rv'incr($1), `pushdef(`l$2$3', _rd)rv$1(`$2$3')')')
forloop(1, 254, `prep(', `, `$', 1)')
define(`sk0')
define(`prep', `define(`sk'incr($1), `pushdef(`t$2$3', _rd)sk$1(`$2$3')')')
forloop(0, 254, `prep(', `, `$', 1)')

define(`p', 0)define(`s', 0)
define(`sk', `sk$1(`$2')pushdef(`t$2', `popdef(`l$2')define(`head',
  `l'incr(`$2'))head()')define(`s', `$2')')
define(`rnd', `ifelse(`$1',,, `define(`p',eval((p+$1+s)&255))_$0($1,
  incr(s))$0(shift($@))')')
define(`_rnd', `pushdef(`l$2', `popdef(`l$2')stack_reverse(`t$2',
  `l$2')head()')rv$1(`$2')sk(eval(s&255), `$2')')
forloop(0, 63, `rnd(list)')
define(`last', `pushdef(`l$1', `popdef(`l$1')stack_reverse(`t$1',
  `l$1')head()')rv1($1)sk(eval(255-p), $1)')
last(incr(s))
define(`dense', `eval(0forloop_arg(0, 15, `^_rd'), 16, 2)')
define(`part2', forloop_arg(0, 15, `dense'))

', defn(`algo'), `pack', `
output(1, `Using packed stack')
pushdef(`l0', `0(,1)')
define(`prep', `pushdef(`l0', 8(0forloop(eval($1*8), eval($1*8+7),
  `,')))')
forloop_rev(31, 0, `prep(', `)')
define(`C', `0')
define(`prep', `define(`rv$1',
  `pushdef(`l$2$3', swap$1(`$2$3', _rd(`$1', l$2$4)))')')
forloop(1, 8, `prep(', `, `$', 1, 2)')
define(`prep', `define(`rv$1', `rv8($2@)rv'eval($1-8)`(`$2$3',C)')')
forloop(9, 255, `prep(', `, `$', 1)')
define(`swap1', ``1(`$1',`$2')'')
define(`swap2', ``2(`$1',`$3',`$2')'')
define(`swap3', ``3(`$1',`$4',`$3',`$2')'')
define(`swap4', ``4(`$1',`$5',`$4',`$3',`$2')'')
define(`swap5', ``5(`$1',`$6',`$5',`$4',`$3',`$2')'')
define(`swap6', ``6(`$1',`$7',`$6',`$5',`$4',`$3',`$2')'')
define(`swap7', ``7(`$1',`$8',`$7',`$6',`$5',`$4',`$3',`$2')'')
define(`swap8', ``8(`$1',`$9',`$8',`$7',`$6',`$5',`$4',`$3',`$2')'')
define(`sk0')
define(`prep', `define(`sk$1',
  `pushdef(`t$2$3', $1(`$2$3',_rd(`$1', l$2$4)))')')
forloop(1, 8, `prep(', `, `$', 1, 2)')
define(`prep', `define(`sk$1', `sk8($2@)sk'eval($1-8)`(`$2$3',C)')')
forloop(9, 255, `prep(', `, `$', `1')')

define(`_rd', `_rd$1$2')
define(`_rd10', `ifelse(`$1', `', `define(`C', `$2')', `stack_reverse(`t$1',
  `l$1')')_rd(1, l$1$2)')
define(`_rd11', ``$2'popdef(`l$1')')
define(`_rd12', ``$2'define(`l$1', `1(`$1',`$3')')')
define(`_rd13', ``$2'define(`l$1', `2(`$1',`$3',`$4')')')
define(`_rd14', ``$2'define(`l$1', `3(`$1',`$3',`$4',`$5')')')
define(`_rd15', ``$2'define(`l$1', `4(`$1',`$3',`$4',`$5',`$6')')')
define(`_rd16', ``$2'define(`l$1', `5(`$1',`$3',`$4',`$5',`$6',`$7')')')
define(`_rd17', ``$2'define(`l$1', `6(`$1',`$3',`$4',`$5',`$6',`$7',`$8')')')
define(`_rd18', ``$2'define(`l$1', `7(`$1',`$3',`$4',`$5',`$6',`$7',`$8',`$9')')')
define(`_rd20', `ifelse(`$1', `', `define(`C', `$2')', `stack_reverse(`t$1',
  `l$1')')_rd(2, l$1$2)')
define(`_rd21', ``$2',popdef(`l$1')_rd(1, l$1)')
define(`_rd22', ``$2',`$3'popdef(`l$1')')
define(`_rd23', ``$2',`$3'define(`l$1', `1(`$1',`$4')')')
define(`_rd24', ``$2',`$3'define(`l$1', `2(`$1',`$4',`$5')')')
define(`_rd25', ``$2',`$3'define(`l$1', `3(`$1',`$4',`$5',`$6')')')
define(`_rd26', ``$2',`$3'define(`l$1', `4(`$1',`$4',`$5',`$6',`$7')')')
define(`_rd27', ``$2',`$3'define(`l$1', `5(`$1',`$4',`$5',`$6',`$7',`$8')')')
define(`_rd28', ``$2',`$3'define(`l$1', `6(`$1',`$4',`$5',`$6',`$7',`$8',`$9')')')
define(`_rd30', `ifelse(`$1', `', `define(`C', `$2')', `stack_reverse(`t$1',
  `l$1')')_rd(3, l$1$2)')
define(`_rd31', ``$2',popdef(`l$1')_rd(2, l$1)')
define(`_rd32', ``$2',`$3',popdef(`l$1')_rd(1, l$1)')
define(`_rd33', ``$2',`$3',`$4'popdef(`l$1')')
define(`_rd34', ``$2',`$3',`$4'define(`l$1', `1(`$1',`$5')')')
define(`_rd35', ``$2',`$3',`$4'define(`l$1', `2(`$1',`$5',`$6')')')
define(`_rd36', ``$2',`$3',`$4'define(`l$1', `3(`$1',`$5',`$6',`$7')')')
define(`_rd37', ``$2',`$3',`$4'define(`l$1', `4(`$1',`$5',`$6',`$7',`$8')')')
define(`_rd38', ``$2',`$3',`$4'define(`l$1', `5(`$1',`$5',`$6',`$7',`$8',`$9')')')
define(`_rd40', `ifelse(`$1', `', `define(`C', `$2')', `stack_reverse(`t$1',
  `l$1')')_rd(4, l$1$2)')
define(`_rd41', ``$2',popdef(`l$1')_rd(3, l$1)')
define(`_rd42', ``$2',`$3',popdef(`l$1')_rd(2, l$1)')
define(`_rd43', ``$2',`$3',`$4',popdef(`l$1')_rd(1, l$1)')
define(`_rd44', ``$2',`$3',`$4',`$5'popdef(`l$1')')
define(`_rd45', ``$2',`$3',`$4',`$5'define(`l$1', `1(`$1',`$6')')')
define(`_rd46', ``$2',`$3',`$4',`$5'define(`l$1', `2(`$1',`$6',`$7')')')
define(`_rd47', ``$2',`$3',`$4',`$5'define(`l$1', `3(`$1',`$6',`$7',`$8')')')
define(`_rd48', ``$2',`$3',`$4',`$5'define(`l$1', `4(`$1',`$6',`$7',`$8',`$9')')')
define(`_rd50', `ifelse(`$1', `', `define(`C', `$2')', `stack_reverse(`t$1',
  `l$1')')_rd(5, l$1$2)')
define(`_rd51', ``$2',popdef(`l$1')_rd(4, l$1)')
define(`_rd52', ``$2',`$3',popdef(`l$1')_rd(3, l$1)')
define(`_rd53', ``$2',`$3',`$4',popdef(`l$1')_rd(2, l$1)')
define(`_rd54', ``$2',`$3',`$4',`$5',popdef(`l$1')_rd(1, l$1)')
define(`_rd55', ``$2',`$3',`$4',`$5',`$6'popdef(`l$1')')
define(`_rd56', ``$2',`$3',`$4',`$5',`$6'define(`l$1', `1(`$1',`$7')')')
define(`_rd57', ``$2',`$3',`$4',`$5',`$6'define(`l$1', `2(`$1',`$7',`$8')')')
define(`_rd58', ``$2',`$3',`$4',`$5',`$6'define(`l$1', `3(`$1',`$7',`$8',`$9')')')
define(`_rd60', `ifelse(`$1', `', `define(`C', `$2')', `stack_reverse(`t$1',
  `l$1')')_rd(6, l$1$2)')
define(`_rd61', ``$2',popdef(`l$1')_rd(5, l$1)')
define(`_rd62', ``$2',`$3',popdef(`l$1')_rd(4, l$1)')
define(`_rd63', ``$2',`$3',`$4',popdef(`l$1')_rd(3, l$1)')
define(`_rd64', ``$2',`$3',`$4',`$5',popdef(`l$1')_rd(2, l$1)')
define(`_rd65', ``$2',`$3',`$4',`$5',`$6',popdef(`l$1')_rd(1, l$1)')
define(`_rd66', ``$2',`$3',`$4',`$5',`$6',`$7'popdef(`l$1')')
define(`_rd67', ``$2',`$3',`$4',`$5',`$6',`$7'define(`l$1', `1(`$1',`$8')')')
define(`_rd68', ``$2',`$3',`$4',`$5',`$6',`$7'define(`l$1', `2(`$1',`$8',`$9')')')
define(`_rd70', `ifelse(`$1', `', `define(`C', `$2')', `stack_reverse(`t$1',
  `l$1')')_rd(7, l$1$2)')
define(`_rd71', ``$2',popdef(`l$1')_rd(6, l$1)')
define(`_rd72', ``$2',`$3',popdef(`l$1')_rd(5, l$1)')
define(`_rd73', ``$2',`$3',`$4',popdef(`l$1')_rd(4, l$1)')
define(`_rd74', ``$2',`$3',`$4',`$5',popdef(`l$1')_rd(3, l$1)')
define(`_rd75', ``$2',`$3',`$4',`$5',`$6',popdef(`l$1')_rd(2, l$1)')
define(`_rd76', ``$2',`$3',`$4',`$5',`$6',`$7',popdef(`l$1')_rd(1, l$1)')
define(`_rd77', ``$2',`$3',`$4',`$5',`$6',`$7',`$8'popdef(`l$1')')
define(`_rd78', ``$2',`$3',`$4',`$5',`$6',`$7',`$8'define(`l$1', `1(`$1',`$9')')')
define(`_rd80', `ifelse(`$1', `', `define(`C', `$2')', `stack_reverse(`t$1',
  `l$1')')_rd(8, l$1$2)')
define(`_rd81', ``$2',popdef(`l$1')_rd(7, l$1)')
define(`_rd82', ``$2',`$3',popdef(`l$1')_rd(6, l$1)')
define(`_rd83', ``$2',`$3',`$4',popdef(`l$1')_rd(5, l$1)')
define(`_rd84', ``$2',`$3',`$4',`$5',popdef(`l$1')_rd(4, l$1)')
define(`_rd85', ``$2',`$3',`$4',`$5',`$6',popdef(`l$1')_rd(3, l$1)')
define(`_rd86', ``$2',`$3',`$4',`$5',`$6',`$7',popdef(`l$1')_rd(2, l$1)')
define(`_rd87', ``$2',`$3',`$4',`$5',`$6',`$7',`$8',popdef(`l$1')_rd(1, l$1)')
define(`_rd88', ``$2',`$3',`$4',`$5',`$6',`$7',`$8',`$9'popdef(`l$1')')

define(`p', 0)define(`s', 0)
define(`sk', `sk$1(`$2', `$3')pushdef(`t$2',
  `0(,'incr(`$2')`)')define(`s', `$2')')
define(`rnd', `ifelse(`$1',,, `define(`p',eval((p+$1+s)&255))_$0($1, C,
  incr(s))$0(shift($@))')')
define(`_rnd', `pushdef(`l$3', `0(`$3')')rv$1(`$3', `$2')sk(eval(s&255), `$3',
  `$2')')
forloop(0, 63, `rnd(list)')
pushdef(`l'incr(s), 0(incr(s)))rv1(incr(s), C)sk(eval(255-p), incr(s), C)
define(`read', `_$0(_rd8$1)')
define(`_read', `$1^$2^$3^$4^$5^$6^$7^$8')
define(`dense', `eval(read(defn(`l'C))^read(defn(`l'C)), 16, 2)')
define(`part2', forloop_arg(0, 15, `dense'))
', `errprintn(`unrecognized algo 'algo),m4exit(1)')

divert`'part1
part2
