divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dseed=XYZ] [-Dfile=day14.input] day14.m4
# Optionally use -Dverbose=1 to see some progress

include(`common.m4')ifelse(common(14, 65537), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

ifdef(`seed', `', `define(`seed', translit(include(defn(`file')), nl))')
define(`part1', 0)
define(`D', defn(`define'))define(`E', defn(`eval'))
define(`prep', `define(`I$1', dquote(incr($1)))')
forloop_arg(0, 254, `prep')
define(`I255', ``0'')
define(`prep', `define(`D$1', dquote(decr($1)))')
forloop_arg(1, 255, `prep')
define(`D0', ``255'')
define(`_reset', `D(`A$1', $1)')
define(`reset', `undefine(`list')forloop_arg(0, 255, `_$0')D(`P', 0)D(`S', 0)')
define(`W', `D(`A$1',A$2`'D(`A$2',A$1))')
define(`R', `R$1(`$2',E(`$2+$1-1&255'))D(`P',E(`$2+$3+$1&255'))D(`S',I$3)')
define(`prep', `define(`R$1', `W($'`@)R'decr(D$1)`(I$'1`,D$'2`)')')
forloop_arg(2, 255, `prep')
define(`R0')define(`R1')
define(`set', `define(`V$1', eval($1 + $2))')
forloop(0, 9, `set(', `, 48)')
define(`V_', 45)
define(`set', `define(`V'eval($1+9, 36), eval($1 + $2))')
forloop(1, 26, `set(', `, 96)')
define(`split', `ifelse(`$1',, `_foreach(`pushdef(`list',', `)',, 17, 31, 73,
  47, 23)', `pushdef(`list', defn(`V'substr(`$1', 0, 1)))$0(substr(`$1', 1))')')
define(`dense', `grid(E(0forloop(E(`$1*16'), E(`$1*16+15'),
  `^defn(`A'', `)')), E(`$1*8'), $2)')
define(`grid', `forloop(0, 7, `_$0($1, ', `, $2, 'incr($3)`)')')
define(`_grid', `ifelse(eval(`$1 & 1 << 7-$2'), 0, `',
  `define(`g'eval(`$2+$3+1')`_$4')define(`part1', incr(part1))')')
define(`hash', `output(1, `...$1')reset()split(defn(`seed')`_$1')forloop(0, 63,
  `_stack_foreach(`list', `R(', `, P, S)', `T')')forloop(0, 15,
  `dense(', `, $1)')')
forloop_arg(0, 127, `hash')
define(`part2', 0)
define(`mark', `ifdef(`g$1_$2', `ifelse(g$1_$2, `', `_$0($1, $2,
  part2)define(`part2', incr(part2))')')')
define(`_mark', `ifdef(`g$1_$2', `ifelse(g$1_$2, `', `define(`g$1_$2',
  $3)$0(incr($1), $2, $3)$0($1, incr($2), $3)$0(decr($1), $2, $3)$0($1,
  decr($2), $3)')')')
define(`row', `forloop(1, 128, `mark(', `, $1)')')
forloop_arg(1, 128, `row')

divert`'part1
part2
