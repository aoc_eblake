#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

// cheat - pre-examined input to learn max name length (7+NUL), children (7)
#define MAXNAME 8
#define MAXCHILD 7

typedef struct node node;
struct node {
  char name[MAXNAME];
  int weight;
  int nchildren;
  char childname[MAXCHILD][MAXNAME];
  node *next; // global list form
  node *children[MAXCHILD]; // tree form
  int total;
};
node *list;

static int compute(node *n)
{
  for (int i = 0; i < n->nchildren; i++)
    for (node *iter = list; iter; iter = iter->next)
      if (!strcmp(n->childname[i], iter->name)) {
	n->children[i] = iter;
	n->total += compute(iter);
	break;
      }
  if (n->nchildren && n->total != n->children[0]->total * n->nchildren) {
    printf("node %s is bad, with %d children\n", n->name, n->nchildren);
    for (int i = 0; i < n->nchildren; i++) {
      int w1 = n->children[(i+1) % n->nchildren]->total;
      int w2 = n->children[(i+2) % n->nchildren]->total;
      if (getenv("DEBUG"))
	printf(" child %d: %s %d %d vs. %d %d\n", i, n->children[i]->name,
	       n->children[i]->weight, n->children[i]->total, w1, w2);
      if (n->children[i]->total != w1 && n->children[i]->total != w2) {
	printf("fix by adjusting %s weight to %d\n", n->children[i]->name,
	       n->children[i]->weight - n->children[i]->total + w1);
	break;
      }
    }
  }
  n->total += n->weight;
  return n->total;
}

int main(void)
{
  int count = 0;
  // ugly, but works because our data is sane
  while (1) {
    node *item = calloc(sizeof *item, 1);
    item->nchildren = scanf(" %8[a-z] (%d) -> %8[a-z], %8[a-z], %8[a-z], "
			    "%8[a-z], %8[a-z], %8[a-z], %8[a-z]",
			    item->name, &item->weight, item->childname[0],
			    item->childname[1], item->childname[2],
			    item->childname[3], item->childname[4],
			    item->childname[5], item->childname[6]) - 2;
    if (item->nchildren < 0) {
      free (item);
      break;
    }
    if (getenv("DEBUG"))
      printf("%s (%d) -> %d: %s %s %s %s %s %s %s\n", item->name, item->weight,
	     item->nchildren, item->childname[0], item->childname[1],
	     item->childname[2], item->childname[3], item->childname[4],
	     item->childname[5], item->childname[6]);
    item->next = list;
    list = item;
    count++;
  }
  printf("parsed %d lines\n", count);
  node *root = list;
  int change = 1;
  while (change) {
    change = 0;
    for (node *iter = list; iter; iter = iter->next)
      for (int i = 0; i < iter->nchildren; i++)
	if (!strcmp(root->name, iter->childname[i])) {
	  change++;
	  if (getenv("DEBUG"))
	    printf("%s is parent of %s\n", iter->name, root->name);
	  root = iter;
	}
  }
  printf("root is %s\n", root->name);
  printf("root weight is %d\n", compute(root));
  return 0;
}
