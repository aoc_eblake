#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <ctype.h>
#include <assert.h>

#define MAX 2187  // precomputed for 18 iterations
#define OUT_2 6   // 2x2 transformations: 6 among 16 (others by flip/rotate)
#define OUT_3 102 // 3x3 transformations: 102 among 512 (others by flip/rotate)
static unsigned short in2[1 << 4];
static int in3[1 << 9];
static char buf[4000]; // sized based on wc output
static bool grid[MAX][MAX];

int
main (int argc, char **argv)
{
  // initial pattern is fixed
  int size = 3;
  grid[0][1] = grid[1][2] = grid[2][0] = grid[2][1] = grid[2][2] = true;
  memset(in2, -1, sizeof in2);
  memset(in3, -1, sizeof in3);
  int iter = 5, read2 = OUT_2, read3 = OUT_3;
  if (argc > 1)
    iter = atoi (argv[1]);
  if (argc > 2)
    read2 = atoi (argv[2]);
  if (argc > 3)
    read3 = atoi (argv[3]);
  int x, y, i;
  unsigned short p_in, p_out;
  // read the rules
  i = fread (buf, 1, sizeof buf, stdin);
  assert (i > 0 && i < sizeof buf);
  char *p = buf;
  while (read2--) {
    assert (p[6] == '=' && p[7] == '>' && p[20] == '\n');
    p_in = (((p[0] == '#') << 3) |
	    ((p[1] == '#') << 2) |
	    ((p[3] == '#') << 1) |
	    ((p[4] == '#') << 0));
    p_out = (((p[9] == '#') << 8) |
	     ((p[10] == '#') << 7) |
	     ((p[11] == '#') << 6) |
	     ((p[13] == '#') << 5) |
	     ((p[14] == '#') << 4) |
	     ((p[15] == '#') << 3) |
	     ((p[17] == '#') << 2) |
	     ((p[18] == '#') << 1) |
	     ((p[19] == '#') << 0));
    // For 2x2, rotations is sufficient to cover all flips
    for (i = 0; i < 4; i++) {
      p_in = ((((p_in & (1 << 3)) != 0) << 2) |
	      (((p_in & (1 << 2)) != 0) << 0) |
	      (((p_in & (1 << 1)) != 0) << 3) |
	      (((p_in & (1 << 0)) != 0) << 1));
      if (in2[p_in] <= 0x1ff)
	assert (in2[p_in] == p_out);
      in2[p_in] = p_out;
    }
    p += 21;
  }
  while (read3--) {
    assert (p[12] == '=' && p[13] == '>' && p[34] == '\n');
    p_in = (((p[0] == '#') << 8) |
	    ((p[1] == '#') << 7) |
	    ((p[2] == '#') << 6) |
	    ((p[4] == '#') << 5) |
	    ((p[5] == '#') << 4) |
	    ((p[6] == '#') << 3) |
	    ((p[8] == '#') << 2) |
	    ((p[9] == '#') << 1) |
	    ((p[10] == '#') << 0));
    p_out = (((p[15] == '#') << 15) |
	     ((p[16] == '#') << 14) |
	     ((p[17] == '#') << 13) |
	     ((p[18] == '#') << 12) |
	     ((p[20] == '#') << 11) |
	     ((p[21] == '#') << 10) |
	     ((p[22] == '#') << 9) |
	     ((p[23] == '#') << 8) |
	     ((p[25] == '#') << 7) |
	     ((p[26] == '#') << 6) |
	     ((p[27] == '#') << 5) |
	     ((p[28] == '#') << 4) |
	     ((p[30] == '#') << 3) |
	     ((p[31] == '#') << 2) |
	     ((p[32] == '#') << 1) |
	     ((p[33] == '#') << 0));
    // For 3x3, need rotations + 1 flip
    for (i = 0; i < 4; i++) {
      p_in = ((((p_in & (1 << 8)) != 0) << 6) |
	      (((p_in & (1 << 7)) != 0) << 3) |
	      (((p_in & (1 << 6)) != 0) << 0) |
	      (((p_in & (1 << 5)) != 0) << 7) |
	      (((p_in & (1 << 4)) != 0) << 4) |
	      (((p_in & (1 << 3)) != 0) << 1) |
	      (((p_in & (1 << 2)) != 0) << 8) |
	      (((p_in & (1 << 1)) != 0) << 5) |
	      (((p_in & (1 << 0)) != 0) << 2));
      if (in3[p_in] >= 0)
	assert (in3[p_in] == p_out);
      in3[p_in] = p_out;
    }
    p_in = (((p_in & (7 << 6)) >> 6) |
	    ((p_in & (7 << 3)) >> 0) |
	    ((p_in & (7 << 0)) << 6));
    for (i = 0; i < 4; i++) {
      p_in = ((((p_in & (1 << 8)) != 0) << 6) |
	      (((p_in & (1 << 7)) != 0) << 3) |
	      (((p_in & (1 << 6)) != 0) << 0) |
	      (((p_in & (1 << 5)) != 0) << 7) |
	      (((p_in & (1 << 4)) != 0) << 4) |
	      (((p_in & (1 << 3)) != 0) << 1) |
	      (((p_in & (1 << 2)) != 0) << 8) |
	      (((p_in & (1 << 1)) != 0) << 5) |
	      (((p_in & (1 << 0)) != 0) << 2));
      if (in3[p_in] >= 0)
	assert (in3[p_in] == p_out);
      in3[p_in] = p_out;
    }
    p += 35;
  }
  // perform the expansions
  for (i = 0; i < iter; i++) {
    if (size & 1) { // expand 3x3 into 4x4
      for (y = size / 3; y--; )
	for (x = size / 3; x--; ) {
	  p_in = ((grid[y * 3 + 0][x * 3 + 0] << 8) |
		  (grid[y * 3 + 0][x * 3 + 1] << 7) |
		  (grid[y * 3 + 0][x * 3 + 2] << 6) |
		  (grid[y * 3 + 1][x * 3 + 0] << 5) |
		  (grid[y * 3 + 1][x * 3 + 1] << 4) |
		  (grid[y * 3 + 1][x * 3 + 2] << 3) |
		  (grid[y * 3 + 2][x * 3 + 0] << 2) |
		  (grid[y * 3 + 2][x * 3 + 1] << 1) |
		  (grid[y * 3 + 2][x * 3 + 2]));
	  if (in3[p_in] < 0) {
	    printf ("3x3 mapping for %x not found!\n", p_in);
	    return 1;
	  }
	  p_out = in3[p_in];
	  grid[y * 4 + 0][x * 4 + 0] = p_out & (1 << 15);
	  grid[y * 4 + 0][x * 4 + 1] = p_out & (1 << 14);
	  grid[y * 4 + 0][x * 4 + 2] = p_out & (1 << 13);
	  grid[y * 4 + 0][x * 4 + 3] = p_out & (1 << 12);
	  grid[y * 4 + 1][x * 4 + 0] = p_out & (1 << 11);
	  grid[y * 4 + 1][x * 4 + 1] = p_out & (1 << 10);
	  grid[y * 4 + 1][x * 4 + 2] = p_out & (1 << 9);
	  grid[y * 4 + 1][x * 4 + 3] = p_out & (1 << 8);
	  grid[y * 4 + 2][x * 4 + 0] = p_out & (1 << 7);
	  grid[y * 4 + 2][x * 4 + 1] = p_out & (1 << 6);
	  grid[y * 4 + 2][x * 4 + 2] = p_out & (1 << 5);
	  grid[y * 4 + 2][x * 4 + 3] = p_out & (1 << 4);
	  grid[y * 4 + 3][x * 4 + 0] = p_out & (1 << 3);
	  grid[y * 4 + 3][x * 4 + 1] = p_out & (1 << 2);
	  grid[y * 4 + 3][x * 4 + 2] = p_out & (1 << 1);
	  grid[y * 4 + 3][x * 4 + 3] = p_out & (1 << 0);
	}
      size = size / 3 * 4;
    } else { // expand 2x2 into 3x3
      for (y = size / 2; y--; )
	for (x = size / 2; x--; ) {
	  p_in = ((grid[y * 2 + 0][x * 2 + 0] << 3) |
		  (grid[y * 2 + 0][x * 2 + 1] << 2) |
		  (grid[y * 2 + 1][x * 2 + 0] << 1) |
		  (grid[y * 2 + 1][x * 2 + 1]));
	  if (in2[p_in] > 0x1ff) {
	    printf ("2x2 mapping for %x not found!\n", p_in);
	    return 1;
	  }
	  p_out = in2[p_in];
	  grid[y * 3 + 0][x * 3 + 0] = p_out & (1 << 8);
	  grid[y * 3 + 0][x * 3 + 1] = p_out & (1 << 7);
	  grid[y * 3 + 0][x * 3 + 2] = p_out & (1 << 6);
	  grid[y * 3 + 1][x * 3 + 0] = p_out & (1 << 5);
	  grid[y * 3 + 1][x * 3 + 1] = p_out & (1 << 4);
	  grid[y * 3 + 1][x * 3 + 2] = p_out & (1 << 3);
	  grid[y * 3 + 2][x * 3 + 0] = p_out & (1 << 2);
	  grid[y * 3 + 2][x * 3 + 1] = p_out & (1 << 1);
	  grid[y * 3 + 2][x * 3 + 2] = p_out & (1 << 0);
	}
      size = size / 2 * 3;
    }
  }
  // describe the result
  int on = 0;
  for (y = 0; y < size; y++)
    for (x = 0; x < size; x++)
      if (grid[y][x])
	on++;
  printf ("after %d cycles, image size %d, %d pixels are on\n", iter, size, on);
  return 0;
}
