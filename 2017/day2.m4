divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day2.input] day2.m4

include(`common.m4')ifelse(common(2), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`input', translit(include(defn(`file')), nl`	', `; '))
define(`part1', 0)define(`part2', 0)
define(`store', `define(`a'n, $1)define(`n', incr(n))')
define(`find', `define(`part2', eval(part2 + forloop_arg(0, decr($1), `_$0')))')
define(`_find', `forloop(incr($1), decr(n), `pair($1,', `)')')
define(`pair', `ifelse(eval(a$1%a$2), 0, `a$1/a$2', eval(a$2%a$1), 0,
  `a$2/a$1')')
define(`line', `define(`n', 0)_$0(, translit(`$1', ` ', `,'))find(decr(n))')
define(`_line', `ifelse($1, `', `store($2)$0($2$@)', $3, `', `define(`part1',
  eval(part1 + $2 - $1))', `store($3)$0(ifelse(eval($3 < $1), 1, `$3, $2',
  eval($3 > $2), 1, `$1, $3', `$1, $2'), shift(shift(shift($@))))')')
ifdef(`__gnu__', `
  patsubst(defn(`input'), `\([^;]*\);', `line(`\1')')
',`
  define(`chew', `line(substr(`$1', 0, index(`$1', `;')))define(`tail',
    substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(
    `tail'), `;'), -1, `', `$0(defn(`tail'))')')
  define(`split', `ifelse(eval($1 < 250), 1, `chew(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  split(len(defn(`input')), defn(`input'))
')

divert`'part1
part2
