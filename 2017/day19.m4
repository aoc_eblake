divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dhashsize=H] [-Dfile=day19.input] day19.m4

include(`common.m4')ifelse(common(19, 65537), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`s', defn(`define'))define(`f', defn(`ifelse'))
define(`i', defn(`incr'))define(`c', defn(`decr'))
define(`input', translit(include(defn(`file')), ` 'nl, `.;'))
define(`x', 1)define(`y', 1)
define(`v', `f(`$1',;,`s(`x',1)s(`y',i(`$3'))',`f(`$1',.,,`ifdef(`b',,
  `s(`b',`$2,$3')')s(`g$2_$3',`$1')')s(`x',i(`$2'))')')
ifdef(`__gnu__', `
  patsubst(defn(`input'), `.', `v(`\&',x,y)')
',`
  define(`split', `ifelse($1, 1, `v(`$2',x,y)', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)), `$3')$0(eval($1 - $1/2), substr(`$2',
    eval($1/2)), `$3')')')
  split(len(defn(`input')), defn(`input'))
')
define(`let', `define(`part1', defn(`part1')`$0')-')
forloop(0, 25, `define(substr(ALPHA, ', `, 1), defn(`let'))')
define(`part1')define(`part2', 0)
define(`do', `ifdef(`g$1_$2',`_$0(`$1',`$2',`$3',g$1_$2,i(`$4'))',`$4')')
define(`_do', `do(f(`$4',+,`t',`n')$3(`$1',`$2',`$3'),`$5')')
define(`td', `ifdef(`g'i(`$1')`_$2',`i(`$1'),`$2',`r'',`c(`$1'),`$2',`l'')')
define(`tl', `ifdef(`g$1_'i(`$2'),``$1',i(`$2'),`d'',``$1',c(`$2'),`u'')')
define(`tu', defn(`td'))
define(`tr', defn(`tl'))
define(`nd', ``$1',i(`$2'),`d'')
define(`nl', `c(`$1'),`$2',`l'')
define(`nu', ``$1',c(`$2'),`u'')
define(`nr', `i(`$1'),`$2',`r'')
define(`part2', do(b, `d', 0))

divert`'part1
part2
