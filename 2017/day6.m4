divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dhashsize=H] [-Dfile=day6.input] day6.m4

include(`common.m4')ifelse(common(6, 65537), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`cmp', `ifelse(eval(`$1>'defn(`m')), 1,
  `define(`m', `$1')define(`i', $2)')')
define(`max', `define(`m',`($1>>24&31)')define(`i',0)'dnl
`cmp(`($1>>16&31)',1)cmp(`($1>>8&31)',2)cmp(`($1&31)',3)'dnl
`cmp(`($2>>24&31)',4)cmp(`($2>>16&31)',5)cmp(`($2>>8&31)',6)'dnl
`cmp(`($2&31)',7)cmp(`($3>>24&31)',8)cmp(`($3>>16&31)',9)'dnl
`cmp(`($3>>8&31)',10)cmp(`($3&31)',11)cmp(`($4>>24&31)',12)'dnl
`cmp(`($4>>16&31)',13)cmp(`($4>>8&31)',14)cmp(`($4&31)',15)'dnl
`define(`m',eval(m))')
define(`save', `_$0(`s$2_$3_$4_$5', $1)trace(
  eval($2,16,8) eval($3,16,8) eval($4,16,8) eval($5,16,8) $1)')
define(`trace')
define(`_save', `ifdef(`$1', `define(`part2', eval($2 - $1))-',
  `define(`$1', $2)')')

define(`bump0', `ifelse($1, 0, ``$2,$3,$4,$5'',
  `bump1(decr($1), eval(`$2+($6<<24)'), $3, $4, $5, 1)')')
define(`bump1', `ifelse($1, 0, ``$2,$3,$4,$5'',
  `bump2(decr($1), eval(`$2+($6<<16)'), $3, $4, $5, 1)')')
define(`bump2', `ifelse($1, 0, ``$2,$3,$4,$5'',
  `bump3(decr($1), eval(`$2+($6<<8)'), $3, $4, $5,1)')')
define(`bump3', `ifelse($1, 0, ``$2,$3,$4,$5'',
  `bump4(decr($1), eval(`$2+$6'), $3, $4, $5, 1)')')
define(`bump4', `ifelse($1, 0, ``$2,$3,$4,$5'',
  `bump5(decr($1), $2, eval(`$3+($6<<24)'), $4, $5, 1)')')
define(`bump5', `ifelse($1, 0, ``$2,$3,$4,$5'',
  `bump6(decr($1), $2, eval(`$3+($6<<16)'), $4, $5, 1)')')
define(`bump6', `ifelse($1, 0, ``$2,$3,$4,$5'',
  `bump7(decr($1), $2, eval(`$3+($6<<8)'), $4, $5, 1)')')
define(`bump7', `ifelse($1, 0, ``$2,$3,$4,$5'',
  `bump8(decr($1), $2, eval(`$3+$6'), $4, $5, 1)')')
define(`bump8', `ifelse($1, 0, ``$2,$3,$4,$5'',
  `bump9(decr($1), $2, $3, eval(`$4+($6<<24)'), $5, 1)')')
define(`bump9', `ifelse($1, 0, ``$2,$3,$4,$5'',
  `bump10(decr($1), $2, $3, eval(`$4+($6<<16)'), $5, 1)')')
define(`bump10', `ifelse($1, 0, ``$2,$3,$4,$5'',
  `bump11(decr($1), $2, $3, eval(`$4+($6<<8)'), $5, 1)')')
define(`bump11', `ifelse($1, 0, ``$2,$3,$4,$5'',
  `bump12(decr($1), $2, $3, eval(`$4+$6'), $5, 1)')')
define(`bump12', `ifelse($1, 0, ``$2,$3,$4,$5'',
  `bump13(decr($1), $2, $3, $4, eval(`$5+($6<<24)'), 1)')')
define(`bump13', `ifelse($1, 0, ``$2,$3,$4,$5'',
  `bump14(decr($1), $2, $3, $4, eval(`$5+($6<<16)'), 1)')')
define(`bump14', `ifelse($1, 0, ``$2,$3,$4,$5'',
  `bump15(decr($1), $2, $3, $4, eval(`$5+($6<<8)'), 1)')')
define(`bump15', `ifelse($1, 0, ``$2,$3,$4,$5'',
  `bump0(decr($1), $2, $3, $4, eval(`$5+$6'), 1)')')
define(`setup', `define(`p$1_$2', bump$2(incr($1), 0,0,0,0, eval(`-$1&31')))')
define(`spread', `ifdef(`p$1_$2', `', `setup($1, $2)')_$0($3, $4, $5, $6,
  p$1_$2)')
define(`_spread', `eval(`($1+$5)&0x1f1f1f1f'),eval(`($2+$6)&0x1f1f1f1f'),'dnl
`eval(`($3+$7)&0x1f1f1f1f'),eval(`($4+$8)&0x1f1f1f1f')')

define(`iter', `max(`$2', `$3', `$4', `$5')_$0(`$1', spread(m, i, `$2', `$3',
  `$4', `$5'))')
define(`_iter', `ifelse(save($@), -, `define(`part1', $1)',
  `iter(incr($1),`$2',`$3',`$4',`$5')')')
define(`prep', `ifelse(`$#', `16', `iter(1_$0($@))', `errprintn(
  `unexpected input')m4exit(1)')')
define(`_prep', `ifelse(`$1', `', `', `prep4($1, $2, $3,
  $4)$0(shift(shift(shift(shift($@)))))')')
define(`prep4', `,eval(($1<<24)|($2<<16)|($3<<8)|$4)')
prep(translit(include(defn(`file')), `	'nl, `,'))

divert`'part1
part2
