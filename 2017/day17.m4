divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dstep=N] [-Dhashsize=H] [-Dfile=day17.input] day17.m4

include(`common.m4')ifelse(common(17, 65537), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

ifdef(`step', `', `define(`step', translit(include(defn(`file')), nl))')
define(`set', `ifelse($1, 2018, `', `define(`p$1', eval(($2+step)%$1+1))$0(
  incr($1), p$1)')')
define(`p0', 0)
set(1, 0)
define(`find', `ifelse(p$1, $2, `$1', `$0(decr($1), ifelse(eval(p$1 < $2), 1,
  `decr($2)', $2))')')
define(`part1', find(2016, p2017))
define(`do', `ifelse(eval(`$1 < 50000000'), 1, `ifelse($2, 1, `define(`part2',
  $1)')_$0($1, $2, $3, eval(`($1 - $2)/$3'))')')
define(`_do', `do(eval(`$1+$4+1'), eval(`($2+($4+1)*($3+1)-1)%($1+$4+1)+1'),
  $3)')
do(2017, p2017, step)

divert`'part1
part2
