divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dmax=N] [-Dseeda=X -Dseedb=Y] [-Dfile=day15.input] day15.m4
# Optionally use -Donly=[12] to skip a part
# Optionally use -Dverbose=1 to see some progress

include(`common.m4')ifelse(common(15), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

ifdef(`max', `', `define(`max', 40000000)')
define(`Generator', `(')define(`with', `)')
define(`parse', `define(`seeda', $2)define(`seedb', $4)')
ifdef(`seeda', `', `parse(translit(include(defn(`file')), ` 'nl, `,,'))')

# Montgomery multiplies: R=0x80000000, N=0x7fffffff, N' = 1,
# R^2 mod N = 1
# aR modN * bR modN = redc(a * b)
# aR modN = redc(a * R^2 modN) = redc(a * 1)
# redc(T) = (T + (T modR * N' * N))/R - (0 or 1 * N)
# if m = T&((1<<31)-1)=T%R, redc(T) = (T + (m<<31)-m)/R%N
# = (T/R*R + T%R + T%R*R - T%R)/R%N = (T/R+T%R)%N
# Note that for n < N, redc(n) = n
define(`E', defn(`eval'))
define(`R', `R1(E(`($1>>16)*$2'),E(`($1&0xffff)*$2'))')
define(`R1', `R2($@,E(`(($1&0x7fff)<<16)+($2&0x7fffffff)'))')
define(`R2', `R3(E(`($1>>15)+($2<0)+($3<0)+($3&0x7fffffff)'))')
define(`R3', `E(`($1&0x7fffffff)+($1<0)')')

define(`part1', 0)define(`part2', 0)
define(`rename', `ifdef(`$2', `', `define(`$2', defn(`$1'))popdef(`$1')')')
define(`stat', `output(1, `...$1')rename(`$0', `do'eval(`$1+100000'))_do($@)')
define(`do', `ifdef(`$0$1',`$0$1',`_$0')(`$1',A(`$2'),B(`$3'))')
define(`_do', `ifelse(E(`($2^$3)&0xffff'),0,`C`'')do(incr(`$1'),`$2',`$3')')

ifelse(defn(`only'), 2, `', `
define(`A', `R(`$1',`16807')')
define(`B', `R(`$1',`48271')')
define(`C', `define(`part1', incr(part1))')
define(`do'max, `pushdef(`_do')')
define(`do0', defn(`stat'))
do(0, seeda, seedb)
popdef(`_do')
')

ifelse(defn(`only'), 1, `', `
define(`A', `a(R(`$1',`16807'))')
define(`a', `ifelse(eval(`$1&3'),0,`$1',`A(`$1')')')
define(`B', `b(R(`$1',`48271'))')
define(`b', `ifelse(eval(`$1&7'),0,`$1',`B(`$1')')')
define(`C', `define(`part2', incr(part2))')
popdef(`do'max)
define(`do'eval(max/8), `pushdef(`_do')')
define(`do0', defn(`stat'))
do(0, seeda, seedb)
')

divert`'part1
part2
