divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day7.input] day7.m4

include(`common.m4')ifelse(common(7), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`input', translit((include(defn(`file'))), alpha`'nl`,()', ALPHA`;'))
define(`line', `_$0(translit(`$1', ` ', `,'))')
define(`_line', `pushdef(`prog', `$1')ifelse(`$3', `->', `holds(`$1',
  shift(shift(shift($@))))')define(`w$1', $2)')
define(`holds', `define(`c$1', dquote(shift($@)))_$0($@)')
define(`_holds', `ifelse(`$2', `', `', `define(`p$2', `$1')$0(`$1',
  shift(shift($@)))')')
ifdef(`__gnu__', `
  patsubst(defn(`input'), `\([^;]*\);', `line(`\1')')
',`
  define(`chew', `line(substr(`$1', 0, index(`$1', `;')))define(`tail',
    substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(`tail'), `;'),
    -1, `', `$0(defn(`tail'))')')
  define(`split', `ifelse(eval($1 < 155), 1, `chew(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  split(len(defn(`input')), defn(`input'))
')
define(`lower', `translit(`$1', 'dquote(ALPHA)`, 'dquote(alpha)`)')
define(`fix', `define(`part2', eval(w$1 - $2))define(`s$1', eval(s$1 - $2))')
define(`balance', `ifdef(`s$1', `', `define(`s$1', ifdef(`c$1',
  `eval(w$1 + _$0(c$1))', `w$1'))')s$1')
define(`_balance', `ifelse(balance(`$1'), balance(`$2'), `ifelse(`$3', `',
  `s$1 + s$2', `ifelse(s$1, balance(`$3'), `', `fix(`$3', (s$3 - s$1))')s$1 +
  $0(shift($@))')', `$3', `', `errprintn(`too few children to balance')m4exit(
  1)', `ifelse(s$1, balance(`$3'), `fix(`$2', (s$2 - s$1))', `fix(`$1',
  (s$1 - s$2))')s$1 + $0(shift($@))')')
define(`check', `ifdef(`p$1', `', `define(`part1',
  lower(`$1'))balance(`$1')')')
_stack_foreach(`prog', `check(', `)', `tmp')

divert`'part1
part2
