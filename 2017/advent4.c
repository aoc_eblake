#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int sorter(const void *a, const void *b) {
  return *(unsigned char *)a - *(unsigned char *)b;
}

int main(void) {
  ssize_t nread;
  size_t len = 0;
  char *line = NULL;
  int sum = 0;
  while ((nread = getline(&line, &len, stdin)) >= 0) {
    char *cur = line;
    char *space;
    line[nread - 1] = ' ';
    sum++;
#if 1 // part 2
    while ((space = memchr(cur, ' ', line + nread - cur))) {
      qsort(cur, space - cur, 1, sorter);
      cur = space + 1;
    }
    cur = line;
#endif
    while ((space = strchr(cur, ' '))) {
      if (space - line == nread - 1)
	break;
      char *candidate = space;
      while ((candidate = memmem(candidate + 1, line + nread - candidate - 1,
				 cur, space - cur + 1))) {
	if (candidate[-1] == ' ') {
	  goto skip;
	}
      }
      cur = space + 1;
    }
    continue;
  skip:
    sum--;
  }
  free (line);
  printf("%d\n", sum);
}
