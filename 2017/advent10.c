#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#if 0 // part 1

int main(int argc, char **argv)
{
  if (argc != 3)
    return 1;
  int size = atoi(argv[1]);
  if (size < 2 || size > 256)
    return 2;
  unsigned char *list = malloc (size);
  for (int i = 0; i < size; i++)
    list[i] = i;
  char *p = argv[2];
  char *end;
  int skip = 0;
  int pos = 0;
  int length;
  while ((length = strtol (p, &end, 10)), p != end) {
    printf ("length %d, skip %d, pos %d\n", length, skip, pos % size);
    for (int i = 0; i < length / 2; i++) {
      int tmp = list[(i + pos) % size];
      list[(i + pos) % size] = list[(length - i - 1 + pos) % size];
      list[(length - i - 1 + pos) % size] = tmp;
    }
    if (size == 5)
      printf (" %d %d %d %d %d\n", list[0], list[1], list[2], list[3], list[4]);
    pos += length + skip++;
    p = end + 1;
  }
  printf ("final product %d\n", list[0] * list[1]);
  return 0;
}

#else // part 2

int main(int argc, char **argv)
{
  if (argc != 2)
    return 1;
  const int size = 256;
  unsigned char list[size];
  for (int i = 0; i < size; i++)
    list[i] = i;
  int skip = 0;
  int pos = 0;
  const char suffix[6] = { 17, 31, 73, 47, 23 };
  char *str = malloc(strlen(argv[1]) + 5 + 1);
  strcpy(stpcpy(str, argv[1]), suffix);
  for (int j = 0; j < 64; j++) {
    printf ("round %d, skip %d, pos %d\n", j, skip, pos % size);
    for (char *p = str; *p; p++) {
      int length = *p;
      for (int i = 0; i < length / 2; i++) {
	int tmp = list[(i + pos) % size];
	list[(i + pos) % size] = list[(length - i - 1 + pos) % size];
	list[(length - i - 1 + pos) % size] = tmp;
      }
      pos += length + skip++;
    }
  }
  unsigned char chunk = 0;
  for (int i = 0; i < size; i++) {
    chunk ^= list[i];
    if (i % 16 == 15) {
      printf ("%02x", chunk);
      chunk = 0;
    }
  }
  puts ("");
  return 0;
}

#endif
