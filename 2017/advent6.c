#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

static void
dump(const char *prefix, int banks, int *state)
{
  puts(prefix);
  for (int i = 0; i < banks; i++)
    printf(" %d", state[i]);
  puts("");
}

typedef struct entry entry;
struct entry {
  int *state;
  entry *next;
} head;

static entry *
cycle(int banks, entry *in)
{
  int i;
  int max = 0;
  for (i = 0; i < banks; i++)
    if (in->state[i] > in->state[max])
      max = i;
  int remaining = in->state[max];
  entry *out = calloc(1, sizeof *out);
  out->state = malloc(banks * sizeof *out->state);
  memcpy(out->state, in->state, banks * sizeof *out->state);
  in->next = out;
  out->state[max] = 0;
  while (remaining--)
    out->state[++max % banks]++;
  return out;
}

int
main(int argc, char **argv)
{
  if (argc < 3)
    return 1;
  int banks = atoi(argv[1]);
  if (banks != argc - 2)
    return 2;
  head.state = malloc(banks * sizeof *head.state);
  int i;
  for (i = 0; i < banks; i++)
    head.state[i] = atoi(argv[i + 2]);
  dump("starting with", banks, head.state);
  entry *e = &head;
  while (1) {
    entry *next = cycle(banks, e);
    //    dump("next", banks, next->state);
    e = &head;
    while (memcmp(e->state, next->state, banks * sizeof *e->state))
      e = e->next;
    if (e->next)
      break;
  }
  dump("repeat state", banks, e->state);
  const char *msg;
#if 0 // part 1
  e = &head;
  msg = "loop at cycle";
#else // part 2
  msg = "loop length";
#endif
  for (i = 0; e->next; i++, e = e->next);
  printf("%s %d\n", msg, i);
  return 0;
}
