#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <ctype.h>

#define MAX 202 // cheat by pre-inspecting input with wc
char grid[MAX][MAX + 1];

enum dir {
  U,
  D,
  L,
  R,
};

int
main (int argc, char **argv)
{
  int rows = 0;
  char out[11]; // cheat by pre-inspecting 'tr -d -c A-Z < day19.input'
  char *p = out;
  while (fgets (grid[rows], MAX + 1, stdin) && grid[rows][0])
    rows++;
  int width = strchr (grid[0], '\n') - grid[0];
  printf ("read %d rows, width %d\n", rows, width);
  int r = 0, c;
  enum dir d = D;
  c = strchr (grid[0], '|') - grid[0];
  bool done = false;
  int steps = 0;
  while (!done) {
    if (getenv ("DEBUG"))
      printf ("at grid[%d][%d]='%c', direction %d\n", r, c, grid[r][c], d);
    steps++;
    if (isalpha (grid[r][c]))
      *p++ = grid[r][c];
    switch (d) {
    case U:
      if (r && grid[r - 1][c] != ' ')
	r--;
      else if (c < width - 1 && grid[r][c + 1] != ' ') {
	d = R;
	c++;
      } else if (c && grid[r][c - 1] != ' ') {
	d = L;
	c--;
      } else
	done = true;
      break;
    case D:
      if (r < rows - 1 && grid[r + 1][c] != ' ')
	r++;
      else if (c < width - 1 && grid[r][c + 1] != ' ') {
	d = R;
	c++;
      } else if (c && grid[r][c - 1] != ' ') {
	d = L;
	c--;
      } else
	done = true;
      break;
    case L:
      if (c && grid[r][c - 1] != ' ')
	c--;
      else if (r < rows - 1 && grid[r + 1][c] != ' ') {
	d = D;
	r++;
      } else if (r && grid[r - 1][c] != ' ') {
	d = U;
	r--;
      } else
	done = true;
      break;
    case R:
      if (c < width - 1 && grid[r][c + 1] != ' ')
	c++;
      else if (r < rows - 1 && grid[r + 1][c] != ' ') {
	d = D;
	r++;
      } else if (r && grid[r - 1][c] != ' ') {
	d = U;
	r--;
      } else
	done = true;
      break;
    }
  }
  printf ("encountered %s after %d steps\n", out, steps);
  return 0;
}
