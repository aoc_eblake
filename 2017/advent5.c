#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

int main(void) {
  /* slurp in all of stdin. Cheat - we know our input file size, so
     pick a buffer larger than that */
  static char buf[5000];
  int len = 0;
  while (len < sizeof buf) {
    int ret = read(STDIN_FILENO, buf + len, sizeof buf - len);
    if (ret < 0)
      return 1;
    if (!ret)
      break;
    len += ret;
  }
  if (len == sizeof buf)
    return 2;
  /* count lines */
  int count = 0;
  char *p = buf;
  while ((p = memchr (p, '\n', buf + len - p)) && p++)
    count++;
  printf("found %d lines\n", count);
  /* populate array */
  int *array = calloc(sizeof(int), count);
  p = buf;
  char *q;
  int i = 0;
  while ((q = memchr (p, '\n', buf + len - p))) {
    *q = '\0';
    array[i++] = atoi(p);
    p = q + 1;
  }
  if (getenv("DEBUG")) {
    for (i = 0; i < count; i++)
      printf("%d ", array[i]);
    puts("\n");
  }
  /* run through maze */
  int pc = 0;
  int steps = 0;
  while (pc >= 0 && pc < count) {
    steps++;
#if 0 // part 1
    pc += array[pc]++;
#else // part 2
    int jump = array[pc];
    if (jump >= 3)
      array[pc]--;
    else
      array[pc]++;
    pc += jump;
#endif
    if (getenv("DEBUG"))
      printf("moving to pc %d\n", pc);
  }
  printf("exited in %d steps\n", steps);
  return 0;
}
