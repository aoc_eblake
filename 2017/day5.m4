divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dhashsize=H] [-Dfile=day5.input] day5.m4
# Optionally use -Donly=[12] to skip a part
# Optionally use -Dverbose=1 to see some progress

include(`common.m4')ifelse(common(5, 65537), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`input', translit(include(defn(`file')), nl, `;'))
ifdef(`__gnu__', `
  patsubst(defn(`input'), `\([^;]*\);', `pushdef(`list', `\1')')
',`
  define(`chew', `pushdef(`list', substr(`$1', 0, index(`$1', `;')))define(
    `tail', substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(
    `tail'), `;'), -1, `', `$0(defn(`tail'))')')
  define(`split', `ifelse(eval($1 < 12), 1, `chew(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  split(len(defn(`input')), defn(`input'))
')
define(`D', defn(`define'))define(`I', defn(`incr'))define(`C', defn(`decr'))
define(`N', defn(`defn'))define(`F', defn(`ifdef'))define(`E', defn(`ifelse'))
define(`rename', `define(`$2', defn(`$1'))popdef(`$1')')
define(`stat', `output(1, `...'decr($2))rename(`$0',
  `do'eval(`$2+99999'))do($@)')
define(`prep', `define(`m$1_', eval($1+$2))define(`m$1', defn(ifdef(`m_$2',
  ``m_$2'', ``m'')))')
define(`run', `define(`cnt', 0)define(`do0', defn(`stat'))_stack_foreach(
  `list', `prep(cnt, ', `)define(`cnt', incr(cnt))', `t')do(0, 0)')
define(`do', `F(`m$1',`F(`$0$2',`$0$2',`$0')(m$1(`$1',m$1_),I(`$2'))',`$2')')

ifelse(defn(`only'), 2, `', `
define(`m', `D(`$0_',I(`$2'))`$2'')
define(`part1', run())
')
ifelse(defn(`only'), 1, `', `
define(`m', `E(`$1',`$2',`D(`$0',N(`m_1'))')D(`$0_',I(`$2'))`$2'')
define(`m_1', `D(`$0',N(`m_2'))D(`$0_',I(`$2'))`$2'')
define(`m_2', `D(`$0',N(`m_3'))D(`$0_',I(`$2'))`$2'')
define(`m_3', `D(`$0',N(`m_2'))D(`$0_',C(`$2'))`$2'')
define(`part2', run())
')

divert`'part1
part2
