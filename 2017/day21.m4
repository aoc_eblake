divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day21.input] day21.m4

include(`common.m4')ifelse(common(21), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`input', translit(include(defn(`file')), nl`.#/ =>', `;01'))
define(`c2', 0)define(`c3', 0)
define(`line', `ifelse(len(`$1'), 13, `line2(substr(`$1', 0, 4), substr(`$1',
  4))', `line3(substr(`$1', 0, 9), substr(`$1', 9))')')
define(`line2', `define(`r2'c2, `$2')_$0(3, `$1', c2)define(`c2', incr(c2))')
define(`_line2', `define(`p2$2', `$3')ifelse(`$1', 0, `', `$0(decr($1),
  rot(`$2'), `$3')')')
define(`rot', `translit(`3142', `1234', `$1')')
define(`line3', `define(`r3'c3, `$2')define(`l'c3, len(translit(`$1',
  0)))_$0t(3, `$1', c3)define(`c3', incr(c3))')
define(`_line3t', `define(`p3$2', `$3')_line3s($1, tran(`$2'), `$3')')
define(`_line3s', `define(`p3$2', `$3')ifelse(`$1', 0, `', `_line3t(decr(`$1'),
  swap(`$2'), `$3')')')
define(`tran', `translit(`147258369', `123456789', `$1')')
define(`swap', `translit(`789456123', `123456789', `$1')')
ifdef(`__gnu__', `
  patsubst(defn(`input'), `\([^;]*\);', `line(`\1')')
',`
  define(`chew', `line(substr(`$1', 0, index(`$1', `;')))define(`tail',
    substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(`tail'), `;'),
    -1, `', `$0(defn(`tail'))')')
  define(`split', `ifelse(eval($1 < 70), 1, `chew(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  split(len(defn(`input')), defn(`input'))
')

define(`init', p3010001111)
pushdef(`q0', init)define(`c0_'init, 1)
define(`iter', `_$0(decr($1), $1)')
define(`_iter', `ifdef(`q$1', `bump(q$1, $1, $2)popdef(`q$1')$0($@)')')
define(`expand2', `first(translit(
  ``r2'p2ABEF`()r2'p2CDGH`()r2'p2IJMN`()r2'p2KLOP', `ABCDEFGHIJKLMNOP',
  r3$1))')
define(`do', ``,'defn(`p3'r2$1)')
define(`expand', `define(`L$1', translit(`do(p2ABDE)do(p2CJFM)do(p2KLNO)''dnl
``do(p2GHST)do(p2IPU1)do(p2QR_3)do(p2VWYZ)do(p2X407)do(p25689)',
  `ABCDEFGHIJKLMNOPQRSTUVWXYZ01_3456789', expand2(`$1')))')
define(`bump', `ifdef(`L$1', `', `expand(`$1')')_foreach(`_$0(c$2_$1, $3, ',
  `)', L$1)popdef(`c$2_$1')')
define(`_bump', `ifdef(`c$2_$3', `define(`c$2_$3', eval(c$2_$3 + $1))',
  `pushdef(`q$2', $3)define(`c$2_$3', $1)')')
define(`lit2', `len(translit(expand2(`$1'), 0))')
define(`lit0', `l$1')
define(`_tally', `+c$1_$2*lit$3(`$2')')
define(`tally', `eval(0_stack_foreach(`q$1', `_$0($1, ', `, `$2')', `t'))')
iter(1)
define(`part1', tally(1, 2))
forloop_arg(2, 6, `iter')
define(`part2', tally(6, 0))

divert`'part1
part2
