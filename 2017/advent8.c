#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

// cheat - pre-examined input to learn max name length (3+NUL)
#define MAXNAME 4

typedef struct reg reg;
struct reg {
  char name[MAXNAME];
  int value;
  reg *next;
};
reg *list;

static int
get(const char *name)
{
  for (reg *iter = list; iter; iter = iter->next)
    if (!strcmp (iter->name, name))
      return iter->value;
  reg *r = calloc (1, sizeof *r);
  strcpy (r->name, name); // safe because of max name
  r->next = list;
  list = r;
  return 0;
}

static int
set(const char *name, int adjust)
{
  if (getenv ("DEBUG"))
    printf(" adjusting %s by %d\n", name, adjust);
  for (reg *iter = list; iter; iter = iter->next)
    if (!strcmp (iter->name, name)) {
      iter->value += adjust;
      return iter->value;
    }
  reg *r = calloc (1, sizeof *r);
  strcpy (r->name, name); // safe because of max name
  r->value = adjust;
  r->next = list;
  list = r;
  return adjust;
}

int main(void)
{
  char d[MAXNAME], s[MAXNAME], dir, op[3];
  int adjust, v;
  int count = 0;
  int high = 0;
  // ugly, but works because our data is sane
  while (scanf("%3[a-z] %c%*[ne]c %d if %3[a-z] %2[><!=] %d\n",
	       d, &dir, &adjust, s, op, &v) == 6) {
    switch (dir) {
    case 'i':
      break;
    case 'd':
      adjust = -adjust;
      break;
    default:
      return 2;
    }
    int r = get (s);
    if (getenv("DEBUG"))
      printf("%s %d if %s (%d) %s %d\n", d, adjust, s, r, op, v);
    count++;
    int doit = 0;
    switch (op[0] + op[1]) {
    case '>':
      if (r > v)
	doit = 1;
      break;
    case '<':
      if (r < v)
	doit = 1;
      break;
    case '>' + '=':
      if (r >= v)
	doit = 1;
      break;
    case '<' + '=':
      if (r <= v)
	doit = 1;
      break;
    case '=' + '=':
      if (r == v)
	doit = 1;
      break;
    case '!' + '=':
      if (r != v)
	doit = 1;
      break;
    default:
      return 3;
    }
    if (doit) {
      v = set(d, adjust);
      if (v > high)
	high = v;
    }
  }
  printf("parsed %d lines\n", count);
  int max = 0;
  for (reg *iter = list; iter; iter = iter->next)
    if (iter->value > max)
      max = iter->value;
  printf("largest register contains %d\n", max);
  printf("largest value seen %d\n", high);
  return 0;
}
