divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dhashsize=H] [-Dfile=day25.input] day25.m4

include(`common.m4')ifelse(common(25, 65537), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`begin_', `define(`init', `$4')')
define(`perform_', `define(`do$6', `$'2)')
define(`in_', `define(`st', `$3')')
define(`if_', `define(`rd', `$6')')
define(`write_', `define(st`'rd, defn(`rd')`,`$4'')')
define(`move_', `define(st`'rd, defn(st`'rd)`,'dquote(substr(`$6', 0, 1)))')
define(`cont_', `define(st`'rd, defn(st`'rd)`,`$4'')')
translit(include(defn(`file')), `.: ', define(`Begin', `begin_(')define(
  `Perform', `perform_(')define(`In', `in_(')define(`If', `if_(')define(
  `Write', `write_(')define(`Move', `move_(')define(`Continue', `cont_(')`)),')

define(`rename', `ifdef(`$2', `', `define(`$2', defn(`$1'))popdef(`$1')')')
define(`do'100000, `output(1, `...'decr(`$1'))rename(`$0', `do'eval(
  `$1+99999'))_do($@)')
define(`do', `ifdef(`$0$1',`$0$1',`_do')(incr(`$1'),`$2',`$3',ifdef(`t$3',
  `$4'1, `$4'0))')
define(`_do', `do(`$1',w$4$5(`$2',`$3'),m$6(`$3'),`$7')')
define(`w00', `$1')
define(`w01', `define(`t$2')incr(`$1')')
define(`w10', `popdef(`t$2')decr(`$1')')
define(`w11', `$1')
define(`ml', defn(`decr'))define(`mr', defn(`incr'))
define(`part1', do(0, 0, 12000, init))

divert`'part1
no part2
