#include <stdio.h>
#include <stdlib.h>

#ifdef TRY1
static int ringmax (int ring) {
  return (2 * ring - 1) * (2 * ring - 1);
}

static int corner (int ring, int c) {
  return ringmax (ring) - ((ring - 1) * 2 * (4 - c));
}

#if 0 // part 1

int main (int argc, char **argv) {
  if (argc != 2)
    return 1;
  int goal = atol(argv[1]);
  long ring = 1;
  while (ringmax (ring) < goal)
    ring++;
  printf ("ring = %ld\n", ring);
  int c = 0;
  while (corner (ring, ++c) < goal);
  printf ("between corners %d, %d\n", corner(ring, c - 1), corner(ring, c));
  int mid = (corner (ring, c - 1) + corner (ring, c)) / 2;
  printf ("distance = %ld\n", ring - 1 + abs (mid - goal));
  return 0;
}

#else // part 2

static int ring_of (int cell) {
  int ring = 1;
  while (ringmax (ring) < cell)
    ring++;
  return ring;
}

static int corner_of (int cell, int ring) {
  int c = 0;
  while (corner (ring, ++c) < cell);
  return c;
}

static int e_of (int cell);
static int n_of (int cell);
static int w_of (int cell);
static int s_of (int cell);

int e_of (int cell) {
  if (cell == 1)
    return 2;
  int ring = ring_of (cell);
  int c = corner_of (cell, ring);
  switch (c) {
  case 1:
    return cell + 8 * (ring - 1) + 1;
    break;
  case 2:
    return cell - 1;
  case 3:
    return cell == corner (ring, c) ? cell + 1 : cell - 8 * (ring - 1) + 3;
  case 4:
    return cell + 1;
  }
  abort ();
}

int n_of (int cell) {
  if (cell == 1)
    return 4;
  int ring = ring_of (cell);
  int c = corner_of (cell, ring);
  switch (c) {
  case 1:
    return cell == corner (ring, c) ? e_of (cell) + 2 : cell + 1;
  case 2:
    return cell + 8 * (ring - 1) + 3;
  case 3:
    return cell - 1;
  case 4:
    return cell - 8 * (ring - 1) + 1;
  }
  abort ();
}

int w_of (int cell) {
  if (cell == 1)
    return 6;
  int ring = ring_of (cell);
  int c = corner_of (cell, ring);
  switch (c) {
  case 1:
    return (cell == corner (ring, c) ? cell + 1 :
	    cell == corner (ring, c - 1) + 1 ? cell - 1 :
	    cell - 8 * (ring - 2) - 1);
  case 2:
    return cell == corner (ring, c) ? n_of (cell) + 2 : cell + 1;
  case 3:
    return cell + 8 * ring - 3;
  case 4:
    return cell - 1;
  }
  abort ();
}

int s_of (int cell) {
  if (cell == 1)
    return 8;
  int ring = ring_of (cell);
  int c = corner_of (cell, ring);
  switch (c) {
  case 1:
    return cell == corner (ring, c - 1) + 1 ? corner (ring, 4) : cell - 1;
  case 2:
    return cell == corner (ring, c) ? cell + 1 : cell - 8 * (ring - 2) - 3;
  case 3:
    return cell == corner (ring, c) ? w_of (cell) + 2 : cell + 1;
  case 4:
    return cell + 8 * ring - 1;
  }
  abort ();
}

static void locate (int goal) {
  printf ("\nprocessing %d\n", goal);
  int ring = ring_of (goal);
  printf ("ring = %d\n", ring);
  int c = corner_of (goal, ring);
  printf ("between corners %d, %d\n", corner(ring, c - 1), corner(ring, c));
  printf ("%4d %4d %4d\n", w_of (n_of(goal)), n_of(goal), e_of (n_of(goal)));
  printf ("%4d %4d %4d\n", w_of (goal), goal, e_of (goal));
  printf ("%4d %4d %4d\n", w_of (s_of(goal)), s_of(goal), e_of (s_of(goal)));
}

static int *array;
static int get(int cell, int neighbor) {
  if (neighbor < 1)
    abort();
  if (neighbor > cell)
    return 0;
  return array[neighbor];
}

#define limit 100
int main (int argc, char **argv) {
  if (argc != 2)
    return 1;
  int goal = atoi(argv[1]);
  int i;
  array = calloc(sizeof(int), limit + 1);
  array[1] = 1;
  for (i = 2; i <= limit; i++) {
    array[i] = get(i, e_of(i)) + get(i, e_of(n_of(i))) + get(i, n_of(i))
      + get(i, w_of(n_of(i))) + get(i, w_of(i)) + get(i, w_of(s_of(i)))
      + get(i, s_of(i)) + get(i, e_of(s_of(i)));
    if (array[i] > goal)
      break;
  }
  if (i > limit) {
    printf("limit is too small, recompile\n");
    return 2;
  }
  locate(i);
  printf("value = %d\n", array[i]);
  return 0;
}

#endif



#else // try 2



typedef struct point {
  int x;
  int y;
  int value;
} point;
int main (int argc, char **argv) {
  if (argc != 2)
    return 1;
  int limit = atoi(argv[1]);
  point *grid = calloc(sizeof *grid, limit);
  if (!grid)
    return 2;
  enum direction { R, U, L, D } dir = R;
  grid[0].value = 1;
  for (int i = 1; i < limit; i++) {
    switch (dir) {
    case R:
      grid[i].x = grid[i - 1].x + 1;
      grid[i].y = grid[i - 1].y;
      if (grid[i].x == -grid[i].y + 1)
	dir = U;
      break;
    case U:
      grid[i].x = grid[i - 1].x;
      grid[i].y = grid[i - 1].y + 1;
      if (grid[i].x == grid[i].y)
	dir = L;
      break;
    case L:
      grid[i].x = grid[i - 1].x - 1;
      grid[i].y = grid[i - 1].y;
      if (grid[i].x == -grid[i].y)
	dir = D;
      break;
    case D:
      grid[i].x = grid[i - 1].x;
      grid[i].y = grid[i - 1].y - 1;
      if (grid[i].x == grid[i].y)
	dir = R;
      break;
    }
    for (int j = 0; j < i; j++)
      if (abs(grid[j].x - grid[i].x) <= 1 &&
	  abs(grid[j].y - grid[i].y) <= 1)
	grid[i].value += grid[j].value;
#if 1 // part 2
    if (grid[i].value > limit) {
      printf("%d = %d\n", i + 1, grid[i].value);
      return 0;
    }
#endif
  }
  printf("%d,%d = %d\n", grid[limit - 1].x, grid[limit - 1].y,
	 abs(grid[limit - 1].x) + abs(grid[limit - 1].y));
  return 0;
}
#endif
