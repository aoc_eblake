#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

int main(void)
{
  int depth = 0;
  int score = 0;
  int garbage = 0;
  int count = 0;
  int c;
  while ((c = getchar ()) >= 0)
    if (garbage) {
      if (c == '!')
	getchar ();
      else if (c == '>')
	garbage = 0;
      else
	count++;
    } else {
      if (c == '{')
	score += ++depth;
      else if (c == '}')
	depth--;
      else if (c == '<')
	garbage = 1;
    }
  if (depth)
    printf("oops\n");
  printf("score=%d garbage=%d\n", score, count);
  return 0;
}
