#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <stdbool.h>

char buf[50000]; // cheat, pre-inspect file

extern uint64_t doit(uint64_t);

const char *
output (uint64_t array)
{
  static char out[17];
  for (int i = 0; i < 16; i++)
    out[i] = ((array & (0xfULL << ((15 - i) * 4))) >> ((15 - i) * 4)) + 'a';
  return out;
}

int main(int argc, char **argv)
{
  int limit = 1000000000;
  if (argc == 2)
    limit = atoi (argv[1]);
  int a, b, c, j, n;
  char x, y;
  char *s;
  if (getenv ("GEN")) {
    int ops = 0;
    fread (buf, 1, sizeof buf, stdin);
    printf ("#include <stdint.h>\n");
    printf ("uint64_t doit(uint64_t n)\n");
    printf ("{\n");
    printf ("  int i;\n");
    printf ("  uint64_t a, b;\n");
    s = buf;
    while (*s) {
      switch (*s++) {
      case 's':
	ops++;
	sscanf(s, "%d%n", &a, &n);
	s += n;
	printf ("  // s%d\n", a);
	printf ("  n = (n >> %d) | (n << %d);\n", a * 4, 64 - a * 4);
	break;
      case 'x':
	ops++;
	sscanf(s, "%d/%d%n", &a, &b, &n);
	s += n;
	printf ("  // x%d/%d\n", a, b);
	if (a > b) {
	  c = a;
	  a = b;
	  b = c;
	}
	printf ("  a = (n & (0xfULL << %d)) >> %d;\n",
		(15 - a) * 4, (b - a) * 4);
	printf ("  b = (n & (0xfULL << %d)) << %d;\n",
		(15 - b) * 4, (b - a) * 4);
	printf ("  n = (n & %#llx) | a | b;\n",
		-1ULL ^ (0xfULL << ((15 - a) * 4)) ^
		(0xfULL << ((15 - b)  * 4)));
	break;
      case 'p':
	ops++;
	sscanf(s, "%c/%c%n", &x, &y, &n);
	s += n;
	printf ("  // p%c/%c\n", x, y);
	x -= 'a';
	y -= 'a';
	printf ("  for (i = 0; i < 64; i += 4) {\n");
	printf ("    if (((n & (0xfULL << i)) >> i) == %d)\n", x);
	printf ("      n = (n & ~(0xfULL << i)) | (%dULL << i);\n", y);
	printf ("    else if (((n & (0xfULL << i)) >> i) == %d)\n", y);
	printf ("      n = (n & ~(0xfULL << i)) | (%dULL << i);\n", x);
	printf ("  }\n");
	break;
      }
    }
    printf ("  // %d ops\n", ops);
    printf ("  return n;\n");
    printf ("}\n");
    return 0;
  }
  uint64_t array1 = 0x0123456789abcdefULL;
  uint64_t array2 = 0x0123456789abcdefULL;
  bool cycle = false;
  for (j = 0; j < limit; j++) {
    if (j < 10)
      printf ("after dance %d: %s\n", (cycle ? j : j * 2), output (array1));
    else if (!(j % 1000))
      printf ("\b\b\b\b\b\b\b\b\b\b\b\b%10d", j), fflush (stdout);
    array1 = doit (array1);
    if (!cycle && j < limit) {
      array1 = doit (array1);
      array2 = doit (array2);
      if (array1 == array2) {
	printf ("cycle detected at %d\n", j);
	j = limit - limit % (j + 1) - 1;
	cycle = true;
      }
    }
  }
  printf ("\nfinal string: %s\n", output (array1));
  return 0;
}
