#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>

int main(void)
{
  int severity = 0;
  int count = 0;
  int layer;
  int depth;
  int list[100] = {0}; // cheat based on pre-inspecting input
  int max = 0;
  while (scanf("%d: %d\n", &layer, &depth) == 2) {
    count++;
    max = layer;
    list[layer] = depth;
  }
  printf ("read %d items, max layer %d\n", count, max);
  int delay = -1;
  while (1) {
    delay++;
    int i;
    for (i = 0; i <= max; i++) {
      if (!list[i])
	continue;
      int cycle = (list[i] - 1) * 2;
      if (! ((i + delay) % cycle)) {
	// printf (" delay %d, i %d, cycle %d\n", delay, i, cycle);
	if (!delay)
	  severity += i * list[i];
	else
	  break;
      }
    }
    if (delay && i > max)
      break;
  }
  printf ("with no delay, severity is %d\n", severity);
  printf ("delay %d to avoid collisions\n", delay);
  return 0;
}
