divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day1.input] day1.m4

include(`common.m4')ifelse(common(1), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`input', translit(include(defn(`file')), nl))
define(`wrap', `substr(`$1', $2)substr(`$1', 0, $2)')
ifdef(`__gnu__', `
  define(`get', `patsubst(`$1', `.', `pushdef(`$2', \&)')')
',`
  define(`split', `ifelse($1, 1, `pushdef(`$3', `$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)), `$3')$0(eval($1 - $1/2), substr(`$2',
    eval($1/2)), `$3')')')
  define(`get', `split(len(`$1'), `$1', `$2')')
')
get(defn(`input'), `a')
get(wrap(defn(`input'), 1), `b')
get(defn(`input'), `c')
get(wrap(defn(`input'), eval(len(defn(`input'))/2)), `d')
define(`compare', `eval(_$0($@))')
define(`_compare', `ifdef(`$1', `ifelse($1, $2, +$1)popdef(`$1')popdef(
  `$2')$0($@)')')
define(`part1', compare(`a', `b'))
define(`part2', compare(`c', `d'))

divert`'part1
part2
