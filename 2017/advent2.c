#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#if 0 // part 1

int main(void) {
  ssize_t nread;
  size_t len = 0;
  char *line = NULL;
  int sum = 0;
  while ((nread = getline(&line, &len, stdin)) >= 0) {
    unsigned int min = -1, max = 0;
    char *token = strtok(line, " \t");
    while (token) {
      int value = atoi(token);
      if (value < min)
	min = value;
      if (value > max)
	max = value;
      token = strtok(NULL, " \t");
    }
    sum += max - min;
  }
  printf("%d\n", sum);
}

#else // part 2

int main(void) {
  ssize_t nread;
  size_t len = 0;
  char *line = NULL;
  int sum = 0;
  while ((nread = getline(&line, &len, stdin)) >= 0) {
    int *array = calloc(sizeof(int), nread);
    char *token = strtok(line, " \t");
    int i = 0, j;
    while (token) {
      array[i++] = atoi(token);
      token = strtok(NULL, " \t");
    }
    for (i = 0; i < nread; i++) {
      if (!array[i])
	continue;
      for (j = 0; j < nread; j++) {
	if (i == j || !array[j])
	  continue;
	if (!(array[i] % array[j])) {
	  sum += array[i] / array[j];
	  printf("found %d/%d=%d\n", array[i], array[j], array[i]/array[j]);
	  goto done;
	}
      }
    }
  done:
    ;
  }
  printf("%d\n", sum);
}

#endif
