divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dhashsize=H] [-Dfile=day23.input] day23.m4

include(`common.m4')ifelse(common(23, 65537), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`input', translit(include(defn(`file')), nl, `;'))
define(`cnt', 0)
define(`line', `_$0(translit(`$1', ` ', `,'))')
define(`_line', `define(`i'cnt, `$1_')define(`a'cnt, ``$2',`$3'')define(`cnt',
  incr(cnt))ifdef(`r$2', `', `ifelse(index(alpha, `$2'), -1, `',
  `pushdef(`regs', `$2')define(`r$2', 0)')')')
ifdef(`__gnu__', `
  patsubst(defn(`input'), `\([^;]*\);', `line(`\1')')
', `
  define(`_chew', `line(substr(`$1', 0, index(`$1', `;')))define(`tail',
    substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(`tail'), `;'), -1,
    `', `$0(defn(`tail'))')')
  define(`chew', `ifelse(eval($1 < 28), 1, `_$0(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  chew(len(defn(`input')), defn(`input'))
')

define(`part1', 0)
define(`r_', `ifdef(`r$1', `r')$1')
define(`set_', `define(`r$1', r_(`$2'))incr($3)')
define(`sub_', `define(`r$1', eval(r$1 - r_(`$2')))incr($3)')
define(`mul_', `define(`r$1', eval(r$1 * r_(`$2')))define(`part1',
  incr(part1))incr($3)')
define(`jnz_', `ifelse(r_(`$1'), 0, `incr(`$3')', `eval($3+r_(`$2'))')')
define(`run', `ifdef(`i$1', `_$0(i$1()(a$1, $1), incr($2))')')
define(`_run', `run($@)')
run(0, 0)

_stack_foreach(`regs', `define(`r'', `, 0)', `t')
define(`ra', 1)
# For each b visited, the puzzle tests each pair 2<=d<=b and 2<=e<=b and sets
# f=0 if if finds any d*e==b. If b is prime, f stays 1. Then h is incremented
# based on f. This is therefore a major peephole optimization to do O(sqrt(N))
# instead of O(N^2) work per value of b.
define(`mul_', `ifelse(`$2', `e', `define(`rf', isprime(rb))eval(`$3+12')',
  `define(`r$1', eval(r$1 * r_(`$2')))incr($3)')')
define(`isprime', `ifelse(eval(`$1&1'), 0, 0, `_$0(`$1', `3')')')
define(`_isprime', `ifelse(eval(`$2*$2>$1'), 1, 1, eval(`$1%$2'), 0, 0,
  `$0(`$1', incr(incr(`$2')))')')
run(0, 0)
define(`part2', rh)

divert`'part1
part2
