#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <ctype.h>

#if 0

typedef struct state state;
struct state {
  long long regs[26];
  unsigned int pc;
  int count;
  int mul;
} states;

char *program[50]; // sized based on pre-inspecting file
int instr;

long long
get (state *s, const char *value)
{
  if (isalpha(*value))
    return s->regs[*value - 'a'];
  return atoi (value);
}

void
set (state *s, const char *reg, long long value)
{
  s->regs[*reg - 'a'] = value;
  if (getenv ("DEBUG"))
    printf (" %s=%lld\n", reg, value);
}

bool
run (state *s, int id)
{
  while (s->pc < instr) {
    char arg1[10], arg2[10];
    if (getenv ("DEBUG"))
      printf ("p%d instr %d at pc %d: %s\n", id, s->count, s->pc,
	      program[s->pc]);
    if (!(s->count % 10000))
      printf ("\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b%10d h=%lld", s->count,
	     s->regs['h' - 'a']);
    if (s->pc == 8) {
      printf ("\nstarting computation at count %d,", s->count);
      for (int i = 'a'; i <= 'h'; i++)
	printf (" %c=%lld", i, s->regs[i - 'a']);
      printf ("\n");
    }
    sscanf (program[s->pc], "%*s %9s %9s", arg1, arg2);
    switch ((program[s->pc][0] << 8) + program[s->pc][1]) {
    case ('s' << 8) + 'e': // set X Y
      set (s, arg1, get (s, arg2));
      break;
    case ('s' << 8) + 'u': // sub X Y
      set (s, arg1, get (s, arg1) - get (s, arg2));
      break;
    case ('a' << 8) + 'd': // add X Y
      set (s, arg1, get (s, arg1) + get (s, arg2));
      break;
    case ('m' << 8) + 'u': // mul X Y
      s->mul++;
      set (s, arg1, get (s, arg1) * get (s, arg2));
      break;
    case ('m' << 8) + 'o': // mod X Y
      set (s, arg1, get (s, arg1) % get (s, arg2));
      break;
    case ('j' << 8) + 'n': // jnz X Y
      if (get (s, arg1))
	s->pc += get (s, arg2) - 1;
      break;
    default:
      return 1;
    }
    s->count++;
    s->pc++;
  }
  return false;
}

int main(int argc, char **argv)
{
  char buf[400]; // sized based on pre-inspecting file
  int nread = fread (buf, 1, sizeof buf, stdin);
  char *p = buf;
  while (p < buf + nread) {
    program[instr++] = p;
    p = strchr (p, '\n');
    *p++ = '\0';
  }
  printf ("program consists of %d instructions\n", instr);
  states.regs[0] = 1;
  if (argc > 1)
    states.regs[0] = atoi (argv[1]);
  if (argc > 2) {
    states.regs[1] = atoi (argv[2]);
    states.pc = 2;
  }
  if (argc > 3)
    states.regs[2] = atoi (argv[3]);
  run (&states, 0);
  printf ("\nafter %d operations, reg h is %lld\n", states.count,
	  states.regs['h' - 'a']);
  return 0;
}

#else

int main(int argc, char **argv)
{
  int a = 1, b = 0, c = 0, d = 0, e = 0, f = 0, g = 0, h = 0;
  bool skip_a = false;
  if (argc > 1)
    a = atoi (argv[1]);
  if (argc > 2) {
    b = atoi (argv[2]);
    skip_a = true;
  }
  if (argc > 3)
    c = atoi (argv[3]);
  if (skip_a)
    goto l9;

 l1: // set b 57
  b = 57;
 l2: // set c b
  c = b;
 l3: // jnz a 2
  if (a)
    goto l5;
 l4: // jnz 1 5
  goto l9;
 l5: // mul b 100
  b *= 100;
 l6: // sub b -100000
  b -= -100000;
 l7: // set c b
  c = b;
 l8: // sub c -17000
  c -= -17000;
 l9: // set f 1
  f = 1;
 l10: // set d 2
  d = 2;
 l11: // set e 2
  e = 2;
 l12: // set g d
  g = d;
 l13: // mul g e
  g *= e;
 l14: // sub g b
  g -= b;
 l15: // jnz g 2
  if (g)
    goto l17;
 l16: // set f 0
  printf ("b=%d c=%d d=%d e=%d f=%d g=%d h=%d\n", b, c, d, e, f, g, h);
  f = 0;
 l17: // sub e -1
  e -= -1;
 l18: // set g e
  g = e;
 l19: // sub g b
  g -= b;
 l20: // jnz g -8
  if (g)
    goto l12;
 l21: // sub d -1
  d -= -1;
 l22: // set g d
  g = d;
 l23: // sub g b
  g -= b;
 l24: // jnz g -13
  if (g)
    goto l11;
 l25: // jnz f 2
  if (f)
    goto l27;
 l26: // sub h -1
  printf (" b=%d c=%d d=%d e=%d f=%d g=%d h=%d\n", b, c, d, e, f, g, h);
  h -= -1;
 l27: // set g b
  g = b;
 l28: // sub g c
  g -= c;
 l29: // jnz g 2
  if (g)
    goto l31;
 l30: // jnz 1 3
  goto end;
 l31: // sub b -17
  b -= -17;
 l32: // jnz 1 -23
  goto l9;
 end:
  printf ("final value of h: %d\n", h);
}

#endif
