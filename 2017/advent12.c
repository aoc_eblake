#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>

typedef struct node node;
typedef struct neighbor neighbor;
struct neighbor {
  int n;
  neighbor *next;
};
struct node {
  int group;
  neighbor *next;
};
node nodes[2000]; // cheat, by pre-inspecting input file

int main(void)
{
  int count = 0;
  ssize_t nread;
  size_t len = 0;
  char *line = NULL;
  while ((nread = getline(&line, &len, stdin)) >= 0) {
    char *p = line;
    char *e;
    if (strtol (p, &e, 10) != count)
      return 1;
    if (strncmp (e, " <-> ", 5))
      return 2;
    p = e + 5;
    *e = ',';
    while (*e == ',') {
      int n = strtol (p, &e, 10);
      neighbor *next = calloc (1, sizeof *next);
      next->n = n;
      next->next = nodes[count].next;
      nodes[count].next = next;
      p = e + 1;
    }
    count++;
  }
  printf ("parsed connections for %d nodes\n", count);
  int groups = 0;
  int i;
  while (1) {
    for (i = 0; i < count; i++)
      if (!nodes[i].group)
	break;
    if (i == count)
      break;
    bool changed = true;
    nodes[i].group = ++groups;
    while (changed) {
      changed = false;
      for (i = 0; i < count; i++) {
	if (nodes[i].group != groups)
	  continue;
	for (neighbor *next = nodes[i].next; next; next = next->next)
	  if (!nodes[next->n].group)
	    changed = (nodes[next->n].group = groups);
      }
    }
  }
  int size = 0;
  for (i = 0; i < count; i++)
    if (nodes[i].group == 1)
      size++;
  printf ("node 0 among group of %d nodes\n", size);
  printf ("total of %d groups\n", groups);
  return 0;
}
