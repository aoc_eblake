divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dhashsize=H] [-Dfile=day18.input] day18.m4

include(`common.m4')ifelse(common(18, 65537), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`input', translit(include(defn(`file')), nl, `;'))
define(`cnt', 0)
define(`line', `_$0(translit(`$1', ` ', `,'))')
define(`_line', `define(`i'cnt, `$1_')define(`a'cnt, ``$2',`$3'')ifelse(`$3',
  8505, `pushdef(`i'cnt, `hck_')')define(`cnt', incr(cnt))ifdef(`r$2', `',
  `ifelse(index(alpha, `$2'), -1, `', `pushdef(`regs', `$2')define(`r$2',
  0)')')')
ifdef(`__gnu__', `
  patsubst(defn(`input'), `\([^;]*\);', `line(`\1')')
', `
  define(`_chew', `line(substr(`$1', 0, index(`$1', `;')))define(`tail',
    substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(`tail'), `;'), -1,
    `', `$0(defn(`tail'))')')
  define(`chew', `ifelse(eval($1 < 25), 1, `_$0(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  chew(len(defn(`input')), defn(`input'))
')

define(`r_', `ifdef(`r$1', `r$2')$1')
define(`snd_', `define(`part1', r_(`$1', `$3'))incr($5)')
define(`set_', `define(`r$3$1', r_(`$2', `$3'))incr($5)')
define(`add_', `define(`r$3$1', eval(r$3$1 + r_(`$2', `$3')))incr($5)')
define(`mul_', `define(`r$3$1', eval(r$3$1 * r_(`$2', `$3')))incr($5)')
define(`mod_', `define(`r$3$1', eval(r$3$1 % r_(`$2', `$3')))incr($5)')
# peephole for a=0x7fffffff, p=(p*8505*129749+12345)%a
define(`hck_', `ifelse(r$3a, 2147483647, `define(`r$3p', R3(eval(12345+R(R(
  r$3p, 8505), 129749))))', `errprintn(`peephole failed')m4exit(1)')eval($5+5)')
define(`R', `R1(eval(`($1>>16)*($2&0xffff)+$1*!!($2&0x10000)'),
  eval(`($1&0xffff)*($2&0xffff)'))')
define(`R1', `R2($@, eval(`(($1&0x7fff)<<16)+($2&0x7fffffff)'))')
define(`R2', `R3(eval(`(($1>>15)&0x1ffff)+($2<0)+($3<0)+($3&0x7fffffff)'))')
define(`R3', `eval(`($1&0x7fffffff)+($1<0)')')
define(`rcv_', `ifelse(r_(`$1', `$3'), 0, `incr($5)', `pushdef(`run',
  `popdef(`run')')$5')')
define(`jgz_', `ifelse(eval(r_(`$1', `$3')>0), 1, `eval($5+r_(`$2', `$3'))',
  `incr($5)')')
define(`run', `ifdef(`i$1', `_$0(i$1()(a$1, $3, $4, $1), incr($2),
  $3, $4)')')
define(`_run', `run($@)')
run(0, 0)

define(`q0h', 0)define(`q0t', 0)
define(`q1h', 0)define(`q1t', 0)
define(`pc0', 0)define(`pc1', 0)
define(`snd_', `define(`q$4_'q$4t, r_(`$1', `$3'))define(`q$4t',
  incr(q$4t))incr($5)')
define(`rcv_', `ifelse(q$3h, q$3t, `pushdef(`run',
  `popdef(`run')save($'`@)')$5', `define(`r$3$1', defn(`q$3_'q$3h))popdef(
  `q$3_'q$3h)define(`q$3h', incr(q$3h))incr($5)')')
define(`prep', `define(`r0$1', 0)define(`r1$1', 0)')
stack_foreach(`regs', `prep')
define(`r1p', 1)define(`inst', 0)
define(`save', `define(`pc$3', `$1')define(`inst', $2)')
define(`resume', `run(pc$1, inst, $1, eval(1-$1))')
define(`resolve', `ifelse(q0h.q1h, q0t.q1t, `', `resume(1)resume(0)$0()')')
resume(0)resolve()
define(`part2', q0h)

divert`'part1
part2
