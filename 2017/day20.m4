divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dhashsize=H] [-Dfile=day20.input] day20.m4
# Optionally use -Dverbose=1 to see some progress

include(`common.m4')ifelse(common(20, 65537), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`D', defn(`define'))define(`E', defn(`eval'))
define(`cnt', 0)define(`best', 9999999)
define(`abs', ``$1*(1-2*($1<0))'')
define(`d', `E(abs($1)+abs($2)+abs($3))')
define(`P', `D(`p'cnt, `$@')')
define(`V', `D(`v'cnt, `$@')')
define(`A', `D(`a'cnt, `$@')_$0(d($@))D(`cnt', incr(cnt))')
define(`_A', `ifelse($1, best, `pushdef(`group', cnt)', E($1 < best), 1,
  `undefine(`group')D(`group', cnt)D(`best', $1)')')
translit((include(defn(`file'))), `<>pva=', `()PVA')

define(`push', `pushdef(`p$1', defn(`p$1'))pushdef(`v$1',
  defn(`v$1'))pushdef(`a$1', defn(`a$1'))')
define(`pop', `popdef(`p$1')popdef(`v$1')popdef(`a$1')')
define(`b', `D(`$1',E(`$2+$5')`,'E(`$3+$6')`,'E(`$4+$7'))')
define(`adj', `b(`v$1',v$1,a$1)b(`p$1',p$1,v$1)')
define(`settle', `stack_foreach(`group', `_$0')ifdef(`S', `popdef(`S')$0()')')
define(`_settle', `ifdef(`d$1',,`ifelse(check(d(p$1),d(v$1),adj($@)d(p$1),
  d(v$1)),1,`D(`S')')')')
define(`check', `E(`$1>$3||$2>$4')')
define(`find', `define(`best', 9999999)stack_foreach(`group', `_$0')')
define(`_find', `ifelse(eval(d(v$1) < best), 1, `define(`part1', $1)define(
  `best', d(v$1))')')
stack_foreach(`group', `push')
define(`stable', 0)
settle()
find()
stack_foreach(`group', `pop')

define(`part2', cnt)
define(`drop', `define(`part2', decr(part2))')
define(`n', `translit(``$1_$2_$3'', `-', `_')')
define(`adj', `$0_1($2`_'n(p$1))b(`v$1',v$1,a$1)b(`p$1',p$1,v$1)$0_2(
  $3`_'n(p$1),$1)')
define(`adj_1', `popdef(`P$1')')
define(`adj_2', `ifdef(`P$1',`_$0(`$1')D(`d$2')drop()',`D(`P$1',$2)')')
define(`_adj_2', `ifelse(P$1,,,`drop()D(`d'P$1)D(`P$1')')')
define(`settle', `output(1, `...$1 'part2)forloop(0, 'decr(cnt)`, `_$0(',
  `, $1, $2)')ifdef(`S', `popdef(`S')$0($2, incr($2))')')
settle(0, 1)

divert`'part1
part2
