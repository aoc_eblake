divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day16.input] day16.m4

include(`common.m4')ifelse(common(16), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`perm', substr(ALPHA, 0, 16))
define(`name', substr(ALPHA, 0, 16))
define(`spin', `define(`perm', substr(perm, $1)`'substr(perm, 0, $1))')
define(`prep', `define(`S$1', `spin('eval(`16-$1')`)')')
forloop_arg(0, 15, `prep')
define(`xchg', `define(`perm', substr(perm, 0, $1)`'substr(perm, $2,
  1)`'substr(perm, incr($1), eval(`$2-$1-1'))`'substr(perm, $1, 1)`'substr(
  perm, incr($2)))')
define(`_prep', `define(`X$1_$2', `xchg($1, $2)')define(`X$2_$1',
  defn(`X$1_$2'))')
define(`prep', `forloop(incr($1), 15, `_$0($1,', `)')')
forloop_arg(0, 14, `prep')
define(`part', `define(`name', translit(name, `$1$2', `$2$1'))')
define(`_prep', `$0_(substr(ALPHA, $1, 1), substr(ALPHA, $2, 1))')
define(`_prep_', `define(`P$1_$2', `part(`$1', `$2')')define(`P$2_$1',
  defn(`P$1_$2'))')
forloop_arg(0, 14, `prep')
translit((include(defn(`file'))), alpha/, ALPHA`_')
define(`part1', translit(translit(perm, ALPHA, name), ALPHA, alpha))

define(`perm1e0', perm)define(`name1e0', name)
define(`grow', `_$0(`perm', $@)_$0(`name', $@)')
define(`_grow', `define(`$1$4', translit($1$2, ALPHA, $1$3))')
define(`grow10', `grow(`1e$1', `1e$1', `2e$1')grow(`2e$1', `2e$1',
  `4e$1')grow(`1e$1', `4e$1', `5e$1')grow(`5e$1', `5e$1', `1e'incr($1))')
forloop_arg(0, 8, `grow10')
define(`part2', translit(translit(perm1e9, ALPHA, name1e9), ALPHA, alpha))

divert`'part1
part2
