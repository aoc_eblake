#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char **argv)
{
  if (argc != 3)
    return 1;
  unsigned int a = atoi (argv[1]);
  unsigned int b = atoi (argv[2]);
  int match = 0;
  for (int i = 0; i < 5000000; i++) {
    do {
      a = a * 16807ULL % 2147483647;
    } while (a % 4);
    do {
      b = b * 48271ULL % 2147483647;
    } while (b % 8);
    if ((a & 0xffff) == (b & 0xffff))
      match++;
  }
  printf ("total matches: %d\n", match);
  return 0;
}
