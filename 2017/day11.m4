divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day11.input] day11.m4

include(`common.m4')ifelse(common(11), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`input', translit((include(defn(`file'))), nl`,()', `;;'))
define(`x', 0)define(`y', 0)define(`part2', 0)
define(`abs', `ifelse(index(`$1', `-'), 0, `substr(`$1', 1)', `$1')')
define(`dist', `_$0(abs($1), abs($2))')
define(`_dist', `eval($1 + ($2 > $1) * ($2 - $1) / 2)')
define(`check', `_$0(dist(x, y))')
define(`_check', `ifelse(eval($1 > part2), 1, `define(`part2', $1)')')
define(`n_', `define(`y', decr(decr(y)))check()')
define(`ne_', `define(`x', incr(x))define(`y', decr(y))check()')
define(`se_', `define(`x', incr(x))define(`y', incr(y))check()')
define(`s_', `define(`y', incr(incr(y)))check()')
define(`sw_', `define(`x', decr(x))define(`y', incr(y))check()')
define(`nw_', `define(`x', decr(x))define(`y', decr(y))check()')
ifdef(`__gnu__', `
  patsubst(defn(`input'), `\([^;]*\);', `\1_()')
',`
  define(`chew', `substr(`$1', 0, index(`$1', `;'))_()define(`tail',
    substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(`tail'), `;'),
    -1, `', `$0(defn(`tail'))')')
  define(`split', `ifelse(eval($1 < 6), 1, `chew(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  split(len(defn(`input')), defn(`input'))
')
define(`part1', dist(x, y))

divert`'part1
part2
