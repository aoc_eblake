divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day4.input] day4.m4

include(`common.m4')ifelse(common(4), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`input', translit(include(defn(`file')), alpha`'nl, ALPHA`;'))
define(`part1', 0)define(`part2', 0)
define(`check', `ifelse(`$1', `', `-', index(`$*,', `,$1,'), -1,
  `$0(shift($@))')')
define(`arrange', `_$0(`$1')`'ifelse(`$2', `', `', `,$0(shift($@))')')
define(`_arrange', `(translit(defn(`p'len(`$1')),
  `012345678', `$1'))')
define(`p1', `,`0'')
define(`p2', `sort(`0',`1')')
define(`p3', `sort(`0'sort(`1',`2'))')
define(`p4', `sort(`0'sort(`1'sort(`2',`3')))')
define(`p5', `sort(`0'sort(`1'sort(`2'sort(`3', `4'))))')
define(`p6', `sort(`0'sort(`1'sort(`2'sort(`3'sort(`4',`5')))))')
define(`p7', `sort(`0'sort(`1'sort(`2'sort(`3'sort(`4'sort(`5',`6'))))))')
define(`p8',
  `sort(`0'sort(`1'sort(`2'sort(`3'sort(`4'sort(`5'sort(`6',`7')))))))')
define(`_prep', `define(`sort'substr('dquote(ALPHA)`, `$1', 1)`'substr(
  'dquote(ALPHA)`, `$2', 1), eval(`$1 <= $2'))')
define(`prep', `forloop(0, 25, `_$0(`$1', ', `)')')
forloop_arg(0, 25, `prep')
define(`sort', `ifelse(`$2', `', `,`$1'', `ifelse(sort$1$2, 1, `,$@',
  `,`$2'$0(`$1', shift(shift($@)))')')')
define(`line', `_$0(translit(`$1', ` ', `,'))')
define(`_line', `ifelse(check($@), -, `define(`part1', incr(part1))ifelse(
  check(arrange($@)), -, `define(`part2', incr(part2))')')')
ifdef(`__gnu__', `
  patsubst(defn(`input'), `\([^;]*\);', `line(`\1')')
',`
  define(`chew', `line(substr(`$1', 0, index(`$1', `;')))define(`tail',
    substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(
    `tail'), `;'), -1, `', `$0(defn(`tail'))')')
  define(`split', `ifelse(eval($1 < 180), 1, `chew(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  split(len(defn(`input')), defn(`input'))
')

divert`'part1
part2
