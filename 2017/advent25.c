#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <ctype.h>
#include <assert.h>

#define MAX 30000 // educated guess
typedef enum State State;
enum State {
  A, B, C, D, E, F,
};
typedef struct state state;
struct state {
  bool write;
  bool right;
  State next;
};
bool tape[MAX];

// open-coding based on the input, instead of parsing it...
#if 0 // sample
#define LIMIT 6
state states[2][2] = {
  { { true, true, B }, { false, false, B }, },
  { { true, false, A }, { true, true, A }, },
};

#else // day25.input
#define LIMIT 12861455
state states[6][2] = {
  { { true, true, B }, { false, false, B }, },
  { { true, false, C }, { false, true, E }, },
  { { true, true, E }, { false, false, D }, },
  { { true, false, A }, { true, false, A }, },
  { { false, true, A }, { false, true, F }, },
  { { true, true, E }, { true, true, A }, },
};
#endif

int main(int argc, char **argv)
{
  int limit = LIMIT;
  if (argc > 1)
    limit = atoi (argv[1]);
  int sum = 0;
  State S = A;
  int pos = MAX/2;
  int min = MAX;
  int max = 0;
  while (limit--) {
    state *s = &states[S][tape[pos]];
    if (!tape[pos] && s->write)
      sum++;
    else if (tape[pos] && !s->write)
      sum--;
    tape[pos] = s->write;
    S = s->next;
    pos = pos + (s->right ? 1 : -1);
    if (pos < 0 || pos == MAX) {
      printf ("still %d ops to perform, out of tape, used %d-%d\n",
	      limit, min, max);
      return 1;
    }
    if (pos < min)
      min = pos;
    else if (pos > max)
      max = pos;
  }
  printf ("checksum is %d\n", sum);
  return 0;
}
