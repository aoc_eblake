divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day12.input] day12.m4

include(`common.m4')ifelse(common(12), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`input', translit((include(defn(`file'))), nl`,()', `;'))
define(`line', `_$0(translit(`$1', ` ', `,'))')
define(`_line', `define(`n', $1)ifdef(`G$1', `', `pushdef(`g$1',
  $1)define(`G$1', $1)')pipe(G$1, shift(shift($@)))')
define(`pipe', `ifelse(`$2', `', `', `ifdef(`G$2', `ifelse($1, G$2, `',
  `merge(ifelse(eval($1 < G$2), 1, `$1, G$2', `G$2, $1'))')', `pushdef(`g$1',
  $2)define(`G$2', $1)')$0(G$1, shift(shift($@)))')')
define(`merge', `ifdef(`g$2', `pushdef(`g$1', g$2)define(`G'g$2,
  $1)popdef(`g$2')$0($1, $2)')')
ifdef(`__gnu__', `
  patsubst(defn(`input'), `\([^;]*\);', `line(`\1')')
',`
  define(`chew', `line(substr(`$1', 0, index(`$1', `;')))define(`tail',
    substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(`tail'), `;'),
    -1, `', `$0(defn(`tail'))')')
  define(`split', `ifelse(eval($1 < 90), 1, `chew(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  split(len(defn(`input')), defn(`input'))
')
define(`cnt', `-')
define(`part1', len(stack_foreach(`g0', `cnt')))
define(`part2', len(forloop(0, n, `ifdef(`g'', `, `-')')))

divert`'part1
part2
