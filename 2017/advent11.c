#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

int distance(int nw, int n, int ne) {
  // rotating path so all steps are positive results in same distance...
  while (nw < 0 || n < 0 || ne < 0) {
    int tnw = -ne, tn = nw, tne = n;
    nw = tnw;
    n = tn;
    ne = tne;
  }
  if (nw < ne) {
    n += nw;
    ne -= nw;
    nw = 0;
  } else {
    n += ne;
    nw -= ne;
    ne = 0;
  }
  return nw + n + ne;
}

int main(int argc, char **argv)
{
  if (argc != 2)
    return 1;
  int nw = 0, n = 0, ne = 0;
  char *p = argv[1];
  char *next = p;
  int count = 0;
  int max = 0, dist = 0;
  while (*next && (next = strchrnul(p, ','))) {
    count++;
    if ((next - p) == 1)
      n += *p == 'n' ? 1 : -1;
    else if (p[1] == 'e')
      if (*p == 'n')
	ne++;
      else
	nw--;
    else if (*p == 'n')
      nw++;
    else
      ne--;
    dist = distance(nw, n, ne);
    if (max < dist)
      max = dist;
    p = next + 1;
  }
  printf("after %d steps, nw=%d n=%d ne=%d\n", count, nw, n, ne);
  printf("max distance %d, final distance %d\n", max, dist);
  return 0;
}
