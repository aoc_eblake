divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day13.input] day13.m4

include(`common.m4')ifelse(common(13), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`input', translit(include(defn(`file')), nl`:', `;'))
ifdef(`__gnu__', `
  patsubst(defn(`input'), `\([^ ]*\) \([^ ]*\);', `pushdef(`list', `\1,\2')')
',`
  define(`setup', `pushdef(`list', `$1,$2')')
  define(`chew', `setup(translit(substr(`$1', 0, index(`$1', `;')), ` ',
    `,'))define(`tail', substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(
    `tail'), `;'), -1, `', `$0(defn(`tail'))')')
  define(`split', `ifelse(eval($1 < 12), 1, `chew(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  split(len(defn(`input')), defn(`input'))
')
define(`part1', 0)
define(`visit', `_$0($1)')
define(`_visit', `ifelse(eval($1%(($2-1)*2)), 0, `define(`part1',
  eval(part1 + $1*$2))')')
stack_foreach(`list', `visit')

define(`mods')
define(`sort', `ifelse($2, `', ``,$1'', `ifelse($1, $2, ``,'shift($@)',
  eval(`$1 < $2'), 1, ``,$@'', ``,$2'$0(`$1', shift(shift($@)))')')')
define(`prep', `_$0($1, eval($2*2-2))')
define(`_prep', `define(`mods', quote(sort($2mods)))define(`neq$2',
  defn(`neq$2')`,'eval((60*$2-$1)%$2))')
_stack_foreach(`list', `prep(first(', `))', `t')
define(`cmp', `eval((($1) > ($2)) - (($1) < ($2)))')
define(`gcd', `ifelse($2, 0, $1, `$0($2, eval(`$1 % $2'))')')
define(`lcm', `eval($1 / gcd($1, $2) * $2)')
define(`whiledef', `ifdef(`$1', `$2$1$3popdef(`$1')$0($@)')')
define(`l', 1)pushdef(`r1', 0)
define(`filter', `whiledef(`r'l, `_$0('l`, $1, ', `)'define(`l', lcm($1, l)))')
define(`_filter', `forloop(0, eval(l/$1-1), `try($2, eval($3+$1*', `))')')
define(`try', `ifelse(index(defn(`neq$1')`,', `,'eval(`$2%$1')`,'), -1,
  `pushdef(`r'l, `$2')')')
foreach(`filter'mods)
define(`part2', defn(`r'l))
define(`min', `ifelse(eval(`$1 < 'part2), 1, `define(`part2', $1)')')
whiledef(`r'l, `min(', `)')

divert`'part1
part2
