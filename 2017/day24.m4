divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day24.input] day24.m4

include(`common.m4')ifelse(common(24), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`input', translit(include(defn(`file')), nl, `;'))
define(`line', `_$0(translit(`$1', `/', `,'))')
define(`_line', `ifelse(`$1', `$2', `define(`d$1')', `define(`l$1',
  defn(`l$1')`,`$2'')define(`l$2', defn(`l$2')`,`$1'')')')
ifdef(`__gnu__', `
  patsubst(defn(`input'), `\([^;]*\);', `line(`\1')')
', `
  define(`_chew', `line(substr(`$1', 0, index(`$1', `;')))define(`tail',
    substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(`tail'), `;'), -1,
    `', `$0(defn(`tail'))')')
  define(`chew', `ifelse(eval($1 < 12), 1, `_$0(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  chew(len(defn(`input')), defn(`input'))
')

define(`X', 0)define(`B', 0)define(`Y', 0)
define(`D', defn(`define'))define(`P', defn(`popdef'))define(`A', defn(`incr'))
define(`E', defn(`eval'))define(`F', defn(`ifelse'))define(`I', defn(`ifdef'))
define(`V', `I(`n$1_$2',,`D(`n$1_$2')D(`n$2_$1')_$0(`$1',`$2',''dnl
``F(I(`d$2',-)I(`n$2',=),`-',`D(`n$2')E(`$1+$2*3+$3'),A(A(`$4')),`P(`n$2')'',
`E(`$1+$2+$3'),A(`$4')'))P(`n$1_$2')P(`n$2_$1')')')
define(`_V', `F(E(`$3>'X),1,`D(`X',`$3')')F(E(`$4>'B`||($3>'Y`&&$4=='B`)'),
1,`D(`B',`$4')D(`Y',`$3')')_foreach(`V(`$2',',`,`$3',`$4')',l$2)$5')
V(0, 0, 0, 0)
define(`part1', X)
define(`part2', Y)

divert`'part1
part2
