divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day3.input] day3.m4

include(`common.m4')ifelse(common(3), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`list', translit(include(file), `^>v<'nl, `NESW'))
define(`offset', 8000)define(`x', offset)define(`y', offset)
define(`x0', offset)define(`x1', offset)define(`y0', offset)define(`y1', offset)
define(`part1', 0)define(`part2', 0)define(`s', 0)
define(`visit', `ifdef(`h$1_$2_$3', `', `define(`h$1_$2_$3')define(`part$1',
  incr(defn(`part$1')))')')
define(`flip', `define(`s', ifelse(s, 0, 1, 0))')
visit(1, x, y)visit(2, x0, y0)visit(2, x1, y1)
define(`N_', `define(`y', incr(y))visit(1, x, y)define(`y's,
  incr(defn(`y's)))visit(2, defn(`x's), defn(`y's))flip()')
define(`E_', `define(`x', incr(x))visit(1, x, y)define(`x's,
  incr(defn(`x's)))visit(2, defn(`x's), defn(`y's))flip()')
define(`S_', `define(`y', decr(y))visit(1, x, y)define(`y's,
  decr(defn(`y's)))visit(2, defn(`x's), defn(`y's))flip()')
define(`W_', `define(`x', decr(x))visit(1, x, y)define(`x's,
  decr(defn(`x's)))visit(2, defn(`x's), defn(`y's))flip()')
ifdef(`__gnu__', `
  patsubst(defn(`list'), `\(.\)', `\1_()')
',`
  define(`split', `ifelse($1, 1, `$2_()', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  split(len(defn(`list')), defn(`list'))
')

divert`'part1
part2
