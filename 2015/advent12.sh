if [[ ! $part1 ]]; then
    exec < <(jq 'delpaths([path(..|select(type=="object" and .[] == "red"))])')
fi

sum=0
for val in $(tr -c '0-9-' ' '); do
    sum=$((sum+val))
done
echo $sum
