unset places distances
declare -A places distances
count=0
while read a to b eq l; do
    if [[ ! ${places[$a]+set} ]]; then
	places[$a]=$a
	list[$count]=$a
	count=$((count+1))
    fi
    if [[ ! ${places[$b]+set} ]]; then
	places[$b]=$b
	list[$count]=$b
	count=$((count+1))
    fi
    distances[$a.$b]=$l
    distances[$b.$a]=$l
done
echo "parsed ${#list[*]} places: ${list[*]}"
attempt=0
indent=
shortest=9999999999999
longest=0
# debug message
debug() {
    [[ ! ${DEBUG+set} ]] && return
    echo "$*"
}
# visit seed places...
visit() {
    debug "${indent}visit $@"
    indent+=" "
    local seed=$1 place
    shift
    for place; do
	compute $seed $place $*
    done
    indent=${indent%?}
}
# compute seed start places... # places includes start
compute() {
    debug "${indent}compute $@"
    indent+=" "
    if [[ $# == 3 ]]; then
	debug "${indent}computed $1"
	try[$attempt]=$1
	[[ $1 -lt $shortest ]] && shortest=$1
	[[ $1 -gt $longest ]] && longest=$1
	attempt=$((attempt+1))
	indent=${indent%?}
	return
    fi
    local sublist=() seed=$1 start=$2 place
    shift 2
    for place; do
	[[ $place != $start ]] && sublist+=($place)
    done
    for place in ${sublist[*]}; do
	try $seed $start $place ${sublist[*]}
    done
    indent=${indent%?}
}
# try seed start dest places... # places includes dest, but not start
try() {
    debug "${indent}try $@"
    indent+=" "
    local seed=$1 start=$2
    shift 2
    compute $(($seed+${distances[$start.$1]})) $*
    indent=${indent%?}
}
visit 0 ${list[*]}
echo "computed $attempt permutations, shortest $shortest, longest $longest"
