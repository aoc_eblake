iters=${1-100}
declare -A grid next # use namerefs and function to toggle between them
i=0
while read line; do
    j=0
    while read cell; do
	if [[ $cell == '#' ]]; then
	    grid[$i.$j]=1
	else
	    grid[$i.$j]=0
	fi
	: $((j++))
    done < <(printf $line | sed 's/\(.\)/\1\n/g')
    : $((i++))
done
w=$j h=$i
# count [name] # set $count to the number of cells turned on in ${grid[*]}
count() {
    declare -n g=${1-grid}
    local i=0 j
    count=0
    while [[ $i -lt $h ]]; do
	j=0
	while [[ $j -lt $w ]]; do
	    count=$((count + g[$i.$j]))
	    : $((j++))
	done
	: $((i++))
    done
}
if [[ ! $part1 ]]; then
    grid[0.0]=1
    grid[0.$((w-1))]=1
    grid[$((h-1)).0]=1
    grid[$((h-1)).$((w-1))]=1
fi
count
echo "initial $w*$h grid contains ${#grid[*]} cells, $count on"
# generate old new # compute the next generation in new reading from old
generate() {
    declare -n old=$1 new=$2
    local i=0 j neighbors
    while [[ $i -lt $h ]]; do
	j=0
	while [[ $j -lt $w ]]; do
	    neighbors=$((old[$((i-1)).$((j-1))] +
			 old[$((i-1)).$((j  ))] +
			 old[$((i-1)).$((j+1))] +
			 old[$((i  )).$((j-1))] +
			 old[$((i  )).$((j+1))] +
			 old[$((i+1)).$((j-1))] +
			 old[$((i+1)).$((j  ))] +
			 old[$((i+1)).$((j+1))]))
	    new[$i.$j]=$((neighbors == 3 || (neighbors == 2 && old[$i.$j])))
	    # echo "cell $i.$j has $neighbors on, new state ${new[$i.$j]}"
	    : $((j++))
	done
	: $((i++))
    done
    if [[ ! $part1 ]]; then
	new[0.0]=1
	new[0.$((w-1))]=1
	new[$((h-1)).0]=1
	new[$((h-1)).$((w-1))]=1
    fi
}
iter=0
while [[ $iter -lt $iters ]]; do
    echo "computing generation $iter"
    if ((iter%2)); then # odd iteration
	generate next grid
    else # even iteration
	generate grid next
    fi
    : $((iter++))
done
if ((iter%2)); then
    count next
else
    count grid
fi
echo "final grid has $count cells on"
