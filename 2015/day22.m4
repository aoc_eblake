divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day22.input] [-Dhashsize=H] day22.m4

include(`common.m4')ifelse(common(22, 65537), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`prep', `define(`boss', $3)define(`damage', $5)')
prep(translit(include(defn(`file')), ` 'nl, `,,'))
define(`part1', 100000)define(`part2', 1000000)

define(`cost0', 53)define(`cast0', `define(`boss', eval(boss - 4))')
define(`cost1', 73)define(`cast1', `define(`boss', decr(decr(boss)))define(`hp',
  incr(incr(hp)))')
define(`cost2', 113)define(`cast2', `define(`shield', 6)')
define(`cost3', 173)define(`cast3', `define(`poison', 6)')
define(`cost4', 229)define(`cast4', `define(`recharge', 5)')
define(`cast', `ifelse(eval($2 > mana), 1, -, `define(`mana',
  eval(mana - $2))define(`spent', eval(spent + $2))cast$1()')')
define(`win', `_$0($1, `part'incr(hard))')
define(`_win', `ifelse(eval($1 < $2), 1, `define(`$2', $1)')')

# gen spell hp mana spent shield poison recharge boss
define(`queue', `_$0(incr($1), shift($@))')
define(`_queue', `ifdef(`b$2_$3_$4_$5_$6_$7_$8_$9_'hard, `', `pushdef(`gen$1',
  `$@')define(`b$2_$3_$4_$5_$6_$7_$8_$9_'hard)')')
define(`battle', `ifelse(eval($5 > defn(`part'incr(hard)) || $3 == hard), 1,
  `', `define(`hp', eval($3 - hard))
  define(`mana', $4)
  define(`spent', $5)
  define(`boss', $9)
  define(`shield', ifelse($6, 0, 0, `decr($6)'))
  define(`poison', ifelse($7, 0, 0, `decr($7)define(`boss', eval(boss - 3))'))
  define(`recharge', ifelse($8, 0, 0, `decr($8)define(`mana',
    eval(mana + 101))'))
  ifelse(cast($2, cost$2), `', `
  ifelse(shield, 0, `', `define(`shield', decr(shield))')
  ifelse(poison, 0, `', `define(`boss', eval(boss - 3))define(`poison',
    decr(poison))')
  ifelse(recharge, 0, `', `define(`mana', eval(mana + 101))define(`recharge',
    decr(recharge))')
  ifelse(eval(boss <= 0), 1, `win(spent)', `
  define(`hp', eval(hp - damage + ifelse(shield, 0, 0, 7)))
  ifelse(eval(hp > 0), 1, `
  queue($1, 0, hp, mana, spent, shield, poison, recharge, boss)
  queue($1, 1, hp, mana, spent, shield, poison, recharge, boss)
  ifelse(eval(shield | 1), 1, `queue($1, 2, hp, mana, spent, shield, poison,
    recharge, boss)')
  ifelse(eval(poison | 1), 1, `queue($1, 3, hp, mana, spent, shield, poison,
    recharge, boss)')
  ifelse(recharge, 0, `queue($1, 4, hp, mana, spent, shield, poison, recharge,
    boss)')
  ')')')')')

define(`do', `output(1, $1)_$0($1)ifdef(`gen'incr($1), `$0(incr($1))')')
define(`_do', `ifdef(`gen$1', `battle(gen$1)popdef(`gen$1')$0($1)')')
pushdef(`boss', boss)define(`hard', 0)
forloop(0, 4, `queue(-1, ', `, 50, 500, 0, 0, 0, 0, boss)')
do(0)
popdef(`boss')define(`hard', 1)
forloop(0, 4, `queue(-1, ', `, 50, 500, 0, 0, 0, 0, boss)')
do(0)

divert`'part1
part2
