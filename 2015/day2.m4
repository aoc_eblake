divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day2.input] day2.m4

include(`common.m4')ifelse(common(2), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`list', translit(include(file), nl, `;'))
define(`part1', 0)define(`part2', 0)
define(`sort', `ifelse(eval($1 < $2), 1, `ifelse(eval($2 < $3), 1, `$1, $2, $3',
  `$1, $3, $2')', `ifelse(eval($1 < $3), 1, `$2, $1, $3', `$2, $3, $1')')')
define(`line', `_$0(sort($@))')
define(`_line', `define(`part1', eval(part1 + 3 * $1 * $2 + 2 * $1 * $3 +
  2 * $2 * $3))define(`part2', eval(part2 + 2 * ($1 + $2) + $1 * $2 * $3))')
ifdef(`__gnu__', `
  patsubst(defn(`list'), `\([0-9]*\)x\([0-9]*\)x\([0-9]*\);',
    `line(\1, \2, \3)')
',`
  define(`chew', `line(translit(substr(`$1', 0, index(`$1', `;')), `x',
    `,'))define(`tail', substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(
    `tail'), `;'), -1, `', `$0(defn(`tail'))')')
  define(`split', `ifelse(eval($1 < 20), 1, `chew(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  split(len(defn(`list')), defn(`list'))
')

divert`'part1
part2
