wrap=0 # part 1
ribbon=0 # part 2
while read line; do
    x=${line%%x*}
    t=${line#*x}
    t=${t%%x*}
    if test $t -gt $x; then
	y=$t
    else
	y=$x
	x=$t
    fi
    t=${line##*x}
    if test $t -gt $y; then
	z=$t
    else
	z=$y
	y=$t
    fi
    wrap=$(( wrap + 3*x*y + 2*x*z + 2*y*z ))
    ribbon=$(( ribbon + 2*(x+y) + x*y*z ))
done
echo $wrap $ribbon
