if [[ $part1 ]]; then

nice=0
while read word; do
    case $word in
	*ab*|*cd*|*pq*|*xy*) ;;
	*[aeiou]*[aeiou]*[aeiou]*)
        case $word in
	    *aa*|*bb*|*cc*|*dd*|*ee*|*ff*|*gg*|*hh*|*ii*|*jj*|*kk*|*ll*|*mm*|\
	    *nn*|*oo*|*pp*|*qq*|*rr*|*ss*|*tt*|*uu*|*vv*|*ww*|*xx*|*yy*|*zz*)
		nice=$((nice+1))
	esac
    esac
done
echo $nice

else

nice=0
patt1=
for l1 in {a..z}; do
    for l2 in {a..z}; do
	patt1+="|*$l1$l2*$l1$l2*"
    done
done
patt2=
for l in {a..z}; do
    patt2+="|*$l?$l*"
done
while read word; do
    eval 'case $word in '"${patt1##?}"') ;; *) continue ;; esac'
    eval 'case $word in '"${patt2##?}"') ;; *) continue ;; esac'
    nice=$((nice+1))
done
echo $nice

fi
