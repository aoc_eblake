divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day12.input] day12.m4

include(`common.m4')ifelse(common(12), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`input', translit((include(defn(`file'))), `,()'nl, `;'))
define(`scrubbed', translit(defn(`input'), `:"abcdefghijklmnopqrstuvwxyz[]{}',
  `;'))
define(`part1', 0)define(`part2', 0)define(`Red')define(`Num')
define(`str', `ifelse(`$1', `"', `popdef(`$0')ifelse(defn(`Str')Value, `red-',
  `define(`Red', -)')define(`Value')', `define(`Str', defn(`Str')`$1')')')
define(`lex', `ifelse(translit(`$1', `-0123456789'), `', `define(`Num',
  defn(`Num')`$1')', defn(`Num'), `', `', `define(`part2', eval(Num +
  part2))define(`Num')')ifelse(`$1', `:', `define(`Value', -)', `$1', `"',
  `define(`Str')pushdef(`$0', defn(`str'))', `$1', `{', `pushdef(`part2',
  0)pushdef(`Red')define(`Value')', `$1', `}', `ifelse(Red, `',
  `define(`part2', eval(part2 + popdef(`part2')part2))', `popdef(
  `part2')')popdef(`Red')define(`Value')', `define(`Value')')')
ifdef(`__gnu__', `
  define(`part1', eval(patsubst(defn(`scrubbed'), `;', `+ ')))
  patsubst(defn(`input'), `.', `lex(`\&')')
',`
  define(`add', `ifelse(`$1', `', `', `define(`part1', eval(part1 + $1))')')
  define(`chew', `add(substr(`$1', 0, index(`$1', `;')))define(`tail',
    substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(
    `tail'), `;'), -1, `', `$0(defn(`tail'))')')
  define(`split', `ifelse(eval($1 < $3), 1, `$4(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)), `$3', `$4')$0(eval(len(defn(`tail'))
    + $1 - $1/2), defn(`tail')substr(`$2', eval($1/2)), `$3', `$4')')')
  split(len(defn(`scrubbed')`;'), defn(`scrubbed')`;', 10, `chew')
  split(len(defn(`input')), defn(`input'), 2, `lex')
')

divert`'part1
part2
