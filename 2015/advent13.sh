debug() {
    [[ ! ${DEBUG+set} ]] && return
    echo "$*"
}
indent=
# http://www.geeksforgeeks.org/heaps-algorithm-for-generating-permutations/
# _permute size n # helper for permute, munges $arr
_permute() {
    debug "$indent in _permute $@ ${arr[*]}"
    indent+=" "
    if [[ $1 == 1 ]]; then
	shift 2
	echo ${arr[*]}
	indent=${indent%?}
	return
    fi
    local size=$1 n=$2 tmp i
    i=0
    while [[ $i -lt $size ]]; do
	_permute $((size-1)) $n
	if [[ $((size%2)) == 1 ]]; then
	    debug "$indent $size odd, swapping first and last"
	    tmp=${arr[0]}
	    arr[0]=${arr[$((size-1))]}
	    arr[$((size-1))]=$tmp
	else
	    debug "$indent $size even, swapping $i and last"
	    tmp=${arr[$i]}
	    arr[$i]=${arr[$((size-1))]}
	    arr[$((size-1))]=$tmp
	fi
	i=$((i+1))
    done
    debug "$indent leaving $size $n ${arr[*]}"
    indent=${indent%?}
}
# permute n # outputs n! lines, for each possible permutation
permute() {
    local arr=() i=0
    while [[ $i -lt $1 ]]; do
	arr+=($i)
	i=$((i+1))
    done
    _permute $1 $1
}

unset people change
declare -A change
count=0 lines=0
while read a amt b; do
    : $((lines++))
    case " ${people[*]} " in
	*" $a "*) ;;
	*) people[$((count++))]=$a ;;
    esac
    change[$a.$b]=$amt
done < <(sed 's/would gain //; s/would lose /-/; s/happ.*next to //; s/.$//')
echo "read $lines lines, found ${#change[*]} rules among ${#people[*]} people:"
echo ${people[*]}
[[ ! $part1 ]] && people+=("you")
max=0
while read first rest; do
    echo trying $first $rest
    sum=0
    set $rest $first
    prev=$first
    for next; do
	sum=$((${change[${people[$prev]}.${people[$next]}]}+sum))
	sum=$((${change[${people[$next]}.${people[$prev]}]}+sum))
	prev=$next
    done
    echo $sum
    [[ $sum -gt $max ]] && max=$sum
done < <(permute ${#people[*]})
echo "max happiness $max"
