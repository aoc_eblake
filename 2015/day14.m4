divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day14.input] day14.m4

include(`common.m4')ifelse(common(14), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`min', `ifelse(eval($1 < $2), 1, $1, $2)')
define(`compute', `$6(`$1', eval($2 * ($3 * ($5 / ($3 + $4)) +
  min($5 % ($3 + $4), $3))))')
define(`phrase1', `define(`$1', 0)`,' dquote(dquote(`$1', $4, $7,')
define(`phrase2', `$6))')
define(`_slurp', `ifelse(`$#', 1, `', `phrase1(translit(`$1', ` ',
  `,'))phrase2(translit(`$2', ` ', `,'))$0(shift(shift($@)))')')
define(`slurp', `define(`list', _$0$1)')
slurp(translit((include(defn(`file'))), `.'nl, `,'))
define(`find', `ifelse(eval($2 > max), 1, `define(`max', $2)')')
define(`score', `ifelse($2, max, `define(`$1', incr($1))')')
define(`do', `compute($1, $2, `$3')')
define(`round', `define(`max', 0)_foreach(`do(', `, $1, `find')',
  list)_foreach(`do(', `, $1, `score')', list)')
forloop_arg(1, 2503, `round')
define(`part1', max)define(`part2', 0)
define(`best', `_$0($1)')
define(`_best', `ifelse(eval($1 > part2), 1, `define(`part2', $1)')')
foreach(`best'list)

divert`'part1
part2
