# visit n limit prefix... # output all visits of n items that sum to limit
visit() {
    local n=$1 limit=$2 i
    shift 2
    if [[ $n == 1 ]]; then
	echo $* $limit
	return
    fi
    i=0
    while [[ $i -le $limit ]]; do
	visit $((n - 1)) $((limit - i)) $* $i
	: $((i++))
    done
}
debug() {
    [[ ! ${DEBUG+set} ]] && return
    echo "$*"
}

count=0
regex='(.*): capacity (.*), durability (.*), flavor (.*),'
regex+=' texture (.*), calories (.*)'
while read line; do
    [[ $line =~ $regex ]] || { echo 'unable to parse line'; exit 1; }
    name[$count]=${BASH_REMATCH[1]}
    cap[$count]=${BASH_REMATCH[2]}
    dur[$count]=${BASH_REMATCH[3]}
    fla[$count]=${BASH_REMATCH[4]}
    tex[$count]=${BASH_REMATCH[5]}
    cal[$count]=${BASH_REMATCH[6]}
    : $((count++))
done
echo "parsed details on $count ingredients"
# score values... # set score to the combination of the values
score() {
    local key=$*
    debug "score for weights $key: "
    if [[ $# != $count ]]; then
	echo "wrong number of weights"; exit 1
    fi
    local weight=0 count=0 i
    c=0 d=0 f=0 t=0 C=0
    for i; do
	if [[ $1 -lt 0 || $(($1 + weight)) -gt 100 ]]; then
	    echo "huh? $1 $i $weight"
	    score=0
	    return
	fi
	weight=$((weight + $1))
	c=$((c + $1 * ${cap[$count]}))
	d=$((d + $1 * ${dur[$count]}))
	f=$((f + $1 * ${fla[$count]}))
	t=$((t + $1 * ${tex[$count]}))
	C=$((C + $1 * ${cal[$count]}))
	: $((count++))
	shift
    done
    if [[ $C != 500 ]]; then
	score=0
    elif case $c$d$f$t in *-*) score=0; false ;; *) : ;; esac; then
	score=$((c * d * f * t))
    fi
    debug "$score"
}
best=0 tries=0
while read line; do
    ! ((tries % 1000)) && echo $tries
    : $((tries++))
    score $line
    [[ $best -lt $score ]] && best=$score
done < <(visit $count 100)
echo "after $tries tries, best score is $best"
