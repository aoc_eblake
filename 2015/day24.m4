divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day24.input] [-Dhashsize=H] day24.m4
# Optionally use -Dverbose=1 to see some progress

include(`common.m4')ifelse(common(24, 65537), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

include(`math64.m4')
define(`n', 0)define(`sum', 0)define(`even', 0)
define(`item', `ifelse($1, `', `', `define(`i'n, $1)define(`n',
  incr(n))ifelse(eval($1 % 2), 0, `define(`even', 1)')define(`sum',
  eval(sum + $1))$0(shift($@))')')
item(translit(include(defn(`file')), nl, `,'))
define(`cnt', 0)
define(`bump', `ifelse(eval(cnt % 100), 0, `output(1, ...cnt)')define(`cnt',
  incr(cnt))')

define(`body1', `_$0(eval($1 + i$2), $2)')
define(`_body1', `ifelse(eval(0 < $1 - target), 0, `ifdef(`li$1', `define(
  `li$1', defn(`li$1')`,$2')', `define(`li$1', $2)pushdef(`li', $1)')')')
define(`body0', `_foreach(`body1(', `, $1)',
  _stack_foreach(`li', `,', `', `t'))')
define(`recur', `ifdef(`r$1_$2', `', `define(`r$1_$2', _foreach(`_$0(',
  `, $@)', `', li$1))bump()')r$1_$2`'')
define(`_recur', `ifelse($1, -1, ``, `''', eval($3 <= $1), 0,
  `_foreach(`body2(i$1, ', `, $4)', recur(eval($2 - i$1), $1, decr($4)))')')
define(`body2', `ifelse(eval(_$0($2) <= $3), 1, ``, `$2,$1''')')
define(`_body2', `$#')

define(`find', `_$0(incr($1), eval($2 + defn(`i'eval(n - $1 - 1))))')
define(`_find', `ifelse(eval($2 >= target && (even ||
  (target & 1) == ($2 & 1))), 1, $1, `find($@)')')
define(`prod', `ifelse($2, `', $1, `$0(mul64($1, $2), shift(shift($@)))')')
define(`check', `ifelse(ifdef(`best', `lt64($1, best)', 1), 1,
  `define(`best', $1)')')

define(`clean', `ifdef(`li', `undefine(`li'li)popdef(`li')$0()',
  `popdef(`maxlen')popdef(`target')best`'popdef(`best')')')
define(`subsets', `pushdef(`target', $1)pushdef(`maxlen', find(0, 0))output(1,
  `prepping groups of length 'maxlen` adding to 'target)define(`li0',
  -1)pushdef(`li', 0)forloop_arg(0, decr(n), `body0')output(1,
  `collecting groups')_foreach(`check(prod(1first(', `)))', recur($1,
  n, maxlen)output(1, `finding best quantum entanglement'))clean()')

define(`part1', subsets(eval(sum / 3)))
define(`part2', subsets(eval(sum / 4)))

divert`'part1
part2
