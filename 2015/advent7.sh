unset wires cache
declare -A wires cache
shopt -s extglob
count=0
while IFS=, read op wire; do
    wires[$wire]="$op"
    count=$((count+1))
done < <(sed 's/ -> /,/')
echo "found $count wires"
# Find wire $1, store its value in $val. Recursive.
indent=
call=0
get() {
    call=$((call+1))
    if [[ call -gt $((count**2)) ]]; then
	echo "recursing too deep!"; exit 1
    fi
    local a b vala valb mycall
    mycall=$call
    echo "$indent$mycall: looking up wire/value $1 (${wires[$1]})"
    indent+=" "
    if [[ ${cache[$1]+set} ]]; then
	indent=${indent#?}
	echo "${indent}cached: ${cache[$1]}"
	val=${cache[$1]}
	return;
    fi
    case $1_${wires[$1]} in
	*[0-9]_*) val=$1 ;;
	*AND*) read a ignore b <<< "${wires[$1]}"
	       get $a
	       vala=$val
	       get $b
	       valb=$val
	       val=$((vala & valb));;
	*OR*) read a ignore b <<< "${wires[$1]}"
	      get $a
	      vala=$val
	      get $b
	      valb=$val
	      val=$((vala | valb));;
	*NOT*) read ignore a <<< "${wires[$1]}"
	      get $a
	      vala=$val
	      val=$((~vala & 0xffff));;
	*LSHIFT*) read a ignore b <<< "${wires[$1]}"
		  get $a
		  vala=$val
		  val=$(((vala << b) & 0xffff));;
	*RSHIFT*) read a ignore b <<< "${wires[$1]}"
		  get $a
		  vala=$val
		  val=$((vala >> b));;
	*) get ${wires[$1]} ;;
    esac
    indent=${indent#?}
    printf "$indent$mycall: wire $1 contains 0x%x (%d)\n" "$val" "$val"
    cache[$1]=$val
}
get ${1:-a}
if [[ ${1:-a} == a ]]; then
    echo "rewriting wire b"
    unset cache
    declare -A cache
    wires[b]=$val
    get a
fi
