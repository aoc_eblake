valid() {
    case $1 in
	*[iol]*) return 1;;
	*abc*|*bcd*|*cde*|*def*|*efg*|*fgh*|*pqr*|*qrs*|*rst*\
	    |*stu*|*tuv*|*uvw*|*vwx*|*wxy*|*xyz*) ;;
	*) return 1;;
    esac
    first=$(echo $1 | sed '/\(.\)\1/{s/.*\(.\)\1.*/\1/;q}; Q1') || return 1
    echo $1 | sed -n "/\([^$first]\)\1/q; q1"
}
next() {
    case $1 in
	*i?*) next=${1%i*}j
	      next+=${a:${#next}}
	      return;;
	*l?*) next=${1%l*}m
	      next+=${a:${#next}}
	      return;;
	*o?*) next=${1%o*}p
	      next+=${a:${#next}}
	      return;;
    esac
    next=$(echo $1 | sed 's/z$/0/; s/y$/z/; s/x$/y/; s/w$/x/; s/v$/w/;
    		          s/u$/v/; s/t$/u/; s/s$/t/; s/r$/s/; s/q$/r/;
			  s/p$/q/; s/[no]$/p/; s/m$/n/; s/[kl]$/m/;
			  s/j$/k/; s/[hi]$/j/; s/g$/h/; s/f$/g/; s/e$/f/;
			  s/d$/e/; s/c$/d/; s/b$/c/; s/a$/b/; s/0/0a/')
    while case $next in *0*) : ;; *) false ;; esac; do
	next=$(echo $next | sed 's/z0/1/; s/y0/z/; s/x0/y/; s/w0/x/; s/v0/w/;
    		       	    	 s/u0/v/; s/t0/u/; s/s0/t/; s/r0/s/; s/q0/r/;
			      	 s/p0/q/; s/n0/p/; s/m0/n/; s/k0/m/;
			      	 s/j0/k/; s/h0/j/; s/g0/h/; s/f0/g/; s/e0/f/;
			      	 s/d0/e/; s/c0/d/; s/b0/c/; s/a0/b/; s/1/0a/')
    done
}
for arg; do
    a=$(printf '%0*d' ${#arg} 0 | tr 0 a)
    tries=0
    echo "searching for password after $arg"
    while ! valid $arg; do
	tries=$((tries+1))
	next $arg
	arg=$next
	(( $tries % 1000 )) || echo "trying $next"
    done
    echo "found $next after $tries tries"
done
