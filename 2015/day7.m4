divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day7.input] day7.m4

include(`common.m4')ifelse(common(7), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`AND', `&')define(`OR', `|')define(`LSHIFT', `<<')define(`RSHIFT', `>>')
define(`list', translit(include(defn(`file')), nl, `;'))
define(`memo', `define(`$1', `define(`$1', eval($2))$1`'')')
define(`line', `_$0(translit(`$1', ` ', `,'))')
define(`_line', `ifelse(`$#', 3, `memo(`$3_', `$1_')memo(`$3__', `$1__')',
  `$#', 4, `memo(`$4_', `~$2_ & `0xffff'')memo(`$4__', `~$2__ & `0xffff'')',
  `memo(`$5_', `($1_ $2 $3_) & `0xffff'')memo(`$5__',
  `($1__ $2 $3__) & `0xffff'')')')
define(`_')define(`__')
ifdef(`__gnu__', `
  patsubst(defn(`list'), `\([^;]*\);', `line(`\1')')
',`
  define(`chew', `line(substr(`$1', 0, index(`$1', `;')))define(`tail',
    substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(
    `tail'), `;'), -1, `', `$0(defn(`tail'))')')
  define(`split', `ifelse(eval($1 < 40), 1, `chew(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  split(len(defn(`list')), defn(`list'))
')
define(`part1', a_)define(`b__', part1)define(`part2', a__)

divert`'part1
part2
