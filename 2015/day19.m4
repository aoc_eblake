divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day19.input] [-Dhashsize=H] day19.m4

include(`common.m4')ifelse(common(19, 65537), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`part1', 0)define(`max', 0)
define(`input', translit(include(defn(`file')), `e'nl, `E;'))
define(`list', substr(defn(`input'), 0, incr(index(defn(`input'), `;;'))))
define(`mol', translit(substr(defn(`input'), index(defn(`input'), `;;')), `;'))
define(`rev', `ifelse(`$1', `', `', `$0(substr(`$1', 1))`'substr(`$1', 0, 1)')')
define(`line', `pushdef(`$1_', `$2')pushdef(`out', rev(`$2'))define(defn(
  `out')`_', rev(`$1'))ifelse(eval(len(`$2') > max), 1, `define(`max',
  len(`$2'))')')
ifdef(`__gnu__', `
  patsubst(defn(`list'), `\([^ ]*\) => \([^ ]*\);', `line(`\1', `\2')')
',`
  define(`chew', `line(translit(substr(`$1', 0, index(`$1', `;')), `=> ',
    `,'))define(`tail', substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(
    `tail'), `;'), -1, `', `$0(defn(`tail'))')')
  define(`split', `ifelse(eval($1 < 40), 1, `chew(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  split(len(defn(`list')), defn(`list'))
')
define(`res', `ifdef(`$1$2$3', `', `define(`$1$2$3')define(`part1',
  incr(part1))')')
define(`visit', `ifelse(`$2', `', `', index('alpha`, substr(`$2', 1, 1)), -1,
  `_$0(`$1', substr(`$2', 0, 1), substr(`$2', 1))', `_$0(`$1', substr(`$2', 0,
  2), substr(`$2', 2))')')
define(`_visit', `_stack_foreach(`$2_', `res(`$1',', `, `$3')',
  `t')visit(`$1$2', `$3')')
visit(`', mol)

define(`mol0', rev(mol))
define(`check', `ifelse(eval($1 >= 0 && $1 <= best), 1, `define(`best',
  $1)define(`mol$3', substr(`$4', 0, $1)`'defn(`$2_')substr(`$4',
  eval($1 + len(`$2'))))')')
define(`contract', `_stack_foreach(`out', `_$0(', `, $@)', `t')')
define(`_contract', `check(index(`$3', `$1'), $@)')
define(`round', `define(`best', len(mol$1))ifelse(best, 1, `define(`part2',
  $1)', `contract(incr($1), mol$1)$0(incr($1))')')
round(0)

divert`'part1
part2
