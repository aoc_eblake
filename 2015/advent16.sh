while read line; do
    if [[ $line =~ 'children: '([0-9]+) ]]; then
     [[ ${BASH_REMATCH[1]} == 3 ]] || continue
    fi
    if [[ $line =~ 'cats: '([0-9]+) ]]; then
	[[ ${BASH_REMATCH[1]} -gt 7 ]] || continue
    fi
    if [[ $line =~ 'samoyeds: '([0-9]+) ]]; then
	[[ ${BASH_REMATCH[1]} == 2 ]] || continue
    fi
    if [[ $line =~ 'pomeranians: '([0-9]+) ]]; then
	[[ ${BASH_REMATCH[1]} -lt 3 ]] || continue
    fi
    if [[ $line =~ 'akitas: '([0-9]+) ]]; then
	[[ ${BASH_REMATCH[1]} == 0 ]] || continue
    fi
    if [[ $line =~ 'vizslas: '([0-9]+) ]]; then
	[[ ${BASH_REMATCH[1]} == 0 ]] || continue
    fi
    if [[ $line =~ 'goldfish: '([0-9]+) ]]; then
	[[ ${BASH_REMATCH[1]} -lt 5 ]] || continue
    fi
    if [[ $line =~ 'trees: '([0-9]+) ]]; then
	[[ ${BASH_REMATCH[1]} -gt 3 ]] || continue
    fi
    if [[ $line =~ 'cars: '([0-9]+) ]]; then
	[[ ${BASH_REMATCH[1]} == 2 ]] || continue
    fi
    if [[ $line =~ 'perfumes: '([0-9]+) ]]; then
	[[ ${BASH_REMATCH[1]} == 1 ]] || continue
    fi
    echo $line
done
