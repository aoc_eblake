# part 1
echo $(( $(tr -d -c \( < day1.input | wc -c) - \
         $(tr -d -c \) < day1.input | wc -c) ))


# part 2
i=0 floor=0
for act in $(sed 's/(/ +1/g; s/)/ -1/g' < day1.input); do
    i=$((i+1)) floor=$((floor $act))
    if test $floor -lt 0; then
	echo $i
	break
    fi
done
