if [[ $part1 ]]; then

read input
unset houses
declare -A houses
x=0 y=0
houses[0/0]=1
while test "$input"; do
    case ${input%%${input#?}} in
	'<') x=$((x-1)) ;;
	^)   y=$((y+1)) ;;
	'>') x=$((x+1)) ;;
	v)   y=$((y-1)) ;;
	*) echo 'unexpected input'; exit 1 ;;
    esac
    houses[$x/$y]=$(( ${houses[$x/$y]} +1 ))
    input=${input#?}
done
echo ${houses[*]} | tr ' ' '\n' | wc -l

else # part 2

read input
unset houses
declare -A houses
x1=0 y1=0
x2=0 y2=0
houses[0/0]=2
while test "$input"; do
    case ${input%%${input#?}} in
	'<') x1=$((x1-1)) ;;
	^)   y1=$((y1+1)) ;;
	'>') x1=$((x1+1)) ;;
	v)   y1=$((y1-1)) ;;
	*) echo 'unexpected input'; exit 1 ;;
    esac
    houses[$x1/$y1]=$(( ${houses[$x1/$y1]} +1 ))
    input=${input#?}
    case ${input%%${input#?}} in
	'<') x2=$((x2-1)) ;;
	^)   y2=$((y2+1)) ;;
	'>') x2=$((x2+1)) ;;
	v)   y2=$((y2-1)) ;;
	*) echo 'unexpected input'; exit 1 ;;
    esac
    houses[$x2/$y2]=$(( ${houses[$x2/$y2]} +1 ))
    input=${input#?}
done
echo ${houses[*]} | tr ' ' '\n' | wc -l

fi
