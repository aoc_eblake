mapfile -t list < <(sed 's/turn //; s/through //; s/,/ /g')
echo "parsed ${#list[*]} instructions"
count=0
for row in {0..999}; do
    echo "processing row $row"
    for col in {0..999}; do
	line[$col]=0
    done
    # printf " inst 000"
    for ((i = 0; i < ${#list[*]}; i++)); do
	# printf '\b\b\b%3d' $i
	read inst x1 y1 x2 y2 <<< ${list[$i]}
	if [[ $row -lt $y1 || $row -gt $y2 ]]; then
	    # echo " no-op for inst $i on row $row"
	    continue;
	fi
	# echo " $i: running operation $inst over $((x2-x1+1)) cells"
	pos=$x1
	while [[ $pos -le $x2 ]]; do
	    if [[ $part1 ]]; then
		case $inst in
		    on) line[$pos]=1 ;;
		    off) line[$pos]=0 ;;
		    toggle) line[$pos]=$((1-${line[$pos]})) ;;
		    *) echo oops; exit 2 ;;
		esac
	    else
		case $inst in
		    on) line[$pos]=$((${line[$pos]}+1)) ;;
		    off) line[$pos]=$((${line[$pos]}?${line[$pos]}-1:0)) ;;
		    toggle) line[$pos]=$((${line[$pos]}+2)) ;;
		    *) echo oops; exit 2 ;;
		esac
	    fi
	    pos=$((pos + 1))
	done
    done
    subcount=$(( $(echo ${line[*]} | tr ' ' +) ))
    # echo
    printf "row $row adds $subcount\n"
    count=$((count + subcount))
done
echo "final count $count"
