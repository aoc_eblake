i=$(($2+0))
while :; do
    case $(printf $1$i | md5sum) in
	000000*) echo $i; break ;;
	00000*) echo $i; [[ $part1 ]] && break ;;
    esac
    case $i in
	*000) echo " $i" ;;
    esac
    i=$((i+1))
done
