divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day25.input] day25.m4

include(`common.m4')ifelse(common(25), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`To', `<')define(`row', `>')
define(`parse', `define(`r', $2)define(`c', $5)')
parse(translit((include(defn(`file'))), `<> .()', `(),,'))
define(`code', eval(c*(c+1)/ 2 + (c-1)*(r-1) + (r-1)*r/2))

define(`bits', `_$0(eval($1, 2))')
define(`_bits', ifdef(`__gnu__', ``shift(patsubst($1, ., `, \&'))'',
  ``ifelse(len($1), 1, `$1', `substr($1, 0, 1),$0(substr($1, 1))')''))
define(`modmul', `define(`d', 0)define(`b', $2)pushdef(`m', $3)foreach(`_$0',
  bits($1))popdef(`m')d`'')
define(`_modmul', `define(`d', eval((d * 2 + $1 * b) % m))')
define(`modpow', `define(`r', 1)define(`a', $1)pushdef(`m', $3)foreach(`_$0',
  bits($2))popdef(`m')r`'')
define(`_modpow', `define(`r', ifelse($1, 1, `modmul(modmul(r, r, m), a, m)',
  `modmul(r, r, m)'))')
define(`part1', modmul(20151125, modpow(252533, decr(code), 33554393),
  33554393))

divert`'part1
no part2
