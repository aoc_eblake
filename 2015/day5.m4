divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day5.input] day5.m4

include(`common.m4')ifelse(common(5), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`list', translit(include(file), `abcdefghijklmnopqrstuvwxyz'nl,
  `ABCDEFGHIJKLMNOPQRSTUVWXYZ;'))
define(`part1', 0)define(`part2', 0)
define(`check', `ifelse(eval(len(translit(`$1', `BCDFGHJKLMNPQRSTVWXYZ')) >
  2)index(`$1', `AB')index(`$1', `CD')index(`$1', `PQ')index(`$1', `XY'),
  1-1-1-1-1, `double(`$1')')two(`$1')')
ifdef(`__gnu__', `
  define(`double', `regexp(`$1', `\(.\)\1', `define(`part1', incr(part1))')')
  define(`two', `regexp(`$1', `\(..\).*\1', `_$0(`$1')')')
  define(`_two', `regexp(`$1', `\(.\).\1', `define(`part2', incr(part2))')')
  patsubst(defn(`list'), `\([A-Z]*\);', `check(`\1')')
',`
  define(`double', `_$0(substr(`$1', 0, 1), substr(`$1', 1))')
  define(`_double', `ifelse(`$2', `', `', index(`$2', `$1'), 0, `define(
    `part1', incr(part1))', `$0(substr(`$2', 0, 1), substr(`$2', 1))')')
  define(`two', `define(`a')define(`b')_$0(substr(`$1', 0, 1), substr(`$1',
    1, 1), substr(`$1', 2))')
  define(`_two', `ifelse(index(`$3', `$1'), 0, `define(`a', -)')ifelse(
    index(`$3', `$1$2'), -1, `', `define(`b', -)')ifelse(`$3', `', `', a`'b,
    --, `define(`part2', incr(part2))', `$0(`$2', substr(`$3', 0, 1),
    substr(`$3', 1))')')
  define(`chew', `check(substr(`$1', 0, index(`$1', `;')))define(`tail',
    substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(
    `tail'), `;'), -1, `', `$0(defn(`tail'))')')
  define(`split', `ifelse(eval($1 < 40), 1, `chew(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  split(len(defn(`list')), defn(`list'))
')

divert`'part1
part2
