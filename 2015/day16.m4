divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day16.input] day16.m4

include(`common.m4')ifelse(common(16), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`Sue', `check{')
define(`input', translit((include(defn(`file'))), nl`:', `},'))
define(`children', ==3)define(`cats', >7)define(`samoyeds', ==2)
define(`pomeranians', <3)define(`akitas', ==0)define(`vizslas', ==0)
define(`goldfish', <5)define(`trees', >3)define(`cars', ==2)
define(`perfumes', ==1)
define(`check', `ifelse(c1($2, $3)c1($4, $5)c1($6, $7), 111, `define(`part1',
  $1)', c2($2, $3)c2($4, $5)c2($6, $7), 111, `define(`part2', $1)')')
define(`c1', `ifelse(translit($1, `<>='), $2, 1)')
define(`c2', `eval($2 $1)')
translit(defn(`input'), `{}', `()')

divert`'part1
part2
