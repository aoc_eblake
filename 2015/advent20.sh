house=${1-0}
target=${2-33100000}
got=0
max=0
while [[ $got -lt $target ]]; do
    (( house++ % 1000 )) || echo " $((house-1)) at $got, max $max"
    if [[ $house == 1 ]]; then
	got=11
	max=11
	continue
    fi
    got=$(((house + (house <= 50))*11))
    odd=$((house&1))
    i=$((2+$odd))
    while (( i <= 50 && i*i < house )); do
	if ! (( house % i )); then
	    # echo "  $house $i"
	    got=$((got + (house/i)*11))
	    ((house/i <= 50)) && got=$((got + i*11))
	fi
	i=$((i+1+odd))
    done
    (( i <= 50 && i*i == house )) && got=$((got + i*11))
    # echo " house $house got $got"
    [[ $max -lt $got ]] && max=$got
done
echo "house $house got $got gifts"
