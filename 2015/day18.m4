divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Diter=N] [-Dfile=day18.input] [-Dhashsize=H] day18.m4
# Optionally use -Dverbose=1 to see some progress

include(`common.m4')ifelse(common(18, 65537), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

# Operate 6 cells per int, requiring 17*102 ints to represent 100*100 cells
ifdef(`iter', `', `define(`iter', 100)')
define(`grid', translit(include(defn(`file')), `.#'nl, `01;'))
ifdef(`__gnu__', `
  patsubst(defn(`grid'), `\([^;]*\);', `pushdef(`row', `\1')')
', `
  define(`half', `eval($1/100/2*100)')
  define(`split', `ifelse($1, 100, `pushdef(`row', `$2')', `$0(half($1),
    substr(`$2', 0, half($1)))$0(eval($1-half($1)), substr(`$2', half($1)))')')
  split(eval(len(defn(`grid'))/101*100), translit(defn(`grid'), `;'))
')
define(`prep', `define(`c$1_0', 0)define(`c$1_101', 0)')
forloop_arg(0, 16, `prep')
define(`visit', `forloop(1, 100, `_$0$3(`$1', ', `)$2')')
define(`_visit', `forloop(0, 16, `$1(', `, `$2')')')
define(`_visit1', `forloop(0, 16, `$1(', `,`$2','decr(`$2')`,'incr(`$2')`)')')
define(`_visit2_', ``$2$3(`$1', `$2$4', 'decr(`$1')`, 'incr(`$1')`)'')
define(`_visit2', `$1_0(0, `$2', 1)'forloop(1, 15, `_visit2_(',
  `, `$', `1', `2')')`$1_16(16, `$2', 15)')
define(`prep', `define(`c$1_$2', eval(`0x'substr(0row`'0, eval($1 * 6), 6)0))')
visit(`prep', `popdef(`row')')

define(`near', `define(`n$1_$2', _near((c$1_$3+c$1_$2+c$1_$4)))')
define(`_near', `eval(`($1<<4)+$1+($1>>4)')')
define(`set_0', `_set(`$1', `$2', eval((n$1_$2-c$1_$2
  +(n$3_$2>>24))|c$1_$2), `0x00111110')')
define(`set_16', `_set(`$1', `$2', eval((n$1_$2-c$1_$2
  +(n$3_$2<<24))|c$1_$2), `0x01111100')')
define(`set', `_$0(`$1', `$2', eval((n$1_$2-c$1_$2+(n$3_$2<<24)
  +(n$4_$2>>24))|c$1_$2), `0x01111110')')
define(`_set', `define(`c$1_$2',
  eval(`$3&($3>>1)&(~(($3>>2)|($3>>3)))&$4'))')
define(`round', `ifelse($1, 'iter`, `', `ifelse(index($1, 0), -1, `',
 `output(1, $1)')visit(`near', `', `1')visit(`set', `', `2')$2`'$0(incr($1),
 `$2')')')
define(`count', `len(translit(visit(`_$0'), 0))')
define(`_count', `eval(c$1_$2, 16)')

define(`push', `pushdef(`c$1_$2', defn(`c$1_$2'))')
visit(`push')
round(0)
define(`part1', count)
define(`pop', `popdef(`c$1_$2')')
visit(`pop')
define(`stuck', `define(`c0_1', eval(c0_1`|0x00100000'))define(`c16_1',
  eval(c16_1`|0x00000100'))define(`c0_100',
  eval(c0_100`|0x00100000'))define(`c16_100', eval(c16_100`|0x00000100'))')
stuck
round(0, `stuck')
define(`part2', count)

divert`'part1
part2
