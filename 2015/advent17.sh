limit=${1-150}
count=0
while read size; do
    sizes[$((count++))]=$size
done
i=$((1 << $count))
echo "computing $i combinations of ${#sizes[*]} containers for limit $limit"
solutions=0
best=$count
while ((--i)); do
    sum=0
    containers=()
    for j in ${!sizes[*]}; do
	if ((i & (1 << j))); then
	    containers+=(${sizes[j]})
	    sum=$((sum + sizes[j]))
	fi
    done
    if [[ $sum == $limit && ${#containers[*]} -le $best ]]; then
	echo ${containers[*]}
	if [[ $part1 || ${#containers[*]} == $best ]]; then
	    : $((solutions++))
	else
	    echo "better configuration found"
	    solutions=1 best=${#containers[*]}
	fi
    fi
done
echo "$solutions total solutions"
