divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day23.input] day23.m4

include(`common.m4')ifelse(common(23), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

include(`math64.m4')
define(`input', translit((include(defn(`file'))), nl`,()', `;'))
define(`cnt', 0)
define(`line', `_$0(translit(`$1', ` ', `,'))')
define(`_line', `define(`i'cnt, `$1_(`$2', $3)')define(`cnt', incr(cnt))')
ifdef(`__gnu__', `
  patsubst(defn(`input'), `\([^;]*\);', `line(`\1')')
', `
  define(`_chew', `line(substr(`$1', 0, index(`$1', `;')))define(`tail',
    substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(`tail'), `;'), -1,
    `', `$0(defn(`tail'))')')
  define(`chew', `ifelse(eval($1 < 20), 1, `_$0(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  chew(len(defn(`input')), defn(`input'))
')

define(`div10', `substr(`$1', 0, decr(len(`$1')))')
define(`hlf_', `define(`r$1', div10(mul64(r$1, 5)))1')
define(`tpl_', `define(`r$1', mul64(r$1, 3))1')
define(`inc_', `define(`r$1', add64(r$1, 1))1')
define(`jmp_', `$1')
define(`jie_', `ifelse(eval(substr(r$1, decr(len(r$1))) & 1), 0, $2, 1)')
define(`jio_', `ifelse(r$1, 1, $2, 1)')
define(`run', `define(`steps', incr(steps))ifdef(`i'pc, `_$0(pc)$0`'')')
define(`_run', `define(`pc', eval(pc + i$1()))')

define(`ra', 0)define(`rb', 0)define(`pc', 0)define(`steps', 0)
run()
define(`part1', rb)
define(`ra', 1)define(`rb', 0)define(`pc', 0)define(`steps', 0)
run()
define(`part2', rb)

divert`'part1
part2
