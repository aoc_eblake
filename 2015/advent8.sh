[[ $# == 1 ]] || { echo "missing filename"; exit 1; }
echo $(( $(wc -c < $1) -
	 $(sed 's/\\[\"]/B/g; s/\\x../?/g; s/^"//; s/"$//' < $1 | wc -c) ))
echo $(( $(sed 's/[\"]/\\\\/g; s/^/"/; s/$/"/' < $1 | wc -c) - $(wc -c < $1) ))
