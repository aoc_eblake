read _ _ boss_hit
read _ boss_damage
read _ boss_armor
# battle hit damage armor boss_hit damage armor # return 0 if player beats boss
battle() {
    echo "battle $@"
    p=$1 b=$4
    att=$(($2-$6)) def=$(($5-$3))
    (( att < 1 )) && att=1
    (( def < 1 )) && def=1
    turn=0
    while :; do
	b=$((b-att))
	(( b > 0 )) || return 0;
	p=$((p-def))
	(( p > 0 )) || return 1;
    done
}
weapon=('8 4 0' '10 5 0' '25 6 0' '40 7 0' '74 8 0')
armor=('0 0 0' '13 0 1' '31 0 2' '53 0 3' '75 0 4' '102 0 5')
ring=('0 0 0' '25 1 0' '50 2 0' '100 3 0' '20 0 1' '40 0 2' '80 0 3')
lowest=99999
highest=0
# prep w w w a a a r1 r1 r1 r2 r2 r2 # buy stuff to prepare for battle
prep() {
    echo "prep $@"
    cost=$(($1 + $4 + $7 + ${10}))
    if battle 100 $(($2 + $5 + $8 + ${11})) $(($3 + $6 + $9 + ${12})) \
	      $boss_hit $boss_damage $boss_armor; then
	echo "win, after spending $cost"
	[[ $cost -lt $lowest ]] && lowest=$cost
    else
	echo "loss, after spending $cost"
	[[ $cost -gt $highest ]] && highest=$cost
    fi
}
count=0
for w in ${!weapon[*]}; do
    for a in ${!armor[*]}; do
	for r1 in ${!ring[*]}; do
	    for r2 in ${!ring[*]}; do
		(( r1 ? r1 == r2 : r2 )) && continue
		: $((count++))
		prep ${weapon[w]} ${armor[a]} ${ring[r1]} ${ring[r2]}
	    done
	done
    done
done
echo "after trying $count scenarios, cheapest win costs $lowest"
echo "most expensive loss is $highest"
