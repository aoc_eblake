divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dstart=NNN] [-Dtarget=NNN] [-Dfile=day20.input] day20.m4
# Optionally use -Dverbose=1 to see some progress

include(`common.m4')ifelse(common(20), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

ifdef(`target', `', `define(`target', translit(include(defn(`file')), nl))')
define(`bound', eval(target / 11)0)
# Per https://en.wikipedia.org/wiki/Highly_abundant_number, we want a highly
# abundant (HA) number.  360360 is HA with sum 1572480, and all HA numbers
# > 630 are divisible by 12; so limit our search bounds.
ifelse(eval(target / 10 < 1572480), 1, `errprintn(`unexpected input')m4exit(1)')
define(`start', ifdef(`start', `eval((start + 11) / 12)', 360360))

define(`n2', 3)define(`n3', 5)define(`n5', 7)
define(`nextp', `ifdef(`n$1', `', `define(`n$1', _$0(incr(incr($1))))')n$1')
define(`_nextp', `ifelse(eval($1 % 3 && $1 % 5), 0, `$0(incr(incr($1)))',
  `ifelse(nargs(factor($1)), 2, $1, `$0(incr(incr($1)))')')')
define(nargs, `$#')
define(`factor', `ifelse($1, 1, `', `ifelse(eval($1 & 1), 0,
  `,2$0(eval($1 >> 1))', `_$0($1, 3)')')')
define(`_factor', `ifelse($1, 1, `', `ifelse(eval($2 * $2 > $1), 1, `,$1',
  `ifelse(eval($1 % $2), 0, `,$2$0(eval($1 / $2), $2)',
  `$0($1, nextp($2))')')')')

define(`total', `_$0(10, $2, shift($@))')
define(`_total', `ifelse(`$#', 3, `eval($1 * (($2 * $3) - 1) / ($3 - 1))', $3,
  $4, `$0($1, $2 * $3, shift(shift(shift($@))))', `$0(eval($1 * (($2 * $3) - 1)
  / ($3 - 1)), $4, shift(shift(shift($@))))')')
define(`best', 10)
define(`try', `_$0($1factor($1))')
define(`_try', `ifelse(eval(`$# > 8'), 1, `_$0(total($@), $1)')')
define(`__try', `ifelse(eval($1 > best), 1, `define(`best', $1)')ifdef(
  `start', `', `ifelse(eval($1 > bound), 1, `define(`start', $2)')')')
define(`iter', `ifelse(eval($1 % 6000), 0, `output(1, `$1 'best)')try(
  $1)ifelse(eval(best < target), 1, `$0(eval($1 + 12), `$2')',
  `define(`$2', $1)')')
iter(start`'popdef(`start'), `part1')

define(`divisors50', `eval(11 * (_$0($1, 50)))')
define(`_divisors50', `$1ifelse(eval($1 & 1), 0, `_$0($1, $2, 2)')_$0($1,
  $2, 3)')
define(`__divisors50', `ifelse(eval($3 > $2), 1, `', `ifelse(eval($1 % $3), 0,
  `+ eval($1 / $3)')$0($1, $2, incr(incr($3)))')')
define(`best', 10)
define(`try', `_$0(divisors50($1))')
define(`_try', `ifelse(eval($1 > best), 1, `define(`best', $1)')')
iter(start, `part2')

divert`'part1
part2
