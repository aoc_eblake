divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day15.input] day15.m4

include(`common.m4')ifelse(common(15), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`capacity')define(`durability')define(`flavor')define(`texture')
define(`calories')define(`items', 0)
define(`_slurp', `ifelse(`$#', 1, `', `define(`c_'items, $2)define(`d_'items,
  $3)define(`f_'items, $4)define(`t_'items, $5)define(`C_'items, $6)define(
  `items', incr(items))$0(shift(shift(shift(shift(shift(shift($@)))))))')')
define(`slurp', `define(`list', _$0$1)')
slurp(translit((include(defn(`file'))), `:'nl, `,,'))
ifelse(items, 4, `', `errprintn(`unexpected input')m4exit(1)')

define(`part1', 0)define(`part2', 0)
define(`score', `_$0(eval($1_0 * $2 + $1_1 * $3 + $1_2 * $4 + $1_3 * $5))')
define(`_score', `ifelse(index($1, -), 0, 0, $1)')
define(`do', `_$0(eval(score(`c', $@) * score(`d', $@) * score(`f', $@) *
  score(`t', $@)), score(`C', $@))')
define(`_do', `ifelse(eval($1 > part1), 1, `define(`part1', $1)')ifelse($2,
  500, `ifelse(eval($1 > part2), 1, `define(`part2', $1)')')')
forloop_var(`a', 0, 100, `forloop_var(`b', 0, 100 - a, `forloop_var(`c', 0,
  100 - a - b, `do(a, b, c, eval(100 - a - b - c))')')')

divert`'part1
part2
