count=0
sum=0
while read line; do
    size[count++]=$line
    sum=$((sum+line))
done
if [[ $part1 ]]; then
    target=$((sum/3))
else
    target=$((sum/4))
fi
echo "read $count lines, summing to $sum; target $target"
# sum n... # set $sum, return 0 for exact, 1 for too high, 2 for too low
sum() {
    sum=0
    for i; do
	sum=$((sum+i))
    done
    if [[ $sum -gt $target ]]; then
	return 1
    elif [[ $sum -lt $target ]]; then
	return 2
    fi
    return 0
}
# product n... # set $product, possibly $min
product() {
    product=1
    for i; do
	product=$((product*i))
    done
    ((product < min)) && min=$product
}
min=$((1<<50))
calls=0
# pick n prefix sum list... # choose n more items from list, add to prefix/sum
# output the list on success, return 1 if no output issued
pick() {
    # echo "  pick $@"
    local n=$1 p=$2 s=$3 i l r=1
    shift 3
    if [[ $n == 1 ]]; then
	: $((calls++))
	((calls % 10000)) || echo "   $calls"
	case " $* " in
	    *" $((target-s)) "*)
		echo $p $((target-s))
		product $p $((target-s))
		return 0 ;;
	esac
	return 1
    fi
    i=$(($#-n+1))
    while (( i-- )); do
	l=$1
	shift
	pick $((n-1)) "$l $p" $((s+l)) $* && r=0
    done
    return $r
}
# shave off early rounds
tail=1
while { sum ${size[*]: -$tail}; [[ $? == 2 ]]; }; do
    : $((tail++))
done
echo "last $tail items give sum of $sum, skipping to combinations of $tail"
# iterate until we find a winner
while ! pick $tail '' 0 ${size[*]}; do
    : $((tail++))
done
echo "made $calls calls, lowest quantum in group of $tail is $min"
