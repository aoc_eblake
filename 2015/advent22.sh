read _ _ boss_hit
read _ boss_damage
echo "input: $boss_hit $boss_damage"
exec 3>&1
debug() {
    [[ ! ${DEBUG+set} ]] && return
    echo "$*" >&3
}
spells=(missile drain shield poison recharge)
cost=(53 73 113 173 229)
minspent=99999
# battle spell hit mana spent shield poison recharge boss_hit
# return 0 (and sets $spent) for win, 1 for loss/not possible, 2 + output
# to continue, 3 to prune path as non-maximal
battle() {
    debug " battle $@" >&3
    if [[ $# != 8 ]]; then
	echo " invalid input" >&3
	return 1
    fi
    spent=$4
    if [[ $spent -gt $minspent ]]; then
	debug " not a minimal path, pruning rather than following"
	return 3
    fi
    local spell=$1 hit=$2 mana=$3 shield=$5 poison=$6 recharge=$7 boss=$8
    debug "player turn"
    debug " player has $hit hit points, $((7*!!shield)) armor, $mana mana"
    debug " boss has $boss hit points"
    if [[ ! $part1 && $((--hit)) == 0 ]]; then
	debug " hard mode, hit points ran out"
	return 1
    fi
    if [[ $shield -gt 0 ]]; then
	: $((shield--))
	debug " shield effect down to $shield"
    fi
    if [[ $shield -gt 0 && $spell == 2 ]]; then
	debug " shield effect still active"
	return 1
    fi
    if [[ $poison -gt 0 ]]; then
	boss=$((boss-3))
	: $((poison--))
	debug " poison effect down to $poison"
    fi
    if [[ $poison -gt 0 && $spell = 3 ]]; then
	debug " poison effect still active"
	return 1
    fi
    if [[ $recharge -gt 0 ]]; then
	mana=$((mana+101))
	: $((recharge--))
	debug " recharge effect down to $recharge, mana up to $mana"
    fi
    if [[ $recharge -gt 0 && $spell = 4 ]]; then
	debug " recharge effect still active"
	return 1
    fi
    if [[ ${cost[$spell]} -gt $mana ]]; then
	debug " not enough mana left"
	return 1
    fi
    debug " casting ${spells[$spell]}"
    mana=$((mana - cost[spell]))
    spent=$((spent + cost[spell]))
    case $spell in
	0) boss=$((boss-4)) ;;
	1) boss=$((boss-2)) hit=$((hit+2)) ;;
	2) shield=6 ;;
	3) poison=6 ;;
	4) recharge=5 ;;
    esac
    if [[ $boss -le 0 ]]; then
	debug " boss defeated"
	return 0
    fi

    debug "boss turn"
    debug " player has $hit hit points, $((7*!!shield)) armor, $mana mana"
    debug " boss has $boss hit points"
    if [[ $shield -gt 0 ]]; then
	: $((shield--))
	debug " shield effect down to $shield"
    fi
    if [[ $poison -gt 0 ]]; then
	boss=$((boss-3))
	: $((poison--))
	debug " poison effect down to $poison"
    fi
    if [[ $recharge -gt 0 ]]; then
	mana=$((mana+101))
	: $((recharge--))
	debug " recharge effect down to $recharge, mana up to $mana"
    fi
    if [[ $boss -le 0 ]]; then
	debug " boss defeated"
	return 0
    fi
    hit=$((hit - boss_damage + 7*!!shield))
    if [[ $hit -le 0 ]]; then
	debug " player defeated"
	return 1
    fi
    echo $hit $mana $spent $shield $poison $recharge $boss
    return 2
}
hit=${1-50} mana=${2-500}
echo $hit $mana 0 0 0 0 $boss_hit > tmp
gen=0
wins=0
scenarios=0
while [[ -s tmp ]]; do
    echo "computing generation $((++gen)) from $(wc -l < tmp) scenarios"
    while read line; do
	: $((scenarios++))
	for i in {0..4}; do
	    battle $i $line
	    if [[ $? == 0 ]]; then
		: $((wins++))
		(( spent < minspent )) && minspent=$spent
	    fi
	done
    done < tmp > tmp2
    sort -k4,4n -k8,8rn < tmp2 | uniq > tmp
done
rm tmp tmp2
echo "traced $scenarios possible sequences over $gen generations"
echo "computed $wins different win paths; cheapest cost $minspent mana"
