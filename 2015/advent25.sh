row=${1-2978} col=${2-3083}
start=$((col*(col+1)/2))
echo "code sequence $start at row 1 column $col"
cell=$((start+(col-1)*(row-1)+(row-1)*row/2))
echo "code sequence $cell at row $row column $col"
i=1
code=20151125
while [[ $i -lt $cell ]]; do
    : $((i++))
    (( i % 10000 )) || echo " $i"
    code=$((code * 252533 % 33554393))
done
echo "code at row $row column $col is $code"
