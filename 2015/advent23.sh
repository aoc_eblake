count=0
while read line; do
    inst[$((count++))]=$line
done
echo "read $count instructions, starting execution"
a=0 b=0 pc=0 count=0
[[ ! $part1 ]] && a=1
while [[ ${inst[pc]+set} ]]; do
    : $((count++))
    read i r o <<< "${inst[pc]}"
    case $i in
	hlf) case $r in a) a=$((a/2));; b) b=$((b/2));; *) exit 2;; esac
	     pc=$((pc+1)) ;;
	tpl) case $r in a) a=$((a*3));; b) b=$((b*3));; *) exit 2;; esac
	     pc=$((pc+1)) ;;
	inc) case $r in a) a=$((a+1));; b) b=$((b+1));; *) exit 2;; esac
	     pc=$((pc+1)) ;;
	jmp) pc=$((pc+r)) ;;
	jie) case $r in a,) ((a%2));; b,) ((b%2));; *) exit 2;; esac
	     pc=$((pc+($? ? o : 1))) ;;
	jio) case $r in a,) ((a==1));; b,) ((b==1));; *) exit 2;; esac
	     pc=$((pc+($? ? 1 : o))) ;;
	*) echo "invalid instruction $i, exiting"; exit 1 ;;
    esac
done
echo "final register state a=$a b=$b, after $count instructions executed"
