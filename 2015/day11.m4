divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dstart=XXX] [-Dfile=day11.input] day11.m4

include(`common.m4')ifelse(common(11), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

ifdef(`start', `', `define(`start', translit(include(defn(`file')), nl))')

# Verify the first four letters fail all rules, the fourth letter is x,
# and the fifth letter < x.  Then the answer is trivial.
define(`prep', `define(eval(9 + $1, 36)`_', incr($1))')
forloop_arg(1, 26, `prep')
define(`h_', i_)define(`k_', l_)define(`n_', o_)
define(`fail', `errprintn(`input in unexpected form')m4exit(1)')
define(`check', `ifelse(`$1', translit(`$1', `ilo'), `_$0(substr(`$1', 0, 1),
  substr(`$1', 1, 1), substr(`$1', 2, 1), substr(`$1', 3, 1), substr(`$1', 4,
  1))', `fail')')
define(`_check', `ifelse(`$1', `$2', `fail', `$2', `$3', `fail', `$3', `$4',
  `fail', eval($2_ + 1 == $3_), 1, `fail', eval($5_ >= $4_), 1, `fail',
  `$4', x, `define(`part1', `$1$2$3xxyzz')define(`part2',
  `$1$2'eval(9 + $3_, 36)`aabcc')', `fail')')
check(start)

divert`'part1
part2
