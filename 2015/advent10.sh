val=${1-1} rounds=${2-5}
echo "running $rounds iterations on initial input $val"
exec 3>&1
# visit rounds val
visit() {
    echo "starting visit $@" >&3
    if [[ $1 == 0 ]]; then
	echo $val | sed 's/\(.\)/\1\n/g'
	return
    fi
    exec < <(visit $(($1 - 1)) $val)
    read prev
    lines=1
    index=1
    count=1
    summary="($prev"
    while read next; do
	[[ ! $next ]] && continue
	lines=$((lines + 1))
	if [[ $lines -lt 70 ]]; then
	    summary+="$next"
	elif [[ $lines == 70 ]]; then
	    summary+=...
	fi
	if [[ $next == $prev ]]; then
	    count=$((count+1))
	else
	    echo $count
	    echo $prev
	    prev=$next count=1
	fi
    done
    echo $count
    echo $prev
    echo "$summary)" >&3
    echo "visit $1 parsed $lines bytes" >&3
}
echo "final length $(visit $rounds $val | wc -l)"
