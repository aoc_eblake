divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day9.input] day9.m4

include(`common.m4')ifelse(common(9), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`input', translit(include(defn(`file')), nl, `;'))
define(`part1', 0)define(`part2', 0)define(`n', 0)
define(`lookup', `ifdef(`$1_', `', `define(`n', incr(n))define(`$1_',
  n)')$1_()')
define(`line', `_$0(translit(`$1', ` ', `,'))')
define(`_line', `define(`part1', eval(part1 + $5))define(
  `d'lookup(`$1')`_'lookup(`$3'), $5)')
ifdef(`__gnu__', `
  patsubst(defn(`input'), `\([^;]*\);', `line(`\1')')
',`
  define(`chew', `line(substr(`$1', 0, index(`$1', `;')))define(`tail',
    substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(
    `tail'), `;'), -1, `', `$0(defn(`tail'))')')
  define(`split', `ifelse(eval($1 < 65), 1, `chew(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  split(len(defn(`input')), defn(`input'))
')

# Lifted from Sawada-Williams' sigma-tau algorithm, mentioned at
# https://en.wikipedia.org/wiki/Permutation#Generation_with_minimal_changes
# Hard-coded to 8 here for less expansion of n, but could easily be extended
ifelse(n, 8, `', `errprintn(`unexpected input length')m4exit(1)')
define(`swap', `ifelse($1, 'n`, `define(`next', $3)', $2, 'n`,
  `define(`next', $1)', next, $1, `define(`next', $2)')$2,$1,shift(shift($@))')
define(`rot', `shift($@),$1')
define(`fact', `ifelse($1, 1, 1, `eval($1 * fact(decr($1)))')')
define(`base', quote(shift(forloop_rev(n, 1, `,'))))
define(`check', `_$0(ifelse($1, 'n`, $3, next), $2)')
define(`_check', `eval($2 == $1 * ($1 < 'decr(n)`) + 1)')
define(`permute', `_$0(fact(n), (swap(base)))')
define(`_permute', `ifelse($1, 0, `', `try$2$0(decr($1), (ifelse(`$2',
  ''dquote(dquote((base)))``, `rot', check$2, 1, `swap', `rot')$2))')')

define(`d', `defn(`d$1_$2')defn(`d$2_$1')')
define(`try', `_$0(eval(d($1, $2) + d($2, $3) + d($3, $4) + d($4, $5) +
  d($5, $6) + d($6, $7) + d($7, $8)))')
define(`_try', `ifelse(eval($1 < part1), 1, `define(`part1', $1)')ifelse(
  eval($1 > part2), 1, `define(`part2', $1)')')
permute(n)

divert`'part1
part2
