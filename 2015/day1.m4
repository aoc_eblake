divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day1.input] day1.m4

include(`common.m4')ifelse(common(1), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

# The input file contains bare unbalanced ( and ), which is a pain in
# m4.  Tackle it by reading twice and exploiting that the data starts
# with ( but eventually balances out to go to a negative floor.  On
# the first read, our define() will terminate early and the remaining
# input is discarded because we are diverting to -1.  On the second
# read, we redefine comments to begin with the start of input and end
# with a newline (the file lacks one, so just one that we supply), so
# that the input is a single comment that we can then post-process
# into saner contents.
define(`input', include(defn(`file')))
define(`part2', incr(len(defn(`input'))))
changecom(defn(`input'), `
')
define(`input', include(defn(`file'))
)
changecom(`#')
define(`input', translit(dquote(defn(`input')), `()
', `<>'))

define(`part1', eval(len(translit(input, >)) - len(translit(input, <))))

divert`'part1
part2
