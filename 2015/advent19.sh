final=false
count=0
declare -A elements
longest=0
while read line; do
    if $final; then
	target=$line
    elif [[ ! $line ]]; then
	final=:
    else
	regex='([A-Z][a-z]*) => +([A-Za-z ]*)'
	[[ $line =~ $regex ]] || { echo "failed to parse"; exit 1; }
	elements[${BASH_REMATCH[1]}]=${BASH_REMATCH[1]}
	set ${BASH_REMATCH[2]}
	[[ $longest -gt $# ]] || longest=$#
	eval "${BASH_REMATCH[1]}+=('"$*"')"
	: $((count++))
    fi
    # cheat: after pre-reading the input, all elements start with capital
    # except 'e', but 'e' does not occur in target or any substitution.
    # Break input into per-element chunks
done < <(sed 's/e/E/g; s/\([A-Z]\)/ \1/g')
echo "read $count rules for ${elements[*]}, longest expansion $longest"
exec 3>&1
debug() {
    [[ ! ${DEBUG+set} ]] && return
    echo "$*" >&3
}
# contract n list... # output all contractions of the nth element of list
contract() {
    debug "  contract: $@"
    local n=$1
    shift
    prefix=${@:1:$n}
    debug " prefix: $prefix"
    shift $n
    local el i tail=$*
    for el in ${elements[*]}; do
	[[ $el == E && ( $prefix || $# -gt 2) ]] && continue # trivial prune
	declare -n arr=$el
	for i in ${!arr[*]}; do
	    case $tail" " in
		${arr[i]}" "*)
		    debug " matched $el[$i] (${arr[i]})"
		    echo $prefix $el ${tail#${arr[i]}}
	    esac
	done
    done
}
list=$target
gen=0
nl=$'\n'
while case "$nl$list$nl" in *"${nl}E$nl"*) false;; *) : ;; esac; do
    echo " generation $gen, list has $(echo "$list" | wc -l) lines"
    while read start; do
	set $start
	i=0
	while [[ $i -lt $# && $i -lt $longest ]]; do
	    contract $i $start
	    : $((i++))
	done
    done <<< "$list" > tmp
    list=$(sed '/^ /d' < tmp | sort -u)
    debug "list is:"$'\n'"$list"
    : $((gen++))
done
rm tmp
echo "found target in $gen steps"
