limit=${1-2503}
count=0
while read speed duration rest; do
    echo "using speed $speed, fly duration $duration, rest duration $rest"
    s[$count]=$speed d[$count]=$duration r[$count]=$rest points[$count]=0
    : $((count++))
done < <(sed 's/.*fly //; s,km/s for ,,; s/seconds.*for//; s/ seconds.$//')
echo "parsed details on $count racers"

# race speed duration rest time # reports distance in $dist for racer at time
race() {
    local speed=$1 duration=$2 rest=$3 limit=$4
    dist=$((speed*duration * (limit/(duration+rest))))
    tail=$((limit%(duration+rest)))
    if [[ $tail -gt $duration ]]; then
	dist=$((dist+speed*duration))
    else
	dist=$((dist+speed*tail))
    fi
}
# compute time # sets max to max distance, and increments points[$n] of winners
compute() {
    max=0
    local i=0
    while [[ $i -lt ${#s[*]} ]]; do
	race ${s[i]} ${d[i]} ${r[i]} $1
	# echo "distance $dist"
	[[ $dist -gt $max ]] && max=$dist
	: $((i++))
    done
    i=0
    while [[ $i -lt ${#s[*]} ]]; do
	race ${s[i]} ${d[i]} ${r[i]} $1
	[[ $dist == $max ]] && : $((points[i]++))
	: $((i++))
    done
}
i=0
while [[ $i -lt $limit ]]; do
    echo "racing at second $i"
    compute $((++i))
done
echo "furthest racer at end: $max"
echo ${points[*]}
max=0 i=0
while [[ $i -lt ${#s[*]} ]]; do
    [[ $max -gt ${points[i]} ]] || max=${points[i]}
    : $((i++))
done
echo "max points: $max"
