divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dseed=NNN] [-Dfile=day10.input] day10.m4
# Optionally use -Dverbose=1 to see some progress
# Optionally use -Dalgo=full|sparse to choose an algorithm

include(`common.m4')ifelse(common(10, 131101), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

ifdef(`seed', `', `define(`seed', translit(include(defn(`file')), nl))')
ifdef(`algo', `', `define(`algo', `sparse')')

# Using http://www.se16.info/js/lands2.htm
define(`table', `
`1, H, `H', 22',
`2, He, `Hf,Pa,H,Ca,Li', 13112221133211322112211213322112',
`3, Li,	`He', 312211322212221121123222112',
`4, Be, `Ge,Ca,Li', 111312211312113221133211322112211213322112',
`5, B, `Be', 1321132122211322212221121123222112',
`6, C, `B', 3113112211322112211213322112',
`7, N, `C', 111312212221121123222112',
`8, O, `N', 132112211213322112',
`9, F, `O', 31121123222112',
`10, Ne, `F', 111213322112',
`11, Na, `Ne', 123222112',
`12, Mg, `Pm,Na', 3113322112',
`13, Al, `Mg', 1113222112',
`14, Si, `Al', 1322112',
`15, P, `Ho,Si', 311311222112',
`16, S, `P', 1113122112',
`17, Cl, `S', 132112',
`18, Ar, `Cl', 3112',
`19, K, `Ar', 1112',
`20, Ca, `K', 12',
`21, Sc, `Ho,Pa,H,Ca,Co', 3113112221133112',
`22, Ti, `Sc', 11131221131112',
`23, V, `Ti', 13211312',
`24, Cr, `V', 31132',
`25, Mn, `Cr,Si', 111311222112',
`26, Fe, `Mn', 13122112',
`27, Co, `Fe', 32112',
`28, Ni, `Zn,Co', 11133112',
`29, Cu, `Ni', 131112',
`30, Zn, `Cu', 312',
`31, Ga, `Eu,Ca,Ac,H,Ca,Zn', 13221133122211332',
`32, Ge, `Ho,Ga', 31131122211311122113222',
`33, As, `Ge,Na', 11131221131211322113322112',
`34, Se, `As', 13211321222113222112',
`35, Br, `Se', 3113112211322112',
`36, Kr, `Br', 11131221222112',
`37, Rb, `Kr', 1321122112',
`38, Sr, `Rb', 3112112',
`39, Y, `Sr,U', 1112133',
`40, Zr, `Y,H,Ca,Tc', 12322211331222113112211',
`41, Nb, `Er,Zr', 1113122113322113111221131221',
`42, Mo, `Nb', 13211322211312113211',
`43, Tc, `Mo', 311322113212221',
`44, Ru, `Eu,Ca,Tc', 132211331222113112211',
`45, Rh, `Ho,Ru', 311311222113111221131221',
`46, Pd, `Rh', 111312211312113211',
`47, Ag, `Pd', 132113212221',
`48, Cd, `Ag', 3113112211',
`49, In, `Cd', 11131221',
`50, Sn, `In', 13211',
`51, Sb, `Pm,Sn', 3112221',
`52, Te, `Eu,Ca,Sb', 1322113312211',
`53, I, `Ho,Te', 311311222113111221',
`54, Xe, `I', 11131221131211',
`55, Cs, `Xe', 13211321',
`56, Ba, `Cs', 311311',
`57, La, `Ba', 11131',
`58, Ce, `La,H,Ca,Co', 1321133112',
`59, Pr, `Ce', 31131112',
`60, Nd, `Pr', 111312',
`61, Pm, `Nd', 132',
`62, Sm, `Pm,Ca,Zn', 311332',
`63, Eu, `Sm', 1113222',
`64, Gd, `Eu,Ca,Co', 13221133112',
`65, Tb, `Ho,Gd', 3113112221131112',
`66, Dy, `Tb', 111312211312',
`67, Ho, `Dy', 1321132',
`68, Er, `Ho,Pm', 311311222',
`69, Tm, `Er,Ca,Co', 11131221133112',
`70, Yb, `Tm', 1321131112',
`71, Lu, `Yb', 311312',
`72, Hf, `Lu', 11132',
`73, Ta, `Hf,Pa,H,Ca,W', 13112221133211322112211213322113',
`74, W, `Ta', 312211322212221121123222113',
`75, Re, `Ge,Ca,W', 111312211312113221133211322112211213322113',
`76, Os, `Re', 1321132122211322212221121123222113',
`77, Ir, `Os', 3113112211322112211213322113',
`78, Pt, `Ir', 111312212221121123222113',
`79, Au, `Pt', 132112211213322113',
`80, Hg, `Au', 31121123222113',
`81, Tl, `Hg', 111213322113',
`82, Pb, `Tl', 123222113',
`83, Bi, `Pm,Pb', 3113322113',
`84, Po, `Bi', 1113222113',
`85, At, `Po', 1322113',
`86, Rn, `Ho,At', 311311222113',
`87, Fr, `Rn', 1113122113',
`88, Ra, `Fr', 132113',
`89, Ac, `Ra', 3113',
`90, Th, `Ac', 1113',
`91, Pa, `Th', 13',
`92, U, `Pa', 3'')
define(`prep', `_$0($1)')
define(`_prep', `define(`e$1', `$4')define(`l$1', len(`$4'))define(`$2',
  $1)ifelse('seed`, `$4', `define(`init', $1)')')
foreach(`prep', table)
ifdef(`init', `',
  `errprintn(`unsure how to break 'seed` into elements')m4exit(1)')

ifelse(algo, `full', `
output(1, `Using full matrix multiply, O(log n)')
define(`_prep', `_foreach(`setup($1,', `)', `', $3)')
define(`setup', `define(`m1_$1_$2', incr(0defn(`m1_$1_$2')))')
foreach(`prep', table)
define(`_prep', `ifdef(`m1_$1_$2', `', `define(`m1_$1_$2', 0)')')
define(`prep', `forloop(1, 92, `_$0($1,', `)')')
forloop_arg(1, 92, `prep')
define(`_dot', `+m$1_$3_$5*m$2_$5_$4')
define(`dot', `define(`m$3_$4_$5', eval(forloop(1, 92, `_$0($1, $2, $4, $5, ',
  `)')))')
define(`_mult', `forloop(1, 92, `dot($1, $2, $3, $4, ', `)')')
define(`mult', `output(1, $3)forloop(1, 92, `_$0($1, $2, $3, ', `)')')
mult(1, 1, 2)
mult(2, 2, 4)
mult(1, 4, 5)
mult(5, 5, 10)
mult(10, 10, 20)
mult(20, 20, 40)
define(`prod', `+m$1_$2_$3*l$3')
define(`part1', eval(forloop(1, 92, `prod(40, 'init`, ', `)')))
mult(10, 40, 50)
define(`part2', eval(forloop(1, 92, `prod(50, 'init`, ', `)')))

', algo, `sparse', `
output(1, `Using sparse matrix lookup, O(n)')
define(`_prep', `define(`L$1', quote($3))')
foreach(`prep', table)
pushdef(`q0', init)define(`c0_'init, 1)
define(`iter', `_$0(decr($1), $1)')
define(`_iter', `ifdef(`q$1', `bump(q$1, $1, $2)popdef(`q$1')$0($@)')')
define(`bump', `_foreach(`_$0(c$2_$1, $3, ', `)', `', L$1)popdef(`c$2_$1')')
define(`_bump', `ifdef(`c$2_$3', `define(`c$2_$3', eval(c$2_$3 + $1))',
  `pushdef(`q$2', $3)define(`c$2_$3', $1)')')
define(`_tally', `+c$1_$2*l$2')
define(`tally', `eval(0_stack_foreach(`q$1', `_$0($1, ', `)', `t'))')
forloop_arg(1, 40, `iter')
define(`part1', tally(40))
forloop_arg(41, 50, `iter')
define(`part2', tally(50))

', `errprintn(`unrecognized algorithm 'algo)m4exit(1)')

divert`'part1
part2
