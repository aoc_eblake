divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day8.input] day8.m4

include(`common.m4')ifelse(common(8), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

# Gag: some input files happen to contain macro names
pushdef(`nl', `ifelse(`$#', 0, ``$0'', `''nl`)')
define(`list', translit(include(defn(`file')), nl(), `;'))
define(`part1', len(defn(`list')))
define(`part2', eval(2 * part1 - len(translit(defn(`list'), `;')) -
  len(translit(defn(`list'), `\";'))))
define(`_visit', `popdef(`$0')ifelse(`$1', `x', `pushdef(`$0',
  `popdef(`$0')')pushdef(`$0', `popdef(`$0')')')define(`part1', decr(part1))')
define(`visit', `ifelse(`$1', `\', `pushdef(`$0', defn(`_$0'))', `$1', `"',
  `', `define(`part1', decr(part1))')')
ifdef(`__gnu__', `
  patsubst(defn(`list'), `\(.\)', `visit(`\1')')
',`
  define(`split', `ifelse($1, 1, `visit(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  split(len(defn(`list')), defn(`list'))
')

divert`'part1
part2
