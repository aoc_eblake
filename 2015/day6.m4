divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day6.input] day6.m4
# Optionally use -Dverbose=1 to see some progress
# Optionally use -Dalgo=list|btree to choose the list implementation

include(`common.m4')ifelse(common(6, 65537), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`turn')define(`through')
pushdef(`inst', `0,999,`R',0,999')
define(`On', `pushdef(`inst', `$3,$6,`S',$2,$5')')
define(`Off', `pushdef(`inst', `$3,$6,`C',$2,$5')')
define(`Tog', `pushdef(`inst', `$3,$6,`T',$2,$5')')
translit((include(defn(`file'))), ` 'nl, `,)'define(
  `on', `On(')define(`off', `Off(')define(`toggle', `Tog(')))
define(`F', `$1')
define(`D', defn(`define'))define(`I', defn(`ifelse'))
define(`P', defn(`incr'))define(`M', defn(`decr'))
define(`E', defn(`eval'))define(`N', defn(`defn'))

ifelse(ifdef(`algo', `algo', `btree'), `btree', `

output(1, `using 2-3 B-tree to track row')
define(`n', 0)dnl counter for naming nodes
dnl root: head of 2-3 range tree subdividing 0-999, nNt: type,
dnl nNa: left range lo, nNA: left range hi, nNv: left value
dnl nNb: right range lo, nNB: right range hi, nNV: right value
dnl nNl: left child nNc: center child, nNr: right child, nNp: parent
define(`type', `N(`n$1t')')dnl type: Leaf: l1, l2, Internal: 2, 3
define(`partype', `ifdef(`n$1p', `type(n$1p)', ``r'')')
define(`find', `_$0($1, root)')dnl find(lo, node) output node and slot
define(`_find', `F(`$0'type($2))($1, $2)')
define(`_findl1', `$2,`a',`A',`v'')
define(`_findl2', `I(E($1 < n$2b), 1, `$2,`a',`A',`v'',
  `$2,`b',`B',`V'')')
define(`_find2', `I(E($1 < n$2a), 1, `_find($1, n$2l)',
  E($1 > n$2A), 1, `_find($1, n$2r)', `$2,`a',`A',`v'')')
define(`_find3', `I(E($1 < n$2a), 1, `_find($1, n$2l)',
  E($1 > n$2A && $1 < n$2b), 1, `_find($1, n$2c)', E($1 > n$2B), 1,
  `_find($1, n$2r)', E($1 <= n$2A), 1, `$2,`a',`A',`v'', `$2,`b',`B',`V'')')
dnl setX(node, lo, hi, value) prep left or right of node
define(`seta', `D(`n$1a', $2)D(`n$1A', $3)D(`n$1v', `$4')')
define(`setb', `D(`n$1b', $2)D(`n$1B', $3)D(`n$1V', `$4')')
dnl new(lo, hi, value, l, r) create new 2-node, return its number
define(`new', `_$0(n, $@)')
define(`_new', `seta($1, $2, $3, `$4')I(`$5$6', `', `D(`n$1t',
  `l1')', `D(`n$1t', 2)D(`n$1l', $5)D(`n$5p', $1)D(`n$1r',
  $6)D(`n$6p', $1)')popdef(`n$1p')D(`n', P($1))$1')
dnl init() create new tree, assigned to root
define(`init', `D(`root', new(0, 999, `0'))')
dnl next(node, Slot) returns node, slot of next range, undefined at end
define(`next', `ifdef(`n$1p', `F(`_$0'type($1)`$2')($1, n$1p,
  P(n$1$2))', `_find(P(n$1$2), $1)')')
define(`_nextl1A', `I(n$2l, $1, `$2,`a',`A',`v'', n$2t.n$2c, 3.$1,
  `$2,`b',`B',`V'', `$0($2, n$2p, $3)')')
define(`_nextl2A', `$1,`b',`B',`V'')
define(`_nextl2B', N(`_nextl1A'))
define(`_next2A', `_find($3, n$1r)')
define(`_next3A', `_find($3, n$1c)')
define(`_next3B', `_find($3, n$1r)')
dnl merge(op, lo, hi) split/redefine nodes as needed to insert new subrange
define(`merge', `_insert($2, $3, `$1', find($2))')
define(`_insert', `I(n$4$5, $1, `I(E(n$4$6 < $2), 1,
  `F(`$0'type($4)`$5')($1, n$4$6, `$3', $4)$0(P(n$4$6), $2, `$3',
  next($4, `$6'))', `F(`$0'type($4)`$5')($1, $2, `$3', $4)')', `F(
  `split'type($4)`$5')($1, $4)merge(`$3', $1, $2)')')
define(`_insertl1a', `I($2, n$4A, `', `splitl1a(P($2),
  $4)')D(`n$4v', $3($1, $2, n$4v))')
define(`_insertl2a', `I($2, n$4A, `', `splitl2a(P($2),
  $4)')D(`n$4v', $3($1, $2, n$4v))')
define(`_insertl2b', `I($2, n$4B, `D(`n$4V', $3($1, $2, n$4V))',
  `_splitl2b(P($2), $4, N(`n$4V')D(`n$4V', $3($1, $2, n$4V)))')')
define(`_insert2a', `I($2, n$4A, `D(`n$4v', $3($1, $2, n$4v))',
  `D(`n$4a', P($2))F(`add'type(n$4l))(n$4l, $1, $2, $3($1, $2,
  n$4v))')')
define(`_insert3a', N(`_insert2a'))
define(`_insert3b', `I($2, n$4B, `D(`n$4V', $3($1, $2, n$4V))',
  `D(`n$4b', P($2))F(`add'type(n$4c))(n$4c, $1, $2, $3($1, $2,
  n$4V))')')
dnl split(mid, node) split a range, and shuffle nodes as needed
define(`splitl1a', `D(`n$2t', `l2')setb($2, $1, n$2A,
  N(`n$2v'))D(`n$2A', M($1))')
define(`splitl2a', `F(`promote'partype($2))(N(`n$2p'), $1,
  n$2A`'D(`n$2A', M($1)), N(`n$2v'), $2, new(n$2b, n$2B,
  N(`n$2V')))D(`n$2t', `l1')')
define(`splitl2b', `_$0($1, $2, N(`n$2V'))')
define(`_splitl2b', `F(`promote'partype($2))(N(`n$2p'), n$2b,
  M($1), N(`n$2V'), $2, new($1, n$2B, `$3'))D(`n$2t', `l1')')
define(`split2a', `F(`add'type(n$2l))(n$2l, n$2a`'D(`n$2a', $1),
  M($1), N(`n$2v'))')
define(`split3a', N(`split2a'))
define(`split3b', `F(`add'type(n$2c))(n$2c, n$2b`'D(`n$2b', $1),
  M($1), N(`n$2V'))')
dnl addX(node, lo, hi, value) add range into child node
define(`addl1', `D(`n$1t', `l2')setb($1, $2, $3, `$4')')
define(`addl2', `_$0($1, new($2, $3, `$4'))')
define(`_addl2', `F(`promote'partype($1))(N(`n$1p'), n$1b,
  n$1B, N(`n$1V'), $1, $2)D(`n$1t', `l1')')
define(`add2', `F(`add'type(n$1r))(n$1r, $2, $3, `$4')')
define(`add3', N(`add2'))
dnl promoteX(node, lo, hi, value, child, new) tie just-split child into node
define(`promoter', `D(`root', new($2, $3, `$4', $5, $6))')
define(`promote2', `I($5, n$1l, `setb($1, n$1a, n$1A, N(`n$1v'))seta(
  $1, $2, $3, `$4')D(`n$1c', $6)', `setb($1, $2, $3, `$4')D(`n$1c',
  n$1r)D(`n$1r', $6)')D(`n$6p', $1)D(`n$1t', 3)')
define(`promote3', `_$0($1, I($5, n$1l, `n$1a, n$1A, N(`n$1v'),
  new(n$1b, n$1B, N(`n$1V'), n$1c, n$1r), $6D(`n$1a', $2)D(`n$1A',
  $3)D(`n$1v', `$4')D(`n$6p', $1)', $5, n$1c, `$2, $3, `$4',
  new(n$1b, n$1B, N(`n$1V'), $6, n$1r), n$1c', `n$1b, n$1B, N(`n$1V'),
  new($2, $3, `$4', n$1r, $6), n$1c'))')
define(`_promote3', `F(`promote'partype($1))(N(`n$1p'), $2, $3, `$4',
  $1, $5)D(`n$1t', 2)D(`n$1r', $6)')

define(`R', `define(`n', base)init()')
define(`use', `$3')
define(`prep', `define(`base', n)define(`rows', root)')
define(`walk', `_$0(`$1', _find(0, I(`$2', `rows', `rows', `root')))')
define(`_walk', `$1(n$2$3, n$2$4, n$2$5)I(n$2$4, 999, `', `$0(`$1',
  next($2, `$4'))')')

', defn(`algo'), `list', `

output(1, `using m4 stack as list to track row')
pushdef(`r', `0,999,0')
define(`R', `undefine(`r')pushdef(`r', `0,999,0')')
define(`merge', `D(`todo', `$2,$3')_stack_foreach(`r', `_$0(`$1',', `)',
  `t')')
define(`_merge', `ifdef(`todo', `handle(`$1', todo, $2)')')
define(`handle', `I($2, $4, `I(E($3 < $5), 1, `$1($2, $3,
  $6)D(`t', P($3)`,$5,$6')pushdef(`t')popdef(`todo')', $3, $5,
  `$1($2, $3, $6)popdef(`todo')', `$1($2, $5, $6)D(`todo',
  P($5)`,$3')')', E($2 <= $5), 1, `use($4, M($2), $6)D(`t',
  `$2,$5,$6')pushdef(`t')')')
define(`use', `D(`r', `$1,$2,$3')')
define(`prep', `stack_reverse(`r', `rows')')
define(`walk', `_stack_foreach(I(`$2', `rows', ``rows'', ``r''),
  `$1(F(', `))', `tmp$2')')

', `
errprintn(`unrecognized -Dalgo= value')m4exit(1)
')

define(`set', `use($1, $2, I(index($3, `-'), 0, `M($3)', `-P($3)'))')
define(`clr', `use($1, $2, I($3, 0, 0, `M(substr($3,
  P(index($3, `-'))))'))')
define(`tgl', `use($1, $2, I(index($3, `-'), 0,
  `P(P(substr($3, 1)))', `-P(P($3))')))')
define(`S', `merge(`set', $1, $2)')
define(`C', `merge(`clr', $1, $2)')
define(`T', `merge(`tgl', $1, $2)')
define(`part1', 0)define(`part2', 0)define(`cnt', 0)
define(`gatherX', `+($2-$1+1)*($3<0)')
define(`gatherY', `+($2-$1+1)*($3*(1-2*($3<0)))')
define(`do', `_$0($1)D(`part1', E(part1 + (walk(`gatherX'))` * ($2-$1+1)'))D(
  `part2', E(part2 + (walk(`gatherY'))` * ($2-$1+1)'))I(E(cnt % 10), 0,
  `output(1, `...$1')')D(`cnt', P(cnt))')
define(`_do', `ifdef(`row$1', `row$1()popdef(`row$1')$0($@)')')

init()
define(`divvy', `ifdef(`inst', `_$0(inst)popdef(`inst')$0()')')
define(`_divvy', `ifelse(`$3', `R', `', `merge(`use', $1, $2)')forloop($1, $2,
  `pushdef(`row'', `, `$3($4,$5)')')')
divvy()
prep()
walk(`do', `rows')

divert`'part1
part2
