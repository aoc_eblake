divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dsum=N] [-Dfile=day17.input] [-Dhashsize=H] day17.m4
# Optionally use -Dverbose=1 to see some progress

include(`common.m4')ifelse(common(17, 65537), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

ifdef(`sum', `', `define(`sum', 150)')
define(`part1', 0)
define(`sort', `ifelse(`$2', `', ``,$1'', `ifelse(eval($1 < $2), 1,
  ``,$@'', ``,$2'$0(`$1', shift(shift($@)))')')')
define(`prep', `ifelse(`$2', `', `store(0$1)', `$0(sort($2$1),
  shift(shift($@)))')')
define(`store', `ifelse(`$2', `', `define(`n', $1)', `define(`c$1',
  $2)$0(incr($1), shift(shift($@)))')')
prep(`', translit(include(defn(`file')), nl, `,'))
define(`cnt', 0)
define(`bump', `ifelse(eval(cnt % 100), 0, `output(1, ...cnt)')define(`cnt',
  incr(cnt))')

define(`body1', `_$0(eval($1 + c$2), $2)')
define(`_body1', `ifelse(eval(0 < $1 - sum), 0, `ifdef(`l$1', `define(
  `l$1', defn(`l$1')`,$2')', `define(`l$1', $2)pushdef(`l', $1)')')')
define(`body0', `_foreach(`body1(', `, $1)',
  _stack_foreach(`l', `,', `', `t'))')
define(`recur', `ifdef(`r$1_$2', `', `define(`r$1_$2', _foreach(`_$0(',
  `, $@)', `', l$1))bump()')r$1_$2`'')
define(`_recur', `ifelse($1, -1, ``, `''', eval($3 <= $1), 0,
  `_foreach(`body2(c$1, ', `)', recur(eval($2 - c$1), $1))')')
define(`body2', ``, `$2,$1''')

define(`part1', 0)define(`best', n)
define(`check', `define(`part1', incr(part1))ifelse(eval(`$# < 'best), 1,
  `define(`best', `$#')define(`part2', 1)', `$#', best, `define(`part2',
  incr(part2))')')
output(1, `prepping to find groups adding to 'sum)
define(`l0', -1)pushdef(`l', 0)
forloop_arg(0, decr(n), `body0')
output(1, `collecting groups')
_foreach(`check(first(', `))', recur(sum, n))

divert`'part1
part2
