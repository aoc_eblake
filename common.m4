divert(-1)dnl -*- m4 -*-
# Not intended for standalone usage. Instead, include it from dayXX.m4 as:
# include(`common.m4')ifelse(common(1), `ok', `',
# `errprint(`Missing common initialization
# ')m4exit(1)')

# Borrow from m4's examples/forloop3.m4
define(`forloop_arg', `ifelse(eval(`($1) <= ($2)'), `1',
  `_forloop(eval(`$1'), `incr', eval(`$2'), `$3(', `)')')')
define(`forloop_var', `ifelse(eval(`($2) <= ($3)'), `1',
  `pushdef(`$1')_forloop(eval(`$2'), `incr', eval(`$3'),
    `define(`$1',', `)$4')popdef(`$1')')')
define(`forloop_rev', `ifelse(eval(`($1) >= ($2)'), `1',
  `_forloop(eval(`$1'), `decr', eval(`$2'), `$3', `$4')')')
define(`forloop', `_$0($1, `incr', $2, `$3', `$4')')
define(`_forloop', `$4`$1'$5`'ifelse(`$1', `$3', `',
  `$0($2(`$1'), `$2', $3, `$4', `$5')')')

# Idiomatic POSIX m4 uses tail recursion via shift($@), but it costs quadratic
# scanning time in GNU m4 1.4 as $@ gets longer. If we detect GNU m4, we
# can rely on $10 expanding to the tenth argument (instead of $1 concatenated
# with a literal 0), and exploit that for faster list processing.
# See https://git.sv.gnu.org/cgit/autoconf.git/tree/lib/m4sugar/foreach.m4
ifdef(`__gnu__',
  `define(`_foreach', `ifelse(`$#', 3, `', `pushdef(`t', `popdef(
    `t')'_forloop(4, `incr', `$#', `$0_(1, 2,',
    `)'))t($@)')')define(`_foreach_', ``$$1`$$3'$$2`''')',
  `define(`_foreach', `ifelse(`$#', 3, `', `$1`$4'$2`'$0(`$1', `$2',
    shift(shift(shift($@))))')')')
define(`foreach', `_$0(`$1(', `)', $@)')

# Borrow from Autoconf's m4sugar.m4
define(`stack_reverse', `ifdef(`$1', `pushdef(`$2',
  defn(`$1'))$3`'popdef(`$1')$0($@)')')
define(`stack_foreach', `_$0(`$1', `$2(', `)', `t')')
define(`_stack_foreach', `stack_reverse(`$1', `$4')stack_reverse(`$4', `$1',
  `$2`'defn(`$4')$3')')

# Other frequently used macros
define(`first', `$1')
define(`alpha', `abcdefghijklmnopqrstuvwxyz')
define(`ALPHA', `ABCDEFGHIJKLMNOPQRSTUVWXYZ')
define(`nl', `
')
define(`errprintn', `errprint(`$1'nl())')
define(`fatal', `errprintn(`$1')m4exit(1)')
define(`exists', `syscmd(`test -f "$1"')ifelse(sysval, 0, `$2', `$3')')
define(`_include', defn(`include'))
pushdef(`include', `exists(`$1', `',
  `fatal(`Could not locate file $1')')_$0(`$1')')
ifdef(`verbose', `ifelse(defn(`verbose'), `', `define(`verbose', 1)')',
  `define(`verbose', 0)')
define(`output', `ifelse(eval(verbose >= $1), 1, `errprintn(`$2')')')

define(`quote', `ifelse($#, `0', `', ``$*'')')
define(`dquote', ``$@'')

# There are some puzzles where 'm4 -H...' makes a difference in runtime;
# if $2 is non-empty, attempt to re-exec if the command line learned
# from /proc/self is not at least that large.  Not robust to command
# lines with ' or \n embedded, so use with caution.
define(`ensurehash', `define(`cmdline', dquote(patsubst(
  esyscmd(`tr "\\0" "\\n" < /proc/$PPID/cmdline | sed s/^/#/'),
  `#\(.*\)', ``\1',')))regexp(defn(`cmdline'),
  ``\(-H\|--ha[a-z=]*\)\(.,.\)?\([0-9]*\)'',
  `define(`foundhash', `\3')')ifdef(`foundhash', `', `define(`foundhash',
  509)')ifelse(eval($1 <= foundhash), 1, `', ifdef(`hashsize', 1), 1,
  `output(1, `-Dhashsize='hashsize` detected, not reexecing')',
  index(defn(`cmdline'), `,`-','), -1, `reexec($1, cmdline)',
  `errprintn(`refusing to reexec interactive run for hashsize $1')')')
define(`reexec', `output(1, `attempting reexec for larger hashsize $1')syscmd(
  `exec $2 -H$1 -Dhashsize=$1'_$0(shift(shift($@))))m4exit(sysval)')
define(`_reexec', `ifelse(`$1', `-H', `$0(shift(shift($@)))', index(`$1',
  `-H'), 0, `$0(shift($@))', index(`$1', `--ha')index(`$1', `='), 0-1,
  `$0(shift(shift($@)))', index(`$1', `--ha'), 0, `$0(shift($@))', `$#', 1,
  `', `` $1'$0(shift($@))')')
define(`common', `ifdef(`__gnu__', `ifelse(`$2', `', `', `ensurehash(ifdef(
  `hashsize', hashsize, `$2'))')')ifdef(`file', `', `define(`file',
  `day$1.input')')`ok'')
