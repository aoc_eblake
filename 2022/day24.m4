divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day24.input] day24.m4
# Optionally use -Dverbose=1 to see some progress
# Optionally use -Dalgo=bfs|astar to choose search algorithm
# Optionally use -Dpriority=0|1|2|3|4|5 to choose priority queue algorithm

include(`common.m4')ifelse(common(24, 1000003), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

include(`priority.m4')
ifdef(`algo', `', `define(`algo', `astar')')

define(`input', translit(include(defn(`file')), nl`#', `;@'))
define(`x', 0)define(`y', 0)
define(`do', `ifelse(`$3', `;', `define(`y', incr($2))define(`maxx',
  decr($1))define(`x', 0)', `ifelse(`$3', `>', `define(`r$1_$2', `.')', `$3',
  `<', `define(`R$1_$2', `.')', `$3', `v', `define(`c$1_$2', `.')', `$3', `^',
  `define(`C$1_$2', `.')')define(`x', incr($1))')')

ifdef(`__gnu__', `
  patsubst(defn(`input'), `.', `do(x, y, `\&')')
',`
  define(`chew', `ifelse($1, 1, `do(x, y, `$2')', `$0(eval($1/2), substr(
    `$2', 0, eval($1/2)))$0(eval($1-$1/2), substr(`$2', eval($1/2)))')')
  chew(len(defn(`input')), defn(`input'))
')
define(`maxy', decr(y))define(`x', decr(maxx))define(`y', decr(maxy))

#              123456      123456
# time 0: rx_1 >>.... Rx_1 ...<.< look up 1,1 at x=1 x=1; 2,1 at x=2 x=2
# time 1: rx_1 .>>... Rx_1 ..<.<. look up 1,1 at x=6 x=2; 2,1 at x=1 x=3
# time 2: rx_1 ..>>.. Rx_1 .<.<.. look up 1,1 at x=5 x=3; 2,1 at x=6 x=4
define(`r', `defn(`r'eval(('x`-$3%'x`-1+$1)%'x`+1)`_$2')')
define(`R', `defn(`R'eval(($3+'x`-1+$1)%'x`+1)`_$2')')
define(`c', `defn(`c$1_'eval(('y`-$3%'y`-1+$2)%'y`+1))')
define(`C', `defn(`C$1_'eval(($3+'y`-1+$2)%'y`+1))')
define(`b', `ifdef(`b$1_$2_$3', `', `define(`b$1_$2_$3',
  r($@)R($@)c($@)C($@))')b$1_$2_$3')
define(`p', `ifelse(`$1.$2', `1.0',
  `addwork($@)', `$1.$2', 'x.maxy`, `addwork($@)', $1, 0, `', $1, 'maxx`, `',
  $2, -1, `', $2, 0, `', $2, 'maxy`, `', $2, 'incr(maxy)`, `', `ifelse(b($@),
  `', `addwork($@)')')')
define(`visit', `p($1, $2, $3)p(incr($1), $2, $3)p(decr($1), $2, $3)p($1,
  incr($2), $3)p($1, decr($2), $3)')

ifelse(defn(`algo'), `bfs', `
output(1, `Using bfs search')
define(`addwork', `ifdef(`p$1_$2_$3', `', `pushdef(`work$3',
  `$*')define(`p$1_$2_$3')')')
define(`_round', `ifelse(`$1,$2', defn(`goal'), `clear($3)clear(incr(
  $3))pushdef(`round', `$3popdef(`round')')', `popdef(`p$1_$2_$3')visit($1,
  $2, $4)')')
define(`round', `ifdef(`work$1', `_$0(work$1, $2)popdef(`work$1')$0($@)',
  `ifelse(eval($1%50), 0, `output(1, `...time $1')')$0($2, incr($2))')')
define(`_clear', `popdef(`p$1_$2_$3')')
define(`clear', `ifdef(`work$1', `_$0(work$1)popdef(`work$1')$0($@)')')

', defn(`algo'), `astar', `
output(1, `Using A* search')
define(`abs', `($1<$2)*($2-$1)+($1>$2)*($1-$2)')
define(`dist', `eval(abs($1, $3)+abs($2, $4)+$5)')
define(`addwork', `ifdef(`p$1_$2_$3', `', `define(`p$1_$2_$3')insert(dist(goal,
  $1, $2, $3), $@)')')
define(`_round', `ifelse(defn(`goal'), `$2,$3', `$4clearall()popdef(
  `p$2_$3_$4')', `visit($2, $3, incr($4))round()')')
define(`round', `_$0(pop())')

', `fatal(`unknown search algo')')

define(`progress', 0)
define(`rename', `define(`$2', defn(`$1'))popdef(`$1')')
define(`show0', `output(1, `...$1')rename(`show$1', `show'eval($1+20000))')
define(`show', `ifdef(`$0$1', `$0$1($1)')define(`progress', incr($1))')
ifelse(eval(verbose >1), 1, `define(`round', `show(progress)'defn(`round'))')

define(`find', `define(`goal', `$1,$2')addwork($3, $4, $5)round($5, incr($5))')
define(`part1', find(x, maxy, 1, 0, 0))
define(`back', find(1, 0, x, maxy, part1))
define(`part2', find(x, maxy, 1, 0, back))

divert`'part1
part2
