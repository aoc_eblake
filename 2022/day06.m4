divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day06.input] day06.m4

include(`common.m4')ifelse(common(06), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`input', translit(include(defn(`file')), alpha`'nl, ALPHA))
define(`cnt', 1)define(`cur')
define(`goal', `ABCDEFGHIJKLMN')define(`need', 10)
define(`end', `define(`part2', `$1')pushdef(`do')')
pushdef(`goal', `ABCD')pushdef(`need', 4)
pushdef(`end', `define(`part1', `$1')popdef(`goal')popdef(`end')popdef(`need')')
define(`collect', `ifelse($2, 0, `substr(`$3', 1)', `define(`need',
  decr($2))`$3'')`$1', defn(`goal')')
define(`cmp', `ifelse(translit(`$2', `$2', `$3'), `$3', `end(`$1')')define(
  `cur', `$2')')
define(`do', `cmp($1, collect(`$2', need, defn(`cur')))define(`cnt', incr($1))')

ifdef(`__gnu__', `
  patsubst(defn(`input'), `.', `do(cnt, `\&')')
',`
  define(`chew', `ifelse($1, 1, `do(cnt, `$2')', `$0(eval($1/2), substr(
    `$2', 0, eval($1/2)))$0(eval($1-$1/2), substr(`$2', eval($1/2)))')')
  chew(len(defn(`input')), defn(`input'))
')

divert`'part1
part2
