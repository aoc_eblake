divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day15.input] [-Drow=N] [-Dbound=N] day15.m4
# Defaults: row=10 bound=20 for file=*tmp.*, 2000000/4000000 otherwise

include(`common.m4')ifelse(common(15), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')
ifdef(`row', `', `define(`row', ifelse(index(defn(`file'), `tmp.'), -1,
  `2000000', `10'))')
ifdef(`bound', `', `define(`bound', ifelse(index(defn(`file'), `tmp.'), -1,
  `4000000', `20'))')

define(`ranges')define(`offset', 0)define(`diags')define(`diffs')
define(`delta', `(($1 - $2)*($1>$2) + ($2 - $1)*($2>$1))')
define(`dist', `eval(delta($1, $3)+delta($2, $4))')
define(`cmp', `ifelse(eval(`$1$2$3'), 1, `$1', `$3')')
define(`merge', `ifelse(`$3', `', ``,$@'', `ifelse(eval(`$2<$3'), 1, ``,$@'',
  `ifelse(eval(`$4<$1'), 1, ``,`$3',`$4''merge(`$1', `$2'', `merge(cmp(`$1',
  `<', `$3'), cmp(`$2', `>', `$4')'), shift(shift(shift(shift($@)))))')')')

define(`edge', `ifelse(eval(`$2>=0&&$2<2*''bound`), 1, `ifdef(`$3$2',
  `ifelse(eval($3$2+$4), 3, `define(`$3$2', 3)define(`$3s',
  `,'eval($1)defn(`$3s'))')', `define(`$3$2', $4)')')')
define(`diag', `edge($1, eval($1), `$0', $2)')
define(`diff', `edge($1, eval($1+'bound`), `$0', $2)')
define(`scan', `ifelse(eval(`$5>=$6'), 1, `define(`ranges', merge(eval(
  `$1-$5+$6'), eval(`$1+$5-$6+1')ranges))ifelse(`$4', ''row``, `ifdef(`b$3_$4',
  `', `define(`b$3_$4')define(`offset', incr(offset))')')')diag(`$1+$2-$5-1',
  1)diag(`$1+$2+$5+1', 2)diff(`$1-$2-$5-1', 1)diff(`+$1-$2+$5+1', 2)')
define(`S', `scan($@, dist($@), delta($2, 'row`))')
translit((include(defn(`file'))), `r:'nl`= a-z()', `(,)')

define(`collect', `ifelse(`$2', `', `$1', `$0(eval(`$1 + $3 - $2'),
  shift(shift(shift($@))))')')
define(`part1', eval(collect(-offset`'ranges)))
define(`_score', `eval(`$1*4+$2/1000000')eval(`$2%1000000', `', 6)')
define(`score', `ifelse(`$#', 4, `_$0(`($2 + $4)/2', `($2 - $4)/2')',
  ``more work needed: $*'')')
define(`part2', score(diags, diffs))

divert`'part1
part2
