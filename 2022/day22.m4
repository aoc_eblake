divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day22.input] day22.m4

include(`common.m4')ifelse(common(22), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

# https://www.reddit.com/r/adventofcode/comments/zsgbe7/2022_day_22_question_about_your_input/
# There are 11 possible cube nets (compounded by which reflection and
# rotation a given net is presented in), but rather than generically
# coding all of them, I'm limiting effort by just hardcoding those
# two puzzle layouts.
define(`input', translit(include(defn(`file')), nl`# ', `;@-'))
define(`idx', index(defn(`input'), `.'))
define(`_map', `define(`m'eval($1)`_'eval($2)`_$3', eval($4)`,'eval($5))define(
  `M'eval($1)`_'eval($2)`_$3', eval($6)`,'eval($7)`,$8')')
define(`R', 0)define(`D', 1)define(`L', 2)define(`U', 3)
ifelse(idx, 8, `
dnl sample input
dnl ..1.
dnl 234.
dnl ..56
define(`map', `forloop_var(`v', 1, 4, `_$0($@)')')
map( `8+v',     0, U,  `8+v',    12,  `5-v',      5, D)dnl 1U/5D/2U
map(     8,   `v', L,     12,   `v',  `4+v',      5, D)dnl 1L/1R/3U
map(    13,   `v', R,      9,   `v',     16, `13-v', L)dnl 1R/1L/6R
map(   `v',     4, U,    `v',     8, `13-v',      1, D)dnl 2U/2D/1U
map( `4+v',     4, U,  `4+v',     8,      9,    `v', R)dnl 3U/3D/1L
map(     0, `4+v', L,     12, `4+v', `17-v',     12, U)dnl 2L/4R/6D
map(    13, `4+v', R,      1, `4+v', `17-v',      9, D)dnl 4R/2L/6U
map(   `v',     9, D,    `v',     5, `13-v',     12, U)dnl 2D/2U/5D
map( `4+v',     9, D,  `4+v',     5,      9, `13-v', R)dnl 3D/3U/5L
map(`12+v',     8, U, `12+v',    12,     12,  `9-v', L)dnl 6U/6D/4R
map(     8, `8+v', L,     16, `8+v',  `9-v',      8, U)dnl 5L/6R/3D
map(    17, `8+v', R,      9, `8+v',     12,  `5-v', L)dnl 6R/5L/1R
map( `8+v',    13, D,  `8+v',     1,  `5-v',      8, U)dnl 5D/1U/2D
map(`12+v',    13, D, `12+v',     9,      1,  `9-v', R)dnl 6D/6U/2L
', idx, 50, `
dnl puzzle input
dnl .12
dnl .3.
dnl 45.
dnl 6..
define(`map', `forloop_var(`v', 1, 50, `_$0($@)')')
map( `50+v',       0, U,  `50+v',     150,       1, `150+v', R)dnl 1U/5D/6L
map(`100+v',       0, U, `100+v',      50,     `v',     200, U)dnl 2U/2D/6D
map(     50,     `v', L,     150,     `v',       1, `151-v', R)dnl 1L/2R/4L
map(    151,     `v', R,      51,     `v',     100, `151-v', L)dnl 2R/1L/5R
map(`100+v',      51, D, `100+v',       1,     100,  `50+v', L)dnl 2D/2U/3R
map(     50,  `50+v', L,     100,  `50+v',     `v',     101, D)dnl 3L/3R/4U
map(    101,  `50+v', R,      51,  `50+v', `100+v',      50, U)dnl 3R/3L/2D
map(    `v',     100, U,     `v',     200,      51,  `50+v', R)dnl 4U/6D/3L
map(      0, `100+v', L,     100, `100+v',      51,  `51-v', R)dnl 4L/5R/1L
map(    101, `100+v', R,       1, `100+v',     150,  `51-v', L)dnl 5R/4L/2R
map( `50+v',     151, D,  `50+v',       1,      50, `150+v', L)dnl 5D/1U/6R
map(      0, `150+v', L,      50, `150+v',  `50+v',       1, D)dnl 6L/6R/1U
map(     51, `150+v', R,       1, `150+v',  `50+v',     150, U)dnl 6R/6L/5D
map(    `v',     201, D,     `v',     101, `100+v',       1, D)dnl 6D/4U/2U
', `fatal(`unexpected input layout')')

define(`set3', `define(`$1', $4)define(`$2', $5)define(`$3', $6)')
set3(`x', `y', `a', 1, 1, 0)
define(`dx', `define(`x', incr($1))')
define(`dy', `ifelse(`$1', 1, `popdef(`do')s()',
  `define(`x', 1)define(`y', incr($2))')')
define(`dp', `define(`p$1_$2', `$3')dx($1)')
pushdef(`dp', `set3(`X', `Y', `F', $1, $2, 0)define(`s', `set3(`x',
  `y', `f', $1, $2, 0)')popdef(`$0')$0($@)')
define(`try', `ifelse(p$3_$4, `@', `$1', `decr($2), $3, $4, $5')')
define(`try1', `try(`0, $4, $5, $6', `$1', ifdef(`p$2_$3', `$2, $3',
  `m$2_$3_$6'), $6)')
define(`try2', `try(`0, $4, $5, $6', `$1', ifdef(`p$2_$3', `$2, $3, $6',
  `M$2_$3_$6'))')
define(`_move0', `incr($1), $2')
define(`_move1', `$1, incr($2)')
define(`_move2', `decr($1), $2')
define(`_move3', `$1, decr($2)')
define(`_move', `ifelse($1, 0, `$2, $3, $4', `$0(try$5($1,
  $0$4($2, $3), $2, $3, $4), $5)')')
define(`move', `set3(`x', `y', `f', _$0($1, x, y, f, 1))define(`f',
  eval((f+$2)%4))set3(`X', `Y', `F', _$0($1, X, Y, F, 2))define(`F',
  eval((F+$2)%4))define(`a', 0)')
define(`do', `ifelse(`$1', `L', `move(a, 3)', `$1', `R', `move(a, 1)', `$1',
  `;', `move(a, 0)define(`part1', eval(y*1000+x*4+f))define(`part2',
  eval(Y*1000+X*4+F))', `define(`a', eval(a*10+$1))')')
pushdef(`do', `ifelse(`$1', `;', `dy(x, y)', `$1', `-', `dx(x)', `dp(x, y,
  `$1')')')

ifdef(`__gnu__', `
  patsubst(defn(`input'), `.', `do(`\&')')
',`
  define(`chew', `ifelse($1, 1, `do(`$2')', `$0(eval($1/2), substr(
    `$2', 0, eval($1/2)))$0(eval($1-$1/2), substr(`$2', eval($1/2)))')')
  chew(len(defn(`input')), defn(`input'))
')

divert`'part1
part2
