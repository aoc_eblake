divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day23.input] day23.m4
# Optionally use -Dverbose=1 to see some progress

include(`common.m4')ifelse(common(23), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

# My 71x71 input grid expands to around 140x140 before stabilizing; this
# is small enough to map into a 1D position (256x256 with offset 50)
define(`input', translit(include(defn(`file')), nl`.#', `;01'))
define(`n', 0)define(`offset', 50)
define(`y', offset)define(`x', eval(offset+(y<<8)))
define(`do', `ifelse(`$3', `;', `define(`y', incr($2))define(`x',
  eval(offset+(($2+1)<<8)))', `define(`p$1', ifelse(`$3', 1, `define(`e'n,
  n`,$1')define(`n', incr(n))1'))define(`x', incr($1))')')

ifdef(`__gnu__', `
  patsubst(defn(`input'), `.', `do(x, y, `\&')')
',`
  define(`chew', `ifelse($1, 1, `do(x, y, `$2')', `$0(eval($1/2), substr(
    `$2', 0, eval($1/2)))$0(eval($1-$1/2), substr(`$2', eval($1/2)))')')
  chew(len(defn(`input')), defn(`input'))
')

# At most two elves compete for a position
define(`D', defn(`define'))define(`P', defn(`pushdef'))
define(`U', defn(`popdef'))define(`I', defn(`ifdef'))
define(`E', defn(`ifelse'))define(`V', defn(`eval'))
define(`M', defn(`decr'))define(`N', defn(`incr'))
define(`qa')
define(`q', `D(`q$2',I(`q$2',,``m(e$1,$2)'P(`qa',`q$2(U(`q$2'))U(`qa')qa')'))')
define(`qx')
define(`q0', `q($1,V($2-256))')
define(`q1', `q($1,V($2+256))')
define(`q2', `q($1,M($2))')
define(`q3', `q($1,N($2))')
define(`c0', `E(`$2$3$4$5$6$7$8$9',,`qx',`$2$3$4',,`q0','dnl
``$7$8$9',,`q1',`$2$5$7',,`q2',`$4$6$9',,`q3',`qx')')
define(`c1', `E(`$2$3$4$5$6$7$8$9',,`qx',`$7$8$9',,`q1','dnl
``$2$5$7',,`q2',`$4$6$9',,`q3',`$2$3$4',,`q0',`qx')')
define(`c2', `E(`$2$3$4$5$6$7$8$9',,`qx',`$2$5$7',,`q2','dnl
``$4$6$9',,`q3',`$2$3$4',,`q0',`$7$8$9',,`q1',`qx')')
define(`c3', `E(`$2$3$4$5$6$7$8$9',,`qx',`$4$6$9',,`q3','dnl
``$2$3$4',,`q0',`$7$8$9',,`q1',`$2$5$7',,`q2',`qx')')
define(`_at', `I(`p$1',,`D(`p$1')')`,p$1'')
define(`at', `D(`a$1',_at(V($1-257))_at(V($1-256))_at(V($1-255))'dnl
`_at(M($1))_at(N($1))_at(V($1+255))_at(V($1+256))_at(V($1+257)))')
define(`p', `I(`a$2',,`at($2)')c$3(a$2)($1,$2)')
define(`m', `D(`p$2')D(`p$3',1)D(`e$1',`$1,$3')')
define(`act', `E(defn(`qa'),,`D(`part2', $1)P(`round')', `qa`'')')
define(`_round', forloop(0, decr(n), ``p(e'', ``,$'1`)''))
define(`round', `ifelse($1, 11, `define(`part1', bound)', eval($1%50), 0,
  `output(1, ...$1)')_$0(eval(`($1+3)%4'))act($1)$0(incr($1))')
define(`prep', `define(`x1', eval($2&255))define(`x2', x1)define(`y1',
  eval($2>>8))define(`y2', y1)')
define(`_bound', `ifelse(eval(($2&255)<x1), 1, `define(`x1', `($2&255)')',
  `ifelse(eval(($2&255)>x2), 1, `define(`x2', `($2&255)')')')ifelse(
  eval(($2>>8)<y1), 1, `define(`y1', `($2>>8)')', `ifelse(eval(($2>>8)>y2),
  1, `define(`y2', `($2>>8)')')')')
define(`bound', `prep(e0)forloop(0, decr(n), `_$0(first(`e'', `))')eval(
  (x2-x1+1)*(y2-y1+1)-n)')
round(1)
ifdef(`part1', `', `define(`part1', bound)')

divert`'part1
part2
