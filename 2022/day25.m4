divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day25.input] day25.m4

include(`common.m4')ifelse(common(25), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`input', translit(include(defn(`file')), `-=012'nl, `43567;'))
define(`accum')
define(`_add', `add(`$1', `$2', eval($3/5-1))eval($3%5+3)')
define(`add', `ifelse(`$1$2$3', 0, `', `$1', `', `$0(5, `$2', `$3')',
  `$2', `', `$0(`$1', 5, `$3')', `_$0(substr(`$1', 0, decr(len(`$1'))),
  substr(`$2', 0, decr(len(`$2'))), ($3+substr(`$1', decr(len(`$1')))+substr(
  `$2', decr(len(`$2')))-3))')')
define(`do', `define(`accum', add(accum, `$1'))')

ifdef(`__gnu__', `
  patsubst(defn(`input'), `\([^;]*\);', `do(`\1')')
', `
  define(`_chew', `do(substr(`$1', 0, index(`$1', `;')))define(
    `tail', substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(`tail'),
    `;'), -1, `', `$0(defn(`tail'))')')
  define(`chew', `ifelse(eval($1 < 40), 1, `_$0(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  chew(len(defn(`input')), defn(`input'))
')
define(`part1', translit(accum, `43567', `-=012'))

divert`'part1
no part2
