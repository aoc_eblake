divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day10.input] day10.m4

include(`common.m4')ifelse(common(10), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

# Map 'addx N' to 'x(N)', 'noop' to 'n()'.
define(`clk', 1)define(`accum', 1)define(`part1', 0)
define(`abs', `translit(eval($1), -)')
define(`x', `n()n()define(`accum', eval(accum+$1))')
define(`out', `define(`o$1', defn(`o$1')`$2')')
define(`_n', `ifelse(eval($1%40), 20, `define(`part1', eval(part1 +
  $1*$2))')out(eval(($1-1)%40/5), ifelse(eval(abs($2 - ($1-1)%40) < 2), 1,
  ``X'', `` ''))define(`clk', incr($1))')
define(`n', `_n(clk, accum)')
translit(include(defn(`file')), nl`p ado', `)((')

# Now run OCR on the output
include(`ocr.m4')
define(`part2', forloop(0, 7, `ocr(defn(`o'', `))'))

divert`'part1
part2
