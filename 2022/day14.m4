divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day14.input] day14.m4

include(`common.m4')ifelse(common(14), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

# Lots of duplicate rows in input; track pairs seen for less work
define(`input', translit((include(defn(`file'))), nl`,>() -', `;..'))
define(`maxy', 0)
define(`towards', `eval($1+($2>$1)-($1>$2))')
define(`_mark', `define(`p$1_$2')ifelse(`$1.$2', `$3.$4', `', `$0(towards(`$1',
  `$3'), towards(`$2', `$4'), `$3', `$4')')')
define(`mark', `ifdef(`p$1_$2_$3_$4', `', `define(`p$1_$2_$3_$4')_$0(
  $@)ifelse(eval($2 > maxy), 1, `define(`maxy', $2)')')')
define(`_do', `ifelse(`$3', `', `', `mark(`$1', `$2', `$3',
  `$4')$0(shift(shift($@)))')')
define(`do', `_$0(translit(`$1', `.', `,'))')

ifdef(`__gnu__', `
  patsubst(defn(`input'), `\([^;]*\);', `do(`\1')')
', `
  define(`_chew', `do(substr(`$1', 0, index(`$1', `;')))define(
    `tail', substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(
    `tail'), `;'), -1, `', `$0(defn(`tail'))')')
  define(`chew', `ifelse(eval($1 < 600), 1, `_$0(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  chew(len(defn(`input')), defn(`input'))
')

define(`floor', incr(incr(maxy)))define(`count', 0)
define(`check', `ifdef(`p$1_$2', `', `find($1, $2, incr($2))')')
define(`find', `ifelse($2, 'maxy`, `ifdef(`part1', `', `define(`part1',
  count)')')ifelse($2, 'floor`, `', `check($1, $3)check(decr($1), $3)check(
  incr($1), $3)define(`p$1_$2')define(`count', incr(count))')')
find(500, 0, 1)define(`part2', count)

divert`'part1
part2
