divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day13.input] day13.m4

include(`common.m4')ifelse(common(13), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`input', translit((include(defn(`file'))), nl`,()', `;.');)
define(`part1', 0)define(`lo', 1)define(`hi', 1)define(`cnt', 1)
define(`eat')
define(`cmp', `ifelse(`$1', `$2', `$0(', `$1', (), `1eat(', `$2',
  (), `0eat(', eat$1eat$2, `', `$0(first$1, first$2, (shift$1), (shift$2),',
  eat$1, `', `$0(first$1, `$2', (shift$1), `()',', eat$2, `', `$0(`$1',
  first$2, `()', (shift$2),', `eval(`$1<$2')eat(')shift(shift($@)))')
define(`arrange', `ifelse(cmp(`$2', `((2))'), 1, `define(`lo', incr(incr(lo)))',
  `ifelse(cmp(`((6))', `$1'), 1, `', `ifelse(cmp(`$1', `((2))'), 1, `define(
  `lo', incr(lo))define(`hi', eval(hi+cmp(`$2', `((6))')))', `define(`hi',
  eval(hi+1+cmp(`$2', `((6))')))')')')')
define(`_do', `ifelse(cmp(`$2', `$3'), 1, `define(`part1',
  eval(part1 + $1))arrange(`$2', `$3')', `arrange(`$3', `$2')')')
define(`do', `ifelse(`$1', `', `_$0(cnt`'translit(defn(`tmp'), `[.]',
  `(,)'))define(`tmp')define(`cnt', incr(cnt))', `define(`tmp',
  defn(`tmp')`.$1')')')

ifdef(`__gnu__', `
  patsubst(defn(`input'), `\([^;]*\);', `do(`\1')')
', `
  define(`_chew', `do(substr(`$1', 0, index(`$1', `;')))define(
    `tail', substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(
    `tail'), `;'), -1, `', `$0(defn(`tail'))')')
  define(`chew', `ifelse(eval($1 < 450), 1, `_$0(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  chew(len(defn(`input')), defn(`input'))
')
define(`part2', eval(lo*(lo+hi)))

divert`'part1
part2
