divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day16.input] day16.m4
# Optionally use -Dverbose=1 to see some progress
# Optionally use -Dreduce=floyd|bfs to choose reduction algorithm
# Optionally use -Dalgo=dfs|dijkstra|astar to choose search algorithm
# Optionally use -Dpriority=0|1|2|3|4|5 to choose priority queue algorithm

include(`common.m4')ifelse(common(16, 1000003), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

include(`priority.m4')
ifdef(`reduce', `', `define(`reduce', `bfs')')
ifdef(`algo', `', `define(`algo', `astar')')

# First, parse in the data, and simplify the graph
# Floyd-Warshall computes all pairwise minimum distances in O(n^3) effort,
# but since n is only 60 for my input, that isn't horrible.  Alternatively,
# a BFS search from each of the 16 interesting starting points (AA and up
# to 15 valve roooms) is also O(n^2) search from O(n) points, but with less
# overall work to perform.
#
# There are at most 15 valves worth opening. Part 2 needs to track the best
# score for each possible set of reachable rooms bM; however, that score is
# not sufficient for pruning (A->B may score worse than B->A, but if B is
# closer to C, A->B->C may be better than B->A->C), so this also uses bXXM
# which includes the current room.  Time taken (cutoffs at 26/30) does not
# need to be stored in state, but is passed through recursion to prune paths
# when time expires.
# n - number of input lines
# nN - number to name
# nXX - name to number
# rXX - valve rate for room XX
# tXX_ - neighbors of XX
# tXX_YY - travel time from XX to YY
# v - number of interesting rooms
# vXX - bitmask value for interesting room XX
# vall - bitmask for all interesting rooms
# bXXM - best time-30 score for room + bitmask
# BXXM - best time-30 time for room + bitmask
# bM - best time-26 score for bitmask
# i - number of seen time-26 bitmasks
# list - interesting rooms, sorted highest rate first
define(`v', 0)define(`vall', 0)
define(`list')
define(`sort', `ifelse(`$2', `', ``,`$1''', `ifelse(eval(r$1>r$2), 1,
  ``,$@'', ``,`$2''$0(`$1', shift(shift($@)))')')')
define(`gen', `define(`v$6', `(1<<$7)')define(`vall', eval(vall|
  v$6))define(`visit$6', `ifelse(eval('dquote(`$8$5&'v$6)`),0,`visit(`$6',
  eval(`$8$2+1+'t$8$1_$6),`$8$3',`$8$4',`$8$5')')')define(`list',
  sort(`$6'list))define(`v', incr($7))')
define(`n', 0)define(`ceil', 0)define(`Valve', `_/')
translit((include(defn(`file'))), `/=;'nl` 'alpha, `(,,)'define(`_',
  `define(`n'n, `$1')define(`n$1', n)define(`t$1_$1', 0)define(`r$1',
  $2)define(`ceil', eval(ceil+$2))define(`t$1_', dquote(`', shift(shift(
  $@))))ifelse(`$2', 0, `', `gen(1, 2, 3, 4, 5, `$1', v, `$')')define(`n',
  incr(n))'))
define(`_visiteach', _foreach(`defn(`visit'', `)', list))dnl see _visit below

ifelse(defn(`reduce'), `floyd', `
output(1, `Using Floyd-Warshall to reduce graph')
define(`prep', `_foreach(`define(`t$1_'', `, 1)', t$1_)')
forloop(0, decr(n), `prep(defn(`n'', `))')
define(`t', `ifdef(`t$1_$2', `t$1_$2', 'n`)')
define(`_bump', `ifelse(eval(`$2>$3+$4'), 1, `define(`$1', eval(`$3+$4'))')')
define(`bump', `_$0(`t$1_$2', t(`$1', `$2'), t(`$1', `$3'), t(`$3', `$2'))')
define(`prep2', `forloop(0, $1, `bump(`$2', defn(`n'', `), `$3')')')
define(`prep1', `forloop(0, $1, `prep2($1, defn(`n'', `), `$2')')')
forloop(0, decr(n), `prep1('decr(n)`, defn(`n'', `))')

', defn(`reduce'), `bfs', `
output(1, `Using BFS to reduce graph')
define(`round', `ifdef(`t$1_$3', `', `define(`t$1_$3', $2)t$3_')')
define(`_walk', `ifelse(`$3', `', `', `$0(`$1', incr($2)_foreach(`round(`$1',
  `$2', ', `)', shift($@)))')')
define(`walk', `ifelse(r$1, 0, `', `_$0(`$1', 1t$1_)')')
_walk(`AA', 1tAA_)
forloop(0, decr(n), `walk(defn(`n'', `))')

', `fatal(`unknown algorithm for reduce')')

define(`part1', 0)define(`i', 0)
define(`progress', 0)
define(`rename', `define(`$2', defn(`$1'))popdef(`$1')')
define(`show0', `output(1, `...$1')rename(`show$1', `show'eval($1+100000))')
define(`show', `ifdef(`$0$1', `$0$1($1)')define(`progress', incr($1))')
define(`check1', `ifelse(eval($1>part1), 1, `define(`part1', $1)')')
define(`check2', `ifelse(ifdef(`b$2', `eval(`$1-4*$3>'b$2)', `define(`i',
  incr(i))1'), 1, `define(`b$2', eval(`$1-4*$3'))')')
# _visit(oldroom, oldtime, newscore, newrate, newmask)
define(`_visit', `ifelse(eval(`$2<26'), 1, `check2($3, $5, $4)')ifelse(
  ifelse(eval(defn(`b$1$5')`+0<$3'), 1, `define(`b$1$5', $3)1')ifelse(
  eval(defn(`B$1$5')`+0<30-$2'), 1, `define(`B$1$5', `30-$2')1'), `', `', $5,
  'vall`, `check1($3)', `$0each($@)')')
# visit(newroom, newtime, oldscore, oldrate, oldmask)
define(`visit', `ifelse(eval($2>=30), 1, `check1($3)', `_$0(`$1', `$2',
  eval(`$3+(30-$2)*'r$1), eval($4+r$1), eval($5|v$1))')')
ifelse(eval(verbose >1), 1, `define(`visit', `show(progress)'defn(`visit'))',
  `output(1, `Starting search')')
define(`vAA', 0)visit(`AA', 0, 0, 0, 0)

output(1, ...i` possibilities to check')
define(`part2', 0)
define(`max', `ifelse($2, `', $1, `$0(ifelse(eval($2>$1), 1, $2, $1),
  shift(shift($@)))')')
define(`_b', `ifelse(eval(`$1&(1<<$2)'), 0, `', `, b(eval(`$1&~(1<<$2)'))')')
define(`b', `ifdef(`b$1', `', `define(`b$1', max(0forloop(0, ''decr(v)``,
  `_$0($1, ', `)')))')b$1')
define(`_collect', `ifelse(eval(`$1+$2'>part2), 1, `define(`part2',
  eval(`$1+$2'))')')
define(`collect', `_$0(b($1), b(eval('vall`^$1)))')
forloop_arg(0, eval(vall/2), `collect')

# TODO: does an alternative to DFS help?
# Search favors minimum priority, but we want maximum score; do this by
# using 30*sum(valves) as the ceiling, where ceiling-score is priority
dnl define(`goal', eval(0xfffff1e))
ifelse(`
define(`_use', `ifelse(ifdef(`v$3', `v$3($1)', 0), 1, `', ifdef(`d$1',
  `eval($2 < d$1)', 1), 1, `addwork($1, $2)show(progress)')')
define(`progress', 0)
define(`rename', `define(`$2', defn(`$1'))popdef(`$1')')
define(`show1000', `output(1, `...$1')rename(`show$1', `show'eval($1+1000))')
define(`show', `ifdef(`$0$1', `$0$1($1)')define(`progress', incr($1))')
define(`neighbors',)
dnl forloop(0, decr(count), ``check('', ``, $'`1, $'`2)'')dnl
dnl `first(ifdef(`n0', `defn(`n0')undefine(`n0')undefine(`n1')', `_$0()'))')
define(`_neighbors', `ifdef(`n1', `defn(`n1')popdef(`n1')$0()')')
define(`distance', `output(1, `...part $2')addwork(`0000000$1$2', 0)_$0($2)')
')

divert`'part1
part2
