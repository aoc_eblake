divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day18.input] day18.m4

include(`common.m4')ifelse(common(18), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`part1', 0)define(`part2', 0)define(`bound', 1)
define(`bump', `ifelse(eval($1>=bound), 1, `define(`bound', incr($1))')')
define(`_do', `pushdef(`pts', `$1,$2,$3')define(`p$1_$2_$3')define(`part1',
  eval(part1+6))bump($1)bump($2)bump($3)')
define(`do', `_$0(translit(`$1', `.', `,')))')

define(`input', translit((include(defn(`file'))), nl`,()', `;.'))
ifdef(`__gnu__', `
  patsubst(defn(`input'), `\([^;]*\);', `do(`\1')')
', `
  define(`_chew', `do(substr(`$1', 0, index(`$1', `;')))define(
    `tail', substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(`tail'),
    `;'), -1, `', `$0(defn(`tail'))')')
  define(`chew', `ifelse(eval($1 < 18), 1, `_$0(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  chew(len(defn(`input')), defn(`input'))
')

# Part 1: Identify all pairs of points that share a common face.
define(`shares', `ifdef(`p$1_$2_$3', `define(`part1', decr(decr(part1)))')')
define(`_check', `shares(incr($1), $2, $3)shares($1, incr($2), $3)shares($1,
  $2, incr($3))')
define(`check', `ifdef(`pts', `_$0(pts)popdef(`pts')$0()')')
check()

# Part 2: Identify all faces of points reachable in a modulo bounding box.
define(`fill', `ifdef(`p$1_$2_$3', `define(`part2', incr(part2))',
  `ifdef(`v$1_$2_$3', `', `define(`v$1_$2_$3')$0(eval(`($1+1)%$4'), $2, $3,
  $4)$0(eval(`($1+$4-1)%$4'), $2, $3, $4)$0($1, eval(`($2+1)%$4'), $3,
  $4)$0($1, eval(`($2+$4-1)%$4'), $3, $4)$0($1, $2, eval(`($3+1)%$4'),
  $4)$0($1, $2, eval(`($3+$4-1)%$4'), $4)')')')
fill(bound, bound, bound, incr(bound))

divert`'part1
part2
