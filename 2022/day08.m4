divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day08.input] day08.m4

include(`common.m4')ifelse(common(08), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`input', translit(include(defn(`file')), nl, `;'))
define(`x', 0)define(`y', 0)define(`part1', 0)define(`part2', 0)
define(`do', `ifelse($3, `;', `define(`maxx', decr($1))define(`x',
  0)define(`y', incr($2))', `define(`g$1_$2', $3)define(`s$1_$2', 1)define(`x',
  incr($1))')')

ifdef(`__gnu__', `
  patsubst(defn(`input'), `.', `do(x, y, `\&')')
',`
  define(`chew', `ifelse($1, 1, `do(x, y, `$2')', `$0(eval($1/2), substr(
    `$2', 0, eval($1/2)))$0(eval($1-$1/2), substr(`$2', eval($1/2)))')')
  chew(len(defn(`input')), defn(`input'))
')

define(`maxy', decr(y))
ifelse(maxx, maxy, `', `fatal(`input is not square')')

define(`bump1', `ifdef(`v$1', `', `define(`v$1')define(`part1', incr(part1))')')
define(`_bump2', `define(`s$1', `$2')ifelse(eval(`$2>'part2), 1,
  `define(`part2', `$2')')')
define(`bump2', `_$0(`$1', eval(s$1`*$2'))')
define(`view', `ifelse(eval(s1$2`<$1'), 1, `popdef(`s1$2')popdef(`s2$2')$0($@)')')
define(`_probe', `ifelse(eval(`$2>'v$4), 1, `bump1(`$1')define(`v$4', $2)')view(
  $2, `$4')bump2(`$1', ($3-s2$4))pushdef(`s1$4', `$2')pushdef(`s2$4', `$3')')
define(`probe', `ifelse($2, $3, `', `_$0(`$1_$2', g$1_$2, $5, `x')_$0(`$2_$1',
  g$2_$1, $5, `y')$0($1, $4($2), $3, `$4', incr($5))')')
define(`prep', `define(`v$1', -1)define(`s1$1', 10)define(`s2$1', 0)')
forloop(0, maxx, `prep(`x')prep(`y')probe(', `, 0, 'maxy`, `incr', 0)')
forloop(0, maxx, `prep(`x')prep(`y')probe(', `, 'maxy`, 0, `decr', 0)')

divert`'part1
part2
