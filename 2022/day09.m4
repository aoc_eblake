divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day09.input] day09.m4

include(`common.m4')ifelse(common(09), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`prep', `define(`p$1x', 500)define(`p$1y', 500)')
forloop_arg(0, 9, `prep')
define(`v1_500_500')define(`v2_500_500')define(`part1', 1)define(`part2', 1)
define(`bump', `ifdef(`v$1_$2_$3', `', `define(`v$1_$2_$3')define(`part$1',
  incr(part$1))')')
define(`visit', `define(`p$1x', eval(p$1x+$2))define(`p$1y',
  eval(p$1y+$3))ifelse($1, 1, `bump(1, p1x, p1y)', $1, 9, `bump(2, p9x,
  p9y)')ifelse($1, 9, `', `check($1, incr($1))')')
define(`check', `first(`off'eval(p$2x-p$1x+2)eval(p$2y-p$1y+2))($2)')
define(`off00', `visit($1, 1, 1)')
define(`off10', `visit($1, 1, 1)')
define(`off20', `visit($1, 0, 1)')
define(`off30', `visit($1, -1, 1)')
define(`off40', `visit($1, -1, 1)')
define(`off01', `visit($1, 1, 1)')
define(`off11')
define(`off21')
define(`off31')
define(`off41', `visit($1, -1, 1)')
define(`off02', `visit($1, 1, 0)')
define(`off12')
define(`off22')
define(`off32')
define(`off42', `visit($1, -1, 0)')
define(`off03', `visit($1, 1, -1)')
define(`off13')
define(`off23')
define(`off33')
define(`off43', `visit($1, -1, -1)')
define(`off04', `visit($1, 1, -1)')
define(`off14', `visit($1, 1, -1)')
define(`off24', `visit($1, 0, -1)')
define(`off34', `visit($1, -1, -1)')
define(`off44', `visit($1, -1, -1)')
define(`move', `ifelse($1, 0, `', `visit(0, $2, $3)$0(decr($1), $2, $3)')')

translit(include(defn(`file')), ` 'nl, `()'define(`U', `move('$`1, 0,
  -1)')define(`D', `move('$`1, 0, 1)')define(`L', `move('$`1, -1,
  0)')define(`R', `move('$`1, 1, 0)'))

divert`'part1
part2
