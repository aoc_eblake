divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day05.input] day05.m4

include(`common.m4')ifelse(common(05), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`input', translit(include(defn(`file')), ` 'nl, `.;'))
define(`mark', incr(index(defn(`input'), `;;')))
define(`intro', substr(defn(`input'), 0, mark))

# First phase: parse into stacks.  Assumes no more than 9 stacks in input;
# because of the use of translit to map an input line into stack population,
# and we are limited to single-letter macro names being beyond j.
# Process lines in reverse order to build stacks correctly.
define(`n', eval(index(defn(`intro'), `;')`/4+1'))
ifdef(`__gnu__', `
  patsubst(defn(`intro'), translit(eval(n*4, 1), 1, .), `pushdef(`line', `\&')')
', `
  define(`half', `eval($1/(n*4)/2*(n*4))')
  define(`chew', `ifelse($1, eval(n*4), `pushdef(`line', `$2')', `$0(half($1),
    substr(`$2', 0, half($1)))$0(eval($1-half($1)), substr(`$2', half($1)))')')
  chew(len(defn(`intro')), defn(`intro'))
')
define(`u', `ifelse(`$1', `.', `', `pushdef(`s$2', `$1')pushdef(`w$2', `$1')')')
define(`prep', `ifdef(`line', `translit(
  `u(`B',1)u(`F',2)u(`J',3)u(`N',4)u(`R',5)u(`V',6)u(`Z',7)u(`d',8)u(`h',9)',
  ''dquote(dquote(defn(`ALPHA')))```abcdefghij',
  defn(`line'))popdef(`line')$0()')')
prep()

# Second half: Swizzle the stacks per the instructions.
# The translit here turns "move A from B to C\n" into "v(A,B,C)"
define(`exch', `ifelse(`$1', 0, `', `pushdef(`$3', defn(`$2'))popdef(
  `$2')$0(decr($1), `$2', `$3')')')
define(`v', `exch(`$1', `s$2', `s$3')exch(`$1', `w$2', `tmp')exch(`$1',
  `tmp', `w$3')')
translit(substr(defn(`input'), incr(mark)), `eft;.mor', `(,,)')
define(`part1', forloop(1, n, `defn(`s'', `)'))
define(`part2', forloop(1, n, `defn(`w'', `)'))

divert`'part1
part2
