divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day07.input] day07.m4

include(`common.m4')ifelse(common(07), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

# Input has no vowels other than in 'dir' and no mixed case; changing 'n' to
# 'N' is sufficient to avoid a collision with dnl, and no other macro collides
define(`input', translit(include(defn(`file')), nl`n', `;N'))
define(`part1', 0)
define(`push', `pushdef(`accum', 0)')
define(`_pop', `ifdef(`accum', `define(`accum', eval(accum + $1))')')
define(`pop', `ifelse(eval(accum < 100000), 1, `define(`part1',
  eval(part1 + accum))')pushdef(`out', accum)_$0(accum`'popdef(`accum'))')
define(`_do', `ifelse(`$1$2', `$ls', `', `$1$3', `$..', `pop()',
  `$1', `$', `push()', `$1', `dir', `', `define(`accum', eval(accum + $1))')')
define(`do', `_$0(translit(`$1', ` ', `,'))')

ifdef(`__gnu__', `
  patsubst(defn(`input'), `\([^;]*\);', `do(`\1')')
', `
  define(`_chew', `do(substr(`$1', 0, index(`$1', `;')))define(
    `tail', substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(`tail'),
    `;'), -1, `', `$0(defn(`tail'))')')
  define(`chew', `ifelse(eval($1 < 40), 1, `_$0(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  chew(len(defn(`input')), defn(`input'))
')

define(`finish', `ifdef(`accum', `pop()finish()')')
finish()define(`part2', out)
define(`limit', eval(part2 - 40000000))
define(`finish', `ifdef(`out', `ifelse(eval(out < part2 && out > ''limit``),
  1, `define(`part2', out)')popdef(`out')finish()')')
finish()

divert`'part1
part2
