divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day19.input] day19.m4
# Optionally use -Dverbose=1 to see some progress
# Optionally use -Dalgo=dfs|dijkstra|astar to choose search algorithm
# Optionally use -Dpriority=0|1|2|3|4|5 to choose priority queue algorithms

include(`common.m4')ifelse(common(19), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

include(`priority.m4')
ifdef(`algo', `', `define(`algo', `astar')')

# Instead of tracking current geodes and geode robots, just track a
# single score (each time a geode robot is created, it is known how many
# geodes it will contribute).  Instead of directly simulating wait states,
# the choice to visit a neighbor computes the number of cycles needed to
# reach the chosen bot, so each _visit() can branch at most 4 times.  To
# further prune things, note that it is not worth choosing a bot that
# would not produce useful resources, and line up the DFS search with
# the later bots first so that states can short-circuit if they cannot
# possibly score better than the current max.
# - geode: need at least one obsR
# - obs: need at least one clayR, avoid if obsR>=cost6
# - clay: avoid if clayR>=cost4
# - ore: avoid if oreR>=max(cost1,cost2,cost3,cost5)
# - score cap: assuming infinite ore and obsidian supply, skip any visit
#   that cannot produce more geodes in time remaining than current best

define(`max', `ifelse(eval($2>$1), 1, $2, $1)')
define(`max4', `max(max($1, $2), max($3, $4))')
define(`check', `ifelse(eval($1>best), 1, `define(`best', $1)')')
# wait(have, need, bots)
define(`wait', `eval(`($1<$2)*($2-$1+$3-1)/$3+1')')
# buildgeo(oldtime, ore, clay, obs, oreR, clayR, obsR, oldscore, deltatime)
define(`buildgeo', `ifelse(eval($9>$1-1), 0, `visit(eval($1-$9),
  eval($2-cost5+$5*$9), eval($3+$6*$9), eval($4-cost6+$7*$9), $5, $6, $7,
  eval($8+$1-$9))')')
# buildobs(oldtime, ore, clay, obs, oreR, clayR, obsR, oldscore, deltatime)
define(`buildobs', `ifelse(eval($9>$1-3), 0, `visit(eval($1-$9),
  eval($2-cost3+$5*$9), eval($3-cost4+$6*$9), eval($4+$7*$9), $5, $6,
  incr($7), $8)')')
# buildclay(oldtime, ore, clay, obs, oreR, clayR, obsR, oldscore, deltatime)
define(`buildclay', `ifelse(eval($9>$1-5), 0, `visit(eval($1-$9),
  eval($2-cost2+$5*$9), eval($3+$6*$9), eval($4+$7*$9), $5, incr($6), $7,
  $8)')')
# buildore(oldtime, ore, clay, obs, oreR, clayR, obsR, oldscore, deltatime)
define(`buildore', `ifelse(eval($9>$1-3), 0, `visit(eval($1-$9),
  eval($2-cost1+$5*$9), eval($3+$6*$9), eval($4+$7*$9), incr($5), $6, $7,
  $8)')')
# _visit(oldtime, ore, clay, obs, oreR, clayR, obsR, oldscore)
define(`_visit', `ifelse($7, 0, `', `buildgeo($@, max(wait($2, cost5, $5),
  wait($4, cost6, $7)))')ifelse(eval(`$6 && $4+$7*($1-1) < ($1-1)*'cost6), 1,
  `buildobs($@, max(wait($2, cost3, $5), wait($3, cost4, $6)))')ifelse(
  eval(`$3+$6*($1-3) < ($1-3)*'cost4), 1, `buildclay($@, wait($2, cost2,
  $5))')ifelse(eval(`$3+$5*($1-1) < ($1-1)*'costm), 1, `buildore($@,
  wait($2, cost1, $5))')')
# visit(newtime, ore, clay, obs, oreR, clayR, obsR, newscore)
define(`visit', `check($8)ifelse(eval($8+$1*($1-1)/2>best), 1,
  `ifelse(ifdef(`v$2_$3_$4_$5_$6_$7', `eval($8>v$2_$3_$4_$5_$6_$7)',
  `pushdef(`state', `v$2_$3_$4_$5_$6_$7')1'), 1, `define(`v$2_$3_$4_$5_$6_$7',
  $8)_$0($@)')')')

define(`part1', 0)define(`part2', 1)
ifelse(eval(verbose >1), 1, `define(`visit', `show(progress)'defn(`visit'))',
  `output(1, `Starting search')')
define(`progress', 0)
define(`rename', `define(`$2', defn(`$1'))popdef(`$1')')
define(`show10000', `output(1, `...$1')rename(`show$1', `show'eval($1+10000))')
define(`show', `ifdef(`$0$1', `$0$1($1)')define(`progress', incr($1))')
define(`reset', `ifdef(`state', `popdef(defn(`state'))popdef(`state')$0()',
  `define(`best', 0)')')
define(`blueprint', `define(`cost1', $2)define(`cost2', $3)define(`cost3',
  $4)define(`cost4', $5)define(`cost5', $6)define(`cost6', $7)define(`costm',
  max4(cost1, cost2, cost3, cost5))reset()visit(23, 1, 0, 0, 1, 0, 0,
  0)define(`part1', eval(part1 + best*$1))output(1, `...$1/24 'best)ifelse(
  eval($1<=3), 1, `reset()visit(31, 1, 0, 0, 1, 0, 0, 0)define(`part2',
  eval(part2 * best))output(1, `...$1/32 'best)')')

# Now, parse in the data
define(`and', `.')define(`Blueprint', `_/')
translit(include(defn(`file')), `/:.'nl`'alpha` 'ALPHA, `(,,)'define(`_',
  `blueprint($@)'))

define(`_use', `ifelse(ifdef(`v$3', `v$3($1)', 0), 1, `', ifdef(`d$1',
  `eval($2 < d$1)', 1), 1, `addwork($1, $2)show(progress)')')
define(`neighbors',)
dnl forloop(0, decr(count), ``check('', ``, $'`1, $'`2)'')dnl
dnl `first(ifdef(`n0', `defn(`n0')undefine(`n0')undefine(`n1')', `_$0()'))')
define(`_neighbors', `ifdef(`n1', `defn(`n1')popdef(`n1')$0()')')
define(`distance', `output(1, `...part $2')addwork(`0000000$1$2', 0)_$0($2)')

dnl define(`part1', distance(init`'12341234, 1))

divert`'part1
part2
