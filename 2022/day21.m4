divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day21.input] day21.m4

include(`common.m4')ifelse(common(21), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

include(`math64.m4')
# div64 is not in math64.m4 because it is not fully generic; but it is good
# enough for the few large divisions seen in this puzzle
define(`bits', `_$0(eval($1, 2))')
define(`_bits', ifdef(`__gnu__', ``shift(patsubst($1, ., `, \&'))'',
  ``ifelse(len($1), 1, `$1', `substr($1, 0, 1),$0(substr($1, 1))')''))
define(`bits64', `ifelse(eval(len($1) < 10), 1, `bits($1)', `_$0(mul64($1,
  5)), eval(substr($1, decr(len($1))) & 1)')')
define(`_bits64', `bits64(substr($1, 0, decr(len($1))))')
define(`div64p', `ifelse($4, `', `$1,$2', `$0(_$0(add64($1, $1), add64(add64($2,
  $2), $4), $3), $3, shift(shift(shift(shift($@)))))')')
define(`_div64p', `ifelse(lt64($2, $3), 0, `add64($1, 1), sub64($2, $3)',
  `$1, $2')')
define(`div64', `ifelse(eval(`$1'), `$1', `eval(`$1/$2')',
  `first($0p(0, 0, $2, bits64(`$1')))')')

# Input can contain `nl' as substring; avoid macro collisions
define(`input', translit(include(defn(`file')), defn(`alpha')nl`:',
  defn(`ALPHA')`;'))
define(`_do', `define(`$1_', `e(`$2', `$3', `$4')')')
define(`do', `_$0(translit(`$1', ` ', `,'))')

ifdef(`__gnu__', `
  patsubst(defn(`input'), `\([^;]*\);', `do(`\1')')
', `
  define(`_chew', `do(substr(`$1', 0, index(`$1', `;')))define(
    `tail', substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(`tail'),
    `;'), -1, `', `$0(defn(`tail'))')')
  define(`chew', `ifelse(eval($1 < 35), 1, `_$0(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  chew(len(defn(`input')), defn(`input'))
')
define(`_e', `ifelse(`$1', `@', `pushdef(`undo', `popdef(`undo')undo(_e(
  $'1`,'translit(`$2', `-*/+', `+/*-')`,`$3'))')@', `$3', `@', `pushdef(`undo',
  `popdef(`undo')undo(_e('ifelse(`$2', `+', ``$'1`,-,$1'', `$2', `-',
  ``$1,-,$'1', `$2', `*', ``$'1`,/,$1'', ``$1,/,$'1')`))')@', `ifelse(`$2',
  `+', `add', `$2', `-', `sub', `$2', `*', `mul', `div')64(`$1', `$3')')')
define(`e', `ifelse(`$2', `', `$1', `_$0($1_, $2, $3_)')')
define(`part1', ROOT_)
define(`undo', `$1')
pushdef(`HUMN_', `@')
ROOT_
pushdef(`_e', `popdef(`$0')$1$3')
define(`part2', undo)

divert`'part1
part2
