divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day11.input] day11.m4
# Optionally use -Dverbose=1 to see some progress

include(`common.m4')ifelse(common(11), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

# Compress input into a more usable form.  From the example:
# Monkey 0:
#   Starting items: 79, 98
#   Operation: new = old * 19
#   Test: divisible by 23
#     If true: throw to monkey 2
#     If false: throw to monkey 3
# becomes 0,79.98,$1*19,1%$%23,2,$3,
define(`input', translit((include(defn(`file'))), `ldb,'nl` :='alpha`'ALPHA,
  `$1%.,'))
define(`_parse', `pushdef(`l$1', $2)pushdef(`L$1', `$2,$2')')
define(`parse', `ifelse(`$1', `', `', `define(`last', $1)define(`c$1',
  0)_foreach(`_$0($1,', `)', translit(`.$2', `.', `,'))define(`a$1',
  `eval(($3)$'`2)')define(`div$1', substr(`$4', 4))define(`t$1',
  `ifelse(eval($'eval(`($1&1)+1')substr(`$4', 3)`), 0, $5, 'substr($6,
  1)`)')$0(shift(shift(shift(shift(shift(shift(shift($@))))))))')')
first(`parse'defn(`input'))

# Part 1 is nonlinear (the truncating division by 3), but also small enough
# that 20 iterations fits in 32-bit math without special effort
define(`throw', `define(`c$1', incr(c$1))pushdef(`l't$1($2, $2), $2)turn($1)')
define(`turn', `ifdef(`l$1', `throw($1, a$1(l$1,
  `/3'popdef(`l$1')))')')
define(`round1000', `output(1, ...$1)define(`round'eval($1+1000),
  defn(`$0'))popdef(`$0')')
define(`round', `ifdef(`$0$1', `$0$1($1)')'forloop(0, last,
  ``turn('', ``)''))
forloop_arg(1, 20, `round')

define(`prod', `eval(`$1*$2')')
define(`max2', `ifelse($3, `', `$1, $2', `$0(ifelse(eval($3 > $1), 1,
  `$3, $1', eval($3 > $2), 1, `$1, $3', `$1, $2'), shift(shift(shift($@))))')')
define(`part1', prod(max2(0, 0forloop(0, last, `,defn(`c'', `)'))))

# Part 2 won't fit in signed 32-bit math directly. But since all 8 monkeys
# only care about distinct prime divisors, we can instead track numbers
# relative to lcm(divisors). Even that won't fit in 32-bit math, but if we
# split even and odd index divisors, the worst we can get is a modulus of
# 11*13*17*19 (half of the first 8 primes), which fortunately squares within
# 31 bits. The sample input uses 4 primes 13*17*19*23 instead of first 8.
define(`f0', 1)define(`f1', 1)
define(`_prep', `define(`f$2', eval(f$2*div$1))')
define(`prep', `_$0($1, eval($1&1))define(`c$1', 0)')
forloop_arg(0, last, `prep')

define(`throw', `define(`c$1', incr(c$1))pushdef(`L't$1($2, $3),
  `$2,$3')turn($1)')
define(`_turn', `throw($1, a$1($2, %'f0`), a$1($3, %'f1`))')
define(`turn', `ifdef(`L$1', `_$0($1, L$1`'popdef(`L$1'))')')
forloop_arg(1, 10000, `round')

include(`math64.m4')
define(`prod', `mul64(`$1', `$2')')
define(`part2', prod(max2(0, 0forloop(0, last, `,defn(`c'', `)'))))

divert`'part1
part2
