divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day20.input] day20.m4
# Optionally use -Dverbose=1 to see some progress
# Optionally use -Dscan=linear|sqrt|log to choose scan algorithm

include(`common.m4')ifelse(common(20), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

ifdef(`scan', `', `define(`scan', `log')')

# Each input is stored in iN, with cnt tracking number of inputs, and z the
# index of the line relevant to the solutions.
define(`cnt', 0)
define(`do', `define(`i$1', $2)define(`cnt', incr($1))ifelse($2, 0,
  `define(`z', $1)')')

define(`input', translit(include(defn(`file')), nl, `;'))
ifdef(`__gnu__', `
  patsubst(defn(`input'), `\([^;]*\);', `do(cnt, `\1')')
', `
  define(`_chew', `do(cnt, substr(`$1', 0, index(`$1', `;')))define(
    `tail', substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(`tail'),
    `;'), -1, `', `$0(defn(`tail'))')')
  define(`chew', `ifelse(eval($1 < 12), 1, `_$0(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  chew(len(defn(`input')), defn(`input'))
')

ifelse(defn(`scan'), `linear', `
output(1, `Using O(n) scan on doubly-linked list')
# Nodes are stored in a doubly-linked circular list: a given index with value
# iN has next at nN and previous pN, and action aN says which direction and
# how many spots forward or backward (fN/bN) to scan for the insertion point.
# Scanning is linear, with an average of cnt/4 spots visited.  Once start
# and end spots are found, stitching redefines 6 links associated with 5
# nodes: s(a,b,i,d,e) turns a<=>i<=>b, c<=>d into a<=>b, c<=>i<=>d
# For 5000 elements, effort exceeds 67 million scans, but less than 50k eval
define(`D', defn(`define'))
define(`_prep', ``s(p$1,n$1,$1,$2$3($1))'')
define(`prep', `D(`a$1', ifelse($2, 0, `', `_$0($1, ifelse(eval(
  $2>''cnt``/2), 1, ``b',eval(- $2+''cnt``-1)', eval($2<-''cnt``/2), 1,
  ``f',eval($2+''cnt``-1)', eval($2>0), 1, ``f',$2', ``b',eval(- $2)'))'))')
ifelse(eval(verbose >1), 1, `define(`_prep', defn(`_prep')`define(`effort',
  eval(effort+$3))')')
define(`_reset', `D(`n$1', incr($1))D(`p'incr($1), $1)prep($1,
  eval(i$1$2%('cnt`-1)))ifelse(`$2', `', `D(`f'incr($1),
  `f$1(n$'1`)')D(`b'incr($1), `b$1(p$'1`)')')')
define(`reset', `forloop(0, decr(cnt), `_$0(', `, `$1')')define(`n'decr(cnt),
  0)define(`p0', decr(cnt))define(`f0', `$'1`,n$'1)define(`b0', `p$'1`,$'1)')

define(`a', `a$1')
define(`s', `D(`p$2',`$1')D(`n$1',`$2')D(`n$3',`$5')D(`p$5',`$3')D(`n$4',
  `$3')D(`p$3',`$4')')
define(`coord3', `+i$1')
define(`coord2', `+i$1`'coord3(f'eval(1000%cnt)`($1))')
define(`coord1', `+i$1`'coord2(f'eval(1000%cnt)`($1))')
define(`coord', `coord1(f'eval(1000%cnt)`(z))')
define(`account', `output(2, `effort: 'eval($1*defn(`effort')+0))')

', defn(`scan'), `sqrt', `
output(1, `Using O(sqrt n) scan on bins of singly-linked lists')
# Nodes are stored in bins starting at 70 elements each, for 72 bins. Each bin
# is singly-linked with bN at the head and lN the current bin size; for
# convenience, bbN is the next bin in a circular list of bins.  Each index has
# nN as the next element in its bin or blank if it the tail of the bin, and BN
# is the bin currently containing that index.  Bin size is not automatically
# rebalanced, but with only 10 mixes of input that is fairly evenly
# distributed, the bins do not degrade too badly.  On average, moving a node
# requires scanning through half of one bin to find the start spot, jumping
# through half the bins to find the destination bin, then scanning through half
# that bin to find the destination spot.  Worst-case input could degrade to all
# indices migrating to a common bin, with linear scanning from then on.
# f: find by value, c: move by count, s: stitch
# For 5000 elements, effort exceeds 7.3 million recursive calls to f and c,
# along with 3.9 million calls to eval.  Tracing bin assignments shows that
# bin 1 is the destination only 9 times, while bin 63 is the destination
# 1900 times; this skew explains why effort is more than the predicted 5.7
# million calls if the code also implemented bin rebalancing.
# Implementing backward bin scanning may cut effort roughly in half.
ifdef(`bin', `', `define(`bin', ifelse(cnt, 7, 3, 70))')
define(`_prep', ``s($'`1,$1,n$1,f(`b'$'`1,0,$1)+$2)'')
define(`prep', `define(`a$1', ifelse($4, 0, `', `_$0($1, $4)'))ifelse($2,
  'decr(bin)`, `define(`n$1')define(`l$3', 'bin`)define(`bb$3',
  incr($3))define(`b'incr($3), incr($1))', `define(`n$1', incr($1))')define(
  `B$1', $3)')
define(`_reset', `prep($1, eval($1%'bin`), eval($1/'bin`),
  eval((i$1$2%$3+$3)%$3))')
define(`reset', `forloop(0, decr(cnt), `_$0(', `, `$1', ''decr(
  cnt)``)')define(`bb'eval(cnt/bin), 0)define(`l'eval(cnt/bin),
  eval(cnt%bin))define(`b0', 0)define(`n'decr(cnt))')

define(`a', `a$1(B$1)')
define(`_c', `ifelse($2, 0, `$1,`$3'', `$0($1, decr($2), `n'$3)')')
define(`c', `ifelse(eval(l$1<$3), 1, `$0(bb$1, `', eval($3-l$1))', `_$0($1,
  eval($3), `b$1')')')
define(`f', `ifelse($1, $3, ``$1',$2', `$0(`n'$1, incr($2), $3)')')
define(`augment', `define(`$1', defn(`$1')`define(`effort', incr(effort))')')
ifelse(eval(verbose >1), 1, `augment(`f')augment(`c')augment(`_c')')
define(`_s', `define(`B$3', $1)define(`n$3', $2)define(`$2', $3)define(`l$1',
  incr(l$1))')
define(`s', `define(`l$1', decr(l$1))define(`$4', $3)_$0(c($1, `', $5), $2)')
define(`get', `ifelse($2, `', `defn(`b'bb$1)', `$2')')
define(`coord3', `+i$1')
define(`coord2', `+i$1`'coord3(get(c(B$1, f(`b'B$1, 0, $1)+eval(1000%cnt))))')
define(`coord1', `+i$1`'coord2(get(c(B$1, f(`b'B$1, 0, $1)+eval(1000%cnt))))')
define(`coord', `coord1(get(c(defn(`B'z), f(`b'defn(`B'z), 0,
  z)+eval(1000%cnt))))')
define(`account', `output(2, `effort: 'effort)')

', defn(`scan'), `log', `
output(1, `Using O(log n) scan on WAVL tree')
# See http://sidsen.azurewebsites.net//papers/rb-trees-talg.pdf
# Store the indices in a modified WAVL search tree of ordered nodes (keys are
# implicit: insertion by position rather than key value). Every node knows the
# size of its subtrees, and by storing a parent link, a given node can
# determine its index in O(log n) effort.  For 5000 nodes, there are just over
# 12 levels, or roughly 24 defines per deletion/insertion sequence just to keep
# the sizes up-to-date (over 1.3 million calls), plus more overhead in
# maintaining tree pointers.  Still, this is less overall effort, and we
# are guaranteed the tree remains self-balanced due to the WAVL algorithm.
# Each index N is tied to a given node consisting of nNL, nNR, nNp, nNP, nNs
# for left, right, parent, rank parity, and size; there are always cnt nodes,
# but shuffling temporarily removes a node from one spot in tree r and inserts
# it elsewhere. The nil pointer is an empty string, not an undefined macro.
define(`D', defn(`define'))define(`I', defn(`ifelse'))
define(`Def', defn(`define'))
ifelse(eval(verbose >1), 1, `define(`Def', `D(`effort',incr(effort))D($'`@)')')
define(`nP', 1)define(`np')define(`ns', 0)define(`nL')define(`nR')
define(`parity', `n$1P`'')
define(`t0', 1)define(`t1', 0)
define(`toggle', `D(`n$1P', defn(`t'n$1P))')
define(`size', `n$1s`'')
define(`cmp', `eval(`(($1)>($2))-(($2)>($1))')')
define(`splicep', `D(`n$1p', $3)I($3, `', `D(`r', $1)',
  `I($2, n$3L, `D(`n$3L', $1)', `D(`n$3R', $1)')')')
define(`splicec', `D(`n$1$3', $2)I($2, `', `', `D(`n$2p', $1)')')
#    z=$3         y
# x=$2    D =>  x   z
# A  y=$1      A B C D
#    B  C
define(`_dblrotr', `splicep($1, $3, n$3p)splicec($2, n$1L, `R')D(
  `n$1L', $2)D(`n$2p', $1)splicec($3, n$1R, `L')D(`n$1R', $3)D(
  `n$3p', $1)Def(`n$3s', eval(size(n$3L)+size(n$3R)+1))Def(`n$2s',
  eval(size(n$2L)+size(n$2R)+1))Def(`n$1s', eval(n$2s+n$3s+1))')
define(`dblrotr', `_$0($1, $2, n$2p)')
#    z=$3         x
#  x=$1   C =>  A   z
# A  y=$2          y C
define(`_rotr', `splicep($1, $3, n$3p)D(`n$1R', $3)D(`n$3p',
  $1)D(`n$3L', $2)Def(`n$3s', eval(n$3s-n$1s+n$2s))Def(`n$1s',
  eval(n$1s-n$2s+n$3s))I($2, `', `', `D(`n$2p', $3)')')
define(`rotr', `_$0($1, n$1R, n$1p)')
define(`_dblrotl', `splicep($1, $3, n$3p)splicec($3, n$1L, `R')D(
  `n$1L', $3)D(`n$3p', $1)splicec($2, n$1R, `L')D(`n$1R', $2)D(
  `n$2p', $1)Def(`n$3s', eval(size(n$3L)+size(n$3R)+1))Def(`n$2s',
  eval(size(n$2L)+size(n$2R)+1))Def(`n$1s', eval(n$2s+n$3s+1))')
define(`dblrotl', `_$0($1, $2, n$2p)')
define(`_rotl', `splicep($1, $3, n$3p)D(`n$1L', $3)D(`n$3p',
  $1)D(`n$3R', $2)Def(`n$3s', eval(n$3s-n$1s+n$2s))Def(`n$1s',
  eval(n$1s-n$2s+n$3s))I($2, `', `', `D(`n$2p', $3)')')
define(`rotl', `_$0($1, n$1L, n$1p)')
define(`sibling', `I($2, `', `', n$2L, $1, `n$2R', `n$2L')')
define(`balance', `I($4, `', `rot$5($1)toggle($2)', parity($4), $3,
  `rot$5($1)toggle($2)', `dblrot$5($4, n$4p)toggle($4)toggle($1)toggle($2)')')
define(`_insbal_', `I($3$4, $4t$5, `_insbal($2, n$2p)', $3$4, $4$5,
  `balance($1, $2, $3, I($1, n$2L, `n$1R, `r'', `n$1L, `l''))')')
define(`_insbal', `toggle($1)I($2, `', `', `$0_($1, $2, n$1P, n$2P,
  parity(sibling($1, $2)))')')
define(`insbal', `_$0($1, n$1p)')
define(`_insert_', `Def(`n$3s', incr(n$3s))I(eval($1<=n$4s), 1,
  `I($4, `', `D(`n$3L', $2)D(`n$2p', $3)n$3R', `$0($1, $2, $4,
  n$4L)')', `I(n$3R, `', `D(`n$3R', $2)D(`n$2p', $3)$4',
  `$0(eval($1-n$4s-1), $2, n$3R, defn(`n'n$3R`L'))')')')
define(`_insert', `D(`n$2L')D(`n$2R')D(`n$2p')D(`n$2P',
  0)Def(`n$2s', 1)I($3, `', `D(`r', $2)', `I($0_($1, $2, $3,
  n$3L), `', `insbal(n$2p)')')')
# insert(pos, node)
define(`insert', `_$0($1, $2, r)')
define(`_at_', `I($3, 0, $2, $3, -1, `_at($1, n$2L)',
  `_at($1-size(n$2L)-1, n$2R)')')
define(`_at', `$0_($1, $2, cmp($1, size(n$2L)))')
# at(index) => node
define(`at', `_$0($1, r)')
define(`_pos', `I($1, `', `', `I($1, n$2R, `+size(n$2L)+1')$0($2, n$2p)')')
# pos(node) => index
define(`pos', `eval(size(n$1L)_$0($1, n$1p))')
define(`_succ', `I(n$1L, `', $1, `$0(n$1L)')')
define(`_succ_', `I($2, `', `', $1, n$2R, `$0($2, n$2p)', $2)')
define(`succ', `I(n$1R, `', `_$0_($1, n$1p)', `_$0(n$1R)')')
define(`swap', `splicep($2, $1, n$1p)splicec($2, n$1R, `R')D(
  `n$1R')splicec($2, n$1L, `L')D(`n$1L')D(`n$2P', n$1P)D(
  `n$1p')Def(`n$2s', n$1s)')
define(`fix3', `I($1, n$2L, `I(n$3P, parity(n$3R), `dblrotl(n$3L,
  $3)toggle($3)', `rotl($3)toggle($3)I(n$2L., .n$2R, `', `toggle($2)')')',
  `I(n$3P, parity(n$3L), `dblrotr(n$3R, $3)toggle($3)',
  `rotr($3)toggle($3)I(n$2L., .n$2R, `', `toggle($2)')')')')
define(`_bal3', `I(n$3P, n$2P, `toggle($2)I($4, `', `',
  $5, $6, `bal3($2, $4)')', n$3P.parity(n$3L), parity(n$3R).n$3P, `toggle(
  $2)toggle($3)I($4, `', `', $5, $6, `bal3($2, $4)')', `fix3($1, $2,
  $3)')')
define(`bal3', `_$0($1, $2, sibling($1, $2), n$2p, n$2P, parity(n$2p))')
define(`bal2', `I(n$1P, parity(n$1p), `toggle($1)bal3($1, n$1p)',
  `toggle($1)')')
define(`resize', `I($1, `', `', `Def(`n$1s',
  eval(size(n$1L)+size(n$1R)+1))$0(n$1p)')')
define(`rembal', `I($5, `', `', $1, $2, `bal3($4, $5)', $4.n$5L, .n$5R,
  `bal2($5)')resize($5)')
define(`_remove_', `I($3, `', `', `D(`n$3p', n$2p)')rembal(I($4,
  `', `0,1D(`r', $3)', `n$2P,n$4P`'I($2, n$4L, `D(`n$4L',
  $3)', `D(`n$4R', $3)')'), $2, $3, I($1, $2, $4, `swap($1,
  $2)I($1, $4, $2, $4)'))')
define(`_remove', `$0_($1, $2, I(n$2L, `', `n$2R', `n$2L'), n$2p)')
define(`remove', `_$0($1, I(n$1L, `', $1, n$1R, `', $1,
  `_succ(n$1R)'))D(`n$1L')D(`n$1R')D(`n$1p')D(`n$1P', 0)')
define(`_build_1', `D(`n$1L')D(`n$1R')D(`n$1P', 0)Def(`n$1s', 1)$1')
define(`_build_2', `D(`n$1L')D(`n$1R', $2)D(`n$1P', 1)Def(`n$1s',
  2)D(`n$2L')D(`n$2R')D(`n$2p', $1)D(`n$2P', 0)Def(`n$2s', 1)$1')
define(`_build_3', `D(`n$2L', $1)D(`n$1p', $2)D(`n$2R', $3)D(`n$3p',
  $2)D(`n$2P', defn(`t'n$3P))Def(`n$2s', eval(n$1s+n$3s+1))$2')
define(`_build', `I($1, $3, `$0_1($1)', $1, $2, `$0_2($1, $3)',
  `$0_3(build($1, decr($2)), $2, build(incr($2), $3))')')
define(`build', `_$0($1, eval(($1+$2)/2), $2)')

# Mostly useful for debugging my implementation:
define(`nop')
define(`dump', `errprintn(`node:$1 depth:$2 left:'n$1L` right:'n$1R(
  )` parent:'n$1p` parity:'n$1P` size:'n$1s)')
define(`_walk', `ifelse(`$1', `', `', `$3($1, $2)`'$0(n$1L, incr($2), `$3',
  `$4', `$5')$4($1, $2)`'$0(n$1R, incr($2), `$3', `$4', `$5')$5($1, $2)`'')')
# walk(pre, in, post) => preorder, inorder, postorder walk of tree
# each func takes node and depth parameters; nop and dump are predefined
define(`walk', `_$0(r, 0, $@)')

# Now to make use of the WAVL tree.
define(`a', `ifelse(a$1, 0, `', `insert(eval((a$1+pos($1))%''decr(cnt)``),
  remove($1)$1)')')
define(`_reset', `define(`a$1', eval((i$1$2%$3+$3)%$3))')
define(`reset', `define(`r', build(0, decr(cnt)))define(`n'r`p')forloop(0,
  decr(cnt), `_$0(', `, `$1', ''decr(cnt)``)')')
define(`coord3', `+i$1')
define(`coord2', `+i$1`'coord3(at(eval((pos($1)+1000)%cnt)))')
define(`coord1', `+i$1`'coord2(at(eval((pos($1)+1000)%cnt)))')
define(`coord', `coord1(at(eval((pos(z)+1000)%cnt)))')
define(`account', `output(2, `effort: 'effort)')

', `fatal(`unknown scan algorithm')')

define(`mix', `define(`effort', 0)reset(`$2')forloop(1, $1, `output(1, ...',
  `)forloop_arg(0, 'decr(cnt)`, `a')')account($1)')

mix(1, `')
define(`part1', eval(coord))

mix(10, `*'eval(811589153%(cnt-1)))
include(`math64.m4')
define(`part2', mul64(eval(coord), 811589153))

divert`'part1
part2
