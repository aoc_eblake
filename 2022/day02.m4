divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day02.input] day02.m4

include(`common.m4')ifelse(common(02), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`part1', 0)define(`part2', 0)
define(`do', `define(`part1', eval(part1`+($2+$1)%3*3+$1'))define(`part2',
eval(part2`+$1*3-($3-$1)%3'))')
define(`setup', `define(`$1', `do('$`1, $2, $3)')')
translit(include(defn(`file')), `XYZ 'nl, `123()'setup(`A', 0, 4)setup(`B',
2, 3)setup(`C', 1, 5))

ifelse(`dnl golfing variants, the newline after Y is essential
dnl part1
eval(translit(include(f),X Y
Z,1(2)3define(d,`define($1,+($2+$`1')%3*3+$`1')')d(A,0)d(B,2)d(C,1)))
dnl part2
eval(translit(include(f),X Y
Z,1(2)3define(d,`define($1,+$`1'*3-($2-$`1')%3)')d(A,4)d(B,3)d(C,5)))
')

divert`'part1
part2
