divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day03.input] day03.m4

include(`common.m4')ifelse(common(03), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

dnl Input is mixed case random letters.  Avoid one-letter macro names, and
dnl any macro names ending in a trailing digit without _, so that we can
dnl ensure that an arbitrary substr of the input is either a single byte or
dnl concatenated with a digit to avoid accidental macro expansion.
define(`part1', 0)define(`part2', 0)define(`cnt', 1)
define(`prep', `define(substr('dquote(defn(`alpha'))`, decr($1), 1)_,
$1)define(substr('dquote(defn(`ALPHA'))`, decr($1), 1)_, eval($1 + 26))')
forloop_arg(1, 26, `prep')
define(`filter', `translit(``$1'', `$2''dquote(defn(`alpha'))dquote(
defn(`ALPHA'))`, `$2')')
define(`do_1', `define(`part1', eval(part1 + substr(filter(substr(`$1', $2),
substr(`$1', 0, $2)), 0, 1)_))')
define(`do_2', `define(`part2', eval(part2 + substr(filter(filter(`$1', `$2'),
`$3'), 0, 1)_))define(`cnt', 1)')
define(`do', `$0_1(`$2', eval(len(`$2')/2))ifelse($1, 3, `$0_2(defn(`s_1'),
defn(`s_2'), `$2')', `define(`s_$1', `$2')define(`cnt', incr($1))')')

define(`input', translit(include(defn(`file')), nl, `0'))
ifdef(`__gnu__', `
  patsubst(defn(`input'), `\([^0]*\)0', `do(cnt, `\1')')
', `
  define(`_chew', `do(cnt, substr(`$1', 0, index(`$1', `0')))define(
    `tail', substr(`$1', incr(index(`$1', `0')))1)ifelse(index(defn(`tail'),
    `0'), -1, `', `$0(translit(dquote(defn(`tail')), 1))')')
  define(`chew', `ifelse(eval($1 < 100), 1, `_$0(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) - 1 + $1 - $1/2),
    translit(dquote(defn(`tail')), 1)substr(`$2', eval($1/2)))')')
  chew(len(defn(`input')), defn(`input'))
')

divert`'part1
part2
