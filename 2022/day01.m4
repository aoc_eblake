divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day01.input] day01.m4

include(`common.m4')ifelse(common(01), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`best1', 0)define(`best2', 0)define(`best3', 0)define(`acc', 0)
define(`swp', `ifelse(eval($2>$1), 1, `define(`$1', $2`'define(`$2', $1))')')
define(`do', `ifelse(`$1', `', `swp(`best1', `acc')swp(`best2', `acc')swp(
`best3', `acc')define(`acc', 0)', `define(`acc', eval(acc+$1))')')

define(`input', translit(include(defn(`file')), nl, `;');)
ifdef(`__gnu__', `
  patsubst(defn(`input'), `\([^;]*\);', `do(`\1')')
', `
  define(`_chew', `do(substr(`$1', 0, index(`$1', `;')))define(
    `tail', substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(`tail'),
    `;'), -1, `', `$0(defn(`tail'))')')
  define(`chew', `ifelse(eval($1 < 12), 1, `_$0(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  chew(len(defn(`input')), defn(`input'))
')
define(`part1', best1)
define(`part2', eval(best1 + best2 + best3))

ifelse(`dnl golfing variants, only the newline between `' is essential
dnl part1
define(_,`ifelse($2,,`eval($1',$3,,`_(eval(($2>$1)*$2+($2<=$1)*$1)',`_($1,
eval($3+$2)'),shift(shift(shift($@))))')_(0,translit(include(i),`
',`,'))
dnl both parts
define(_,`ifelse($4,,`$1 eval($1+$2+$3',$5,,`_(eval(($4>$1)*$4+($4<=$1)*$1),
eval(($4>$1)*$1+($4<=$1&$4>$2)*$4+($4<=$2)*$2),eval(($4>$2)*$2+($4<=$2&$4>$3)
*$4+($4<=$3)*$3)',`_($1,$2,$3,eval($5+$4)'),shift(shift(shift(shift(shift(
$@))))))')_(0,0,0,translit(include(f),`
',`,'))
')

divert`'part1
part2
