divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day17.input] [-Dlimit=N] day17.m4
# Optionally use -Dverbose=1 to see some progress

include(`common.m4')ifelse(common(17), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

ifdef(`limit', `', `define(`limit', 2022)')

define(`input', translit(include(defn(`file')), `<>'nl, `01'))
define(`n', 0)
define(`do', `define(`d$1', $2)define(`n', incr($1))')

ifdef(`__gnu__', `
  patsubst(defn(`input'), `.', `do(n, `\&')')
',`
  define(`chew', `ifelse($1, 1, `do(n, `$2')', `$0(eval($1/2), substr(
    `$2', 0, eval($1/2)))$0(eval($1-$1/2), substr(`$2', eval($1/2)))')')
  chew(len(defn(`input')), defn(`input'))
')
dnl bitmaps for rocks:                   aaaa_aaab_bbbb_bbcc_cccc_cddd_dddd
define(`mask0', eval(`0x8102040'))dnl    1   _   1_    _  1 _    _ 1  _
define(`mask1', eval(`0x204081'))dnl         _  1 _    _ 1  _    _1   _   1
define(`rock1', eval(`0x1e'))dnl             _    _    _    _    _   1_111
define(`rock2', eval(`0x20e08'))dnl          _    _ 010_    _111 _   0_10
define(`rock3', eval(`0x1021c'))dnl          _    _ 001_    _001 _   1_11
define(`rock4', eval(`0x2040810'))dnl      1 _    _ 1  _    _1   _   1_
define(`rock0', eval(`0xc18'))dnl            _    _    _    _11  _   1_1
define(`row0', eval(`0x7f'))dnl              _    _    _    _    _ 111_1111
define(`h', 0)define(`t', 0)

define(`d', `first(`jet'defn(`d'eval($1%'n`)))')
define(`get', `eval(defn(`row$2')`+0|(($1&0x1fffff)<<7)'), $2')
define(`jet0', mask0`,*')define(`jet1', mask1`,/')
define(`jet', `fall(eval(`$1$5(1+!($1&$4||($1$5 2)&$2))'), get($2, decr($3)),
  incr(t))')
define(`fall', `define(`t', $4)ifelse(eval(`$1&$2'), 0, `jet(`$1', $2, $3,
  d($4))', `settle(`$1', incr($3))')')
define(`_settle', `ifdef(`row$2', `', `define(`h', $2)')define(`row$2',
  defn(`row$2')`+0|($1)')')
define(`settle', `ifelse(`$1', 0, `', `_$0(`$1&0x7f', $2)$0(eval(`$1>>7'),
  incr($2))')')
define(`cycle', `output(1, `...cycle found between rocks $1/$5')define(`ch',
  eval(`$2-$6'))define(`cr', eval(`$1-$5'))define(`cs', $5)pushdef(`_rock')')
define(`_rock', `ifelse(eval(`$1%5+($3==$4)'), 0, `ifdef(`s$4', `cycle($@,
  s$4)', `define(`s$4', `$1,$2')')')')
define(`rock', `_$0($@)jet(defn(`rock'eval($1%5)), 0, eval(`$2+4'), d($4))')
define(`run', `forloop($1, $2, `rock(', `, h, t, eval(t%''n``))')')
run(1, limit)
define(`part1', h)

include(`math64.m4')
# div64 is not in math64.m4 because it is not fully generic; but it is good
# enough for the one large division needed in this puzzle
define(`bits', `_$0(eval($1, 2))')
define(`_bits', ifdef(`__gnu__', ``shift(patsubst($1, ., `, \&'))'',
  ``ifelse(len($1), 1, `$1', `substr($1, 0, 1),$0(substr($1, 1))')''))
define(`bits64', `ifelse(eval(len($1) < 10), 1, `bits($1)', `_$0(mul64($1,
  5)), eval(substr($1, decr(len($1))) & 1)')')
define(`_bits64', `bits64(substr($1, 0, decr(len($1))))')
define(`div64p', `ifelse($4, `', `$1,$2', `$0(_$0(add64($1, $1), add64(add64($2,
  $2), $4), $3), $3, shift(shift(shift(shift($@)))))')')
define(`_div64p', `ifelse(lt64($2, $3), 0, `add64($1, 1), sub64($2, $3)',
  `$1, $2')')
define(`remquo64', `ifelse(eval(`$1'), `$1', `eval(`$1/$2'),eval(`$1%$2')',
  `div64p(0, 0, $2, bits64(`$1'))')')

define(`search', `ifelse(ifdef(`cs', 1), 1, `', eval(t<2*n), 1,
  `run(incr(limit), define(`limit', eval(limit*2))limit)$0()')')
search()
ifdef(`cs', `', `fatal(`unable to detect cycle under limit 'limit)')
define(`rest', `run(incr(limit), eval(limit + $2))define(`part2',
  add64(mul64($1, ch), h))')
rest(remquo64(sub64(1000000000000, limit), cr))

divert`'part1
part2
