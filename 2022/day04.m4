divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day04.input] day04.m4

include(`common.m4')ifelse(common(04), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`part1', 0)define(`part2', 0)
define(`bump', `define(`$1', eval($1`+($2)'))')
define(`_do', `bump(`part1', `$1>=$3&$2<=$4|$3>=$1&$4<=$2')bump(`part2',
`$3<=$2&$4>=$1')')
define(`do', `_$0(translit(`$1', `-', `,'))')

define(`input', translit((include(defn(`file'))), nl`,()', `;-'))
ifdef(`__gnu__', `
  patsubst(defn(`input'), `\([^;]*\);', `do(`\1')')
', `
  define(`_chew', `do(substr(`$1', 0, index(`$1', `;')))define(
    `tail', substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(`tail'),
    `;'), -1, `', `$0(defn(`tail'))')')
  define(`chew', `ifelse(eval($1 < 25), 1, `_$0(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  chew(len(defn(`input')), defn(`input'))
')

divert`'part1
part2
