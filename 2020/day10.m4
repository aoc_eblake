divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day10.input] day10.m4

include(`common.m4')ifelse(common(10), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

include(`math64.m4')
define(`list', translit(include(file), nl, `;'))
ifdef(`__gnu__', `
  patsubst(defn(`list'), `\([^;]*\);', `pushdef(`val', `\1')')
',`
  define(`chew', `pushdef(`val', substr(`$1', 0, index(`$1', `;')))define(
    `tail',substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(`tail'),
    `;'), -1, `', `$0(defn(`tail'))')')
  define(`Split', `ifelse(eval($1 < 35), 1, `chew(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  Split(len(defn(`list')), defn(`list'))
')

define(`inc', `define(`$1', incr($1))')
define(`c1', 0)define(`c3', 0)define(`r', 0)define(`max', 0)define(`part2', 1)
define(`n', `ifdef(`n$1', 1, 0)')define(`n0')
define(`tr', `ifdef(`tr$1', `', `define(`tr$1', eval(tr(decr($1)) +
  tr(decr(decr($1))) + tr(eval($1 - 3))))')tr$1`'')
define(`tr0', 1)define(`tr1', 1)define(`tr2', 2)
define(`prep', `ifelse(eval($1 > max), 1, `define(`max', $1)')define(`n$1')')
stack_foreach(`val', `prep')
define(`l', `ifdef(`n$1', `ifdef(`n'incr($1), `inc(`r')inc(`c1')',
  `inc(`c3')define(`part2', mul64(part2, tr(r)))define(`r', 0)')')')
forloop_arg(0, max, `l')
define(`part1', eval(c1 * c3))

divert`'part1
part2
