divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day11.input] [-Dhashsize=H] day11.m4

include(`common.m4')ifelse(common(11, 65537), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`list', translit(include(defn(`file')), `.'nl, `FR'))
define(`x', 1)define(`y', 1)
define(`maxx', incr(index(list, `R')))
define(`maxy', incr(len(translit(list, `FL'))))
define(`coord', `eval($1 + $2 * 'maxx`)')
define(`F_', `define(`x', incr(x))')
define(`L_', `pushdef(`seat', coord(x, y))define(`s_'coord(x, y))F_()')
define(`R_', `define(`x', 1)define(`y', incr(y))')
ifdef(`__gnu__', `
  patsubst(defn(`list'), `.', `\&_()')
',`
  define(`split', `ifelse($1, 1, `$2_()', `$0(eval($1/2), substr(`$2', 0,
    eval($1/2)))$0(eval($1-$1/2), substr(`$2', eval($1/2)))')')
  split(len(defn(`list')), defn(`list'))
')

# States:
# 1 - empty, but still live
# 2 - empty and locked (at least one neighbor in state 3)
# 3 - occupied and locked (less than four neighbors in state 1, 3, 4)
# 4 - occupied, but still live
define(`ident', `$1')
define(`_s', `s(eval($1 $2), `$2')')
define(`find', `define(`s_$1', 1)define(`n_$1', _s($1, -$2-1)_s($1, -$2)_s($1,
  -$2+1)_s($1, -1)_s($1, +1)_s($1, +$2-1)_s($1, +$2)_s($1, +$2+1))')
define(`check', `ifelse(_$0($1, s_$1), -, ``s'', $2)')
define(`_check', `check$2($1, n_$1)')
define(`check2', `-')
define(`check3', `-')
define(`check1', `ifelse(translit($2, 12), `', `pushdef(`act', `define(`s_$1',
  eval(3 + (len(translit($2, 2)) >= 4)))')', index(translit($2, 124), 3), 0,
  `pushdef(`act', `define(`s_$1', 2)')')')
define(`check4', `ifelse(index(translit($2, 3412, --), full), 0,
  `pushdef(`act', `define(`s_$1', 1)')', eval(len(translit($2, 2)) < len(
  full)), 1, `pushdef(`act', `define(`s_$1', 3)')')')
define(`visit', `ifdef(`$1', `pushdef(`seat'check($1, $2),
  $1)popdef(`$1')visit($@)')')
define(`do', `ifdef(`act', `act`'popdef(`act')do()')')
define(`round', `visit(`seat$2', incr($2))ifdef(`act', `do()round($1,
  incr($2))', `incr($2)')')
define(`compute', `stack_reverse(`seat', `seat0', `find(seat, 'maxx`)')define(
  `full', `$2')define(`part$1', len(translit(stack_reverse(`seat'round($1, 0),
  `seat', `defn(`s_'seat)')stack_reverse(`seats', `seat', `defn(`s_'seat)'),
  12)))')

define(`s', `ifdef(`s_$1', ``s_$1`''')')
compute(1, `----')
define(`s', `ifelse(eval($1%'maxx` == 0 || ($1/'maxx`)%'maxy` == 0), 1, `',
  `ifdef(`s_$1', ``s_$1`''', `s(eval($1 $2), `$2')')')')
compute(2, `-----')

divert`'part1
part2
