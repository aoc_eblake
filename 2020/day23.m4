divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day23.input] [-Dmax=N] [-Dmoves=M] [-Dhashsize=H] \
# [-Dverbose=V] day23.m4

include(`common.m4')ifelse(common(23, 2000003), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

ifdef(`max', `', `define(`max', 1000000)')
ifdef(`moves', `', `define(`moves', max`0')')

include(`math64.m4')
define(`input', translit(include(defn(`file')), nl))
define(`mod', len(input))
define(`_prep', `define(`n'substr(input, $1, 1), substr(input,
  eval(($1+1)%'mod`), 1))')
define(`prep', `forloop_arg(0, mod-1, `_prep')define(`c', substr(input, 0,
  1))define(`b1', $1)')
define(`I', defn(`incr'))
define(`i', defn(`ifdef'))
define(`D', defn(`define'))
define(`E', defn(`ifelse'))
define(`X', defn(`index'))

output(1, `...prep')
define(`P', `D(`n$1'0,`$1'1)D(`b$1'1,`$1'0)D(`n$1'1,`$1'2)D(`b$1'2,`$1'1)'dnl
`D(`n$1'2,`$1'3)D(`b$1'3,`$1'2)D(`n$1'3,`$1'4)D(`b$1'4,`$1'3)'dnl
`D(`n$1'4,`$1'5)D(`b$1'5,`$1'4)D(`n$1'5,`$1'6)D(`b$1'6,`$1'5)'dnl
`D(`n$1'6,`$1'7)D(`b$1'7,`$1'6)D(`n$1'7,`$1'8)D(`b$1'8,`$1'7)'dnl
`D(`n$1'8,`$1'9)D(`b$1'9,`$1'8)D(`n$1'9,`$2'0)D(`b$2'0,`$1'9)')
define(`p', `E(`$1',`100000',,`P($@)p(`$2',n$2)')')
P(`', 1)
p(1, 2)

define(`d', `E(X(`.$2.$3.$4.',`.$1.'),-1,`$1',`d(b$1,$2,$3,$4)')')
define(`N', `n$1,$@')
define(`r', `i(`r$1',`r$1',`R')($1,N(N(N($2))))')
define(`R', `S($1,d(b$5,$4,$3,$2),$5,$4,N($2))')
define(`S', `D(`n$6',n$2)D(`n$2',$4)D(`n$3',$5)r(I($1),$5)')

prep(mod)
define(`dump', `_$0(n1)')
define(`_dump', `ifelse($1, 1, `', `$1$0(n$1)')')
define(`r100')
r(0, c)
define(`part1', dump)popdef(`r100')

prep(max)
define(`n'substr(input, decr(mod)), 10)
define(`n'max, c)
define(`rename', `ifdef(`$2', `', `define(`$2', defn(`$1'))')popdef(`$1')')
define(`r0', `output(1, `...$1')rename(`$0', `r'eval($1 + 100000))R($@)')
define(`r'moves)
r(0, c)
define(`part2', mul64(N(n1)))

divert`'part1
part2
