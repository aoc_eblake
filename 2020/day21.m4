divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day21.input] day21.m4

include(`common.m4')ifelse(common(21), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`n_', 0)
define(`lexlt', `_$0(substr(`$1', 0, 1), substr(`$2', 0, 1), substr(`$1', 1),
  substr(`$2', 1))')
define(`_lexlt', `ifelse(`$1', `$2', `lexlt(`$3', `$4')', `eval(index(-'alpha`,
  `$1') < index(-'alpha`, `$2'))')')
define(`sort', `ifelse(`$2', `', ``,$1'', `ifelse(lexlt(`$1', `$2'), 1,
  ``,$@'', ``,$2'sort(`$1', shift(shift($@)))')')')
define(`row', `_row(translit(`$1', ` ()', `,'))define(`n_', incr(n_))')
define(`_row', `ifelse(`$1', `', `', `pushdef(`r'n_`_i', `$1')$0(shift($@))')')
define(`contains', `_$0('))
define(`_contains', `foreach(`allergen', $@)))row(')
define(`allergens')
define(`allergen', `ifdef(`a_$1', `', `define(`allergens',
  sort(`$1'allergens))')pushdef(`a_$1', n_)')
row(include(defn(`file')))define(`n_', decr(n_))

define(`whiledef', `ifdef(`$1', `$2`'popdef(`$1')$0($@)')')
define(`settle', `ifdef(`e_$1', `', `define(`cnt', 0)_stack_foreach(`a_$1',
  `_$0(', `)', `t2')ifelse(whiledef(`set', `ifelse('cnt`, defn(`s_'set),
  `ifdef(`match', `-', `define(`match', set)')')undefine(`s_'set)'), `',
  `define(`e_$1', match)define(`e_'match, `$1')', `define(`done',
  0)')undefine(`match')')')
define(`_settle', `define(`cnt', incr(cnt))_stack_foreach(`r$1_i',
  `bump(', `)', `t3')')
define(`bump', `ifdef(`e_$1', `', `ifdef(`s_$1', `define(`s_$1', incr(s_$1))',
  `pushdef(`set', `$1')define(`s_$1', 1)')')')
define(`do', `define(`done', 1)
foreach(`settle', shift(allergens))
ifelse(done, 0, `$0()')')do()
define(`part1', 0)
define(`count', `stack_foreach(`r$1_i', `_$0')')
define(`_count', `ifdef(`e_$1', `', `define(`part1', incr(part1))')')
forloop_arg(0, n_ - 1, `count')
define(`part2', quote(shift(_foreach(`,defn(`e_'', `)', allergens))))

divert`'part1
part2
