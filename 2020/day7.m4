divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day7.input] day7.m4

include(`common.m4')ifelse(common(7), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`contain')define(`no')define(`other')define(`bag')define(`bags')
define(`list', quote(translit((include(file)), `,()'nl)))
ifdef(`__gnu__', `
  define(`list', patsubst((defn(`list')), `\([a-z]+\) \([a-z]+\)',
    `\1_\2,'))
  define(`list', patsubst(defn(`list'), `\([0-9]\) \(\w+\),', `(\1, \2),'))
  patsubst(defn(`list'), `\(\w+\), *\([^.]*\)\.', `pushdef(`names',
    `\1')define(`n_\1', `\2')')
',`
  define(`node', `pushdef(`names', `$1_$2')define(`n_$1_$2',
    quote(collect(shift(shift($@)))))')
  define(`collect', `ifelse(`$#', 1, `', len(`$1'), 1,
    `($1, `$2_$3'),$0(shift(shift(shift($@))))', `$0(shift($@))')')
  define(`chew', `node(translit(substr(`$1', 0, index(`$1', `.')), ` ',
    `,'))define(`tail',substr(`$1', incr(index(`$1', `.'))))ifelse(index(defn(
    `tail'), `.'), -1, `', `$0(defn(`tail'))')')
  define(`split', `ifelse(eval($1 < 170), 1, `chew(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  split(len(defn(`list')), defn(`list'))
')

define(`g_shiny_gold', +0)
define(`check', `ifelse(`$1', `', `', `_$0$1')')define(`_check', `good(`$2')')
define(`good', `ifdef(`g_$1', `', `define(`g_$1', ifelse(foreach(`check',
  n_$1), `', `', +1))')g_$1')
define(`part1', eval(stack_foreach(`names', `good')))
define(`holds', `ifelse(`$1', `', `', `_$0$1')')
define(`_holds', `+ $1 * count(`$2')')
define(`count', `ifdef(`c_$1', `', `define(`c_$1', eval(1 foreach(`holds',
  n_$1)))')c_$1')
define(`part2', decr(count(`shiny_gold')))

divert`'part1
part2
