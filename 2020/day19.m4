divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day19.input] day19.m4

include(`common.m4')ifelse(common(19), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`part1', 0)define(`part2', 0)
define(`input', include(defn(`file')))
ifdef(`__gnu__', `
dnl Regex solution
define(`getrule', `define(`r$1', quote(translit(`$2', ` ', `,')))')
define(`match', `define(`$1', incr($1))')
patsubst(defn(`input'), `^\([0-9]*\): \(.*\)', `getrule(\1, \2)')
define(`patt', `ifdef(`p$1', `', `define(`p$1', build(r$1))')defn(`p$1')')
define(`build', `ifelse(`$1', `"a"', ``a'', `$1', `"b"', ``b'',
  ``\('_$0($@)`\)'')')
define(`_build', `ifelse(`$1', `', `_', `$1', `|', ``\|'',
  `patt($1)')$0(shift($@))')
define(`__build')
patsubst(defn(`input'), ^patt(0)$, `match(`part1')')
# Hard-coded to 0: 8 11, 8: 42 8, 11: 42 31 | 42 11 31, with no other 8/11
define(`check', `ifelse(eval(len(patsubst(`$2', patt(42), -)) >
  len(patsubst(substr(`$1', len(`$2')), patt(31), -))), 1, `match(`part2')')')
ifdef(`r42', `patsubst(defn(`input'), `^\('patt(42)`+\)'patt(31)`+$',
  `check(`\&', `\1')')', `define(`part2', ``not possible'')')
', `
dnl Recursive match solution
define(`_chew', `$2(substr(`$1', 0, index(`$1', nl)))define(`tail',
  substr(`$1', incr(index(`$1', nl))))ifelse(index(defn(`tail'), nl), -1,
  `', `$0(defn(`tail'), `$2')')')
define(`chew', `ifelse(eval($1 < 400), 1, `_$0(`$2', `$3')', `$0(eval($1/2),
  substr(`$2', 0, eval($1/2)), `$3')$0(eval(len(defn(`tail')) + $1 - $1/2),
  defn(`tail')substr(`$2', eval($1/2)), `$3')')')
define(`do', `chew(len(`$1'), `$1', `$2')')
define(`p', `),(')
define(`getrule', `define(`r$1', translit(((translit(`$2', `| ', `p,'))),
  `()', ``''))')
define(`ruleline', `getrule(substr(`$1', 0, index(`$1', `:')), substr(`$1',
  incr(incr(index(`$1', `:')))))')
do(substr(defn(`input'), 0, incr(index(defn(`input'), nl()nl))), `ruleline')
define(`match', `ifelse(`$1', `x', `x', `_foreach(`_$0(`$1', first(',
  `), shift(shift($@)))', `', r$2)')')
define(`_match', `ifelse(good, `-', `', `$1$2$#', 2, `define(`good', `-')',
  `$2$#', `2', `x', `$2', `', `$0(`$1', shift(shift($@)))', substr($2, 0, 1),
  ", `ifelse(substr($1, 0, 1), substr($2, 1, 1), `$0(substr($1, 1),
  shift(shift($@)))', `x')', `match($@)')')
define(`matchline', `define(`good', `')ifelse(`$1', `', `',
  `translit(match(`$1', 0), `x')')good()')
define(`part1', len(do(substr(defn(`input'), index(defn(`input'), nl()nl)),
  `matchline')))
pushdef(`r8', ``42',`42,8'')pushdef(`r11',``42,31',`42,11,31'')
define(`part2', len(do(substr(defn(`input'), index(defn(`input'), nl()nl)),
  `matchline')))
')

divert`'part1
part2
