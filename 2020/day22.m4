divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day22.input] day22.m4

include(`common.m4')ifelse(common(22), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

# 'f'ront, 'b'ack', 't'ortoise, 'h'are, 'c'opy
define(`Player')
define(`input', translit(include(defn(`file')), `:'))
define(`init', `_$0(`$1t')_$0(`$1h')ifelse($1, 1, `_$0(`$1c')')')
define(`_init', `define(`p1_$1f', 0)define(`p1_$1b', 0)define(`p2_$1f',
  0)define(`p2_$1b', 0)')
define(`grow', `define(`p$1_$2_'p$1_$2b, $3)define(`p$1_$2b', incr(p$1_$2b))')
define(`grab', `defn(`p$1_$2_'p$1_$2f)popdef(`p$1_$2_'p$1_$2f)define(`p$1_$2f',
  incr(p$1_$2f))')
define(`deal', `ifelse(`$2', `', `', `grow($1, `1t', $2)grow($1, `1h',
  $2)grow($1, `1c', $2)$0($1, shift(shift($@)))')')
define(`match', `ifelse(eval(p1_$1$2b - p1_$1$2f == p1_$1hb - p1_$1hf &&
  p2_$1$2b - p2_$1$2f == p2_$1hb - p2_$1hf), 1, `ifelse(_$0(p1_$1$2f,
  p1_$1hf, p1_$1$2b, `p1_$1', `$2')_$0(p2_$1$2f, p2_$1hf, p1_$1$2b, `p2_$1',
  `$2'), `', 1, 0)', 0)')
define(`_match', `ifelse($1, $3, `', defn(`$4$5_$1'), defn(`$4h_$2'),
  `$0(incr($1), incr($2), shift(shift($@)))', `-')')
define(`fullround', `ifelse(round(`$1', `h', eval($2 * 2 - 1))round(`$1', `h',
  eval($2 * 2))round(`$1', `t', $2), `', `ifelse(match($1, `t'), 1,
  `ifelse($1, 1, `finish($1, 0, eval($2 * 2 + 1))')define(`g$1', 1)1', `$0($1,
  incr($2))')', `g$1`'')')
define(`finish', `ifelse(match($1, `c'), 1, `', `round(`$1', `h', $3)round(`$1',
  `c', $2)$0($1, incr($2), incr($3))')')
define(`over', `ifelse(p1_$1$2f, p1_$1$2b, `define(`g$1', 2)2', p2_$1$2f,
  p2_$1$2b, `define(`g$1', 1)1')')
define(`round', `ifelse(over($@), `', `_$0(grab(1, `$1$2'), grab(2, `$1$2'),
  $@)', `g$1`'')')
define(`_round', `ifelse(compare($@), 1, `grow(1, `$3$4', $1)grow(1, `$3$4',
  $2)', `grow(2, `$3$4', $2)grow(2, `$3$4', $1)')')
define(`score', `ifelse(p$1_1hf, p$1_1hb, `', `define(`$2',
  eval($2 + (p$1_1hb - p$1_1hf) * grab($1, `1h')))score($@)')')
define(`prep', `init(`1')deal(translit(substr(input, 0, index(input, nl()nl)),
  nl, `,'))deal(translit(substr(input, index(input, ` 2')), nl, `,'))')

define(`compare', `eval($1 > $2)')
prep()define(`part1', 0)score(fullround(1, 1), `part1')
define(`game', 1)
define(`compare', `ifelse(eval($1 <= p1_$3$4b - p1_$3$4f && $2 <= p2_$3$4b -
  p2_$3$4f), 1, `ifelse(`$4', `h', `define(`game', incr(game))define(`g$3_$5',
  game)recurse($1, $2, $3, game)', `defn(`g'g$3_$5)')', `eval($1 > $2)')')
define(`copy', `ifelse($1, 0, `$6', `$0(decr($1), $2, $3, incr($4), $5,
  _$0($2, $5, p$2_$3h_$4, $6))')')
define(`_copy', `grow($1, `$2h', $3)grow($1, `$2t', $3)ifelse(eval($3 > $4),
  1, $3, $4)')
define(`cleanup', `_$0(1, `$1h')_$0(1, `$1t')_$0(2, `$1h')_$0(2, `$1t')')
define(`_cleanup', `ifelse(p$1_$2f, p$1_$2b, `popdef(`p$1_$2f')popdef(
  `p$1_$2b')', `ifelse(grab($1, `$2'))$0($@)')')
define(`recurse', `init($4)ifelse(eval(copy($1, 1, $3, p1_$3hf, $4, 0) >
  copy($2, 2, $3, p2_$3hf, $4, 0)), 1, `define(`g$4', 1)1', `fullround($4,
  1)')cleanup($4)')
prep()define(`part2', 0)score(fullround(1, 1), `part2')

divert`'part1
part2
