divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day24.input] [-Dhashsize=H] [-Dverbose=V] \
# [-Dmemoize=1] day24.m4

include(`common.m4')ifelse(common(24, 65537), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')
ifdef(`memoize', `', `ifelse(eval(defn(`foundhash') + 0 > 65536), 1,
  `define(`memoize', 1)')')

define(`width', eval(150*4))define(`offset', eval(width*150))define(`part1', 0)
define(`list', translit(include(defn(`file')), nl, `r'))
define(`reset', `define(`x', 'offset`)')
reset()
define(`n_', `define(`dir1', `N')')
define(`s_', `define(`dir1', `S')')
define(`e_', `dir(defn(`dir1')`E')')
define(`w_', `dir(defn(`dir1')`W')')
define(`dir', `dir$1()popdef(`dir1')')
define(`dirE', `define(`x', incr(incr(x)))')
define(`dirSE', `define(`x', eval(x + 1 - 'width`))')
define(`dirSW', `define(`x', eval(x - 1 - 'width`))')
define(`dirW', `define(`x', decr(decr(x)))')
define(`dirNW', `define(`x', eval(x - 1 + 'width`))')
define(`dirNE', `define(`x', eval(x + 1 + 'width`))')
define(`r_', `flip(x)reset()')
define(`flip', `define(`t$1', ifdef(`t$1', `ifelse(t$1, 0, 1, 0)',
  `pushdef(`tiles', `$1')1'))')
ifdef(`__gnu__', `
  patsubst(defn(`list'), `.', `\&_()')
',`
  define(`split', `ifelse($1, 1, `$2_()', `$0(eval($1/2), substr(`$2', 0,
    eval($1/2)))$0(eval($1-$1/2), substr(`$2', eval($1/2)))')')
  split(len(defn(`list')), defn(`list'))
')
define(`prune', `ifelse(t$1, 0, `popdef(`tiles')popdef(`t$1')',
  `define(`part1', incr(part1))')')
stack_foreach(`tiles', `prune')
define(`whiledef', `ifdef(`$1', `$2($1)popdef(`$1')$0($@)')')
define(`bump', `define(`c$1', ifdef(`c$1', `c$1', `pushdef(`near', `$1')')-)')
ifelse(defn(`memoize'), 1, `
  output(1, `using memoized neighbor lookup')
  define(`bumpall', `ifdef(`b$1', `', `getb($1, ''width``)')b$1()')
  define(`getb', `define(`b$1', `bump($1)bump('incr(incr($1))`)bump('eval(
  $1 + 1 - $2)`)bump('eval($1 - 1 - $2)`)bump('decr(decr($1))`)bump('eval(
  $1 - 1 + $2)`)bump('eval($1 + 1 + $2)`)')')
', `
  output(1, `using dynamic neighbor lookup')
  define(`bumpall', `bump($1)bump(incr(incr($1)))bump(eval(
    $1 + 1 - 'width`))bump(eval($1 - 1 - 'width`))bump(decr(decr($1)))bump(
    eval($1 - 1 + 'width`))bump(eval($1 + 1 + 'width`))')
')
define(`adjust', `ifelse(c$1, --, `pushdef(`tiles', `$1')define(`t$1', 1)',
  c$1`'t$1, ---1, `pushdef(`tiles', `$1')', `popdef(`t$1')')popdef(`c$1')')
define(`day', `whiledef(`tiles', `bumpall')whiledef(`near', `adjust')')
forloop_arg(1, 100, `day')
define(`count', `-')
define(`part2', len(stack_foreach(`tiles', `count')))

divert`'part1
part2
