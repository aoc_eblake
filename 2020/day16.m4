divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day16.input] day16.m4

include(`common.m4')ifelse(common(16), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

include(`math64.m4')
define(`or', `:')define(`part1', 0)define(`part2', 1)define(`slot', 0)
define(`input', translit((include(defn(`file'))), `-,'nl`() ', `::;'))
define(`valid', `_$0(slot, translit(`$1', `:', `,'))')
define(`_valid', `define(`s'eval(1<<$1), `$2')forloop($3, $4, `allow(',
  `, $1)')forloop($5, $6, `allow(', `, $1)')define(`slot', incr($1))')
define(`allow', `define(`v$1', eval((defn(`v$1')+0) | (1<<$2)))')
define(`check', `narrow(foreach(`_$0', translit(`$1', `:', `,')))')
define(`_check', `, ifdef(`v$1', `v$1', `-define(`part1', eval(part1 + $1))')')
define(`narrow', `ifelse(index(`$*', -), -1, `_$0(0$@)')')
define(`_narrow', `define(`n$1', eval(n$1 & $2))ifelse(`$#', 2, `',
  `$0(incr($1), shift(shift($@)))')')
ifdef(`__gnu__', `
  define(`do', `patsubst(`$1', `\([^;]*\);', `$2(`\1')')')
',`
  define(`_chew', `$2(substr(`$1', 0, index(`$1', `;')))define(`tail',
    substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(`tail'), `;'), -1,
    `', `$0(defn(`tail'), `$2')')')
  define(`chew', `ifelse(eval($1 < 160), 1, `_$0(`$2', `$3')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)), `$3')$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)), `$3')')')
  define(`do', `chew(len(`$1'), `$1', `$2')')
')
do(substr(defn(`input'), 0, incr(index(defn(`input'), `;;'))), `valid')
forloop(0, decr(slot), `define(`n'', `, 'eval((1 << slot) - 1)`)')
do(substr(defn(`input'), eval(index(defn(`input'), `kets:;') + 6)), `check')
forloop(0, decr(slot), `forloop_var(`i', 0, decr(slot), `ifdef(`s'defn(`n'i),
  `define(`l'i, defn(`n'i))forloop_var(`j', 0, decr(slot), `define(`n'j,
  eval(defn(`n'j) & ~defn(`l'i)))')')')')
define(`tally', `_$0(0, translit(`$1', `:', `,'))')
define(`_tally', `ifelse(index(defn(`s'l$1), `depart'), 0, `define(`part2',
  mul64(part2, $2))')ifelse(`$#', 2, `', `$0(incr($1), shift(shift($@)))')')
tally(substr(defn(`input'), eval(index(defn(`input'), `ket:;') + 5),
  eval(index(defn(`input'), `;;nea') - index(defn(`input'), `ket:;') - 5)))

divert`'part1
part2
