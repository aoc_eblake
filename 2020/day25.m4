divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day25.input] day25.m4

include(`common.m4')ifelse(common(25), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

# Using optimizations from reddit:
# https://www.reddit.com/r/adventofcode/comments/kkq6r3/2020_optimized_solutions_in_c_291_ms_total/gh730cw
# https://github.com/Voltara/advent2020-fast/blob/main/src/day25.cpp
# Combines baby-step/giant-step, Pohlig-Hellman, and Chinese Remainder
# 7^k = input (mod 20201227) =>
# (7^m)^a*7^b = input (mod 20201227)
# 20201226 = 2 * 3 * 29 * 116099
define(`M', 20201227)define(`M1', decr(M))
define(`bits', `_$0(eval($1, 2))')
define(`_bits', ifdef(`__gnu__', ``shift(patsubst($1, ., `, \&'))'',
  ``ifelse(len($1), 1, `$1', `substr($1, 0, 1),$0(substr($1, 1))')''))
define(`modmul', `define(`d', 0)define(`b', $2)pushdef(`m', $3)foreach(`_$0',
  bits($1))popdef(`m')d`'')
define(`_modmul', `define(`d', eval((d * 2 + $1 * b) % m))')
define(`modpowM', `define(`r', 1)define(`a', $1)foreach(`_$0', bits($2))r`'')
define(`_modpowM', `define(`r', ifelse($1, 1, `modmul(modmul(r, r, M), a, M)',
  `modmul(r, r, M)'))')

# Compute the lookup table for baby-step/giant-step
define(`br', 1)
define(`setup', `define(`D'br, $1)define(`br', modmul(br, 19372176, M))')
forloop_arg(0, 340, `setup')
define(`dlog116099', `_$0(0, modpowM($1, 174))')
define(`_dlog116099', `ifdef(`D$2', `eval($1 * 341 + D$2)', `$0(incr($1),
  modmul($2, 16585664, M))')')
# Precomputed lookups
define(`idx', 0)
define(`setup', `define(`dlog29_$1', idx)define(`idx', incr(idx))')
foreach(`setup', 1, 176, 100, 0, 140, 48, 125, 117,
  76, 190, 244, 161, 206, 177, 24, 128,
  158, 59, 20, 232, 250, 195, 106, 105,
  220, 21, 102, 28, 26)
define(`dlog29', `defn(`$0_'eval(modpowM($1, 696594) % 255))')
define(`dlog3', `eval(modpowM($1, 6733742) >> 5 & 3)')
define(`dlog2', `eval(modpowM($1, 10100613) != 1)')
# CRT over factors of M1, with partial products precomputed
define(`dlog', `eval((modmul(18227544, dlog116099($1), M1) +
  modmul(18808038, dlog29($1), M1) +
  modmul(13467484, dlog3($1), M1) +
  modmul(10100613, dlog2($1), M1)) % M1)')

define(`prep', `define(`C', $1)define(`D', $2)')
prep(translit(include(defn(`file')), nl, `,'))
define(`part1', modpowM(C, dlog(D)))

divert`'part1
no part2
