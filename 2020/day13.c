#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <assert.h>

long mul(long a, long b, long m) {
  long d = 0, mp2 = m >> 1;
  int i;
  if (a < 0)
    a += m;
  if (b < 0)
    b += m;
  assert(m < 0x4000000000000000ULL);
  assert(a < m && b < m);
  for (i = 0; i < 63; i++) {
    d = (d > mp2) ? (d << 1) - m : d << 1;
    if (a & 0x4000000000000000ULL) d += b;
    if (d >= m) d -= m;
    a <<= 1;
  }
  return d;
}
void ext(int a, int b, int *m, int *n) {
  int old_r = a, r = b;
  int old_s = 1, s = 0;
  int old_t = 0, t = 1;
  int tmp;
  while (r) {
    int q = old_r / r;
    tmp = r;
    r = old_r - q * r;
    old_r = tmp;
    tmp = s;
    s = old_s - q * s;
    old_s = tmp;
    tmp = t;
    t = old_t - q * t;
    old_t = tmp;
  }
  *m = old_s;
  *n = old_t;
}
int main() {
  int time, best, i, n = 0, idx = 0;
  char c[5];
  int num[10], rem[10];
  int prod1 = 1, prod2 = 1, m;
  int rem1 = 0, rem2 = 0;
  long res;
  scanf("%d ", &time);
  best = time;
  while (scanf("%[x0123456789],", c) > 0) {
    n++;
    if (*c == 'x')
      continue;
    i = atoi(c);
    if ((i - time % i) < (best - time % best))
      best = i;
    num[idx] = i;
    rem[idx] = (5 * i - n + 1) % i;
    idx++;
  }
  for (i = 0; i < idx; i++) {
    int *p = prod2 < prod1 ? &prod2 : &prod1;
    int *r = prod2 < prod1 ? &rem2 : &rem1;
    if (*p == 1) {
      *p = num[i];
      *r = rem[i];
    } else {
      ext(*p, num[i], &m, &n);
      *r = (mul(mul(*r, n, *p * num[i]), num[i], *p * num[i]) +
            mul(mul(rem[i], m, *p * num[i]), *p, *p * num[i])) % (*p * num[i]);
      *p *= num[i];
    }
  }
  ext(prod1, prod2, &m, &n);
  res = mul(mul(rem1, n, 1L * prod1 * prod2), prod2, 1L * prod1 * prod2) +
    mul(mul(rem2, m, 1L * prod1 * prod2), prod1, 1L * prod1 * prod2);
  printf("%d %ld\n", (best - time % best) * best, res);
  return 0;
}
