divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day8.input] day8.m4

include(`common.m4')ifelse(common(8), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`list', translit(include(file), nl, `;'))
ifdef(`__gnu__', `
  patsubst(defn(`list'), `\(\w*\) \([^;]*\);', `pushdef(`inst', `\1(\2)')')
',`
  define(`line', `pushdef(`inst', `$1($2)')')
  define(`chew', `line(translit(substr(`$1', 0, index(`$1', `;')), ` ',
    `,'))define(`tail',substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(
    `tail'), `;'), -1, `', `$0(defn(`tail'))')')
  define(`split', `ifelse(eval($1 < 20), 1, `chew(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  split(len(defn(`list')), defn(`list'))
')
define(`pc', 0)
define(`prep', `define(`p'pc, `$1')define(`pc', incr(pc))')
stack_foreach(`inst', `prep')
define(`max', pc)

define(`acc', `define(`accum', eval(accum + $1))next(1)')
define(`nop', `next(1)')
define(`jmp', `next($1)')
define(`next', `latch()run(eval(pc + $1))')
define(`run', `define(`pc', $1)ifelse(eval($1 < 'max`)check($1, $2), 11,
  `p$1()')')

define(`latch', `define(`w'pc, 1)')
define(`check', `ifdef(`w$1', $2, 1)')
define(`accum', 0)run(0)
define(`part1', accum)
define(`latch', `define(`w'pc, 2)')
define(`swap', `ifelse(`$2', `a', `', defn(`w$1'), 1, `1pushdef(`p$1',
  ifelse(`$2', `n', ``jmp'', ``nop'')substr(defn(`p$1'), 3))')')
define(`done', `define(`check', 1)define(`accum', 0)run(0)define(`part2',
  accum)pushdef(`try')')
define(`try', `ifelse(swap($1, substr(defn(`p$1'), 0, 1)), 1,
  `run($1, 1)ifelse(eval(pc >= max), 1, `done`'')popdef(`p$1')')')')
forloop_arg(0, max, `try')

divert`'part1
part2
