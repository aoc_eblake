divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day12.input] day12.m4

include(`common.m4')ifelse(common(12), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`list', translit(include(file), nl, `;'))
define(`act', `$1_1($2)$1_2($2)')
ifdef(`__gnu__', `
  define(`do', `patsubst(defn(`list'), `\([A-Z]\)\([^;]*\);', `act(`\1',\2)')')
',`
  define(`chew', `act(substr(`$1', 0, 1), (substr(`$1', 1, decr(index(`$1',
    `;')))))define(`tail', substr(`$1', incr(index(`$1', `;'))))ifelse(index(
    defn(`tail'), `;'), -1, `', `$0(defn(`tail'))')')
  define(`split', `ifelse(eval($1 < 10), 1, `chew(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  define(`do', `split(len(defn(`list')), defn(`list'))')
')
define(`abs', `ifelse(index($1, -), 0, `substr($1, 1)', $1)')

define(`x', 0)define(`y', 0)
define(`dir', `E')
define(`N_1', `define(`y', eval(y + $1))')
define(`E_1', `define(`x', eval(x + $1))')
define(`S_1', `define(`y', eval(y - $1))')
define(`W_1', `define(`x', eval(x - $1))')
define(`F_1', `dir()_1($1)')
define(`R_1', `define(`dir', substr(`ESWNESWN',
  eval(index(`ESWN', dir) + $1/90), 1))')
define(`L_1', `R_1(eval(360 - $1))')

define(`X', 0)define(`Y', 0)define(`w', 10)define(`z', 1)
define(`N_2', `define(`z', eval(z + $1))')
define(`E_2', `define(`w', eval(w + $1))')
define(`S_2', `define(`z', eval(z - $1))')
define(`W_2', `define(`w', eval(w - $1))')
define(`F_2', `define(`X', eval(X + w * $1))define(`Y', eval(Y + z * $1))')
define(`R_2', `R_$1(w, z)')
define(`R_90', `define(`w', $2)define(`z', eval(- $1))')
define(`R_180', `define(`w', eval(- $1))define(`z', eval(- $2))')
define(`R_270', `define(`w', eval(- $2))define(`z', $1)')
define(`L_2', `R_2(eval(360 - $1))')

do()
define(`part1', eval(abs(x) + abs(y)))
define(`part2', eval(abs(X) + abs(Y)))

divert`'part1
part2
