divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day9.input] [-Dpreamble=N] day9.m4

include(`common.m4')ifelse(common(9), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')
ifdef(`preamble', `', `define(`preamble', 25)')

define(`list', translit(include(file), nl, `;'))
ifdef(`__gnu__', `
  patsubst(defn(`list'), `\([^;]*\);', `pushdef(`val', `\1')')
',`
  define(`chew', `pushdef(`val', substr(`$1', 0, index(`$1', `;')))define(
    `tail',substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(`tail'),
    `;'), -1, `', `$0(defn(`tail'))')')
  define(`Split', `ifelse(eval($1 < 35), 1, `chew(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  Split(len(defn(`list')), defn(`list'))
')

define(`check', `ifelse($1, $2, `', `ifdef(`m'eval(($1 != 2 * $2) * ($1 - $2)),
  1, `$0($1, defn(`m$2'))')')')
define(`do', `define(`m'last, $1)define(`last', $1)ifelse(check($1, head), `',
  `define(`part1', $1)undefine(`t')', `define(`head',
  defn(`m'head)popdef(`m'head))')')
pushdef(`do', `define(`m'last, $1)define(`last', $1)define(`iter',
  incr(iter))ifelse(iter, preamble, `popdef(`do')')')
pushdef(`do', `define(`head', $1)define(`last', $1)define(`iter',
  1)popdef(`do')')
stack_foreach(`val', `do')

define(`trimhead', `ifelse(eval(sum > 'part1`), 1, `define(`sum', eval(sum
  - defn(`a'l)))define(`l', incr(l))$0()')')
define(`do', `trimhead()ifelse(sum, 'part1`, `undefine(`t')', `define(`r',
  incr(r))define(`a'r, $1)define(`sum', eval(sum + $1))')')
pushdef(`do', `define(`l', 0)define(`r', 0)define(`a'r, $1)define(`sum',
  $1)popdef(`do')')
stack_foreach(`val', `do')
define(`lo', part1)define(`hi', 0)
define(`find', `ifelse(eval($1 < lo), 1, `define(`lo', $1)', eval($1 > hi), 1,
  `define(`hi', $1)')')
forloop(l, r, `find(a', `)')
define(`part2', eval(lo + hi))

divert`'part1
part2
