divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day4.input] day4.m4

include(`common.m4')ifelse(common(4), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`part1', 0)define(`part2', 0)
define(`list', translit(include(defn(`file'))nl, `#: 'nl, `H_..')))
ifdef(`__gnu__', `
  define(`list', patsubst(defn(`list'), `\.\.', `;'))
  patsubst(defn(`list'), `\([^;]*\);', `pushdef(`line', `\1')')
', `
  define(`chew', `pushdef(`line', substr(`$1', 0, index(`$1', `..')))define(
    `tail', substr(`$1', incr(incr(index(`$1', `..')))))ifelse(
    index(defn(`tail'), `..'), -1, `', `$0(defn(`tail'))')')
  define(`split', `ifelse(eval($1 < 160), 1, `chew(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  split(len(defn(`list')), defn(`list'))
')
define(`bound', `eval($1 >= $2 && $1 <= $3)')
define(`byr_', `bound($1, 1920, 2002)')
define(`iyr_', `bound($1, 2010, 2020)')
define(`eyr_', `bound($1, 2020, 2030)')
define(`hgt_', `translit($1, 0123456789)vh(translit($1, `cmin'))')
define(`vh', 0)
define(`cmvh', `bound($1, 150, 193)')
define(`invh', `bound($1, 59, 76)')
define(`hcl_', `ifelse(`H'substr(`$1', 0, 1)`'len(`$1'), translit(`$1',
  `0123456789abcdef')`H7', 1, 0)')
define(`ecl_', `ifelse(`$1', `amb', 1, `$1', `blu', 1, `$1', `brn', 1,
  `$1', `gry', 1, `$1', `grn', 1, `$1', `hzl', 1, `$1', `oth', 1, 0)')
define(`pid_', `ifelse(len($1)len(translit($1, 0123456789)), 90, 1, 0)')
define(`cid_')
define(`wrap', `define(`$1_', `define(`r', defn(`r')'defn(`$1_')`)')')
wrap(`byr')wrap(`iyr')wrap(`eyr')wrap(`hgt')wrap(`hcl')wrap(`ecl')wrap(`pid')
define(`_', `ifelse(r, 1111111, `define(`part2', incr(part2))', len(r), 7,
  `define(`part1', incr(part1))')define(`r')')
define(`do', `substr(`$1', 0, 3)_(substr(`$1', 4))')
define(`doline', `foreach(`do', translit(`$1', `.', `,'),)')
stack_foreach(`line', `doline')
define(`part1', eval(part1 + part2))

divert`'part1
part2
