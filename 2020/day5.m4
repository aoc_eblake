divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day5.input] day5.m4

include(`common.m4')ifelse(common(5), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`lo', 1023)define(`hi', 0)define(`s', 0)
define(`list', quote(translit(include(defn(`file')), `FBRL'nl, `0110,')))
define(`store', `ifelse(eval($1 < lo), 1, `define(`lo', $1)')ifelse(eval($1
  > hi), 1, `define(`hi', $1)')define(`s', eval(s + $1))')
ifdef(`__gnu__', `patsubst(defn(`list'), `\([^,]*\),', `store(eval(0b\1))')',
  `define(`b2d', `_$0(0, `$1')')define(`_b2d', `ifelse(`$2', `', `$1',
    `$0(eval($1*2+substr(`$2', 0, 1)), substr(`$2', 1))')')
  define(`split', `ifelse($1, 10, `store(b2d($2))', `$0(eval($1/20*10),
    substr(`$2', 0, eval($1/20*10)))$0(eval($1-$1/20*10), substr(`$2',
    eval($1/20*10)))')')
  split(eval(len(defn(`list'))/11*10), translit(defn(`list'), `,'))')
define(`part1', hi)
define(`part2', eval((lo + hi) * (hi - lo + 1) / 2 - s))

divert`'part1
part2
