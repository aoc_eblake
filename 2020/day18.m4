divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day18.input] day18.m4

include(`common.m4')ifelse(common(18), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

include(`math64.m4')
ifdef(`__gnu__', `
  define(`do', `patsubst(`$1', `\([^;]*\);', `line(`\1')')')
',`
  define(`_chew', `line(substr(`$1', 0, index(`$1', `;')))define(`tail',
    substr(`$1', incr(index(`$1', `;'))))ifelse(index(defn(`tail'), `;'), -1,
    `', `$0(defn(`tail'))')')
  define(`chew', `ifelse(eval($1 < 380), 1, `_$0(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  define(`do', `chew(len(`$1'), `$1')')
')
define(`part1', 0)define(`part2', 0)
define(`op', `ifelse($2, +, `add64', `mul64')($1, $3)')
define(`math', `ifelse(index(`$1', `('), 0, `math(math$1, shift($@))',
  index(`$3', `('), 0, `math($1, $2, math$3, shift(shift(shift($@))))',
  $2, `', $1, p($@)`math(op($1, $2, $3), shift(shift(shift($@))))')')
define(`p', `$2$4, `*+', `math($1, $2, math(shift(shift($@))))', ')
define(`line', `_$0(translit($1, `<> ', `(),'))')
define(`_line', `pushdef(`p')define(`part1', add64(part1,
  math($@)))popdef(`p')define(`part2', add64(part2, math($@)))')
do(translit(include(defn(`file')), `()'nl, `<>;'))

divert`'part1
part2
