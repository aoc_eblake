divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day1.input] day1.m4

include(`common.m4')ifelse(common(1), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`list', quote(translit(include(defn(`file')), nl, `,')2021))
_foreach(`pushdef(`row', ', `)', `', list)
define(`do', `ifdef(`l'eval(2020 - $1), `define(`part1',
  eval($1 * (2020 - $1)))')define(`l$1')')
stack_foreach(`row', `do')
define(`_inner', `ifelse($1, $3, `', $2, $3, `', `ifdef(`l$3',
  `define(`part2', eval($1 * $2 * $3))')')')
define(`inner', `ifelse($1, $2, `', `_$0($1, $2, eval(2020 - $1 - $2))')')
define(`do', `ifelse(len($1), 3, `stack_reverse(`t', `t2')stack_reverse(`t2',
  `t', `inner($1, defn(`t'))')')')
stack_foreach(`row', `do')dnl common.m4 uses `t' for rest of stack

divert`'part1
part2
