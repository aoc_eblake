divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day15.input | -Dinput=1,2,...,] [-Dhashsize=H] \
# [-Dmax=N] [-Dverbose=V] day15.m4

include(`common.m4')ifelse(common(15, 30000001), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

ifdef(`max', `', `define(`max', 30000000)')
ifdef(`input', `', `define(`input', quote(translit((include(defn(`file'))),
  nl`()', `,')))')
define(`D', defn(`define'))
define(`I', defn(`incr'))
define(`i', defn(`ifdef'))
define(`E', defn(`eval'))
define(`t', `i(`t$1',`t$1',`T')($1,$2,i(`m$2',`E($1-m$2)',0))')
define(`T', `D(`m$2',$1)t(I($1),$3)')
define(`t2020', `define(`part1', $3)T($@)')
define(`rename', `ifdef(`$2', `', `define(`$2', defn(`$1'))')popdef(`$1')')
define(`t100000', `output(1, `...$1')rename(`$0', `t'eval($1 + 100000))T($@)')
define(`t'max, `define(`part2', $3)')
define(`prep', `ifelse(`$1', `', `', `$0(shift($@))pushdef(`t',
  `popdef(`t')define(`m$1', incr($'1`))t(incr($'1`), $1)')')')
prep(input)
t(1)

divert`'part1
part2
