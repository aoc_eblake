divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day6.input] day6.m4

include(`common.m4')ifelse(common(6), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`reg', 0)define(`p1', 0)define(`p2', -1)
define(`part1', 0)define(`part2', 0)
define(`list', translit(include(defn(`file')), alpha, ALPHA)nl)
define(`letter', `define(substr(ALPHA, decr($1), 1)`_',
  `undefine(`eol')define(`reg', eval(reg | (1 << $1)))')')
forloop_arg(1, 26, `letter')
define(`pop', `len(translit(eval($1, 2), 0))')
define(`_', `ifdef(`eol',
  `define(`part1', eval(part1 + pop(p1)))define(`part2',
    eval(part2 + pop(p2)))define(`p1', 0)define(`p2', -1)',
  `define(`eol')define(`p1', eval(p1 | reg))define(`p2',
    eval(p2 & reg))define(`reg', 0)')')
ifdef(`__gnu__',
  `patsubst(defn(`list'), .\|nl, `\&_()')',
  `define(`split', `ifelse($1, 1, `$2_()', `$0(eval($1/2), substr(`$2', 0,
    eval($1/2)))$0(eval($1-$1/2), substr(`$2', eval($1/2)))')')
  split(len(defn(`list')), defn(`list'))')

divert`'part1
part2
