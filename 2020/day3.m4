divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day3.input] day3.m4

include(`common.m4')ifelse(common(3), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

include(`math64.m4')define(`e')
define(`list', quote(translit(include(defn(`file')), `.#'nl, `e1;')))
define(`width', index(defn(`list'), `;'))
ifdef(`__gnu__', `
  patsubst(defn(`list'), `\([^;]*\);', `pushdef(`row', `\1')')
', `
  define(`half', `eval($1/'width`/2*'width`)')
  define(`Split', `ifelse($1, 'width`, `pushdef(`row', `$2')', `$0(half($1),
    substr(`$2', 0, half($1)))$0(eval($1-half($1)), substr(`$2', half($1)))')')
  Split(eval(len(defn(`list'))/(width+1)*width), translit(defn(`list'), `;'))
')
define(`check', `substr(`$1', eval($2 % 'width`), 1)`'')
define(`iter', `ifelse(eval(y % $2), 0,
  `check(`$3', x)define(`x', eval(x + $1))')define(`y', incr(y))')
define(`run', `define(`x', 0)define(`y', 0)_stack_foreach(`row',
  `iter($@,', `)', `t')')
define(`part1', len(run(3, 1)))
define(`part2', mul64(eval(part1 * len(run(1, 1)) * len(run(5, 1))),
  eval(len(run(7, 1)) * len(run(1, 2)))))

divert`'part1
part2
