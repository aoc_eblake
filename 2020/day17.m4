divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day17.input] [-Dhashsize=H] day17.m4

include(`common.m4')ifelse(common(17, 65537), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`offset', incr(6))
define(`list', translit(include(defn(`file')), `.#'nl, `IAR'))
define(`x', offset)define(`y', offset)
define(`width', index(defn(`list'), `R'))
define(`height', len(translit(defn(`list'), `IA')))
define(`prep', `define(`c$1_$2_0_', 1)define(`c$1_$2_0_0', 1)pushdef(
  `cells', `$1,$2,0,')pushdef(`cells', `$1,$2,0,0')')
define(`I_', `define(`x', incr(x))')
define(`A_', `prep(x, y)I_()')
define(`R_', `define(`x', offset)define(`y', incr(y))')
ifdef(`__gnu__', `
  patsubst(defn(`list'), `.', `\&_()')
',`
  define(`split', `ifelse($1, 1, `$2_()', `$0(eval($1/2), substr(`$2', 0,
    eval($1/2)))$0(eval($1-$1/2), substr(`$2', eval($1/2)))')')
  split(len(defn(`list')), defn(`list'))
')
define(`whiledef', `ifdef(`$1', `$2($1)popdef(`$1')$0($@)')')
pushdef(`stack1', ``decr'')
pushdef(`stack1', ``first'')
pushdef(`stack1', ``incr'')
define(`treble', `pushdef(`$1', `$2,`decr'')pushdef(`$1',
  `$2,`first'')pushdef(`$1', `$2,`incr'')')
_stack_foreach(`stack1', `treble(`stack2', ', `)', `t')
_stack_foreach(`stack2', `treble(`stack3', ', `)', `t')
_stack_foreach(`stack3', `treble(`stack4', ', `)', `t')
define(`bump', `define(`n$1_$2_$3_$4', ifdef(`n$1_$2_$3_$4', `n$1_$2_$3_$4',
  `pushdef(`near', `$1,$2,$3,$4')')$5)')
define(`bumpall', `ifdef(`b$1_$2_$3_$4', `', `getb($@)')b$1_$2_$3_$4()')
define(`b', `ifelse($3, -1, `', $4, -1, `', $3$4$5$6, 0011,
  ``bump($1, $2, $3, $4, ----)'', $3$5, 01, ``bump($1, $2, $3, $4, --)'',
  $4$6, 01, ``bump($1, $2, $3, $4, --)'', ``bump($1, $2, $3, $4, -)'')')
define(`applystack', `stack_reverse(ifdef(`stack$1', ``stack$1', `stack$1_'',
  ``stack$1_', `stack$1''), `$2stack$1$3')')
define(`getb', `define(`b$1_$2_$3_$4', applystack(ifelse($4, `', 3, 4),
  `_$0($@,', `)'))')
define(`_getb', `b($5($1), $6($2), $7($3), ifelse($4, `', `',
  `$8($4)'), $3, $4)')
define(`adjust', `ifelse(n$1_$2_$3_$4, ---, `pushdef(`cells',
  `$1,$2,$3,$4')define(`c$1_$2_$3_$4', 1)', n$1_$2_$3_$4`'c$1_$2_$3_$4, ----1,
  `pushdef(`cells', `$1,$2,$3,$4')', `popdef(`c$1_$2_$3_$4')')popdef(
  `n$1_$2_$3_$4')')
define(`gen', `whiledef(`cells', `bumpall')whiledef(`near', `adjust')')
forloop_arg(1, offset - 1, `gen')
define(`count', `_$0(ifelse($4, `', ``part1'', ``part2''), $3, $4)')
define(`_count', `define(`$1', eval($1 + (!!$2+1)*(!!$3+1)))')
define(`part1', 0)define(`part2', 0)whiledef(`cells', `count')

divert`'part1
part2
