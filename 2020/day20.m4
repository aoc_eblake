divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day20.input] day20.m4

include(`common.m4')ifelse(common(20), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

include(`math64.m4')
define(`list', translit(include(defn(`file')), `.#'nl, `01'))
define(`flip', `ifelse(`$1', `', `', `$0(substr(`$1', $2), $2)substr(`$1', 0,
  $2)')')
define(`diag', `forloop_var(`i', 0, $2 - 1, `forloop(0, decr($2), `substr($1,
  eval(', ` * $2 + i), 1)')')')
define(`tile', `_$0(translit(`$1', ` :', `,,'))')
define(`_tile', `pushdef(`tiles', `$2')define(`t$2', forloop(1, 8,
  `substr(`$3', ', `1, 8)')findedges(`$3', `$2'))')
define(`edge', `ifdef(`e$1', `define(`e$1', defn(`e$1')`, $2')',
  `pushdef(`edges', $1)define(`e$1', $2)')')
define(`edgepair', `edge($1, $2)edge(flip($1, 1), $2)`, $1'')
define(`findedges', `edgepair(substr(`$1', 0, 10), `$2')edgepair(substr(`$1',
  90), `$2')edgepair(forloop(0, 9, `substr(`$1', ', `0, 1)'),
  `$2')edgepair(forloop(0, 9, `substr(`$1', ', `9, 1)'), `$2')')
ifdef(`__gnu__', `
  patsubst(defn(`list'), translit(eval(110, 1), 1, .), `tile(`\&')')
', `
  define(`half', `eval($1/110/2*110)')
  define(`chew', `ifelse($1, 110, `tile(`$2')', `$0(half($1), substr(`$2', 0,
    half($1)))$0(eval($1-half($1)), substr(`$2', half($1)))')')
  chew(len(defn(`list')), defn(`list'))
')
define(`part1', 1)define(`part2', 0)
define(`count', `define(`part2', eval(part2 + len(translit($1, 0))))$@')
define(`_check', `ifelse(`$1', `$2', -)')
define(`check', `ifelse(_foreach(`_$0($1, defn(`e'', `))', count(t$1)), --,
  `define(`part1', mul64(part1, $1))define(`p_0_0', $1)')')
stack_foreach(`tiles', `check')
define(`hflip', `define(`t$1', _$0(t$1))')
define(`_hflip', `forloop(0, 8, `flip(substr($1, eval(', ` * 8), 8),
  1)')`, 'flip($2, 1)`, 'flip($3, 1)`, $5, $4'')
define(`vflip', `define(`t$1', _$0(t$1))')
define(`_vflip', `flip($1, 8)`, $3, $2, 'flip($4, 1)`, 'flip($5, 1)')
define(`swap', `define(`t$1', _$0(t$1))')
define(`_swap', `diag($1, 8)`, $4, $5, $2, $3'')
define(`right', `_$0(t$1)')
define(`_right', `$5')
define(`below', `_$0(t$1)')
define(`_below', `$3')
define(`neighbor', `_$0($2, first(defn(`e'$1($2))))')
define(`_neighbor', `ifelse($@, $2)')
define(`orient', `first(`orient'_$0($2, flip($2, 1), t$1)`$3($1)')')
define(`_orient', `ifelse($1, $4, 1, $1, $5, 2, $1, $6, 3, $1, $7, 4,
  $2, $4, 5, $2, $5, 6, $2, $6, 7, 8)')
define(`orient1r', `swap($1)')
define(`orient2r', `swap($1)hflip($1)')
define(`orient3r', `')
define(`orient4r', `hflip($1)')
define(`orient5r', `swap($1)vflip($1)')
define(`orient6r', `swap($1)vflip($1)hflip($1)')
define(`orient7r', `vflip($1)')
define(`orient8r', `vflip($1)hflip($1)')
define(`orient1b', `')
define(`orient2b', `vflip($1)')
define(`orient3b', `swap($1)')
define(`orient4b', `swap($1)vflip($1)')
define(`orient5b', `hflip($1)')
define(`orient6b', `vflip($1)hflip($1)')
define(`orient7b', `swap($1)hflip($1)')
define(`orient8b', `swap($1)vflip($1)hflip($1)')
ifelse(defn(`e'right(p_0_0)), p_0_0, `hflip(p_0_0)')
ifelse(defn(`e'below(p_0_0)), p_0_0, `vflip(p_0_0)')
define(`x', 0)define(`y', 0)
define(`nextrow', `define(`x', 0)_$0(y, incr(y))')
define(`_nextrow', `define(`p_0_$2', neighbor(`below', p_0_$1))orient(p_0_$2,
  below(p_0_$1), `b')define(`y', $2)')
pushdef(`nextrow', `popdef(`$0')')
define(`nextcol', `_$0(x, incr(x), y)')
define(`_nextcol', `define(`p_$2_$3', neighbor(`right', p_$1_$3))orient(
  p_$2_$3, right(p_$1_$3), `r')define(`x', $2)')
define(`until', `ifelse($1, $2, `', `$3`'$0($@)')')
until(`neighbor(`below', defn(`p_'x`_'y))', `defn(`p_'x`_'y)', `nextrow()
  until(`neighbor(`right', defn(`p_'x`_'y))', `defn(`p_'x`_'y)', `nextcol()')')
forloop_var(`j', 0, y, `forloop_var(`k', 0, 7, `define(`image'eval(j * 8 + k),
  forloop_var(`i', 0, x, `substr(first(first(defn(`t'defn(`p_'i`_'j)))),
  eval(k*8), 8)'))')')
define(`hpatt', ``18,0', `0,1', `5,1', `6,1', `11,1', `12,1', `17,1', `18,1',
  `19,1', `1,2', `4,2', `7,2', `10,2', `13,2', `16,2'')
define(`vpatt', ``0,18', `1,0', `1,5', `1,6', `1,11', `1,12', `1,17', `1,18',
  `1,19', `2,1', `2,4', `2,7', `2,10', `2,13', `2,16'')
define(`monsters', 0)
define(`match', `_$0(`$1, $2, $3, $4', $5)')
define(`_match', `ifelse(`$2', `', `define(`monsters', incr(monsters))',
  `ifelse(at($1, $2), 1, `$0(`$1', shift(shift($@)))')')')
define(`at', `substr(defn(`image'eval($2 + ($4$6))), eval($1 + ($3$5)), 1)')
forloop_var(`j', 0, (y+1) * 8 - 3, `forloop(0, eval((x+1) * 8 - 20), `match(',
  `, j, `', `', defn(`hpatt'))')')
forloop_var(`j', 0, (y+1) * 8 - 3, `forloop(0, eval((x+1) * 8 - 20), `match(',
  `, j, `19-', `', defn(`hpatt'))')')
forloop_var(`j', 0, (y+1) * 8 - 3, `forloop(0, eval((x+1) * 8 - 20), `match(',
  `, j, `', `2-', defn(`hpatt'))')')
forloop_var(`j', 0, (y+1) * 8 - 3, `forloop(0, eval((x+1) * 8 - 20), `match(',
  `, j, `19-', `2-', defn(`hpatt'))')')
forloop_var(`j', 0, (y+1) * 8 - 20, `forloop(0, eval((x+1) * 8 - 3), `match(',
  `, j, `', `', defn(`vpatt'))')')
forloop_var(`j', 0, (y+1) * 8 - 20, `forloop(0, eval((x+1) * 8 - 3), `match(',
  `, j, `', `19-', defn(`vpatt'))')')
forloop_var(`j', 0, (y+1) * 8 - 20, `forloop(0, eval((x+1) * 8 - 3), `match(',
  `, j, `2-', `', defn(`vpatt'))')')
forloop_var(`j', 0, (y+1) * 8 - 20, `forloop(0, eval((x+1) * 8 - 3), `match(',
  `, j, `2-', `19-', defn(`vpatt'))')')
define(`part2', eval(part2 - 15 * monsters))

divert`'part1
part2
