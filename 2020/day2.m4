divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day2.input] day2.m4

include(`common.m4')ifelse(common(2), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

define(`part1', 0)define(`part2', 0)
define(`list', translit(include(defn(`file')), nl`:', `.'))
define(`check1', `define(`part1', eval(part1 + ($3 >= $1 && $3 <= $2)))')
define(`check2', `define(`part2', eval(part2 + ($1 != $2)))')
define(`at', `ifelse(substr(`$1', decr($2), 1), `$3', 1, 0)')
define(`parse', `check1($1, $2, len(`$4') - len(translit(``$4'', `$3')))
  check2(at(`$4', $1, `$3'), at(`$4', $2, `$3'))')
ifdef(`__gnu__', `
  patsubst(defn(`list'), `\([0-9]*\)-\([0-9]*\) \(.\) \([^.]*\)\.',
    `parse(\1, \2, `\3', `\4')')
', `
  define(`chew', `parse(translit(substr(`$1', 0, index(`$1', `.')), `- ',
    `,,'))define(`tail', substr(`$1', incr(index(`$1', `.'))))ifelse(
    index(defn(`tail'), `.'), -1, `', `$0(defn(`tail'))')')
  define(`split', `ifelse(eval($1 < 60), 1, `chew(`$2')', `$0(eval($1/2),
    substr(`$2', 0, eval($1/2)))$0(eval(len(defn(`tail')) + $1 - $1/2),
    defn(`tail')substr(`$2', eval($1/2)))')')
  split(len(defn(`list')), defn(`list'))
')

divert`'part1
part2
