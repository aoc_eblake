divert(-1)dnl -*- m4 -*-
# Usage: m4 [-Dfile=day14.input] [-Dhashsize=H] day14.m4

include(`common.m4')ifelse(common(14, 65537), `ok', `',
`errprint(`Missing common initialization
')m4exit(1)')

include(`math64.m4')
define(`input', include(defn(`file')))
define(`p1h', 0)define(`p1l', 0)define(`p2', 0)
define(`mask', `define(`m', `$1')')
define(`mem', `ifdef(`m1h_$1', `define(`p1h', eval(p1h - m1h_$1))define(`p1l',
  eval(p1l - m1l_$1))')define(`tmp', `visit($1, eval($'1`, 2, 36), $'1`)')tmp')
define(`visit', `$0_1($1, m, $2)_foreach(`$0_2($3,', `)', addr(m,
  eval($1, 2, 36)))')
define(`visit_1', `define(`m1h_$1', merge(0, substr($2, 0, 18), substr($3, 0,
  18)))define(`m1l_$1', merge(0, substr($2, 18), substr($3, 18)))define(`p1h',
  eval(p1h + m1h_$1))define(`p1l', eval(p1l + m1l_$1))')
ifdef(`__gnu__', `
  define(`merge', `eval((0b$3 | 0b`'translit($2, `X', 0)) &
    0b`'translit($2, `X', 1))')
', `
  define(`merge', `ifelse(`$2', `', `$1', `$0(eval(2 * $1 + _$0(substr(`$2', 0,
    1), substr(`$3', 0,  1))), substr(`$2', 1), substr(`$3', 1))')')
  define(`_merge', `ifelse(`$1', 1, 1, `$1', 0, 0, $2)')
')
define(`big', `errprintn(`input branches too much for part 2')pushdef(`big')')
define(`addr', `ifelse(eval(len(translit($1, 01)) > 9), 1, `big()', index(`$1',
  `X'), -1, `_$0(translit($1, -0, `0X'), $2)', `addr(substr(`$1', 0, index(`$1',
  `X'))`'-substr(`$1', incr(index(`$1', `X'))), $2)addr(substr(`$1', 0,
  index(`$1', `X'))`'1substr(`$1', incr(index(`$1', `X'))), $2)')')
define(`_addr', `, eval(merge(0, substr($1, 0, 18), substr($2, 0, 18)), 2,
  18)eval(merge(0, substr($1, 18), substr($2, 18)), 2, 18)')
define(`visit_2', `ifdef(`m2_$2', `ifelse(index(p2, -), -1, `define(`p2',
  eval(p2 - m2_$2))', `pushdef(`p2', -m2_$2)')')define(`m2_$2',
  $1)ifelse(eval(p2 + $1 > $1), 1, `define(`p2', eval(p2 + $1))',
  `pushdef(`p2', $1)')')

translit(defn(`input'), =nl`[] ', `()()')
define(`part1', add64(mul64(p1h, eval(1<<18)), p1l))
define(`do', `ifdef(`p2', `define(`part2', add64(part2, p2))popdef(`p2')do()')')
define(`part2', 0)do()

divert`'part1
part2
