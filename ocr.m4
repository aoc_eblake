divert(-1)dnl -*- m4 -*-
# Optical Character Recognition of AOC fonts, using maps learned at
# https://hackage.haskell.org/package/advent-of-code-ocr-0.1.2.0/docs/src/Advent.OCR.LetterMap.html
# assumes common.m4 is already loaded

define(`_ocr', `pushdef(`ocr_', ``$2',``$1''')')
# small font: 2016d8, 2019d8, 2019d11, 2021d13, 2022d10
_ocr(`A',
` XX  'dnl
`X  X 'dnl
`X  X 'dnl
`XXXX 'dnl
`X  X 'dnl
`X  X ')
_ocr(`B',
`XXX  'dnl
`X  X 'dnl
`XXX  'dnl
`X  X 'dnl
`X  X 'dnl
`XXX  ')
_ocr(`C',
` XX  'dnl
`X  X 'dnl
`X    'dnl
`X    'dnl
`X  X 'dnl
` XX  ')
_ocr(`E',
`XXXX 'dnl
`X    'dnl
`XXX  'dnl
`X    'dnl
`X    'dnl
`XXXX ')
_ocr(`F',
`XXXX 'dnl
`X    'dnl
`XXX  'dnl
`X    'dnl
`X    'dnl
`X    ')
_ocr(`G',
` XX  'dnl
`X  X 'dnl
`X    'dnl
`X XX 'dnl
`X  X 'dnl
` XXX ')
_ocr(`H',
`X  X 'dnl
`X  X 'dnl
`XXXX 'dnl
`X  X 'dnl
`X  X 'dnl
`X  X ')
_ocr(`I',
`XXX  'dnl
` X   'dnl
` X   'dnl
` X   'dnl
` X   'dnl
`XXX  ')
_ocr(`J',
`  XX 'dnl
`   X 'dnl
`   X 'dnl
`   X 'dnl
`X  X 'dnl
` XX  ')
_ocr(`K',
`X  X 'dnl
`X X  'dnl
`XX   'dnl
`X X  'dnl
`X X  'dnl
`X  X ')
_ocr(`L',
`X    'dnl
`X    'dnl
`X    'dnl
`X    'dnl
`X    'dnl
`XXXX ')
_ocr(`O',
` XX  'dnl
`X  X 'dnl
`X  X 'dnl
`X  X 'dnl
`X  X 'dnl
` XX  ')
_ocr(`P',
`XXX  'dnl
`X  X 'dnl
`X  X 'dnl
`XXX  'dnl
`X    'dnl
`X    ')
_ocr(`R',
`XXX  'dnl
`X  X 'dnl
`X  X 'dnl
`XXX  'dnl
`X X  'dnl
`X  X ')
_ocr(`S',
` XXX 'dnl
`X    'dnl
`X    'dnl
` XX  'dnl
`   X 'dnl
`XXX  ')
_ocr(`U',
`X  X 'dnl
`X  X 'dnl
`X  X 'dnl
`X  X 'dnl
`X  X 'dnl
` XX  ')
_ocr(`Y',
`X   X'dnl
`X   X'dnl
` X X 'dnl
`  X  'dnl
`  X  'dnl
`  X  ')
_ocr(`Z',
`XXXX 'dnl
`   X 'dnl
`  X  'dnl
` X   'dnl
`X    'dnl
`XXXX ')

# large font: 2018d10
_ocr(`A',
`  XX   'dnl
` X  X  'dnl
`X    X 'dnl
`X    X 'dnl
`X    X 'dnl
`XXXXXX 'dnl
`X    X 'dnl
`X    X 'dnl
`X    X 'dnl
`X    X ')
_ocr(`B',
`XXXXX  'dnl
`X    X 'dnl
`X    X 'dnl
`X    X 'dnl
`XXXXX  'dnl
`X    X 'dnl
`X    X 'dnl
`X    X 'dnl
`X    X 'dnl
`XXXXX  ')
_ocr(`C',
` XXXX  'dnl
`X    X 'dnl
`X      'dnl
`X      'dnl
`X      'dnl
`X      'dnl
`X      'dnl
`X      'dnl
`X    X 'dnl
` XXXX  ')
_ocr(`E',
`XXXXXX 'dnl
`X      'dnl
`X      'dnl
`X      'dnl
`XXXXX  'dnl
`X      'dnl
`X      'dnl
`X      'dnl
`X      'dnl
`XXXXXX ')
_ocr(`F',
`XXXXXX 'dnl
`X      'dnl
`X      'dnl
`X      'dnl
`XXXXX  'dnl
`X      'dnl
`X      'dnl
`X      'dnl
`X      'dnl
`X      ')
_ocr(`G',
` XXXX  'dnl
`X    X 'dnl
`X      'dnl
`X      'dnl
`X      'dnl
`X  XXX 'dnl
`X    X 'dnl
`X    X 'dnl
`X   XX 'dnl
` XXX X ')
_ocr(`H',
`X    X 'dnl
`X    X 'dnl
`X    X 'dnl
`X    X 'dnl
`XXXXXX 'dnl
`X    X 'dnl
`X    X 'dnl
`X    X 'dnl
`X    X 'dnl
`X    X ')
_ocr(`J',
`   XXX 'dnl
`    X  'dnl
`    X  'dnl
`    X  'dnl
`    X  'dnl
`    X  'dnl
`    X  'dnl
`X   X  'dnl
`X   X  'dnl
` XXX   ')
_ocr(`K',
`X    X 'dnl
`X   X  'dnl
`X  X   'dnl
`X X    'dnl
`XX     'dnl
`XX     'dnl
`X X    'dnl
`X  X   'dnl
`X   X  'dnl
`X    X ')
_ocr(`L',
`X      'dnl
`X      'dnl
`X      'dnl
`X      'dnl
`X      'dnl
`X      'dnl
`X      'dnl
`X      'dnl
`X      'dnl
`XXXXXX ')
_ocr(`N',
`X    X 'dnl
`XX   X 'dnl
`XX   X 'dnl
`X X  X 'dnl
`X X  X 'dnl
`X  X X 'dnl
`X  X X 'dnl
`X   XX 'dnl
`X   XX 'dnl
`X    X ')
_ocr(`P',
`XXXXX  'dnl
`X    X 'dnl
`X    X 'dnl
`X    X 'dnl
`XXXXX  'dnl
`X      'dnl
`X      'dnl
`X      'dnl
`X      'dnl
`X      ')
_ocr(`R',
`XXXXX  'dnl
`X    X 'dnl
`X    X 'dnl
`X    X 'dnl
`XXXXX  'dnl
`X  X   'dnl
`X   X  'dnl
`X   X  'dnl
`X    X 'dnl
`X    X ')
_ocr(`X',
`X    X 'dnl
`X    X 'dnl
` X  X  'dnl
` X  X  'dnl
`  XX   'dnl
`  XX   'dnl
` X  X  'dnl
` X  X  'dnl
`X    X 'dnl
`X    X ')
_ocr(`Z',
`XXXXXX 'dnl
`     X 'dnl
`     X 'dnl
`    X  'dnl
`   X   'dnl
`  X    'dnl
` X     'dnl
`X      'dnl
`X      'dnl
`XXXXXX ')

define(`ocr_l', `ifelse(`$1',$2)')
define(`_ocr', `ifelse(len(`$1'), 1, ``$1'',
  `errprintn(`ocr map incomplete')forloop(0, $3, `nl`'substr(`$2', eval(',
  `*$4), $4)`'')')')
define(`ocr', `_$0(_stack_foreach(`$0_', `ocr_l(`$1',', `)', `t'), `$1',
  ifelse(len(`$1'), 30, `6,5', `10,6'))')
